BIG Folio Website
================

Requirements
----------------

This site is built using [Jekyll](http://jekyllrb.com/), which requires Ruby and the jekyll gem. No database required and Ruby is not required on the server. 

Once installed, the static site will be generated and placed in a _site folder. This folder is not part of the repository. The _sync.sh file will build and upload the generated site to our server. 

Data
----------------

There are YAML files in the _data folder which are used in the site. 

**samples.yml** 

This file stores all the sample Rubix screenshot file names & links used on the home page. 

