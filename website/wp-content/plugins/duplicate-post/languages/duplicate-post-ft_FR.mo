��          �      �       0     1     E  	   _     i     x     �     �  '   �  '   �  3     3   M     �  �  �      T     u     �     �     �     �     �     �     �  n     3   z     �                            
      	                        Copy to a new draft Creates a copy of a post. Duplicate Duplicate Post Enrico Battocchi Make a duplicate from this page Make a duplicate from this post No page to duplicate has been supplied! No post to duplicate has been supplied! Post creation failed, could not find original post: http://wordpress.org/extend/plugins/duplicate-post/ http://www.lopo.it Project-Id-Version: Duplicate Post 1.0
Report-Msgid-Bugs-To: http://wordpress.org/tag/duplicate-post
POT-Creation-Date: 2009-12-02 23:12+0000
PO-Revision-Date: 2009-12-03 15:30+0100
Last-Translator: Enrico Battocchi <enrico.battocchi@gmail.com>
Language-Team: Enrico Battocchi <enrico.battocchi@gmail.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
X-Poedit-Language: French
X-Poedit-Country: FRANCE
 Copier dans un nouveau brouillon Pour copier les articles. Copier Duplicate Post Enrico Battocchi Copier cette page Copier cet article Aucune page à copier! Aucun article à copier! La création de l&rsquo;article a échoué, il n&rsquo;était pas possible de trouver l&rsquo;article original http://wordpress.org/extend/plugins/duplicate-post/ http://www.lopo.it 