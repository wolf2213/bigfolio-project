��          �      �       0     1     E  	   _     i     x     �     �  '   �  '   �  3     3   M     �  �  �     <     W     q     x     �     �     �  (   �  (   �  Q   (  3   z     �                            
      	                        Copy to a new draft Creates a copy of a post. Duplicate Duplicate Post Enrico Battocchi Make a duplicate from this page Make a duplicate from this post No page to duplicate has been supplied! No post to duplicate has been supplied! Post creation failed, could not find original post: http://wordpress.org/extend/plugins/duplicate-post/ http://www.lopo.it Project-Id-Version: Duplicate Post 1.0
Report-Msgid-Bugs-To: http://wordpress.org/tag/duplicate-post
POT-Creation-Date: 2009-12-02 23:12+0000
PO-Revision-Date: 2009-12-10 01:14+0100
Last-Translator: Enrico Battocchi <enrico.battocchi@gmail.com>
Language-Team: Enrico Battocchi <enrico.battocchi@gmail.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
X-Poedit-Language: Spanish
 Copia en un borrador nuevo Para copiar las entradas. Copiar Duplicate Post Enrico Battocchi Crea una copia de esta página Crea una copia de esta entrada No se facilitó ninguna página a copiar No se facilitó ninguna entrada a copiar Creación realizada sin éxito, no ha sido posible encontrar la entrada original: http://wordpress.org/extend/plugins/duplicate-post/ http://www.lopo.it 