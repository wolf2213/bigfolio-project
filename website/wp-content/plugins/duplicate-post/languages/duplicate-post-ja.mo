��          �      �       0     1     E  	   _     i     x     �     �  '   �  '   �  3     3   M     �  t  �  !   	  $   +     P     W     f  $   w  $   �  5   �  2   �  R   *  3   }     �                            
      	                        Copy to a new draft Creates a copy of a post. Duplicate Duplicate Post Enrico Battocchi Make a duplicate from this page Make a duplicate from this post No page to duplicate has been supplied! No post to duplicate has been supplied! Post creation failed, could not find original post: http://wordpress.org/extend/plugins/duplicate-post/ http://www.lopo.it Project-Id-Version: Duplicate Post 0.6.1
Report-Msgid-Bugs-To: http://wordpress.org/tag/duplicate-post
POT-Creation-Date: 2009-12-02 23:12+0000
PO-Revision-Date: 2009-12-03 15:34+0100
Last-Translator: Naoko McCracken <info@nao-net.com>
Language-Team: WordPress J-Series Project <http://wppluginsj.sourceforge.jp/i18n-ja_jp/>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Language: Japanese
X-Poedit-Country: JAPAN
X-Poedit-SourceCharset: utf-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_c
X-Poedit-Basepath: ../
Plural-Forms: nplurals=1; plural=0;
X-Poedit-SearchPath-0: .
 新規下書きとしてコピー 投稿の複製を作成します。 複製 Duplicate Post Enrico Battocchi この投稿を元に複製を作成 この投稿を元に複製を作成 複製元のページが指定されていません ! 複製元の投稿が指定されていません ! 複製の作成に失敗しました。複製元の投稿が見つかりません: http://wordpress.org/extend/plugins/duplicate-post/ http://www.lopo.it 