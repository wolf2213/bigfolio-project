=== Multi-level Navigation Plugin ===
Contributors: ryanhellyer
Donate link: http://pixopoint.com/premium_support/
Tags: navigation, dropdown, menu, flyout, suckerfish, nav, slider, jquery, sfhover
Requires at least: 2.3
Tested up to: 2.7
Stable tag: 1.0.8

Description: Adds a dropdown/flyout/slider menu (using Suckerfish technnique) to your WordPress blog. Visit the <a href="http://pixopoint.com/multi-level-navigation/">Multi-level Navigation Plugin page</a> for more information about the plugin, or our navigation <a href="http://pixopoint.com/forum/index.php?board=4.0">support board</a> for help with adding the menu to your theme. Paid premium support for this plugin is available via the <a href="http://pixopoint.com/premium-support/">PixoPoint Premium Support service</a>.

== Description ==

Adds a Suckerfish dropdown/flyout/slider menu to your WordPress blog. Visit the <a href="http://pixopoint.com/multi-level-navigation/">Multi-level Navigation Plugin page</a> for more information about the plugin, or the PixoPoint navigation <a href="http://pixopoint.com/forum/index.php?board=4.0">support board</a> for help with adding the menu to your theme.

The plugin produces W3C valid HTML and CSS. The options page allows you to choose what items (pages, categories, archives etc.) are included in the menu and whether you want your menu to be keyboard accessible or not. The Son of Suckerfish technique is used to enable the menu to function in very old browsers such as IE 6.

You can style the menu by copy and pasting the code from the <a href="http://pixopoint.com/suckerfish_css/">Suckerfish CSS Generator page</a>.

To see a live demo of the plugin in action, please visit the <a href="http://pixopoint.com/demo/wordpress/index.php?wptheme=Dropdown%20Plugin">PixoPoint WordPress demo page</a> or see examples of other sites in the plugins' <a href="http://pixopoint.com/forum/index.php?topic=357.0">Live Examples forum topic</a>.

= Beta version =
A new version of thePixoPoint Multi-level Navigation Plugin is due to be released shortly. It features a signifcant upgrade over the current version. If you are interested in trying this version, please visit the <a href="http://pixopoint.com/forum/index.php?topic=660.0">Multi-level Navigation Beta plugin page</a>.

The new beta version features easy integration into themes exported from the <a href="http://pixopoint.com/generator/">PixoPoint Template Generator</a>. Themes from the template generator integrate flawlessly with the new version of the plugin and do not require any theme editing. Simply activate your theme, then activate the plugin and the new menu will appear instantly. Visit the <a href="http://pixopoint.com/generator/">PixoPoint Template Generator</a> to get your own pre-supported theme.

Here is a list of the various changes between the current version and the new beta"
* Totally redesigned admin interface
* Changed menu option names to make things more obvious to new users of the plugin
* Support for blogroll/links categories as a menu option
* Ability to include as well as exclude specific pages or categories from the menu
* Support for changing wp-content/plugins folder
* Option in admin panel to allow users to change between their own themes CSS and the plugin CSS (for themes designed for the Multi-level Navigation Plugin
* Support for hover delay
* Uninstall system. When plugin is deleted via the WordPress admin panel, it automatically removes the database info added by the plugin
* Improved keyboard accessibility scriptj which uses jQuery. Script courtesy of <a href="http://transientmonkey.com/">malcalevak</a>
* Removed need to tick enable animations box. You now just need to change the speed of the menu.
* Changed function calls from suckerfish() to pixpoint_menu() as very few people know what a 'suckerfish' is.
* Added 'recommended plugins' and 'FAQ' sections in admin page
* Control how sensitive the menu is onmouseover
* Mousein delay option in admin page
* Maintenance mode. Allows you to test and develop your menu while hiding it from your sites visitors.

Thanks to <a href="http://transientmonkey.com/">malcalevak</a> for massive help with development of this version.

== Installation ==

After you've downloaded and extracted the files:

1. Upload the complete `multi-level-navigation-plugin` folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Add &lt;?php if (function_exists('suckerfish')) {suckerfish();} ?&gt; to you theme wherever you want the menu to appear (usually your header.php file)
4. Copy the CSS from the <a href="http://pixopoint.com/suckerfish_css/">Multi-level menu CSS Generator page</a>
5. Visit the "Multi-level Navigation" page in your WordPress options menu

= Premium Support =
For direct help via the plugin (and CSS generator) author, please sign up for the <a href="http://pixopoint.com/premium-support/">PixoPoint Premium Support service</a>.

The <a href="http://pixopoint.com/premium-support/">PixoPoint Premium Support</a> option is ideal if you have insufficient time to fix any problems you may have or simply don't know much about coding. You will receive not only techinical support and access to the latest PixoPoint betas but they will also install the plugin and set it up on your site if needed. Customisations of the plugin are also done for some premium members, although we recommend asking first via the <a href="http://pixopoint.com/contact/">PixoPoint Contact Form</a> for such requests as some customisations may require extra payment depending on their complexity.

= Free support =

If you follow all of the instructions here, activate the plugin and find the menu is appearing on your site but looks  messed up, then the problem is probably caused by a clash between your themes CSS and plugins CSS. These problems can usually be remedied by removing the wrapper tags which surround the menu in your theme. For example, most themes will have some HTML such as <div id="nav"><?php wp_list_pages(); ?></div> which contains the existing themes menu. By placing the pixopoint_menu() function between those DIV tags, the menu will often interact with that DIV tag. The solution is to either remove the DIV tag or to alter it's CSS so that it doesn't interact with the menu.

If you require further help with the plugin, please visit the <a href="http://pixopoint.com/multi-level-navigation/">PixoPoint Multi-level Navigation Plugin page</a> or the <a href="http://pixopoint.com/forum/">PixoPoint support forum</a>.

Please read the following tips to help have questions answered faster.
* Where is your CSS?
* What modifications have you made to the CSS?
* What browsers are you having problems with?
* What is the URL for your site?
* Provide a link to the problem. Most problems can not be answered without actually seeing your site. If you don't want to install the plugin on your live site and don't have a test site to show us, then view the source code in your browser when you do have the plugin installed, save it to an HTML file and upload that somewhere so that we can see what the page looks like.
* Do not bother providing us with HTML and/or CSS code snippets (without a link). There is very little we can do without seeing the entire page as most problems are caused by an obscure piece of CSS somewhere else on the page.
* Let us know if you have modified the CSS. If it is modified beyond what is available in the CSS generator we are unlikely to offer support for free. Rummaging through other peoples code is too time consuming sorry.
* If you didn't paste your CSS into the WP plugins settings page, let us know which exact file it is in. Searching through a dozen CSS files in your theme trying to find your menu code is not fun.

== Frequently Asked Questions ==

= How do I get a fully customised version? =

Leave a message on the PixoPoint <a href="http://pixopoint.com/contact/">Contact Page</a> with your requirements and we will get back to you ASAP with pricing information.
Alternatively you can sign up for our <a href="http://pixopoint.com/premium-support/">Premium Support</a> option which gives you access to our new dropdown, flyout and
slider menu CSS generator, plus access to our premium support forum.

= Why can't the plugin do X, Y or Z? =

It probably can, we just haven't supplied instructions on how to do it. If you have any requests, then please leave them in the <a href="http://pixopoint.com/forum/index.php?board=4.0">PixoPoint dropdown menu support board</a>. We often update the plugin with new functionality and we're far more likely to include the functionality you want if we know there is a demand for it already.

= Why should I use this plugin? =

If you are having trouble making your posts easily accessible to your users without them having to rifle endlessly through pages of posts.

If you have been using a menu which requires Javascript to work, then this plugin will allow more of your visitors to access your site.

If you want to have an easy way to style your dropdown menu. This plugin is 100% compatible with the code generated on the <a href="http://pixopoint.com/suckerfish_css/">Suckerfish Dropdown CSS Generator page</a>.

= Does it work for WordPress version x.x.x? =

We have only tested this plugin on WordPress 2.3+, however it should (in theory) work in WordPress 2.0+. Please let us know if you successfully test it on another version of WordPress.

If you are using a newer version of WordPress than the latest version supported then we suggest trying the plugin anyway. It shouldn't (in theory) break anything other than the plugin itself which you can just delete if it doesn't work.

== History ==

Version 0.95 Beta: Upgrade from 'Ryans Suckerfish Dropdown Menu'<br />
Version 0.96 Beta: Fixed the "There is a new version of ..." bug<br />
Version 0.97 Beta: Corrected 'Custom 1' bug<br />
Version 0.98 Beta: Added inline CSS option<br />
Version 0.99 RC: Final test version before stable release<br />
Version 1.0: Official release to WordPress plugins repository<br />
Version 1.0.1: Corrected VERY MAJOR bug which prevented menu from functioning in IE6 - how did nobody notice this?<br />
Version 1.0.2: Removed random file from plugin and confirmed support for WordPress 2.6<br />
Version 1.0.3: Stupid WP repository packed a wobbly and wouldn't upload the images folder so resorted to releasing new version<br />
Version 1.0.4: Fixed W3C validation error<br />
Version 1.0.5: Refixed infernal IE Javascript bug<br />
Version 1.0.6: Refixed infernal IE Javascript bug AGAIN!!!!<br />
Version 1.0.6.1: Will probably fix that darn IE Javascript bug again :(<br />
Version 1.0.7: Included support for WordPress 2.7<br />
Version 1.0.8: Updated documentation<br />

= Credits =

Thanks to the following for help with the development of this plugin:<br />

* jenyum - Reported WordPress 2.7 RC1 admin page bug<br />
* Ande - Reported W3C validation bug<br />
* Kevin M. Russell - helped with the style.php bug<br />
* <a href="http://access-bydesign.com/">Clive Loseby</a> - discovered strict doctype validation error<br />
* <a href="http://www.fether.net/">Paula</a> - Feature suggestions and donation<br />
* <a href="http://www.marucchi.com/">Karim  A. Marucchi</a> - Bug reporting, feature suggestions and donations</br />
* <a href="http://www.acooldryplacephotography.com/">Mark Gooding</a> - created a new version featuring a custom menu option. Although this menu system hasn't been used directly in this plugin, an almost identical system has since been installed based on the concept that Mark created.<br />
* <a href="http://www.veterinaryparasite.com/">BigAlReturns</a> - helped programming the pages and categories exclusions<br />
* <a href="http://wordpressgarage.com/">Miriam Schwab</a> - brought lack of WP dropdown plugins to my attention<br />
* <a href="http://wpcandy.com/author/admin/">Michael Castilla</a><br />
* <a href="http://cjbonline.org/">CjB</a> - Bug reporting<br />
* <a href="http://www.clearvisionpartners.com/">Troy</a> - Beta testing feedback<br />
* <a href="http://www.michaelmitchell.co.nz/">Michael Mitchell</a> - programming assistance<br />
* <a href="http://www.carroll.org.uk/">Matthew Carroll</a> - designed code for accessible keyboard dropdown<br />
* <a href="http://chrislaing.net/">Chris Laing</a> - Beta feedback<br />
* <a href="http://jquery.com/">John Resig</a> - jQuery Javascript plugin<br />
* <a href="http://users.tpg.com.au/j_birch/plugins/superfish/">Joel Birch</a> - jQuery menu widget<br />
* <a href="http://brandonaaron.net">Brandon Aaron</a> - jQuery bgiframe plugin<br />
* <a href="http://cherne.net/brian/resources/jquery.hoverIntent.html">Brian Cherne</a> - jQuery Hover Intent plugin<br />
* <a href="http://abundantharvest.biz/">Karynn</a> - Bug reporting</br />

