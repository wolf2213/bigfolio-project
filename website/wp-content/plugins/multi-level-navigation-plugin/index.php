<?php
/*
Plugin Name: Multi-level Navigation Plugin
Plugin URI: http://pixopoint.com/multi-level-navigation/
Description: A WordPress plugin which adds a multi-level Suckerfish dropdown/flyout/slider menu to your WordPress blog. Visit the <a href="http://pixopoint.com/multi-level-navigation/">WP Multi-level Navigation Plugin page</a> for more information about the plugin, or our navigation <a href="http://pixopoint.com/forum/index.php?board=4.0">support board</a> for help with adding the menu to your theme.
Author: PixoPoint Web Development / Ryan Hellyer
Version: 1.0.8
Author URI: http://pixopoint.com/

Copyright 2008 PixoPoint Web Development

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

add_action('wp_head', 'suckerfishhead');


function suckerfishhead() {?>
	<!-- Multi-level Navigation Plugin v1.0.8 by PixoPoint Web Development ... http://pixopoint.com/multi-level-navigation/ -->
	<?php	if (get_option('suckerfish_keyboard') == 'on') { ?>
	<!--[if IE]><script type="text/javascript" src="<?php bloginfo('wpurl'); ?>/wp-content/plugins/multi-level-navigation-plugin/suckerfish_ie.js"></script><![endif]-->
	<script type="text/javascript" src="<?php bloginfo('wpurl'); ?>/wp-content/plugins/multi-level-navigation-plugin/suckerfish_keyboard.js"></script><?php } ?>
	<?php	if (get_option('suckerfish_keyboard') != 'on') { ?>
	<!--[if lte IE 6]><script type="text/javascript" src="<?php bloginfo('wpurl'); ?>/wp-content/plugins/multi-level-navigation-plugin/suckerfish_ie.js"></script><![endif]-->
	<?php } ?>
	<?php	if (get_option('suckerfish_disablecss') == 'on') {}
	else {?><link rel="stylesheet" type="text/css" href="<?php bloginfo('wpurl'); ?>/wp-content/plugins/multi-level-navigation-plugin/style.php" /><?php } ?>

	<?php
	if (get_option('suckerfish_superfish') == 'on') {echo '
	<!--[if IE 7]><!-->
	<script type="text/javascript" src="' , bloginfo('wpurl') , '/wp-content/plugins/multi-level-navigation-plugin/jquery-1.2.3.min.js"></script>
	<script type="text/javascript" src="' , bloginfo('wpurl') , '/wp-content/plugins/multi-level-navigation-plugin/hoverIntent.php"></script>
	<script type="text/javascript" src="' , bloginfo('wpurl') , '/wp-content/plugins/multi-level-navigation-plugin/bgIframe.js"></script>
	<script type="text/javascript" src="' , bloginfo('wpurl') , '/wp-content/plugins/multi-level-navigation-plugin/superfish.js"></script>
	<script type="text/javascript" src="' , bloginfo('wpurl') , '/wp-content/plugins/multi-level-navigation-plugin/superfish_style.php"></script>
	<!--<![endif]-->'
	;}


	if (get_option('suckerfish_inlinecss') == 'on') {?>
	<style type="text/css">
		<?php echo get_option('suckerfish_css'); ?>
	</style><?php }

}

	$suckerfish_css = get_option('suckerfish_css');

	function pages() {wp_list_pages('title_li=&exclude='. get_option('suckerfish_excludepages'));}
	function pagesdropdown() {echo '<li><a href="">' . get_option('suckerfish_pagestitle') . '</a><ul>' , wp_list_pages('title_li=&exclude='. get_option('suckerfish_excludepages')) , '</ul></li>';}
	function category() {wp_list_categories('title_li=&exclude='. get_option('suckerfish_excludecategories'));}
	function categoriesdropdown() {echo '<li><a href="">' . get_option('suckerfish_categoriestitle') . '</a><ul>' , wp_list_categories('title_li=&exclude='. get_option('suckerfish_excludecategories')) , '</ul></li>';}
	function home() {echo '<li><a href="' , bloginfo('url') , '/">' . get_option('suckerfish_hometitle') . '</a></li>';}
	function blogroll() {wp_list_bookmarks('title_li=&categorize=0');}
	function blogrolldropdown() {echo '<li><a href="">' . get_option('suckerfish_blogrolltitle') . '</a> <ul>' , wp_list_bookmarks('title_li=&categorize=0') , '</ul></li>';}
	function archivesmonths() {wp_get_archives('type=monthly');}
	function archivesyears() {wp_get_archives('type=yearly');}
	function archivesmonthsdropdown() {echo '<li><a href="">' . get_option('suckerfish_archivestitle') . '</a><ul>' , wp_get_archives('type=monthly') , '</ul></li>';}
	function archivesyearsdropdown() {echo '<li><a href="">' . get_option('suckerfish_archivestitle') . '</a><ul>' , wp_get_archives('type=yearly') , '</ul></li>';}
	function recentcomments() {echo '<li><a href="">' . get_option('suckerfish_recentcommentstitle') . '</a>'; global $wpdb; $sql = "SELECT DISTINCT ID, post_title, post_password, comment_ID, comment_post_ID, comment_author, comment_date_gmt, comment_approved, comment_type,comment_author_url, SUBSTRING(comment_content,1,30) AS com_excerpt FROM $wpdb->comments LEFT OUTER JOIN $wpdb->posts ON ($wpdb->comments.comment_post_ID = $wpdb->posts.ID) WHERE comment_approved = '1' AND comment_type = '' AND post_password = '' ORDER BY comment_date_gmt DESC LIMIT 10"; $comments = $wpdb->get_results($sql); $output = $pre_HTML; $output .= "\n<ul>"; foreach ($comments as $comment) {$output .= "\n<li><a href=\"" . get_permalink($comment->ID) . "#comment-" . $comment->comment_ID . "\" title=\"on " . $comment->post_title . "\">".strip_tags($comment->comment_author) .":" . " " . strip_tags($comment->com_excerpt) ."</a></li>"; } $output .= "\n</ul>"; $output .= $post_HTML; echo $output; echo '</li>';}
	function custom() {echo get_option('suckerfish_custommenu');}
	function custom2() {echo get_option('suckerfish_custommenu2');}


function suckerfish() {
	echo '<!-- Multi-level Navigational Plugin by PixoPoint Web Development ... http://pixopoint.com/multi-level-navigation/ -->
	<ul id="suckerfishnav">';

	$suckerfish_menuitem1 = get_option('suckerfish_menuitem1');
	$suckerfish_menuitem2 = get_option('suckerfish_menuitem2');
	$suckerfish_menuitem3 = get_option('suckerfish_menuitem3');
	$suckerfish_menuitem4 = get_option('suckerfish_menuitem4');
	$suckerfish_menuitem5 = get_option('suckerfish_menuitem5');
	$suckerfish_menuitem6 = get_option('suckerfish_menuitem6');
	$suckerfish_menuitem7 = get_option('suckerfish_menuitem7');
	$suckerfish_menuitem8 = get_option('suckerfish_menuitem8');
	$suckerfish_menuitem9 = get_option('suckerfish_menuitem9');
	$suckerfish_menuitem10 = get_option('suckerfish_menuitem10');

	switch ($suckerfish_menuitem1){	case "Pages":pages();break;	case "Pages Dropdown":pagesdropdown();break;	case "Categories":category();break;	case "Categories Dropdown":categoriesdropdown();break;	case "Home":home();break;	case "Blogroll":blogroll();break;	case "Blogroll Dropdown":blogrolldropdown();break;	case "Archives (months)":archivesmonths();break;	case "Archives (years)":archivesyears();break;	case "Archives (months) Dropdown":archivesmonthsdropdown();break;	case "Archives (years) Dropdown":archivesyearsdropdown();break;	case "Recent Comments":recentcomments();break;	case "Custom 1":custom();break; case "Custom 2":custom2();break; }
	switch ($suckerfish_menuitem2){	case "Pages":pages();break;	case "Pages Dropdown":pagesdropdown();break;	case "Categories":category();break;	case "Categories Dropdown":categoriesdropdown();break;	case "Home":home();break;	case "Blogroll":blogroll();break;	case "Blogroll Dropdown":blogrolldropdown();break;	case "Archives (months)":archivesmonths();break;	case "Archives (years)":archivesyears();break;	case "Archives (months) Dropdown":archivesmonthsdropdown();break;	case "Archives (years) Dropdown":archivesyearsdropdown();break;	case "Recent Comments":recentcomments();break;	case "Custom 1":custom();break; case "Custom 2":custom2();break; }
	switch ($suckerfish_menuitem3){	case "Pages":pages();break;	case "Pages Dropdown":pagesdropdown();break;	case "Categories":category();break;	case "Categories Dropdown":categoriesdropdown();break;	case "Home":home();break;	case "Blogroll":blogroll();break;	case "Blogroll Dropdown":blogrolldropdown();break;	case "Archives (months)":archivesmonths();break;	case "Archives (years)":archivesyears();break;	case "Archives (months) Dropdown":archivesmonthsdropdown();break;	case "Archives (years) Dropdown":archivesyearsdropdown();break;	case "Recent Comments":recentcomments();break;	case "Custom 1":custom();break; case "Custom 2":custom2();break; }
	switch ($suckerfish_menuitem4){	case "Pages":pages();break;	case "Pages Dropdown":pagesdropdown();break;	case "Categories":category();break;	case "Categories Dropdown":categoriesdropdown();break;	case "Home":home();break;	case "Blogroll":blogroll();break;	case "Blogroll Dropdown":blogrolldropdown();break;	case "Archives (months)":archivesmonths();break;	case "Archives (years)":archivesyears();break;	case "Archives (months) Dropdown":archivesmonthsdropdown();break;	case "Archives (years) Dropdown":archivesyearsdropdown();break;	case "Recent Comments":recentcomments();break;	case "Custom 1":custom();break; case "Custom 2":custom2();break; }
	switch ($suckerfish_menuitem5){	case "Pages":pages();break;	case "Pages Dropdown":pagesdropdown();break;	case "Categories":category();break;	case "Categories Dropdown":categoriesdropdown();break;	case "Home":home();break;	case "Blogroll":blogroll();break;	case "Blogroll Dropdown":blogrolldropdown();break;	case "Archives (months)":archivesmonths();break;	case "Archives (years)":archivesyears();break;	case "Archives (months) Dropdown":archivesmonthsdropdown();break;	case "Archives (years) Dropdown":archivesyearsdropdown();break;	case "Recent Comments":recentcomments();break;	case "Custom 1":custom();break; case "Custom 2":custom2();break; }
	switch ($suckerfish_menuitem6){	case "Pages":pages();break;	case "Pages Dropdown":pagesdropdown();break;	case "Categories":category();break;	case "Categories Dropdown":categoriesdropdown();break;	case "Home":home();break;	case "Blogroll":blogroll();break;	case "Blogroll Dropdown":blogrolldropdown();break;	case "Archives (months)":archivesmonths();break;	case "Archives (years)":archivesyears();break;	case "Archives (months) Dropdown":archivesmonthsdropdown();break;	case "Archives (years) Dropdown":archivesyearsdropdown();break;	case "Recent Comments":recentcomments();break;	case "Custom 1":custom();break; case "Custom 2":custom2();break; }
	switch ($suckerfish_menuitem7){	case "Pages":pages();break;	case "Pages Dropdown":pagesdropdown();break;	case "Categories":category();break;	case "Categories Dropdown":categoriesdropdown();break;	case "Home":home();break;	case "Blogroll":blogroll();break;	case "Blogroll Dropdown":blogrolldropdown();break;	case "Archives (months)":archivesmonths();break;	case "Archives (years)":archivesyears();break;	case "Archives (months) Dropdown":archivesmonthsdropdown();break;	case "Archives (years) Dropdown":archivesyearsdropdown();break;	case "Recent Comments":recentcomments();break;	case "Custom 1":custom();break; case "Custom 2":custom2();break; }
	switch ($suckerfish_menuitem8){	case "Pages":pages();break;	case "Pages Dropdown":pagesdropdown();break;	case "Categories":category();break;	case "Categories Dropdown":categoriesdropdown();break;	case "Home":home();break;	case "Blogroll":blogroll();break;	case "Blogroll Dropdown":blogrolldropdown();break;	case "Archives (months)":archivesmonths();break;	case "Archives (years)":archivesyears();break;	case "Archives (months) Dropdown":archivesmonthsdropdown();break;	case "Archives (years) Dropdown":archivesyearsdropdown();break;	case "Recent Comments":recentcomments();break;	case "Custom 1":custom();break; case "Custom 2":custom2();break; }
	switch ($suckerfish_menuitem9){	case "Pages":pages();break;	case "Pages Dropdown":pagesdropdown();break;	case "Categories":category();break;	case "Categories Dropdown":categoriesdropdown();break;	case "Home":home();break;	case "Blogroll":blogroll();break;	case "Blogroll Dropdown":blogrolldropdown();break;	case "Archives (months)":archivesmonths();break;	case "Archives (years)":archivesyears();break;	case "Archives (months) Dropdown":archivesmonthsdropdown();break;	case "Archives (years) Dropdown":archivesyearsdropdown();break;	case "Recent Comments":recentcomments();break;	case "Custom 1":custom();break; case "Custom 2":custom2();break; }
	switch ($suckerfish_menuitem10){	case "Pages":pages();break;	case "Pages Dropdown":pagesdropdown();break;	case "Categories":category();break;	case "Categories Dropdown":categoriesdropdown();break;	case "Home":home();break;	case "Blogroll":blogroll();break;	case "Blogroll Dropdown":blogrolldropdown();break;	case "Archives (months)":archivesmonths();break;	case "Archives (years)":archivesyears();break;	case "Archives (months) Dropdown":archivesmonthsdropdown();break;	case "Archives (years) Dropdown":archivesyearsdropdown();break;	case "Recent Comments":recentcomments();break;	case "Custom 1":custom();break; case "Custom 2":custom2();break; }

	echo '</ul>';
}





// OLD FUNCTIONS FOR PREVIOUS VERSIONS OF THE PLUGIN
function suckerfish1() {echo '<ul id="suckerfishnav">' , wp_list_pages('title_li=') , '</ul>';}
function suckerfish2() {echo '<ul id="suckerfishnav"><li><a href="' , bloginfo('url') , '/">Home</a></li>' , wp_list_pages('title_li=') , '</ul>';}
function suckerfish3() {echo '<ul id="suckerfishnav"><li><a href="#">Pages</a><ul>' , wp_list_pages('title_li=') , '</ul></li><li><a href="#">Archives</a><ul>' , wp_get_archives() , '</ul></li><li><a href="#">Categories</a><ul>' , wp_list_categories('title_li=') , '</ul></li><li><a href="#">Blogroll</a> <ul>' , wp_list_bookmarks('title_li=&categorize=0') , '</ul></li></ul>';}
function suckerfish4() {echo '<ul id="suckerfishnav">' , wp_list_pages('title_li=') , '<li><a href="#">Archives</a><ul>' , wp_get_archives() , '</ul></li><li><a href="#">Categories</a><ul>' , wp_list_categories('title_li=') , '</ul></li></ul>';}
function suckerfish5() {echo '<ul id="suckerfishnav"><li><a href="' , bloginfo('url') , '/">Home</a></li>' , wp_list_pages('title_li=') , '<li><a href="#">Archives</a><ul>' , wp_get_archives() , '</ul></li><li><a href="#">Categories</a><ul>' , wp_list_categories('title_li=') , '</ul></li></ul>';}





// adds options menu
add_action('admin_menu', 'show_suckerfish_options');

function show_suckerfish_options() {
// Add a new submenu under Options:
add_options_page('Multi-level Navigation Plugin Options', 'Multi-level Navigation', 8, 'suckerfishoptions', 'suckerfish_options');

add_option('suckerfish_css', '#suckerfishnav {background:#1F3E9F url("../multi-level-navigation-plugin/images/suckerfish_blue.png") repeat-x;font-size:18px;font-family:verdana,sans-serif;font-weight:bold;	width:100%;}#suckerfishnav, #suckerfishnav ul {float:left;list-style:none;line-height:40px;padding:0;border:1px solid #aaa;margin:0;	width:100%;}#suckerfishnav a {display:block;color:#dddddd;text-decoration:none;padding:0px 10px;}#suckerfishnav li {float:left;padding:0;}#suckerfishnav ul {position:absolute;left:-999em;height:auto;	width:151px;font-weight:normal;margin:0;line-height:1;	border:0;border-top:1px solid #666666;	}#suckerfishnav li li {	width:149px;border-bottom:1px solid #666666;border-left:1px solid #666666;border-right:1px solid #666666;font-weight:bold;font-family:verdana,sans-serif;}#suckerfishnav li li a {padding:4px 10px;	width:130px;font-size:12px;color:#dddddd;}#suckerfishnav li ul ul {margin:-21px 0 0 150px;}#suckerfishnav li li:hover {background:#1F3E9F;}#suckerfishnav li ul li:hover a, #suckerfishnav li ul li li:hover a, #suckerfishnav li ul li li li:hover a, #suckerfishnav li ul li li li:hover a  {color:#dddddd;}#suckerfishnav li:hover a, #suckerfishnav li.sfhover a {color:#dddddd;}#suckerfishnav li:hover li a, #suckerfishnav li li:hover li a, #suckerfishnav li li li:hover li a, #suckerfishnav li li li li:hover li a {color:#dddddd;}#suckerfishnav li:hover ul ul, #suckerfishnav li:hover ul ul ul, #suckerfishnav li:hover ul ul ul ul, #suckerfishnav li.sfhover ul ul, #suckerfishnav li.sfhover ul ul ul, #suckerfishnav li.sfhover ul ul ul ul  {left:-999em;}#suckerfishnav li:hover ul, #suckerfishnav li li:hover ul, #suckerfishnav li li li:hover ul, #suckerfishnav li li li li:hover ul, #suckerfishnav li.sfhover ul, #suckerfishnav li li.sfhover ul, #suckerfishnav li li li.sfhover ul, #suckerfishnav li li li li.sfhover ul  {left:auto;background:#444444;}#suckerfishnav li:hover, #suckerfishnav li.sfhover {background:#5E7AD3;}');
add_option('suckerfish_superfish', '');
add_option('suckerfish_superfish_speed', 'normal');
add_option('suckerfish_superfish_time', '800');
add_option('suckerfish_superfish_timeout', '100');
add_option('suckerfish_menuitem1', 'Home');
add_option('suckerfish_menuitem2', 'Pages Dropdown');
add_option('suckerfish_menuitem3', 'Categories Dropdown');
add_option('suckerfish_menuitem4', 'Archives (years) Dropdown');
add_option('suckerfish_menuitem5', 'Blogroll Dropdown');
add_option('suckerfish_menuitem6', 'None');
add_option('suckerfish_menuitem7', 'None');
add_option('suckerfish_menuitem8', 'None');
add_option('suckerfish_menuitem9', 'None');
add_option('suckerfish_menuitem10', 'None');
add_option('suckerfish_hometitle', 'Home');
add_option('suckerfish_pagestitle', 'Pages');
add_option('suckerfish_categoriestitle', 'Categories');
add_option('suckerfish_archivestitle', 'Archives');
add_option('suckerfish_blogrolltitle', 'Blogroll');
add_option('suckerfish_recentcommentstitle', 'Recent Comments');
add_option('suckerfish_keyboard', '');
add_option('suckerfish_disablecss', '');
add_option('suckerfish_inlinecss', '');
}

function suckerfish_options() {
		$suckerfish_menuitem1 = get_option('suckerfish_menuitem1');
		$suckerfish_menuitem2 = get_option('suckerfish_menuitem2');
		$suckerfish_menuitem3 = get_option('suckerfish_menuitem3');
		$suckerfish_menuitem4 = get_option('suckerfish_menuitem4');
		$suckerfish_menuitem5 = get_option('suckerfish_menuitem5');
		$suckerfish_menuitem6 = get_option('suckerfish_menuitem6');
		$suckerfish_menuitem7 = get_option('suckerfish_menuitem7');
		$suckerfish_menuitem8 = get_option('suckerfish_menuitem8');
		$suckerfish_menuitem9 = get_option('suckerfish_menuitem9');
		$suckerfish_menuitem10 = get_option('suckerfish_menuitem10');
?>

<style type="text/css">
#options h3 { margin-bottom: -10px; }
#options h2 { margin-top: 30px; }
#options label { width: 200px; float: left; margin-right: 25px; font-weight: bold; }
#options input { float: left; }
#options p { clear: both; }
.menuitems {float:left;width:300px;height:70px}
</style>

<script type="text/javascript" src="<?php bloginfo('wpurl'); ?>/wp-content/plugins/multi-level-navigation-plugin/animatedcollapse.js"></script>

<div class="wrap">
	<form method="post" action="options.php" id="options">
		<?php wp_nonce_field('update-options') ?>
		<h2>Multi-level Navigation Plugin</h2>
		<p>
			This plugin creates a dropdown, flyout or slider menu for your WordPress blog based on the
			<a href="http://www.htmldog.com/articles/suckerfish/ target="_blank">Son of Suckerfish technique</a>.
			If you have any comments, questions or suggestions about this plugin, please visit the
			<a href="http://pixopoint.com/forum/index.php?board=4.0">PixoPoint multi-level navigation forum</a>.
		</p>
		<p>
			To style your menu, please visit the <a href="http://pixopoint.com/suckerfish_css/">Multi-level Navigation CSS Generator</a>
			page to obtain your CSS and enter it below.
		</p>
		<div style="clear:both;padding-top:5px;"></div>

		<h3>Enter your CSS here</h3>
		<p>
			<textarea name="suckerfish_css" value="" cols="100%" rows="10" tabindex="4"><?php echo get_option('suckerfish_css'); ?></textarea>
		</p>

		<h2>Menu Contents</h2>
		<?php
			function contentsnone() {echo '<option>None</option><option>Home</option><option>Pages</option><option>Pages Dropdown</option><option>Categories</option><option>Categories Dropdown</option><option>Archives (months)</option><option>Archives (months) Dropdown</option><option>Archives (years)</option><option>Archives (years) Dropdown</option><option>Blogroll</option><option>Blogroll Dropdown</option><option>Custom 1</option><option>Recent Comments</option><option>Custom 2</option>';}
			function contentshome() {echo '<option>Home</option><option>None</option><option>Pages</option><option>Pages Dropdown</option><option>Categories</option><option>Categories Dropdown</option><option>Archives (months)</option><option>Archives (months) Dropdown</option><option>Archives (years)</option><option>Archives (years) Dropdown</option><option>Blogroll</option><option>Blogroll Dropdown</option><option>Custom 1</option><option>Recent Comments</option><option>Custom 2</option>';}
			function contentspages() {echo '<option>Pages</option><option>None</option><option>Home</option><option>Pages Dropdown</option><option>Categories</option><option>Categories Dropdown</option><option>Archives (months)</option><option>Archives (months) Dropdown</option><option>Archives (years)</option><option>Archives (years) Dropdown</option><option>Blogroll</option><option>Blogroll Dropdown</option><option>Custom 1</option><option>Recent Comments</option><option>Custom 2</option>';}
			function contentspagesdropdown() {echo '<option>Pages Dropdown</option><option>None</option><option>Home</option><option>Pages</option><option>Categories</option><option>Categories Dropdown</option><option>Archives (months)</option><option>Archives (months) Dropdown</option><option>Archives (years)</option><option>Archives (years) Dropdown</option><option>Blogroll</option><option>Blogroll Dropdown</option><option>Custom 1</option><option>Recent Comments</option><option>Custom 2</option>';}
			function contentscategories() {echo '<option>Categories</option><option>None</option><option>Home</option><option>Pages</option><option>Pages Dropdown</option><option>Categories Dropdown</option><option>Archives (months)</option><option>Archives (months) Dropdown</option><option>Archives (years)</option><option>Archives (years) Dropdown</option><option>Blogroll</option><option>Blogroll Dropdown</option><option>Custom 1</option><option>Recent Comments</option><option>Custom 2</option>';}
			function contentscategoriesdropdown() {echo '<option>Categories Dropdown</option><option>None</option><option>Home</option><option>Pages</option><option>Pages Dropdown</option><option>Categories</option><option>Archives (months)</option><option>Archives (months) Dropdown</option><option>Archives (years)</option><option>Archives (years) Dropdown</option><option>Blogroll</option><option>Blogroll Dropdown</option><option>Custom 1</option><option>Recent Comments</option><option>Custom 2</option>';}
			function contentsarchivesmonths() {echo '<option>Archives (months)</option><option>None</option><option>Home</option><option>Pages</option><option>Pages Dropdown</option><option>Categories</option><option>Categories Dropdown</option><option>Archives (years)</option><option>Archives (months) Dropdown</option><option>Archives (years) Dropdown</option><option>Blogroll</option><option>Blogroll Dropdown</option><option>Custom 1</option><option>Recent Comments</option><option>Custom 2</option>';}
			function contentsarchivesyears() {echo '<option>Archives (years)</option><option>None</option><option>Home</option><option>Pages</option><option>Pages Dropdown</option><option>Categories</option><option>Categories Dropdown</option><option>Archives (months)</option><option>Archives (months) Dropdown</option><option>Archives (years) Dropdown</option><option>Blogroll</option><option>Blogroll Dropdown</option<option>Custom</option>><option>Recent Comments</option><option>Custom 2</option>';}
			function contentsarchivesmonthsdropdown() {echo '<option>Archives (months) Dropdown</option><option>None</option><option>Home</option><option>Pages</option><option>Pages Dropdown</option><option>Categories</option><option>Categories Dropdown</option><option>Archives (months)</option><option>Archives (years)</option><option>Archives (years) Dropdown</option><option>Blogroll</option><option>Blogroll Dropdown</option><option>Custom 1</option><option>Recent Comments</option><option>Custom 2</option>';}
			function contentsarchivesyearsdropdown() {echo '<option>Archives (years) Dropdown</option><option>None</option><option>Home</option><option>Pages</option><option>Pages Dropdown</option><option>Categories</option><option>Categories Dropdown</option><option>Archives (months)</option><option>Archives (months) Dropdown</option><option>Archives (years)</option><option>Blogroll</option><option>Blogroll Dropdown</option><option>Custom 1</option><option>Recent Comments</option><option>Custom 2</option>';}
			function contentsblogroll() {echo '<option>Blogroll</option><option>None</option><option>Home</option><option>Pages</option><option>Pages Dropdown</option><option>Categories</option><option>Categories Dropdown</option><option>Archives (months)</option><option>Archives (months) Dropdown</option><option>Archives (years)</option><option>Archives (years) Dropdown</option><option>Blogroll Dropdown</option><option>Custom 1</option><option>Recent Comments</option><option>Custom 2</option>';}
			function contentsblogrolldropdown() {echo '<option>Blogroll Dropdown</option><option>None</option><option>Home</option><option>Pages</option><option>Pages Dropdown</option><option>Categories</option><option>Categories Dropdown</option><option>Archives (months)</option><option>Archives (months) Dropdown</option><option>Archives (years)</option><option>Archives (years) Dropdown</option><option>Blogroll</option><option>Custom 1</option><option>Recent Comments</option><option>Custom 2</option>';}
			function contentscustom() {echo '<option>Custom 1</option><option>Home</option><option>Pages</option><option>Pages Dropdown</option><option>Categories</option><option>Categories Dropdown</option><option>Archives (months)</option><option>Archives (months) Dropdown</option><option>Archives (years)</option><option>Archives (years) Dropdown</option><option>Blogroll</option><option>Blogroll Dropdown</option><option>None</option><option>Recent Comments</option><option>Custom 2</option>';}
			function contentscustom2() {echo '<option>Custom 2</option><option>Custom 1</option><option>Home</option><option>Pages</option><option>Pages Dropdown</option><option>Categories</option><option>Categories Dropdown</option><option>Archives (months)</option><option>Archives (months) Dropdown</option><option>Archives (years)</option><option>Archives (years) Dropdown</option><option>Blogroll</option><option>Blogroll Dropdown</option><option>None</option><option>Recent Comments</option>';}
			function contentsrecentcomments() {echo '<option>Recent Comments</option><option>Custom 1</option><option>Home</option><option>Pages</option><option>Pages Dropdown</option><option>Categories</option><option>Categories Dropdown</option><option>Archives (months)</option><option>Archives (months) Dropdown</option><option>Archives (years)</option><option>Archives (years) Dropdown</option><option>Blogroll</option><option>Blogroll Dropdown</option><option>None</option><option>Custom 1</option><option>Custom 2</option>';}
		?>
		<p>
			<div class="menuitems">
				<label>Menu Item #1</label>
				<select name="suckerfish_menuitem1">
					<?php switch ($suckerfish_menuitem1){	case "None":contentsnone();break;	case "":contentsnone();break;	case "Pages":contentspages();break;	case "Pages Dropdown":contentspagesdropdown();break;	case "Categories":contentscategories();break;	case "Categories Dropdown":contentscategoriesdropdown();break;	case "Home":contentshome();break;	case "Blogroll":contentsblogroll();break;	case "Blogroll Dropdown":contentsblogrolldropdown();	break;	case "Archives (months)":contentsarchivesmonths();break;	case "Archives (years)":contentsarchivesyears();break;	case "Archives (months) Dropdown":contentsarchivesmonthsdropdown();break;	case "Archives (years) Dropdown":contentsarchivesyearsdropdown();break;	case "Recent Comments":contentsrecentcomments();break;	case "Custom 1":contentscustom();break; case "Custom 2":contentscustom2();break; } ?>
				</select>
			</div>
			<div class="menuitems">
				<label>Menu Item #2</label>
				<select name="suckerfish_menuitem2">
					<?php switch ($suckerfish_menuitem2){	case "None":contentsnone();break;	case "":contentsnone();break;	case "Pages":contentspages();break;	case "Pages Dropdown":contentspagesdropdown();break;	case "Categories":contentscategories();break;	case "Categories Dropdown":contentscategoriesdropdown();break;	case "Home":contentshome();break;	case "Blogroll":contentsblogroll();break;	case "Blogroll Dropdown":contentsblogrolldropdown();	break;	case "Archives (months)":contentsarchivesmonths();break;	case "Archives (years)":contentsarchivesyears();break;	case "Archives (months) Dropdown":contentsarchivesmonthsdropdown();break;	case "Archives (years) Dropdown":contentsarchivesyearsdropdown();break;	case "Recent Comments":contentsrecentcomments();break;	case "Custom 1":contentscustom();break; case "Custom 2":contentscustom2();break; } ?>
				</select>
			</div>
			<div class="menuitems">
				<label>Menu Item #3</label>
				<select name="suckerfish_menuitem3">
					<?php switch ($suckerfish_menuitem3){	case "None":contentsnone();break;	case "":contentsnone();break;	case "Pages":contentspages();break;	case "Pages Dropdown":contentspagesdropdown();break;	case "Categories":contentscategories();break;	case "Categories Dropdown":contentscategoriesdropdown();break;	case "Home":contentshome();break;	case "Blogroll":contentsblogroll();break;	case "Blogroll Dropdown":contentsblogrolldropdown();	break;	case "Archives (months)":contentsarchivesmonths();break;	case "Archives (years)":contentsarchivesyears();break;	case "Archives (months) Dropdown":contentsarchivesmonthsdropdown();break;	case "Archives (years) Dropdown":contentsarchivesyearsdropdown();break;	case "Recent Comments":contentsrecentcomments();break;	case "Custom 1":contentscustom();break; case "Custom 2":contentscustom2();break; } ?>
				</select>
			</div>
			<div class="menuitems">
				<label>Menu Item #4</label>
				<select name="suckerfish_menuitem4">
					<?php switch ($suckerfish_menuitem4){	case "None":contentsnone();break;	case "":contentsnone();break;	case "Pages":contentspages();break;	case "Pages Dropdown":contentspagesdropdown();break;	case "Categories":contentscategories();break;	case "Categories Dropdown":contentscategoriesdropdown();break;	case "Home":contentshome();break;	case "Blogroll":contentsblogroll();break;	case "Blogroll Dropdown":contentsblogrolldropdown();	break;	case "Archives (months)":contentsarchivesmonths();break;	case "Archives (years)":contentsarchivesyears();break;	case "Archives (months) Dropdown":contentsarchivesmonthsdropdown();break;	case "Archives (years) Dropdown":contentsarchivesyearsdropdown();break;	case "Recent Comments":contentsrecentcomments();break;	case "Custom 1":contentscustom();break; case "Custom 2":contentscustom2();break; } ?>
				</select>
			</div>
			<div class="menuitems">
				<label>Menu Item #5</label>
				<select name="suckerfish_menuitem5">
					<?php switch ($suckerfish_menuitem5){	case "None":contentsnone();break;	case "":contentsnone();break;	case "Pages":contentspages();break;	case "Pages Dropdown":contentspagesdropdown();break;	case "Categories":contentscategories();break;	case "Categories Dropdown":contentscategoriesdropdown();break;	case "Home":contentshome();break;	case "Blogroll":contentsblogroll();break;	case "Blogroll Dropdown":contentsblogrolldropdown();	break;	case "Archives (months)":contentsarchivesmonths();break;	case "Archives (years)":contentsarchivesyears();break;	case "Archives (months) Dropdown":contentsarchivesmonthsdropdown();break;	case "Archives (years) Dropdown":contentsarchivesyearsdropdown();break;	case "Recent Comments":contentsrecentcomments();break;	case "Custom 1":contentscustom();break; case "Custom 2":contentscustom2();break; } ?>
				</select>
			</div>
			<div class="menuitems">
				<label>Menu Item #6</label>
				<select name="suckerfish_menuitem6">
					<?php switch ($suckerfish_menuitem6){	case "None":contentsnone();break;	case "":contentsnone();break;	case "Pages":contentspages();break;	case "Pages Dropdown":contentspagesdropdown();break;	case "Categories":contentscategories();break;	case "Categories Dropdown":contentscategoriesdropdown();break;	case "Home":contentshome();break;	case "Blogroll":contentsblogroll();break;	case "Blogroll Dropdown":contentsblogrolldropdown();	break;	case "Archives (months)":contentsarchivesmonths();break;	case "Archives (years)":contentsarchivesyears();break;	case "Archives (months) Dropdown":contentsarchivesmonthsdropdown();break;	case "Archives (years) Dropdown":contentsarchivesyearsdropdown();break;	case "Recent Comments":contentsrecentcomments();break;	case "Custom 1":contentscustom();break; case "Custom 2":contentscustom2();break; } ?>
				</select>
			</div>
			<div class="menuitems">
				<label>Menu Item #7</label>
				<select name="suckerfish_menuitem7">
					<?php switch ($suckerfish_menuitem7){	case "None":contentsnone();break;	case "":contentsnone();break;	case "Pages":contentspages();break;	case "Pages Dropdown":contentspagesdropdown();break;	case "Categories":contentscategories();break;	case "Categories Dropdown":contentscategoriesdropdown();break;	case "Home":contentshome();break;	case "Blogroll":contentsblogroll();break;	case "Blogroll Dropdown":contentsblogrolldropdown();	break;	case "Archives (months)":contentsarchivesmonths();break;	case "Archives (years)":contentsarchivesyears();break;	case "Archives (months) Dropdown":contentsarchivesmonthsdropdown();break;	case "Archives (years) Dropdown":contentsarchivesyearsdropdown();break;	case "Recent Comments":contentsrecentcomments();break;	case "Custom 1":contentscustom();break; case "Custom 2":contentscustom2();break; } ?>
				</select>
			</div>
			<div class="menuitems">
				<label>Menu Item #8</label>
				<select name="suckerfish_menuitem8">
					<?php switch ($suckerfish_menuitem8){	case "None":contentsnone();break;	case "":contentsnone();break;	case "Pages":contentspages();break;	case "Pages Dropdown":contentspagesdropdown();break;	case "Categories":contentscategories();break;	case "Categories Dropdown":contentscategoriesdropdown();break;	case "Home":contentshome();break;	case "Blogroll":contentsblogroll();break;	case "Blogroll Dropdown":contentsblogrolldropdown();	break;	case "Archives (months)":contentsarchivesmonths();break;	case "Archives (years)":contentsarchivesyears();break;	case "Archives (months) Dropdown":contentsarchivesmonthsdropdown();break;	case "Archives (years) Dropdown":contentsarchivesyearsdropdown();break;	case "Recent Comments":contentsrecentcomments();break;	case "Custom 1":contentscustom();break; case "Custom 2":contentscustom2();break; } ?>
				</select>
			</div>
			<div class="menuitems">
				<label>Menu Item #9</label>
				<select name="suckerfish_menuitem9">
					<?php switch ($suckerfish_menuitem9){	case "None":contentsnone();break;	case "":contentsnone();break;	case "Pages":contentspages();break;	case "Pages Dropdown":contentspagesdropdown();break;	case "Categories":contentscategories();break;	case "Categories Dropdown":contentscategoriesdropdown();break;	case "Home":contentshome();break;	case "Blogroll":contentsblogroll();break;	case "Blogroll Dropdown":contentsblogrolldropdown();	break;	case "Archives (months)":contentsarchivesmonths();break;	case "Archives (years)":contentsarchivesyears();break;	case "Archives (months) Dropdown":contentsarchivesmonthsdropdown();break;	case "Archives (years) Dropdown":contentsarchivesyearsdropdown();break;	case "Recent Comments":contentsrecentcomments();break;	case "Custom 1":contentscustom();break; case "Custom 2":contentscustom2();break; } ?>
				</select>
			</div>
			<div class="menuitems">
				<label>Menu Item #10</label>
				<select name="suckerfish_menuitem10">
					<?php switch ($suckerfish_menuitem10){	case "None":contentsnone();break;	case "":contentsnone();break;	case "Pages":contentspages();break;	case "Pages Dropdown":contentspagesdropdown();break;	case "Categories":contentscategories();break;	case "Categories Dropdown":contentscategoriesdropdown();break;	case "Home":contentshome();break;	case "Blogroll":contentsblogroll();break;	case "Blogroll Dropdown":contentsblogrolldropdown();	break;	case "Archives (months)":contentsarchivesmonths();break;	case "Archives (years)":contentsarchivesyears();break;	case "Archives (months) Dropdown":contentsarchivesmonthsdropdown();break;	case "Archives (years) Dropdown":contentsarchivesyearsdropdown();break;	case "Recent Comments":contentsrecentcomments();break;	case "Custom 1":contentscustom();break; case "Custom 2":contentscustom2();break; } ?>
				</select>
			</div>
		</p>

	<h2 style="clear:left">Advanced</h2>
  <div><strong><a class="temp1"  href="javascript:collapse2.slideit()">Click here to show advanced options &raquo;</a></strong></div>
	<div id="suckerfish_animatedbox" style="margin:0;padding:0 0 0 5px;background:#eee">

		<h3>Titles</h3>
		<p>
			To change the text displayed in the top level menu items for Pages, Categories etc. change the following menu items.
		</p>
  	<div class="menuitems">
			<p>
				<label>Home</label>
				<input type="text" name="suckerfish_hometitle" value="<?php echo get_option('suckerfish_hometitle'); ?>" />
			</p>
		</div>
  	<div class="menuitems">
			<p>
				<label>Pages</label>
				<input type="text" name="suckerfish_pagestitle" value="<?php echo get_option('suckerfish_pagestitle'); ?>" />
			</p>
		</div>
  	<div class="menuitems">
			<p>
				<label>Categories</label>
				<input type="text" name="suckerfish_categoriestitle" value="<?php echo get_option('suckerfish_categoriestitle'); ?>" />
			</p>
		</div>
  	<div class="menuitems">
			<p>
				<label>Archives</label>
				<input type="text" name="suckerfish_archivestitle" value="<?php echo get_option('suckerfish_archivestitle'); ?>" />
			</p>
		</div>
  	<div class="menuitems">
			<p>
				<label>Blogroll</label>
				<input type="text" name="suckerfish_blogrolltitle" value="<?php echo get_option('suckerfish_blogrolltitle'); ?>" />
			</p>
		</div>
  	<div class="menuitems">
			<p>
				<label>Recent Comments</label>
				<input type="text" name="suckerfish_recentcommentstitle" value="<?php echo get_option('suckerfish_recentcommentstitle'); ?>" />
			</p>
		</div>

		<div style="clear:both"></div>
		<h3>Exclude items</h3>
  	<div class="menuitems">
			<p>
				<label>Exclude Pages</label>
				<input type="text" name="suckerfish_excludepages" value="<?php echo get_option('suckerfish_excludepages'); ?>" />
			</p>
		</div>
  	<div class="menuitems">
			<p>
				<label>Exclude Categories</label>
				<input type="text" name="suckerfish_excludecategories" value="<?php echo get_option('suckerfish_excludecategories'); ?>" />
			</p>
		</div>
  	<div class="menuitems">
			<p><small>Pages and categories can be excluded by specifying their ID numbers, seperated by commas, ie: <em>14, 5</em></small></p>
		</div>
		<div style="clear:both"></div>

		<h3>Custom Menu</h3>
		<p>
			To use the "Custom" option available in the "Menu Contents" section above, add your code below.
		</p>
		<p>
			<strong>Note:</strong> The menu uses an unordered list to format the menu. You will need to
			know some HTML to use this option. The menu is already wrapped in UL tags.
		</p>
		<h4>Custom Code 1</h4>
		<p>
			<textarea name="suckerfish_custommenu" value="" cols="100%" rows="10" tabindex="4"><?php echo get_option('suckerfish_custommenu'); ?></textarea>
		</p>
		<h4>Custom Code 2</h4>
		<p>
			<textarea name="suckerfish_custommenu2" value="" cols="100%" rows="10" tabindex="4"><?php echo get_option('suckerfish_custommenu2'); ?></textarea>
		</p>

		<h3>Keyboard Accessibility</h3>
		<p>
			This option enables users to access your menu via the tab key on their keyboard rather than the mouse.
			This works in all modern browsers, plus IE 6 and 7. Choosing this option will add ~3 kB to each page download.
		</p>
		<p>
			<label>Enable keyboard accessible menu?</label>
			<?php
				if (get_option('suckerfish_keyboard') == 'on') {echo '<input type="checkbox" name="suckerfish_keyboard" checked="yes" />';}
				else {echo '<input type="checkbox" name="suckerfish_keyboard" />';}
			?>
			<br /><br />
		</p>

		<h3>Superfish</h3>
		<p>
			This option enhances the behaviour of the dropdown by creating an animated fade-in effect. Each page download will be ~65 kB
			larger due to the large Javascript files required for it to work. The menu will still be accessible before the script
			has fully loaded.
		</p>
		<p>
			<label>Enable SUPERFISH mode?</label>
			<?php
				if (get_option('suckerfish_superfish') == 'on') {echo '<input type="checkbox" name="suckerfish_superfish" checked="yes" />';}
				else {echo '<input type="checkbox" name="suckerfish_superfish" />';}
			?>
		</p>
		<p>&nbsp;</p>
		<p>
			<label>Speed</label>
			<select name="suckerfish_superfish_speed">
			<?php
				$suckerfish_superfish_speed = get_option('suckerfish_superfish_speed');
				switch ($suckerfish_superfish_speed){
					case "slow":echo '<option>slow</option><option>normal</option><option>fast</option>';break;
					case "normal":echo '<option>normal</option><option>slow</option><option>fast</option>';break;
					case "fast":echo '<option>fast</option><option>slow</option><option>normal</option>';break;
					case "":echo '<option>normal</option><option>slow</option><option>fast</option>';break;
					}
			?>
			</select>
		</p>
			<div class="menuitems">
		<p>
			<label>Mouseover delay (ms)</label>
			<input type="text" name="suckerfish_superfish_time" value="<?php echo get_option('suckerfish_superfish_time'); ?>" />
		</p>
			</div>
			<div class="menuitems">
		<p>
			<label>Mouseout delay (ms)</label>
			<input type="text" name="suckerfish_superfish_timeout" value="<?php echo get_option('suckerfish_superfish_timeout'); ?>" />
		</p>
			</div>
		<div style="clear:both"></div>

		<h3>Move CSS data</h3>
		<p>
			There is a known bug in this plugin which causes the CSS file (style.php) to not work on <strong>some</strong>
			web hosts. You can either choose to display the CSS in your HTML (easiest) or disable the
			plugin CSS file so that you can manually copy the CSS to your theme (usually the style.css file). If
			you find that you need to use this option to correct the bug in the style sheet, please leave a message in
			the PixoPoint multi-level navigation forums so we can take a look as we are actively working to fix this bug
			and need more examples of the problem in action (we can't replicate it).
		</p>
		<p>
			<label>Enable inline CSS</label>
			<?php
				if (get_option('suckerfish_inlinecss') == 'on') {echo '<input type="checkbox" name="suckerfish_inlinecss" checked="yes" />';}
				else {echo '<input type="checkbox" name="suckerfish_inlinecss" />';}
			?>
		</p>
		<p>
			<label>Disable Plugin CSS file</label>
			<?php
				if (get_option('suckerfish_disablecss') == 'on') {echo '<input type="checkbox" name="suckerfish_disablecss" checked="yes" />';}
				else {echo '<input type="checkbox" name="suckerfish_disablecss" />';}
			?>
		</p>


	</div><!-- END OF ANIMATED BOX -->
 	<script type="text/javascript">
	//Syntax: var uniquevar=new animatedcollapse("DIV_id", animatetime_milisec, enablepersist(true/fase), [initialstate] )
	var collapse2=new animatedcollapse("suckerfish_animatedbox", 800, true)
	</script>

		<input type="hidden" name="action" value="update" />
		<input type="hidden" name="page_options" value="suckerfish_css, suckerfish_superfish, suckerfish_superfish_speed, suckerfish_superfish_time, suckerfish_superfish_timeout, suckerfish_menuitem1, suckerfish_menuitem2, suckerfish_menuitem3, suckerfish_menuitem4, suckerfish_menuitem5, suckerfish_menuitem6, suckerfish_menuitem7, suckerfish_menuitem8, suckerfish_menuitem9, suckerfish_menuitem10, suckerfish_pagestitle, suckerfish_keyboard, suckerfish_excludepages, suckerfish_excludecategories, suckerfish_hometitle, suckerfish_pagestitle, suckerfish_categoriestitle, suckerfish_archivestitle, suckerfish_blogrolltitle, suckerfish_recentcommentstitle, suckerfish_disablecss, suckerfish_custommenu, suckerfish_custommenu2, suckerfish_inlinecss" />
		<div style="clear:both;padding-top:20px;"></div>
		<p class="submit"><input type="submit" name="Submit" value="<?php _e('Update Options') ?>" /></p>
		<div style="clear:both;padding-top:20px;"></div>

		<h2>Installation</h2>
		<p>Add the following code wherever you want the dropdown to appear in your theme (usually header.php)</p>
		<p><code>&lt;?php if (function_exists('suckerfish')) {suckerfish();} ?&gt;</code></p>
		<p>
			Note, that the functions which were implemented in previous version of this plugin still work. But you can not
			modify them via this admin panel. To change the menu contents, you must change your suckerfish1(), suckerfish2(),
			suckerfish3(), suckerfish4() or suckerfish5() function in your theme to suckerfish().
		</p>
		<h2>Help</h2>
		<p>
			For help with the plugin, please visit the <a href="http://pixopoint.com/multi-level-navigation/">Multi-level Navigation Plugin page</a> or
			the PixoPoint <a href="http://pixopoint.com/forum/index.php?board=4.0">multi-level navigation support board</a>.
		</p>

	</form>
</div>
<?php } ?>
