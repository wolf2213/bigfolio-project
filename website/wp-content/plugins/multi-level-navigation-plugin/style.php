<?php
require('../../../wp-blog-header.php');
header('Content-type: text/css');

echo '/*
  CSS generated via the Multi-level Navigation Plugin ... http://pixopoint.com/multi-level-navigation/

  If you would like a similar menu for your own site, then please try the PixoPoint Web Development
  CSS generator for creating flyout, dropdown and horizontal slider menus ... http://pixopoint.com/suckerfish_css/
*/

';
echo get_option('suckerfish_css');
 ?>
