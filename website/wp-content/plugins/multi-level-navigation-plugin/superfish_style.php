<?php
require('../../../wp-blog-header.php');
header('Content-Type: application/x-javascript');
?>

  $(document).ready(function(){
	$("#suckerfishnav")
	.superfish({

		delay		: <?php echo get_option('suckerfish_superfish_timeout'); ?>,
		speed		: "<?php echo get_option('suckerfish_superfish_speed'); ?>",
		animation : { opacity:"show", height:"show" }
	})
	.find(">li:has(ul)")
		.mouseover(function(){
			$("ul", this).bgIframe({opacity:false});
		})
		.find("a")
			.focus(function(){
				$("ul", $("#suckerfishnav>li:has(ul)")).bgIframe({opacity:false});
			});
});

