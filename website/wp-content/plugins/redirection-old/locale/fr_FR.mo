��    D      <  a   \      �     �     �     �     	          -     =  Z   E  4   �     �     �     �     �  !        0     7     G  $   W      |     �     �  	   �     �     �     �     �     �     �     �                    (     1     @     I     h     �  #   �     �     �     �     �     	     #	     ,	     ;	     A	     G	     S	     X	     _	  
   r	  	   }	  
   �	  T   �	     �	     �	     �	  
   
     
     &
     8
     R
     n
     q
     u
  �  x
     b  	   r     |     �      �     �     �  j   �  <   `     �     �     �     �  ?   �  	   *      4     U  +   s      �     �     �     �     �               (     1     9     =     \     o     w     ~     �     �  -   �  $   �     �  3     '   P     x     �     �     �  
   �     �     �     �     �                 
   )     4  	   B  ^   L     �     �     �     �  )   �          0     P     p     s     x            "   8   .   (      @   &          	   !             7      B      -   D              =              >      3             #      ?      5         C       <   *           2       4       ;       1          $   %          
   0           '   6          /                        )                     A             +   :   ,                         9    %s Redirections %s ago 404 Report for:  Add Redirection Add new redirection Add redirection Add www An empty URL means the source URL is not redirected when the user is logged in/logged out. An empty URL means the source URL is not redirected. Are you sure? Auto-generate URL Cancel Check for updates Create 301 when post slug changes Delete Delete All 404s Delete All Logs Globally redirect unknown 404 errors How many widgets would you like? IP Last Referrer Last User Log 404 errors Logged in URL Logged out URL Method Next No Not from referrer Open referrer Options Pass-through Per page Please wait... Previous Redirect based on login status Redirect based on referrer Redirect index.php/index.html Redirect to a random WordPress post Redirect to one of several URLs Redirected by Redirection Redirection 404 Log Redirection Log Referrer Referrer Regex Regex Reset Root domain Save Search Simple redirection Source URL Strip www Target URL This will delete all logged 404 errors.  Please be sure this is what you want to do. Type URL from referrer Update User Agent You have no 404 logs! You have no logs! You have no redirections. Your logs have been deleted at for go Project-Id-Version: Redirection 1.7.26
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-08-10 11:07+0800
PO-Revision-Date: 2008-03-08 15:46+0100
Last-Translator: Oncle Tom <thomas@oncle-tom.net>
Language-Team: Thomas Parisot aka Oncle Tom <thomas@oncle-tom.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
X-Poedit-Language: French
X-Poedit-Country: FRANCE
X-Poedit-SourceCharset: utf-8
X-Poedit-KeywordsList: __;_e;__ ;_e 
X-Poedit-Basepath: .
 %s redirections il y a %s Rapport d'erreurs 404 pour : Ajouter la redirection Ajouter une nouvelle redirection Ajouter une redirection Ajouter www Une URL vide signifie que l'URL source n'est pas redirigée lorsque l'utilisateur se connecte/déconnecte. Une URL vide signifie que l'URL source n'est pas redirigée. Êtes-vous sûr(e) ? URL auto-générée Annuler Vérifier les mises à jour Créer une redirection 301 lorsque l'identifiant de page change Supprimer Supprimer toutes les erreurs 404 Supprimer toutes les archives Redirection globale d'erreurs 404 inconnues Combien de widgets voulez-vous ? IP Dernier référant Dernier utilisateur Archiver les erreurs 404 URL de connexion URL de déconnexion Méthode Suivant Non N'est pas depuis un référant Référant ouvrant Options Neutre par page Veuillez patienter ... Précédent Redirection basée sur le statut de connexion Redirection basée sur le référant Rediriger index.php/index.html Redirection vers un article aléatoire de Wordpress Redirection vers une URL parmi d'autres Redirigé par Redirection Archives des redirections 404 Archives de redirection Référant Motif de référant Motif RÀZ Domaine racine Sauvegarder Chercher Redirection simple URL source Supprimer www URL cible Ceci va supprimer toutes les erreurs 404 archivées. Assurez-vous que c'est bel et bien voulu. Type URL depuis un référant Mises à jour Agent utilisateur Vous n'avez pas d'archives d'erreurs 404. Vous n'avez aucune archive ! Vous n'avez pas de redirection. Votre archivage a été effacé à pour go 