<?php
/* Plugin Name: Referly WordPress GETter
Plugin URI: http://bigfolio.com
Description: Gets a visit_id from a Referly link and stores it in a session
Author: Erik Dungan
Version: 1.0
Author URI: http://www.bigfolio.com/
*/
add_filter('query_vars', 'parameter_queryvars' );
function parameter_queryvars( $qvars ) {
    $qvars[] = 'visit_id';
    return $qvars;
}
?>