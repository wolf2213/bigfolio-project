<?php
/**
 * The samples template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package dazzling
 */

get_header(); ?>




    <!-- Masthead image -->
   <div class='page-header' id='about' style="height:400px;">
  <div class='container'>
    <h2>About Us</h2>
    <h3>10 years. Thousands of creatives. Millions of images. One focus.</h3>
  </div>
</div>
<div class='container page-body' style="min-height: 0px; padding-bottom: 0px;">
  <div class='page-section'>
    <h2 style="color:#F98D37;">Our Mission &amp; History</h2>
    <div class='row'>
      <div class='col-md-7'>
        <p class='standout'>
          At BIG Folio, our mission is simple:
          <em>
            Provide creative professionals with the tools and support
            to grow their business online.
          </em>
        </p>
        <p>
We believe that every great company has an enduring mission and we are proud to share our mission statement with the world. This mission is ambitious and at the core of what our customers deeply care about. We have a unique capability in harmonizing the needs of both individuals and organizations. This is in our DNA. We also deeply care about taking things global and making a difference in lives and organizations in all corners of the planet. We started this mission back in 2004 building Flash-based photography portfolios for friends and family and today we've had over 250,000 creative professionals around the world give Bigfolio a shot.
        </p>
        <p>
        Thank you from the bottom of our hearts for helping make our dream become reality.
        </p>
      </div>
      <div class='col-md-5'>
        <h3 class='columned'>Vitals</h3>
        <h4>
          <i class='fa fa-calendar'></i>
          Founded in 2004
        </h4>
        <p class='vital-detail'>
          in California's beautiful central coast.
        </p>
        <h4>
          <i class='fa fa-picture-o'></i>
          Over 10 Million Images
        </h4>
        <p class='vital-detail'>
          have been uploaded, hosted, and served by our servers, websites and tools.
        </p>
        <h4>
          <i class='fa fa-globe'></i>
          Over 250,000 Creatives
        </h4>
        <p class='vital-detail'>
          from around the globe use our websites and tools. From wedding photographers to interior designers.
        </p>
        
      </div>
    </div>
  </div>

</div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/wp-content/themes/dazzling/inc/js/new/js/new/jquery-1.10.1.min.js"><\/script>')</script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/backbone.js/1.1.0/backbone-min.js"></script>
    <script src="/wp-content/themes/dazzling/inc/js/new/js/new/bootstrap.min.js"></script>
    <script src="/wp-content/themes/dazzling/inc/js/new/js/new/scroll_view.js"></script>
    <script src="/wp-content/themes/dazzling/inc/js/new/js/new/main.js"></script>




<?php get_footer(); ?>