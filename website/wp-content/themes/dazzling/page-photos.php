<?php
/**
 * The samples template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package dazzling
 */

get_header(); ?>




    <!-- Masthead image -->
<div class='page-header' id='sell-photos' style="height:400px;">
  <div class='container'>
		<h2>Sell Your Photography Online</h2>
  </div>
</div>
<!-- CTA navigation (below masthead) -->
<!-- Rubix information and samples -->
<div class='page-body'>
  <div class='container'>
  		<h2 style="color:#F98D37;">Sell Prints and Digital Negatives for Weddings, Events, Portraits, Stock Photography
Fine Art, and Much More.</h2>
    <div class='row'>
      <div class='col-md-6'>
<h4 style="color:#F98D37;">A Secure E-Commerce Platform for Today's Photographer</h4>

        <p>
Bigfolio allows photographers of all types to sell prints, products and digital downloads. Secure, hassle-free payment processing and automatic fulfillment of digital file sales.
        </p>
<h4 style="color:#F98D37;">Galleries That Look as Good as Your Work</h4>
        <p>
Upload your logo and choose from a variety of themes for your galleries. Engage your 
clients with slideshows of your best images.
        </p>

<h4 style="color:#F98D37;">Flexibility and Protection</h4>
        <p>
Watermark your images, password-protect your galleries, even require registration. 
Product pricing and print fulfillment is up to you.
        </p>
     

      </div>
      <div class='col-md-6'>
        <div class='big-show-sample'>          
            <img class='img-responsive' src='/wp-content/themes/dazzling/img/sell-photos.png'>         
        </div>

      </div>
    </div>
  </div>
</div>



<BR /><BR />     
        



<?php get_footer(); ?>