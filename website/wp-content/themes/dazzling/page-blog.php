<?php
/**
Template Name: Blog
*/

get_header(); ?> 
<style = "text/css">


a.pagelink {
font-family:Arial;
color:#000000;
font-size:24px;
text-decoration:none;
}

a:hover.pagelink {
font-family:Arial;
color:#f5b037;
font-size:24px;
text-decoration:none;
}

a:active.pagelink {
font-family:Arial;
color:#f5b037;
font-size:24px;
text-decoration:none;
}


.pagination {
clear:both;
padding:20px 0;
position:relative;
font-size:24px;
line-height:13px;
}

.pagination span, .pagination a {
display:block;
float:left;
margin: 2px 2px 2px 0;
padding:6px 9px 5px 9px;
text-decoration:none;
width:auto;
color:#000;

}

.pagination a:hover{
color:#f5b037;

}

.pagination .current{
padding:6px 9px 5px 9px;
color:#f5b037;
}

</style>
	<div id="content" class="site-content container">
		<section id="primary" class="content-area col-sm-12 col-md-8 <?php echo of_get_option( 'site_layout' ); ?>">
        		 <h3 align="center" style="color:#000000; font-size:18px;">Start By Selecting A Photography Template You Love</h3>
                 	<h3 align="center" style="color:#000000; font-size:12px;">Templates Can Be Changed Anytime</h3>
			<main id="main" class="site-main" role="main">
            
           

<?php 
// Check if there are any posts to display
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array( 'post_type' => 'post', 'posts_per_page' => 5, 'paged' => $paged );
$wp_query = new WP_Query($args);

function kriesi_pagination($pages = '', $range = 2)
{  
     $showitems = ($range * 2)+1;  

     global $paged;
     if(empty($paged)) $paged = 1;

     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   

     if(1 != $pages)
     {
         echo "<div class='pagination'>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo;</a>";
         if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo;</a>";

         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<span class='current'>".$i."</span>":"<a href='".get_pagenum_link($i)."' class='inactive' >".$i."</a>";
             }
         }

         if ($paged < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($paged + 1)."'>&rsaquo;</a>";  
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>&raquo;</a>";
         echo "</div>\n";
     }
}

while ( have_posts() ) : the_post();  

?>

 <?php the_content(); ?>
 <br /><br />
<?php endwhile; ?>

<hr width="100%" style="border-bottom: 2px solid #878787;" />
<br /><br />

<!-- then the pagination links -->
<div align="right">
<!--<a href="/blog/page/1/" class="pagelink">1</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="/blog/page/2/" class="pagelink">2</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="/blog/page/3/" class="pagelink">3</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="/blog/page/4/" class="pagelink">4</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="/blog/page/5/" class="pagelink">5</a> -->

<?php

kriesi_pagination();

?>

<br /><br /><br /><br /><br /><br /><br /><br />
			</main><!-- #main -->
		</section><!-- #primary -->


<?php get_sidebar(); ?>
<?php get_footer(); ?>
