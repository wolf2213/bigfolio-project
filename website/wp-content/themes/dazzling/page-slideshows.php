<?php
/**
 * The samples template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package dazzling
 */

get_header(); ?>




    <!-- Masthead image -->
<div class='page-header' id='bigshow' style="height:400px;">
  <div class='container'>


  </div>
</div>
<!-- CTA navigation (below masthead) -->
<!-- Rubix information and samples -->
<div class='page-body' style="min-height: 0px; padding-bottom: 0px;">
  <div class='container'>
    <div class='row'>
      <div class='col-md-6'>
        <h3 class='bigshow-col-header' style="color:#F98D37;">
          Welcome To The New BIG Show
        </h3>
        <p class='about-bigshow'>
          With BIG Show you have the ability to craft unique slide shows, add custom music, and share with clients, visitors, or the entire world! The best part? BIG Show is a seamless, web based, slideshow generator that works without any downloads, installs, or updates.
        </p>
        <h4>
          Who should use Big Show?
        </h4>
        <p>
          Photographers who want to create stunning HTML5 slideshows and album previews to display their images to clients or potential customers.
        </p>
        <h4>
          What do I need to create a BIG Show?
        </h4>
        <p>
          BIG Show is a web based application that runs on any modern browser. If you're an existing BIG Folio customer simply sign in to the admin area for your site and select BIG Show from the main menu. All BIG Folio clients can create five shows for free with a limit of fifty images per show.  If you need more shows, you can subscribe at only $99 a year and create as many shows as you want with a limit of one hundred images per show.
        </p>


                <p>
          <a class='btn btn-primary' href='/photography-website-templates/'>Sign-up Now</a>
        </p>
      </div>
      <div class='col-md-6'>
        <h3 class='bigshow-col-header' style="color:#F98D37;" align="center">
         <a href="http://bigshowdemo.bigfolio.com//gallery/index.php?s=luna-photo-wedding" target="_blank">View BIG Show Sample - Click Here</a>
        </h3>
        <div class='big-show-sample'>
          <a href='http://bigshowdemo.bigfolio.com//gallery/index.php?s=luna-photo-wedding' target="_blank">
            <img class='img-responsive' src='/wp-content/themes/dazzling/images/big-show-luna.jpg'>
          </a>
        </div>

      </div>
    </div>
  </div>
</div>



<BR /><BR />     
        



<?php get_footer(); ?>