<?php
/**
* A Simple Category Template
*/

get_header(); ?> 
<style = "text/css">


a.pagelink {
font-family:Arial;
color:#000000;
font-size:24px;
text-decoration:none;
}

a:hover.pagelink {
font-family:Arial;
color:#f5b037;
font-size:24px;
text-decoration:none;
}

.pagination {
clear:both;
padding:20px 0;
position:relative;
font-size:24px;
line-height:13px;
}

.pagination span, .pagination a {
display:block;
float:left;
margin: -25px 2px 2px 0;
padding:6px 9px 5px 9px;
text-decoration:none;
width:auto;
color:#000;

}

.pagination a:hover{
color:#f5b037;

}

.pagination .current{
padding:6px 9px 5px 9px;
color:#f5b037;
}

</style>

	<div id="content" class="site-content container" style="padding-top: 100px;">
    
<div id="secondary" class="widget-area col-sm-12 col-md-4" role="complementary">


<BR /><BR />


        <div class="widget tabbed">
            <div class="tabs-wrapper">
                <ul class="nav nav-tabs">
                      <li><a href="#categories" data-toggle="tab">Categories</a></li>
                      <li class="active"><a href="/photography-website-templates/">Most Popular</a></li>
                      <li><a href="/new/">New</a></li>
                </ul>

            <div class="tab-content">
                <ul id="categories" class="tab-pane active">

 <li>
      <a href="/wedding-photographer-website-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Wedding Photography">
          <img src="/wp-content/themes/dazzling/images/icon-wedding.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/wedding-photographer-website-templates/" rel="bookmark" title="Wedding Photography">Wedding Photography</a>
                    </div>
</li>

<li>
      <a href="/children-photography-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Baby/Kid Photography">
          <img src="/wp-content/themes/dazzling/images/icon-kid.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/children-photography-templates/" rel="bookmark" title="Baby/Kid Photography">Baby/Kid Photography</a>
                    </div>
</li>

<li>
      <a href="/fashion-photography-themes/" class="tab-thumb thumbnail" rel="bookmark" title="Fashion Photography">
          <img src="/wp-content/themes/dazzling/images/icon-fashion.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/fashion-photography-themes/" rel="bookmark" title="Fashion Photography">Fashion Photography</a>
                    </div>
</li>               
                
                

<li>
      <a href="/pet-photography-themes/" class="tab-thumb thumbnail" rel="bookmark" title="Pet Photography">
          <img src="/wp-content/themes/dazzling/images/icon-pet.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/pet-photography-themes/" rel="bookmark" title="Pet Photography">Pet Photography</a>
                    </div>
</li>


<li>
      <a href="/professional-photography-website-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Professional Photography">
          <img src="/wp-content/themes/dazzling/images/icon-professional.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/professional-photography-website-templates/" rel="bookmark" title="Professional Photography">Professional Photography</a>
                    </div>
</li>


<li>
      <a href="/event-photography-website-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Event Photography">
          <img src="/wp-content/themes/dazzling/images/icon-event.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/event-photography-website-templates/" rel="bookmark" title="Event Photography">Event Photography</a>
                    </div>
</li>


<li>
      <a href="/model-photography-themes/" class="tab-thumb thumbnail" rel="bookmark" title="Model Photography">
          <img src="/wp-content/themes/dazzling/images/icon-model.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/model-photography-themes/" rel="bookmark" title="Model Photography">Model Photography</a>
                    </div>
</li>


<li>
      <a href="/photographer-portfolio-template/" class="tab-thumb thumbnail" rel="bookmark" title="Portfolio Photography">
          <img src="/wp-content/themes/dazzling/images/icon-portfolio.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/photographer-portfolio-template/" rel="bookmark" title="Portfolio Photography">Portfolio Photography</a>
                    </div>
</li>


<li>
      <a href="/outdoor-photography-portfolio-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Outdoor Photography">
          <img src="/wp-content/themes/dazzling/images/icon-outdoor.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/outdoor-photography-portfolio-templates/" rel="bookmark" title="Outdoor Photography">Outdoor Photography</a>
                    </div>
</li>


<li>
      <a href="/general-photography-themes/" class="tab-thumb thumbnail" rel="bookmark" title="General Photography">
          <img src="/wp-content/themes/dazzling/images/icon-general.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/general-photography-themes/" rel="bookmark" title="General Photography">General Photography</a>
                    </div>
</li>
                    
                </ul>
                
                <ul id="new" class="tab-pane">

                                
 <li>
      <a href="/wedding-photographer-website-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Wedding Photography">
          <img src="/wp-content/themes/dazzling/images/icon-wedding.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/wedding-photographer-website-templates/" rel="bookmark" title="Wedding Photography">Wedding Photography</a>
                    </div>
</li>

<li>
      <a href="/children-photography-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Baby/Kid Photography">
          <img src="/wp-content/themes/dazzling/images/icon-kid.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/children-photography-templates/" rel="bookmark" title="Baby/Kid Photography">Baby/Kid Photography</a>
                    </div>
</li>

<li>
      <a href="/fashion-photography-themes/" class="tab-thumb thumbnail" rel="bookmark" title="Fashion Photography">
          <img src="/wp-content/themes/dazzling/images/icon-fashion.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/fashion-photography-themes/" rel="bookmark" title="Fashion Photography">Fashion Photography</a>
                    </div>
</li>               
                
                

<li>
      <a href="/pet-photography-themes/" class="tab-thumb thumbnail" rel="bookmark" title="Pet Photography">
          <img src="/wp-content/themes/dazzling/images/icon-pet.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/pet-photography-themes/" rel="bookmark" title="Pet Photography">Pet Photography</a>
                    </div>
</li>


<li>
      <a href="/professional-photography-website-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Professional Photography">
          <img src="/wp-content/themes/dazzling/images/icon-professional.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/professional-photography-website-templates/" rel="bookmark" title="Professional Photography">Professional Photography</a>
                    </div>
</li>


<li>
      <a href="/event-photography-website-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Event Photography">
          <img src="/wp-content/themes/dazzling/images/icon-event.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/event-photography-website-templates/" rel="bookmark" title="Event Photography">Event Photography</a>
                    </div>
</li>


<li>
      <a href="/model-photography-themes/" class="tab-thumb thumbnail" rel="bookmark" title="Model Photography">
          <img src="/wp-content/themes/dazzling/images/icon-model.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/model-photography-themes/" rel="bookmark" title="Model Photography">Model Photography</a>
                    </div>
</li>


<li>
      <a href="/photographer-portfolio-template/" class="tab-thumb thumbnail" rel="bookmark" title="Portfolio Photography">
          <img src="/wp-content/themes/dazzling/images/icon-portfolio.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/photographer-portfolio-template/" rel="bookmark" title="Portfolio Photography">Portfolio Photography</a>
                    </div>
</li>


<li>
      <a href="/outdoor-photography-portfolio-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Outdoor Photography">
          <img src="/wp-content/themes/dazzling/images/icon-outdoor.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/outdoor-photography-portfolio-templates/" rel="bookmark" title="Outdoor Photography">Outdoor Photography</a>
                    </div>
</li>


<li>
      <a href="/general-photography-themes/" class="tab-thumb thumbnail" rel="bookmark" title="General Photography">
          <img src="/wp-content/themes/dazzling/images/icon-general.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/general-photography-themes/" rel="bookmark" title="General Photography">General Photography</a>
                    </div>
</li>
                    
                </ul>
                
                <ul id="popular" class="tab-pane">

                    
 <li>
      <a href="/wedding-photographer-website-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Wedding Photography">
          <img src="/wp-content/themes/dazzling/images/icon-wedding.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/wedding-photographer-website-templates/" rel="bookmark" title="Wedding Photography">Wedding Photography</a>
                    </div>
</li>

<li>
      <a href="/children-photography-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Baby/Kid Photography">
          <img src="/wp-content/themes/dazzling/images/icon-kid.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/children-photography-templates/" rel="bookmark" title="Baby/Kid Photography">Baby/Kid Photography</a>
                    </div>
</li>

<li>
      <a href="/fashion-photography-themes/" class="tab-thumb thumbnail" rel="bookmark" title="Fashion Photography">
          <img src="/wp-content/themes/dazzling/images/icon-fashion.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/fashion-photography-themes/" rel="bookmark" title="Fashion Photography">Fashion Photography</a>
                    </div>
</li>               
                
                

<li>
      <a href="/pet-photography-themes/" class="tab-thumb thumbnail" rel="bookmark" title="Pet Photography">
          <img src="/wp-content/themes/dazzling/images/icon-pet.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/pet-photography-themes/" rel="bookmark" title="Pet Photography">Pet Photography</a>
                    </div>
</li>


<li>
      <a href="/professional-photography-website-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Professional Photography">
          <img src="/wp-content/themes/dazzling/images/icon-professional.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/professional-photography-website-templates/" rel="bookmark" title="Professional Photography">Professional Photography</a>
                    </div>
</li>


<li>
      <a href="/event-photography-website-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Event Photography">
          <img src="/wp-content/themes/dazzling/images/icon-event.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/event-photography-website-templates/" rel="bookmark" title="Event Photography">Event Photography</a>
                    </div>
</li>


<li>
      <a href="/model-photography-themes/" class="tab-thumb thumbnail" rel="bookmark" title="Model Photography">
          <img src="/wp-content/themes/dazzling/images/icon-model.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/model-photography-themes/" rel="bookmark" title="Model Photography">Model Photography</a>
                    </div>
</li>


<li>
      <a href="/photographer-portfolio-template/" class="tab-thumb thumbnail" rel="bookmark" title="Portfolio Photography">
          <img src="/wp-content/themes/dazzling/images/icon-portfolio.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/photographer-portfolio-template/" rel="bookmark" title="Portfolio Photography">Portfolio Photography</a>
                    </div>
</li>


<li>
      <a href="/outdoor-photography-portfolio-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Outdoor Photography">
          <img src="/wp-content/themes/dazzling/images/icon-outdoor.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/outdoor-photography-portfolio-templates/" rel="bookmark" title="Outdoor Photography">Outdoor Photography</a>
                    </div>
</li>


<li>
      <a href="/general-photography-themes/" class="tab-thumb thumbnail" rel="bookmark" title="General Photography">
          <img src="/wp-content/themes/dazzling/images/icon-general.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/general-photography-themes/" rel="bookmark" title="General Photography">General Photography</a>
                    </div>
</li>
                    
                </ul>
                
                <ul id="new" class="tab-pane">

                                
 <li>
      <a href="/wedding-photographer-website-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Wedding Photography">
          <img src="/wp-content/themes/dazzling/images/icon-wedding.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/wedding-photographer-website-templates/" rel="bookmark" title="Wedding Photography">Wedding Photography</a>
                    </div>
</li>

<li>
      <a href="/children-photography-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Baby/Kid Photography">
          <img src="/wp-content/themes/dazzling/images/icon-kid.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/children-photography-templates/" rel="bookmark" title="Baby/Kid Photography">Baby/Kid Photography</a>
                    </div>
</li>

<li>
      <a href="/fashion-photography-themes/" class="tab-thumb thumbnail" rel="bookmark" title="Fashion Photography">
          <img src="/wp-content/themes/dazzling/images/icon-fashion.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/fashion-photography-themes/" rel="bookmark" title="Fashion Photography">Fashion Photography</a>
                    </div>
</li>               
                
                

<li>
      <a href="/pet-photography-themes/" class="tab-thumb thumbnail" rel="bookmark" title="Pet Photography">
          <img src="/wp-content/themes/dazzling/images/icon-pet.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/pet-photography-themes/" rel="bookmark" title="Pet Photography">Pet Photography</a>
                    </div>
</li>


<li>
      <a href="/professional-photography-website-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Professional Photography">
          <img src="/wp-content/themes/dazzling/images/icon-professional.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/professional-photography-website-templates/" rel="bookmark" title="Professional Photography">Professional Photography</a>
                    </div>
</li>


<li>
      <a href="/event-photography-website-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Event Photography">
          <img src="/wp-content/themes/dazzling/images/icon-event.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/event-photography-website-templates/" rel="bookmark" title="Event Photography">Event Photography</a>
                    </div>
</li>


<li>
      <a href="/model-photography-themes/" class="tab-thumb thumbnail" rel="bookmark" title="Model Photography">
          <img src="/wp-content/themes/dazzling/images/icon-model.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/model-photography-themes/" rel="bookmark" title="Model Photography">Model Photography</a>
                    </div>
</li>


<li>
      <a href="/photographer-portfolio-template/" class="tab-thumb thumbnail" rel="bookmark" title="Portfolio Photography">
          <img src="/wp-content/themes/dazzling/images/icon-portfolio.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/photographer-portfolio-template/" rel="bookmark" title="Portfolio Photography">Portfolio Photography</a>
                    </div>
</li>


<li>
      <a href="/outdoor-photography-portfolio-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Outdoor Photography">
          <img src="/wp-content/themes/dazzling/images/icon-outdoor.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/outdoor-photography-portfolio-templates/" rel="bookmark" title="Outdoor Photography">Outdoor Photography</a>
                    </div>
</li>


<li>
      <a href="/general-photography-themes/" class="tab-thumb thumbnail" rel="bookmark" title="General Photography">
          <img src="/wp-content/themes/dazzling/images/icon-general.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/general-photography-themes/" rel="bookmark" title="General Photography">General Photography</a>
                    </div>
</li>
                    
                </ul>
                
                <ul id="new" class="tab-pane">

                                
 <li>
      <a href="/wedding-photographer-website-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Wedding Photography">
          <img src="/wp-content/themes/dazzling/images/icon-wedding.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/wedding-photographer-website-templates/" rel="bookmark" title="Wedding Photography">Wedding Photography</a>
                    </div>
</li>

<li>
      <a href="/children-photography-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Baby/Kid Photography">
          <img src="/wp-content/themes/dazzling/images/icon-kid.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/children-photography-templates/" rel="bookmark" title="Baby/Kid Photography">Baby/Kid Photography</a>
                    </div>
</li>

<li>
      <a href="/fashion-photography-themes/" class="tab-thumb thumbnail" rel="bookmark" title="Fashion Photography">
          <img src="/wp-content/themes/dazzling/images/icon-fashion.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/fashion-photography-themes/" rel="bookmark" title="Fashion Photography">Fashion Photography</a>
                    </div>
</li>               
                
                

<li>
      <a href="/pet-photography-themes/" class="tab-thumb thumbnail" rel="bookmark" title="Pet Photography">
          <img src="/wp-content/themes/dazzling/images/icon-pet.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/pet-photography-themes/" rel="bookmark" title="Pet Photography">Pet Photography</a>
                    </div>
</li>


<li>
      <a href="/professional-photography-website-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Professional Photography">
          <img src="/wp-content/themes/dazzling/images/icon-professional.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/professional-photography-website-templates/" rel="bookmark" title="Professional Photography">Professional Photography</a>
                    </div>
</li>


<li>
      <a href="/event-photography-website-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Event Photography">
          <img src="/wp-content/themes/dazzling/images/icon-event.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/event-photography-website-templates/" rel="bookmark" title="Event Photography">Event Photography</a>
                    </div>
</li>


<li>
      <a href="/model-photography-themes/" class="tab-thumb thumbnail" rel="bookmark" title="Model Photography">
          <img src="/wp-content/themes/dazzling/images/icon-model.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/model-photography-themes/" rel="bookmark" title="Model Photography">Model Photography</a>
                    </div>
</li>


<li>
      <a href="/photographer-portfolio-template/" class="tab-thumb thumbnail" rel="bookmark" title="Portfolio Photography">
          <img src="/wp-content/themes/dazzling/images/icon-portfolio.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/photographer-portfolio-template/" rel="bookmark" title="Portfolio Photography">Portfolio Photography</a>
                    </div>
</li>


<li>
      <a href="/outdoor-photography-portfolio-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Outdoor Photography">
          <img src="/wp-content/themes/dazzling/images/icon-outdoor.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/outdoor-photography-portfolio-templates/" rel="bookmark" title="Outdoor Photography">Outdoor Photography</a>
                    </div>
</li>


<li>
      <a href="/general-photography-themes/" class="tab-thumb thumbnail" rel="bookmark" title="General Photography">
          <img src="/wp-content/themes/dazzling/images/icon-general.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/general-photography-themes/" rel="bookmark" title="General Photography">General Photography</a>
                    </div>
</li>
                    
                </ul>
                
                <ul id="new" class="tab-pane">

                                
 <li>
      <a href="/wedding-photographer-website-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Wedding Photography">
          <img src="/wp-content/themes/dazzling/images/icon-wedding.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/wedding-photographer-website-templates/" rel="bookmark" title="Wedding Photography">Wedding Photography</a>
                    </div>
</li>

<li>
      <a href="/children-photography-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Baby/Kid Photography">
          <img src="/wp-content/themes/dazzling/images/icon-kid.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/children-photography-templates/" rel="bookmark" title="Baby/Kid Photography">Baby/Kid Photography</a>
                    </div>
</li>

<li>
      <a href="/fashion-photography-themes/" class="tab-thumb thumbnail" rel="bookmark" title="Fashion Photography">
          <img src="/wp-content/themes/dazzling/images/icon-fashion.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/fashion-photography-themes/" rel="bookmark" title="Fashion Photography">Fashion Photography</a>
                    </div>
</li>               
                
                

<li>
      <a href="/pet-photography-themes/" class="tab-thumb thumbnail" rel="bookmark" title="Pet Photography">
          <img src="/wp-content/themes/dazzling/images/icon-pet.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/pet-photography-themes/" rel="bookmark" title="Pet Photography">Pet Photography</a>
                    </div>
</li>


<li>
      <a href="/professional-photography-website-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Professional Photography">
          <img src="/wp-content/themes/dazzling/images/icon-professional.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/professional-photography-website-templates/" rel="bookmark" title="Professional Photography">Professional Photography</a>
                    </div>
</li>


<li>
      <a href="/event-photography-website-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Event Photography">
          <img src="/wp-content/themes/dazzling/images/icon-event.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/event-photography-website-templates/" rel="bookmark" title="Event Photography">Event Photography</a>
                    </div>
</li>


<li>
      <a href="/model-photography-themes/" class="tab-thumb thumbnail" rel="bookmark" title="Model Photography">
          <img src="/wp-content/themes/dazzling/images/icon-model.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/model-photography-themes/" rel="bookmark" title="Model Photography">Model Photography</a>
                    </div>
</li>


<li>
      <a href="/photographer-portfolio-template/" class="tab-thumb thumbnail" rel="bookmark" title="Portfolio Photography">
          <img src="/wp-content/themes/dazzling/images/icon-portfolio.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/photographer-portfolio-template/" rel="bookmark" title="Portfolio Photography">Portfolio Photography</a>
                    </div>
</li>


<li>
      <a href="/outdoor-photography-portfolio-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Outdoor Photography">
          <img src="/wp-content/themes/dazzling/images/icon-outdoor.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/outdoor-photography-portfolio-templates/" rel="bookmark" title="Outdoor Photography">Outdoor Photography</a>
                    </div>
</li>


<li>
      <a href="/general-photography-themes/" class="tab-thumb thumbnail" rel="bookmark" title="General Photography">
          <img src="/wp-content/themes/dazzling/images/icon-general.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/general-photography-themes/" rel="bookmark" title="General Photography">General Photography</a>
                    </div>
</li>    
                </ul>
                
                </div>
            </div>
        </div>

	</div><!-- #secondary -->


		<div id="primary" class="content-area col-sm-12 col-md-8 <?php echo of_get_option( 'site_layout' ); ?>">
			<main id="main" class="site-main" role="main">
            
 				        		 <h3 align="center" style="color:#F98D37; font-size:18px; margin-bottom: 50px;">Start By Selecting A Photography Template You Love</h3>
                 	          

<?php 
// Check if there are any posts to display
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array( 'cat' => 8, 'posts_per_page' => 5, 'paged' => $paged );
$wp_query = new WP_Query($args);

function kriesi_pagination($pages = '', $range = 2)
{  
     $showitems = ($range * 2)+1;  

     global $paged;
     if(empty($paged)) $paged = 1;

     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   

     if(1 != $pages)
     {
         echo "<div class='pagination'>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo;</a>";
         if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo;</a>";

         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<span class='current'>".$i."</span>":"<a href='".get_pagenum_link($i)."' class='inactive' >".$i."</a>";
             }
         }

         if ($paged < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($paged + 1)."'>&rsaquo;</a>";  
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>&raquo;</a>";
         echo "</div>\n";
     }
}


// The Loop
while ( have_posts() ) : the_post(); ?>



<div class="entry-content">
<?php the_content(); ?>
 <br />
 </div>
 
 <hr  width="100%" style="border-bottom: 2px solid #878787;" />
<?php endwhile; ?>



<!-- then the pagination links -->
<div align="right">
<?php

kriesi_pagination();

?>
<br /><br />


<br /><br /><br /><br /><br />

</div>

			</main><!-- #main -->
		</div><!-- #primary -->



<?php get_footer(); ?>
