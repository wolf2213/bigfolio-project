<?php
/**
 * The samples template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package dazzling
 */

get_header(); ?>




    <!-- Masthead image -->
   <div class='page-header' id='white-glove' style="height:400px;">
  <div class='container'>
    <h2>White Glove Service</h2>
    <h3>Keep Shooting. We've Got You Covered.</h3>
  </div>
</div>
<div class='page-body'>
  <div class='container'>
    <h2 style="color:#F98D37;">Let Us Build Your Next Portfolio</h2>
    <div class='row'>
      <div class='col-md-8'>
        <p>
          Too busy to build your new portfolio? Need a new site live for next week's bridal fair? Not a problem. Our white glove setup service will make sure your site is initially built and launched in a matter of days.
        </p>
        
        <p>
          Simply contact us before or after signing up
          and we'll invoice you for the service. Like all new customers, your new site will be setup within 1 business day. You'll then provide us with your images and logo (FTP, Dropbox or Email). Once we receive your images, we'll create galleries, add your logo, and copy page content from your old site. (details below)
        </p>
        <p>Provide us with your registrar information or update your name servers and we guarantee your new site will be live within 5 business days. Of course, once your site is live you can continue to make as many changes as you'd like using our comprehensive control panel.</p>
        <h4>Details</h4>
        <p>This services covers only 'standard' features of our sites. We will add images, logo, and page text. The purpose being to get the basic content up and launch your site asap.</p>
        <p>We will not copy custom contact forms and custom formatted pages.  We will not transfer files such as proofing folders, blogs, etc. from your old host.  We will not create/edit an html landing page. We will not do any SEO related work. In most cases, these particular items can be handled by you after the site is made live. **If required, any of the above items can be done by us, but it may incur a customization fee. For example: We offer SEO services here.</p>
      </div>
      <div class='col-md-4'>
        <p>
          <strong>Images</strong>
          <br>
          The images will need to be named and labeled correctly in each folder when added via FTP. Add up to 100 images.
        </p>
        <p>
          <strong>Copy</strong>
          <br>
          We will add up to 5 pages of text. When submitting page text simply email the text in an email.  Please make all page titles in CAPS so we can decipher.
        </p>
        <p>
          <strong>Logo</strong>
          <br>
          We would prefer to receive your logo as a transparent PNG or a vector file format, but it can be in .jpg format as well.
        </p>
        <h3 style="color:#F98D37;">Only $249</h3>
      </div>
    </div>
  </div>
</div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/wp-content/themes/dazzling/inc/js/new/js/new/jquery-1.10.1.min.js"><\/script>')</script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/backbone.js/1.1.0/backbone-min.js"></script>
    <script src="/wp-content/themes/dazzling/inc/js/new/js/new/bootstrap.min.js"></script>
    <script src="/wp-content/themes/dazzling/inc/js/new/js/new/scroll_view.js"></script>
    <script src="/wp-content/themes/dazzling/inc/js/new/js/new/main.js"></script>



<BR /><BR />     
        



<?php get_footer(); ?>