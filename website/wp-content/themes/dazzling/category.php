<?php
/**
* A Simple Category Template
*/

get_header(); ?> 
<style = "text/css">


a.pagelink {
font-family:Arial;
color:#000000;
font-size:24px;
text-decoration:none;
}

a:hover.pagelink {
font-family:Arial;
color:#f5b037;
font-size:24px;
text-decoration:none;
}

</style>

	<div id="content" class="site-content container">
		<section id="primary" class="content-area col-sm-12 col-md-8 <?php echo of_get_option( 'site_layout' ); ?>">
        		 <h3 align="center" style="color:#000000; font-size:18px;">Start By Selecting A Photography Template You Love</h3>
                 	<h3 align="center" style="color:#000000; font-size:12px;">Templates Can Be Changed Anytime</h3>
			<main id="main" class="site-main" role="main">
            
            

<?php 
// Check if there are any posts to display
//$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
//$args = array( 'post_type' => 'post', 'posts_per_page' => 5, 'paged' => $paged );
//$wp_query = new WP_Query($args);

if ( have_posts() ) : ?>

<header class="archive-header">



<?php
// Display optional category description
 if ( category_description() ) : ?>
<div class="archive-meta"><?php echo category_description(); ?></div>
<?php endif; ?>
</header>

<?php

// The Loop
while ( have_posts() ) : the_post(); ?>



<div class="entry">
<?php the_content(); ?>

<br /><br />
</div>

<?php endwhile; 

else: ?>
<p>Sorry, no posts matched your criteria.</p>


<?php endif; ?>

<hr width="100%" style="border-bottom: 2px solid #878787;" />
<br /><br />

<!-- then the pagination links -->
<div align="right">
<a href="/blog" class="pagelink">View All</a>

<br /><br /><br /><br /><br /><br /><br /><br />

			</main><!-- #main -->
		</section><!-- #primary -->


<?php get_sidebar(); ?>
<?php get_footer(); ?>
