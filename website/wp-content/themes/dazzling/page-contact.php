<?php
/**
 * The samples template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package dazzling
 */

get_header(); ?>




    <!-- Masthead image -->
     <div class='page-header' id='contact' style="height:400px;">
  <div class='container'>
    <h2>Contact Us</h2>
    <h3>Phone or Email. We're Here to Help.</h3>
  </div>
</div>
<div class='container page-body' style="min-height: 0px; padding-bottom: 0px;">
  <div class='page-section'>
    <h2 style="color:#F98D37;">How Can We Help?</h2>
    <div class='row'>
      <div class='col-md-4'>
        <h3 align="center">Product Support</h3>
        <p>
          If you need technical support with your website, NextProof store, or PROpx account,
          please submit a support ticket and we'll assist you as soon as possible.
        </p>

        <p align="center">
          <strong>Billing inquiries</strong>
        </p>
        <p>
          Customers requiring billing assistance should call
          (888) 236-9704 during normal business hours
          or email
          <a href='mailto:billing@bigfolio.com'>billing@bigfolio.com</a>
        </p>
         <p>
          <a class='btn btn-primary' href='http://support.bigfolio.com/anonymous_requests/new'>
            Submit a Support Ticket
          </a>
        </p>       
        
      </div>
      <div class='col-md-4'>
        <h3 align="center">Sales &amp; General Inquiries</h3>
        <p>
          If you have questions about or products and services or need assistance signing up,
          please email us at:
        </p>
        <p class='lead' align="center">
          <a href='mailto:info@bigfolio.com'>info@bigfolio.com</a>
        </p>
      </div>
      <div class='col-md-4'>
        <h3 align="center">Phone &amp; Address</h3>
        <p class='lead' align="center">
          (888) 236-9704
        </p>
        <p class='text-muted' align="center">
         Mon-Fri 9am-5pm EST
        </p>
        <hr>
        <address>
          <center><strong><font style="font-size:18px;">Bigfolio Inc.</font></strong></center>
          
          <center>8201 Peters Rd.</center> 
          
          <center>Suite #1000</center>
          
          <center>Plantation, FL 33324</center>
        </address>
      </div>
    </div>
 
      </div>
    </div>
  </div>
</div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/wp-content/themes/dazzling/inc/js/new/js/new/jquery-1.10.1.min.js"><\/script>')</script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/backbone.js/1.1.0/backbone-min.js"></script>
    <script src="/wp-content/themes/dazzling/inc/js/new/js/new/bootstrap.min.js"></script>
    <script src="/wp-content/themes/dazzling/inc/js/new/js/new/scroll_view.js"></script>
    <script src="/wp-content/themes/dazzling/inc/js/new/js/new/main.js"></script>







<?php get_footer(); ?>