<?php
/**
* Blog (Page).
*/

get_header(); ?> 

	<div id="content" class="site-content container">
		<section id="primary" class="content-area col-sm-12 col-md-8 <?php echo of_get_option( 'site_layout' ); ?>">
        		 <h3 align="center" style="color:#000000; font-size:18px;">Start By Selecting A Photography Template You Love</h3>
                 	<h3 align="center" style="color:#000000; font-size:12px;">Templates Can Be Changed Anytime</h3>
			<main id="main" class="site-main" role="main">
            
            

<?php 
// Check if there are any posts to display
if ( have_posts() ) : ?>

<header class="archive-header">



<?php
// Display optional category description
 if ( category_description() ) : ?>
<div class="archive-meta"><?php echo category_description(); ?></div>
<?php endif; ?>
</header>

<?php

// The Loop
while ( have_posts() ) : the_post(); ?>



<div class="entry">
<?php the_content(); ?>

<br /><br />
</div>

<?php endwhile; 

else: ?>
<p>Sorry, no posts matched your criteria.</p>


<?php endif; ?>

			</main><!-- #main -->
		</section><!-- #primary -->


<?php get_sidebar(); ?>
<?php get_footer(); ?>
