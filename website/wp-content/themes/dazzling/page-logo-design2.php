<?php
/**
 * The samples template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package dazzling
 */

get_header(); ?>

    <!-- Owl Carousel Assets -->
    <link rel="stylesheet" href="/wp-content/themes/dazzling/assets/themes/default/css/owl.carousel.css">
    <link rel="stylesheet" href="/wp-content/themes/dazzling/assets/themes/default/css/owl.theme.css">

<link rel="stylesheet" href="/wp-content/themes/dazzling/assets/themes/default/css/component.css"><!--[if !IE 8]><!--><!--[if !IE 9]><!-->
<link rel="stylesheet" href="/wp-content/themes/dazzling/assets/themes/default/css/effect2.css">
    <!--<![endif]--><!--<![endif]--> 
<link rel="stylesheet" href="/wp-content/themes/dazzling/assets/themes/default/css/effect/set1.css">

 <!--[if !IE 8]><!--><!--[if !IE 9]><!-->
    <style>
        .wow:first-child {visibility: hidden;}

        .fraction-slider:hover .prev,
        .fraction-slider:hover .next { display: none !important;}
    </style>
    <!--<![endif]--><!--<![endif]-->
    <!-- -->
    
    <style>
        .no-cssanimations .rw-wrapper .rw-sentence span:first-child{
            opacity: 1;
        }
    </style>
    <!--[if lt IE 9]>
    <style>
        body{ background: #fff; }
        .rw-wrapper{ display: none; }
        .rw-sentence-IE{ display: block;  }
    </style>
    <![endif]-->
    <!--   -->

    <link rel="stylesheet" href="/wp-content/themes/dazzling/assets/themes/default/css/fresco.css" />
    
  <!--   -->
    <link rel="stylesheet" href="/wp-content/themes/dazzling/assets/themes/default/css/component2.css" />

    <!-- hover effect -->
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/dazzling/assets/themes/default/css/style1.css" />
    <script src="/wp-content/themes/dazzling/assets/themes/default/js/modernizr.custom.97074.js"></script>

    <!-- -->
    <link rel="stylesheet" href="/wp-content/themes/dazzling/assets/themes/default/css/layout.css">

    <!-- Litebox -->
    <link rel="stylesheet" href="/wp-content/themes/dazzling/assets/themes/default/css/litebox.css">  


    <!-- Masthead image -->
    <div class='page-header' id='logos' style="height:400px;">
  <div class='container'>
    <h2>Logo Design Service</h2>
    <h3>Make Your Brand Stand Apart From The Rest</h3>
  </div>
</div>
<div class='page-body'>
  <div class='container'>
    <h2 style="color:#F98D37;">Your Business Needs a Strong Brand</h2>
    <p>
      A strong brand is essential to optimizing your business and online presence. Having recently launched our new Big Folio logo, we wanted to extend this top-quality service to our valued clients.
    </p>
    <p>
      Our award-winning designer has 20 years experience in branding, marketing and design. No matter what your vision, we can bring it to life.
    </p>
    <h3 style="color:#F98D37;">Logo Design Details</h3>
   
    <p>
      To get started on your new logo or if you have questions, please contact us at
      <a href='info@bigfolio.com'>info@bigfolio.com</a>
    </p>
    <h4>
      LEVEL 1 -- Basic Plan - $69
    </h4>
    <p>2 Design Concepts, 2 Designers, FREE Multiple File Format, FREE Multiple Color Option, Grey Scale Format, Unlimited Revisions, Delivery 2 Days, 100% Money Back Guarantee, 100% Satisfaction Guarantee, 100% Ownership Rights, $50 for 24 Hour Rush Delivery</p>
    <h4>
      LEVEL 2 -- Basic Plus - $89
    </h4>
    <p>4 Design Concepts, 2 Designers, Unlimited Revisions, FREE Multiple Color Option, FREE Multiple File Format, Stationery Concept, Business Card, Letterhead, Envelope, Delivery 2 Days, 100% Money Back Guarantee, 100% Satisfaction Guarantee, 100% Ownership Rights, $50 for 24 Hour Rush Delivery</p>
    <h4>
      LEVEL 3 -- UNLIMITED - $129  "MOST POPULAR"
    </h4>
    <p>Unlimited Design Concepts, 8 Designers, FREE Multiple File Format, FREE Multiple Color Option, Stationery Concept, Business Card, Letterhead, Envelope, Unlimited Revisions, Delivery 2 Days, 100% Money Back Guarantee, 100% Satisfaction Guarantee, 100% Ownership Rights, $50 for 24 Hour Rush Delivery</p>
    <p></p>
    <p></p>
    <BR />
    <h4 align="center">Logo Design Examples</h4>
  
 <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-1.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-1.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-2.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-2.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-3.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-3.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-4.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-4.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-5.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-5.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-6.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-6.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-7.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-7.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-8.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-8.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-9.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-9.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-10.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-10.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-11.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-11.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-12.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-12.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-13.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-13.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-14.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-14.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-15.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-15.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-16.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-16.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-17.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-17.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-18.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-18.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-19.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-19.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-20.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-20.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-21.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-21.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-22.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-22.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-23.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-23.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-24.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-24.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-25.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-25.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-26.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-26.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-27.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-27.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-28.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-28.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-29.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-29.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-30.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-30.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-31.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-31.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-32.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-32.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div> 
    
    
 </div>

 
        



<?php get_footer(); ?>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

<!-- Latest compiled and minified JavaScript -->

<!-- Effect -->
<script src="/wp-content/themes/dazzling/assets/themes/default/js/modernizr.js"></script>
<script>
    $(document).ready(function(){
        if (Modernizr.touch) {
            // show the close overlay button
            $(".close-overlay").removeClass("hidden");
            // handle the adding of hover class when clicked
            $(".img").click(function(e){
                if (!$(this).hasClass("hover")) {
                    $(this).addClass("hover");
                }
            });
            // handle the closing of the overlay
            $(".close-overlay").click(function(e){
                e.preventDefault();
                e.stopPropagation();
                if ($(this).closest(".img").hasClass("hover")) {
                    $(this).closest(".img").removeClass("hover");
                }
            });
        } else {
            // handle the mouseenter functionality
            $(".img").mouseenter(function(){
                $(this).addClass("hover");
            })
                // handle the mouseleave functionality
                    .mouseleave(function(){
                        $(this).removeClass("hover");
                    });
        }
    });
</script>

<script>
    $(function() {
        $("#sidePanel .myButton").click(function(e) {
            $("#sidePanel .myDiv").toggleClass( "open", 2000 );
        });

        $("#sidePanel .myButton").click(function(e) {
            $(".panel-container").toggleClass( "dark-bg", 2000 );
        });


        $("#sidePanel-2").hover(function(e) {
            $("#sidePanel-2 .myDiv").toggleClass( "open", 2000 );
        });

        $("#sidePanel-3").hover(function(e) {
            $("#sidePanel-3 .myDiv").toggleClass( "open", 2000 );
        });

        /*$("#sidePanel-2,#sidePanel-3").hover(function(e) {
         $(".panel-container").toggleClass( "dark-bg", 2000 );
         });*/
    });

</script>

<!-- Owl Carousel Assets -->
<script src="/wp-content/themes/dazzling/assets/themes/default/js/owl.carousel.js"></script>
<script>
    $(document).ready(function() {

        var owl = $("#owl-demo");

        owl.owlCarousel({

            items : 1, //10 items above 1000px browser width
            itemsDesktop : [1200,1], //5 items between 1000px and 901px
            itemsDesktopSmall : [900,1], // 3 items betweem 900px and 601px
            itemsTablet: [600,1], //2 items between 600 and 0;
            itemsMobile :[480,1] // itemsMobile disabled - inherit from itemsTablet option

        });

        // Custom Navigation Events
        $(".next").click(function(){
            owl.trigger('owl.next');
        })
        $(".prev").click(function(){
            owl.trigger('owl.prev');
        })
        $(".play")
        owl.trigger('owl.play',3500);

        $(".stop").click(function(){
            owl.trigger('owl.stop');
        })




        $("#owl-demo").owlCarousel({

            autoHeight : true,

        });
    });
</script>


<!-- -->
<!--[if !IE 8]><!--> <!--[if !IE 9]><!-->
<script src="/wp-content/themes/dazzling/assets/themes/default/js/effects.js"></script>
<script>
    wow = new WOW(
            {
                animateClass: 'animated',
                offset:       100,
                callback:     function(box) {
                    console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
                }
            }
    );
    wow.init();
    document.getElementById('moar').onclick = function() {
        var section = document.createElement('section');
        section.className = 'section--purple wow fadeInDown';
        this.parentNode.insertBefore(section, this);
    };
</script>
<!--<![endif]--><!--<![endif]-->


<script>
    $(".click-toggle").click(function () {
        $(".click-toggle-content").stop().slideToggle();
        return false;
    });

    $('.click-toggle').on('click', function (e) {
        $('.click-toggle .fa-angle-down').toggleClass("transform1");
    });

    $(".menu-toggle").click(function () {
        $(".menu-toggle-content").stop().slideToggle();
        return false;
    });
</script>


<script>
    $(document).ready(function(){
        $('.js-menu-trigger').on('click touchstart', function(e){
            $('.js-menu').toggleClass('is-visible');
            $('.js-menu-screen').toggleClass('is-visible');
            e.preventDefault();
        });

        $('.js-menu-screen').on('click touchstart', function(e){
            $('.js-menu').toggleClass('is-visible');
            $('.js-menu-screen').toggleClass('is-visible');
            e.preventDefault();
        });
    });
</script>


<script>!function(n,r){"function"==typeof define&&define.amd?define(r):n.transformicons=r()}(this||window,function(){"use strict";var n={},r="tcon-transform",t={transform:["click"],revert:["click"]},e=function(n){return"string"==typeof n?Array.prototype.slice.call(document.querySelectorAll(n)):"undefined"==typeof n||n instanceof Array?n:[n]},o=function(n){return"string"==typeof n?n.toLowerCase().split(" "):n},i=function(n,r,i){var c=(i?"remove":"add")+"EventListener",u=e(n),a=u.length,s={};for(var l in t)s[l]=r&&r[l]?o(r[l]):t[l];for(;a--;)for(var d in s)for(var v=s[d].length;v--;)u[a][c](s[d][v],f)},f=function(r){n.toggle(r.currentTarget)};return n.add=function(r,t){return i(r,t),n},n.remove=function(r,t){return i(r,t,!0),n},n.transform=function(t){return e(t).forEach(function(n){n.classList.add(r)}),n},n.revert=function(t){return e(t).forEach(function(n){n.classList.remove(r)}),n},n.toggle=function(t){return e(t).forEach(function(t){n[t.classList.contains(r)?"revert":"transform"](t)}),n},n});transformicons.add('.tcon');</script>

<script>
    $(document).ready(function() {

        $(window).scroll(function () {
            if ( $(this).scrollTop() > 400 && !$('.panel-container').hasClass('show-panel') ) {
                $('.panel-container').addClass('show-panel');
                $('.panel-container').slideDown();
            } else if ( $(this).scrollTop() <= 400 ) {
                $('.panel-container').removeClass('show-panel');
            }
        });

        $(window).scroll(function () {
            if ( $(this).scrollTop() > 1 && !$('.add-pad').hasClass('add-padding') ) {
                $('.add-pad').addClass('add-padding');
                $('.add-pad').slideDown();
            } else if ( $(this).scrollTop() <= 1 ) {
                $('.add-pad').removeClass('add-padding');
            }
        });

        $(window).scroll(function () {
            if ( $(this).scrollTop() > 400 && !$('.offer2').hasClass('left') ) {
                $('.offer2').addClass('left');
                $('.offer2').slideDown();
            } else if ( $(this).scrollTop() <= 400 ) {
                $('.offer2').removeClass('left');
            }
        });



        $('a[rel="relativeanchor"]').click(function(){
            $('html, body').animate({
                scrollTop: $( $.attr(this, 'href') ).offset().top
            }, 500);
            return false;
        });
    });
</script>
<!-- -->
<script type="text/javascript" src="/wp-content/themes/dazzling/assets/themes/default/js/fresco.js"></script>


<!-- -->

<script src="/wp-content/themes/dazzling/assets/themes/default/js/TweenLite.min.js"></script>
<script src="/wp-content/themes/dazzling/assets/themes/default/js/EasePack.min.js"></script>
<script src="/wp-content/themes/dazzling/assets/themes/default/js/demo-2.js"></script>
<!-- -->
<script src="/wp-content/themes/dazzling/assets/themes/default/js/modernizr.custom.js"></script>
<script src="/wp-content/themes/dazzling/assets/themes/default/js/jquery.dlmenu.js"></script>
<script>
    $(function() {
        $( '#dl-menu' ).dlmenu();
    });
</script>

<!-- -->
<script type="text/javascript" src="/wp-content/themes/dazzling/assets/themes/default/js/jquery.hoverdir.js"></script>
<script type="text/javascript">
    $(function() {

        $(' #da-thumbs > div ').each( function() { $(this).hoverdir({
            hoverDelay : 75
        }); } );

    });
</script>

<!-- -->
<script type="text/javascript" src="/wp-content/themes/dazzling/assets/themes/default/js/jquery.easing.min.js"></script>
<script type="text/javascript" src="/wp-content/themes/dazzling/assets/themes/default/js/jquery.mixitup.min.js"></script>
<script type="text/javascript">
    $(function () {

        var filterList = {

            init: function () {

                // MixItUp plugin
                // http://mixitup.io
                $('#portfoliolist').mixitup({
                    targetSelector: '.portfolio1',
                    filterSelector: '.filter',
                    effects: ['fade'],
                    easing: 'snap',
                    showOnLoad: 'logo',
                    // call the hover effect
                    onMixEnd: filterList.hoverEffect()
                });

            },

            hoverEffect: function () {

                // Simple parallax effect
                $('#portfoliolist .portfolio1').hover(
                        function () {
                            $(this).find('.label').stop().animate({bottom: 0}, 200, 'easeOutQuad');
                            $(this).find('img').stop().animate({top: 0}, 500, 'easeOutQuad');
                        },
                        function () {
                            $(this).find('.label').stop().animate({bottom: -540}, 200, 'easeInQuad');
                            $(this).find('img').stop().animate({top: 0}, 300, 'easeOutQuad');
                        }
                );

            }

        };

        // Run the show!
        filterList.init();


    });
</script>

<!-- Litebox -->
<script src="/wp-content/themes/dazzling/assets/themes/default/js/litebox.js"></script>
<script type="text/javascript">
    $('.litebox').liteBox();
</script>