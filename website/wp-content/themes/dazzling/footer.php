<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package dazzling
 */
?>
	</div><!-- #content -->
    
<div class="container-fluid footer">
            <div class="row">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-3 footer-logo">
                            <img src="/wp-content/themes/dazzling/img/footer-logo.png" border="0">                           
                            <p>All images copyright their respective photographers and used with permission</p>                        
                            
                            
                            <div class="soc-seti">
                                <a href="https://www.facebook.com/bigfolio" target="_blank"><i class="fa fa-facebook fa-2x"></i></a>
                                <a href="https://twitter.com/bigfolio" target="_blank"><i class="fa fa-twitter fa-2x"></i></a>
                                <a href="https://plus.google.com/115097031505103133951" target="_blank"><i class="fa fa-google-plus fa-2x"></i></a>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6 footer-links">
                            <span class="plans-h2">Links</span>
                            <div class="footer-links-items">
                                <ul>
              <li><a href="/websites/">Websites for photographers</a></li>
              <li><a href="http://demo.bigfolio.com">Create a free demo website</a></li>
              <li><a target="_blank" href="http://propx.co">Photography proofing</a></li>
              <li><a href="/slideshows/">Photography slideshows</a></li>
              <li><a href="https://www.highrisksolutions.com/high-risk-merchant-accounts/" target="_blank">High Risk Merchant Account</a></li>
                                </ul>

                                <ul>
                                    <li><a href="/about/">About our company</a></li>
              <li><a href="/contact/">Contact us</a></li>
              <li><a class="standout" href="http://bigfolio.com/help">Help &amp; support</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-3 footer-links">
                            <span class="plans-h2">Navigation</span>
                            <div class="footer-links-items">
                                <ul>
                                    <li><a href="/websites/">Websites</a></li>
              <li><a target="_blank" href="http://propx.co">Products</a></li>
              <li><a href="/photography-logo-design/">Services</a></li>
              <li><a href="/about/">Company</a></li>
                                </ul>
                                <ul>
                                    <li><a href="/contact/">Contact</a></li>
              <li><a href="http://bigfolio.com/help/">Support</a></li>
              <li><a class="standout" href="http://go.bigfolio.com/">Login</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>    
    
     <div id="bottomfooter" style="text-align:center;">&copy; 2017 BIG Folio Inc. &bullet; All rights reserved<br><a href="http://bigfolio.com/legal/terms-of-service/">Terms and Conditions</a></div>   
    

    <script src="/wp-content/themes/dazzling/inc/js/dev/bootstrap.js"></script>    
    <script src="/wp-content/themes/dazzling/inc/js/dev/yii.js"></script>
<?php wp_footer(); ?>



<!-- begin PA code -->
<script type="text/javascript">
  (function() {
    window._pa = window._pa || {};
    var pa = document.createElement('script'); pa.type = 'text/javascript'; pa.async = true;
    pa.src = ('https:' == document.location.protocol ? 'https:' : 'http:') + "//tag.perfectaudience.com/serve/53a7751090f0af6449000051.js";
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(pa, s);
  })();
</script>
<!-- end PA code -->

</body>
</html>