<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package dazzling
 */
?>


<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js"></script>
    <script src="/wp-content/themes/dazzling/inc/js/new/backbone-min.js"></script>
    <script src="/wp-content/themes/dazzling/inc/js/new/scroll_view.js"></script>
    <script src='/wp-content/themes/dazzling/inc/js/new/main.js'></script>
    

<link href="/wp-content/themes/dazzling/css/bootstrap.css" rel="stylesheet">
<link href="/wp-content/themes/dazzling/css/style.css" rel="stylesheet">
<link href="/wp-content/themes/dazzling/css/authchoice.css" rel="stylesheet"> 
    
<link rel="stylesheet" href="/wp-content/themes/dazzling/css/responsive.css">
<link rel="stylesheet" href="/wp-content/themes/dazzling/inc/css/theme.css">
<link rel="stylesheet" href="/wp-content/themes/dazzling/inc/css/font-awesome.min.css">


    <!-- New GA Universal code | Added 6/1/2014 -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-25984646-1', 'bigfolio.com');
      ga('send', 'pageview');
    </script> 

  


<?php wp_head(); ?>



</head>

<body>
<div id="page" class="hfeed site">

	<!-- Fixed navbar -->
    <nav id="w0" class="navbar-default navbar-fixed-top module-nav navbar" role="navigation">
		<div class="container">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#w0-collapse">
			    <span class="sr-only">Toggle navigation</span>
			    <span class="icon-bar"></span>
			    <span class="icon-bar"></span>
			    <span class="icon-bar"></span>
			  </button><a class="navbar-brand" href="/"><img class="logo" src="/wp-content/themes/dazzling/img/logo.png" alt=""></a>



			</div>
            
  <div id="w0-collapse" class="collapse navbar-collapse"><ul id="menu-headermenu" class="nav navbar-nav"><li id="menu-item-3674" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3674"><a title="Templates" href="http://bigfolio.com/photography-website-templates/">Templates</a></li>
<li id="menu-item-1107" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1107"><a title="Our Clients" href="http://bigfolio.com/photographers/">Our Clients</a></li>

<li id="menu-item-20" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-20 dropdown"><a title="Services" href="#" data-toggle="dropdown" class="dropdown-toggle">Services <span class="caret"></span></a>
<ul role="menu" class=" dropdown-menu">
	<li id="menu-item-94" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-94"><a title="Logo Design" href="http://bigfolio.com/photography-logo-design/">Logo Design</a></li>
	<li id="menu-item-1110" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1110"><a title="Sell Your Photos" href="http://bigfolio.com/marketing-selling-photographs-online/">Sell Your Photos</a></li>
	<!--<li id="menu-item-93" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-93"><a title="SEO for Creatives" href="http://bigfolio.com/photography-seo-services/">SEO for Creatives</a></li>-->
	<li id="menu-item-92" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-92"><a title="White Glove Service" href="http://bigfolio.com/photography-portfolio-design/">White Glove Service</a></li>

</ul>
</li>
<li id="menu-item-21" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-21 dropdown"><a title="Company" href="#" data-toggle="dropdown" class="dropdown-toggle">Company <span class="caret"></span></a>
<ul role="menu" class=" dropdown-menu">
<li id="menu-item-754" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-754"><a title="Plans" href="http://bigfolio.com/plans/">Premium Plans</a></li>	
	<li id="menu-item-3673" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3673"><a title="Websites" href="http://bigfolio.com/websites-2/">Websites</a></li>
	<li id="menu-item-84" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-84"><a title="Slideshows" href="http://bigfolio.com/slideshows/">Slideshows</a></li>
	<li id="menu-item-98" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-98"><a title="About Us" href="http://bigfolio.com/about/">About Us</a></li>
</ul>
</li>

<li id="menu-item-23" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-23 dropdown"><a title="Support" href="#" data-toggle="dropdown" class="dropdown-toggle">Support <span class="caret"></span></a>
<ul role="menu" class=" dropdown-menu">
	<li id="menu-item-22" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-22"><a title="Contact" href="http://bigfolio.com/contact/">Contact</a></li>
	<li id="menu-item-3617" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3617"><a title="Help Docs" href="http://bigfolio.com/help/">Help Docs</a></li>
	<li id="menu-item-102" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-102"><a title="Submit a Support Ticket" href="http://support.bigfolio.com/anonymous_requests/new">Submit a Support Ticket</a></li>
</ul>
</li>
<li id="menu-item-1005" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1005"><a title="Login" href="http://go.bigfolio.com/">Login</a></li>
</ul></div>		        
            
            
            
				<?php //  dazzling_header_menu(); ?>
		</div>
      
	</div><!-- .site-navigation -->
