<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package dazzling
 */

get_header(); ?>

    <div class="container">
        <div class="row">
            <div class="landing-stage2" style="padding-top: 100px;">
                <h3 style="color:#F98D37;">See Why Top Photographers Around The World Choose Bigfolio</h3>                
               
                <div id="index_categories" class="landing-stage2-photo">
                    <div class="items">
                        <div class="items-img2">
                            <a href="http://www.capturedbycarrie.com" target="_blank"><img src="/wp-content/themes/dazzling/img/example1.png" alt=""></a>
                        </div>
                        <a class="items-text" href="http://www.capturedbycarrie.com" target="_blank">http://www.capturedbycarrie.com</a>
                    </div>
                    <div class="items">
                        <div class="items-img2">
                            <a href="http://www.elizabethmessina.com" target="_blank"><img src="/wp-content/themes/dazzling/img/example2.png" alt=""></a>
                        </div>
                        <a class="items-text" href="http://www.elizabethmessina.com" target="_blank">http://www.elizabethmessina.com</a>
                    </div>
                    <div class="items">
                        <div class="items-img2">
                            <a href="http://www.katmonk.com" target="_blank"><img src="/wp-content/themes/dazzling/img/example3.png" alt=""></a>
                        </div>
                        <a class="items-text" href="http://www.katmonk.com" target="_blank">http://www.katmonk.com</a>
                    </div>
                    <div class="items">
                        <div class="items-img2">
                            <a href="http://www.freshsugar.com" target="_blank"><img src="/wp-content/themes/dazzling/img/example4.png" alt=""></a>
                        </div>
                        <a class="items-text" href="http://www.freshsugar.com" target="_blank">http://www.freshsugar.com</a>
                    </div>
                    <div class="items">
                        <div class="items-img2">
                            <a href="http://www.marlenerounds.com" target="_blank"><img src="/wp-content/themes/dazzling/img/example5.png" alt=""></a>
                        </div>
                        <a class="items-text" href="http://www.marlenerounds.com" target="_blank">http://www.marlenerounds.com</a>
                    </div>
                    <div class="items">
                        <div class="items-img2">
                           <a href="http://www.urbangrace.com" target="_blank"> <img src="/wp-content/themes/dazzling/img/example6.png" alt=""></a>
                        </div>
                        <a class="items-text" href="http://www.urbangrace.com" target="_blank">http://www.urbangrace.com</a>
                    </div>
                    
                    <div class="items">
                        <div class="items-img2">
                            <a href="http://www.richardmilesphotography.co.uk" target="_blank"><img src="/wp-content/themes/dazzling/img/example7.png" alt=""></a>
                        </div>
                        <a class="items-text" href="http://www.richardmilesphotography.co.uk" target="_blank">http://www.richardmilesphotography.co.uk</a>
                    </div>
                    <div class="items">
                        <div class="items-img2">
                            <a href="http://www.marinakoslowphotography.com" target="_blank"><img src="/wp-content/themes/dazzling/img/example8.png" alt=""></a>
                        </div>
                        <a class="items-text" href="http://www.marinakoslowphotography.com" target="_blank">http://www.marinakoslowphotography.com</a>
                    </div>
                    <div class="items">
                        <div class="items-img2">
                            <a href="http://www.tylerroemer.com" target="_blank"><img src="/wp-content/themes/dazzling/img/example9.png" alt=""></a>
                        </div>
                        <a class="items-text" href="http://www.tylerroemer.com" target="_blank">http://www.tylerroemer.com</a>
                    </div>
                    <div class="items">
                        <div class="items-img2">
                            <a href="http://www.virgilbunao.com" target="_blank"><img src="/wp-content/themes/dazzling/img/example10.png" alt=""></a>
                        </div>
                        <a class="items-text" href="http://www.virgilbunao.com" target="_blank">http://www.virgilbunao.com</a>
                    </div>
                    <div class="items">
                        <div class="items-img2">
                            <a href="http://www.danielagarza.com" target="_blank"><img src="/wp-content/themes/dazzling/img/example11.png" alt=""></a>
                        </div>
                        <a class="items-text" href="http://www.danielagarza.com" target="_blank">http://www.danielagarza.com</a>
                    </div>
                    <div class="items">
                        <div class="items-img2">
                           <a href="http://www.laurenleonardinteriors.com" target="_blank"> <img src="/wp-content/themes/dazzling/img/example12.png" alt=""></a>
                        </div>
                        <a class="items-text" href="http://www.laurenleonardinteriors.com" target="_blank">http://www.laurenleonardinteriors.com</a>
                    </div>
                    
                    <div class="items">
                        <div class="items-img2">
                            <a href="http://www.crphotocommercial.com" target="_blank"><img src="/wp-content/themes/dazzling/img/example13.png" alt=""></a>
                        </div>
                        <a class="items-text" href="http://www.crphotocommercial.com" target="_blank">http://www.crphotocommercial.com</a>
                    </div>
                    <div class="items">
                        <div class="items-img2">
                            <a href="http://www.anetamak.com" target="_blank"><img src="/wp-content/themes/dazzling/img/example14.png" alt=""></a>
                        </div>
                        <a class="items-text" href="http://www.anetamak.com" target="_blank">http://www.anetamak.com</a>
                    </div>
                    <div class="items">
                        <div class="items-img2">
                           <a href="http://www.lathremhomebuilders.com" target="_blank"> <img src="/wp-content/themes/dazzling/img/example15.png" alt=""></a>
                        </div>
                        <a class="items-text" href="http://www.lathremhomebuilders.com" target="_blank">http://www.lathremhomebuilders.com</a>
                    </div>                    
                    
                                        
                    
                </div>
            </div>
        </div>
    </div>
    
    
    

	


<?php get_footer(); ?>