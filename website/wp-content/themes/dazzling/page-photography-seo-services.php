<?php
/**
 * The samples template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package dazzling
 */

get_header(); ?>




    <!-- Masthead image -->
   <div class='page-header' id='seo' style="height:400px;">

</div>
<div class='page-body'>
  <div class='container'>
    <h1 style="color:#F98D37; font-family: 'dejavu_sansbook',sans; font-weight: normal; text-align: center; margin-bottom: 40px;">Photography SEO Services</h1>
    <div class='row'>
      <div class='col-md-8'>
        <p>
          We currently offer SEO consulting services for professional photographers.
         
        </p>
        <p>No matter what type of photography you offer, your customers are trying to find you online. And contrary to what many believe, having a beautiful Flash website does not mean your site will be inaccessible to some or that your search engine rankings have to suffer.</p>
        <p>At BIG Folio, we know Flash & HTML websites and how to optimize them. Our optimization service can help your site attract more potential customers. We will analyze your Flash/HTML site, make necessary optimization changes, and deliver a plan for ongoing improvements.</p>
     
 
        <h3 style="color:#F98D37;">Our SEO Process</h3>
        <p>Once you purchase the service, we will schedule your site for optimization (we're a small team) and send you a thorough survey for the purpose of understanding your site's goals and needs. Once we have received your completed survey and performed an analysis of your site, we will implement our recommended changes, backing up any necessary files. Finally, you will receive a 1-page report outlining the changes we performed and the reasons behind them, and a guide to improving your search engine rankings even further.</p>
                        <h3 style="color:#F98D37;">
          One Time Fee - $249
        </h3>
      </div>
      <div class='col-md-4'>
        <h4>Why is the service cheaper for BIG Folio customers?</h4>
        <p>Because we know the inner workings of our own sites and have immediate access to our servers, optimizing our own sites takes less time.</p>
        <h4>Can You Guarantee Results?</h4>
        <p>While we will do everything in our power to get your site indexed and rank higher, we simply cannot guarantee specific rankings for SEO customers. In fact, we think you should be wary of any company that makes such a promise.</p>

      </div>
      
    </div>
  </div>
</div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/wp-content/themes/dazzling/inc/js/new/js/new/jquery-1.10.1.min.js"><\/script>')</script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/backbone.js/1.1.0/backbone-min.js"></script>
    <script src="/wp-content/themes/dazzling/inc/js/new/js/new/bootstrap.min.js"></script>
    <script src="/wp-content/themes/dazzling/inc/js/new/js/new/scroll_view.js"></script>
    <script src="/wp-content/themes/dazzling/inc/js/new/js/new/main.js"></script>



<BR /><BR />     
        



<?php get_footer(); ?>