<?php
/**
 * The samples template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package dazzling
 */

get_header(); ?>
	<div class="top-section">
<div id='masthead201408'>
  <div class='container'>
    <h2>

      Professional Photography Website Templates
    </h2>
  </div>
  <div class='tryitnow'>
    <div class='tryitnowinner'>
      <a class='tryitnowbtn' href='http://demo.bigfolio.com/'>Try it now</a>
    </div>
  </div>
</div>


<!-- CTA navigation (below masthead) -->
<div id='cta'>
  <div class='container'>
    <div class='row'>
      <div class='col-md-8 col-md-offset-2'>
        <div class='cta-menu'>
          <a href='#'>Big Folio Photographers</a>
          <a href='#'>Templates</a>
          <a href='#'>Why Big Folio</a>
          <a class='active' href='#'>Get Started</a>
        </div>
      </div>
    </div>
  </div>
</div>

<BR /><BR />
<!-- Rubix information and samples -->
<div id='rubix'>
  <div class='row'>
    <div class='col-md-8 col-md-offset-2'>
      <h3 style="color:#000000; font-size:18px;">Some of the world's best photographers, designers, film makers and artists rely on BIG Folio</h3>


    </div>
  </div>   
  
  
  <div id='rubix-samples'>
    <div class='row flush-row'>
      <div class='col-sm-6 no-pad' align="center">
        <a href='#' target='_blank'>
          <img class='img img-responsive' src='/wp-content/themes/dazzling/images/captured-by-carrie.jpg' style="border:thin; background-color:#000000;">
          <p style="font-size:16px;"><font color="#9c9c9c">http://www.capturedbycarrie.com</font></p>
        </a><br />
      </div>
    <div class='col-sm-6 no-pad' align="center">
      <a href='#' target='_blank'>
        <img class='img img-responsive' src='/wp-content/themes/dazzling/images/elizabethmessina.jpg'>
        <p align="center" style="font-size:16px;"><font color="#9c9c9c">http://www.elizabethmessina.com</font></p>
      </a><br />
    </div>
    </div>
    <div class='row'>
      <div class='col-sm-6 no-pad' align="center">
        <a href='#' target='_blank'>
          <img class='img img-responsive' src='/wp-content/themes/dazzling/images/katmonk.jpg' border="1">
          <p align="center" style="font-size:16px;"><font color="#9c9c9c">http://www.katmonk.com</font></p>
        </a><br />
      </div>
      <div class='col-sm-6 no-pad' align="center">
        <a href='#' target='_blank'>
          <img class='img img-responsive' src='/wp-content/themes/dazzling/images/urbangrace.jpg'>
          <p align="center" style="font-size:16px;"><font color="#9c9c9c">http://www.urbangrace.com</font></p>
        </a><br />
      </div>
    </div>
    <div class='row'>
      <div class='col-sm-6 no-pad' align="center">
        <a href='#' target='_blank'>
          <img class='img img-responsive' src='/wp-content/themes/dazzling/images/marlenerounds.jpg'>
          <p align="center" style="font-size:16px;"><font color="#9c9c9c">http://www.marlenerounds.com</font></p>
        </a><br />
      </div>
      <div class='col-sm-6 no-pad' align="center">
        <a href='#' target='_blank'>
          <img class='img img-responsive' src='/wp-content/themes/dazzling/images/virgilbunao.jpg'>
          <p align="center" style="font-size:16px;"><font color="#9c9c9c">http://www.virgilbunao.com</font></p>
        </a><br />
      </div>
    </div>  
    
    <div class='row'>
      <div class='col-sm-6 no-pad' align="center">
        <a href='#' target='_blank'>
          <img class='img img-responsive' src='/wp-content/themes/dazzling/images/tylerroemer.jpg'>
          <p align="center" style="font-size:16px;"><font color="#9c9c9c">http://www.tylerroemer.com</font></p>
        </a><br />
      </div>
      <div class='col-sm-6 no-pad' align="center">
        <a href='#' target='_blank'>
          <img class='img img-responsive' src='/wp-content/themes/dazzling/images/marinakoslow.jpg'>
          <p align="center" style="font-size:16px;"><font color="#9c9c9c">http://www.marinakoslowphotography.com</font></p>
        </a><br />
      </div>
    </div>
    
    <div class='row'>
      <div class='col-sm-6 no-pad' align="center">
        <a href='#' target='_blank'>
          <img class='img img-responsive' src='/wp-content/themes/dazzling/images/richardmiles.jpg'>
          <p align="center" style="font-size:16px;"><font color="#9c9c9c">http://www.richardmilesphotography.co.uk</font></p>
        </a><br />
      </div>
      <div class='col-sm-6 no-pad' align="center">
        <a href='#' target='_blank'>
          <img class='img img-responsive' src='/wp-content/themes/dazzling/images/danielagarza.jpg'>
          <p align="center" style="font-size:16px;"><font color="#9c9c9c">http://www.danielagarza.com</font></p>
        </a><br />
      </div>
    </div>  
    
     <div class='row'>
      <div class='col-sm-6 no-pad' align="center">
        <a href='#' target='_blank'>
          <img class='img img-responsive' src='/wp-content/themes/dazzling/images/laurenleonard.jpg'>
          <p align="center" style="font-size:16px;"><font color="#9c9c9c">http://www.laurenleonardinteriors.com</font></p>
        </a><br />
      </div>
      <div class='col-sm-6 no-pad' align="center">
        <a href='#' target='_blank'>
          <img class='img img-responsive' src='/wp-content/themes/dazzling/images/crphoto.jpg'>
          <p align="center" style="font-size:16px;"><font color="#9c9c9c">http://www.crphotocommercial.com</font></p>
        </a><br />
      </div>
    </div> 
    
     <div class='row'>
      <div class='col-sm-6 no-pad' align="center">
        <a href='#' target='_blank'>
          <img class='img img-responsive' src='/wp-content/themes/dazzling/images/anetamak.jpg'>
          <p align="center" style="font-size:16px;"><font color="#9c9c9c">http://www.anetamak.com</font></p>
        </a><br />
      </div>
      <div class='col-sm-6 no-pad' align="center">
        <a href='#' target='_blank'>
          <img class='img img-responsive' src='/wp-content/themes/dazzling/images/lathremhomebuilders.jpg'>
          <p align="center" style="font-size:16px;"><font color="#9c9c9c">http://www.lathremhomebuilders.com</font></p>
        </a><br />
      </div>
    </div>  
    
    
    <BR /><BR />
<div id='cta' align="center">
  <div class='container'>
    <div class='row'>
      <div class='col-md-8 col-md-offset-2'>
        <div class='cta-menu' align="center">
        <center><a class='active' href='#'>Start Designing Your Professional Photography Website</a></center>
        </div>
      </div>
    </div>
  </div>
</div>             
    
    
  </div>
</div>   



<BR /><BR />     
        
	</div>


<?php get_footer(); ?>