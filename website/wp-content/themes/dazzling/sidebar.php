<div id="secondary" class="widget-area col-sm-12 col-md-4" role="complementary">


<BR /><BR />




        <div class="widget tabbed">
            <div class="tabs-wrapper">
                <ul class="nav nav-tabs">
                      <li class="active"><a href="#categories" data-toggle="tab">Categories</a></li>
                      <li><a href="/photography-website-templates/">Most Popular</a></li>
                      <li><a href="/new/">New</a></li>
                </ul>

            <div class="tab-content">
                <ul id="categories" class="tab-pane active">
                
 <li>
      <a href="/wedding-photographer-website-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Wedding Photography">
          <img src="/wp-content/themes/dazzling/images/icon-wedding.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/wedding-photographer-website-templates/" rel="bookmark" title="Wedding Photography">Wedding Photography</a>
                    </div>
</li>

<li>
      <a href="/children-photography-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Baby/Kid Photography">
          <img src="/wp-content/themes/dazzling/images/icon-kid.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/children-photography-templates/" rel="bookmark" title="Baby/Kid Photography">Baby/Kid Photography</a>
                    </div>
</li>

<li>
      <a href="/fashion-photography-themes/" class="tab-thumb thumbnail" rel="bookmark" title="Fashion Photography">
          <img src="/wp-content/themes/dazzling/images/icon-fashion.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/fashion-photography-themes/" rel="bookmark" title="Fashion Photography">Fashion Photography</a>
                    </div>
</li>               
                
                

<li>
      <a href="/pet-photography-themes/" class="tab-thumb thumbnail" rel="bookmark" title="Pet Photography">
          <img src="/wp-content/themes/dazzling/images/icon-pet.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/pet-photography-themes/" rel="bookmark" title="Pet Photography">Pet Photography</a>
                    </div>
</li>


<li>
      <a href="/professional-photography-website-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Professional Photography">
          <img src="/wp-content/themes/dazzling/images/icon-professional.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/professional-photography-website-templates/" rel="bookmark" title="Professional Photography">Professional Photography</a>
                    </div>
</li>


<li>
      <a href="/event-photography-website-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Event Photography">
          <img src="/wp-content/themes/dazzling/images/icon-event.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/event-photography-website-templates/" rel="bookmark" title="Event Photography">Event Photography</a>
                    </div>
</li>


<li>
      <a href="/model-photography-themes/" class="tab-thumb thumbnail" rel="bookmark" title="Model Photography">
          <img src="/wp-content/themes/dazzling/images/icon-model.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/model-photography-themes/" rel="bookmark" title="Model Photography">Model Photography</a>
                    </div>
</li>


<li>
      <a href="/photographer-portfolio-template/" class="tab-thumb thumbnail" rel="bookmark" title="Portfolio Photography">
          <img src="/wp-content/themes/dazzling/images/icon-portfolio.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/photographer-portfolio-template/" rel="bookmark" title="Portfolio Photography">Portfolio Photography</a>
                    </div>
</li>


<li>
      <a href="/outdoor-photography-portfolio-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Outdoor Photography">
          <img src="/wp-content/themes/dazzling/images/icon-outdoor.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/outdoor-photography-portfolio-templates/" rel="bookmark" title="Outdoor Photography">Outdoor Photography</a>
                    </div>
</li>


<li>
      <a href="/general-photography-themes/" class="tab-thumb thumbnail" rel="bookmark" title="General Photography">
          <img src="/wp-content/themes/dazzling/images/icon-general.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/general-photography-themes/" rel="bookmark" title="General Photography">General Photography</a>
                    </div>
</li>

                  
                </ul>
                
                <ul id="popular" class="tab-pane">

                    
 <li>
      <a href="/wedding-photographer-website-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Wedding Photography">
          <img src="/wp-content/themes/dazzling/images/icon-wedding.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/wedding-photographer-website-templates/" rel="bookmark" title="Wedding Photography">Wedding Photography</a>
                    </div>
</li>

<li>
      <a href="/children-photography-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Baby/Kid Photography">
          <img src="/wp-content/themes/dazzling/images/icon-kid.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/children-photography-templates/" rel="bookmark" title="Baby/Kid Photography">Baby/Kid Photography</a>
                    </div>
</li>

<li>
      <a href="/fashion-photography-themes/" class="tab-thumb thumbnail" rel="bookmark" title="Fashion Photography">
          <img src="/wp-content/themes/dazzling/images/icon-fashion.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/fashion-photography-themes/" rel="bookmark" title="Fashion Photography">Fashion Photography</a>
                    </div>
</li>               
                
                

<li>
      <a href="/pet-photography-themes/" class="tab-thumb thumbnail" rel="bookmark" title="Pet Photography">
          <img src="/wp-content/themes/dazzling/images/icon-pet.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/pet-photography-themes/" rel="bookmark" title="Pet Photography">Pet Photography</a>
                    </div>
</li>


<li>
      <a href="/professional-photography-website-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Professional Photography">
          <img src="/wp-content/themes/dazzling/images/icon-professional.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/professional-photography-website-templates/" rel="bookmark" title="Professional Photography">Professional Photography</a>
                    </div>
</li>


<li>
      <a href="/event-photography-website-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Event Photography">
          <img src="/wp-content/themes/dazzling/images/icon-event.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/event-photography-website-templates/" rel="bookmark" title="Event Photography">Event Photography</a>
                    </div>
</li>


<li>
      <a href="/model-photography-themes/" class="tab-thumb thumbnail" rel="bookmark" title="Model Photography">
          <img src="/wp-content/themes/dazzling/images/icon-model.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/model-photography-themes/" rel="bookmark" title="Model Photography">Model Photography</a>
                    </div>
</li>


<li>
      <a href="/photographer-portfolio-template/" class="tab-thumb thumbnail" rel="bookmark" title="Portfolio Photography">
          <img src="/wp-content/themes/dazzling/images/icon-portfolio.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/photographer-portfolio-template/" rel="bookmark" title="Portfolio Photography">Portfolio Photography</a>
                    </div>
</li>


<li>
      <a href="/outdoor-photography-portfolio-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Outdoor Photography">
          <img src="/wp-content/themes/dazzling/images/icon-outdoor.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/outdoor-photography-portfolio-templates/" rel="bookmark" title="Outdoor Photography">Outdoor Photography</a>
                    </div>
</li>


<li>
      <a href="/general-photography-themes/" class="tab-thumb thumbnail" rel="bookmark" title="General Photography">
          <img src="/wp-content/themes/dazzling/images/icon-general.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/general-photography-themes/" rel="bookmark" title="General Photography">General Photography</a>
                    </div>
</li>
                    
                </ul>
                
                <ul id="new" class="tab-pane">

                                
 <li>
      <a href="/wedding-photographer-website-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Wedding Photography">
          <img src="/wp-content/themes/dazzling/images/icon-wedding.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/wedding-photographer-website-templates/" rel="bookmark" title="Wedding Photography">Wedding Photography</a>
                    </div>
</li>

<li>
      <a href="/children-photography-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Baby/Kid Photography">
          <img src="/wp-content/themes/dazzling/images/icon-kid.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/children-photography-templates/" rel="bookmark" title="Baby/Kid Photography">Baby/Kid Photography</a>
                    </div>
</li>

<li>
      <a href="/fashion-photography-themes/" class="tab-thumb thumbnail" rel="bookmark" title="Fashion Photography">
          <img src="/wp-content/themes/dazzling/images/icon-fashion.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/fashion-photography-themes/" rel="bookmark" title="Fashion Photography">Fashion Photography</a>
                    </div>
</li>               
                
                

<li>
      <a href="/pet-photography-themes/" class="tab-thumb thumbnail" rel="bookmark" title="Pet Photography">
          <img src="/wp-content/themes/dazzling/images/icon-pet.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/pet-photography-themes/" rel="bookmark" title="Pet Photography">Pet Photography</a>
                    </div>
</li>


<li>
      <a href="/professional-photography-website-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Professional Photography">
          <img src="/wp-content/themes/dazzling/images/icon-professional.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/professional-photography-website-templates/" rel="bookmark" title="Professional Photography">Professional Photography</a>
                    </div>
</li>


<li>
      <a href="/event-photography-website-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Event Photography">
          <img src="/wp-content/themes/dazzling/images/icon-event.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/event-photography-website-templates/" rel="bookmark" title="Event Photography">Event Photography</a>
                    </div>
</li>


<li>
      <a href="/model-photography-themes/" class="tab-thumb thumbnail" rel="bookmark" title="Model Photography">
          <img src="/wp-content/themes/dazzling/images/icon-model.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/model-photography-themes/" rel="bookmark" title="Model Photography">Model Photography</a>
                    </div>
</li>


<li>
      <a href="/photographer-portfolio-template/" class="tab-thumb thumbnail" rel="bookmark" title="Portfolio Photography">
          <img src="/wp-content/themes/dazzling/images/icon-portfolio.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/photographer-portfolio-template/" rel="bookmark" title="Portfolio Photography">Portfolio Photography</a>
                    </div>
</li>


<li>
      <a href="/outdoor-photography-portfolio-templates/" class="tab-thumb thumbnail" rel="bookmark" title="Outdoor Photography">
          <img src="/wp-content/themes/dazzling/images/icon-outdoor.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/outdoor-photography-portfolio-templates/" rel="bookmark" title="Outdoor Photography">Outdoor Photography</a>
                    </div>
</li>


<li>
      <a href="/general-photography-themes/" class="tab-thumb thumbnail" rel="bookmark" title="General Photography">
          <img src="/wp-content/themes/dazzling/images/icon-general.jpg" class="attachment-tab-small wp-post-image" width="51" height="42" /></a>
                    <div class="content">
                                <a class="tab-entry" href="/general-photography-themes/" rel="bookmark" title="General Photography">General Photography</a>
                    </div>
</li>
                    
                </ul>
                
                </div>
            </div>
        </div>

	</div><!-- #secondary -->