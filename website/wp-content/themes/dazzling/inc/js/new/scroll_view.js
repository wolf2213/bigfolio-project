var ScrollView = Backbone.View.extend({

  events : {
    "scroll" : "detect_scroll"
  },

  initialize: function() {
    _.bindAll(this, 'detect_scroll');
    $(window).scroll(this.detect_scroll);
  },

  detect_scroll: function() {
    var scroll = $(window).scrollTop();
    if (scroll >= 475) {
      $('#cta').addClass('fixed');
      $('#rubix').addClass('unfixed');
    } else {
      $('#cta').removeClass('fixed');
      $('#rubix').removeClass('unfixed');
    }
  }
});