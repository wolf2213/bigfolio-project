var SampleView = Backbone.View.extend({
  el: '#rubix-samples',
  
  template: _.template($('#template-rubix').html()),
  
  render: function() {
    var attributes = {msg: 'hello'};
    this.$el.append(this.template(attributes));
    $('#rubix-more').remove();
  }, 

  events: {'click #rubix-more': 'showMore'},

  showMore: function() {
    this.render();
  }
});