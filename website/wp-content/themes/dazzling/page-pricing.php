<?php
/**
 * The samples template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package dazzling
 */

get_header(); ?>
	<div class="top-section">
<div id='masthead201408'>
  <div class='container'>
    <h2>

      Professional Photography Website Templates
    </h2>
  </div>
  <div class='tryitnow'>
    <div class='tryitnowinner'>
      <a class='tryitnowbtn' href='http://demo.bigfolio.com/'>Try it now</a>
    </div>
  </div>
</div>


<!-- CTA navigation (below masthead) -->
<div id='cta'>
  <div class='container'>
    <div class='row'>
      <div class='col-md-8 col-md-offset-2'>
        <div class='cta-menu'>
          <a href='#'>Big Folio Photographers</a>
          <a href='#'>Templates</a>
          <a href='#'>Why Big Folio</a>
          <a class='active' href='#'>Get Started</a>
        </div>
      </div>
    </div>
  </div>
</div>

  
<!-- Pricing -->
<div id='pricing'>
  <div id='pricing-header'>
    <div class='container'>
      <h2>Simple Pricing</h2>
      <h3>
        30 day money-back guarantee.
        No high-pressure sales people.
      </h3>
    </div>
  </div>
  <div id='pricing-grid'>
    <div class='row'>
      <div class='plan col-sm-4'>
        <div class='top'>
          <div class='name'>
            Lite
          </div>
          <div class='price'>
            $9
            <span>/mo</span>
          </div>
          <div class='frequency'>
            Billed annually. $12/mo monthly.
          </div>
        </div>
        <div class='bottom'>
          <ul>
            <li>1 page</li>
            <li>1 gallery (up to 50 images)</li>
            <li>Contact form</li>
            <li>1 external link</li>
            <li>2GB of storage</li>
            <li>25GB of bandwidth</li>
            <li>Video embedding</li>
            <li>Mobile website</li>
            <li>SEO features</li>
            <li>BIG Show slideshows (5)</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
          </ul>
        </div>
        <div class='action'>
          <p>
            <a class='btn btn-warning' href='https://signup.bigfolio.com/signup/plan/bf-lite-annual'>Get Started</a>
          </p>
          <p>
            or
            <a href='https://signup.bigfolio.com/signup/plan/bf-lite-monthly'>signup with monthly billing</a>
          </p>
        </div>
      </div>
      <div class='plan col-sm-4'>
        <div class='top'>
          <div class='name'>
            Professional
          </div>
          <div class='price'>
            $19
            <span>/mo</span>
          </div>
          <div class='frequency'>
            Billed annually. $22/mo monthly.
          </div>
        </div>
        <div class='bottom'>
          <ul>
            <li>Unlimited pages</li>
            <li>Unlimited galleries</li>
            <li>Contact form</li>
            <li>Unlimited external links</li>
            <li>4GB of storage</li>
            <li>50GB of bandwidth</li>
            <li>Video embedding</li>
            <li>Mobile website</li>
            <li>SEO features</li>
            <li>BIG Show slideshows (5)</li>
            <li>FTP access</li>
            <li>WordPress blog</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
          </ul>
        </div>
        <div class='action'>
          <p>
            <a class='btn btn-warning' href='https://signup.bigfolio.com/signup/plan/bf-pro-annual'>Get Started</a>
          </p>
          <p>
            or
            <a href='https://signup.bigfolio.com/signup/plan/bf-pro-monthly'>signup with monthly billing</a>
          </p>
        </div>
      </div>
      <div class='plan col-sm-4'>
        <div class='top'>
          <div class='name'>
            Ecommerce
          </div>
          <div class='price'>
            $29
            <span>/mo</span>
          </div>
          <div class='frequency'>
            Billed annually. $32/mo monthly.
          </div>
        </div>
        <div class='bottom'>
          <ul>
            <li>Unlimited pages</li>
            <li>Unlimited galleries</li>
            <li>Contact form</li>
            <li>Unlimited external links</li>
            <li>4GB of storage</li>
            <li>50GB of bandwidth</li>
            <li>Video embedding</li>
            <li>Mobile website</li>
            <li>SEO features</li>
            <li>BIG Show slideshows (5)</li>
            <li>FTP access</li>
            <li>WordPress blog</li>
            <li>PROpx account</li>
            <li>Integrated ecommerce</li>
          </ul>
        </div>
        <div class='action'>
          <p>
            <a class='btn btn-warning' href='https://signup.bigfolio.com/signup/plan/commerce-plan-annual'>Get Started</a>
          </p>
          <p>
            or
            <a href='https://signup.bigfolio.com/signup/plan/commerce-plan-monthly'>signup with monthly billing</a>
          </p>
        </div>
      </div>
    </div>
  </div>
              
    
    
  </div>
</div>   



<BR /><BR />     
        



<?php get_footer(); ?>