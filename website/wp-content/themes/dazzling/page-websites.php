<?php
/**
 * The samples template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package dazzling
 */

get_header(); ?>

    <!-- Preview box for demo generator -->
<div id='rubixPreview'>
  <div class='close-btn'>
    <i class='fa fa-times-circle fa-2x'></i>
  </div>
  <iframe frameborder='0' height='86%' id='rubixPreviewFrame' style='margin:4% 4% 0 4%;' width='92%'></iframe>
</div>


    <!-- Masthead image -->
<div class='page-header' id='rubix-masthead'>
  <div class='container'>
    <h2>
      We're Rethinking HTML5 Portfolios
    </h2>
  </div>
</div>
<div class='container page-body' style="min-height: 0px; padding-bottom: 0px;">
  <div class='page-section' id='rubix-about2'>
    <div class='row'>
      <div class='col-md-6'>
        <h2 style="padding-top:35px; color:orange; font-size: 24px;">Bigfolio Cutting Edge Technology</h2>
        <p>
          Bigfolio is more than just your average website builder.  Bigfolio is a custom site platform with unprecedented layout control and SEO tools which allows you to create a stunning site to show off your work, rank higher, and get noticed faster.
        <br /><br />
        
          Just select a template, add your content and images start showing off your work. Simple, Easy and Professional!
        </p>
        <p align="center">
          Try it for 14 days absolutely free.
        </p>
        <center><a class='btn btn-warning' href='/photography-website-templates/'>Get Started &raquo;</a></center>
      </div>
      <div class='col-md-6'>
        <div class='rubixScreen curRubixScreen'>
          <img class='img img-responsive' src='/wp-content/themes/dazzling/img/rubix_MBA13_straight02.png'>
        </div>
        <div>
          <img class='img img-responsive' src='/wp-content/themes/dazzling/img/rubix_MBA13_placeholder.gif'>
        </div>
      </div>
    </div>
    <div class='row' id='giveItAWhirl'>
      <div class='col-md-8 col-md-offset-2'>
        <h2 style="color:orange; font-size: 24px;">Give it a Whirl</h2>
        <p style="font-size: 15px;">Select the menu position, gallery type, and thumbnails type from the selections below. Then click the show me button below to instantly generate a sample with your selections.</p>
      </div>
      <div id='layout-headings'>
        <div class='col-sm-4'>
          <h3 style="color:orange; font-size: 18px;">Menu Position</h3>
        </div>
        <div class='col-sm-4'>
          <h3 style="color:orange; font-size: 18px;">Gallery Type</h3>
        </div>
        <div class='col-sm-4'>
          <h3 style="color:orange; font-size: 18px;">Thumbnails Type</h3>
        </div>
      </div>
      <div id='layout-selections'>
        <div class='col-md-1 no-pad'>
          <h3 style="color:orange; font-size: 18px;">Menu Position</h3>
          <label>
            <img class='img img-responsive' src='/wp-content/themes/dazzling/img/rubix-layout/menu-left.jpg'>
            <input checked='checked' name='menu_position' type='radio' value='left'>
              Left
            </input>
          </label>
        </div>
        <div class='col-md-1 no-pad'>
          <label>
            <img class='img img-responsive' src='/wp-content/themes/dazzling/img/rubix-layout/menu-top.jpg'>
            <input name='menu_position' type='radio' value='top'>
              Top
            </input>
          </label>
        </div>
        <div class='col-md-1 no-pad'>
          <label>
            <img class='img img-responsive' src='/wp-content/themes/dazzling/img/rubix-layout/menu-right.jpg'>
            <input name='menu_position' type='radio' value='right'>
              Right
            </input>
          </label>
        </div>
        <div class='col-md-1 no-pad'>
          <h3>Gallery Type</h3>
          <label>
            <img class='img img-responsive' src='/wp-content/themes/dazzling/img/rubix-layout/gallery-single.jpg'>
            <input checked='checked' name='gallery_mode' type='radio' value='large'>
              Single
            </input>
          </label>
        </div>
        <div class='col-md-1 no-pad'>
          <label>
            <img class='img img-responsive' src='/wp-content/themes/dazzling/img/rubix-layout/gallery-horizontal.jpg'>
            <input name='gallery_mode' type='radio' value='horizontal'>
              Horizontal
            </input>
          </label>
        </div>
        <div class='col-md-1 no-pad'>
          <label>
            <img class='img img-responsive' src='/wp-content/themes/dazzling/img/rubix-layout/gallery-vertical.jpg'>
            <input name='gallery_mode' type='radio' value='vertical'>
              Vertical
            </input>
          </label>
        </div>
        <div class='col-md-1 no-pad'>
          <label>
            <img class='img img-responsive' src='/wp-content/themes/dazzling/img/rubix-layout/gallery-fullscreen.jpg'>
            <input name='gallery_mode' type='radio' value='fullscreen'>
              Fullscreen
            </input>
          </label>
        </div>
        <div class='col-md-1 no-pad'>
          <label>
            <img class='img img-responsive' src='/wp-content/themes/dazzling/img/rubix-layout/gallery-masonry.jpg'>
            <input name='gallery_mode' type='radio' value='masonry'>
              Masonry
            </input>
          </label>
        </div>
        <div id='thumbnail_mode'>
          <div class='col-md-1 no-pad'>
            <h3>Thumbnails Type</h3>
            <label>
              <img class='img img-responsive' src='/wp-content/themes/dazzling/img/rubix-layout/thumbs-bottom.jpg'>
              <input checked='checked' name='thumbnail_mode' type='radio' value='bottom'>
                Bottom
              </input>
            </label>
          </div>
          <div class='col-md-1 no-pad'>
            <label>
              <img class='img img-responsive' src='/wp-content/themes/dazzling/img/rubix-layout/thumbs-left.jpg'>
              <input name='thumbnail_mode' type='radio' value='left'>
                Left
              </input>
            </label>
          </div>
          <div class='col-md-1 no-pad'>
            <label>
              <img class='img img-responsive' src='/wp-content/themes/dazzling/img/rubix-layout/thumbs-right.jpg'>
              <input name='thumbnail_mode' type='radio' value='right'>
                Right
              </input>
            </label>
          </div>
          <div class='col-md-1 no-pad'>
            <label>
              <img class='img img-responsive' src='/wp-content/themes/dazzling/img/rubix-layout/thumbs-overlay.jpg'>
              <input name='thumbnail_mode' type='radio' value='overlay'>
                Overlay
              </input>
            </label>
          </div>
        </div>
        <div id='masonry_thumbnail_mode'>
          <div class='col-md-1 no-pad'></div>
          <div class='col-md-1 no-pad'>
            <h3>Thumbnails Type</h3>
            <label>
              <img class='img img-responsive' src='/wp-content/themes/dazzling/img/rubix-layout/thumbs-masonry-static.jpg'>
              <input name='thumbnail_mode' type='radio' value='static'>
              Static
            </label>
          </div>
          <div class='col-md-1 no-pad'>
            <label>
              <img class='img img-responsive' src='/wp-content/themes/dazzling/img/rubix-layout/thumbs-masonry-fluid.jpg'>
              <input name='thumbnail_mode' type='radio' value='fluid'>
                Fluid
              </input>
            </label>
          </div>
        </div>
        <div class='col-md-12'>
          <input class='btn btn-warning' id='showPreview' type='submit' value='Show Me!'>
        </div>
      </div>
    </div>
    <div class='row' id='circles'>
      <h2  style="color:orange; font-size: 24px;">Even More Control</h2>
      <div class='col-sm-2'>
        <img class='img img-responsive img-circle img-thumbnail' src='/wp-content/themes/dazzling/img/preview-layout.png'>
        <h3 style="color:orange; font-size: 18px;">Expanded Options</h3>
        <p>The RUBIX CMS includes an expanded layout area.</p>
      </div>
      <div class='col-sm-2'>
        <img class='img img-responsive img-circle img-thumbnail' src='/wp-content/themes/dazzling/img/preview-position.png'>
        <h3 style="color:orange; font-size: 18px;">Adjust Positions</h3>
        <p>Change the position of your logo and navigation menu at any time.</p>
      </div>
      <div class='col-sm-2'>
        <img class='img img-responsive img-circle img-thumbnail' src='/wp-content/themes/dazzling/img/preview-alignment.png'>
        <h3 style="color:orange; font-size: 18px;">Text Alignment</h3>
        <p>Adjust the text alignment of navigation elements.</p>
      </div>
      <div class='col-sm-2'>
        <img class='img img-responsive img-circle img-thumbnail' src='/wp-content/themes/dazzling/img/preview-gallery.png'>
        <h3 style="color:orange; font-size: 18px;">Gallery Styles</h3>
        <p>Choose from a growing list of gorgeous gallery styles.</p>
      </div>
      <div class='col-sm-2'>
        <img class='img img-responsive img-circle img-thumbnail' src='/wp-content/themes/dazzling/img/preview-thumbs.png'>
        <h3 style="color:orange; font-size: 18px;">Thumbnail Styles</h3>
        <p>Separately choose from various thumbnail options.</p>
      </div>
      <div class='col-sm-2'>
        <img class='img img-responsive img-circle img-thumbnail' src='/wp-content/themes/dazzling/img/preview-seo.png'>
        <h3 style="color:orange; font-size: 18px;">Even Better SEO</h3>
        <p>Fine-grained SEO controls with per-page titles and more.</p>
      </div>
    </div>
    <div class='row' id='details'>
      <div class='col-sm-4'>
        <h2 style="color:orange; font-size: 24px;"><center>Questions</center></h2>
        <div class='question' style="color:orange;">
          How much do your sites cost?
        </div>
        <p>
          We have 3 plans available with pricing starting at $9/month.
          <a href='/plans/'>
            View our pricing plans for details.
          </a>
        </p>
        <div class='question' style="color:orange;">
          Can I try a site before purchasing?
        </div>
        <p>
          Yes. You can create a 14-day trial site with no credit card required.
          <a href='/photography-website-templates/'>
            Create a demo site now
          </a>
          to get started.
        </p>
        <div class='question' style="color:orange;">
          When does my subscription start?
        </div>
        <p>
          Your monthly subscription starts as soon as you sign-up for a paid site.
        </p>
        <div class='question' style="color:orange;">
          Can I host the site on my current host?
        </div>
        <p>
          At this time, sites can only be hosted on our professional-grade hosting platform.
        </p>
        <div class='question' style="color:orange;">
          What is your refund/cancellation policy?
        </div>
        <p>
          Our guarantee: if you're not completely satisfied, cancel within 30 days of signing up and receive a full refund. There are no contracts or long-term commitments. You may cancel your account at any time.
        </p>
        <div class='question' style="color:orange;">
          Will my new site be live immediately?
        </div>
        <p>
          No. We will setup your site on a temporary address while you customize it. As soon as you're ready, going live is easy. Plus, you can continue to customize your site after it's live.
        </p>
        <div class='question' style="color:orange;">
          Can I change the colors of the site?
        </div>
        <p>
          Yes. Our feature-rich control panel allows you to change colors, add your logo, add music, and more.
        </p>
        <div class='question' style="color:orange;">
          Can I create external links and menu items?
        </div>
        <p>
          Yes. It's easy to create menu items and links to external sites, blogs, or proofing galleries. You can even link to your Facebook and Twitter accounts with ease.
        </p>
        <div class='question' style="color:orange;">
          Can I have one of these designs customized?
        </div>
        <p>
          Absolutely! If you need a feature beyond what our comprehensive control panel can do, simply contact us for an estimate.
        </p>
        <div class='question' style="color:orange;">
          Are your sites mobile/tablet friendly?
        </div>
        <p>
          Yes. Mobile versions of your site are included for viewing on iPhones &amp; tablets.
        </p>
        <div class='question' style="color:orange;">
          Are your sites SEO friendly?
        </div>
        <p>
          Absolutely. Our team has years of experience helping photographers rank higher in search engines. Our sites are designed to rank well and our control panel gives you the ability to update important ranking factors.
        </p>
        <div class='question' style="color:orange;">
          Is a blog included?
        </div>
        <p>
          Yes. We can install WordPress with all new sites. A default WordPress theme is included. If you'd like a different theme there are tons online for you to choose from and upload to your WordPress. You can also easily move your existing WordPress blog to your new site.
        </p>
        <div class='question' style="color:orange;">
          Can I host slideshows or other files on your server?
        </div>
        <p>
          Generally, yes. FTP access to your hosting account is included. Our servers support most PHP and Ruby on Rails applications, including WordPress. Please contact us if you have specific application hosting requirements.
        </p>
      </div>
      <div class='col-sm-8 designs'>
        <div class='preview'>
          <img class='img img-responsive img-thumbnail' src='/wp-content/themes/dazzling/img/rubix-urbangrace.jpg'>
          <div class='info'>
            <div class='inner'>
              <h2>Urban Grace</h2>
              <p>
                Erika Powell is one of the most talked about designers of the moment. Her design firm, Urban Grace Interiors, has been heralded by countless publications, from Traditional Home and Coastal Living to The San Francisco Chronicle and The Washington Post.
              </p>
              <p>
                <a href='http://www.urbangrace.com/' target='_blank'>
                  View this site
                </a>
              </p>
            </div>
          </div>
        </div>
        <div class='preview'>
          <img class='img img-responsive img-thumbnail' src='/wp-content/themes/dazzling/img/rubix-theleonardi.jpg'>
          <div class='info'>
            <div class='inner'>
              <h2>The Leonardi</h2>
              <p>
                The Leonardi is one of BIG Folio's only Indonesia- based clients. The Leonardi specializes in both photography & videography.
              </p>
              <p>
                <a href='http://www.the-leonardi.com/' target='_blank'>
                  View this site
                </a>
              </p>
            </div>
          </div>
        </div>
        <div class='preview'>
          <img class='img img-responsive img-thumbnail' src='/wp-content/themes/dazzling/img/rubix-carriesandoval.jpg'>
          <div class='info'>
            <div class='inner'>
              <h2>Carrie Sandoval</h2>
              <p>
                Carrie is a baby photographer based in Southern California. She has been recognized both local and world wide for her inventive, conceptual photography work with newborn babies. She is constantly challenging herself by using newfangled poses and props. Each and every baby is a complete inspiration. See Carrie's work
                <a href='http://capturedbycarrie.com/main.php' target='_blank'>
                  here.
                </a>
              </p>
              <p>
                <a href='http://capturedbycarrie.com/main.php' target='_blank'>
                  View this site
                </a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>   

    <script src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/backbone.js/1.1.0/backbone-min.js"></script>
    <script src="/wp-content/themes/dazzling/inc/js/new/sample_view.js"></script>
    <script src="/wp-content/themes/dazzling/inc/js/new/scroll_view.js"></script>
    <script src="/wp-content/themes/dazzling/inc/js/new/main.js"></script>


<?php get_footer(); ?>