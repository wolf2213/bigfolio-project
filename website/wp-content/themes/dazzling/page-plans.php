<?php
/**
 * The samples template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package dazzling
 */

get_header(); ?>




       <div class="content-container">
            <div class="container plans-block">
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12 padding-plans">
            <div class="plans-block-saving">
                <div class="header">
                    <img src="/wp-content/themes/dazzling/img/best-value.png" alt="">
                    <span class="plans-h2">Yearly</span>
                    <span class="plans-h3">Saving plans</span>
                </div>
            </div>
            <div class="plans-block-tablcont">
                <div class="items">
                    Pages
                </div>
                <div class="items">
                    Galleries
                </div>
                <div class="items">
                    Bandwidth
                </div>
                <div class="items">
                    Storage
                </div>
                <div class="items">
                    Video Embedding
                </div>
                <div class="items">
                    Mobile Website
                </div>
                <div class="items">
                    No Mobile Ads
                </div>                
                <div class="items">
                    SEO Magic
                </div>
                <div class="items">
                    Google Analytics
                </div>
                <div class="items">
                    Premium Support
                </div>
                <div class="items">
                    Wordpress Blog
                </div>
                 <div class="items">
                    FTP Access
                </div>
                <div class="items">
                    Connect Your Domain
                </div>                               
            </div>
        </div>
        
        <div class="col-md-3 col-sm-6 col-xs-12 padding-plans">
            <div class="plans-block-top">
                <div class="header">
                    <span class="plans-h2">Bigfolio Free</span>
                    <span class="plans-h3">Yearly Saving Plan</span>
                </div>
                <div class="content gradient">
                    <div class="content-price">
                        <span class="">
                            
                        </span>
                        <span class="">
                            FREE
                        </span>
                        <span class="">
                            
                        </span>
                    </div>
                    <div class="clearfix">
                        <div class="list-price">
                            
                        </div>
                        <div class="price-buy">
                            <a href="/photography-website-templates/" data-plan="price">Start by Selecting a Template</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="plans-block-tablcont text-center">
                <div class="items">
                    <span>3</span>
                </div>
                <div class="items">
                    <span>1</span>
                </div>
                <div class="items">
                    <span>1GB</span>
                </div>
                <div class="items">
                    <span>50 images</span>
                </div>
                <div class="items">
                    &#10003;
                </div>
                <div class="items">
                    &#10003;
                </div>
                <div class="items">
                    &#10003;
                </div>
                <div class="items">
                    &#10003;
                </div>
                <div class="items">
                    &#10003;
                </div>
                <div class="items">
                   
                </div>
                <div class="items">
                   
                </div>
                <div class="items">
                    
                </div>
                <div class="items">
                    
                </div>                                                
            </div>
        </div>        
        
        
        <div class="col-md-3 col-sm-6 col-xs-12 padding-plans">
            <div class="plans-block-top">
                <div class="header">
                    <span class="plans-h2">Bigfolio Lite</span>
                    <span class="plans-h3">Yearly Saving Plan</span>
                </div>
                <div class="content gradient">
                    <div class="content-price">
                        <span class="">
                            $
                        </span>
                        <span class="">
                            9
                        </span>
                        <span class="">
                            /mo
                        </span>
                    </div>
                    <div class="clearfix">
                        <div class="list-price">
                            
                        </div>
                        <div class="price-buy">
                            <a href="/photography-website-templates/" data-plan="ecommerce">Start by Selecting a Template</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="plans-block-tablcont text-center">
                <div class="items">
                    <span>10</span>
                </div>
                <div class="items">
                    <span>3</span>
                </div>
                <div class="items">
                    <span>Unlimited</span>
                </div>
                <div class="items">
                    <span>2GB</span>
                </div>
                <div class="items">
                    &#10003;
                </div>
                <div class="items">
                    &#10003;
                </div>
                <div class="items">
                    &#10003;
                </div>
                <div class="items">
                    &#10003;
                </div>
                <div class="items">
                    &#10003;
                </div>
                 <div class="items">
                    &#10003;
                </div>               
                <div class="items">
                    
                </div>
                <div class="items">
                    
                </div>
                <div class="items">
                    &#10003;
                </div>                                
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12 padding-plans">
            <div class="plans-block-top">
                <div class="header">
                    <span class="plans-h2">Bigfolio Professional</span>
                    <span class="plans-h3">Yearly Saving Plan</span>
                </div>
                <div class="content gradient">
                    <div class="content-price">
                        <span class="">
                            $
                        </span>
                        <span class="">
                            19
                        </span>
                        <span class="">
                            /mo
                        </span>
                    </div>
                    <div class="clearfix">
                        <div class="list-price">
                          
                        </div>
                        <div class="price-buy">
                            <a href="/photography-website-templates/" data-plan="unlimited">Start by Selecting a Template</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="plans-block-tablcont text-center">
                <div class="items">
                    <span>Unlimited</span>
                </div>
                <div class="items">
                    <span>Unlimited</span>
                </div>
                <div class="items">
                    <span>Unlimited</span>
                </div>
                <div class="items">
                    <span>4GB</span>
                </div>
                <div class="items">
                    &#10003;
                </div>
                <div class="items">
                    &#10003;
                </div>
                <div class="items">
                    &#10003;
                </div>
                <div class="items">
                    &#10003;
                </div>
                <div class="items">
                    &#10003;
                </div>
                <div class="items">
                    &#10003;
                </div>
                <div class="items">
                    &#10003;
                </div>
                <div class="items">
                    &#10003;
                </div>                                                
                <div class="items">
                    &#10003;
                </div>
            </div>
        </div>

    </div>
</div>
<div class="container plans-include">
    <div class="row">
        <div class="col-sm-12">
            <div class="plans-include-header">
                All Premium Plans Always Include:
            </div>
            <div class="plans-include-text">
                <div>
                    <span>&#10003;</span> Hosting
                </div>
                <div>
                    <span>&#10003;</span> No Set-up Free
                </div>
                <div>
                    <span>&#10003;</span> Templates
                </div>
                <div>
                    <span>&#10003;</span> Google Analytics
                </div>
                <div>
                    <span>&#10003;</span> Premium Support
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid plans-credit">
    <div class="plans-credit-content">
        <span class="plans-h2">We Accept all the following credit cards</span>
    </div>
    <div class="plans-credit-img">
        <div class="img">
            <a href="#"><img src="/wp-content/themes/dazzling/img/visa.png" alt=""></a>
        </div>
        <div class="img">
            <a href="#"><img src="/wp-content/themes/dazzling/img/master-card.png" alt=""></a>
        </div>
        <div class="img">
            <a href="#"><img src="/wp-content/themes/dazzling/img/discover-network.png" alt=""></a>
        </div>
        <div class="img">
            <a href="#"><img src="/wp-content/themes/dazzling/img/american-express.png" alt=""></a>
        </div>
    </div>


</div>
<div id='pricing'>
 <div id='pricing-questions'>
    <div class='row'>
      <div class='col-md-6'>
        <h3>Can I try it before purchasing?</h3>
        <p>
          Yes. You can create a full-featured demo site and no credit card is required.
          <br>
          <a href='http://demo.bigfolio.com' target='_blank'>Create a demo site now</a>
        </p>
        <h3>When does my subscription start?</h3>
        <p>Your subscription starts as soon as you sign up for a paid site.</p>
        <h3>What features does Bigfolio include?</h3>
        <p>
          Visit our Bigfolio details page to learn more about our website features and layout options.
          <br>
          <a href='/websites-2/'>Click Here</a>
        </p>
        <h3>Can I self host the site?</h3>
        <p>At this time sites can only be hosted on our professional-grade hosting platform.</p>
        <h3>Do you offer email accounts?</h3>
        <p>Big Folio does not provide email accounts directly, but we do help all of our clients setup their email through third party providers--most commonly with Zoho Mail, Google Apps, or their domain provider.</p>
      </div>
      <div class='col-md-6'>
        <h3>Are your sites mobile friendly?</h3>
        <p>Yes. Mobile versions of your site are automatically included for viewing on all mobile devices. </p>
        <h3>What is your refund/cancellation policy?</h3>
        <p>If you're not completely satisfied, cancel within 30 days of signing up and receive a full refund. There are no contracts or long-term commitments. You may cancel at any time.</p>
        <h3>Will my new site be live immediately?</h3>
        <p>No. We will setup your site on a temporary address while you customize it. As soon as you're ready, going live is easy. Plus, you can continue to customize your site after it's live.</p>
        <h3>Can I have my site customized further?</h3>
        <p>Absolutely! If you need a feature beyond what our comprehensive control panel can do, simply contact us for an estimate.</p>
        <h3>Do I need to be a web expert or know code?</h3>
        <p>Nope, we've taken care of the code so you can play. Just click options, save your choices, and your site will be updated automatically.</p>
      </div>
    </div>
  </div>
</div>




<?php get_footer(); ?>