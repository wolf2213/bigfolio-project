<?php
/**
 * The samples template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package dazzling
 */

get_header(); ?>
	<div class="top-section">
<div id='masthead201408'>
  <div class='container'>
    <h2>

      Professional Photography Website Templates
    </h2>
  </div>
  <div class='tryitnow'>
    <div class='tryitnowinner'>
      <a class='tryitnowbtn' href='http://demo.bigfolio.com/'>Try it now</a>
    </div>
  </div>
</div>


<!-- CTA navigation (below masthead) -->
<div id='cta'>
  <div class='container'>
    <div class='row'>
      <div class='col-md-8 col-md-offset-2'>
        <div class='cta-menu'>
          <a href='#'>Big Folio Photographers</a>
          <a href='#'>Templates</a>
          <a href='#'>Why Big Folio</a>
          <a class='active' href='#'>Get Started</a>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- Rubix information and samples -->
<div id='rubix'>
  <div class='row'>
    <div class='col-md-8 col-md-offset-2'>
      <h3 style="color:#000000; font-size:18px;">Some of the world's best photographers, designers, film makers and artists rely on BIG Folio</h3>


    </div>
  </div>   
  
  
  <div id='rubix-samples'>
    <div class='row flush-row'>
      <div class='col-sm-6 no-pad' align="center">
        <a href='#' target='_blank'>
          <img class='img img-responsive' src='wp-content/themes/dazzling/images/baby-children.jpg' style="border:thin; background-color:#000000;">
          <p style="font-size:16px;"><font color="#9c9c9c">Baby/Children</font> <font color="#666666">Photography</font></p>
        </a><br />
      </div>
    <div class='col-sm-6 no-pad' align="center">
      <a href='#' target='_blank'>
        <img class='img img-responsive' src='wp-content/themes/dazzling/images/wedding.jpg'>
        <p align="center" style="font-size:16px;"><font color="#9c9c9c">Wedding</font> <font color="#666666">Photography</font></p>
      </a><br />
    </div>
    </div>
    <div class='row'>
      <div class='col-sm-6 no-pad' align="center">
        <a href='#' target='_blank'>
          <img class='img img-responsive' src='wp-content/themes/dazzling/images/fashion.jpg' border="1">
          <p align="center" style="font-size:16px;"><font color="#9c9c9c">Fashion</font> <font color="#666666">Photography</font></p>
        </a><br />
      </div>
      <div class='col-sm-6 no-pad' align="center">
        <a href='#' target='_blank'>
          <img class='img img-responsive' src='wp-content/themes/dazzling/images/pet.jpg'>
          <p align="center" style="font-size:16px;"><font color="#9c9c9c">Pet</font> <font color="#666666">Photography</font></p>
        </a><br />
      </div>
    </div>
    <div class='row'>
      <div class='col-sm-6 no-pad' align="center">
        <a href='#' target='_blank'>
          <img class='img img-responsive' src='wp-content/themes/dazzling/images/portrait.jpg'>
          <p align="center" style="font-size:16px;"><font color="#9c9c9c">Portrait</font> <font color="#666666">Photography</font></p>
        </a><br />
      </div>
      <div class='col-sm-6 no-pad' align="center">
        <a href='#' target='_blank'>
          <img class='img img-responsive' src='wp-content/themes/dazzling/images/general.jpg'>
          <p align="center" style="font-size:16px;"><font color="#9c9c9c">General</font> <font color="#666666">Photography</font></p>
        </a><br />
      </div>
    </div>  
    
  </div>
</div>   


<div id='why'>
  <div id='why-header'>
    <div class='container'>
      <h2>Why BIG Folio</h2>
      <h3>Because endless creativity requires a website with endless possibilities</h3>
    </div>
  </div>
  <div id='why-fs-and-bs'>
    <div class='row'>
      <div class='col-md-6'>
        <h3 class='service'>Superior Customer Service</h3>
        <p>Relax&mdash;we've got you covered.  We know how to help &amp; help fast.  Our dedicated team has been providing the best support in the industry for over 10 years. Simply put, we are experts at what we do.</p>
      </div>
      <div class='col-md-6'>
        <h3 class='custom'>Full Custom Control</h3>
        <p>You will never have to "upgrade" to a new design again.  Our platform allows you the control over menu placement, thumbnail placement + type of thumb options, gallery layout, font options, color choices, music choice, social media integration and so much more. It's like having a custom site you can easily refine at anytime for a fraction of the cost.</p>
      </div>
    </div>
    <div class='row'>
      <div class='col-md-6'>
        <h3 class='creative'>All Creatives Welcome</h3>
        <p>Our product is designed to provide websites for all types of creative professionals. We have clients in Photography, Interior Design, Architecture, Videography, Art, Music&mdash;you name it. If you have content to add to a website, you can use our product. It's as simple as that. </p>
      </div>
      <div class='col-md-6'>
        <h3 class='seo'>SEO Magic</h3>
        <p>Search engines are keen on BIG Folio websites. Our sites are designed to rank higher, look great, and boost your sales. Take advantage of the advanced SEO options within our platform & easily update important ranking factors without any HTML knowledge.</p>
      </div>
    </div>
    <div class='row'>
      <div class='col-md-6'>
        <h3 class='wp'>WordPress Installation</h3>
        <p>Ready to blog? We install a free WordPress blog when we setup your new professional or ecommerce site.  Choose to use the default theme provided, or upload themes from other third party providers to make your blog fit your style better.</p>
      </div>
      <div class='col-md-6'>
        <h3 class='cloud'>Worry-Free Hosting</h3>
        <p>High-performance servers ensure your site is fast, reliable & worry free.  A true hosting platform with PHP/MySQL support & FTP access means you can upload slideshows, videos, & more depending on the plan you choose.</p>
      </div>
    </div>
    <div class='row'>
      <div class='col-md-6'>
        <h3 class='cart'>Commerce + Proofing</h3>
        <p>
          Need a shopping cart solution to sell your images and products?  Our Ecommerce plan includes an integrated PROpx account that allows you to sell products directly within your website as a shopping cart solution on top of utilizing the normal PROpx features.
        </p>
        <p>
          <a href='http://propx.co' target='_blank'>Learn more about PROpx</a>
        </p>
      </div>
      <div class='col-md-6'>
        <h3 class='details'>And More ...</h3>
        <p>
          Review our feature list to see what plan fits your needs the best.
        </p>
        <p>
          <a href='#'>View Plans</a>
        </p>
      </div>
    </div>
  </div>
</div>

<BR /><BR />     
        
	</div>


<?php get_footer(); ?>