﻿<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package dazzling
 */


get_header(); ?>


        <div class="content-container">
            <div class="container-fluid landing">
        <div class=row>
            <div class="container">
                <div class="text-carusel">
                    <div>
                        <h1 style="background-color: rgba(0, 0, 0, 0.7); padding: 20px; height: auto;" class="text-carusel-h1 hidden-xs">Easily Create <br />
                            Your <u>FREE</u> Professional <br />
                            Photography Website
                        </h1>
                        <h2 style="background-color: rgba(0, 0, 0, 0.7); padding: 20px; height: auto;" class="text-carusel-h2 hidden-xs">
                            Stunning Photography Designs. Easy to Use. <br />
                            Trusted by Thousands of Professional Photographers
                        </h2>
                        <a href="/photography-website-templates/" class="text-carusel-link" style="text-shadow: 0px 2px 4px rgba(0, 0, 0, 0.7);">Get My FREE Website NOW!</a>
                    </div>
                </div>
            </div>
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <!-- <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                    <li data-target="#myCarousel" data-slide-to="3"></li>
                </ol> -->
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">

                    <div class="item active">
                        <img class="img img-responsive" src="wp-content/themes/dazzling/img/image-2-home-page.jpg" alt="">
                    </div>

                    <div class="item">
                        <img class="img img-responsive" src="wp-content/themes/dazzling/img/image-3-home-page.jpg" alt="">
                    </div>
                    
                     <div class="item">
                        <img class="img img-responsive" src="wp-content/themes/dazzling/img/bigfolio-home-image5.jpg" alt="">
                    </div>                   

                   
                </div>
                <!-- Left and right controls -->
                <!-- <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a> -->
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="landing-stage2">
                <h3 style="color:#F98D37;">Begin by Choosing Your Free Website Theme</h3>
                <p>With many categories to choose from, our large selection of professional photographer website themes is perfect for anyone who wishes to easily create their own website. Start by selecting a category below. 
                </p>
                <div id="index_categories" class="landing-stage2-photo">
                    <div class="items">
                        <a href="/general-photography-themes/">
                        <div class="items-img">
                            <img src="wp-content/themes/dazzling/img/landing-photo1.png" alt="">
                        </div></a>
                        <a href="/general-photography-themes/" class="items-text">General <span>Photography</span></a>
                    </div>
                    <div class="items">
                    	<a href="/wedding-photographer-website-templates/">
                        <div class="items-img">
                            <img src="wp-content/themes/dazzling/img/landing-photo2.png" alt="">
                        </div></a>
                        <a href="/wedding-photographer-website-templates/" class="items-text">Wedding <span>Photography</span></a>
                    </div>
                    <div class="items">
                    	<a href="/fashion-photography-themes/">
                        <div class="items-img">
                            <img src="wp-content/themes/dazzling/img/landing-photo3.png" alt="">
                        </div></a>
                        <a href="/fashion-photography-themes/" class="items-text">Fashion <span>Photography</span></a>
                    </div>
                    <div class="items">
                    	<a href="/children-photography-templates/">
                        <div class="items-img">
                            <img src="wp-content/themes/dazzling/img/landing-photo4.png" alt="">
                        </div></a>
                        <a href="/children-photography-templates/" class="items-text">Baby/Children <span>Photography</span></a>
                    </div>
                    <div class="items">
                    	<a href="/photography-website-templates/">
                        <div class="items-img">
                            <img src="wp-content/themes/dazzling/img/landing-photo5.png" alt="">
                        </div></a>
                        <a href="/photography-website-templates/" class="items-text">Popular <span>Photography</span></a>
                    </div>
                    <div class="items">
                    	<a href="/pet-photography-themes/"> 
                        <div class="items-img">
                           <img src="wp-content/themes/dazzling/img/landing-photo6.png" alt="">
                        </div></a>
                        <a href="/pet-photography-themes/" class="items-text">Pet <span>Photography</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid landing-stage3">
        <div class="row">
            <div class="container">
                <div class="row">
                    <h3>Famous Photographers and Creatives That Love BigFolio</h3>
                    <div class="landing-stage3-photo">
                        <div class="items">
                            <div class='items-img'>
                                <img src="wp-content/themes/dazzling/img/famouse-photo1.png" alt="">
                            </div>
                            <p class="name">Elizabeth Messina</p>
                            <p>www.elizabethmessina.com</p>
                        </div>
                        <div class="items">
                            <div class="items-img">
                                <img src="wp-content/themes/dazzling/img/famouse-photo2.png" alt="">
                            </div>
                            <p class="name">Carrie Sandoval</p>
                            <p>www.capturedbycarrie.com</p>
                        </div>
                        
                        <div class="items">
                            <div class="items-img">
                                <img src="wp-content/themes/dazzling/img/famouse-photo3.png" alt="">
                            </div>
                            <p class="name">Virgil Bunao</p>
                            <p>www.virgilbunao.com</p>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="landing-stage2">
                <h3>Why 1000's of Professional Photographers Trust Big Folio</h3>
                <p>With many categories to choose from, our large selection of professional photographer website themes is perfect for anyone who wishes
                    to easily create their website. Each one of them easily customizable to suit any professional's needs.
                </p>
                <div class="landing-stage2-photo">
                    <div class="items-2">
                        <div class="items-2-icon">
                            <img src="wp-content/themes/dazzling/img/land-icon1.png" alt="">
                        </div>
                        <div class="content">
                            <p class="header">Stunning Templates</p>
                            <p class="text">
                                Choose from our amazing collection of photography templates. Our large selection of professional photographer website themes is perfect for anyone who wishes to easily create their own website.
                            </p>
                        </div>
                    </div>
                    <div class="items-2">
                        <div class="items-2-icon">
                            <img src="wp-content/themes/dazzling/img/land-icon2.jpg" alt="">
                        </div>
                        <div class="content">
                            <p class="header">Simple Website Builder</p>
                            <p class="text">
                                Our platform allows you the control menu placement, thumbnail placement, gallery layouts, font options, color choices, music choice, social media integration and so much more.
                            </p>
                        </div>
                    </div>
                    <div class="items-2">
                        <div class="items-2-icon">
                            <img src="wp-content/themes/dazzling/img/land-icon3.jpg" alt="">
                        </div>
                        <div class="content">
                            <p class="header">Secure / Reliable Hosting</p>
                            <p class="text">
                                High-performance servers ensure your website is fast, reliable & worry free. A true web hosting platform with FTP access which means you can upload slideshows, videos, & much more.
                            </p>
                        </div>
                    </div>
                    <div class="items-2">
                        <div class="items-2-icon">
                            <img src="wp-content/themes/dazzling/img/land-icon4.jpg" alt="">
                            <div class="content">
                                <p class="header">Sell Your Photos Online</p>
                                <p class="text">
                                    Bigfolio allows you to sell prints and digital negatives for weddings, events, portraits, stock photography fine art, and much more.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="items-2">
                        <div class="items-2-icon">
                            <img src="wp-content/themes/dazzling/img/land-icon5.png" alt="">
                        </div>
                        <div class="content">
                            <p class="header">Mobile Responsive</p>
                            <p class="text">
                                Our website builder creates beautiful mobile designs. Mobile versions of your websites are automatically included for viewing on all mobile devices.
                            </p>
                        </div>
                    </div>
                    <div class="items-2">
                        <div class="items-2-icon">
                            <img src="wp-content/themes/dazzling/img/land-icon6.jpg" alt="">
                        </div>
                        <div class="content">
                            <p class="header">SEO Magic</p>
                            <p class="text">
                                Search engines are keen on Bigfolio websites. Our sites are designed to rank higher, look great, and boost your sales. Take advantage of the advanced SEO options within your platform & easily update important ranking factors without any HTML knowledge.
                            </p>
                        </div>
                    </div>
                    <div class="items-2">
                        <div class="items-2-icon">
                            <img src="wp-content/themes/dazzling/img/land-icon7.jpg" alt="">
                        </div>
                        <div class="content">
                            <p class="header">Word Press Installation</p>
                            <p class="text">
                                Ready to blog? We install a free WordPress blog when we setup your new professional or ecommerce site. Choose to use the default theme provided, or upload themes from other third party providers to make your blog fit your style better.
                            </p>
                        </div>
                    </div>
                    <div class="items-2">
                        <div class="items-2-icon">
                            <img src="wp-content/themes/dazzling/img/land-icon8.png" alt="">
                        </div>
                        <div class="content">
                            <p class="header">Dedicated Support</p>
                            <p class="text">
                                Our dedicated team has been providing the best support in the industry for over 10 years. Simply put, we are experts at what we do.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid landing-stage4">
        <div class="row">
            <div class="container">
                <div class="row">
                    <h4>Start By Selecting A Template</h4>
                    <p> With no credit card required, Bigfolio makes it easy for you to design a professional photography website
in just minutes.  Just select a category, pick a template and add your text and images.  It's that easy!  
 
                    </p>
                    <a class="btn-creat" href="/photography-website-templates/">
                        Create Your Free Website Now
                        <span class="btn-creat-icon">

                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="landing-stage2">
                <h4>What Customers Are Saying About BigFolio</h4>
                <div class="landing-stage2-review">
                    <div class="img">
                        <img src="wp-content/themes/dazzling/img/landing-review4.png" alt="">
                    </div>
                    <div class="text">
                        <h5>"The perfect way to showcase and sell my work"</h5>
                        <p class="name">Jamie Bott</p>                 
                        <p>http://www.jamiebottweddingphotography.com</p>
                    </div>
                    <!--
                    <div class="img">
                        <img src="wp-content/themes/dazzling/img/landing-review1.png" alt="">
                    </div>
                    <div class="text">
                        <h5>"What I love most about them is their customer service"</h5>
                        <p class="name">Lisa Boggs</p>                 
                        <p>http://www.lisaboggsphotography.com</p>
                    </div>
                    -->
                    <div class="img">
                        <img src="wp-content/themes/dazzling/img/landing-review2.png" alt="">
                    </div>
                    <div class="text">
                        <h5>"I'm especially happy with the boost in traffic"</h5>
                        <p class="name">Alan Blakely</p>                 
                        <p>http://www.blakelyphotography.com</p>
                    </div>
                                                         
                    
                    <div class="img">
                        <img src="wp-content/themes/dazzling/img/landing-review3.png" alt="">
                    </div>
                    <div class="text">
                        <h5>"Designs are simple and contemporary"</h5>
                        <p class="name">Ryan Graham</p>                 
                        <p>http://www.grahamryanphotography.com</p>
                    </div>                                                          
                    
                </div>
            </div>
        </div>
    </div>        </div>
        
	


<?php get_footer(); ?>
