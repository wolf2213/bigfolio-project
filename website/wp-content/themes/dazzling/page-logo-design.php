<?php
/**
 * The samples template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package dazzling
 */

get_header(); ?>




    <!-- Masthead image -->
    <div class='page-header' id='logos' style="height:400px;">
  <div class='container'>
    <h2>Logo Design Service</h2>
    <h3>Make Your Brand Stand Apart From The Rest</h3>
  </div>
</div>
<div class='page-body'>
  <div class='container'>
    <h2 style="color:#F98D37;">Your Business Needs a Strong Brand</h2>
    <p>
      A strong brand is essential to optimizing your business and online presence. Having recently launched our new Big Folio logo, we wanted to extend this top-quality service to our valued clients.
    </p>
    <p>
      Our award-winning designer has 20 years experience in branding, marketing and design. No matter what your vision, we can bring it to life.
    </p>
    <h3 style="color:#F98D37;">Logo Design Details</h3>
   
    <p>
      To get started on your new logo or if you have questions, please contact us at
      <a href='info@bigfolio.com'>info@bigfolio.com</a>
    </p>
    <h4>
      LEVEL 1 -- Basic Plan - $69
    </h4>
    <p>2 Design Concepts, 2 Designers, FREE Multiple File Format, FREE Multiple Color Option, Grey Scale Format, Unlimited Revisions, Delivery 2 Days, 100% Money Back Guarantee, 100% Satisfaction Guarantee, 100% Ownership Rights, $50 for 24 Hour Rush Delivery</p>
    <h4>
      LEVEL 2 -- Basic Plus - $89
    </h4>
    <p>4 Design Concepts, 2 Designers, Unlimited Revisions, FREE Multiple Color Option, FREE Multiple File Format, Stationery Concept, Business Card, Letterhead, Envelope, Delivery 2 Days, 100% Money Back Guarantee, 100% Satisfaction Guarantee, 100% Ownership Rights, $50 for 24 Hour Rush Delivery</p>
    <h4>
      LEVEL 3 -- UNLIMITED - $129  "MOST POPULAR"
    </h4>
    <p>Unlimited Design Concepts, 8 Designers, FREE Multiple File Format, FREE Multiple Color Option, Stationery Concept, Business Card, Letterhead, Envelope, Unlimited Revisions, Delivery 2 Days, 100% Money Back Guarantee, 100% Satisfaction Guarantee, 100% Ownership Rights, $50 for 24 Hour Rush Delivery</p>
    <p></p>
    <p></p>
    <BR />
    <h4 align="center">Logo Design Examples</h4>
  
 <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-1.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-1.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-2.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-2.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-3.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-3.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-4.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-4.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-5.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-5.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-6.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-6.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-7.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-7.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-8.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-8.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-9.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-9.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-10.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-10.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-11.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-11.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-12.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-12.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-13.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-13.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-14.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-14.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-15.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-15.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-16.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-16.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-17.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-17.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-18.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-18.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-19.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-19.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-20.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-20.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-21.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-21.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-22.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-22.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-23.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-23.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-24.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-24.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-25.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-25.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-26.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-26.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-27.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-27.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-28.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-28.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-29.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-29.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-30.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-30.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-31.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-31.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4 portfolio1 logo" data-cat="logo">
                            <div class="item">
                                <a href="/wp-content/themes/dazzling/images/portfolio/logos/Logo-32.jpg"
                                   class="fresco expand img"
                                   data-fresco-group='g1'
                                   data-fresco-group-options="overflow: 'y', thumbnails: 'vertical'">
                                    <figure class="effect-oscar">
                                        <img class="img-responsive" src="/wp-content/themes/dazzling/images/portfolio/logos/Logo-32.jpg" alt="">
                                        <figcaption class="bg-1">
                                            <h2><i class="fa fa-eye"></i></h2>

                                        </figcaption>
                                    </figure>
                                </a>
                            </div>
                        </div> 
    
    
 </div>

 
        



<?php get_footer(); ?>