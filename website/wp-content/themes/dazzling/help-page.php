<?php
/*
Template Name: Help Page
*/
?>
<?php get_header(); ?> 
<div class="help_page"> 
<div class='container'>
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <div class='page-header' style="padding-bottom: 9px; margin: 40px 0px 20px; border-bottom: 1px solid #EEE; padding-top: 50px; height:100px;">
  	<h1><?php the_title(); ?></h1>
  </div>
  <?php endwhile; endif; ?>
  <div class='row-fluid'>
	
	
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<div class='col-sm-7 help_post'>
		<?php if (get_post_meta($post->ID, 'getting_started', true) == '1') { ?>
		<!--
		  <div class="grid_4"><h3><a class="btn btn-large btn-gold" href="<?php bloginfo('url'); ?>/help/getting-started/step-1-build-your-site/">Step 1: Build Your Site</a></h3></div>
		  <div class="grid_4"><h3><a class="btn btn-large btn-gold" href="<?php bloginfo('url'); ?>/help/getting-started/step-2-setup-your-email/">Step 2: Setup Email</a></h3></div>
		  <div class="grid_4"><h3><a class="btn btn-large btn-gold" href="<?php bloginfo('url'); ?>/help/getting-started/step-3-make-your-site-live/">Step 3: Go Live</a></h3></div>
		
		-->
        <?php } ?>
		<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
		
		<?php if (get_post_meta($post->ID, 'show_buttons', true) == '1') { ?> </p>
        <div id="help_buttons">
        <!--
          <div>
            <h3><a href="<?php bloginfo('url'); ?>/help/getting-started/">Getting Started</a></h3>
            <p>Step-by-step guide to working with your new BIG Folio website</p>
          </div>
          -->
          <div>
            <h3><a href="<?php bloginfo('url'); ?>/help/design-specifications-2">Design Specifications</a></h3>
            <p>Get details on working with your design, sizing images, and using special features</p>
          </div>
          <div>
            <h3><a href="<?php bloginfo('url'); ?>/help/articles-faqs">Articles &amp; FAQs</a></h3>
            <p>Answers to questions and in-depths guides</p>
          </div>
          <div>
            <h3><a href="<?php bloginfo('url'); ?>/help/email-seo-statistics">Email, SEO &amp; Statistics</a></h3>
            <p>Working with Google Apps, Analytics, and SEO</p>
          </div>
          <div>
            <h3><a href="<?php bloginfo('url'); ?>/help/training-videos">Training Videos</a></h3>
            <p>Watch step by step training videos on how to design your BIG Folio website.</p>
          </div>
        </div>
		<a class="btn btn-large" href="http://support.bigfolio.com/anonymous_requests/new">Submit a support ticket &raquo;</a>
        <?php } ?>

		<? if (get_post_meta($post->ID, 'list_designs', true) == '1') { ?>
			<?php $designs = get_pages('child_of=3437&exclude=2054&sort_column=post_date&sort_order=DESC'); ?>
			<?php $colCount = 0; ?>
			<?php foreach ($designs as $d) { ?>
				<div class='col-sm-4 designSpecsIndexThumb'>
					<?php
						$content = apply_filters( 'the_content', $d->post_content );
						$contentArr = explode('src="http://bigfolio.com/', $content);
						$imgFile = explode('"', $contentArr[1]);
					?>
					<?php if ($imgFile[0] != "") { ?>
					<div class="designSpecsIndexImage">
						<a href="<?php echo get_page_link( $d->ID ); ?>">
							<img src="http://bigfolio.com/<?=$imgFile[0];?>" alt="" />
						</a>
					</div>
					<?php } ?>
					<a href="<?php echo get_page_link( $d->ID ); ?>" class="designSpecIndexLink"><?php echo $d->post_title ?></a>
				</div>
				<?php $colCount++; ?>
				<?php if ($colCount >= 3) { ?>
					<?php $colCount=0; ?>
					<div style="clear:both;"></div>
				<?php } ?>
			<?php } ?>
			
		<? } ?>
	</div>
	<?php endwhile; endif; ?>
	
	<div class='col-sm-1'></div>
	
	<div class='col-sm-4 help_index'>
	  <?php //$pg = get_pages('include=28'); ?>
	  <h2><a href="<?php bloginfo('url'); ?>/help/" class="designSpecIndexLink">Help &amp; Support</a></h2>
  	  <? include('searchform.php') ?>
	  <ul id="help_nav">
	    <?php wp_list_pages('title_li=&child_of=2830&show_date=modified&date_format=&exclude=3439&'); ?>
		<li class="old_designs"><a href="<?php bloginfo('url'); ?>/help/old-design-specs/">Old Design Specifications</a></li>
	  </ul>
	  <a class="btn" href="http://support.bigfolio.com/anonymous_requests/new">Submit a Support Ticket &raquo;</a>
	</div>
	
  </div>
</div>
</div>
<BR /><BR />
<?php get_footer(); ?> 
