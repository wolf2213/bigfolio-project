<?
// Includes
include('db.inc.php');
include('functions.inc.php');
include('classTextile.php');

header("Content-type: application/json"); 

$s = get_settings();

// Open the extra directory and look for intro images
$dh  = opendir('../extra');
while (false !== ($filename = readdir($dh))) {
	if (strtolower(substr($filename,0-3)) == 'jpg') {
		$files[] = $filename;
	}
}
sort($files);
$s["extra"] = $files;

// Pages
$s["pages"] = get_sub_pages();

// Prep galleries
$s["galleries"] = array();
// Cats and gals
$cats = get_categories();
$s["categories"] = $cats;
foreach($cats as $c) {
  $gals = get_galleries_by_category($c['category_id']);
  foreach($gals as $g) {
    if(strlen($g["gallery_name"]) > 0) {
      $g["category_name"] = $c["category_name"];
      $g["images"] = get_gallery_files($g['gallery_id']);
      $s["galleries"][] = $g;  
    }
  }
}
// Video cats and gals
$vcats = get_vcategories();
$s["video_categories"] = $vcats;
$s["videos"] = array();
foreach($vcats as $c) {
  $gals = get_vgalleries_by_vcategory($c['category_id']);
  foreach($gals as $g) {
    // Skip if it's a redirect
    if ($g['gallery_redirect'] == '') {
      $g["category_name"] = $c["category_name"];
      $g["files"] = get_video_files($g['gallery_id']);
      $s["videos"][] = $g;
    }
  }
}

if (isset($_GET['callback'])) {
  echo $_GET['callback'] . '(' . json_encode($s) . ');';
} else {
  echo json_encode($s);
}
?>