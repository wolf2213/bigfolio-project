<?php
/**********************************************************
PayPal Sandbox Info
Main account: jobs@bigfolio.com / mazagine

Test account 1: (Seller) billing@bigfolio.com / billin_1189115293_biz@bigfolio.com / 189115272

Test account 2: (Buyer) customer@bigfolio.com / custom_1189116271_biz@bigfolio.com / 189116249

Test account 3: (Buyer) photographer@bigfolio.com / photog_1189118133_per@bigfolio.com / 189118118

**********************************************************/

// include functions
include('db.inc.php');
include('functions.inc.php');
// read the post from PayPal system and add 'cmd'
$req = 'cmd=_notify-validate';

foreach ($_POST as $key => $value) {
	$value = urlencode(stripslashes($value));
	$req .= "&$key=$value";
}
// post back to PayPal system to validate
$header .= "POST /cgi-bin/webscr HTTP/1.0\r\n";
$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
$header .= "Content-Length: " . strlen($req) . "\r\n\r\n";
$fp = fsockopen ('www.paypal.com', 80, $errno, $errstr, 30);

// assign posted variables to local variables
$item_name = $_POST['item_name'];
$item_number = $_POST['item_number'];
$payment_status = $_POST['payment_status'];
$payment_amount = $_POST['mc_gross'];
$payment_currency = $_POST['mc_currency'];
$txn_id = $_POST['txn_id'];
$receiver_email = $_POST['receiver_email'];
$payer_email = $_POST['payer_email'];


if (!$fp) {
	// HTTP ERROR
} else {
	fputs ($fp, $header . $req);
	while (!feof($fp)) {
		$res = fgets ($fp, 1024);
		if (strcmp ($res, "VERIFIED") == 0) {
			// success flag
			$success = true;
			$err_string = "";
			// check the payment_status is Completed
			if ($_POST['payment_status'] != "Completed") {
			    $success = false;
				$err_string .= "Payment Status was not COMPLETED\n";
			}
		} else if (strcmp ($res, "INVALID") == 0) {
			$success = false;
			// log for manual investigation
			// log the errors to a text file
			$err_string = "Validation POST returned INVALID value.";
			$tfp = fopen ("err.txt", "w");
			fputs($tfp, $err_string);
			fclose ($tfp);
		}
	}
	// process payment
	// if everything was successful, update the database and create
	// the user's BIG Show credits
	if ($success) {
		// Insert the PP transaction
		$tid = $_POST['txn_id'];
		$qty = $_POST['quantity'];
		$pdate = $_POST['payment_date'];
		$pg = $_POST['payment_gross'];
		$ps = $_POST['payment_status'];
		
		
		$test_string = "This email confirms the following new customer completed payment via PayPal\n";
		$test_string .= $_POST["payer_email"] . "\n";
		$test_string .= "AMOUNT: " . $_POST['payment_gross'] . "\n";
		$test_string .= "PAYPAL TRANSACTION ID: " . $_POST['txn_id'] . "\n";
		$test_string .= "DOMAIN: " . $_POST['custom'] . "\n";
		
		if ($_POST['payment_gross'] > 80) {
		  // Email big folio
		  $recip = "erik@bigfolio.com,jeff@bigfolio.com,charlie@bigfolio.com,mike@bigfolio.com";
		  mail($recip,'NEW CUSTOMER PAYMENT CONFIRMATION', $test_string);
		  // Email customer
		  $recip = $_POST['payer_email'];
		  $sub = "Welcome to BIG Folio";
		  $msg = file_get_contents("./autoreply.txt");
		  $headers = 'From: support@bigfolio.com' . "\r\n" .
		      'Reply-To: support@bigfolio.com' . "\r\n" .
		      'X-Mailer: PHP/' . phpversion();
		  mail($recip,$sub,$msg,$headers);
	  }
		
		
	} else {
		// Save all POST data in error string
		while (list($key, $val) = each($_POST)) {
			$err_string .= "$key: $val\n";
		}
		$tfp = fopen ("err.txt", "w");
		fputs($tfp, $res);
		fclose ($tfp);
	}
	fclose ($fp);
}
?>