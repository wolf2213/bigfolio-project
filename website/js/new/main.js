$(function() {

  if ($('#rubix-samples').length > 0) {
    var sampleView = new SampleView();
    var scrollView = new ScrollView();
  }

  $('input[name="gallery_mode"]').click(function() {
    var modeVal = $(this).val();
    if (modeVal == "masonry") {
      $("#masonry_thumbnail_mode").show();
      $("#thumbnail_mode").hide();
    } else {
      $("#masonry_thumbnail_mode").hide();
      $("#thumbnail_mode").show();
    }
  })
  $('#layoutPreviewBtn').click(function() {
    var layoutPreviewURL = previewURL + "?previewMode=true&";
    $('input').each(function(i,v) {
      if ($(this).attr('name') != undefined && $(this).attr("checked") == "checked") {
        layoutPreviewURL += $(this).attr("name") + "=" + $(this).val() + "&";
      }
    });
    window.open(layoutPreviewURL.substr(0,layoutPreviewURL.length-1), 'layoutPreview');
  });
  $("#rubixPreview").click(function() {
    $(this).hide();
  })
  setInterval(function() {
    var curIndex = $('.rubixScreen').index($('.curRubixScreen'));
    var nextIndex = curIndex+1;
    if (nextIndex >= $('.rubixScreen').length) {
      nextIndex = 0;
    }
    //console.log(curIndex+', '+nextIndex);
    $('.rubixScreen').eq(curIndex).removeClass('curRubixScreen');
    $('.rubixScreen').eq(nextIndex).addClass('curRubixScreen');
  }, 5000);
  $("#showPreview").click(function(){
    //
    var iframeSrc  = "http://samples.bigfolio.com/rubix-demo/?previewMode=true";
    var galleryDeepLink = "#!/images/portfolio/weddings/1";
      $('input:checked').each(function() {
        if ($(this).val() == "masonry") {
          galleryDeepLink = "#!/images/portfolio/weddings/";
        }
        iframeSrc += "&"+$(this).attr("name")+"="+$(this).val();
        if ($(this).val() == "top") {
          iframeSrc += "&menu_alignment=center";
        }
      });
      iframeSrc += galleryDeepLink;
      
    $("#rubixPreviewFrame").attr("src", iframeSrc);
    $("#rubixPreview").show();
    return false;;
  });
  $(".preview").mouseenter(function () {
	var e;
	return e = $(".info", this), console.log(e), e.fadeIn()
  }).mouseleave(function () {
	var e;
	return e = $(".info", this), e.fadeOut()
  });
});