<?
require_once('/var/www/vhosts/mailer/class.phpmailer.php');
require_once('/var/www/vhosts/mailer/class.smtp.php');
require_once('/var/www/vhosts/mailer/smtp.php');

function is_valid_email($email) {
	if (preg_match("/[a-zA-Z0-9_-.+]+@[a-zA-Z0-9-]+.[a-zA-Z]+/", $email) > 0) {
    return true;
	}	else {
	  return false; 
	}
}

$s['email'] = 'mike@bigfolio.com,elaine@bigfolio.com';

// Build message string
$message = "";
$fields_string = "";
$numFields = 0;
while (list($key, $val) = each($_POST)) {
	if ($key != "onLoad") {
		if ($key != 'oid' && $key != 'retURL' && $key != 'submit') {
			$label = $key;
			if ($label == '00NE0000000b6db') { $label = 'message'; } 
			$message .= "$label:\n $val\n\n";
		}
		$fields_string .= $key.'='.$val.'&'; //for salesforce curl call
		$numFields++;
	}
}
rtrim($fields_string,'&');

// Send email if we're supposed to        
$emails = explode(',', $s['email']);
$sub = "Contact Form Submitted";

$mail             = new PHPMailer();
// SMTP Settings
$mail->IsSMTP(); // telling the class to use SMTP
$mail->Host       = "smtp.sendgrid.net";      // SMTP server
$mail->SMTPDebug  = 0;                        // enables SMTP debug information (for testing)
$mail->SMTPAuth   = true;                     // enable SMTP authentication
$mail->Host       = "smtp.sendgrid.net";      // sets the SMTP server
$mail->Port       = 25;                       // set the SMTP port 
$mail->Username   = $_SMTP_USER; // SMTP account username
$mail->Password   = $_SMTP_PASS; // SMTP account password

if (isset($_POST['email']) && is_valid_email($_POST['email'])) {
  $mail->SetFrom($_POST['email'], 'BIG Folio Contact Form');
} else if (isset($_POST['Email']) && is_valid_email($_POST['Email'])) {
  $mail->SetFrom($_POST['Email'], 'BIG Folio Contact Form');
} else {
  $mail->SetFrom('webmaster@bigfolio.com', 'BIG Folio Contact Form');
}


$mail->Subject    = "BIGFolio contact form submitted.";
$mail->AltBody    = $message;
$mail->MsgHTML(nl2br($message));

foreach ($emails as $e) {
  $mail->AddAddress($e);
}

// Send it ... 
$result = $mail->Send();

$url = "https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8";

// Now send to salesforce
$ch = curl_init();

//set the url, number of POST vars, POST data
curl_setopt($ch,CURLOPT_URL,$url);
curl_setopt($ch,CURLOPT_POST,$numFields);
curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);

//execute post
$result = curl_exec($ch);

//close connection
curl_close($ch);


header("Location: ".$_POST['retURL']);
?>