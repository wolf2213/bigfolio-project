// JavaScript Document
function loadTemplatePreview(img, title, isPremium, previewURL, commURL, id) {
	// Loads template data into templates.php via AHAH
	// Hide right col
	var updateDiv = document.getElementById('rightCol');
	updateDiv.style.display = 'none';
	// Start HTTP request
	if (window.XMLHttpRequest) { // Mozilla, Safari, ...
    	http_request = new XMLHttpRequest();
	} else if (window.ActiveXObject) { // IE
    	http_request = new ActiveXObject("Microsoft.XMLHTTP");
	}
	http_request.onreadystatechange = showTemplatePreview;
	var requestURL = 'templates.inc.php?id='+id+'&title='+title+'&img='+img+'&isPremium='+isPremium+'&url='+previewURL+'&url2='+commURL+'&';
	http_request.open('GET', requestURL, true);
	http_request.send(null);
}
function showTemplatePreview() {
	var updateDiv = document.getElementById('rightCol');
	if (http_request.readyState == 4) {
		if (http_request.status == 200) {
			updateDiv.innerHTML = http_request.responseText;
			// Do blind down effect
			new Effect.BlindDown(updateDiv);	
		} else {
			alert('There was a problem with the request.');
		}
	}
	scroll(0,0);
}