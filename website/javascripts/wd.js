function showAll() {
	
	$('#premium_templates').fadeIn('medium');
	$('#standard_templates').fadeIn('medium');
	$('#all_btn').show();
	$('#all_btn_over').hide();
	$('#prem_btn').show();
	$('#prem_btn_over').hide();
	$('#standard_btn').show();
	$('#standard_btn_over').hide();
	
}
function showPremium() {
	
	$('#premium_templates').fadeIn('medium');
	$('#standard_templates').fadeOut('fast');
	
	$('#all_btn').hide();
	$('#all_btn_over').show();
	
	$('#prem_btn').hide();
	$('#prem_btn_over').show();
	
	$('#standard_btn').show();
	$('#standard_btn_over').hide();
	
}
function showStandard() {
	
	$('#premium_templates').fadeOut('fast');
	$('#standard_templates').fadeIn('medium');
	
	$('#all_btn').hide();
	$('#all_btn_over').show();
	
	$('#prem_btn').show();
	$('#prem_btn_over').hide();
	
	$('#standard_btn').hide();
	$('#standard_btn_over').show();
	

}