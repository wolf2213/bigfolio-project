// JavaScript Document
function doBanner() {
	var imgNum = Math.floor(Math.random()*21);
	var bann = document.images['bannerImage'];
	bann.src = 'images/banner'+imgNum+'.jpg';
	new Effect.BlindDown('banner');
}
function previewBIGShow (element) {
	// If we're IE on Win, just open a new window
	if (window.ActiveXObject) { // IE
		var theURL = 'http://bigshow.bigfolio.com/?s=28&t=851c7f3f37dcaae6e289dcf6f81f5dc2';
		var winName = 'BS';
		var features = 'status=yes,resizable=yes,width=790,height=500';
    	window.open(theURL,winName,features);
	} else {
		var ifcode = '<p style="text-align:right;margin:0px;padding:3px;"><a href="javascript:;" onclick="closeBIGShow(\'bigshow\')"><img src="images/window_close.gif" alt="Close preview" border="0" /></a></p>';
		var ifcode = ifcode + '<iframe frameborder="0" width="760" height="450" scrolling="no" src="http://bigshow.bigfolio.com/?s=28&t=851c7f3f37dcaae6e289dcf6f81f5dc2"></iframe>';
		var bsDiv = document.getElementById(element);
		bsDiv.innerHTML = ifcode;
		element = $(element);
		new Effect.Appear(element);
	}
}
function closeBIGShow(element) {
	var bsDiv = document.getElementById(element);
	bsDiv.innerHTML = '&nbsp;';
	element = $(element);
    new Effect.BlindUp(element);	
}
function previewTemplate(name, title, sampleURL) {
	var tcode = '<h1><a href="javascript:closeTemplate()"><img src="images/window_close.gif" alt="Close preview" width="9" height="9" border="0" align="right" style="padding:3px" /></a>Preview Template - '+title+'</h1>';
  	var tcode = tcode + '<p><img src="images/screen_'+name+'.jpg" alt="BIG Folio Website Sample" name="sampleImg" /></p>';
	var tcode = tcode + '<p><a href="'+sampleURL+'" target="_blank">view live demo of this template</a></p>';
	var prev = document.getElementById('preview');
	if (prev.style.display == 'none') {
		prev.innerHTML = tcode;
		new Effect.Appear('preview');
	} else {
		new Effect.Shake('preview');
		prev.innerHTML = tcode;
	}
}
function closeTemplate() {
	element = 'preview';
	new Effect.Squish(element);
}