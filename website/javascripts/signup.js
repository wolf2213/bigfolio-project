function setTemplate() {
	var t = document.suform.template_sel.options[document.suform.template_sel.selectedIndex].value;
	var t_arr = t.split('|');
	var tName = t_arr[0];
	var tPremium = t_arr[1];
	var tPreview = t_arr[2];
	var blogPrice = $("blog_price");
	if (tPremium == 1) {
		blogPrice.innerHTML = "FREE";
		document.suform.add_blog.checked=true;
		if (getHostingPlan() < 2) {
			document.suform.plan[2].checked=true;
		} 
		document.suform.plan[0].disabled=true;
		document.suform.plan[1].disabled=true;
		$('standard').style.color = "#CCCCCC";
	} else {
		if (getHostingPlan() == 0) {
			blogPrice.innerHTML = "$4.79/month";
		} else {
			blogPrice.innerHTML = "$47.90/year";
		}
		document.suform.plan[0].disabled=false;
		document.suform.plan[1].disabled=false;
		$('standard').style.color = "#000000";
	}
	doSummary();
}
function previewTemplate() {
	var t = document.suform.template_sel.options[document.suform.template_sel.selectedIndex].value;
	var t_arr = t.split('|');
	var tPreview = t_arr[2];
	if (tPreview != 'NONE') {
		window.open(tPreview, 'preview', '');
	}	
}
function getHostingPlan() {
	var theone = -1;
	for (i=0;i<document.suform.plan.length;i++){
		if (document.suform.plan[i].checked){
			theone=i;
			break; //exist for loop, as target acquired.
		}
	}
	return theone;
}
function setHosting() {
	var blogPrice = $("blog_price");
	if (getHostingPlan() < 2) {
		if (getHostingPlan() == 0) {
			blogPrice.innerHTML = "$4.79/month";
		} else {
			blogPrice.innerHTML = "$47.90/year";
		}
	} else {
		document.suform.add_blog.checked=true;
		blogPrice.innerHTML = "FREE";
	}
	doSummary();
}
function doSummary() {
	// Initialize summary values
	var startFee = 0;
	var subFee = 0;
	var extrasFee = 0;
	// Hosting fees
	var p = document.suform.plan[getHostingPlan()].value;
	var p_arr = p.split('|');
	var type = p_arr[0];
	var frequency = p_arr[1];
	var hStartFee = p_arr[2];
	var hSubFee = p_arr[3];
	startFee += parseFloat(hStartFee);
	subFee += parseFloat(hSubFee);
	// Check flex pay
	if (document.suform.do_flex.checked && frequency == "monthly") {
		startFee = startFee/2;
		document.suform.is_flex.value = "1";
	}
	// Disable flex pay if annnual
	if (frequency == "yearly") {
		document.suform.do_flex.disabled = true;
		document.suform.do_flex.checked = false;
	} else {
		document.suform.do_flex.disabled = false;
	}
	// Check blog
	var blogSubFee = 0;
	if (type == "s" && document.suform.add_blog.checked) {
		if (frequency == 'monthly') {
			blogSubFee = 4.79;
		} else {
			blogSubFee = 47.90;
		}
	}
	subFee += blogSubFee;
	// Check seo 
	// if (document.suform.add_seo.checked) {
	// 	extrasFee += 499;
	// }
	// Check big show 
	if (document.suform.add_show.checked) {
		extrasFee += 99;
	}
	// Check textures
	if (document.suform.add_texture.checked) {
		extrasFee += 79;
	}
	// Check small folio 
	if (document.suform.add_smallfolio.checked) {
		extrasFee += 49;
	}
	// Check DVD
	if (document.suform.add_dvd.checked) {
		extrasFee += 84;
	}
	// Template info
	var t = document.suform.template_sel.options[document.suform.template_sel.selectedIndex].value;
	var t_arr = t.split('|');
	var tName = t_arr[0];
	var tPremium = t_arr[1];
	var tId = t_arr[3];
	// Set hidden values
	document.suform.freq.value = frequency;
	document.suform.setup.value = startFee.toFixed(2); 
	document.suform.sub.value = subFee.toFixed(2);
	document.suform.initial.value = (startFee + extrasFee + subFee).toFixed(2);
	document.suform.extras.value = extrasFee;
	document.suform.tname.value = tName;
	document.suform.premium.value = tPremium;
	document.suform.tid.value = tId;
	// Show summary 
	startFee += subFee;
	$("sum_start").innerHTML = "$"+(startFee + extrasFee).toFixed(2);
	$("sum_sub").innerHTML = "$"+subFee.toFixed(2);
	$("sum_freq1").innerHTML = frequency;
	$("sum_freq2").innerHTML = frequency;
	
	// Show Summary
	var sumDiv = $('summary');
	if (sumDiv.style.display == 'none') {
		Effect.BlindDown('summary');
	}
}
function updateSecond(field) {
	var domain = $('the_domain');
	domain.innerHTML = field.value + '/';
}