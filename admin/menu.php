<?
if ($_SESSION['server'] == 'bigshow') {
	
} else {
	$fvalues = unserialize($s['flash_vars']);
	$fvars = split(',',get_property('FLASH_VARS'));
	foreach($fvars as $var) {
		if ($var == 'texture_file') {
			$textureCheck = true;
		}
		if ($var == 'Texture_File_Name') {
			$textureCheck = true;
		}
		if ($var == 'texture_file_name') {
			$textureCheck = true;
		}
	}
	//
	// calendar check
	$template = get_property('template'); 
	$templateType = get_property('TEMPLATE_TYPE');
	if ($template != 'lapine' && $template != 'bandon' && $template != 'desperado' && $templateType != 'HTML5') {
		$showCalendar = true;
	} else {
		$showCalendar = false;
	}
	// 
	// video check
	//
	// call curl call to get contact image existence
	$postArr = array();
	$postArr['u'] = $_SESSION['user']['email'];
	$postArr['p'] = $_SESSION['user']['password'];
	$postArr['dir'] = 'videos';
	$url = SERVER . '_bfadmin/check_for_dir.php'; // 
	$videosDirExists = curl_post($url, $postArr, array());
	//  
	if ($videosDirExists == 1 && $template != 'desperado' && $templateType != "HTML5") {
		$result = mysql_list_tables($db_name) or die(mysql_error());
		while ($row = mysql_fetch_row($result)) {
			if($row[0]=='videos') {
				$hasVideoGalleries = true;
			}
		}
	}

	$template = get_property('template'); 
	if (file_exists("./updates/".$template."/_update.php")) {
		include("./updates/".$template."/_update.php");
		$curVersion = intval(get_property('RUBIX_VERSION'));
		if ($curVersion == "") { $curVersion = 1; }
		if ($update['version'] > $curVersion && $_SESSION['server'] != 'bigshow') {
			$showUpdate = true;	
		}
	}
}
?>
<div id="menu">
	<ul>
		
		<? if ($showUpdate) { ?>
		<li class="parent"><a href="#/update" class="main nodisable">Update <sup>new</sup></a>
		</li>
		<? } ?>
		
		<? if ($_SESSION['server'] != 'bigshow') { ?>
		<li class="parent showPlus"><a href="#/site-settings" class="main">Site Settings</a>
			<ul id="site-settings-sub">
				<li><a href="#/site-settings/profile" class="sub" id="profile">Profile</a></li>
				<li><a href="#/site-settings/browser-settings" class="sub" id="browser-settings">Browser Settings</a></li> 
				<? if (stristr($template,"rubix")) { ?>
				<li><a href="#/site-settings/layout-settings" class="sub" id="layout-settings">Layout Settings</a></li>	
				<? } ?>
				<li><a href="#/site-settings/template-settings" class="sub" id="template-settings">Template Settings</a></li>
				<li><a href="#/site-settings/title-and-logo" class="sub" id="title-and-logo">Title and Logo</a></li>
				<li><a href="#/site-settings/colors" class="sub" id="colors">Colors</a></li>
				<li><a href="#/site-settings/fonts" class="sub" id="fonts">Fonts</a></li>
				<? if ($textureCheck) { ?>
				<li><a href="#/site-settings/texture" class="sub" id="texture">Texture</a></li> 
				<? } ?>
				<li><a href="#/site-settings/social-media" class="sub" id="social-media">Social Media</a></li>
				<? if (get_property("MUSIC") != "0") { ?>
				<li><a href="#/site-settings/music" class="sub" id="music">Music</a></li>
				<? } ?>
			</ul>
		</li>
		<? } ?>
		
		<? if ($_SESSION['server'] != 'bigshow') { ?>
		<li class="parent showPlus"><a href="#/images" class="main">Images</a>
			<ul id="images-sub">
				<li><a href="#/images/home-intro" class="sub" id="home-intro">Home/Intro</a></li>
				<li><a href="#/images/galleries" class="sub" id="galleries">Galleries</a></li>
				<li><a href="#/images/image-bank" class="sub noSave" id="image-bank">Image Bank</a></li> 
			</ul>
		</li>
		<? } ?>
		
		<? if ($_SESSION['server'] != 'bigshow') { ?>
		<? if ($hasVideoGalleries) { ?>
		<li class="parent showPlus"><a href="#" class="main">Videos</a>
			<ul id="videos-sub">
				<li><a href="#/videos/video-galleries" class="sub" id="video-galleries">Galleries</a></li>
				<li><a href="#/videos/video-bank" class="sub noSave" id="video-bank">Video Bank</a></li> 
			</ul>
		</li>
		<? } ?>
		<? } ?>
		
		<? if ($_SESSION['server'] != 'bigshow') { ?>
		<li class="parent showPlus"><a href="#" class="main">Pages</a>
			<ul id="pages-sub">
				<li><a href="#/pages/pages" class="sub" id="pages">Pages</a></li>
				<li><a href="#/pages/contact-form" class="sub" id="contact-form">Contact Form</a></li> 
				<? if ($showCalendar) { ?>
				<li><a href="#/pages/calendar" class="sub" id="calendar">Calendar</a></li>
				<? } ?>
			</ul>
		</li>
		<? } ?>
		
		<?
		$id = "propx_api_key";
		$sel = mysql_query("SELECT * FROM `properties` WHERE `property_id` =  '".$id."' LIMIT 1");
		if(is_resource($sel) && mysql_num_rows($sel) > 0 && stristr($template,"rubix")){
		    $row = mysql_fetch_array($sel,MYSQL_ASSOC);
		    //echo "<!-- " . $id . " Id exists " . $row["property_id"] . ", " . $row["property_value"] . " -->";
			$showEcommerce = true;
		}
		else{
		    $showEcommerce = false;
		}
		?>
		
		<? if ($_SESSION['server'] != 'bigshow' && $showEcommerce) { ?>
		<li class="parent showPlus"><a href="#" class="main">Ecommerce <sup>beta</sup></a>
			<ul id="ecommerce-sub">
				<li><a href="#/ecommerce/settings" class="sub" id="settings">Settings</a></li>
				<li><a href="#/ecommerce/products" class="sub" id="products">Products</a></li> 
				<li><a href="#/ecommerce/proofing-galleries" class="sub" id="proofing-galleries">Proofing Galleries</a></li>
				<li><a href="#/ecommerce/orders" class="sub noSave" id="orders">Orders</a></li>
			</ul>
		</li>
		<? } ?>
		
		<? if ($_SESSION['server'] != 'bigshow') { ?>
		<li class="parent"><a href="#/entry-page-builder" class="main nodisable" id="entry-page_builder">Entry Page Builder</a></li> 
		<? } ?>
		
		<? if ($_SESSION['server'] == 'bigshow') { ?>
		<li class="parent showPlus"><a href="#/bigshow" class="main">BIG Show</a>
			<ul id="bigshow-sub">
				<li><a href="#/bigshow/bigshow-account-settings" class="sub" id="bigshow-account-settings">Account Settings</a></li>
				<li><a href="#/bigshow/bigshow-slideshow-settings" class="sub" id="bigshow-slideshow-settings">Slideshow Settings</a></li>
				<li><a href="#/bigshow/bigshow-slideshows" class="sub" id="bigshow-slideshows">Slideshows</a></li>
			</ul>
		</li>	
		<? } else if (stristr($_SESSION['user']['url'], 'bigfoliodemo') === FALSE) { ?> 
		<li class="parent"><a href="#/bigshow/bigshow-slideshows" class="main nodisable">BIG Show</a></li>
		<? } else { ?> 
		<li class="parent"><a href="#/bigshow" class="main nodisable">BIG Show</a></li>
		<? } ?> 
		
		<? if ($_SESSION['server'] != 'bigshow') { ?>
		<li class="parent showPlus"><a href="#/statistics" class="main">Statistics</a>
			<ul id="statistics-sub">
				<li><a href="#/statistics/summary" class="sub noSave" id="summary">Summary</a></li>
				<li><a href="#/statistics/stats-code" class="sub" id="stats-code">Stats Code</a></li> 
				<li><a href="#/statistics/messages/0" class="sub noSave" id="messages">Messages</a></li>
			</ul>
		</li>
		<? } ?>
		
		<? if ($_SESSION['server'] != 'bigshow') { ?>
		<li class="parent showPlus"><a href="#/advanced" class="main">Advanced</a>
			<ul id="advanced-sub">
				<li><a href="#/advanced/extra-files" class="sub noSave" id="extra-files">Extra Files</a></li>
				<li><a href="#/advanced/rebuild-thumbnails" class="sub noSave" id="rebuild-thumbnails">Rebuild Thumbnails</a></li> 
				<li><a href="#/advanced/source-templates" class="sub" id="source-templates">Source Templates</a></li>
				<li><a href="#/advanced/smugmug" class="sub noSave" id="smugmug">SmugMug Styling</a></li>
			</ul>
		</li>
		<? } ?>
		
		<li class="parent"><a href="#/help" class="main nodisable">Help</a></li>
	</ul>
</div>
