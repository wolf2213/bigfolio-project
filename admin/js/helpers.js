jQuery.log = function( text ){
    if( (window['console'] !== undefined) ){
        console.log( text );
    }
}

function cleanfile(url) {
	return url
			.replace(/^\s+|\s+$/g, "") // trim leading and trailing spaces		
			//.replace(/[_|\s]+/g, "-") // change all spaces and underscores to a hyphen
			.replace(/\s+/g, "-")
			.replace(/[^a-zA-Z0-9-._]+/g, "-") // remove all non-alphanumeric characters except the hyphen
			.replace(/[-]+/g, "-") // replace multiple instances of the hyphen with a single instance
			//.replace(/^-+|-+$/g, "") // trim leading and trailing hyphens				
			;
} 

function getNameAndExtension(file) {
	var arr = file.split('.'); 
	var name = ''; 
	for (var i=0; i<arr.length-1; i++) {
		name += arr[i]; 
		if (i<arr.length-2) {
			name += ".";
		}
	} 
	var nameArr = new Array(); 
	nameArr[0] = cleanfile(name);
	nameArr[1] = arr[arr.length-1];                
	return nameArr;
}

function charReplacements(str) {
	str = str.split("\n\r").join("<br>");
	str = str.split("\r").join("<br>");
	str = str.split("\n").join("<br>");
	return str;
} 

function editorCharReplacements(str) {
	str = str.split("<br>").join("\n");
	return str;
}

function rgb2hex(rgb) {
    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    function hex(x) {
        return ("0" + parseInt(x).toString(16)).slice(-2);
    }
    return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}