/* Author: Eric Bjella

*/

var settingsChanged = false; 
var paths;

$(document).ready(function() {
	// 
	$("#menu .main").click(function() {
		if ($(this).parent().find("ul").length > 0) {
			if ($(this).parent().find("ul").hasClass("isOpen")) {
				$(this).parent().find("ul").removeClass("isOpen"); 
				$(this).parent().removeClass("showMinus");
				$(this).parent().addClass("showPlus");
			} else {
				$(this).parent().find("ul").addClass("isOpen");
				$(this).parent().removeClass("showPlus");
				$(this).parent().addClass("showMinus");
			}
			$(this).parent().find("ul").slideToggle("fast"); 
		}
		//
		if (!$(this).hasClass("nodisable")) {
			return false;
		}
	});
	//
	var rootTitle = $.address.title();
	$.address.change(function(event) {
		//
		// check for changed settings not saved
		if (settingsChanged) {
			var save = confirm("You've made changes to your content without saving.\n\rTo save your changes, select OK.\n\rTo move on without saving, select Cancel.");
			if (save) {
				saveSection(function(){ handleContent(event.value); });
			} else {
				handleContent(event.value);
			}
		} else {
	    	handleContent(event.value);
		}
		//
		if (event.value != 'home' && event.value != '' && event.value != '/') {
			$.address.title(rootTitle + ' - ' + event.value.substr(1).split('/').join(' - ')); 
		} else {
			$.address.title(rootTitle) 
		}
		//$.address.tracker();
    });
	//
	handleUI();
	$(".save-preview").hide();
	$(".save-preview").fadeIn('fast');
	//
	$("#saveBtn").click(function() {
		saveSection();
	});
	$("#viewBtn").click(function() {
		window.open(previewURL, '_blank');
	});
	$("#logoutBtn").click(function() {
		location.href = "login.php?logout";
	});
});

function handleContent(v) { 
	//
	$("#menu ul ul li").removeClass("selected");
	//
	settingsChanged = false;
	paths = v.split("/");
	var mainMenuID = "#"+paths[1]+"-sub";
	if (!$(mainMenuID).hasClass("isOpen")) {
		$(mainMenuID).addClass("isOpen");  
		$(mainMenuID).parent().addClass("showMinus")
		$(mainMenuID).stop().slideToggle("fast");
	}
	//
	var file = "sections";
	saveRoute = "actions";
	if (paths[2] == undefined) {
		file += "/" + paths[1] + "/" + "index.php";
		saveRoute += "/" + paths[1] + "/" + "save.php";
	} else { 
		for (var i=1; i<paths.length; i++) {
			if (Number(paths[i].substr(0,1)).toString() == "NaN" && paths[i-1] != "source-templates") { 
				saveRoute += "/" + paths[i]; 
				if (paths[i].indexOf('image-bank') > -1) {
					file += '/image-bank.php?viewMode=' + paths[i].substr(paths[i].indexOf('/image-bank') + 12);
				} else if (paths[i].indexOf('video-bank') > -1) {
					file += '/video-bank.php?viewMode=' + paths[i].substr(paths[i].indexOf('/video-bank') + 12);
				} else {
					file += "/" + paths[i]; 
				}
			} else {  
				file += "/edit.php?val=" + paths[i]; 
				saveRoute += "/save.php"; //?base="+paths[i];
			}
		} 
		//if (Number(paths[paths.length-1].substr(0,1)).toString() == "NaN") {
		if (file.indexOf('.php') == -1) {
			file += ".php"; 
			saveRoute += ".save.php";              
		}	
		//$(".sub").removeClass("selected");
		$("#"+paths[2]).parent().addClass("selected"); /*css("fontWeight", "bold")*/;  
		if (paths[2].indexOf('image-bank') > -1) {
			$("#image-bank").parent().addClass("selected");
		} else if (paths[2].indexOf('video-bank') > -1) {
			$("#video-bank").parent().addClass("selected"); 
		}
	}
	//
	if ($(".selected").find('a').hasClass("noSave")) {
		$("#saveBtn").attr("disabled", "disabled");
		$("#saveBtn").fadeTo(0,.5);
	} else {
		$("#saveBtn").removeAttr("disabled"); 
		$("#saveBtn").fadeTo(0,1);
	}
	//
	showSpinner();
	$("#content").fadeOut('fast', function() {
		$.ajax({
		   type: "GET",
			url: file,
			data: "",
			success: function(content) {
				hideSpinner();
				var html = content;
				$("#content").html(html);
				handleUI();
				//$("#content").show();
				$("#content").delay(200).fadeIn('fast');
			}
		});
	});
	$.scrollTo(0, 'slow');
	// 
	$.fancybox.close(); 
}

var saveRoute;
function saveSection(callback) {
	showSave(); 
	
	if (saveRoute.indexOf('entry-page-builder') > -1) {
		saveEntry(callback);
		return;
	}
	
	var data = new Object();
	$('input').each(function() {
		if ($(this).attr('name') != undefined) {
			if (($(this).attr("type") != "radio" && $(this).attr("type") != "checkbox")
				|| ($(this).attr("checked") == "checked")) {
				//
				if ($(this).attr('name') != undefined) {
					//data += $(this).attr('name') + '=' + $(this).attr("value"); //$(this).val();
					data[$(this).attr('name')] = $(this).val().split('"').join('&quot;');//$(this).attr("value").split('"').join('&quot;');
				}
			}
		}
	});
	$('select').each(function() {
		if ($(this).attr('name') != undefined) {
			if (data != '') {
				//data += "&";
			}
			data[$(this).attr('name')] = $(this).val();
		}
	});
	
	//
	// do special check for social media 
	if ($("#socialMediaIcons").length > 0) { 
		var social_media_text = "";
		$("#socialMediaIcons").find(".socialItem").each(function() { 
			var img = $(this).find("img").attr("title"); 
			var url = $(this).find("input").val(); 
			if (url == "Enter URL") {
				url = "";
			}
			social_media_text += img + "," + url + "\n"; 
		});                                              
		$("#social_media").val(social_media_text);
	}
	
	$('textarea').each(function() {
		if ($(this).attr('name') != undefined) {
			if (data != '') {
				//data += "&";
			}
			data[$(this).attr('name')] = $(this).val(); //attr("value");
			//
			if ($(this).hasClass('bfMarkupText')) {
				if ($("#bfRedirect").css("display") != "none") {
					data['page_content'] = 'REDIRECT:'+$("#pageRedirect").val(); 
				} else if ($("#bfBlog").css("display") != "none") {
					data['page_content'] = 'BLOG:'+$("#blogURL").val(); 
				} else {
					var str = data[$(this).attr('name')];                    
					//str = str.split("\n\r").join("<br>");
					//str = str.split("\n").join("<br>"); 
					//str = str.split("\r").join("<br>");
					data[$(this).attr('name')] = str;
				}
			} 
			$.log($(this).attr('name')+': '+data[$(this).attr('name')]);
			//
		}
	});
    
	$.ajax({
		//this is the php file that processes the data
		url: saveRoute,	

		//POST method is used
		type: "POST",

		//pass the data			
		data: data,		

		//Do not cache the page
		cache: false,
           
		//success
		success: function (res) { 
			$.log('saveRoute: '+res);   		
			if (res.substr(0,7) == 'success') {					 
				settingsChanged = false;
			} else {
				alert('Sorry, unexpected error. Please try again later.');
			}
			if (callback) {callback();}
			hideSave();				
		}		
	});
}

function showSave() {
	$("#saveOverlay").fadeIn("fast");
}
function hideSave() {
	$("#saveOverlay").delay(1000).fadeOut("fast");	
}

function handleUI() {
	$("button").button();
	$(".uiButton").button();
	$(".uiButtonSet").buttonset();
	$(".bfToolTip").tooltip({
		track: true,
		delay: 0,
		showURL: false,
		showBody: " - ",
		fade: 250,
		top: 30,
		left: -80
	});
	$(".sortable").sortable();
	$(".bfSlider").each(function() {
		var id = $(this).attr("id");
		var range = $(this).attr("title"); //id.substr(7); // after "slider_"
		var ranges = range.split(",");
		$(this).slider({
			min:Number(ranges[0]),
			max:Number(ranges[1]),
			value:Number(ranges[2]),
			
			slide: function(event, ui) {
				var val = $(this).slider( "value");
				if ($(this).slider("option", "max") - $(this).slider("option", "min") > 100 && $(this).slider("option", "min") > 0) {
					val /= 10;
					val = Math.round(val) * 10;
				}
				$(this).parent().find('.bfSliderLabel').attr("value", val);
				settingsChanged = true;
				
			},
			
			change: function(event, ui) {
				var val = $(this).slider( "value");
				if ($(this).slider("option", "max") - $(this).slider("option", "min") > 100 && $(this).slider("option", "min") > 0) {
					val /= 10;
					val = Math.round(val) * 10;
				}
				$(this).parent().find('.bfSliderLabel').attr("value", val);
				settingsChanged = true;
				
			}
			
		});
		/*
		$(this).bind( "slidechange", function(event, ui) {
			$(this).parent().find('.bfSliderLabel').html($(this).slider( "value"));
		});
		*/
	});
	
	$(".bfSliderLabel").each(function() {
		$(this).change(function() {
			var val = $(this).attr("value");
			$(this).parent().find('.bfSlider').slider({
				value:val
			});
		});
	});
	
	$(".bfChangeListen").change(function() {
		settingsChanged = true;
	});
	
	$('.bfSwatch').each(function() {
		//
		var theSwatch = $(this);
		var theInput = $(this).parent().find("input");
		//
		$(this).ColorPicker({
			color: theInput.val(),
			onShow: function (colpkr) {
				$(colpkr).fadeIn('fast');
				return false;
			},
			onHide: function (colpkr) {
				$(colpkr).fadeOut('fast');
				return false;
			},
			onChange: function (hsb, hex, rgb) {
				theInput.val("#" + hex);
				theSwatch.css('backgroundColor', '#' + hex);
				settingsChanged = true;
			}
		});
	});
	$('.bfSwatchSmall').each(function(i,v) {
		//
		var theSwatch = $(this);
		var theInput = $(this).parent().find("input");
		var theColor = theSwatch.attr("title");
		if (theColor == "" || theColor == undefined) {
			//theColor = "#FFFFFF";
		}
		//
		$(this).ColorPicker({
			color: theColor.substr(1),
			onShow: function (colpkr) {
				$(colpkr).fadeIn('fast');
				return false;
			},
			onHide: function (colpkr) {
				$(colpkr).fadeOut('fast');
				return false;
			},
			onChange: function (hsb, hex, rgb) {
				var v = new Array();
				theSwatch.parent().find(".bfSwatchSmall").each(function() {
					v.push($(this).attr("title"));
				});
				theInput.val(v.join(', '));
				theSwatch.attr("title", "#"+hex);
				theSwatch.css('backgroundColor', '#' + hex);
				settingsChanged = true;
			}
		});
	});
	
	//
	// gallery & image bank fancybox
	$("a[rel=galImages]").fancybox({ 
		'padding'			: 30,
		'transitionIn'		: 'none',
		'transitionOut'		: 'none',
		'overlayShow'       : false,
		'titlePosition'     : 'over',
		'type'              : 'image'
	});
	
	//
	// video gallery & video bank fancybox 
	$("a[rel=galVideos]").fancybox({ 
		'padding'			: 30,
		'transitionIn'		: 'none',
		'transitionOut'		: 'none',
		'width'             : 640,
		'height'            : 480,
		'overlayShow'       : false,
		'scrollable'        : 'auto'
	});
	
	//
	// page markup preview
	$("a[rel=markupPreview]").fancybox({
		'padding'           : 30,
		'transitionIn'		: 'none',
		'transitionOut'		: 'none',
		'width'             : 640,
		'height'            : 480,
		'autoScale'         : false,
		'overlayShow'       : false,
		'autoDimensions'    : false
	});
	  
	
} 

function showHidden(sel) {
	$("#"+sel).slideDown("fast");
}

function closeHidden(sel) {
	$("#"+sel).slideUp("fast");
}

function showSpinner() {
	$("#loaderSpinner").fadeIn("fast");
}
function hideSpinner() {
	$("#loaderSpinner").fadeOut("fast");
}

   