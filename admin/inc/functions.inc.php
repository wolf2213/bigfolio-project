<?
include_once('classTextile.php');
include_once('define.php');

function authorize_user() {
	if (!isset($_SESSION['user']) || $_SESSION['is_signed_in'] !== true) {
		header("Location: ./login.php");
	}
}
function mark_update() {
	$ts = mktime();
	$q = "update settings set last_updated = '$ts' where settings_id = 1";
	$upd = mysql_query($q)
		or die(mysql_error());
	return true;
}
function get_property($prop,$trueval=true,$falseval=false) {
	$q = "select * from properties where property_id='$prop'";
	$sel = mysql_query($q)
		or die(mysql_error());
	$row = mysql_fetch_array($sel,MYSQL_ASSOC);
	if ($row['property_value'] == 1) {
		return $trueval;
	} else if ($row['property_value'] == 1) {
		return $falseval;
	} else {
		return $row['property_value'];
	}
}
function get_files ($dir, $type) {
	// Open the directory
	$dh  = opendir('../'.$dir);
	$files = array();
	while (false !== ($filename = readdir($dh))) {
		if (strtolower(substr($filename,0-strlen($type))) == strtolower($type)) {
			$files[] = $filename;
		}
	}
	sort($files);
	return $files;
}
function get_css($css) {
	$file = $_SESSION['user']['path'] . "styles/$css.css";
	$content = file_get_contents($file);
	return $content;
}
function save_css($css, $styles) {
	$filename = $_SESSION['user']['path'] . "styles/$css.css";
	$styles = stripslashes($styles);
	if (is_writable($filename)) {
		if (!$handle = fopen($filename, 'w')) {
			 return false;
		}
		// Write $somecontent to our opened file.
		if (fwrite($handle, $styles) === FALSE) {
		   return false;
		}
		return true;
		fclose($handle);
	} else return false;
}
function get_template($file) {
	$file = $_SESSION['user']['path'] . "inc/$file.tmp.html";
	$content = file_get_contents($file);
	return $content;
} 
/*
function thumbnail($image,$thumb,$size) {
	if (function_exists('imagecreatefromjpeg')) {
		$src_img = imagecreatefromjpeg($image);
    	$origw=imagesx($src_img);
    	$origh=imagesy($src_img);
    	if ($origw > $origh) {
            $new_h = $size;
            $diff = $origh/$new_h;
            $new_w = $origw/$diff;
        } else {
            $new_w = $size;
            $diff = $origw/$new_w;
            $new_h = $origh/$diff;
        }
		if ($size > 50) {
			$dst_img = imagecreatetruecolor($new_w, $new_h);
		} else {
    		$dst_img = imagecreatetruecolor($size,$size);
		}
    	imagecopyresampled($dst_img,$src_img,0,0,0,0,$new_w,$new_h,imagesx($src_img),imagesy($src_img));
   		imagejpeg($dst_img, $thumb, 75);
		imagedestroy($src_img);
		imagedestroy($dst_img);
	}
    return true;
}
*/
function fitimage($image,$thumb,$width,$height,$crop=false) {
	if (function_exists('imagecreatefromjpeg')) {
		if (ereg(".png",strtolower($image))) {
			$src_img = imagecreatefrompng($image);
		} else {
			$src_img = imagecreatefromjpeg($image);
		}

    	$origw=imagesx($src_img);
    	$origh=imagesy($src_img);

		$diff = $origw / $width;

		if ($origh / $diff > $height) {
			$new_h = $height;
			$diff = $origh / $height;
			$new_w = $origw / $diff;
		} else {
			$new_w = $width;
			$new_h = $origh / $diff;
		}


		if ($crop) {
			$new_w = $width*2;
			$new_h = $height*2;
			$dst_img = imagecreatetruecolor($width,$height);
		} else {
			$dst_img = imagecreatetruecolor($new_w, $new_h);
		}

		if (ereg(".png",strtolower($image))) {
			 imagealphablending($dst_img, false);
	   	}

    	imagecopyresampled($dst_img,$src_img,0,0,0,0,$new_w,$new_h,imagesx($src_img),imagesy($src_img));

		if (ereg(".png",strtolower($image))) {
			imagesavealpha($dst_img, true);
			imagepng($dst_img, $thumb, 9, NULL);
		} else {
   			imagejpeg($dst_img, $thumb, 82);
		}

   		//imagejpeg($dst_img, $thumb, 82);
		imagedestroy($src_img);
		imagedestroy($dst_img);
	}
    return true;
}
function fitimageold($image,$w,$h) {
	if (function_exists('imagecreatefromjpeg')) {
		// Get size of image
		$size = getimagesize($image);
		$old_w = $size[0];
		$old_h = $size[1];
		// Find lowest denominator
		// based on image being landscape or portrait
		if ($old_w<$old_h) {
			// Image is portrait
			$new_h = $h;
			$scale = $new_h/$old_h;
			$new_w = round($old_w*$scale);
		} else {
			// Image is landscape
			$new_w = $w;
			$scale = $new_w/$old_w;
			$new_h = round($old_h*$scale);
		}
		$src_img = imagecreatefromjpeg($image);
    	$dst_img = imagecreatetruecolor($new_w,$new_h);
    	imagecopyresampled($dst_img,$src_img,0,0,0,0,$new_w,$new_h,imagesx($src_img),imagesy($src_img));
   		imagejpeg($dst_img, $image,70);
		imagedestroy($src_img);
		imagedestroy($dst_img);
	}
    return true;
}
// Function for showing active tab (in row 2)
function is_active($m, $l) {
	if ($m == $l)
		return "class=\"active\"";
	else
		return "";
}
// Page functions
function get_settings() {
	$settingsID = 1;
	if ($_SESSION['server'] == 'bigshow') {
		$settingsID = $_SESSION['user']['id'];
	}
	$q = "select * from settings where settings_id = $settingsID";
	$sel = mysql_query($q)
		or die(mysql_error());
	$s = mysql_fetch_array($sel, MYSQL_ASSOC);
	// Textile the address
	$textile = new Textile;
	$s['street_address_html'] = $textile->TextileThis($s['street_address']);
	return $s;
}
function delete_page($pid) {
    // First, get the row we're deleting
    $q = "select * from pages where page_id = '$pid'";
	$sel = mysql_query($q)
		or die(mysql_error());
	$s = mysql_fetch_array($sel, MYSQL_ASSOC);
	$order = $s['order_num'];
	// Now, delete the page
	$q = "delete from pages where page_id = '$pid'";
	$sel = mysql_query($q)
		or die(mysql_error());
    // Now, reorder all pages below order
    $q = "update pages set order_num = (order_num-1) where order_num > '$order'";
	$sel = mysql_query($q)
		or die(mysql_error());
	// Now, delete any child pages
	$q = "delete from pages where parent_id = '$pid'";
	$del = mysql_query($q)
		or die(mysql_error());
    return true;
}
function convert_page_text($text) {
	// convert text
	$tags = array('<!-- noformat on -->', '<!-- noformat off -->');
	$status = 0;
	$lineBreaks = array("\n\r","\n","\r");
	while (!(($newpos = strpos($text, $tags[$status], $pos)) === FALSE))
	{
		$sub = substr($text, $pos, $newpos-$pos);

		if ($status)
			$newtext .= $sub;
		else
			$newtext .= str_replace($lineBreaks, "<br>", $sub);	

		$pos = $newpos;//+strlen($tags[$status]);

		$status = $status?0:1;
	}

	$sub = substr($text, $pos, strlen($text)-$pos);

	if ($status)
		$newtext .= $sub;
	else
		$newtext .= str_replace($lineBreaks, "<br>", $sub);	

	//To remove the tags
	//$newtext = str_replace($tags[0], "", $newtext);
	//$newtext = str_replace($tags[1], "", $newtext);
	//
	return $newtext;
}
function get_images_for_pages() {
    $r = array();
    if ($handle = opendir($_SESSION['user']['path'] . 'gallery')) {
        while (false !== ($file = readdir($handle))) {
            if (ereg(".jpg",strtolower($file)) or ereg(".jpeg",strtolower($file))) {
                $r[] = $file;
            }
        }
        closedir($handle);
    }
    return $r;
}
function get_gallery_images($gid) {
	$q = "select * from gallery_images where gallery_id = '$gid' order by order_num asc";
	$sel = mysql_query($q)
		or die(mysql_error());
	$r = array();
	while ($row = mysql_fetch_array($sel, MYSQL_ASSOC)) {
		$r[] = $row;
	}
	return $r;
}
function get_gallery_images_sorted($gid) {
	$q = "select * from gallery_images where gallery_id = '$gid' order by image_file asc";
	$sel = mysql_query($q)
		or die(mysql_error());
	$r = array();
	while ($row = mysql_fetch_array($sel, MYSQL_ASSOC)) {
		$r[] = $row;
	}
	return $r;
}
function delete_gallery_image($iid,$gid) {
    // First, get the row we're deleting
    $q = "select * from gallery_images where image_id = '$iid'";
	$sel = mysql_query($q)
		or die(mysql_error());
	$s = mysql_fetch_array($sel, MYSQL_ASSOC);
	$order = $s['order_num'];
	// Now, delete the page
	$q = "update gallery_images set gallery_id = '999999999' where image_id = '$iid'"; //"delete from gallery_images where image_id = '$iid'";
	$sel = mysql_query($q)
		or die(mysql_error());
    // Now, reorder all pages below order
    $q = "update gallery_images set order_num = (order_num-1) where order_num > '$order' and gallery_id = '$gid'";
	$sel = mysql_query($q)
		or die(mysql_error());
    return true;
}
function get_gallery($gid) {
	$q = "select g.*, c.category_name from galleries g, categories c
		  where g.gallery_id='$gid' and
		  g.category_id = c.category_id";
	$sel = mysql_query($q)
		or die(mysql_error());
	$row = mysql_fetch_array($sel,MYSQL_ASSOC);
	return $row;
}
function get_image_bank($sort='name',$dir='asc') {

	$directory = $_SESSION['user']['path'] . 'gallery/large';
	$r = array();

    if ($handle = opendir($directory)) {

        while (false !== ($file = readdir($handle))) {

            if (ereg(".swf",strtolower($file)) || ereg(".jpg",strtolower($file)) || ereg(".jpeg",strtolower($file)) || ereg(".png",strtolower($file))) {
               	///*
				$fsize = getimagesize($directory."/".$file);
				$image = array();
				// Index 0 is the file name
				$image[0] = $file;
				// Index 1 is the width
				$image[1] = $fsize[0];
				// Index 2 is the width
				$image[2] = $fsize[1];
				// Index 3 is the type
				$image[3] = 'jpg';
				if ($fsize[2] == 4 || $fsize[2] == 13)
					$image[3] = 'swf';
				// Index 4 is true if thumb file exists
				$image[4] = false;
				if ($image[3] == 'jpg' && file_exists($directory.'/../square/'.$file))
					$image[4] = true;
                if ($image[3] == 'swf' && file_exists($directory.'/../square/'.$file.'.jpg'))
					$image[4] = true;
				// Index 5 is the filesize in bytes
				$image[5] = filesize($directory.'/'.$file);
				// Index 6 is the file date modified timestamp
				$image[6] = filemtime($directory . '/' . $file);
				// Index 7 is the file date string
				//$image[7] = date('m/d/Y H:i', $image[6]);
				//*/
				$image = array();
				$image[0] = $file;
                $r[] = $image;

            }
        }

        closedir($handle);

    }

	if ($sort == 'name') {
		if ($dir == 'asc') {
			usort($r, 'ib_sort_name_asc');
		} else {
			usort($r, 'ib_sort_name_desc');
		}
	} else if ($sort == 'date') {
		if ($dir == 'asc') {
			usort($r, 'ib_sort_date_asc');
		} else {
			usort($r, 'ib_sort_date_desc');
		}
	}
    return $r;
}
function ib_sort_date_asc($a, $b) {
	if ($a[6] == $b[6]) {
    	return 0;
   	}
   	return ($a[6] < $b[6]) ? -1 : 1;
}
function ib_sort_date_desc($a, $b) {
	if ($a[6] == $b[6]) {
    	return 0;
   	}
   	return ($a[6] < $b[6]) ? 1 : -1;
}
function ib_sort_name_asc($a, $b) {
	if (strtolower($a[0]) == strtolower($b[0])) {
    	return 0;
   	}
   	return (strtolower($a[0]) < strtolower($b[0])) ? -1 : 1;
}
function ib_sort_name_desc($a, $b) {
	if (strtolower($a[0]) == strtolower($b[0])) {
    	return 0;
   	}
   	return (strtolower($a[0]) < strtolower($b[0])) ? 1 : -1;
}
function images_table($iarr,$cols) {
    $counter = 0;
    $r = "";
    foreach($iarr as $img) {
		$fileN = explode('.', $img[0]);
		$imageName = substr($fileN[0], 0, 12);
		if (strlen($img[0]) > 15) {
			$imageName .= '[...]';
		}
		$q = "select * from gallery_images where image_file = '$img[0]'";
		$sel = mysql_query($q)
			or die(mysql_error());
		$row = mysql_fetch_array($sel,MYSQL_ASSOC);
		$al = $row['align'];
		$al_id = $row['image_id'];
		$imageName .= '.' . $fileN[1];
		if ($al == 'left' || $al == '') {
			$selec = "<select onchange=\"updateAlign(this.options[this.selectedIndex].value,'$al_id')\" style=\"width: 140px;\"><option selected=\"selected\">Left</option><option>Right</option><option>Center</option></select>";
		} else if ($al == 'center') {
			$selec = "<select onchange=\"updateAlign(this.options[this.selectedIndex].value,'$al_id')\" style=\"width: 140px;\"><option>Left</option><option>Right</option><option selected=\"selected\">Center</option></select>";
		} else if ($al == 'right') {
				$selec = "<select onchange=\"updateAlign(this.options[this.selectedIndex].value,'$al_id')\" style=\"width: 140px;\"><option>Left</option><option selected=\"selected\">Right</option><option>Center</option></select>";
		} else {
				$selec = "<select onchange=\"updateAlign(this.options[this.selectedIndex].value,'$al_id')\" style=\"width: 140px;\"><option selected=\"selected\">Left</option><option>Right</option><option>Center</option></select>";
		}
		$r .= "<div class=\"thumbLayer\"><p style=\"margin: 6px 0 12px; height: 100px;\">";
		$r .= "<a href=\"display/popup/image_details.php?img=".$img[0]."\" class=\"lbOn\">".tag_image($img)."</a></p>";
		$r .= "<div style=\"display: block; width: 100%; text-align: center;\"><p style=\"float: left; margin-left: 8px;\"><strong style=\"margin-bottom: 10px; display: block;\">$imageName</strong></p>";
		$r .= "<div style=\"float:right; margin: -3px 8px 0 0;\"><input name=\"ibank[]\" type=\"checkbox\" id=\"ibank[]\" value=\"$img[0]\"  class=\"imageCheck\" /></div><div>$selec</div>";
		$r .= "</div></div>\n";
		$counter++;
	}
	return $r;
}
function images_table_large($iarr,$cols) {
    $counter = 0;
    $r = "";
    foreach($iarr as $img) {
		$q = "select * from gallery_images where image_file = '$img[0]'";
		$sel = mysql_query($q)
			or die(mysql_error());
		$row = mysql_fetch_array($sel,MYSQL_ASSOC);
		$al = $row['align'];
		$al_id = $row['image_id'];
		$imageName .= '.' . $fileN[1];
		if ($al == 'left') {
			$selec = "<select onchange=\"updateAlign(this.options[this.selectedIndex].value,'$al_id')\" style=\"width: 140px;\"><option selected=\"selected\">Left</option><option>Right</option><option>Center</option></select>";
		} else if ($al == 'center') {
			$selec = "<select onchange=\"updateAlign(this.options[this.selectedIndex].value,'$al_id')\" style=\"width: 140px;\"><option>Left</option><option>Right</option><option selected=\"selected\">Center</option></select>";
		} else if ($al == 'right') {
				$selec = "<select onchange=\"updateAlign(this.options[this.selectedIndex].value,'$al_id')\" style=\"width: 140px;\"><option>Left</option><option selected=\"selected\">Right</option><option>Center</option></select>";
		} else {
				$selec = "<select onchange=\"updateAlign(this.options[this.selectedIndex].value,'$al_id')\" style=\"width: 140px;\"><option selected=\"selected\">Left</option><option>Right</option><option>Center</option></select>";
		}
		$r .= "<div class=\"thumbLayerLarge\" style=\"position: relative;\"><p style=\"margin: 6px 0 12px; height: 100px;\">";
		$r .= "<a href=\"display/popup/image_details.php?img=".$img[0]."\" class=\"lbOn\">".tag_image_large($img)."</a></p>";
		$r .= "<div style=\"display: block; width: 100%; text-align: center; position: relative;\"><p style=\"float: left; margin-left: 8px; margin: 90px 0 0 14px;\"><strong style=\"font-size: 11px;\">$img[0]</strong></p>";
		$r .= "<div style=\"float:right; margin: 90px 16px 0 0;\"><input name=\"ibank[]\" type=\"checkbox\" id=\"ibank[]\" value=\"$img[0]\"  class=\"imageCheck\" /></div><div style=\"position: absolute; left: 14px; top: 110px;\">$selec</div></div></div>\n";
		$counter++;
	}
	return $r;
}
function images_table1($iarr,$cols) {
    $counter = 0;
    $r = "";
    foreach($iarr as $img) {
		$fileN = explode('.', $img[0]);
		$imageName = substr($fileN[0], 0, 12);
		if (strlen($img[0]) > 15) {
			$imageName .= '[...]';
		}
		$q = "select * from gallery_images where image_file = '$img[0]'";
		$sel = mysql_query($q)
			or die(mysql_error());
		$row = mysql_fetch_array($sel,MYSQL_ASSOC);
		$al = $row['align'];
		$al_id = $row['image_id'];
		$imageName .= '.' . $fileN[1];
		if ($al == 'left') {
			$selec = "<select onchange=\"updateAlign(this.options[this.selectedIndex].value,'$al_id')\" style=\"width: 140px;\"><option selected=\"selected\">Left</option><option>Right</option><option>Center</option></select>";
		} else if ($al == 'center') {
			$selec = "<select onchange=\"updateAlign(this.options[this.selectedIndex].value,'$al_id')\" style=\"width: 140px;\"><option>Left</option><option>Right</option><option selected=\"selected\">Center</option></select>";
		} else if ($al == 'right') {
				$selec = "<select onchange=\"updateAlign(this.options[this.selectedIndex].value,'$al_id')\" style=\"width: 140px;\"><option>Left</option><option selected=\"selected\">Right</option><option>Center</option></select>";
		} else {
			$selec = "<select onchange=\"updateAlign(this.options[this.selectedIndex].value,'$al_id')\" style=\"width: 140px;\"><option selected=\"selected\">Left</option><option>Right</option><option>Center</option></select>";
		}
		$r .= "<div class=\"thumbLayer\"><p style=\"margin: 6px 0 12px; height: 100px;\">";
		$r .= "<a href=\"display/popup/image_details.php?img=".$img[0]."\" class=\"lbOn\">".tag_image($img)."</a></p>";
		$r .= "<div style=\"display: block; width: 100%; text-align: center;\"><p style=\"float: left; margin-left: 8px;\"><strong style=\"margin-bottom: 10px; display: block;\">$imageName</strong></p>";
		$r .= "<div style=\"float:right; margin: -4px 10px 0 0;\"><input name=\"ibank[]\" type=\"checkbox\" id=\"ibank[]\" value=\"$img[0]\"  class=\"imageCheck\" /><input name=\"Img_$img[0]\" type=\"image\" onclick=\"return setDeleteBankImage('$img[0]')\" id=\"bankDelete\" src=\"images/t_delete_btn.gif\" /></div><div>$selec</div>";
		$r .= "</div></div>\n";
		$counter++;
	}
	return $r;
}
function original_path() {
	$path = TEMP_URL . $_SESSION['user']['db_user'] . '/gallery/original/';
	$c = explode("_",$path);
	if (strlen($c[1]) > 1) {
		$path = $c[0] . '/' . $c[1];
	}
	if ($_SESSION['user']['db_name'] == 'melissatomeoni_portrait') {
		$path = TEMP_URL . 'mtomeoni/portrait/gallery/original/';
	}
	if ($_SESSION['user']['db_name'] == 'laurenkaufmann_york2') {
		$path = TEMP_URL . 'lkaufmann/portraits/gallery/original/';
	}
	if ($_SESSION['user']['db_name'] == 'danielramos_pioneer') {
		$path = TEMP_URL . 'dramosyork/portraits/gallery/original/';
	}
	if ($_SESSION['user']['db_name'] == 'rebeccaknight') {
		$path = TEMP_URL . 'ryork/gallery/original/';
	}
	if ($_SESSION['user']['db_name'] == 'corinnealavekios_arch') {
		$path = TEMP_URL . 'calavekiosyork/gallery/original/';
	}
	if ($_SESSION['user']['db_name'] == 'alexhibbert_greer') {
		$path = 'http://photo.alexhibbert.com/gallery/original/';
	}
	if ($_SESSION['user']['db_name'] == 'rickaguilar') {
		$path = TEMP_URL . 'raguilar/gallery/orignal/';
	}
	if ($_SESSION['user']['db_name'] == 'dawnshields_atwood') {
		$path = 'http://dawnshields.com/family/gallery/original/';
	}
	if ($_SESSION['user']['db_name'] == 'hgamb_portraits') {
		$path = TEMP_URL . 'hgamb/portraits/gallery/original/';
	}
	if ($_SESSION['user']['db_name'] == 'hgamb_weddings') {
		$path = TEMP_URL . 'hgamb/weddings/gallery/original/';
	}
	if ($_SESSION['user']['db_name'] == 'hgamb_cherubs') {
		$path = TEMP_URL . 'hgamb/cherubs/gallery/original/';
	}
	if ($_SESSION['user']['db_name'] == 'williamvirun') {
		$path = TEMP_URL . 'williamqk/events/gallery/original/';
	}
	if ($_SESSION['user']['db_name'] == 'williamvirunfashion') {
		$path = TEMP_URL . 'williamqk/fashion/gallery/original/';
	}
	if ($_SESSION['user']['db_name'] == 'williamvirunglamour') {
		$path = TEMP_URL . 'williamqk/glamour/gallery/original/';
	}
	if ($_SESSION['user']['db_name'] == 'tarasielin') {
		$path = TEMP_URL . 'tarasiebl/weddings/gallery/original/';
	}
	if ($_SESSION['user']['db_name'] == 'tarasielin_portraits') {
		$path = TEMP_URL . 'tarasiebl/portraits/gallery/original/';
	}
	if ($_SESSION['user']['db_name'] == 'johnwarwo') {
		$path = TEMP_URL . 'johnwarwo/actors/gallery/original/';
	}
	if ($_SESSION['user']['db_name'] == 'juancargw') {
		$path = TEMP_URL . 'juancargw/portfolio/gallery/original/';
	}
	if ($_SESSION['user']['db_name'] == 'danieledge') {
		$path = TEMP_URL . 'danielmky/gallery/original/';
	}
	if ($_SESSION['user']['db_name'] == 'darcybacrnak') {
		$path = TEMP_URL . 'darcybacr/boudoir/gallery/original/';
	}
	if ($_SESSION['user']['db_name'] == 'gillianpe2') {
		$path = TEMP_URL . 'gillianer/gallery/original/';
	}
	if ($_SESSION['user']['db_name'] == 'carolinaph') {
		$path = TEMP_URL . 'gillianer/artistseries_backup/gallery/original/';
	}
	if ($_SESSION['user']['db_name'] == 'kimjamerfportraits') {
		$path = TEMP_URL . 'kimjamerf/portraits/gallery/original/';
	}
	if ($_SESSION['user']['db_name'] == 'amydrucrxpioneer') {
		$path = TEMP_URL . 'amydrucrx/fineart/gallery/original/';
	}
	if ($_SESSION['user']['db_name'] == 'peterpryf') {
		$path = 'http://weddings.peterprior.com/gallery/original/';
	}
	if ($_SESSION['user']['db_name'] == 'armandofe_seniors') {
		$path = TEMP_URL . 'armandofe/seniors/gallery/original/';
	} 
	if ($_SESSION['user']['db_name'] == 'armandofe_portraits') {
		$path = TEMP_URL . 'armandofe/portraits/gallery/original/';
	}
	if ($_SESSION['user']['db_name'] == 'rebeccaim_portraits') {
		$path = TEMP_URL . 'rebeccaim/portraits/gallery/original/';
	}
	if ($_SESSION['user']['db_name'] == 'jordanfqo') {
		$path = TEMP_URL . 'jordanfqo/portfolio/gallery/original/';
	}
	return $path;
}
function tag_image($img) {
	$path = TEMP_URL . $_SESSION['user']['db_user'] . '/gallery/thumb/';
	$c = explode("_",$path);
	if (strlen($c[1]) > 1) {
		$path = $c[0] . '/' . $c[1];
	}
	if ($_SESSION['user']['db_name'] == 'melissatomeoni_portrait') {
		$path = TEMP_URL . 'mtomeoni/portrait/gallery/thumb/';
	}
	if ($_SESSION['user']['db_name'] == 'laurenkaufmann_york2') {
		$path = TEMP_URL . 'lkaufmann/portraits/gallery/thumb/';
	}
	if ($_SESSION['user']['db_name'] == 'danielramos_pioneer') {
		$path = TEMP_URL . 'dramosyork/portraits/gallery/thumb/';
	}
	if ($_SESSION['user']['db_name'] == 'rebeccaknight') {
		$path = TEMP_URL . 'ryork/gallery/thumb/';
	}
	if ($_SESSION['user']['db_name'] == 'corinnealavekios_arch') {
		$path = TEMP_URL . 'calavekiosyork/gallery/thumb/';
	}
	if ($_SESSION['user']['db_name'] == 'alexhibbert_greer') {
		$path = 'http://photo.alexhibbert.com/gallery/thumb/';
	}
	if ($_SESSION['user']['db_name'] == 'rickaguilar') {
		$path = TEMP_URL . 'raguilar/gallery/thumb/';
	}
	if ($_SESSION['user']['db_name'] == 'hgamb_portraits') {
		$path = TEMP_URL . 'hgamb/portraits/gallery/thumb/';
	}
	if ($_SESSION['user']['db_name'] == 'hgamb_weddings') {
		$path = TEMP_URL . 'hgamb/weddings/gallery/thumb/';
	}
	if ($_SESSION['user']['db_name'] == 'hgamb_cherubs') {
		$path = TEMP_URL . 'hgamb/cherubs/gallery/thumb/';
	}
	if ($_SESSION['user']['db_name'] == 'dawnshields_atwood') {
		$path = 'http://dawnshields.com/family/gallery/thumb/';
	}
	if ($_SESSION['user']['db_name'] == 'williamvirun') {
		$path = TEMP_URL . 'williamqk/events/gallery/thumb/';
	}
	if ($_SESSION['user']['db_name'] == 'williamvirunfashion') {
		$path = TEMP_URL . 'williamqk/fashion/gallery/thumb/';
	}
	if ($_SESSION['user']['db_name'] == 'williamvirunglamour') {
		$path = TEMP_URL . 'williamqk/glamour/gallery/thumb/';
	}
	if ($_SESSION['user']['db_name'] == 'tarasielin') {
		$path = TEMP_URL . 'tarasiebl/weddings/gallery/thumb/';
	}
	if ($_SESSION['user']['db_name'] == 'tarasielin_portraits') {
		$path = TEMP_URL . 'tarasiebl/portraits/gallery/thumb/';
	}
	if ($_SESSION['user']['db_name'] == 'johnwarwo') {
		$path = TEMP_URL . 'johnwarwo/actors/gallery/thumb/';
	}
	if ($_SESSION['user']['db_name'] == 'juancargw') {
		$path = TEMP_URL . 'juancargw/portfolio/gallery/thumb/';
	}
	if ($_SESSION['user']['db_name'] == 'danieledge') {
		$path = TEMP_URL . 'danielmky/gallery/thumb/';
	}
	if ($_SESSION['user']['db_name'] == 'darcybacrnak') {
		$path = TEMP_URL . 'darcybacr/boudoir/gallery/thumb/';
	}
	if ($_SESSION['user']['db_name'] == 'gillianpe2') {
		$path = TEMP_URL . 'gillianer/gallery/thumb/';
	}
	if ($_SESSION['user']['db_name'] == 'carolinaph') {
		$path = TEMP_URL . 'gillianer/artistseries_backup/gallery/thumb/';
	}
	if ($_SESSION['user']['db_name'] == 'kimjamerfportraits') {
		$path = TEMP_URL . 'kimjamerf/portraits/gallery/thumb/';
	}
	if ($_SESSION['user']['db_name'] == 'amydrucrxpioneer') {
		$path = TEMP_URL . 'amydrucrx/fineart/gallery/thumb/';
	}
	if ($_SESSION['user']['db_name'] == 'peterpryf') {
		$path = 'http://weddings.peterprior.com/gallery/thumb/';
	}
	if ($_SESSION['user']['db_name'] == 'armandofe_seniors') {
		$path = TEMP_URL . 'armandofe/seniors/gallery/thumb/';
	}
	if ($_SESSION['user']['db_name'] == 'armandofe_portraits') {
		$path = TEMP_URL . 'armandofe/portraits/gallery/thumb/';
	}
	if ($_SESSION['user']['db_name'] == 'rebeccaim_portraits') {
		$path = TEMP_URL . 'rebeccaim/portraits/gallery/thumb/';
	}
	if ($_SESSION['user']['db_name'] == 'jordanfqo') {
		$path = TEMP_URL . 'jordanfqo/portfolio/gallery/thumb/';
	}
	if (is_array($img)) {
    	$filepath = $path.$img[0];
        $image_title = $img[0];
	} else {
		$filepath = $path.$img;
		$image_title = $img;
	}
    if ($img[3] == 'swf')
        $filepath .= '.jpg';
    if ($img[4] === false && $img[3] == 'jpg') {
        //$filepath = 'img/jpgthumb.jpg';
    } else if ($img[4] === false && $img[3] == 'swf') {
        $filepath = 'img/swfthumb.jpg';
    }
	$r = '';
	$r .= "<img src=\"".$filepath."\" title=\"".$image_title."\"border=\"0\" />";
	return $r;
}
function tag_image_large($img) {
	$path = TEMP_URL . $_SESSION['user']['db_user'] . '/gallery/small/';
	if ($_SESSION['user']['db_name'] == 'melissatomeoni_portrait') {
		$path = TEMP_URL . 'mtomeoni/portrait/gallery/small/';
	}
	if ($_SESSION['user']['db_name'] == 'laurenkaufmann_york2') {
		$path = TEMP_URL . 'lkaufmann/portraits/gallery/small/';
	}
	if ($_SESSION['user']['db_name'] == 'danielramos_pioneer') {
		$path = TEMP_URL . 'dramosyork/portraits/gallery/small/';
	}
	if ($_SESSION['user']['db_name'] == 'rebeccaknight') {
		$path = TEMP_URL . 'ryork/gallery/small/';
	}
	if ($_SESSION['user']['db_name'] == 'corinnealavekios_arch') {
		$path = TEMP_URL . 'calavekiosyork/gallery/small/';
	}
	if ($_SESSION['user']['db_name'] == 'alexhibbert_greer') {
		$path = 'http://photo.alexhibbert.com/gallery/small/';
	}
	if ($_SESSION['user']['db_name'] == 'rickaguilar') {
		$path = TEMP_URL . 'raguilar/gallery/small/';
	}
	if ($_SESSION['user']['db_name'] == 'hgamb_portraits') {
		$path = TEMP_URL . 'hgamb/portraits/gallery/small/';
	}
	if ($_SESSION['user']['db_name'] == 'hgamb_weddings') {
		$path = TEMP_URL . 'hgamb/weddings/gallery/small/';
	}
	if ($_SESSION['user']['db_name'] == 'hgamb_cherubs') {
		$path = TEMP_URL . 'hgamb/cherubs/gallery/small/';
	}
	if ($_SESSION['user']['db_name'] == 'dawnshields_atwood') {
		$path = 'http://dawnshields.com/family/gallery/small/';
	}
	if ($_SESSION['user']['db_name'] == 'williamvirun') {
		$path = TEMP_URL . 'williamqk/events/gallery/small/';
	}
	if ($_SESSION['user']['db_name'] == 'williamvirunfashion') {
		$path = TEMP_URL . 'williamqk/fashion/gallery/small/';
	}
	if ($_SESSION['user']['db_name'] == 'williamvirunglamour') {
		$path = TEMP_URL . 'williamqk/glamour/gallery/small/';
	}
	if ($_SESSION['user']['db_name'] == 'tarasielin') {
		$path = TEMP_URL . 'tarasielin/weddings/gallery/small/';
	}
	if ($_SESSION['user']['db_name'] == 'tarasielin_portraits') {
		$path = TEMP_URL . 'tarasielin/portraits/gallery/small/';
	}
	if ($_SESSION['user']['db_name'] == 'johnwarwo') {
		$path = TEMP_URL . 'johnwarwo/actors/gallery/small/';
	}
	if ($_SESSION['user']['db_name'] == 'juancargw') {
		$path = TEMP_URL . 'juancargw/portfolio/gallery/small/';
	}
	if ($_SESSION['user']['db_name'] == 'danieledge') {
		$path = TEMP_URL . 'danielmky/gallery/small/';
	}
	if ($_SESSION['user']['db_name'] == 'darcybacrnak') {
		$path = TEMP_URL . 'darcybacr/boudoir/gallery/small/';
	}
	if ($_SESSION['user']['db_name'] == 'gillianpe2') {
		$path = TEMP_URL . 'gillianer/gallery/small/';
	}
	if ($_SESSION['user']['db_name'] == 'carolinaph') {
		$path = TEMP_URL . 'gillianer/artistseries_backup/gallery/small/';
	}
	if ($_SESSION['user']['db_name'] == 'kimjamerfportraits') {
		$path = TEMP_URL . 'kimjamerf/portraits/gallery/small/';
	}
	if ($_SESSION['user']['db_name'] == 'amydrucrxpioneer') {
		$path = TEMP_URL . 'amydrucrx/fineart/gallery/small/';
	}
	if ($_SESSION['user']['db_name'] == 'peterpryf') {
		$path = 'http://weddings.peterprior.com/gallery/small/';
	}       
	if ($_SESSION['user']['db_name'] == 'armandofe_seniors') {
		$path = TEMP_URL . 'armandofe/seniors/gallery/small/';
	}  
	if ($_SESSION['user']['db_name'] == 'armandofe_portraits') {
		$path = TEMP_URL . 'armandofe/portraits/gallery/small/';
	}
	if ($_SESSION['user']['db_name'] == 'rebeccaim_portraits') {
		$path = TEMP_URL . 'rebeccaim/portraits/gallery/small/';
	}
	if ($_SESSION['user']['db_name'] == 'jordanfqo') {
		$path = TEMP_URL . 'jordanfqo/portfolio/gallery/small/';
	}
	$c = explode("_",$path);
	if (strlen($c[1]) > 1) {
		$path = $c[0] . '/' . $c[1];
	}
    $filepath = $path.$img[0];
    if ($img[3] == 'swf')
        $filepath .= '.jpg';
    if ($img[4] === false && $img[3] == 'jpg') {
        $filepath = 'img/jpgthumb.jpg';
    } else if ($img[4] === false && $img[3] == 'swf') {
        $filepath = 'img/swfthumb.jpg';
    }
	$r = '';
	$r .= "<img src=\"".$filepath."\" border=\"0\" title=\"".$img[0]."\"style=\"border: none; margin: 4px 0 0 9px;\" />";
	return $r;
}
function sub_folder_images_table1($iarr,$cols) {
    $q = "select * from folders order by order_num";
	$sel = mysql_query($q)
		or die(mysql_error());
	$fArr = array();
	while ($folder = mysql_fetch_array($sel, MYSQL_ASSOC)) {
		$fArr[] = $folder;
	}
	//
	$html = '';
	$loop = 0;
	$totalImages = 0;
	//
	foreach($fArr as $f) {
		$loop++;
		$counter = 0;
		foreach($iarr as $img) {
			$q = "select * from folder_images where file = '$img[0]'";
			$sel = mysql_query($q)
				or die(mysql_error());
			$row = mysql_fetch_array($sel,MYSQL_ASSOC);
			if ($row['folder_id'] == $f['id']) {
				$counter++;
			}
		}

		$html .= '<div class="fold" style="clear: both;" id="fold_'.$f['id'].'"><div style="position: relative; left: 10px;">';

		if ($counter > 0) {

			$html .= '<a href="javascript:;" onclick="toggle_folder_visibility(\'tLayer'.$loop.'\',\''.$loop.'\')"><img id="exp'.$loop.'" src="/images/expand_icon.gif" style="';

			if ($_SESSION['FOLDER_DISPLAY'] == $loop) {
				$html .= 'display: none; ';
			}

			$html .= 'float: left; position: relative; top: 10px; margin-right: 15px;" /><img id="col'.$loop.'" src="/images/collapse_icon.gif" style="float: left; ';

			if ($_SESSION['FOLDER_DISPLAY'] != $loop) {
				$html .= 'display: none; ';
			}

			$html .= 'position: relative; top: 10px; margin-right: 15px;" /></a>';

		}

		$html .= '<div style="float: left; ';

		if ($counter == 0) {
			$html .= 'margin-left: 30px;';
		}

		$html .= '"><img src="/images/folder_32.png"><span style="font: 14px \'Lucida Grande\',Arial,sans-serif; position: relative; top: -8px; left: 8px; color: #727272;"><span id="foldchange_'.$f['id'].'">' . $f['name'] . '</span>';

		if ($counter > 1) {
			$html .= ' (' . $counter . ' images)</span>';
		} else if ($counter == 0) {
			$html .= ' (empty)</span>';
		} else if ($counter == 1) {
			$html .= ' (' . $counter . ' image)</span>';
		}

		$html .= '<a href="javascript:;" onclick="renameFolder(\''.$f['id'].'\', \'' . $f['name'] . '\');" style="position: relative; top: -5px; left: 15px;"><img src="images/pencil.png" /></a><a href="javascript:;" onclick="deleteFolder(\''.$f['id'].'\');" style="position: relative; margin-left: 24px; top: -5px;"><img src="images/t_delete_btn.gif" /></a></div></div><div id="tLayer'.$loop.'" style="';

		if ($_SESSION['FOLDER_DISPLAY'] != $loop) {
			$html .= 'display: none; ';
		}

		$html .= 'clear: both;">';

		$counter = 0;
		foreach($iarr as $img) {
			$q = "select * from folder_images where file = '$img[0]'";
			$sel = mysql_query($q)
				or die(mysql_error());
			$row = mysql_fetch_array($sel,MYSQL_ASSOC);
			//
			if ($row['folder_id'] == $f['id']) {
				$fileN = explode('.', $img[0]);
				$imageName = substr($fileN[0], 0, 12);
				if (strlen($img[0]) > 15) {
					$imageName .= '[...]';
				}
				$al = $row['align'];
				$imageName .= '.' . $fileN[1];
				if ($al == 'left') {
					$selec = "<select onchange=\"updateAlign(this.options[this.selectedIndex].value,'$img[0]')\" style=\"width: 140px;\"><option selected=\"selected\">Left</option><option>Right</option><option>Center</option></select>";
				} else if ($al == 'center') {
					$selec = "<select onchange=\"updateAlign(this.options[this.selectedIndex].value,'$img[0]')\" style=\"width: 140px;\"><option>Left</option><option>Right</option><option selected=\"selected\">Center</option></select>";
				} else if ($al == 'right') {
						$selec = "<select onchange=\"updateAlign(this.options[this.selectedIndex].value,'$img[0]')\" style=\"width: 140px;\"><option>Left</option><option selected=\"selected\">Right</option><option>Center</option></select>";
				} else {
					$selec = "<select style=\"width: 140px;\" disabled><option>Disabled</option></select>";
				}
				$html .= "<div class=\"thumbLayer\"><p style=\"margin: 6px 0 12px; height: 100px;\">";
				$html .= "<a href=\"display/popup/image_details.php?img=".$img[0]."\" class=\"lbOn\">".tag_image($img)."</a></p>";
				$html .= "<div style=\"display: block; width: 100%; text-align: center;\"><p style=\"float: left; margin-left: 8px;\"><strong style=\"margin-bottom: 10px; display: block;\">$imageName</strong></p>";
				$html .= "<div style=\"float:right; margin: -4px 10px 0 0;\"><input name=\"ibank[]\" type=\"checkbox\" id=\"image$totalImages\" value=\"$img[0]\"  class=\"imageCheck\" onClick=\"checkShiftSelect(event)\" /><input name=\"Img_$img[0]\" type=\"image\" onclick=\"return setDeleteBankImage('$img[0]')\" id=\"bankDelete\" src=\"images/t_delete_btn.gif\" /></div><div>$selec</div>";
				$html .= "</div></div>\n";
				$counter++;
				$totalImages++;
			}
		}
		$html .= '</div></div>';
	}
	$counter = 0;
	foreach($iarr as $img) {
		$q = "select * from folder_images where file = '$img[0]'";
		$sel = mysql_query($q)
			or die(mysql_error());
		$row = mysql_fetch_array($sel,MYSQL_ASSOC);
		if ($row['folder_id'] <= 0) {
			$counter++;
		}
	}
	$loop = $loop + 1;
	$html .= '<div class="fold" style="clear: both;"><div style="position: relative; left: 10px;">';

	if ($counter > 0) {

		$html .= '<a href="javascript:;" onclick="toggle_folder_visibility(\'tLayer'.$loop.'\',\''.$loop.'\')"><img id="exp'.$loop.'" src="/images/expand_icon.gif" style="';

		if ($_SESSION['FOLDER_DISPLAY'] == $loop) {
			$html .= 'display: none; ';
		}

		$html .= 'float: left; position: relative; top: 10px; margin-right: 15px;" /><img id="col'.$loop.'" src="/images/collapse_icon.gif" style="float: left; ';

		if ($_SESSION['FOLDER_DISPLAY'] != $loop) {
			$html .= 'display: none; ';
		}

		$html .= 'position: relative; top: 10px; margin-right: 15px;" /></a>';

	}

	$html .= '<div style="float: left; ';

	if ($counter == 0) {
		$html .= 'margin-left: 30px;';
	}

	$html .= '"><img src="/images/folder_32.png"><span style="font: 14px \'Lucida Grande\',Arial,sans-serif; position: relative; top: -8px; left: 8px; color: #727272;"> Unassigned ';

	if ($counter > 1) {
		$html .= ' (' . $counter . ' images)</span>';
	} else if ($counter == 0) {
		$html .= ' (empty)</span>';
	} else if ($counter == 1) {
		$html .= ' (' . $counter . ' image)</span>';
	}

	$html .= '</div></div><div id="tLayer'.$loop.'" style="';

	if ($_SESSION['FOLDER_DISPLAY'] != $loop) {
		$html .= 'display: none; ';
	}

	$html .= 'clear: both;">';
	$counter = 0;
	foreach($iarr as $img) {
		$q = "select * from folder_images where file = '$img[0]'";
		$sel = mysql_query($q)
			or die(mysql_error());
		$row = mysql_fetch_array($sel,MYSQL_ASSOC);
		//
		if ($row['folder_id'] <= 0) {
			$fileN = explode('.', $img[0]);
			$imageName = substr($fileN[0], 0, 12);
			if (strlen($img[0]) > 15) {
				$imageName .= '[...]';
			}
			$al = $row['align'];
			$imageName .= '.' . $fileN[1];
			if ($al == 'left') {
				$selec = "<select onchange=\"updateAlign(this.options[this.selectedIndex].value,'$img[0]')\" style=\"width: 140px;\"><option selected=\"selected\">Left</option><option>Right</option><option>Center</option></select>";
			} else if ($al == 'center') {
				$selec = "<select onchange=\"updateAlign(this.options[this.selectedIndex].value,'$img[0]')\" style=\"width: 140px;\"><option>Left</option><option>Right</option><option selected=\"selected\">Center</option></select>";
			} else if ($al == 'right') {
					$selec = "<select onchange=\"updateAlign(this.options[this.selectedIndex].value,'$img[0]')\" style=\"width: 140px;\"><option>Left</option><option selected=\"selected\">Right</option><option>Center</option></select>";
			} else {
				$selec = "<select style=\"width: 140px;\" disabled><option>Disabled</option></select>";
			}
			$html .= "<div class=\"thumbLayer\"><p style=\"margin: 6px 0 12px; height: 100px;\">";
			$html .= "<a href=\"display/popup/image_details.php?img=".$img[0]."\" class=\"lbOn\">".tag_image($img)."</a></p>";
			$html .= "<div style=\"display: block; width: 100%; text-align: center;\"><p style=\"float: left; margin-left: 8px;\"><strong style=\"margin-bottom: 10px; display: block;\">$imageName</strong></p>";
			$html .= "<div style=\"float:right; margin: -4px 10px 0 0;\"><input name=\"ibank[]\" type=\"checkbox\" id=\"image$totalImages\" value=\"$img[0]\"  class=\"imageCheck\" onClick=\"checkShiftSelect(event)\" /><input name=\"Img_$img[0]\" type=\"image\" onclick=\"return setDeleteBankImage('$img[0]')\" id=\"bankDelete\" src=\"images/t_delete_btn.gif\" /></div><div>$selec</div>";
			$html .= "</div></div>\n";
			$counter++;
			$totalImages++;
		}
	}
	$html .= '</div></div>';
	//
	return $html;
}

function sub_folder_images_table_large1($iarr,$cols) {
    $q = "select * from folders order by order_num";
	$sel = mysql_query($q)
		or die(mysql_error());
	$fArr = array();
	while ($folder = mysql_fetch_array($sel, MYSQL_ASSOC)) {
		$fArr[] = $folder;
	}
	//
	$html = '';
	$loop = 0;
	$totalImages = 0;
	//
	foreach($fArr as $f) {
		$loop++;
		$counter = 0;
		foreach($iarr as $img) {
			$q = "select * from folder_images where file = '$img[0]'";
			$sel = mysql_query($q)
				or die(mysql_error());
			$row = mysql_fetch_array($sel,MYSQL_ASSOC);
			if ($row['folder_id'] == $f['id']) {
				$counter++;
			}
		}
		$html .= '<div class="fold" style="clear: both;" id="fold_'.$f['id'].'"><div style="position: relative; left: 10px;">';
		if ($counter > 0) {

			$html .= '<a href="javascript:;" onclick="toggle_folder_visibility(\'tLayer'.$loop.'\',\''.$loop.'\')"><img id="exp'.$loop.'" src="/images/expand_icon.gif" style="';

			if ($_SESSION['FOLDER_DISPLAY'] == $loop) {
				$html .= 'display: none; ';
			}

			$html .= 'float: left; position: relative; top: 10px; margin-right: 15px;" /><img id="col'.$loop.'" src="/images/collapse_icon.gif" style="float: left; ';

			if ($_SESSION['FOLDER_DISPLAY'] != $loop) {
				$html .= 'display: none; ';
			}

			$html .= 'position: relative; top: 10px; margin-right: 15px;" /></a>';

		}

		$html .= '<div style="float: left; ';

		if ($counter == 0) {
			$html .= 'margin-left: 30px;';
		}

		$html .= '"><img src="/images/folder_32.png"><span style="font: 14px \'Lucida Grande\',Arial,sans-serif; position: relative; top: -8px; left: 8px; color: #727272;">' . $f['name'];
		if ($counter > 1) {
			$html .= ' (' . $counter . ' images)</span>';
		} else if ($counter == 0) {
			$html .= ' (empty)</span>';
		} else if ($counter == 1) {
			$html .= ' (' . $counter . ' image)</span>';
		}
		$html .= '<a href="javascript:;" onclick="renameFolder(\''.$f['id'].'\', \'' . $f['name'] . '\');" style="position: relative; top: -5px; left: 15px;"><img src="images/pencil.png" /></a><a href="javascript:;" onclick="deleteFolder(\''.$f['id'].'\');" style="position: relative; margin-left: 24px; top: -5px;"><img src="images/t_delete_btn.gif" /></a></div></div><div id="tLayer'.$loop.'" style="';

		if ($_SESSION['FOLDER_DISPLAY'] != $loop) {
			$html .= 'display: none; ';
		}

		$html .= 'clear: both;">';
		$counter = 0;
		foreach($iarr as $img) {
			$q = "select * from folder_images where file = '$img[0]'";
			$sel = mysql_query($q)
				or die(mysql_error());
			$row = mysql_fetch_array($sel,MYSQL_ASSOC);
			//
			if ($row['folder_id'] == $f['id']) {
				$fileN = explode('.', $img[0]);
				$imageName = substr($fileN[0], 0, 12);
				if (strlen($img[0]) > 15) {
					$imageName .= '[...]';
				}
				$al = $row['align'];
				$imageName .= '.' . $fileN[1];
				if ($al == 'left') {
					$selec = "<select onchange=\"updateAlign(this.options[this.selectedIndex].value,'$img[0]')\" style=\"width: 140px;\"><option selected=\"selected\">Left</option><option>Right</option><option>Center</option></select>";
				} else if ($al == 'center') {
					$selec = "<select onchange=\"updateAlign(this.options[this.selectedIndex].value,'$img[0]')\" style=\"width: 140px;\"><option>Left</option><option>Right</option><option selected=\"selected\">Center</option></select>";
				} else if ($al == 'right') {
						$selec = "<select onchange=\"updateAlign(this.options[this.selectedIndex].value,'$img[0]')\" style=\"width: 140px;\"><option>Left</option><option selected=\"selected\">Right</option><option>Center</option></select>";
				} else {
					$selec = "<select style=\"width: 140px;\" disabled><option>Disabled</option></select>";
				}
				$html .= "<div class=\"thumbLayerLarge\" style=\"position: relative;\"><p style=\"margin: 6px 0 12px; height: 100px;\">";
				$html .= "<a href=\"display/popup/image_details.php?img=".$img[0]."\" class=\"lbOn\">".tag_image_large($img)."</a></p>";
				$html .= "<div style=\"display: block; width: 100%; text-align: center;\"><p style=\"float: left; margin-left: 8px; margin: 90px 0 0 14px;\"><strong style=\"margin-bottom: 10px; display: block;\">$imageName</strong></p>";
				$html .= "<div style=\"float:right; margin: 90px 16px 0 0;\"><input name=\"ibank[]\" type=\"checkbox\" id=\"image$totalImages\" value=\"$img[0]\"  class=\"imageCheck\" onClick=\"checkShiftSelect(event)\" /><input name=\"Img_$img[0]\" type=\"image\" onclick=\"return setDeleteBankImage('$img[0]')\" id=\"bankDelete\" src=\"images/t_delete_btn.gif\" /></div><div style=\"position: absolute; left: 14px; top: 240px;\">$selec</div>";
				$html .= "</div></div>\n";
				$counter++;
				$totalImages++;
			}
		}
		$html .= '</div></div>';
	}
	$counter = 0;
	foreach($iarr as $img) {
		$q = "select * from folder_images where file = '$img[0]'";
		$sel = mysql_query($q)
			or die(mysql_error());
		$row = mysql_fetch_array($sel,MYSQL_ASSOC);
		if ($row['folder_id'] <= 0) {
			$counter++;
			$totalImages++;
		}
	}
	$loop = $loop + 1;
	$html .= '<div class="fold" style="clear: both;"><div style="position: relative; left: 10px;">';

	if ($counter > 0) {

		$html .= '<a href="javascript:;" onclick="toggle_folder_visibility(\'tLayer'.$loop.'\',\''.$loop.'\')"><img id="exp'.$loop.'" src="/images/expand_icon.gif" style="';

		if ($_SESSION['FOLDER_DISPLAY'] == $loop) {
			$html .= 'display: none; ';
		}

		$html .= 'float: left; position: relative; top: 10px; margin-right: 15px;" /><img id="col'.$loop.'" src="/images/collapse_icon.gif" style="float: left; ';

		if ($_SESSION['FOLDER_DISPLAY'] != $loop) {
			$html .= 'display: none; ';
		}

		$html .= 'position: relative; top: 10px; margin-right: 15px;" /></a>';

	}

	$html .= '<div style="float: left; ';

	if ($counter == 0) {
		$html .= 'margin-left: 30px;';
	}

	$html .= '"><img src="/images/folder_32.png"><span style="font: 14px \'Lucida Grande\',Arial,sans-serif; position: relative; top: -8px; left: 8px; color: #727272;"> Unassigned ';
	if ($counter > 1) {
		$html .= '(' . $counter . ' images)</span>';
	} else if ($counter == 0) {
		$html .= '(empty)</span>';
	} else if ($counter == 1) {
		$html .= '(' . $counter . ' image)</span>';
	}
	$html .= '</div></div><div id="tLayer'.$loop.'" style="';

	if ($_SESSION['FOLDER_DISPLAY'] != $loop) {
		$html .= 'display: none; ';
	}

	$html .= 'clear: both;">';
	$counter = 0;
	foreach($iarr as $img) {
		$q = "select * from folder_images where file = '$img[0]'";
		$sel = mysql_query($q)
			or die(mysql_error());
		$row = mysql_fetch_array($sel,MYSQL_ASSOC);
		//
		if ($row['folder_id'] <= 0) {
			$fileN = explode('.', $img[0]);
			$imageName = substr($fileN[0], 0, 12);
			if (strlen($img[0]) > 15) {
				$imageName .= '[...]';
			}
			$al = $row['align'];
			$imageName .= '.' . $fileN[1];
			if ($al == 'left') {
				$selec = "<select onchange=\"updateAlign(this.options[this.selectedIndex].value,'$img[0]')\" style=\"width: 140px;\"><option selected=\"selected\">Left</option><option>Right</option><option>Center</option></select>";
			} else if ($al == 'center') {
				$selec = "<select onchange=\"updateAlign(this.options[this.selectedIndex].value,'$img[0]')\" style=\"width: 140px;\"><option>Left</option><option>Right</option><option selected=\"selected\">Center</option></select>";
			} else if ($al == 'right') {
					$selec = "<select onchange=\"updateAlign(this.options[this.selectedIndex].value,'$img[0]')\" style=\"width: 140px;\"><option>Left</option><option selected=\"selected\">Right</option><option>Center</option></select>";
			} else {
				$selec = "<select style=\"width: 140px;\" disabled><option>Disabled</option></select>";
			}
			$html .= "<div class=\"thumbLayerLarge\" style=\"position: relative;\"><p style=\"margin: 6px 0 12px; height: 100px;\">";
			$html .= "<a href=\"display/popup/image_details.php?img=".$img[0]."\" class=\"lbOn\">".tag_image_large($img)."</a></p>";
			$html .= "<div style=\"display: block; width: 100%; text-align: center;\"><p style=\"float: left; margin-left: 8px; margin: 90px 0 0 14px;\"><strong style=\"margin-bottom: 10px; display: block;\">$imageName</strong></p>";
			$html .= "<div style=\"float:right; margin: 90px 16px 0 0;\"><input name=\"ibank[]\" type=\"checkbox\" id=\"image$totalImages\" value=\"$img[0]\"  class=\"imageCheck\" onClick=\"checkShiftSelect(event)\" /><input name=\"Img_$img[0]\" type=\"image\" onclick=\"return setDeleteBankImage('$img[0]')\" id=\"bankDelete\" src=\"images/t_delete_btn.gif\" /></div><div style=\"position: absolute; left: 14px; top: 240px;\">$selec</div>";
			$html .= "</div></div>\n";
			$counter++;
			$totalImages++;
		}
	}
	$html .= '</div></div>';
	//
	return $html;
}
// Page functions
function get_page_info($pid) {
	$q = "select * from pages where page_id='$pid'";
	$sel = mysql_query($q)
		or die(mysql_error());
	$pg = mysql_fetch_array($sel, MYSQL_ASSOC);
	return $pg;
}
function get_sub_pages($pid=0) {
	$q = "select * from pages where parent_id='$pid' order by order_num";
	$sel = mysql_query($q)
		or die(mysql_error());
	$pgArr = array();
	while ($pg = mysql_fetch_array($sel, MYSQL_ASSOC)) {
		$pgArr[] = $pg;
	}
	return $pgArr;
}
function get_all_pages() {
	$q = "select * from pages where parent_id = 0 order by order_num, parent_id, page_title";
	$sel = mysql_query($q)
		or die(mysql_error());
	$pgArr = array();
	while ($pg = mysql_fetch_array($sel, MYSQL_ASSOC)) {
		$pgArr[] = $pg;
	}
	return $pgArr;
}
function get_all_sub_pages() {
	$q = "select * from pages where parent_id !=0 order by order_num, parent_id, page_title";
	$sel = mysql_query($q)
		or die(mysql_error());
	$pgArr = array();
	while ($pg = mysql_fetch_array($sel, MYSQL_ASSOC)) {
		$pgArr[] = $pg;
	}
	return $pgArr;
}
// Category and image functions
function category_exists($name) {
	$q = "select * from categories where category_name = '$name'";
	$sel = mysql_query($q)
		or die(mysql_error());
	if (mysql_num_rows($sel) > 0) {
		return true;
	} else {
		return false;
	}
}
function get_all_categories() {
	$q = "SELECT c .  * , count( g.gallery_id ) as num_galleries
		FROM categories c
		LEFT  JOIN galleries g ON c.category_id = g.category_id
		GROUP BY c.category_id
		ORDER  BY order_num ASC ";
	$sel = mysql_query($q)
		or die(mysql_error());
	$pgArr = array();
	while ($pg = mysql_fetch_array($sel, MYSQL_ASSOC)) {
		$pgArr[] = $pg;
	}
	return $pgArr;
}
function get_category($cid) {
	$q = "select * from categories where category_id = '$cid'";
	$sel = mysql_query($q)
		or die(mysql_error());
	$row = mysql_fetch_array($sel, MYSQL_ASSOC);
	return $row;
}
function remove_category($cid) {
	// First, get the row we're deleting
    $q = "select * from categories where category_id = '$cid'";
	$sel = mysql_query($q)
		or die(mysql_error());
	$s = mysql_fetch_array($sel, MYSQL_ASSOC);
	$order = $s['order_num'];
	// Remove category
	$q = "delete from categories where category_id = '$cid'";
	$del = mysql_query($q)
		or die(mysql_error());
	// Move categories up
	$q = "update categories set order_num = (order_num-1) where order_num > '$order'";
	$upd = mysql_query($q)
		or die(mysql_error());
	return true;
}

function get_galleries($cat) {
	$q = "SELECT g.*, count(i.gallery_id) as num_images
		FROM galleries g
		LEFT JOIN gallery_images i ON g.gallery_id = i.gallery_id
		WHERE g.category_id = '$cat'
		GROUP BY g.gallery_id
		ORDER BY g.order_num ASC ";
	$sel = mysql_query($q)
		or die(mysql_error());
	$pgArr = array();
	while ($pg = mysql_fetch_array($sel, MYSQL_ASSOC)) {
		$pgArr[] = $pg;
	}
	return $pgArr;
}
function get_all_galleries() {
	$q = "select g.*, c.category_name
		  from galleries g
		  left join categories c on g.category_id = c.category_id
		  order by c.order_num asc, g.order_num asc";
	$sel = mysql_query($q)
		or die(mysql_error());
	$pgArr = array();
	while ($pg = mysql_fetch_array($sel, MYSQL_ASSOC)) {
		$pgArr[] = $pg;
	}
	return $pgArr;
}
function remove_gallery($gid) {
	// First, get the row we're deleting
    $q = "select * from galleries where gallery_id = '$gid'";
	$sel = mysql_query($q)
		or die(mysql_error());
	$s = mysql_fetch_array($sel, MYSQL_ASSOC);
	$order = $s['order_num'];
	$cid = $s['category_id'];
	// Remove gallery
	$q = "delete from galleries where gallery_id = '$gid'";
	$del = mysql_query($q)
		or die(mysql_error());
	// Move other galleries up
	$q = "update galleries set order_num = (order_num -1) where order_num > '$order' and category_id = '$cid'";
	$upd = mysql_query($q)
		or die(mysql_error());
	// Remove image records
	$q = "delete from gallery_images where gallery_id = '$gid'";
	$del = mysql_query($q)
		or die(mysql_error());
	return true;
}
function get_extra_files() {
	$directory = $_SESSION['user']['path'] . 'extra';
	$r = array();
	if ($handle = opendir($directory)) {
	       while (false !== ($file = readdir($handle))) {
			if ($file != '.' && $file != '..') {
				$myfile = array();
				$myfile[0] = $file;
				$ext = substr($file, strrpos($file, '.') + 1);
				$myfile[1] = filesize($directory.'/'.$file);
				$myfile[2] = date("m/d/Y H:i", filemtime($directory.'/'.$file));
				//if ($ext != 'jpg' && $ext != 'mp3') {
					$r[] = $myfile;
				//}
			}
		}
    }
    closedir($handle);
    return $r;
}
function get_intro_files() {
	$directory = $_SESSION['user']['path'] . 'extra';
	$r = array();
	if ($handle = opendir($directory)) {
	       while (false !== ($file = readdir($handle))) {
			if ($file != '.' && $file != '..') {
				$myfile = array();
				$myfile[0] = $file;
				$ext = substr($file, strrpos($file, '.') + 1);
				$myfile[1] = filesize($directory.'/'.$file);
				$myfile[2] = date("m/d/Y H:i", filemtime($directory.'/'.$file));
				if (strtolower($ext) == 'jpg') {
					$r[] = $myfile;
				}
			}
		}
        }
        closedir($handle);

	if (count($r) == 0) {
		$f = is_file($_SESSION['user']['path'] . 'images/background.jpg');
		if ($f == true) {
			return 'background';
		}
	} else {
		return $r;
	}

}
function get_font_files() {
	$directory = '../../fonts';
	$r = array();
	$r[][0] = '.swf';
	$r[][0] = '.swf';
	$r[][0] = 'Core Fonts.swf';
	$r[][0] = '---------------------------.swf';
	if ($handle = opendir($directory)) {
	       while (false !== ($file = readdir($handle))) {
			if ($file != '.' && $file != '..') {
				$myfile = array();
				$myfile[0] = $file;
				$r[] = $myfile;
			}
		}
    }
   	closedir($handle);
	$r[][0] = '.swf';
	$r[][0] = '.swf';
	$r[][0] = 'Template Fonts.swf';
	$r[][0] = '---------------------------.swf';
   	$directory = '../../fonts/' . $_SESSION['template'];
	if ($handle = opendir($directory)) {
	       while (false !== ($file = readdir($handle))) {
			if ($file != '.' && $file != '..') {
				$myfile = array();
				$myfile[0] = $file;
				$r[] = $myfile;
			}
		}
    }
   	closedir($handle);
	$r[][0] = '.swf';
	$r[][0] = '.swf';
	$r[][0] = 'Custom Fonts.swf';
	$r[][0] = '---------------------------.swf';
	//
	// get font files from remote server 
	$postArr = array();
	$postArr['u'] = $_SESSION['user']['email'];
	$postArr['p'] = $_SESSION['user']['password'];
	$postArr['mode'] = 'fonts';
	$url = SERVER . '_bfadmin/files_list.php'; // 
	$remoteFiles = curl_post($url, $postArr, array()); 
	$remoteFiles = json_decode($remoteFiles); 
	foreach($remoteFiles as $file) {
		if ($file != '.' && $file != '..') {
			$myfile = array();
			$myfile[0] = $file;
			$r[] = $myfile;
		}
	}
	//
    return $r;
}
function get_music_files() {
	$directory = $_SESSION['user']['path'] . 'music';
	$r = array();
	if ($handle = opendir($directory)) {
	       while (false !== ($file = readdir($handle))) {
			if ($file != '.' && $file != '..') {
				$myfile = array();
				$myfile[0] = $file;
				$myfile[1] = filesize($directory.'/'.$file);
				$myfile[2] = date("m/d/Y H:i", filemtime($directory.'/'.$file));
				$r[] = $myfile;
			}
		}
        }
        closedir($handle);
    return $r;
}
function get_templates() {
	$q = "select * from templates";
	$sel = mysql_query($q)
		or die(mysql_error());
	$r = array();
	while ($row = mysql_fetch_array($sel, MYSQL_ASSOC)) {
		$r[] = $row;
	}
	return $r;
}
/*
function save_template ($id, $content) {
	$q = "update templates set contents = '$content' where template_id = '$id'";
	$update = mysql_query($q)
		or die(mysql_error());
	return true;
}
function create_new_template($id, $type, $content) {
	$q = "insert into `templates` (`template_id`, `template_type`,`contents`) values ('$id','$type','$content')";
	$update = mysql_query($q)
		or die(mysql_error());
	return true;
}
*/ 
function get_source_template($id) {
	$q = "select * from templates where template_id = '$id'";
	$sel = mysql_query($q)
		or die(mysql_error());
	$row = mysql_fetch_array($sel, MYSQL_ASSOC);
	return $row;
}
function get_source_templates($id) {
	$q = "select * from templates where template_id like '$id%' order by template_id desc";
	$sel = mysql_query($q)
		or die(mysql_error());
	return $sel;
}
function save_template ($id, $content) {
	$q = "update templates set contents = '$content' where template_id = '$id'";
	$update = mysql_query($q)
		or die(mysql_error());
	return true;
}
function create_new_template($id, $type, $content) { 
	//
	// adjust id size
	$q = "alter table templates modify template_id VARCHAR(50)";
	$update = mysql_query($q)
		or die(mysql_error()); 
	//   
	$queryIfAlreadyRowWithTemplateId = "select * from templates where template_id = '$id'";
	$result = mysql_query($queryIfAlreadyRowWithTemplateId);
	$resultCount = mysql_num_rows($result);	
	$newTemplateId = "";
	
	// If database already contains $id
	if($resultCount > 0) {
		// rename $id to $id_prior_to_datetime
		$newTemplateId = $id."_prior_to_".date('Y_m_d_H:i:s');
		$queryToRenameId = "update templates set template_id = '$newTemplateId' where template_id = '$id'";
		mysql_query($queryToRenameId);
	}				
	
	// add as usual
	$q = "insert into `templates` (`template_id`, `template_type`,`contents`) values ('$id','$type','$content')";
	$update = mysql_query($q)
		or die(mysql_error());
	return $newTemplateId;
}
//
function count_all_messages() {
	$q = "select count(*) as num from `messages` where date > '2009-10-12 00:00:00'"; //
	$sel = mysql_query($q)
		or die(mysql_error());
	$row = mysql_fetch_array($sel, MYSQL_ASSOC);
	return $row['num'];
}
function get_form_submissions($start, $limit=20) {
	$end = $limit;
	$q = "select *, date_format(date,'%b %D, %Y, %H:%i') as date_string from `messages` where date > '2009-10-12 00:00:00' order by date desc limit $start,$end"; //
	$sel = mysql_query($q)
		or die(mysql_error());
	$r = array();
	while ($row = mysql_fetch_array($sel, MYSQL_ASSOC)) {
		$r[] = $row;
	}
	return $r;
}
function count_all_visits() {
	$q = "select count(*) as num from `visits`";
	$sel = mysql_query($q)
		or die(mysql_error());
	$row = mysql_fetch_array($sel, MYSQL_ASSOC);
	return $row['num'];
}
function get_all_visits($limit=20) {
	$q = "select *, date_format(date,'%b %D, %Y') as date_string from visits order by date desc limit 0,$limit";
	$sel = mysql_query($q)
		or die(mysql_error());
	$r = array();
	while ($row = mysql_fetch_array($sel, MYSQL_ASSOC)) {
		$r[] = $row;
	}
	return $r;
}
function get_visits_by_day($mysql_date) {
	$q = "select * from visits where date > '$mysql_date 00:00:00' and date <= '$mysql_date 23:59:59' ";
	$sel = mysql_query($q)
		or die(mysql_error());
	$r = array();
	while ($row = mysql_fetch_array($sel, MYSQL_ASSOC)) {
		$r[] = $row;
	}
	return $r;
}
function get_num_visits_by_month($m, $y) {
	$q = "select count(*) as num_visits from `visits` where date > '$y-$m-1 00:00:00' and date <= '$y-$m-31 23:59:59'";
	$sel = mysql_query($q)
		or die(mysql_error());
	$r = array();
	$row = mysql_fetch_array($sel, MYSQL_ASSOC);
	return $row['num_visits'];
}
function get_visits_by_month($m, $y) {
	$q = "select * from visits where date > '$y-$m-1 00:00:00' and date <= '$y-$m-31 23:59:59' ";
	$sel = mysql_query($q)
		or die(mysql_error());
	$r = array();
	while ($row = mysql_fetch_array($sel, MYSQL_ASSOC)) {
		$r[] = $row;
	}
	return $r;
}
function get_referrer_visits($limit=20) {
	$q = "select date_format(date,'%b %D, %Y') as date_string, raw_referrer, referrer, search_string
		  from visits
		  where referrer != ''
		  and search_string = ''
		  order by date desc
		  limit 0, $limit";
	$sel = mysql_query($q)
		or die(mysql_error());
	$r = array();
	while ($row = mysql_fetch_array($sel, MYSQL_ASSOC)) {
		$r[] = $row;
	}
	return $r;
}
function get_search_queries($limit=20) {
	$q = "select date_format(date,'%b %D, %Y') as date_string, raw_referrer, referrer, search_string
		  from visits
		  where search_string != ''
		  order by date desc
		  limit 0, $limit";
	$sel = mysql_query($q)
		or die(mysql_error());
	$r = array();
	while ($row = mysql_fetch_array($sel, MYSQL_ASSOC)) {
		$r[] = $row;
	}
	return $r;
}
function colorPalette($imageFile, $numColors, $granularity = 5)
{
   $granularity = max(1, abs((int)$granularity));
   $colors = array();
   $size = @getimagesize($imageFile);
   if($size === false)
   {
      user_error("Unable to get image size data");
      return false;
   }
   $img = @imagecreatefromjpeg($imageFile);
   if(!$img)
   {
      user_error("Unable to open image file");
      return false;
   }
   for($x = 0; $x < $size[0]; $x += $granularity)
   {
      for($y = 0; $y < $size[1]; $y += $granularity)
      {
         $thisColor = imagecolorat($img, $x, $y);
         $rgb = imagecolorsforindex($img, $thisColor);
         $red = round(round(($rgb['red'] / 0x33)) * 0x33);
         $green = round(round(($rgb['green'] / 0x33)) * 0x33);
         $blue = round(round(($rgb['blue'] / 0x33)) * 0x33);
         $thisRGB = sprintf('%02X%02X%02X', $red, $green, $blue);
         if(array_key_exists($thisRGB, $colors))
         {
            $colors[$thisRGB]++;
         }
         else
         {
            $colors[$thisRGB] = 1;
         }
      }
   }
   arsort($colors);
   return array_slice(array_keys($colors), 0, $numColors);
}
function get_font_selection($type) {
	$str = '';
	if (get_property('TEMPLATE_TYPE') != 'HTML5' && get_property('TEMPLATE') != 'desperado') { //is_dir($_SESSION['user']['path'] . 'fonts/')) {
		$f = get_font_files();
		foreach($f as $font) {
			$aa = explode('.', $font[0]);
			if ($aa[1] == 'swf') {
			    $str .= '<option>' . $aa[0] . '</option>';
			} else if ($aa[2] == 'swf') {
			    $str .= '<option>' . $aa[0] . '.' . $aa[1] . '</option>';
			}
		}
	} else {
		switch($type) {
			case 't' :
				$f = explode(',',get_property('TITLE_FONTS'));
				break;
			case 'n' :
				$f = explode(',',get_property('NAVIGATION_FONTS'));
				break;
			case 'p' :
				$f = explode(',',get_property('PAGE_FONTS'));
				break;
		}
		foreach($f as $font) {
			$str .= '<option>' . $font . '</option>';
		}
	}
	return $str;
}
function get_videos($gid) {
	$q = "select * from videos where gallery_id = '$gid' order by order_num asc";
	$sel = mysql_query($q)
		or die(mysql_error());
	$r = array();
	while ($row = mysql_fetch_array($sel, MYSQL_ASSOC)) {
		$r[] = $row;
	}
	return $r;
}
function get_vgallery($gid) {
	$q = "select g.*, c.category_name from v_galleries g, v_categories c
		  where g.gallery_id='$gid' and
		  g.category_id = c.category_id";
	$sel = mysql_query($q)
		or die(mysql_error());
	$row = mysql_fetch_array($sel,MYSQL_ASSOC);
	return $row;
}
function get_all_vcategories() {
	$q = "SELECT c .  * , count( g.gallery_id ) as num_galleries
		FROM v_categories c
		LEFT  JOIN v_galleries g ON c.category_id = g.category_id
		GROUP BY c.category_id
		ORDER  BY order_num ASC ";
	$sel = mysql_query($q)
		or die(mysql_error());
	$pgArr = array();
	while ($pg = mysql_fetch_array($sel, MYSQL_ASSOC)) {
		$pgArr[] = $pg;
	}
	return $pgArr;
}
function delete_gallery_video($iid,$gid) {
    // First, get the row we're deleting
    $q = "select * from videos where video_id = '$iid'";
	$sel = mysql_query($q)
		or die(mysql_error());
	$s = mysql_fetch_array($sel, MYSQL_ASSOC);
	$order = $s['order_num'];
	// Now, delete the page
	$q = "delete from videos where video_id = '$iid'";
	$sel = mysql_query($q)
		or die(mysql_error());
    // Now, reorder all pages below order
    $q = "update videos set order_num = (order_num-1) where order_num > '$order' and gallery_id = '$gid'";
	$sel = mysql_query($q)
		or die(mysql_error());
    return true;
}
function get_video_bank($sort='name',$dir='asc') {
	$directory = $_SESSION['user']['path'] . 'videos';
	$r = array();
    if ($handle = opendir($directory)) {
        while (false !== ($file = readdir($handle))) {
            if (ereg(".flv",strtolower($file)) || ereg(".mpg",strtolower($file)) || ereg(".mov",strtolower($file)) || ereg(".mp4",strtolower($file)) || ereg(".m4v",strtolower($file)) || ereg(".mp3",strtolower($file))) {
                $fsize = getimagesize($directory."/".$file);
				$image = array();
				// Index 0 is the file name
				$image[0] = $file;
				// Index 1 is the width
				$image[1] = $fsize[0];
				// Index 2 is the width
				$image[2] = $fsize[1];
				// Index 3 is the type
				$image[3] = 'jpg';
				if ($fsize[2] == 4 || $fsize[2] == 13)
					$image[3] = 'swf';
				// Index 4 is true if thumb file exists
				$image[4] = false;
				if ($image[3] == 'jpg' && file_exists($directory.'/../square/'.$file))
					$image[4] = true;
                if ($image[3] == 'swf' && file_exists($directory.'/../square/'.$file.'.jpg'))
					$image[4] = true;
				// Index 5 is the filesize in bytes
				$image[5] = filesize($directory.'/'.$file);
				// Index 6 is the file date modified timestamp
				$image[6] = filemtime($directory . '/' . $file);
				// Index 7 is the file date string
				$image[7] = date('m/d/Y H:i', $image[6]);
                $r[] = $image;
            }
        }
        closedir($handle);
    }
	if ($sort == 'name') {
		if ($dir == 'asc') {
			usort($r, 'ib_sort_name_asc');
		} else {
			usort($r, 'ib_sort_name_desc');
		}
	} else if ($sort == 'date') {
		if ($dir == 'asc') {
			usort($r, 'ib_sort_date_asc');
		} else {
			usort($r, 'ib_sort_date_desc');
		}
	}
    return $r;
}
function video_table($iarr,$cols) {
    $counter = 0;
    $r = "";
    foreach($iarr as $img) {
		$fileN = explode('.', $img[0]);
		$imageName = substr($fileN[0], 0, 12);
		if (strlen($img[0]) > 15) {
			$imageName .= '[...]';
		}
		$q = "select * from videos where video_file = '$img[0]'";
		$sel = mysql_query($q)
			or die(mysql_error());
		$row = mysql_fetch_array($sel,MYSQL_ASSOC);
		$imageName .= '.' . $fileN[1];
		if ($row['thumb'] == '') {
			$t_img = "<img src='images/countdown.jpg' />";
		} else {
			$t_img = tag_image($row['thumb']);//'http://72.32.198.216/~' . $_SESSION['user']['db_user'] . '/gallery/thumb/' . $row['thumb'];
		}
		if ($row['video_id'] <= 0) {
			$disabled = '<input type="button" value="Upload Thumbnail" disabled>';
		} else {
			$disabled = "<a class=\"lbOn\" href=\"display/popup/upload_thumbs.php?img=".$img[0]."\" rel=\"gb_page_center[300, 130]\"><input type=\"button\" value=\"Upload Thumbnail\" ></a>";
		}
		$r .= "<div class=\"thumbLayer\"><p style=\"margin: 6px 0 12px; height: 100px;\">";
		$r .= "<a href=\"display/popup/video_details.php?img=".$img[0]."\" rel=\"gb_page_center[690, 470]\" class=\"lbOn\">".$t_img."</a></p>"; //<img src=\"".$t_img."\" />
		$r .= "<div style=\"display: block; width: 100%; text-align: center;\"><p style=\"float: left; margin-left: 8px;\"><strong style=\"margin-bottom: 10px; display: block;\">$imageName</strong></p>";
		$r .= "<div style=\"float:right; margin: -3px 8px 0 0;\"><input name=\"ibank[]\" type=\"checkbox\" id=\"ibank[]\" value=\"$img[0]\"  class=\"imageCheck\" /><input name=\"Img_$img[0]\" type=\"image\" onclick=\"return setDeleteBankVideo('$img[0]')\" id=\"bankDelete\" src=\"images/t_delete_btn.gif\" /</div><div>$disabled</div>";
		$r .= "</div></div>\n";
		$r .= "</div>\n";
		$counter++;
	}
	return $r;
}
function get_vgalleries($cat) {
	$q = "SELECT g.*, count(i.gallery_id) as num_images
		FROM v_galleries g
		LEFT JOIN videos i ON g.gallery_id = i.gallery_id
		WHERE g.category_id = '$cat'
		GROUP BY g.gallery_id
		ORDER BY g.order_num ASC ";
	$sel = mysql_query($q)
		or die(mysql_error());
	$pgArr = array();
	while ($pg = mysql_fetch_array($sel, MYSQL_ASSOC)) {
		$pgArr[] = $pg;
	}
	return $pgArr;
}
function get_all_vgalleries() {
	$q = "select g.*, c.category_name
		  from v_galleries g
		  left join v_categories c on g.category_id = c.category_id
		  order by c.order_num asc, g.order_num asc";
	$sel = mysql_query($q)
		or die(mysql_error());
	$pgArr = array();
	while ($pg = mysql_fetch_array($sel, MYSQL_ASSOC)) {
		$pgArr[] = $pg;
	}
	return $pgArr;
}
function get_vcategory($cid) {
	$q = "select * from v_categories where category_id = '$cid'";
	$sel = mysql_query($q)
		or die(mysql_error());
	$row = mysql_fetch_array($sel, MYSQL_ASSOC);
	return $row;
}
function remove_vcategory($cid) {
	// First, get the row we're deleting
    $q = "select * from v_categories where category_id = '$cid'";
	$sel = mysql_query($q)
		or die(mysql_error());
	$s = mysql_fetch_array($sel, MYSQL_ASSOC);
	$order = $s['order_num'];
	// Remove category
	$q = "delete from v_categories where category_id = '$cid'";
	$del = mysql_query($q)
		or die(mysql_error());
	// Move categories up
	$q = "update v_categories set order_num = (order_num-1) where order_num > '$order'";
	$upd = mysql_query($q)
		or die(mysql_error());
	return true;
}
function remove_vgallery($gid) {
	// First, get the row we're deleting
    $q = "select * from v_galleries where gallery_id = '$gid'";
	$sel = mysql_query($q)
		or die(mysql_error());
	$s = mysql_fetch_array($sel, MYSQL_ASSOC);
	$order = $s['order_num'];
	$cid = $s['category_id'];
	// Remove gallery
	$q = "delete from v_galleries where gallery_id = '$gid'";
	$del = mysql_query($q)
		or die(mysql_error());
	// Move other galleries up
	$q = "update v_galleries set order_num = (order_num -1) where order_num > '$order' and category_id = '$cid'";
	$upd = mysql_query($q)
		or die(mysql_error());
	// Remove image records
	$q = "delete from videos where gallery_id = '$gid'";
	$del = mysql_query($q)
		or die(mysql_error());
	return true;
}
function get_calendar_events() {
	$q = "select * from calendar where id >= 0 order by start_date, start_time asc";
	$sel = mysql_query($q)
		or die(mysql_error());
	$eventsArr = array();
	while ($e = mysql_fetch_array($sel, MYSQL_ASSOC)) {
		$eventsArr[] = $e;
	}
	return $eventsArr;
}

function get_calendar_import_id() {
	$q = "select import_id from calendar where id >= 0 order by import_id";
	$sel = mysql_query($q)
		or die(mysql_error());
	$highestID = 0;
	while ($e = mysql_fetch_array($sel, MYSQL_ASSOC)) {
		if ($highestID < $e['import_id']) {
			$highestID = $e['import_id'];
		}
	}
	$highestID++;
	return $highestID;
}
function convert_to_numeric_values($str) {
    $chars = array('À','à','Á','á','Â','â','Ã','ã','Ä','ä','Å','å','Ā','ā','Ă','ă','Ą','ą','Ǟ','ǟ','Ǻ','ǻ','Æ','æ','Ǽ','ǽ','B','b','Ḃ','ḃ','C','c','Ć','ć','Ç','ç','Č','č','Ĉ','ĉ','Ċ','ċ','D','d','Ḑ','ḑ','Ď','ď','Ḋ','ḋ','Đ','đ','Ð','ð','Ǳ','ǳ','Ǆ','ǆ','E','e','È','è','É','é','Ě','ě','Ê','ê','Ë','ë','Ē','ē','Ĕ','ĕ','Ę','ę','Ė','ė','Ʒ','ʒ','Ǯ','ǯ','F','f','Ḟ','ḟ','ƒ','ﬀ','ﬁ','ﬂ','ﬃ','ﬄ','ﬅ','G','g','Ǵ','ǵ','Ģ','ģ','Ǧ','ǧ','Ĝ','ĝ','Ğ','ğ','Ġ','ġ','Ǥ','ǥ','H','h','Ĥ','ĥ','Ħ','ħ','I','i','Ì','ì','Í','í','Î','î','Ĩ','ĩ','Ï','ï','Ī','ī','Ĭ','ĭ','Į','į','İ','ı','Ĳ','ĳ','J','j','Ĵ','ĵ','K','k','Ḱ','ḱ','Ķ','ķ','Ǩ','ǩ','ĸ','L','l','Ĺ','ĺ','Ļ','ļ','Ľ','ľ','Ŀ','ŀ','Ł','ł','Ǉ','ǉ','M','m','Ṁ','ṁ','N','n','Ń','ń','Ņ','ņ','Ň','ň','Ñ','ñ','ŉ','Ŋ','ŋ','Ǌ','ǌ','O','o','Ò','ò','Ó','ó','Ô','ô','Õ','õ','Ö','ö','Ō','ō','Ŏ','ŏ','Ø','ø','Ő','ő','Ǿ','ǿ','Œ','œ','P','p','Ṗ','ṗ','Q','q','R','r','Ŕ','ŕ','Ŗ','ŗ','Ř','ř','ɼ','S','s','Ś','ś','Ş','ş','Š','š','Ŝ','ŝ','Ṡ','ṡ','ſ','ß','T','t','Ţ','ţ','Ť','ť','Ṫ','ṫ','Ŧ','ŧ','Þ','þ','U','u','Ù','ù','Ú','ú','Û','û','Ũ','ũ','Ü','ü','Ů','ů','Ū','ū','Ŭ','ŭ','Ų','ų','Ű','ű','V','v','W','w','Ẁ','ẁ','Ẃ','ẃ','Ŵ','ŵ','Ẅ','ẅ','X','x','Y','y','Ỳ','ỳ','Ý','ý','Ŷ','ŷ','Ÿ','ÿ','Z','z','Ź','ź','Ž','ž','Ż','ż');
    $numerics = array('&#192;','&#224;','&#193;','&#225;','&#194;','&#226;','&#195;','&#227;','&#196;','&#228;','&#197;','&#229;','&#256;','&#257;','&#258;','&#259;','&#260;','&#261;','&#478;','&#479;','&#506;','&#507;','&#198;','&#230;','&#508;','&#509;','&#66;','&#98;','&#7682;','&#7683;','&#67;','&#99;','&#262;','&#263;','&#199;','&#231;','&#268;','&#269;','&#264;','&#265;','&#266;','&#267;','&#68;','&#100;','&#7696;','&#7697;','&#270;','&#271;','&#7690;','&#7691;','&#272;','&#273;','&#208;','&#240;','&#497;','&#499;','&#452;','&#454;','&#69;','&#101;','&#200;','&#232;','&#201;','&#233;','&#282;','&#283;','&#202;','&#234;','&#203;','&#235;','&#274;','&#275;','&#276;','&#277;','&#280;','&#281;','&#278;','&#279;','&#439;','&#658;','&#494;','&#495;','&#70;','&#102;','&#7710;','&#7711;','&#402;','&#64256;','&#64257;','&#64258;','&#64259;','&#64260;','&#64261;','&#71;','&#103;','&#500;','&#501;','&#290;','&#291;','&#486;','&#487;','&#284;','&#285;','&#286;','&#287;','&#288;','&#289;','&#484;','&#485;','&#72;','&#104;','&#292;','&#293;','&#294;','&#295;','&#73;','&#105;','&#204;','&#236;','&#205;','&#237;','&#206;','&#238;','&#296;','&#297;','&#207;','&#239;','&#298;','&#299;','&#300;','&#301;','&#302;','&#303;','&#304;','&#305;','&#306;','&#307;','&#74;','&#106;','&#308;','&#309;','&#75;','&#107;','&#7728;','&#7729;','&#310;','&#311;','&#488;','&#489;','&#312;','&#76;','&#108;','&#313;','&#314;','&#315;','&#316;','&#317;','&#318;','&#319;','&#320;','&#321;','&#322;','&#455;','&#457;','&#77;','&#109;','&#7744;','&#7745;','&#78;','&#110;','&#323;','&#324;','&#325;','&#326;','&#327;','&#328;','&#209;','&#241;','&#329;','&#330;','&#331;','&#458;','&#460;','&#79;','&#111;','&#210;','&#242;','&#211;','&#243;','&#212;','&#244;','&#213;','&#245;','&#214;','&#246;','&#332;','&#333;','&#334;','&#335;','&#216;','&#248;','&#336;','&#337;','&#510;','&#511;','&#338;','&#339;','&#80;','&#112;','&#7766;','&#7767;','&#81;','&#113;','&#82;','&#114;','&#340;','&#341;','&#342;','&#343;','&#344;','&#345;','&#636;','&#83;','&#115;','&#346;','&#347;','&#350;','&#351;','&#352;','&#353;','&#348;','&#349;','&#7776;','&#7777;','&#383;','&#223;','&#84;','&#116;','&#354;','&#355;','&#356;','&#357;','&#7786;','&#7787;','&#358;','&#359;','&#222;','&#254;','&#85;','&#117;','&#217;','&#249;','&#218;','&#250;','&#219;','&#251;','&#360;','&#361;','&#220;','&#252;','&#366;','&#367;','&#362;','&#363;','&#364;','&#365;','&#370;','&#371;','&#368;','&#369;','&#86;','&#118;','&#87;','&#119;','&#7808;','&#7809;','&#7810;','&#7811;','&#372;','&#373;','&#7812;','&#7813;','&#88;','&#120;','&#89;','&#121;','&#7922;','&#7923;','&#221;','&#253;','&#374;','&#375;','&#159;','&#255;','&#90;','&#122;','&#377;','&#378;','&#381;','&#382;','&#379;','&#380;');
    $str = str_replace($chars,$numerics,$str);
    $str = str_replace($chars,$numerics,$str);
    return $str;
}
function curl_post($url, array $post = NULL, array $options = array()) 
{ 
    $defaults = array( 
        CURLOPT_POST => 1, 
        CURLOPT_HEADER => 0, 
        CURLOPT_URL => $url, 
        CURLOPT_FRESH_CONNECT => 1, 
        CURLOPT_RETURNTRANSFER => 1, 
        CURLOPT_FORBID_REUSE => 1, 
        CURLOPT_TIMEOUT => 4, 
        CURLOPT_CAINFO => '/etc/ssl/certs/bigfolio-wildcard-2015.crt',
        CURLOPT_POSTFIELDS => http_build_query($post) 
    ); 

    $ch = curl_init(); 
    curl_setopt_array($ch, ($options + $defaults)); 
    if( ! $result = curl_exec($ch)) 
    { 
        trigger_error(curl_error($ch)); 
    } 
    curl_close($ch); 
    return $result; 
}

function editorCharReplacements($str) {
	$str = str_replace("<br>", "\r", $str);
	return $str;
}
function charReplacements($str) {
	//$str = str_replace("[[ampersand]]","&", $str);
	//$str = str_replace('"', "&quot;", $str);
	//$str = str_replace("'", "&apos;", $str);
    $arr = array("\r\n", "\n", "\r");
	$str = str_replace($arr, "<br>", $str);
	return $str;	
} 


/* bigshow stuff */
function get_all_bigshows() {
	if ($_SESSION['server'] == 'bigshow') {
		$clientID = $_SESSION['user']['storage_id'];
		$q = "select * from bigshow_slideshows where storage_id = '$clientID' order by id";
	} else {
		$q = "select * from bigshow_slideshows order by id";
	}
	$sel = mysql_query($q)
		or die(mysql_error());
	$pgArr = array();
	while ($pg = mysql_fetch_array($sel, MYSQL_ASSOC)) {
		$pgArr[] = $pg;
	}
	return $pgArr;
} 
function niceURL($name) {
	$name = strToLower($name);
	$name = preg_replace("/[^a-zA-Z0-9\-_]+/", "-", $name);
	return preg_replace("/[-]+/", "-", $name);
}
function remove_slideshow($sid) { 
	 
	$q = "delete from bigshow_images where slideshow_id = '$sid'";
	$del = mysql_query($q)
		or die(mysql_error());
		
	
	$q = "delete from bigshow_slideshows where id = '$sid'";
	$del = mysql_query($q)
		or die(mysql_error());
	return true;
}
function get_bigshow($sid) {
	$q = "select * from bigshow_slideshows where id='$sid'";
	$sel = mysql_query($q)
		or die(mysql_error());
	$row = mysql_fetch_array($sel,MYSQL_ASSOC);
	return $row;
} 
function get_bigshow_images($sid) {
	$q = "select * from bigshow_images where slideshow_id = '$sid' order by order_num asc";
	$sel = mysql_query($q)
		or die(mysql_error());
	$r = array();
	while ($row = mysql_fetch_array($sel, MYSQL_ASSOC)) {
		$r[] = $row;
	}
	return $r;
}

function cleanfile($name) {
	$name = preg_replace("/[^a-zA-Z0-9\-_\.]+/", "-", $name);
	return preg_replace("/[-]+/", "-", $name);
}

function getNameAndExtension($file) {
	$arr = explode('.', $file);
	$name = ''; 
	for ($i=0; $i<count($arr)-1; $i++) {
		$name .= $arr[$i]; 
		if ($i<count($arr)-2) {
			$name .= ".";
		}
	} 
	$nameArr = array(); 
	$nameArr[0] = $name;
	$nameArr[1] = $arr[count($arr)-1];                
	return $nameArr;
}

function thumbnail($image,$thumb,$width,$height,$crop=false) {
	if (function_exists('imagecreatefromjpeg')) {
		$ext = explode('.', $image);
		if ($ext[count($ext)-1] == 'png' || $ext[count($ext)-1] == 'PNG') {
			$src_img = imagecreatefrompng($image); 
		} else {
			$src_img = imagecreatefromjpeg($image); 
		}
		
    	$origw=imagesx($src_img); 
    	$origh=imagesy($src_img); 

		$diff = $origw / $width;
		
		if ($origh / $diff > $height) {
			$new_h = $height;
			$diff = $origh / $height;
			$new_w = $origw / $diff;
		} else {
			$new_w = $width;
			$new_h = $origh / $diff;
		}
		

		if ($crop) {
			$new_w = $width*2;
			$new_h = $height*2;
			$dst_img = imagecreatetruecolor($width,$height);
		} else {
			$dst_img = imagecreatetruecolor($new_w, $new_h);
		}
		if ($ext[count($ext)-1] == 'png' || $ext[count($ext)-1] == 'PNG') {
			imagealphablending($dst_img, false);
		}
    	imagecopyresampled($dst_img,$src_img,0,0,0,0,$new_w,$new_h,imagesx($src_img),imagesy($src_img)); 
		if ($ext[count($ext)-1] == 'png' || $ext[count($ext)-1] == 'PNG') {
			imagesavealpha($dst_img, true);
			imagepng($dst_img, $thumb, 9, NULL);
		} else {
   			imagejpeg($dst_img, $thumb, 85);
		}
		imagedestroy($src_img);
		imagedestroy($dst_img);
	} 
    return true; 
}

?>
