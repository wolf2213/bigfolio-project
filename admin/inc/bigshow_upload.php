<?php 
include('functions.inc.php');

$storageID = $_POST['storageID'];
$ssID = $_POST['ssID']; 
$bucketName = "cdn.bigfoliobigshow.com";
  
$ssPath = 'users/' . $storageID{0} . '/' . $storageID{1} . '/' . $storageID{2} . '/' . $storageID . '/';
if (isset($ssID)) {
	$ssPath .= $ssID . '/'; 
}  

if (!class_exists('S3')) require_once 'S3/S3.php';
include('S3/S3_config.php');

//
// parse file name and get s3 unique name
$tempFile = $_FILES['Filedata']['tmp_name']; 
$fileName = cleanfile($_FILES['Filedata']['name']); //cleanfile($_FILES['Filedata']['name']); 
$s3fileName = $fileName; //uniqid() . '-' . $fileName; 
$nameArr = explode(".", $s3fileName);
$ext = $nameArr[count($nameArr)-1];  
$s3baseName = "";
for ($i=0; $i<count($nameArr)-1; $i++) {
	$s3baseName .= $nameArr[$i] . ".";             
} 
$s3baseName = substr($s3baseName, 0, -1);
$s3fileName =  $s3baseName . '.' . $ext;
$targetPath = '../uploads/';
if (!is_dir(str_replace('//','/',$targetPath))) {
	$dirCreate = mkdir(str_replace('//','/',$targetPath), 0755, true); 
	if (!$dirCreate) {
		echo("\nCan't create dir: ".$targetPath);
	} else {
		echo("\n$targetPath created successffully");
	}
}
$uniqifier = uniqid();
//echo "\nuniqifier: ".$uniqifier;
/*
$ssDirs = explode("/", $ssPath);
foreach ($ssDirs as $ssd) {
	$targetPath .= $ssd . '/';
	// make temp uploads dir 
	if (!is_dir(str_replace('//','/',$targetPath))) {
		$dirCreate = mkdir(str_replace('//','/',$targetPath), 0755, true); 
		if (!$dirCreate) {
			echo("\nCan't create dir: ".$targetPath);
		} else {
			echo("\n$targetPath created successffully");
		}
	}
} 
*/

$targetFile =  str_replace('//','/',$targetPath) . $uniqifier . $s3fileName;

echo "\ntarget file: $targetFile";
echo "\nisfile: ".is_file($tempFile);

$success = move_uploaded_file($tempFile,$targetFile); 
if (!$success) {
	exit("\nERROR: file could not be uploaded...");
} 

$filesToSend = array();  
$filesToSend[] = $s3fileName;

// 
// make thumbnails 
if ((strtolower($ext) == 'png' || strtolower($ext) == 'jpg' || strtolower($ext) == 'jpeg') && isset($ssID)) {  
	//
	$mediumThumb   = $s3baseName . '_medium.' . $ext;
	thumbnail($targetFile, $targetPath . $uniqifier . $mediumThumb, 600, 400); 
	$smallThumb    = $s3baseName . '_small.' . $ext;
	thumbnail($targetPath . $uniqifier . $mediumThumb, $targetPath . $uniqifier . $smallThumb, 300, 200);
	$tinyThumb     = $s3baseName . '_thumb.' . $ext;
	thumbnail($targetPath . $uniqifier . $smallThumb, $targetPath . $uniqifier . $tinyThumb, 100, 66);
	//
	$filesToSend[] = $mediumThumb;
	$filesToSend[] = $smallThumb;
	$filesToSend[] = $tinyThumb; 
	// 
} 


//
// Check for CURL
if (!extension_loaded('curl') && !@dl(PHP_SHLIB_SUFFIX == 'so' ? 'curl.so' : 'php_curl.dll'))
	exit("\nERROR: CURL extension not loaded\n\n");

//
// Pointless without your keys!
if (awsAccessKey == 'change-this' || awsSecretKey == 'change-this')
	exit("\nERROR: AWS access information required\n\nPlease edit the following lines in this file:\n\n".
	"define('awsAccessKey', 'change-me');\ndefine('awsSecretKey', 'change-me');\n\n");

//
// Instantiate the class
$s3 = new S3(awsAccessKey, awsSecretKey);
S3::$useSSL = false;

foreach ($filesToSend as $f) {
	// If you want to use PECL Fileinfo for MIME types:
	//if (!extension_loaded('fileinfo') && @dl('fileinfo.so')) $_ENV['MAGIC'] = '/usr/share/file/magic';

	// Check if our upload file exists
	if (!file_exists($targetPath . $uniqifier . $f) || !is_file($targetPath . $uniqifier . $f))
		exit("\nERROR: No such file: $uploadFile\n\n");
                                                                                                        
	// List your buckets:
	//echo "S3::listBuckets(): ".print_r($s3->listBuckets(), 1)."\n";

	// Create a bucket with public read access
	if ($s3->putBucket($bucketName, S3::ACL_PUBLIC_READ)) {
		echo "Created bucket {$bucketName}".PHP_EOL;

		// Put our file (also with public read access)
		if ($s3->putObjectFile($targetPath . $uniqifier . $f, $bucketName, $ssPath.$f, S3::ACL_PUBLIC_READ)) {
			echo "S3::putObjectFile(): File copied to {$bucketName}/".baseName($uploadFile).PHP_EOL;
			//s3->deleteObject($bucketName, 'pv_images/');
			
			//
			// delete the uploaded file locally since it's now in S3
			unlink($targetPath . $uniqifier . $f);
			
			/*
			// Get the contents of our bucket
			$contents = $s3->getBucket($bucketName);
			//echo "S3::getBucket(): Files in bucket {$bucketName}: ".print_r($contents, 1);

			// Get object info
			$info = $s3->getObjectInfo($bucketName, $f);
			//echo "S3::getObjecInfo(): Info for {$bucketName}/".$f.': '.print_r($info, 1);
			*/
		} else {
			//echo "S3::putObjectFile(): Failed to copy file\n";
		} 
	} else {
		echo "S3::putBucket(): Unable to create bucket (it may already exist and/or be owned by someone else)\n";
	} 
}
?>
