<?
// Include files
include_once("db.inc.php");
include_once("functions.inc.php");

$s = get_settings();                          
$template = get_property("TEMPLATE");
$templateType = get_property("TEMPLATE_TYPE"); 

$includeAll = stristr($_SERVER['PHP_SELF'], 'fonts'); 

function isStandardFont($font) {
	$font = strtolower($font);
	if ($font=="arial" || $font=="helvetica" || $font=="times" || $font=="verdana" || $font=="georgia") {
		return true;		
	} else {
		return false;
	}
}  


if ($templateType == 'HTML5' || $template == 'desperado') {
	
	$tfonts = explode(',',get_property('TITLE_FONTS'));
	$nfonts = explode(',',get_property('NAVIGATION_FONTS'));
	$pfonts = explode(',',get_property('PAGE_FONTS'));
	$fonts = $tfonts;
	//
	// add unique nav fonts
	foreach($nfonts as $nf) {
		$noMatch = true;
		foreach($fonts as $f) {
			if ($f == $nf) {
				$noMatch = false;
			}
		}               
		if ($noMatch) {
			$fonts[] = $nf;
		}
	}
	//
	// add unique page fonts
	foreach($pfonts as $pf) {
		$noMatch = true;
		foreach($fonts as $f) {
		   	if ($f == $nf) {
				$noMatch = false;
			}
		}               
		if ($noMatch) {
			$fonts[] = $pf;
		}
	}
	
	if ($includeAll) { 
		$fontInclude = "<link href='https://fonts.googleapis.com/css?family=";
 	  	foreach($fonts as $f) { 
			if (!isStandardFont($f)) {
				$fontInclude .= str_replace(" ","+",$f) . "|";
	        	//echo "<link href='https://fonts.googleapis.com/css?family=".str_replace(" ","+",$f)."' rel='stylesheet' type='text/css'>";
			}
		}
		$fontInclude .= "' rel='stylesheet' type='text/css'>";
		echo $fontInclude;
  	} else { 
		if (!isStandardFont($f)) {
		    echo "<link href='https://fonts.googleapis.com/css?family=".str_replace(" ","+",$s['page_font'])."' rel='stylesheet' type='text/css'>"; 
		}
	}
}
?>