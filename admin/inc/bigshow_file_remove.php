<?php  

include('functions.inc.php');

$id = $_POST['id'];  

$storageID = $_POST['storageID'];
$ssID = $_POST['ssID']; 
$bucketName = "cdn.bigfoliobigshow.com";

$ssPath = 'users/' . $storageID{0} . '/' . $storageID{1} . '/' . $storageID{2} . '/' . $storageID . '/';
if (isset($ssID)) {
	$ssPath .= $ssID . '/'; 
}

if (!class_exists('S3')) require_once 'S3/S3.php';
include('S3/S3_config.php');

//
// parse file name and get s3 unique name
$fileName = $_POST['file']; 
$s3fileName = $fileName;
$nameArr = explode(".", $s3fileName);
$ext = $nameArr[count($nameArr)-1];  
$s3baseName = "";
for ($i=0; $i<count($nameArr)-1; $i++) {
	$s3baseName .= $nameArr[$i];              
} 
$s3fileName =  $s3baseName . '.' . $ext;
echo "s3fileName: ".$s3fileName; 
// 
// make thumbnails
$mediumThumb   = $s3baseName . '_medium.' . $ext;
$smallThumb    = $s3baseName . '_small.' . $ext;
$tinyThumb     = $s3baseName . '_thumb.' . $ext;
//
$filesToSend = array();  
$filesToSend[] = $s3fileName;
$filesToSend[] = $mediumThumb;
$filesToSend[] = $smallThumb;
$filesToSend[] = $tinyThumb; 

//
// Check for CURL
if (!extension_loaded('curl') && !@dl(PHP_SHLIB_SUFFIX == 'so' ? 'curl.so' : 'php_curl.dll'))
	exit("\nERROR: CURL extension not loaded\n\n");

//
// Pointless without your keys!
if (awsAccessKey == 'change-this' || awsSecretKey == 'change-this')
	exit("\nERROR: AWS access information required\n\nPlease edit the following lines in this file:\n\n".
	"define('awsAccessKey', 'change-me');\ndefine('awsSecretKey', 'change-me');\n\n");

//
// Instantiate the class
$s3 = new S3(awsAccessKey, awsSecretKey);
S3::$useSSL = false;

foreach ($filesToSend as $f) {
	// If you want to use PECL Fileinfo for MIME types:
	//if (!extension_loaded('fileinfo') && @dl('fileinfo.so')) $_ENV['MAGIC'] = '/usr/share/file/magic';
                                                                                                  
	// List your buckets:
	//echo "S3::listBuckets(): ".print_r($s3->listBuckets(), 1)."\n";

	// Create a bucket with public read access
	//if ($s3->putBucket($bucketName, S3::ACL_PUBLIC_READ)) {
		echo "Created bucket {$bucketName}".PHP_EOL;

		// Put our file (also with public read access)
		if ($s3->deleteObject($bucketName, $ssPath . baseName($f))) {
			echo "S3::deleteObject(): Deleted file\n";
		} else {
			//echo "S3::putObjectFile(): Failed to copy file\n";
		} 
	//} else {
	//	echo "S3::putBucket(): Unable to create bucket (it may already exist and/or be owned by someone else)\n";
	//} 
}

?>