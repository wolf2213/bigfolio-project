<?

/*

RUBIX
texture_file_name,
title_font_modifier,
nav_font_modifier,
page_font_modifier,

container_opacity,
max_content_width,
max_content_height,
mobile_width,

slideshow_speed,
auto_start_slideshow,

logo_alignment,
logo_top_margin,
logo_bottom_margin,
logo_left_margin,
logo_right_margin,

pages_menu_label,
condense_pages,
contact_menu_label,
redirect_target,
menu_position,
menu_alignment,
menu_top_margin,
menu_right_margin,
menu_bottom_margin,
menu_left_margin,

gallery_mode,
image_top_margin,
image_right_margin,
image_bottom_margin,
image_left_margin,
image_border_thickness,
gallery_image_spacer,

thumbnail_mode,
thumbnails_top_margin,
thumbnails_right_margin,
thumbnails_bottom_margin,
thumbnails_left_margin,

page_top_margin,
page_right_margin,
page_bottom_margin,
page_left_margin,
max_page_width,

footer_top_margin,
footer_right_margin,
footer_bottom_margin,
footer_left_margin

*/

switch ($v) {
	
	/* container/mobile settings */
	case "container_opacity":
		$arr['type'] = "slider";
		$arr['vals'] = "0,100";
		$arr['tip'] = "This sets the opacity of the main container. Opacity is not applied to the mobile site for performance reasons.";
		break;
		
    case "max_content_width":
		$arr['type'] = "slider";
		$arr['vals'] = "640,2500";
		$arr['tip'] = "This sets the maximum width of the main container.";
        break;

    case "max_content_height":
		$arr['type'] = "slider";
		$arr['vals'] = "360,1400";
		$arr['tip'] = "This sets the maximum height of the main container.";
        break;

	case "mobile_width":
		$arr['type'] = "slider";
		$arr['vals'] = "0,1024";
		$arr['tip'] = "This sets when the mobile site should show. We recommend setting this to 600 or 700 pixels to target phones and small tablets. Setting this to 0 will make the mobile site never show.";
		break;

	/* slideshow settings */
    case "slideshow_speed":
		$arr['type'] = "slider";
		$arr['vals'] = "2,20";
		$arr['tip'] = "This setting controls the speed of intro and gallery slideshows. This is the amount of time in seconds an image shows before the next image loads. Loading times for individual images may vary.";
        break;

	case "auto_start_slideshow":
		$arr['type'] = "radio";
		$arr['vals'] = array('yes','no');
		$arr['labels'] = array('On','Off');
		$arr['tip'] = "If turned on, this will start the slideshow automatically when entering a gallery.";
		break;
		
	
	/* logo settings */	
	case "logo_alignment":   
		$arr['type'] = "radio";
		$arr['vals'] = array('left','center','right');
		$arr['labels'] = array('Left','Center','Right');
		$arr['tip'] = "This setting controls the alignment of logo.";
		break;
	
	case "logo_top_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin above the logo.";
		break;

	case "logo_right_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin to the right of the logo.";
		break;

	case "logo_bottom_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin below the logo.";
		break;

	case "logo_left_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin between to the left of the logo.";
		break;	
	
	
	/* menu settings */
	case "redirect_target":
		$arr['type'] = "dropdown";
		$arr['vals'] = array('_blank','_self','bfRedirect');
		$arr['labels'] = array('new window','same window','all redirects in the same new window');
		$arr['tip'] = "This setting controls whether page redirects open in a new window, the same window, or all redirects in the same new window.";
		break;
		
	case "pages_menu_label":
		$arr['type'] = "text";
		$arr['tip'] = "The value entered here is used in page link urls and also will show on the main menu of condense pages is set to 'Yes'.";
		break;

	case "condense_pages": 
		$arr['type'] = "radio";
		$arr['vals'] = array('1','0');
		$arr['labels'] = array('Yes','No');
		$arr['tip'] = "If set to yes, all top level pages will be condensed under one page heading. The label for this is set in the pages menu label setting.";
		break;
	
	case "contact_menu_label":
		$arr['type'] = "text";
		$arr['tip'] = "The value entered here will be used in the main menu for the contact page and also the contact page title.";
		break;
		
	case "menu_position":
		$arr['type'] = "hidden"; /* in layout section */
		break;

	case "menu_alignment":   
		$arr['type'] = "hidden"; /* in layout section */
		break;

	case "menu_top_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin above the logo.";
		break;

	case "menu_right_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin to the right of the logo.";
		break;

	case "menu_bottom_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin below the logo.";
		break;

	case "menu_left_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin between to the left of the logo.";
		break;
		
	case "menu_divider":
		$arr['type'] = "text";
		$arr['tip'] = "A character entered in this field will be placed between each menu item. To have a line between each menu item, type in line.";
		break;
		
	case "auto_hide_menu":
		$arr['type'] = "radio";
		$arr['vals'] = array('1','0');
		$arr['labels'] = array('Yes','No');
		$arr['tip'] = "If set to yes, this will auto hide the menu when viewing a gallery.";
		break;
	
	
	/* gallery settings */
	case "gallery_mode":
		$arr['type'] = "hidden"; /* in layout section */
		break;
		
	case "gallery_index_pages":
		$arr['type'] = "radio";
		$arr['vals'] = array('1','0');
		$arr['labels'] = array('On','Off');
		$arr['tip'] = "If set to on, gallery indexes will show for each category of galleries. If set to off, a submenu listing each gallery within that category will show below that category in the menu.";
		break;
	
	case "gallery_index_thumbnail_width":
		$arr['type'] = "slider";
		$arr['vals'] = "0,900";
		$arr['tip'] = "This setting controls maximum width of gallery index page thumbnails. Gallery index page thumbnails are scaled down proportionally to fit within the available space.";
		break;
		
	case "gallery_index_thumbnail_height":
		$arr['type'] = "slider";
		$arr['vals'] = "0,600";
		$arr['tip'] = "This setting controls maximum height of gallery index page thumbnails. Gallery index page thumbnails are scaled down proportionally to fit within the available space.";
		break;
		
	case "gallery_index_thumbnail_padding":
		$arr['type'] = "slider";
		$arr['vals'] = "0,50";
		$arr['tip'] = "This setting controls amount of space between gallery index page thumbnails.";
		break;
		
	case "image_top_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin above gallery and home images";
		break;
		
	case "image_right_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin to the right of gallery and home images.";
		break;
		
	case "image_bottom_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin below gallery and home images.";
		break;
		
	case "image_left_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin to the left of gallery and home images.";
		break;
		
	case "image_border_thickness": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,20";
		$arr['tip'] = "This setting controls the thickness of gallery and home image borders.";
		break;
		
	case "gallery_image_spacer": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls the space to the left and right of each gallery image when using a horizontal gallery or the space above and below each gallery image when using a vertical gallery.";
		break;
		
	case "gallery_nav":
		$arr['type'] = "radio";
		$arr['vals'] = array('1','0');
		$arr['labels'] = array('On','Off');
		$arr['tip'] = "If set to On, the gallery navigation arrows will show. If set to Off the gallery navigation arrows will not show.";
		break;
		
		
	/* thumbnail settings */
	case "thumbnail_mode":
		$arr['type'] = "hidden"; /* in layout section */
		break;
		
	case "thumbnails_top_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin above gallery thumbnails.";
		break;

	case "thumbnails_right_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin to the right of gallery thumbnails.";
		break;

	case "thumbnails_bottom_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin below gallery thumbnails.";
		break;

	case "thumbnails_left_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin to the left of gallery thumbnails.";
		break;
		
	
	
	/* page settings */
	case "page_top_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin above pages.";
		break;

	case "page_right_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin to the right of pages.";
		break;

	case "page_bottom_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin below pages.";
		break;

	case "page_left_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin to the left of pages.";
		break;
		
	case "max_page_width": 
		$arr['type'] = "slider";
		$arr['vals'] = "400,1000";
		$arr['tip'] = "This setting controls maximum width of all pages. Pages scale down to fit within the available window space  if the page window is not as wide as this setting.";
		break;
		
		
	case "footer_top_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin above the footer area.";
		break;

	case "footer_right_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin to the right of the footer area.";
		break;

	case "footer_bottom_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin below the footer area.";
		break;

	case "footer_left_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin to the left of the footer area.";
		break;
		
	case "social_media_placement":
		$arr['type'] = "radio";
		$arr['vals'] = array('menu','footer');
		$arr['labels'] = array('menu','footer');
		break;
		
	default: 
		$arr['type'] = "text";
}

?>