<?

/*
BOND
texture_file_name, -hidden

content_width,
content_height,
app_alpha_0_to_100,
intro_image_padding,
gallery_image_padding,
show_main_border,
show_intro_image_border,
show_gallery_image_border,
show_page_image_border,
show_title_reflections,

portfolio_menu_label,
pages_menu_label,
contact_menu_label,

condense_pages,
slideshow_speed,
auto_start_slide_show,

calendar_text,
calendar_title,
unavailable_dates,
title_font_modifier,
nav_font_modifier,
page_font_modifier,

social_media_orientation,
rounded_corners,
menu_orientation,
menu_position,
menu_item_boxes,
menu_item_box_opacity,
menu_item_box_width,
menu_item_random_tilt,
menu_spacer,
sub_menu_spacer,
mouse_following_gallery_navigation

*/

switch ($v) {
	
	case "content_width":
		$arr['type'] = "slider";
		$arr['vals'] = "640,2560";
		$arr['tip'] = "This sets the width of the main container.";
        break;

    case "content_height":
		$arr['type'] = "slider";
		$arr['vals'] = "400,1600";
		$arr['tip'] = "This sets the height of the main container.";
        break;

	case "app_alpha_0_to_100":
		$arr['type'] = "slider";
		$arr['vals'] = "0,100";
		$arr['tip'] = "This sets the opacity of the main container background.";
		break;
		
	case "intro_image_padding":
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This sets the amount of padding in pixels between intro images and the container border";
		break;
		
	case "gallery_image_padding":
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This sets the amount of padding in pixels between gallery images and the container border";
		break;
		
	case "show_main_border":
		$arr['type'] = "radio";
		$arr['vals'] = array('yes','no');
		$arr['labels'] = array('yes','no');
		break;
		
	case "show_intro_image_border":
		$arr['type'] = "radio";
		$arr['vals'] = array('yes','no');
		$arr['labels'] = array('yes','no');
		break;
		
	case "show_gallery_image_border":
		$arr['type'] = "radio";
		$arr['vals'] = array('yes','no');
		$arr['labels'] = array('yes','no');
		break;
		
	case "show_page_image_border":
		$arr['type'] = "radio";
		$arr['vals'] = array('yes','no');
		$arr['labels'] = array('yes','no');
		break;
		
	case "show_title_reflections":
		$arr['type'] = "radio";
		$arr['vals'] = array('yes','no');
		$arr['labels'] = array('yes','no');
		break;
		
	case "condense_pages":
		$arr['type'] = "radio";
		$arr['vals'] = array('yes','no');
		$arr['labels'] = array('yes','no');
		break;
		
    case "slideshow_speed":
		$arr['type'] = "slider";
		$arr['vals'] = "2,20";
		$arr['tip'] = "This setting controls the speed of intro and gallery slideshows. This is the amount of time in seconds an image shows before the next image loads. Loading times for individual images may vary.";
        break;

	case "auto_start_slide_show":
		$arr['type'] = "radio";
		$arr['vals'] = array('yes','no');
		$arr['labels'] = array('On','Off');
		$arr['tip'] = "If turned on, this will start the slideshow automatically when entering a gallery.";
		break;
	
	case "social_media_orientation":
		$arr['type'] = 'radio';
		$arr['vals'] = array('horizontal','vertical');
		$arr['labels'] = array('horizontal','vertical');
		$arr['tip'] = "If set to horizontal, social media icons will sit underneath the copyright information. If set to vertical, social media icons will be stacked vertically, to the left of main container.";
		break;
		
	case "rounded_corners":
		$arr['type'] = "radio";
		$arr['vals'] = array('0','2','4');
		$arr['labels'] = array('off','2','4');
		$arr['tip'] = "This feature rounds the corners of the main container. If set to off, no corners will be rounded. If set to 2, two of the four corners will be rounded. If set to 4, all four corners will be rounded.";
		break;
		
	case "menu_orientation":
		$arr['type'] = "radio";
		$arr['vals'] = array('horizontal','vertical');
		$arr['labels'] = array('horizontal','vertical');
		$arr['tip'] = "Setting the menu orientation to horizontal will display the menu items left to right. Setting the menu orientation to vertical will display the menu items top to bottom.";
		break;
		
	case "menu_position":
		$arr['type'] = "radio";
		$arr['vals'] = array('top left','top right','bottom left','bottom right');
		$arr['labels'] = array('top left','top right','bottom left','bottom right');
		break;
		
	case "menu_item_boxes":
		$arr['type'] = "radio";
		$arr['vals'] = array('on','off');
		$arr['labels'] = array('on','off');
		$arr['tip'] = "If turned on, this will activate a background box around each menu item.";
		break;
		
	case "menu_item_box_opacity":
		$arr['type'] = "slider";
		$arr['vals'] = "0,100";
		$arr['tip'] = "This sets the opacity of the menu item boxes on both the main and sub menus.";
		break;
		
	case "menu_item_box_width":
		$arr['type'] = "radio";
		$arr['vals'] = array('static','dynamic');
		$arr['labels'] = array('static','dynamic');
		$arr['tip'] = "If set to static, all menu item boxes on the main menu will be same width. If set to dynamic, all menu item boxes on the main menu will be dynamically set by the width of the menu item text.";
		break;
		
	case "menu_item_random_tilt":
		$arr['type'] = "radio";
		$arr['vals'] = array('on','off');
		$arr['labels'] = array('on','off');
		$arr['tip'] = "If set on, menu items will be randomly tilted.";
		break;
		
	case "menu_spacer": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,50";
		$arr['tip'] = "This sets the amount of space between each main menu item.";
		break;
	
	case "sub_menu_spacer": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,50";
		$arr['tip'] = "This sets the amount of space between each sub menu item.";
		break;
		
	case "mouse_following_gallery_navigation":
		$arr['type'] = "radio";
		$arr['vals'] = array("arrows","+-","none");
		$arr['labels'] = array("arrows","plus/minus","none");
		$arr['tip'] = "This sets the cursor to be left and right arrows if set to arrows, plus and minus icons if set to plus/minus, and nothing if set to none.";
		break;

	default: 
		$arr['type'] = "text";
}
	
?>