<?

/*
NAKOMA
title_font_modifier,
nav_font_modifier,
page_font_modifier,

slideshow_speed,
page_bg_opacity,

texture_file_name,

image_border,
image_border_thickness,
image_border_mode,
raised_corners,
condense_pages,

pages_menu_title,

menu_position,
auto_start_slideshow
*/

switch ($v) { 
	
	case "slideshow_speed":
		$arr['type'] = "slider";
		$arr['vals'] = "2,20";
		$arr['tip'] = "This setting controls the speed of intro and gallery slideshows. This is the amount of time in seconds an image shows before the next image loads. Loading times for individual images may vary.";
	    break; 
	
	case "page_bg_opacity": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,100";
		$arr['tip'] = "This setting controls the opacity of the page background.";
		break;
		
   	case "image_border":
		$arr['type'] = "radio";
		$arr['vals'] = array('on','off');
		$arr['labels'] = array('on','off');
		break; 

	case "image_border_thickness":  
		$arr['type'] = "slider";
		$arr['vals'] = "0,20";
		$arr['tip'] = "This setting controls the width of image borders if turned on";
		break;
		
    case "image_border_mode": 
		$arr['type'] = "radio";
		$arr['vals'] = array('fill','outline');
		$arr['labels'] = array('fill','outline');
		$arr['tip'] = "Selecting fill fills the space between the edge of the image and the border thickness setting. Selecting outline makes a 1 pixel outline with a distance from the edge of the image determined by the image border thickness setting.";
		break; 
		
	case "raised_corners":  
		$arr['type'] = "radio";
		$arr['vals'] = array('on','off');
		$arr['labels'] = array('on','off');
		$arr['tip'] = "This setting turns the raised corner effect on or off throughout the site.";
		break;
		
	case "raised_corners":  
		$arr['type'] = "radio";
		$arr['vals'] = array('on','off');
		$arr['labels'] = array('on','off');
		$arr['tip'] = "This setting turns the raised corner effect on or off throughout the site.";
		break;
		
    case "condense_pages":
		$arr['type'] = "radio";
		$arr['vals'] = array('yes','no');
		$arr['labels'] = array('yes','no');
		$arr['tip'] = "Setting condense pages to yes will place all non-contact form top-level pages under a single menu heading the same way galleries are placed under category headings.";
		break;
		
    case "condense_pages":
		$arr['type'] = "radio";
		$arr['vals'] = array('yes','no');
		$arr['labels'] = array('yes','no');
		$arr['tip'] = "Setting condense pages to yes will place all non-contact form top-level pages under a single menu heading the same way galleries are placed under category headings.";
		break;
		
    case "menu_position": 
		$arr['type'] = "radio";
		$arr['vals'] = array('top','bottom');
		$arr['labels'] = array('top','bottom');
		break;
		
	case "auto_start_slideshow":
		$arr['type'] = "radio";
		$arr['vals'] = array('yes','no');
		$arr['labels'] = array('on','off');
		$arr['tip'] = "If turned on, this will start the slideshow automatically when entering a gallery.";
		break;  	
		
	default: 
		$arr['type'] = "text"; //pages_menu_title
	
	
	
}
	
?>