<?

/*
BANDON
title_font_modifier,
nav_font_modifier,
page_font_modifier,
texture_file,

use_gallery_color_matching,
color_matching_opacity,
allow_fullscreen_button,

contact_label,
blog_url,
blog_label,
proofing_label,

*/

switch ($v) {
	
	case "use_gallery_color_matching":
		$arr['type'] = "radio";
		$arr['vals'] = array('yes','no');
		$arr['labels'] = array('yes','no');
		$arr['tip'] = "If turned on, the container will be colored by the center of the current gallery image.";
		break;

	case "color_matching_opacity":
		$arr['type'] = "slider";
		$arr['vals'] = "0,100";
		$arr['tip'] = "This sets the opacity of the gallery color matching feature if turned on.";
		break;
		
	case "allow_fullscreen_button":
		$arr['type'] = "radio";
		$arr['vals'] = array('true','false');
		$arr['labels'] = array('yes','no');
		break;

	default: 
		$arr['type'] = "text";
}
	
?>