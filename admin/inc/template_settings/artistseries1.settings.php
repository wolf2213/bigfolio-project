<?

/*
ARTIST SERIES
title_font_modifier, -hidden
nav_font_modifier, -hidden
page_font_modifier, -hidden
texture_file_name, -hidden

app_opacity, 
rounded_corners,
window_padding,
content_padding,
menu_position,
logo_position,
show_main_border,
show_image_border,
image_border_thickness,
use_mouse_following_gallery_arrows,
thumbnail_mode,
thumbnail_colors,
transition_time,
transition_type,
slideshow_speed,
auto_start_slideshow,
show_grass,
grass_color,
show_birds,
birds_color,
show_flowers,
flowers_colors,
show_tree,
tree_colors,
show_clouds,
clouds_colors,
show_balloons,
balloons_colors,
show_kite,
kite_colors,
show_clothesline,
clothes_colors,
show_wagon,
wagon_colors,
show_pinwheels,
pinwheels_colors,
use_animations_universally
*/

switch ($v) {

case "app_opacity":
	$arr['type'] = "slider";
	$arr['vals'] = "0,100";
	$arr['tip'] = "Select the desired opacity for the main container. 0 sets the container to be completely transparent. 100 sets the container to be completely opaque.";
    break;

case "rounded_corners":
	$arr['type'] = "radio";
	$arr['vals'] = array('yes','no');
	$arr['labels'] = array('On','Off');
	$arr['tip'] = "If turned on, this will round the corners of the container and images within the container.";
	break;

case "window_padding":
	$arr['type'] = "slider";
	$arr['vals'] = "0,200";
	$arr['tip'] = "Enter the desired number of padding in pixels between the edge of the browser window or container div if used.";
    break;

case "content_padding":
	$arr['type'] = "slider";
	$arr['vals'] = "0,100";
	$arr['tip'] = "Enter the desired number of padding in pixels between gallery images, intro images, and page content and the main content area background.";
	break;
	
case "menu_position":
	$arr['type'] = "radio";
	$arr['vals'] = array('left','right');
	$arr['labels'] = array('left','right');
	$arr['tip'] = "The menu can be positioned on the left or right side of the main content area. Select left or right.";
	break;
	
case "logo_position":
	$arr['type'] = "radio";
	$arr['vals'] = array('above','adjacent');
	$arr['labels'] = array('above','adjacent');
	$arr['tip'] = "The logo can be positioned either above or adjacent to the container. Select 'above' in the Logo position field to place the logo above the main content container. Select 'adjacent' to place the logo next to the main content container.";
	break;
	
case "show_main_border":
	$arr['type'] = "radio";
	$arr['vals'] = array('yes','no');
	$arr['labels'] = array('yes','no');
	break;
	
case "show_image_border":
	$arr['type'] = "radio";
	$arr['vals'] = array('yes','no');
	$arr['labels'] = array('yes','no');
	break;
	
case "image_border_thickness":
	$arr['type'] = "slider";
	$arr['vals'] = "0,20";
	$arr['tip'] = "Select the desired thickness in pixels for image borders.";
	break;
	
case "thumbnail_mode":
	$arr['type'] = "dropdown";
	$arr['vals'] = array('solid circles','solid squares','image circles', 'image squares');
	$arr['labels'] = array('solid circles','solid squares','image circles', 'image squares');
	$arr['tip'] = "There are four different thumbnail modes available. Select 'image squares' for a 3 column view of square images, 'image circles', for a 3 column of circle images, 'solid circles' for an 8 column view of solid color circles, or 'solid squares' for an 8 column view of solid color squares.";
	break;
	
case "thumbnail_colors":
	$arr['type'] = "colors";
	$arr['numColors'] = 10;
	$arr['tip'] = "If the thumbnail mode is set to either &apos;solid squares&apos; or &apos;solid circles&apos;, custom colors can be used.";				
	break;
	
case "use_mouse_following_gallery_arrows":
	$arr['type'] = "radio";
	$arr['vals'] = array('yes','no');
	$arr['labels'] = array('yes','no');
	break;
	
case "transition_time":
	$arr['type'] = "slider";
	$arr['vals'] = "1,20";
	$arr['tip'] = "Enter the desired transition time in seconds for intro and gallery images.";
    break;

case "transition_type":
	$arr['type'] = "radio";
	$arr['vals'] = array('wipe','slide','fade');
	$arr['labels'] = array('wipe','slide','fade');
	break;

case "slideshow_speed":
	$arr['type'] = "slider";
	$arr['vals'] = "2,20";
	$arr['tip'] = "This setting controls the speed of intro and gallery slideshows. This is the amount of time in seconds an image shows before the next image loads. Loading times for individual images may vary.";
    break;

case "auto_start_slideshow":
	$arr['type'] = "radio";
	$arr['vals'] = array('yes','no');
	$arr['labels'] = array('On','Off');
	$arr['tip'] = "If turned on, this will start the slideshow automatically when entering a gallery.";
	break; 
	
case "show_contact_form_borders":  
	$arr['type'] = "radio";
	$arr['vals'] = array('yes','no');
	$arr['labels'] = array('yes','no');
	break;
	
case "show_grass":
	$arr['type'] = "radio";
	$arr['vals'] = array('yes','no');
	$arr['labels'] = array('yes','no');
	break;
	
case "grass_color":
	$arr['type'] = "color";
	break;
	
case "show_birds":
	$arr['type'] = "radio";
	$arr['vals'] = array('yes','no');
	$arr['labels'] = array('yes','no');
	break;
	
case "birds_color":
	$arr['type'] = "color";
	break;
	
case "show_flowers":
	$arr['type'] = "radio";
	$arr['vals'] = array('yes','no');
	$arr['labels'] = array('yes','no');
	break;
	
case "flowers_colors":
	$arr['type'] = "colors";
	$arr['numColors'] = 4;
	$arr['tip'] = "The first color colors the flower outline and stem. The second, third, and fourth colors color the fill colors of the three flowers.";
	break;
	
case "show_tree":
	$arr['type'] = "radio";
	$arr['vals'] = array('yes','no');
	$arr['labels'] = array('yes','no');
	break;
	
case "tree_colors":
	$arr['type'] = "colors";
	$arr['numColors'] = 2;
	$arr['tip'] = "The first color colors the tree trunk, and the second colors the leaves.";
	break;
	
case "show_clouds":
	$arr['type'] = "radio";
	$arr['vals'] = array('yes','no');
	$arr['labels'] = array('yes','no');
	break;
	
case "clouds_colors":
	$arr['type'] = "colors";
	$arr['numColors'] = 2;
	$arr['labels'] = "The first color colors the cloud outlines, and the second color colors the cloud fills.";
	break;
	
case "show_balloons":
	$arr['type'] = "radio";
	$arr['vals'] = array('yes','no');
	$arr['labels'] = array('yes','no');
	break;
	
case "balloons_colors":
	$arr['type'] = "colors";
	$arr['numColors'] = 4;
	$arr["tip"] = "The first color colors the balloon outlines and strings. The second, third, and fourth colors color the fill colors of the three balloons.";
	break;
	
case "show_kite":
	$arr['type'] = "radio";
	$arr['vals'] = array('yes','no');
	$arr['labels'] = array('yes','no');
	break;
	
case "kite_colors":
	$arr['type'] = "colors";
	$arr['numColors'] = 5;
	$arr['tip'] = 'The first color colors the outline elements throughout the kite element. The second, third, forth, and fifth colors color the individual colored elements throughout the kite body and tail elements.';
	break;
	
case "show_clothesline":
	$arr['type'] = "radio";
	$arr['vals'] = array('yes','no');
	$arr['labels'] = array('yes','no');
	break;
	
case "clothes_colors":
	$arr['type'] = "colors";
	$arr['numColors'] = 6;
	$arr['tip'] = "The first color colors the line and pin elements. The second, third, forth, and fifth colors color the individual pieces of clothing in the following order: shirt, socks, underwear, bikini bottom, bikini top, and boxer shorts. Only one color is used for each piece of clothing if a custom color is used.";
	break;
	
case "show_wagon":
	$arr['type'] = "radio";
	$arr['vals'] = array('yes','no');
	$arr['labels'] = array('yes','no');
	break;
	
case "wagon_colors":
	$arr['type'] = "colors";
	$arr['numColors'] = 6;
	$arr['tip'] = "The first color colors the wagon handle bar, body, and inner wheel rings. The second color colors the outer color of each wheel.";
	break;
	
case "show_pinwheels":
	$arr['type'] = "radio";
	$arr['vals'] = array('yes','no');
	$arr['labels'] = array('yes','no');
	break;
	
case "pinwheels_colors":
	$arr['type'] = "colors";
	$arr['numColors'] = 5;
	break;
	
case "use_animations_universally":
	$arr['type'] = "radio";
	$arr['vals'] = array('yes','no');
	$arr['labels'] = array('yes','no');
	break;
	
default: 
	$arr['type'] = "text";

}
	
?>