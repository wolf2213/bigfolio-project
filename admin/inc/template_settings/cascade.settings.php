<?

/*

CASCADE
slideshow_speed,

portfolio_page_title,contact_page_title,

calendar_title,calendar_text,unavailable_dates,

show_page_titles,
allow_fullscreen_button,

texture_file,
page_background_alpha,

title_font_modifier,
nav_font_modifier,
page_font_modifier,

raised_corners

*/

switch ($v) {   

    case "slideshow_speed":
		$arr['type'] = "slider";
		$arr['vals'] = "2,20";
		$arr['tip'] = "This setting controls the speed of intro and gallery slideshows. This is the amount of time in seconds an image shows before the next image loads. Loading times for individual images may vary.";
        break;

	case "show_page_titles":
		$arr['type'] = "radio";
		$arr['vals'] = array('yes','no');
		$arr['labels'] = array('yes','no');
		break; 

  	case "allow_fullscreen_button":
		$arr['type'] = "radio";
		$arr['vals'] = array('true','false');
		$arr['labels'] = array('yes','no');
		break; 
		
	case "page_background_alpha": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,100";
		$arr['tip'] = "This sets the opacity of the page background rectangle.";
        break;

	case "raised_corners":
		$arr['type'] = "radio";
		$arr['vals'] = array('on','off');
		$arr['labels'] = array('on','off');
		break;
		
	default: 
		$arr['type'] = "text"; // portfolio_page_title,contact_page_title,

}
	
?>