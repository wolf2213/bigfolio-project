<?

/*
NEWPORT
content_width,
content_height,
logo_position,
vertical_offset,

texture_file_name,

decoration_type,

menu_label,
contact_page_label,

slideshow_speed,

calendar_title,
calendar_text,
unavailable_dates,
title_font_modifier,
nav_font_modifier,
page_font_modifier,

menu_type,
initial_menu_position,
app_opacity,

menu_bar_texture_file_name,

auto_start_slideshow
*/

switch ($v) {
	
	case "content_width":
		$arr['type'] = "slider";
		$arr['vals'] = "640,2560";
		$arr['tip'] = "This sets the width of the main container.";
        break;

    case "content_height":
		$arr['type'] = "slider";
		$arr['vals'] = "400,1600";
		$arr['tip'] = "This sets the height of the main container.";
        break; 

	case "logo_position": 
		$arr['type'] = "radio";
		$arr['vals'] = array('left','center','right');
		$arr['labels'] = array('left','center','right');      
		break;
		
	case "vertical_offset": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,100";
		$arr['tip'] = "This sets the distance in pixels from the top of the window to the top of the container";
	    break;    
	
	case "decoration_type":  
		$arr['type'] = "radio";
		$arr['vals'] = array('0','1','2');
		$arr['labels'] = array('none','1','2');
		break;
   
	case "slideshow_speed":
		$arr['type'] = "slider";
		$arr['vals'] = "2,20";
		$arr['tip'] = "This setting controls the speed of intro and gallery slideshows. This is the amount of time in seconds an image shows before the next image loads. Loading times for individual images may vary.";
	    break;         
	          
	case "menu_type":  
		$arr['type'] = "radio";
		$arr['vals'] = array('original','new');
		$arr['labels'] = array('original','new');
		break;
	            
   	case "initial_menu_position": 
       	$arr['type'] = "slider";
		$arr['vals'] = "0,2000";
		$arr['tip'] = "This sets the vertical offset of the main menu button if 'new' is selected as the menu type.";  
		break;
		
	case "app_opacity":
		$arr['type'] = "slider";
		$arr['vals'] = "0,100";
		$arr['tip'] = "This sets the opacity of the main container background.";
        break; 
	
	case "auto_start_slideshow":
		$arr['type'] = "radio";
		$arr['vals'] = array('yes','no');
		$arr['labels'] = array('on','off');
		$arr['tip'] = "If turned on, this will start the slideshow automatically when entering a gallery.";
		break;  	
		
	default: 
		$arr['type'] = "text"; //pages_menu_title
	
	
	
}
	
?>