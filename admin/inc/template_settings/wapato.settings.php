<?

/*
WAPATO
auto_start_slideshow,
slideshow_speed,

title_font_modifier,
nav_font_modifier,
page_font_modifier,

content_width,
content_height,
content_spacing,
content_padding,
content_border_type,
thumb_padding,
page_content_padding,
page_content_margin,
logo_x_position,
logo_y_position,
arrow_style_type,

menu_text,

menu_spacer,
menu_mode,
auto_close_menu,
thumbnail_layout_type,
contact_form_button_type,

texture_file_name
*/

switch ($v) { 
	
case "auto_start_slideshow":
	$arr['type'] = "radio";
	$arr['vals'] = array('yes','no');
	$arr['labels'] = array('On','Off');
	$arr['tip'] = "If turned on, this will start the slideshow automatically when entering a gallery. blah blah blah";
	break; 
	
case "slideshow_speed":
	$arr['type'] = "slider";
	$arr['vals'] = "2,20";
	$arr['tip'] = "This setting controls the speed of intro and gallery slideshows. This is the amount of time in seconds an image shows before the next image loads. Loading times for individual images may vary.";
    break;

case "content_width":
	$arr['type'] = "slider";
	$arr['vals'] = "640,2560";
	$arr['tip'] = "This sets the width of the main container.";
	break; 

case "content_height":
	$arr['type'] = "slider";
	$arr['vals'] = "400,1600";
	$arr['tip'] = "This sets the height of the main container.";
	break;

case "content_padding":
	$arr['type'] = "slider";
	$arr['vals'] = "0,100";
	$arr['tip'] = "Enter the desired amount of padding in pixels between the edge of each content item and inner gallery images.";
	break;
	
case "content_spacing":
	$arr['type'] = "slider";
	$arr['vals'] = "0,100";
	$arr['tip'] = "Enter the desired amount of space in pixels between content sections.";
	break;  
	
case "content_border_type": 
	$arr['type'] = "radio";
	$arr['vals'] = array('0','1','2');
	$arr['labels'] = array('none','1','2');
	break;

case "thumb_padding":
	$arr['type'] = "slider";
	$arr['vals'] = "0,100";
	$arr['tip'] = "Enter the desired amount of padding in pixels between the edge of each content item and gallery thumbnails.";
	break;  
	
case "page_content_padding":
	$arr['type'] = "slider";
	$arr['vals'] = "0,100";
	$arr['tip'] = "Enter the desired amount of padding in pixels between page items.";
	break;

case "page_content_margin":
	$arr['type'] = "slider";
	$arr['vals'] = "0,100";
	$arr['tip'] = "Enter the desired amount of distance in pixels between the edge of the content container internal page text.";
	break; 
	
case "logo_x_position":
	$arr['type'] = "slider";
	$arr['vals'] = "0,200";
	$arr['tip'] = "This setting controls horizontal position of the logo.";
	break; 

case "logo_y_position":
	$arr['type'] = "slider";
	$arr['vals'] = "0,200";
	$arr['tip'] = "This setting controls vertical position of the logo.";
	break;
	
case "arrow_style_type":  
	$arr['type'] = "radio";
	$arr['vals'] = array('all','1','2','3','4','5','6','7');
	$arr['labels'] = array('all','1','2','3','4','5','6','7');
	$arr['tip'] = "Select an arrow style. Select all to cycle through all styles.";
	break;                  
	
case "menu_spacer":
	$arr['type'] = "slider";
	$arr['vals'] = "1,50";
	$arr['tip'] = "Enter the amount of space in pixels between each menu item.";
	break;	
	
case "menu_mode":
	$arr['type'] = "radio";
	$arr['vals'] = array('image','text');
	$arr['labels'] = array('image','text');
	$arr['tip'] = "Select 'image' to use .png images for the menu instead of text using the navigation font. Menu images should be transparent .png files and uploaded to the image bank. The file names should match the category or page title. For example, to use an image for a page named “About Us”, the associated menu item image would need to be named “About Us.png” and uploaded to the image bank. We recommend not having menu item images wider than 300 pixels or taller than 100 pixels.";
	break;
	
case "auto_close_menu":  
	$arr['type'] = "radio";
	$arr['vals'] = array('yes','no');
	$arr['labels'] = array('yes','no');
	$arr['tip'] = "Selecting yes will close the menu when the mouse is not over the menu. Selecting no will make the menu always stay open.";
    break;   

case "thumbnail_layout_type": 	
	$arr['type'] = "radio";
	$arr['vals'] = array('yes','no');
	$arr['labels'] = array('yes','no');
	$arr['tip'] = "There are two types of thumbnail layouts, 1 and 2. Thumbnail layout 1 contains several grids with varying thumbnail sizes and can accommodate 1-4 galleries per category. Thumbnail layout 2 shows a grid overlay on the thumbnail layout and displays square thumbnails.";
	break;

case "contact_form_button_type": 
	$arr['type'] = "radio";
	$arr['vals'] = array('icon','text');
	$arr['labels'] = array('icon','text'); 
	$arr['tip'] = "To use the built in phone icon for the contact form send button, select ‘icon’.";
	break;
	
default: 
	$arr['type'] = "text";

}
	
?>