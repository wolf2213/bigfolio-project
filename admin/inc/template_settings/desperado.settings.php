<?

/*

DESPERADO
title_font_modifier, -hidden
nav_font_modifier, -hidden
page_font_modifier, -hidden
texture_file_name, -hidden

content_width,
slideshow_speed,
auto_start_slideshow,
redirect_target,
featured_category,
browser_scroll_mode,
intro_video,
portfolio_menu_label,
contact_menu_label,

*/

switch ($v) {
    case "content_width":
		$arr['type'] = "slider";
		$arr['vals'] = "640,2560";
		$arr['tip'] = "This sets the maximum width of the main container.";
        break;

    case "slideshow_speed":
		$arr['type'] = "slider";
		$arr['vals'] = "2,20";
		$arr['tip'] = "This setting controls the speed of intro and gallery slideshows. This is the amount of time in seconds an image shows before the next image loads. Loading times for individual images may vary.";
        break;

	case "auto_start_slideshow":
		$arr['type'] = "radio";
		$arr['vals'] = array('yes','no');
		$arr['labels'] = array('On','Off');
		$arr['tip'] = "If turned on, this will start the slideshow automatically when entering a gallery.";
		break;
		
	case "redirect_target":
		$arr['type'] = "dropdown";
		$arr['vals'] = array('_self','_blank','bfRedirect');
		$arr['labels'] = array('same window','new window','all redirects in the same new window');
		break;
		
	case "featured_category":
		$arr['type'] = "dropdown";
		$catsArr = get_all_categories();
		$catLabels = array('None');
		$catVals = array('');
		foreach($catsArr as $c) {
			$catLabels[] = $c['category_name'];
			$catVals[] = $c['category_name'];
		}
		$arr['vals'] = $catVals;
		$arr['labels'] = $catLabels;
		break;
		
	case "browser_scroll_mode":
		$arr['type'] = "radio";
		$arr['vals'] = array('auto', 'none');
		$arr['labels'] = array('auto', 'none');
		$arr['tip'] = "Select auto to use the native browser vertical scroll bar. Select none to make content fit within any browser window size. In-site scroll bars within the main container will be used on pages that have content taller than the available space.";
		break;
		
	case "show_thumbnails_by_default":
		$arr['type'] = "radio";
		$arr['vals'] = array('true','false');
		$arr['labels'] = array('On','Off');
		$arr['tip'] = "If turned on, gallery thumbnails will show by default when a gallery initially loads.";
		break;
		
	default: 
		$arr['type'] = "text";
}

?>