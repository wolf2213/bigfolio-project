<?

/*

YORK HTML5
texture_file_name,
portfolio_menu_label,
pages_menu_label,
contact_menu_label,
slideshow_speed,
auto_start_slideshow,
redirect_target,
title_font_modifier,
nav_font_modifier,
page_font_modifier,
condense_galleries,
condense_pages,
logo_alignment,
menu_alignment,
intro_transition,
max_content_width,
max_content_height,
maintain_aspect_ratio,
show_container,
image_top_margin,
image_right_margin,
image_bottom_margin,
image_left_margin,
image_border_thickness,
page_top_margin,
page_right_margin,
page_bottom_margin,
page_left_margin

*/

switch ($v) {

    case "slideshow_speed":
		$arr['type'] = "slider";
		$arr['vals'] = "2,20";
		$arr['tip'] = "This setting controls the speed of intro and gallery slideshows. This is the amount of time in seconds an image shows before the next image loads. Loading times for individual images may vary.";
        break;

	case "auto_start_slideshow":
		$arr['type'] = "radio";
		$arr['vals'] = array('yes','no');
		$arr['labels'] = array('On','Off');
		$arr['tip'] = "If turned on, this will start the slideshow automatically when entering a gallery.";
		break;
		
	case "redirect_target":
		$arr['type'] = "dropdown";
		$arr['vals'] = array('_self','_blank','bfRedirect');
		$arr['labels'] = array('same window','new window','all redirects in the same new window');
		break;
		
	case "condense_galleries": 
		$arr['type'] = "radio";
		$arr['vals'] = array('1','0');
		$arr['labels'] = array('Yes','No');
		$arr['tip'] = "If set to yes, all categories of galleries will be condensed under one portfolio heading. The label for this is set in the portfolio menu label setting.";
		break;
	
	case "condense_pages": 
		$arr['type'] = "radio";
		$arr['vals'] = array('1','0');
		$arr['labels'] = array('Yes','No');
		$arr['tip'] = "If set to yes, all top level pages will be condensed under one page heading. The label for this is set in the pages menu label setting.";
		break;
		
	case "logo_alignment":   
		$arr['type'] = "radio";
		$arr['vals'] = array('left','right','center');
		$arr['labels'] = array('Left','Right','Center');
		$arr['tip'] = "This setting controls the alignment of logo.";
		break;
		
	case "menu_alignment":   
		$arr['type'] = "radio";
		$arr['vals'] = array('left','right','center');
		$arr['labels'] = array('Left','Right','Center');
		$arr['tip'] = "This setting controls the alignment of menu.";
		break;
		
	case "intro_transition":
		$arr['type'] = "radio";
		$arr['vals'] = array('slide','fade');
		$arr['labels'] = array('Slide','Fade');
		$arr['tip'] = "If set to slide, intro images slide vertically. If set to fade, intro images crossfade.";
		break;
		
    case "max_content_width":
		$arr['type'] = "slider";
		$arr['vals'] = "640,2500";
		$arr['tip'] = "This sets the maximum width of the main container.";
        break;

    case "max_content_height":
		$arr['type'] = "slider";
		$arr['vals'] = "360,1400";
		$arr['tip'] = "This sets the maximum width of the main container.";
        break;

	case "maintain_aspect_ratio": 
		$arr['type'] = "radio";
		$arr['vals'] = array('true','false');
		$arr['labels'] = array('Yes','No');
		$arr['tip'] = "If set to yes, the content area container's aspect ratio will be respected as the site scales to fit within the available window space. If set to no, the width and height of the content area container will be determined independently. In both instances, the width and height of the content area container will not be larger than the max content width and height settings.";
		break;
		
	case "show_container": 
		$arr['type'] = "radio";
		$arr['vals'] = array('true','false');
		$arr['labels'] = array('Yes','No');
		$arr['tip'] = "If set to yes, the content area container will be visible. If set to no, the content area container will be hidden.";
		break;
		
	case "image_top_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin between the top of the browser window and the main content.";
		break;
		
	case "image_right_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin between the right edge of the browser window and the main content.";
		break;
		
	case "image_bottom_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin between the bottom of the browser window and the main content.";
		break;
		
	case "image_left_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin between the left edge of the browser window and the menu.";
		break;
		
	case "image_border_thickness": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,20";
		$arr['tip'] = "This setting controls the thickness of gallery and home image borders.";
		break;
		
		
	case "page_top_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin between the top of the browser window and the available area for all pages.";
		break;

	case "page_right_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin between the right edge of the browser window and the available area for all pages.";
		break;

	case "page_bottom_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin between the top of the footer area and the bottom of all pages.";
		break;

	case "page_left_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin between the left edge of the browser window and the available area for all pages.";
		break;
		
	default: 
		$arr['type'] = "text";
}

?>