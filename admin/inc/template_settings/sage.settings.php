<?

/*
SAGE
title_font_modifier,
nav_font_modifier,
page_font_modifier,
texture_file_name,

show_container,
container_aspect_ratio,
app_opacity,
window_padding,
content_padding,
menu_position,
menu_alignment,
logo_alignment,
show_image_border,
image_border_thickness,
use_mouse_following_gallery_arrows,
transition_time,
transition_type,
slideshow_speed,
auto_start_slideshow
*/

switch ($v) {

case "show_container": 
	$arr['type'] = "radio";
	$arr['vals'] = array('yes','no');
	$arr['labels'] = array('yes','no');
	$arr['tip'] = "If turned on, the flash site will be contained within an html container, showing a border around the flash site.";
	break;

case "container_aspect_ratio": 
	$arr['type'] = "radio";
	$arr['vals'] = array('3:2','4:3','16:9','relative');
	$arr['labels'] = array('3:2','4:3','16:9','relative');
	$arr['tip'] = "If the container is turned on, it can be set to have a fixed aspect ratio of 3:2, 4:3, or 16:9. To set the container width and height relative to the window, select relative.";
   	break;

case "app_opacity":
	$arr['type'] = "slider";
	$arr['vals'] = "0,100";
	$arr['tip'] = "Select the desired opacity for the main container. 0 sets the container to be completely transparent. 100 sets the container to be completely opaque.";
    break;

case "window_padding":
	$arr['type'] = "slider";
	$arr['vals'] = "0,200";
	$arr['tip'] = "Enter the desired number of padding in pixels between the edge of the browser window or container div if used.";
    break;

case "content_padding":
	$arr['type'] = "slider";
	$arr['vals'] = "0,100";
	$arr['tip'] = "Enter the desired number of padding in pixels between gallery images, intro images, and page content and the main content area background.";
	break;
	
case "menu_position":
	$arr['type'] = "radio";
	$arr['vals'] = array('top','bottom');
	$arr['labels'] = array('top','bottom');
	break;
	
case "menu_alignment":  
	$arr['type'] = "radio";
	$arr['vals'] = array('left','center', 'right');
	$arr['labels'] = array('left','center', 'right');
 	break;

case "logo_alignment":  
	$arr['type'] = "radio";
	$arr['vals'] = array('left','center', 'right');
	$arr['labels'] = array('left','center', 'right');
	break;
	
	
case "show_image_border":
	$arr['type'] = "radio";
	$arr['vals'] = array('yes','no');
	$arr['labels'] = array('yes','no');
	break;
	
case "image_border_thickness":
	$arr['type'] = "slider";
	$arr['vals'] = "0,20";
	$arr['tip'] = "Select the desired thickness in pixels for image borders.";
	break; 
	
case "use_mouse_following_gallery_arrows":
	$arr['type'] = "radio";
	$arr['vals'] = array('yes','no');
	$arr['labels'] = array('yes','no');
	break;  
	
case "transition_time":
	$arr['type'] = "slider";
	$arr['vals'] = "1,20";
	$arr['tip'] = "Enter the desired transition time in seconds for intro and gallery images.";
    break;

case "transition_type":
	$arr['type'] = "radio";
	$arr['vals'] = array('wipe','slide','fade');
	$arr['labels'] = array('wipe','slide','fade');
	break;

case "slideshow_speed":
	$arr['type'] = "slider";
	$arr['vals'] = "2,20";
	$arr['tip'] = "This setting controls the speed of intro and gallery slideshows. This is the amount of time in seconds an image shows before the next image loads. Loading times for individual images may vary.";
    break;

case "auto_start_slideshow":
	$arr['type'] = "radio";
	$arr['vals'] = array('yes','no');
	$arr['labels'] = array('On','Off');
	$arr['tip'] = "If turned on, this will start the slideshow automatically when entering a gallery.";
	break; 
	
default: 
	$arr['type'] = "text";

}
	
?>