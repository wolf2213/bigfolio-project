<?

/*

SHORELINE
texture_file_name,
portfolio_menu_label,
pages_menu_label,
contact_menu_label,
slideshow_speed,
auto_start_slideshow,
redirect_target,
title_font_modifier,
nav_font_modifier,
page_font_modifier,

condense_galleries,
condense_pages,
logo_alignment,

image_top_margin,
image_right_margin,
image_bottom_margin,
image_left_margin,
gallery_image_spacer,
image_view_mode,
image_border_thickness,

page_top_margin,page_right_margin,page_bottom_margin,page_left_margin,max_page_width

*/

switch ($v) {

    case "slideshow_speed":
		$arr['type'] = "slider";
		$arr['vals'] = "2,20";
		$arr['tip'] = "This setting controls the speed of intro and gallery slideshows. This is the amount of time in seconds an image shows before the next image loads. Loading times for individual images may vary.";
        break;

	case "auto_start_slideshow":
		$arr['type'] = "radio";
		$arr['vals'] = array('yes','no');
		$arr['labels'] = array('On','Off');
		$arr['tip'] = "If turned on, this will start the slideshow automatically when entering a gallery.";
		break;
		
	case "redirect_target":
		$arr['type'] = "dropdown";
		$arr['vals'] = array('_self','_blank','bfRedirect');
		$arr['labels'] = array('same window','new window','all redirects in the same new window');
		break;
		
	case "condense_galleries": 
		$arr['type'] = "radio";
		$arr['vals'] = array('1','0');
		$arr['labels'] = array('Yes','No');
		$arr['tip'] = "If set to yes, all categories of galleries will be condensed under one portfolio heading. The label for this is set in the portfolio menu label setting.";
		break;
	
	case "condense_pages": 
		$arr['type'] = "radio";
		$arr['vals'] = array('1','0');
		$arr['labels'] = array('Yes','No');
		$arr['tip'] = "If set to yes, all top level pages will be condensed under one page heading. The label for this is set in the pages menu label setting.";
		break;
		
	case "logo_alignment":   
		$arr['type'] = "radio";
		$arr['vals'] = array('left','right');
		$arr['labels'] = array('Left','Right');
		$arr['tip'] = "This setting controls the alignment of menu text.";
		break;
		
	case "intro_transition":
		$arr['type'] = "radio";
		$arr['vals'] = array('slide','fade');
		$arr['labels'] = array('Slide','Fade');
		$arr['tip'] = "If set to slide, intro images slide vertically. If set to fade, intro images crossfade.";
		break;
		
	case "image_view_mode":
		$arr['type'] = "dropdown";
		$arr['vals'] = array('stretch to fit','stretch to fill','do not stretch');
		$arr['labels'] = array('stretch to fit','stretch to fill','do not stretch');
		$arr['tip'] = "This setting controls whether gallery and home images stretch to fit or fill the available area or not scale larger than their native width/height.";
		break;
		
	case "image_top_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin between the top of the browser window and the main content.";
		break;
		
	case "image_right_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin between the right edge of the browser window and the main content.";
		break;
		
	case "image_bottom_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin between the bottom of the browser window and the main content.";
		break;
		
	case "image_left_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin between the left edge of the browser window and the menu.";
		break;
		
	case "gallery_image_spacer": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls the space to the left and right of gallery images.";
		break;
		
	case "image_border_thickness": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,20";
		$arr['tip'] = "This setting controls the thickness of gallery and home image borders.";
		break;
		
		
	case "page_top_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin between the top of the browser window and the available area for all pages.";
		break;

	case "page_right_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin between the right edge of the browser window and the available area for all pages.";
		break;

	case "page_bottom_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin between the top of the footer area and the bottom of all pages.";
		break;

	case "page_left_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin between the left edge of the browser window and the available area for all pages.";
		break;
		
	case "max_page_width": 
		$arr['type'] = "slider";
		$arr['vals'] = "400,1000";
		$arr['tip'] = "This setting controls maximum width of all pages. Pages scale down to fit within the available window space  if the page window is not as wide as this setting.";
		break;
	
	case "page_background_opacity":
		$arr['type'] = "slider";
		$arr['vals'] = "0,100";
		$arr['tip'] = "Select the desired opacity for page backgrounds. 0 sets page backgrounds to be completely transparent. 100 sets page backgrounds to be completely opaque.";
	    break;
		
	default: 
		$arr['type'] = "text";
}

?>