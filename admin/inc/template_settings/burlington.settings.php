<?

/*
BURLINGTON
unavailable_dates,
title_font_modifier,
nav_font_modifier,
page_font_modifier,

logo_alignment,
logo_stage_padding,
menu_mode,
menu_position,
slideshow_speed,
image_border,
image_border_thickness,
auto_tilt,
condense_pages,

pages_menu_title,

show_page_background,
loader_type,

texture_file_name,

auto_start_slideshow

*/

switch ($v) {
	
	case "logo_alignment":
		$arr['type'] = "radio";
		$arr['vals'] = array('left','center','right');
		$arr['labels'] = array('left','center','right');
		break;
		
	case "logo_stage_padding":
		$arr['type'] = "slider";
		$arr['vals'] = "0,100";
		$arr['tip'] = "Select the desired amount of space in pixels between the edge of the browser window and top of the logo file.";
		break;
		
	case "menu_mode":
		$arr['type'] = "radio";
		$arr['vals'] = array('1','2','3');
		$arr['labels'] = array('1','2','3');
		break;
		
	case "menu_position":
		$arr['type'] = "radio";
		$arr['vals'] = array('top','bottom');
		$arr['labels'] = array('top','bottom');
		break;
		
	case "slideshow_speed":
		$arr['type'] = "slider";
		$arr['vals'] = "2,20";
		$arr['tip'] = "This setting controls the speed of intro and gallery slideshows. This is the amount of time in seconds an image shows before the next image loads. Loading times for individual images may vary.";
	    break;
	
	case "auto_start_slideshow":
		$arr['type'] = "radio";
		$arr['vals'] = array('yes','no');
		$arr['labels'] = array('On','Off');
		$arr['tip'] = "If turned on, this will start the slideshow automatically when entering a gallery.";
		break;
		
	case "force_uppercase":
		$arr['type'] = "radio";
		$arr['vals'] = array('yes','no');
		$arr['labels'] = array('On','Off');
		$arr['tip'] = "If turned on, this will force menu items to be capitalized.";
		break;
	
	case "image_border":
		$arr['type'] = "radio";
		$arr['vals'] = array('on','off');
		$arr['labels'] = array('on','off');
		break; 
		
	case "image_border_thickness":  
		$arr['type'] = "slider";
		$arr['vals'] = "0,20";
		$arr['tip'] = "This setting controls the width of image borders if turned on";
		break;                                                                        
		
	case "auto_tilt":  
		$arr['type'] = "radio";
		$arr['vals'] = array('on','off');
		$arr['labels'] = array('on','off');
		break; 
		
	case "condense_pages": 
		$arr['type'] = "radio";
		$arr['vals'] = array('on','off');
		$arr['labels'] = array('on','off');
		break;
		
    case "show_page_background":
		$arr['type'] = "radio"; 
		$arr['vals'] = array('on','off');
		$arr['labels'] = array('on','off');
        break; 
	
	case "loader_type": 
		$arr['type'] = "dropdown";
		$arr['vals'] = array('line','circle');
		$arr['labels'] = array('bar','circle');
		break;
		
	default: 
		$arr['type'] = "text"; //pages_menu_title
	
	
	
}
	
?>