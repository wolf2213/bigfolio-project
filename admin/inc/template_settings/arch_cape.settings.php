<?

/*
ARCH CAPE
unavailable_dates, -hidden
title_font_modifier, -hidden
nav_font_modifier, -hidden
page_font_modifier, -hidden
texture_file_name, -hidden

slide_show_speed,
auto_start_slide_show,
menu_mode,
intro_mode,
thumb_mode,
auto_tilt,
contact_message_1,
contact_message_2
*/
switch ($v) {
	case "slide_show_speed":
		$arr['type'] = "slider";
		$arr['vals'] = "2,20";
		$arr['tip'] = "This setting controls the speed of intro and gallery slideshows. This is the amount of time in seconds an image shows before the next image loads. Loading times for individual images may vary.";
	    break;

	case "auto_start_slide_show":
		$arr['type'] = "radio";
		$arr['vals'] = array('yes','no');
		$arr['labels'] = array('On','Off');
		$arr['tip'] = "If turned on, this will start the slideshow automatically when entering a gallery.";
		break;

	case "menu_mode":
		$arr['type'] = "radio";
		$arr['vals'] = array('1','2');
		$arr['labels'] = array('1','2');
		break;

	case "intro_mode":
		$arr['type'] = "radio";
		$arr['vals'] = array('slideshow','thumbs');
		$arr['labels'] = array('Slideshow','Thumbnails');
		break;

	case "thumb_mode":
		$arr['type'] = "radio";
		$arr['vals'] = array('portrait','landscape','square');
		$arr['labels'] = array('portrait','landscape','square');
		break;
	
	case "auto_tilt":
		$arr['type'] = "radio";
		$arr['vals'] = array('yes','no');
		$arr['labels'] = array('On','Off');
		break;

	default: 
		$arr['type'] = "text";
}
	
?>