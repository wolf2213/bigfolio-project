<?

/*

EDGEWOOD
content_width,
content_height,

texture_file_name,

info_menu_label,contact_menu_label,

slideshow_speed,
auto_start_slideshow,

link_1_label,
link_1,
link_2_label,
link_2,

calendar_title,
calendar_text,
unavailable_dates,
title_font_modifier,
nav_font_modifier,
page_font_modifier,

raised_corners,
hash_marks,

page_background_opacity,
top_bar_height,
bottom_bar_height

*/

switch ($v) {   

   	case "content_width":
		$arr['type'] = "slider";
		$arr['vals'] = "640,2560";
		$arr['tip'] = "This sets the width of the main container.";
        break;

    case "content_height":
		$arr['type'] = "slider";
		$arr['vals'] = "400,1600";
		$arr['tip'] = "This sets the height of the main container.";
        break; 
		       
    case "slideshow_speed":
		$arr['type'] = "slider";
		$arr['vals'] = "2,20";
		$arr['tip'] = "This setting controls the speed of intro and gallery slideshows. This is the amount of time in seconds an image shows before the next image loads. Loading times for individual images may vary.";
        break;

	case "auto_start_slideshow":
		$arr['type'] = "radio";
		$arr['vals'] = array('yes','no');
		$arr['labels'] = array('On','Off');
		$arr['tip'] = "If turned on, this will start the slideshow automatically when entering a gallery.";
		break;
		
	case "raised_corners":
		$arr['type'] = "radio";
		$arr['vals'] = array('on','off');
		$arr['labels'] = array('on','off');
		break;  
		
	case "hash_marks":
		$arr['type'] = "radio";
		$arr['vals'] = array('true','false');
		$arr['labels'] = array('on','off');
		break; 
		
	case "page_background_opacity": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,100";
		$arr['tip'] = "This sets the opacity of the page background rectangle.";
        break; 

	case "top_bar_height": 
		$arr['type'] = "slider";
		$arr['vals'] = "40,200";
		$arr['tip'] = "This sets the height in pixels of the top bar above the main content area.";
        break;

   	case "bottom_bar_height": 
		$arr['type'] = "slider";
		$arr['vals'] = "20,200";
		$arr['tip'] = "This sets the height in pixels of the bottom bar above the main content area.";
        break;

	default: 
		$arr['type'] = "text"; //

}
	
?>