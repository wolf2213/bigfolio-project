<?

/*
NORTH BELIZE
title_font_modifier,
nav_font_modifier,
page_font_modifier,
texture_file_name,
secondary_texture,

sub_menu_alignment,
slideshow_speed,
auto_start_slideshow,
app_opacity,
left_bar_opacity,

portfolio_menu_title,

thumbnail_mode,
logo_position,
main_menu_text_size,
main_menu_alignment,
preloader
*/
switch ($v) {
	case "sub_menu_alignment":
		$arr['type'] = "radio";
		$arr['vals'] = array('top','middle','bottom');
		$arr['labels'] = array('top','middle','bottom');
		break;
		
	case "slideshow_speed":
		$arr['type'] = "slider";
		$arr['vals'] = "2,20";
		$arr['tip'] = "This setting controls the speed of intro and gallery slideshows. This is the amount of time in seconds an image shows before the next image loads. Loading times for individual images may vary.";
	    break;

	case "auto_start_slideshow":
		$arr['type'] = "radio";
		$arr['vals'] = array('yes','no');
		$arr['labels'] = array('On','Off');
		$arr['tip'] = "If turned on, this will start the slideshow automatically when entering a gallery.";
		break; 
		
	case "app_opacity":
		$arr['type'] = "slider";
		$arr['vals'] = "0,100";
		$arr['tip'] = "This sets the opacity of the main container and menu backgrounds.";
        break; 

	case "left_bar_opacity": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,100";
		$arr['tip'] = "This sets the opacity of the left bar.";
		break;
    
    case "thumbnail_mode":  
		$arr['type'] = "radio";
		$arr['vals'] = array('1','2');
		$arr['labels'] = array('1','2');
		break;
		
   	case "logo_position": 
		$arr['type'] = "radio";
		$arr['vals'] = array('top','bottom');
		$arr['labels'] = array('top','bottom');      
		break;
		
	case "main_menu_text_size":  
		$arr['type'] = "slider";
		$arr['vals'] = "10,100";
		//$arr['tip'] = "This sets the opacity of the left bar.";
		break;  
		
	case "main_menu_alignment":   
		$arr['type'] = "radio";
        $arr['vals'] = array('right','center','left');
		$arr['labels'] = array('right','center','left');
		break;
               
	case "preloader":
		$arr['type'] = "radio";
		$arr['vals'] = array('northbelize_','');
		$arr['labels'] = array('Bubbles','Line'); 
		$arr['tip'] = "This sets the initial preloader.";
		break;

	default: 
		$arr['type'] = "text";
}
	
?>