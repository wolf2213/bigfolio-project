<?

/*

BURNSIDE
allow_fullscreen_button,

calendar_title,
calendar_text,
unavailable_dates,
title_font_modifier,
nav_font_modifier,
page_font_modifier,

content_width,
content_height,

slideshow_speed,
content_padding,
image_padding,
content_vertical_offset,
logo_position,
app_alpha_0_to_100,
shade_alpha,

texture_file,

menu_orientation,   

number_of_menu_links,

link_1_label,
link_1,
link_2_label,
link_2,

portfolio_button_label,
info_button_label,
contact_button_label,

*/

switch ($v) {
	
	case "allow_fullscreen_button":
		$arr['type'] = "radio";
		$arr['vals'] = array('true','false');
		$arr['labels'] = array('yes','no');
		break;   
		
   	case "content_width":
		$arr['type'] = "slider";
		$arr['vals'] = "640,2560";
		$arr['tip'] = "This sets the width of the main container.";
        break;

    case "content_height":
		$arr['type'] = "slider";
		$arr['vals'] = "400,1600";
		$arr['tip'] = "This sets the height of the main container.";
        break; 

    case "slideshow_speed":
		$arr['type'] = "slider";
		$arr['vals'] = "2,20";
		$arr['tip'] = "This setting controls the speed of intro and gallery slideshows. This is the amount of time in seconds an image shows before the next image loads. Loading times for individual images may vary.";
        break;

	case "content_padding":
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This sets the amount of padding in pixels between the browser window's edge and the container border.";
		break;
		
	case "image_padding":
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This sets the amount of padding in pixels between content within the container and the container border.";
		break; 
		
	case "content_vertical_offset":  
		$arr['type'] = "slider";
		$arr['vals'] = "0,100";
		$arr['tip'] = "This sets the vertical offset in pixels of the main container.";
		break; 
		
	case "logo_position": 
		$arr['type'] = "radio";
		$arr['vals'] = array('left','center','right');
		$arr['labels'] = array('left','center','right');      
		break;
		
	case "app_alpha_0_to_100":
		$arr['type'] = "slider";
		$arr['vals'] = "0,100";
		$arr['tip'] = "This sets the opacity of the main container background.";
		break; 
		
	case "shade_alpha": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,100";
		$arr['tip'] = "This sets the opacity of the content shade used throughout the site.";
		break;
		
	case "menu_orientation":  
		$arr['type'] = "radio";
		$arr['vals'] = array('horizontal','vertical');
		$arr['labels'] = array('horizontal','vertical');
		break;
		
	case "number_of_menu_links": 
		$arr['type'] = "radio";
		$arr['vals'] = array('0','1','2');
		$arr['labels'] = array('0','1','2');
		$arr['tip'] = "This sets the number of extra menu links on the main menu. These can be used to link to external sites like blogs or social media sites.";
		break;
		
	default: 
		$arr['type'] = "text"; // portfolio_button_label, info_button_label, contact_button_label, 
	
	
	
}
	
?>