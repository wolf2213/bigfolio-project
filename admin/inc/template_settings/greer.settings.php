<?

/*
BURLINGTON
unavailable_dates,
title_font_modifier,
nav_font_modifier,
page_font_modifier,

logo_stage_padding,
logo_in_menu,
slideshow_speed,
auto_start_slideshow,
logo_alignment,
page_bg_opacity,
page_rounded_corners,
loading_text_size,
image_mode,
scroll_images_with_mouse,
use_ken_burns_effect,

texture_file_name,
menu_text,
menu_texture_file_name 

*/

switch ($v) {
	
	case "logo_alignment":
		$arr['type'] = "radio";
		$arr['vals'] = array('left','center','right');
		$arr['labels'] = array('left','center','right');
		break;
		
	case "logo_stage_padding":
		$arr['type'] = "slider";
		$arr['vals'] = "0,100";
		$arr['tip'] = "Select the desired amount of space in pixels between the edge of the browser window and top of the logo file.";
		break; 
		
	case "logo_in_menu": 
		$arr['type'] = "radio";
		$arr['vals'] = array('yes','no');
		$arr['labels'] = array('yes','no');
		$arr['tip'] = "If set to yes the logo will appear in the menu. If set to no the logo will be separate from the menu.";
		break; 
		
	case "logo_alignment":
		$arr['type'] = "radio";
		$arr['vals'] = array('left','right');
		$arr['labels'] = array('left','right');
		break;
		
	case "slideshow_speed":
		$arr['type'] = "slider";
		$arr['vals'] = "2,20";
		$arr['tip'] = "This setting controls the speed of intro and gallery slideshows. This is the amount of time in seconds an image shows before the next image loads. Loading times for individual images may vary.";
	    break;
	
	case "auto_start_slideshow":
		$arr['type'] = "radio";
		$arr['vals'] = array('yes','no');
		$arr['labels'] = array('On','Off');
		$arr['tip'] = "If turned on, this will start the slideshow automatically when entering a gallery.";
		break;
		
	case "page_bg_opacity": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,100";
		$arr['tip'] = "This setting controls the opacity of the page background.";
		break;

    case "page_rounded_corners":
		$arr['type'] = "slider";
		$arr['vals'] = "0,20";
		$arr['tip'] = "This setting controls the radius of the rounded corners on the page backgrounds.";
		break;
		
    case "loading_text_size":   
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls the size of the loading numbers that appear in the middle of the site. If set to 0, the loading numbers will not appear.";
		break;
		
	case "image_mode":
		$arr['type'] = "radio";
		$arr['vals'] = array('full bleed','show all');
		$arr['labels'] = array('full bleed','show all');
		$arr['tip'] = "If set to full bleed, gallery and intro images will fill the browser window. Cropping will occur if an image's aspect ratio does not exactly match the aspect ratio of the window. If set to show all, the entire gallery or intro image will be shown.";
		break;
		
    case "scroll_images_with_mouse":   
		$arr['type'] = "radio";
		$arr['vals'] = array('yes','no');
		$arr['labels'] = array('yes','no');
		$arr['tip'] = "If the image mode is set to full bleed setting this to yes will allow the user to scroll the gallery or intro image based on mouse position to reveal and cropped portion of the image if cropping occurs.";
		break; 
		
	case "use_ken_burns_effect":
		$arr['type'] = "radio";
		$arr['vals'] = array('yes','no');
		$arr['labels'] = array('yes','no');
		$arr['tip'] = "If set to yes, intro and gallery images will scale down slowly from a slightly increased scale to the end scale determined by the image mode settings.<br /><br />The Ken Burns effect is a popular name for a type of panning and zooming effect used in video production from still imagery.";
		break;

	default: 
		$arr['type'] = "text"; //menu_text, menu_texture_file_name
	
}
	
?>