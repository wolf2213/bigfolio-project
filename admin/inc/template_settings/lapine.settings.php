<?

/*
LAPINE
App_Alpha_0_to_100,
Info_Page_Alpha_0_to_100,

Texture_File_Name,
Info_Menu_Title,
Contact_Menu_Title,

Menu_Link_1_Title,
Menu_Link_1_URL,

title_font_modifier,
nav_font_modifier,
page_font_modifier,

auto_menu_thumbs,
resize_gallery_images

*/

switch ($v) {
    case "App_Alpha_0_to_100":
		$arr['type'] = "slider";
		$arr['vals'] = "0,100";
		$arr['tip'] = "This sets the opacity of the top and bottom bars above and below the main container area.";
        break;

    case "Info_Page_Alpha_0_to_100":
		$arr['type'] = "slider";
		$arr['vals'] = "400,1600";
		$arr['tip'] = "This sets the opacity page background images.";
        break;

	case "slideshow_speed":
		$arr['type'] = "slider";
		$arr['vals'] = "2,20";
		$arr['tip'] = "This setting controls the speed of intro and gallery slideshows. This is the amount of time in seconds an image shows before the next image loads. Loading times for individual images may vary.";
	    break;
	
	case "auto_menu_thumbs":
		$arr['type'] = "radio";
		$arr['vals'] = array('true','false');
		$arr['labels'] = array('on','off');
		$arr['tip'] = "If turned on, thumbnails for the category and pages menu will be taken from the first image in the first gallery and the page image respectively.  If turned off, custom thumbnails can be uploaded to the image bank. Custom thumbnails ought to match the page or category name and be appended by _thumb.jpg. For instance, the category Engagements would need a thumbnail named Engagements_thumb.jpg";
		break;

	case "allow_fullscreen_button":
		$arr['type'] = "radio";
		$arr['vals'] = array('true','false');
		$arr['labels'] = array('yes','no');
		break;
		
	case "resize_gallery_images":
		$arr['type'] = "radio";
		$arr['vals'] = array('true','false');
		$arr['labels'] = array('yes','no');
		$arr['tip'] = "If turned on, gallery images will resize to fit within the available content space. If turned off, gallery images will be cropped if they are taller than the available content space.";
		break;

	default: 
		$arr['type'] = "text";
}
	
?>