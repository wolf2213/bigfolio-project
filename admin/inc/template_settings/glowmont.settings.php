<?

/*

GLOWMONT
texture_file_name,
title_font_modifier,
nav_font_modifier,
page_font_modifier,
contact_menu_label,
slideshow_speed,
auto_start_slideshow,
image_border_thickness,
menu_link_label,
menu_link_url

*/

switch ($v) {
	
	case "contact_menu_label":
		$arr['type'] = "text";
		$arr['tip'] = "The value entered here will be used in the main menu for the contact page and also the contact page title.";
		break;
		
	case "menu_link_label":
		$arr['type'] = "text";
		$arr['tip'] = "The value entered here appears on the main menu for a link out to an external page. The URL for the link can be assigned under the Menu link url setting.";
		break;
	
	case "menu_link_url":
		$arr['type'] = "text";
		$arr['tip'] = "The value entered here is used for the link assigned to the Menu link label setting.";
		break;

    case "slideshow_speed":
		$arr['type'] = "slider";
		$arr['vals'] = "2,20";
		$arr['tip'] = "This setting controls the speed of intro slideshow. This is the amount of time in seconds an image shows before the next image loads. Loading times for individual images may vary.";
        break;
		
	case "image_border_thickness": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,20";
		$arr['tip'] = "This setting controls the thickness of gallery and home image borders.";
		break;
		
	default: 
		$arr['type'] = "text";
}

?>