<?

/*
texture_file_name,
title_font_modifier,
nav_font_modifier,
page_font_modifier,
logo_alignment,
menu_alignment,
mobile_menu_label,
portfolio_menu_label,
contact_menu_label,

slideshow_speed,
auto_start_slideshow,

image_top_margin,
image_bottom_margin,
image_left_margin,
image_right_margin,
image_border_thickness,

page_top_margin,page_bottom_margin,page_left_margin,page_right_margin,max_page_width
*/

switch ($v) {
	
	

	/* slideshow settings */
    case "slideshow_speed":
		$arr['type'] = "slider";
		$arr['vals'] = "2,20";
		$arr['tip'] = "This setting controls the speed of intro and gallery slideshows. This is the amount of time in seconds an image shows before the next image loads. Loading times for individual images may vary.";
        break;

	case "auto_start_slideshow":
		$arr['type'] = "radio";
		$arr['vals'] = array('yes','no');
		$arr['labels'] = array('On','Off');
		$arr['tip'] = "If turned on, this will start the slideshow automatically when entering a gallery.";
		break;	

	case "logo_alignment":   
		$arr['type'] = "radio";
		$arr['vals'] = array('left','right','center');
		$arr['labels'] = array('Left','Right','Center');
		$arr['tip'] = "This setting controls the alignment of logo.";
		break;

	case "menu_alignment":   
		$arr['type'] = "radio";
		$arr['vals'] = array('left','right','center');
		$arr['labels'] = array('Left','Right','Center');
		$arr['tip'] = "This setting controls the alignment of menu.";
		break;
		
	case "mobile_menu_label":
		$arr['type'] = "text";
		$arr['tip'] = "The value entered here will be used in the mobile menu label.";
		break;
	
	case "portfolio_menu_label":
		$arr['type'] = "text";
		$arr['tip'] = "The value entered here will be used in the gallery urls.";
		break;

	case "contact_menu_label":
		$arr['type'] = "text";
		$arr['tip'] = "The value entered here will be used in the main menu for the contact page and also the contact page title.";
		break;
		


	/* image margin settings */
	case "image_top_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin above gallery and home images";
		break;
		
	case "image_right_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin to the right of gallery and home images.";
		break;
		
	case "image_bottom_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin below gallery and home images.";
		break;
		
	case "image_left_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin to the left of gallery and home images.";
		break;
		
	case "image_border_thickness": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,20";
		$arr['tip'] = "This setting controls the thickness of gallery and home image borders.";
		break;
		
	
	
	/* page settings */
	case "page_top_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin above pages.";
		break;

	case "page_right_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin to the right of pages.";
		break;

	case "page_bottom_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin below pages.";
		break;

	case "page_left_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin to the left of pages.";
		break;
		
	case "max_page_width": 
		$arr['type'] = "slider";
		$arr['vals'] = "400,1000";
		$arr['tip'] = "This setting controls maximum width of all pages. Pages scale down to fit within the available window space  if the page window is not as wide as this setting.";
		break;
		

	default: 
		$arr['type'] = "text";
}

?>