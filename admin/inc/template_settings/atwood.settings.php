<?

/*
ATWOOD
texture_file_name, -hidden
calendar_title, -hidden
calendar_text, -hidden
unavailable_dates, -hidden
title_font_modifier, -hidden
nav_font_modifier, -hidden
page_font_modifier, -hidden

content_width,
content_height,
slideshow_speed,
hash_marks,
menu_type,

portfolio_menu_label,
info_menu_label,
contact_menu_label,
link_1_label,
link_1,
link_2_label,
link_2,
*/

switch ($v) {
    case "content_width":
		$arr['type'] = "slider";
		$arr['vals'] = "640,2560";
		$arr['tip'] = "This sets the width of the main container.";
        break;

    case "content_height":
		$arr['type'] = "slider";
		$arr['vals'] = "400,1600";
		$arr['tip'] = "This sets the height of the main container.";
        break;

	case "slideshow_speed":
		$arr['type'] = "slider";
		$arr['vals'] = "2,20";
		$arr['tip'] = "This setting controls the speed of intro and gallery slideshows. This is the amount of time in seconds an image shows before the next image loads. Loading times for individual images may vary.";
	    break;
	
	case "hash_marks":
		$arr['type'] = "radio";
		$arr['vals'] = array('yes','no');
		$arr['labels'] = array('yes','no');
		break;

	case "menu_type":
		$arr['type'] = "radio";
		$arr['vals'] = array('horizontal','vertical');
		$arr['labels'] = array('horizontal','vertical');
		break;

	default: 
		$arr['type'] = "text";
}
	
?>