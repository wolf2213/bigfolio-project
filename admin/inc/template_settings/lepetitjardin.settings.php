<?

/*
LE PETIT JARDIN
title_font_modifier,
nav_font_modifier,
page_font_modifier,
texture_file_name,

show_container,
container_aspect_ratio,
window_padding,
content_padding,
show_image_border,
image_border_padding,
image_border_type,

transition_time,
slideshow_speed,
auto_start_slideshow,
arrow_style,

portfolio_menu_title
*/

switch ($v) { 
	
case "show_container": 
	$arr['type'] = "radio";
	$arr['vals'] = array('yes','no');
	$arr['labels'] = array('yes','no');
	$arr['tip'] = "If turned on, the flash site will be contained within an html container, showing a border around the flash site.";
	break;
	
case "container_aspect_ratio": 
	$arr['type'] = "radio";
	$arr['vals'] = array('3:2','4:3','16:9','relative');
	$arr['labels'] = array('3:2','4:3','16:9','relative');
	$arr['tip'] = "If the container is turned on, it can be set to have a fixed aspect ratio of 3:2, 4:3, or 16:9. To set the container width and height relative to the window, select relative.";
   	break; 

case "window_padding":
	$arr['type'] = "slider";
	$arr['vals'] = "0,200";
	$arr['tip'] = "Select the desired amount of padding in pixels from the edge of the window or container to the edge of any content within the flash site.";
	break;
	
case "content_padding":
	$arr['type'] = "slider";
	$arr['vals'] = "0,200";
	$arr['tip'] = "Select the desired amount of padding in pixels from the edge of the main content area and any inner content like images or pages.";
	break; 
	
case "show_image_border":  
	$arr['type'] = "radio";
	$arr['vals'] = array('yes','no');
	$arr['labels'] = array('yes','no');
	break; 
	
case "image_border_padding":
	$arr['type'] = "slider";
	$arr['vals'] = "0,100";
	$arr['tip'] = "Select the desired amount of padding in pixels from the edge of an image to the border element.";
	break;
	
case "image_border_type":
	$arr['type'] = "dropdown";
	$arr['vals'] = array('0', '1','2','3','4','5','6');
	$arr['labels'] = array('none', '1','2','3','4','5','6');
	$arr['tip'] = "Select the desired border style. See this template's help page for examples of each border type.";
	break; 
	
case "transition_time":
	$arr['type'] = "slider";
	$arr['vals'] = "0,10";
	$arr['tip'] = "Select the desired amount time in seconds for image transitions.";
	break;
	
case "slideshow_speed":
	$arr['type'] = "slider";
	$arr['vals'] = "2,20";
	$arr['tip'] = "This setting controls the speed of intro and gallery slideshows. This is the amount of time in seconds an image shows before the next image loads. Loading times for individual images may vary.";
    break;

case "auto_start_slideshow":
	$arr['type'] = "radio";
	$arr['vals'] = array('yes','no');
	$arr['labels'] = array('On','Off');
	$arr['tip'] = "If turned on, this will start the slideshow automatically when entering a gallery.";
	break;

case "arrow_style":
	$arr['type'] = "radio";
	$arr['vals'] = array('arrow','circle');
	$arr['labels'] = array('arrow','circle');
	$arr['tip'] = "Select the desired icon for the gallery image navigation.";
	break;
	
default: 
	$arr['type'] = "text";

}
	
?>