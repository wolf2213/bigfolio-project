<?

/*

DESPERADO
texture_file_name,
portfolio_menu_label,
contact_menu_label,
slideshow_speed,
auto_start_slideshow,
redirect_target,
title_font_modifier,
nav_font_modifier,
page_font_modifier,
condense_galleries,

top_margin,
right_margin,
bottom_margin,
left_margin,
menu_top_margin,
left_content_margin,
menu_alignment

*/

switch ($v) {

    case "slideshow_speed":
		$arr['type'] = "slider";
		$arr['vals'] = "2,20";
		$arr['tip'] = "This setting controls the speed of intro and gallery slideshows. This is the amount of time in seconds an image shows before the next image loads. Loading times for individual images may vary.";
        break;

	case "auto_start_slideshow":
		$arr['type'] = "radio";
		$arr['vals'] = array('yes','no');
		$arr['labels'] = array('On','Off');
		$arr['tip'] = "If turned on, this will start the slideshow automatically when entering a gallery.";
		break;
		
	case "redirect_target":
		$arr['type'] = "dropdown";
		$arr['vals'] = array('_self','_blank','bfRedirect');
		$arr['labels'] = array('same window','new window','all redirects in the same new window');
		break;
		
	case "condense_galleries": 
		$arr['type'] = "radio";
		$arr['vals'] = array('1','0');
		$arr['labels'] = array('Yes','No');
		$arr['tip'] = "If set to yes, all categories of galleries will be condensed under one portfolio heading. The label for this is set in the portfolio menu label setting.";
		break;
		
	case "top_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin between the top of the browser window and the main content.";
		break;
		
	case "right_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin between the right edge of the browser window and the main content.";
		break;
		
	case "bottom_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin between the bottom of the browser window and the main content.";
		break;
		
	case "left_margin": 
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin between the left edge of the browser window and the menu.";
		break;
		
	case "left_content_margin":
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin between the left edge of the menu and the main content.";
		break;
		
	case "menu_top_margin":   
		$arr['type'] = "slider";
		$arr['vals'] = "0,200";
		$arr['tip'] = "This setting controls margin between the top of the browser window and the menu.";
		break;
		
	case "menu_alignment":   
		$arr['type'] = "radio";
		$arr['vals'] = array('left','center','right');
		$arr['labels'] = array('Left','Center','Right');
		$arr['tip'] = "This setting controls the alignment of menu text.";
		break;
		
	default: 
		$arr['type'] = "text";
}

?>