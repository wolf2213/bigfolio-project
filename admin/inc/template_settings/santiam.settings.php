<?

/*
SANTIAM
title_font_modifier,
nav_font_modifier,
page_font_modifier,
texture_file_name,

slideshow_speed,
auto_start_slideshow,

portfolio_menu_title,

auto_menu_thumbnails,
intro_images_per_set,
do_intro_from_gallery_images
*/
switch ($v) {
		
	case "slideshow_speed":
		$arr['type'] = "slider";
		$arr['vals'] = "2,20";
		$arr['tip'] = "This setting controls the speed of intro and gallery slideshows. This is the amount of time in seconds an image shows before the next image loads. Loading times for individual images may vary.";
	    break;

	case "auto_start_slideshow":
		$arr['type'] = "radio";
		$arr['vals'] = array('yes','no');
		$arr['labels'] = array('On','Off');
		$arr['tip'] = "If turned on, this will start the slideshow automatically when entering a gallery.";
		break;   
		
	case "auto_menu_thumbs":
		$arr['type'] = "radio";
		$arr['vals'] = array('true','false');
		$arr['labels'] = array('on','off');
		$arr['tip'] = "If turned on, thumbnails for the galleries menu will be taken from the first image in the gallery. If turned off, custom thumbnails can be uploaded to the image bank. Custom thumbnails ought to match the page or category name and be appended by .jpg. For instance, the category Engagements would need a thumbnail named Engagements.jpg";
		break; 
		
	case "intro_images_per_set":
		$arr['type'] = "slider";
		$arr['vals'] = "1,6";
		$arr['tip'] = "This setting controls the number of intro images shown at a time.";
		break;
		
	case "do_intro_from_gallery_images":
		$arr['type'] = "radio";
		$arr['vals'] = array('yes','no');
		$arr['labels'] = array('yes','no');   
		$arr['tip'] = "If set to yes, the intro image slideshow will load images randomly from all galleries.";
		break;

	default: 
		$arr['type'] = "text";
}
	
?>