<?

/*
PIONEER
title_font_modifier,
nav_font_modifier,
page_font_modifier,
texture_file_name,

app_opacity,
window_padding,
content_padding,
logo_position,
show_main_border,
show_image_border,
image_border_thickness,
slideshow_speed,
auto_start_slideshow,
show_contact_form_borders
*/

switch ($v) {

case "app_opacity":
	$arr['type'] = "slider";
	$arr['vals'] = "0,100";
	$arr['tip'] = "Select the desired opacity for the main container. 0 sets the container to be completely transparent. 100 sets the container to be completely opaque.";
    break;
/*
case "rounded_corners":
	$arr['type'] = "radio";
	$arr['vals'] = array('yes','no');
	$arr['labels'] = array('On','Off');
	$arr['tip'] = "If turned on, this will round the corners of the container and images within the container.";
	break;
*/

case "window_padding":
	$arr['type'] = "slider";
	$arr['vals'] = "0,200";
	$arr['tip'] = "Enter the desired number of padding in pixels between the edge of the browser window or container div if used.";
    break;

case "content_padding":
	$arr['type'] = "slider";
	$arr['vals'] = "0,100";
	$arr['tip'] = "Enter the desired number of padding in pixels between gallery images, intro images, and page content and the main content area background.";
	break;

/*	
case "menu_position":
	$arr['type'] = "radio";
	$arr['vals'] = array('left','right');
	$arr['labels'] = array('left','right');
	$arr['tip'] = "The menu can be positioned on the left or right side of the main content area. Select left or right.";
	break; 
*/
	
case "logo_position":
	$arr['type'] = "radio";
	$arr['vals'] = array('above','adjacent');
	$arr['labels'] = array('above','adjacent');
	$arr['tip'] = "The logo can be positioned either above or adjacent to the container. Select 'above' in the Logo position field to place the logo above the main content container. Select 'adjacent' to place the logo next to the main content container.";
	break;
	
case "show_main_border":
	$arr['type'] = "radio";
	$arr['vals'] = array('yes','no');
	$arr['labels'] = array('yes','no');
	break;
	
case "show_image_border":
	$arr['type'] = "radio";
	$arr['vals'] = array('yes','no');
	$arr['labels'] = array('yes','no');
	break;
	
case "image_border_thickness":
	$arr['type'] = "slider";
	$arr['vals'] = "0,20";
	$arr['tip'] = "Select the desired thickness in pixels for image borders.";
	break;

/*	
case "thumbnail_mode":
	$arr['type'] = "dropdown";
	$arr['vals'] = array('solid circles','solid squares','image circles', 'image squares');
	$arr['labels'] = array('solid circles','solid squares','image circles', 'image squares');
	$arr['tip'] = "There are four different thumbnail modes available. Select 'image squares' for a 3 column view of square images, 'image circles', for a 3 column of circle images, 'solid circles' for an 8 column view of solid color circles, or 'solid squares' for an 8 column view of solid color squares.";
	break;
	
case "thumbnail_colors":
	$arr['type'] = "colors";
	$arr['numColors'] = 5;
	$arr['tip'] = "If the thumbnail mode is set to either 'solid squares' or 'solid circles', custom colors can be used.";				
	break;
	
case "use_mouse_following_gallery_arrows":
	$arr['type'] = "radio";
	$arr['vals'] = array('yes','no');
	$arr['labels'] = array('yes','no');
	break;
	
case "transition_time":
	$arr['type'] = "slider";
	$arr['vals'] = "1-20";
	$arr['tip'] = "Enter the desired transition time in seconds for intro and gallery images.";
    break;

case "transition_type":
	$arr['type'] = "radio";
	$arr['vals'] = array('wipe','slide','fade');
	$arr['labels'] = array('wipe','slide','fade');
	break;
*/

case "slideshow_speed":
	$arr['type'] = "slider";
	$arr['vals'] = "2,20";
	$arr['tip'] = "This setting controls the speed of intro and gallery slideshows. This is the amount of time in seconds an image shows before the next image loads. Loading times for individual images may vary.";
    break;

case "auto_start_slideshow":
	$arr['type'] = "radio";
	$arr['vals'] = array('yes','no');
	$arr['labels'] = array('On','Off');
	$arr['tip'] = "If turned on, this will start the slideshow automatically when entering a gallery.";
	break; 
	
case "show_contact_form_borders":  
	$arr['type'] = "radio";
	$arr['vals'] = array('yes','no');
	$arr['labels'] = array('yes','no');
	break;
	
default: 
	$arr['type'] = "text";

}
	
?>