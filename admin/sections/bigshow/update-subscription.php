<?
session_start();
//
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache'); 

require_once('../../inc/stripe-php/lib/Stripe.php'); 
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");

$s = get_settings();

include("../../inc/stripe-php/StripeKey.php");
$stripeCustomer = Stripe_Customer::retrieve($_SESSION['user']["bigshow_stripe_customer"]);
//$stripeCustomer = json_decode($stripeCustomer);
// Include files
//include_once("../../inc/db.inc.php");
//include_once("../../inc/functions.inc.php");

$nextAmount = $stripeCustomer->next_recurring_charge->amount / 100;
$nextBillDate = $stripeCustomer->next_recurring_charge->date;

$last4 = $stripeCustomer->active_card->last4;

$exp_month = $stripeCustomer->active_card->exp_month;
if (strlen($exp_month) == 1) {
	$exp_month = "0".$exp_month;
}
$exp_year = $stripeCustomer->active_card->exp_year;

?>
<div class="content_inner" id="">
	<div class="contentHeader">
		<h1>BIG Show Subscription</h1>
	</div>  
	<div class="tip_message"> 
		Below is the current card on file for your BIG Show subscription. If desired, enter a new credit card here for the next billing of <strong>$<?=$nextAmount?></strong> which will occur on <strong><?=$nextBillDate?></strong>.
		<div class="payment-errors">
		</div>
		<!--
		<?= $stripeCustomer; ?>
		-->
	</div> 
	<div id="subscriptionForm"> 
		<legend>Billing Information</legend> 
		<ul>
		<!-- <form action="" method="POST" id="payment-form"> -->  
		<div id="payment-form">
			<li>
				<label>Card Number</label>   
				<input type="text" size="20" autocomplete="off" class="card-number bfInputText" style="width: 160px;" value="**** **** **** <?=$last4?>"/>
			</li>
			<li>
				<label>CVC</label>    
				<input type="text" size="4" autocomplete="off" class="card-cvc bfInputText" style="width: 50px;" value="***"/> 
			</li>
		    <li> 
				<label>Expiration (MM/YYYY)</label>
		        <input type="text" size="2" class="card-expiry-month bfInputText" style="width: 20px;" value="<?=$exp_month?>"/>
		        <span style="float:left; padding: 10px;"> / </span>
		        <input type="text" size="4" class="card-expiry-year bfInputText" style="width: 40px;" value="<?=$exp_year?>"/>
			</li> 
			<li>
		    	<button type="submit" id="bigShowStripeSubmit" class="submit-button">Update Card</button> 
			</li>
		</div>
		<!-- </form> -->
		</ul>
	</div> 
</div> 

<script type="text/javascript" src="https://js.stripe.com/v1/"></script>
<script type="text/javascript" src="/inc/stripe-php/StripeKey.js"></script>

<script type="text/javascript">
// <![CDATA[                        

$(document).ready(function() {
	
	$(".content_inner").hide();   

	//$("#payment-form").submit(function(event) { 
	$("#bigShowStripeSubmit").click(function() {
		// disable the submit button to prevent repeated clicks
		$(this).attr("disabled", "disabled");

		Stripe.createToken({
		    number: $('.card-number').val(),
		    cvc: $('.card-cvc').val(),
		    exp_month: $('.card-expiry-month').val(),
		    exp_year: $('.card-expiry-year').val()
		}, stripeResponseHandler);

		// prevent the form from submitting with the default action
		return false;
	});
	
	function stripeResponseHandler(status, response) {
	    if (response.error) {
	        //show the errors on the form
	        $(".payment-errors").html(response.error.message);
			$("#bigShowStripeSubmit").removeAttr("disabled");
	    } else {
	        var form$ = $("#payment-form");
	        // token contains id, last4, and card type
	        var token = response['id'];
	        // insert the token into the form so it gets submitted to the server
	        //form$.append("<input type='hidden' name='stripeToken' value='" + token + "'/>"); 
	        showSave();
			var data = new Object();
			data['stripeToken'] = token;
	        // and submit  
			$.ajax({
				//this is the php file that processes the data
				url: 'actions/bigshow/update-subscription.php',	

				//POST method is used
				type: "POST",

				//pass the data			
				data: data,		

				//Do not cache the page
				cache: false,

				//success
				success: function (res) { 
					$.log('subscriptionSetup: '+res);   		
					if (res.indexOf("subscriptionSuccess") > -1) {					 
						alert('Your card was successfully updated.');
						location.href = "#/bigshow/bigshow-slideshows";
					} else {
						$(".payment-errors").html("There was a problem with your credit card. Please verify you have entered everything correctly and try again.");
						$("#bigShowStripeSubmit").removeAttr("disabled");
					}
					hideSave();			
				}		
			});
	        //form$.get(0).submit();
	    }
	} 
	
	setTimeout(function() { 
		Stripe.setPublishableKey(StripeKey);
		$(".content_inner").show(); 
	}, 1000);
	
});

// ]]>
</script>