<?
session_start();
//
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache'); 

require_once('../../inc/stripe-php/lib/Stripe.php'); 
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");

$s = get_settings();

include("../../inc/stripe-php/StripeKey.php");
$stripeCustomer = Stripe_Customer::retrieve($_SESSION['user']["bigshow_stripe_customer"]);

$nextAmount = $stripeCustomer->next_recurring_charge->amount / 100;
$nextBillDate = $stripeCustomer->next_recurring_charge->date;

$all_slideshows = get_all_bigshows();
$numOverImageLimit = 0;
foreach ($all_slideshows as $ss) {
	$numImages = count(get_bigshow_images($ss['id']));
	if ($numImages > 50) {
		$numOverImageLimit++;
	}
}
$okToCancel = true;

if ($_SESSION['server'] == 'bigshow') {
	$freeLimit = 3;
} else {
	$freeLimit = 5;
}
if (count($all_slideshows) > $freeLimit || $numOverImageLimit > 0) {
	$okToCancel = false;
}

?>
<div class="content_inner" id="">
	<div class="contentHeader">
		<h1>BIG Show Subscription</h1>
	</div>  
	<div class="tip_message"> 
		<? if (count($all_slideshows) > $freeLimit) { ?>
		<div class="payment-errors">There are currently <?=count($all_slideshows)?> slideshows. There must be <?=$freeLimit?> or fewer slideshows in order to cancel your subscription.<br><br></div>
		<? } ?>
		<? if ($numOverImageLimit > 0) { ?>
		<div class="payment-errors">There are currently <?=$numOverImageLimit?> slideshows with more than 50 images. All slideshows must have 50 or fewer images per slideshow in order to cancel your subscription.<br><br></div>
		<? } ?>
		The next billing of <strong>$<?=$nextAmount?></strong> will occur on <strong><?=$nextBillDate?></strong>.<br><br>
		<? if ($okToCancel) { ?>Click the button below to cancel your unlimited BIG Show subscription.<br> Subscription payments are not prorated.<br>Click <a href="#/bigshow/bigshow-slideshows">here</a> to return to your slideshows.
		<? } else { ?>
			Click <a href="#/bigshow/bigshow-slideshows">here</a> to manage your slideshows.<br><br>
		<? } ?>
	</div> 
	<? if ($okToCancel) { ?>
	<div id="subscriptionForm"> 
		<ul> 
			<li>
		    	<button type="submit" id="bigShowStripeSubmit" class="submit-button">Cancel Subscription</button> 
			</li>
		</div>
		<!-- </form> -->
	</div> 
	<? } ?>
</div> 

<script type="text/javascript" src="https://js.stripe.com/v1/"></script>

<script type="text/javascript">
// <![CDATA[                        

$(document).ready(function() { 

	//$("#payment-form").submit(function(event) { 
	$("#bigShowStripeSubmit").click(function() {
		var save = confirm("Are you sure you want to cancel your subscription? This cannot be undone. Subscription payments are not prorated.");
		if (save) {
			stripeResponseHandler();
		}
	});
	
	function stripeResponseHandler() {
	    showSave();
        // and submit  
		$.ajax({
			//this is the php file that processes the data
			url: 'actions/bigshow/cancel-subscription.php',	

			//POST method is used
			type: "POST",	

			//Do not cache the page
			cache: false,

			//success
			success: function (res) { 
				$.log('subscriptionSetup: '+res);   		
				if (res.substr(0,7) == 'success') {					 
					alert('Your subscription was successfully cancelled.');
					location.href = "#/bigshow/bigshow-slideshows";
				} else {
					alert('Sorry, unexpected error. Please try again later.');
					$("#bigShowStripeSubmit").removeAttr("disabled");
				}
				hideSave();			
			}		
		});
        //form$.get(0).submit();
	} 
	
});

// ]]>
</script>