CREATE TABLE  `bf_admin4_sample`.`bigshow_slideshows` (
`id` INT( 5 ) NOT NULL AUTO_INCREMENT ,
`storage_id` VARCHAR( 255 ) NOT NULL ,
`name` VARCHAR( 500 ) NOT NULL ,
`url` VARCHAR( 500 ) NOT NULL ,
`header` TEXT NOT NULL ,
`footer` TEXT NOT NULL ,
`settings` TEXT NOT NULL ,
PRIMARY KEY (  `id` ) ,
INDEX (  `id` )
) ENGINE = MYISAM ;                    

CREATE TABLE  `bf_admin4_sample`.`bigshow_images` (
`image_id` INT( 8 ) NOT NULL AUTO_INCREMENT ,
`slideshow_id` INT( 5 ) NOT NULL ,
`order_num` INT( 4 ) NOT NULL ,
`image_file` VARCHAR( 255 ) NOT NULL ,
`image_caption` TEXT NOT NULL ,
`align` VARCHAR( 20 ) NOT NULL ,
PRIMARY KEY (  `image_id` ) ,
INDEX (  `image_id` )
) ENGINE = MYISAM ;