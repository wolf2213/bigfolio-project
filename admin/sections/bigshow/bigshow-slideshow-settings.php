<?
session_start(); 
//
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');

// Include files
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");
//
// check if bishow db tables are present and install if need be..
$hasBIGshow = false;
$result = mysql_list_tables($db_name) or die(mysql_error());
while ($row = mysql_fetch_row($result)) {
	if($row[0]=='bigshow_slideshows') {
		$hasBIGshow = true;
	}
}
if (!$hasBIGshow) {
	include_once("bigshow-setup.php");
}                                     
include_once("bigshow-source-update.php");
// 
$all_slideshows = get_all_bigshows();
$loop = 0; 
if ($_SESSION['server'] == 'bigshow') {
	$freeLimit = 3;
} else {
	$freeLimit = 5;
}
// 
$s = get_settings();
$isUnlimited = $_SESSION['user']['bigshow_subscription'] == '1';
//
$storageID = $_SESSION['user']['storage_id'];
$imgStorage = "http://cdn.bigfoliobigshow.com.s3.amazonaws.com/users/" . $storageID{0} . '/' . $storageID{1} . '/' . $storageID{2} . '/' . $storageID . '/';
//

//
// colors
//
$colorsArr       = array();
$colorsLabels    = array();
//
$colorsArr[]     = 'body_color'; 
$colorsLabels[]  = 'Background color';
//
$colorsArr[]     = 'title_color'; 
$colorsLabels[]  = 'Title color';  
//
$colorsArr[]     = 'text_color';
$colorsLabels[]  = 'Text color'; 
//
$colorsArr[]     = 'custom_color_1'; 
$colorsLabels[]  = 'Link color';
// 
$colorsArr[]     = 'custom_color_2';
$colorsLabels[]  = 'Link hover color';
//
$colorsArr[]     = 'app_border_color';  
$colorsLabels[]  = 'Border color';


//
// slideshow speed
$fvalues = unserialize($s['flash_vars']); 
if (!isset($fvalues['slideshow_speed'])) {
	$fvalues['slideshow_speed'] = 5;
}
?>

<div class="content_inner" id="">
	<div class="contentHeader">
		<h1>BIG Show Settings</h1>
	</div>  
	<div class="tip_message">
		BIG Show is a quick and easy way to create elegant HTML5 slideshows for clients.
		<br>
		Use this area to manage your general settings for each slideshow.
		<br><br>
		<? if(!$isUnlimited){ ?>
			You can create up to <?=$freeLimit?> slideshows each with 50 images and music.
			<br>Unlimited use can be purchased <a href="#/bigshow/setup-subscription" target="bigshowBilling">here</a> for $99/year.
		<? } else { ?>
			To update the the credit card billed for the yearly subscription fee, click <a href="#/bigshow/update-subscription" target="bigshowBilling">here</a>. 
			<br>To cancel your BIG Show subscription, click <a href="#/bigshow/cancel-subscription" target="bigshowBilling">here</a>.
		<? } ?>
	</div>
	
	<ul>
		<!-- logo uploader -->
		<li class="divider"></li>
		<h2>Logo</h2>
		<div class="tip_message">
			Upload your logo. Changes to your logo here affect all slideshows set to show your logo.
		</div>
		<li> 
			<div id="logoHolder">
				<? if ($s['logo_file'] != '') {	?>
					<img src="<?= $s['logo_file'] ?>" border="0" id="logoFileImg"/> 
				<? } ?>
		 	</div>
		
			<a href="javascript:;" class="bfToolTip" title="Make sure to size your logo appropriately. Recommended sizing for your logo is 200px by 75px or smaller.">
			<input name="logofile" type="file" id="logofile" size="10" /> 
			</a>
			<div id="status-message">
			
			</div> 
			<div id="logo-queue">
			</div>
		</li> 
		<li>
			<label for="logo_link">Logo link (<a href="javascript:;" class="bfToolTip" title="Enter a URL to be attached to the logo if it's shown on a slideshow.">?</a>)</label>
			<input name="logo_link" type="text" id="logo_link" value="<?= $s['logo_link'] ?>"  class="bfInputText bfChangeListen" />
		</li>
		<li class="divider"></li>
		<h2>Default colors</h2>
		<div class="tip_message">
			Set the default colors for your slideshows below. Colors can be adjusted on each individual slideshow, but the colors entered here will be used for the default colors for new slideshows. Slideshows that have already been created are unaffected by changes here.
		</div>
		<? for($i=0; $i<count($colorsArr); $i++) { ?> 
			<li>
				<label><?=$colorsLabels[$i];?></label>
				<input type='hidden' name='<?=$colorsArr[$i]?>' id='<?=$colorsArr[$i]?>' value='<?=$s[$colorsArr[$i]]?>' class='bfChangeListen' />
				<div class='bfSwatch' style='background-color:<?=$s[$colorsArr[$i]]?>'></div>
			</li>
		<? } ?>
		
		<li class="divider"></li>
		<h2>Default slideshow speed</h2>
		<div class="tip_message">
			Set the desired default slideshow speed in seconds below. Slideshow speed can be adjusted on each individual slideshow, but the speed entered here will be used for the default slideshow speed for new slideshows. Slideshows that have already been created are unaffected by changes here.
		</div>
		<div class='bfSlider' title='0,20,<?=$fvalues['slideshow_speed']?>'></div>
		<input type='text' class='bfInputText bfSliderLabel bfChangeListen' name='var_slideshow_speed' id='var_slideshow_speed' value='<?=$fvalues['slideshow_speed']?>' />
		
	</ul>
	
</div>


<script type="text/javascript">
// <![CDATA[   

var storageID = "<?=$storageID?>";
var imgStorage = "<?=$imgStorage?>";   
$.log("imgStorage: "+imgStorage);                  

function settingsFunctionality() {
	
} 

settingsFunctionality();

//
// gallery image upload  
$("#logofile").uploadify({
  'uploader'       : 'js/plugins/uploadify-v2.1.4/uploadifyCS3.swf',
  'script'         : 'inc/bigshow_upload.php', 
  'scriptData'     : {'storageID':storageID},      
  'method'         : 'post',
  'cancelImg'      : 'js/plugins/uploadify-v2.1.4/cancel.png',
  'multi'          : true,
  'auto'           : true,
  'fileExt'        : '*.jpg;*.jpeg;*.png;',
  'fileDesc'       : 'Image Files (.JPG, .JPEG, .PNG)',
  'queueID'        : 'logo-queue', 
  'buttonText'     : 'UPLOAD LOGO',
  'width'          : 125,
  'queueSizeLimit' : 1,
  'simUploadLimit' : 1,
  'removeCompleted': true,
  'onSelectOnce'   : function(event,data) {
      	//$('#status-message').text(data.filesSelected + 'uploading...');
		$('#status-message').text('uploading...');
    }, 
  'onComplete'     : function(event, ID, fileObj, response, data) { 	
		//
		// get last logo for deletion
		if ($("#logoFileImg").length > 0) {
			var lastLogoPaths = $("#logoFileImg").attr("src").split("/");
			var lastLogo = lastLogoPaths[lastLogoPaths.length-1];
			$.log("lastLogo: "+lastLogo);
		}
		var fileParts = getNameAndExtension(fileObj['name']); 
		var logoFile = cleanfile(fileParts[0])+'.'+fileParts[1]; 
		//
		$.log('fileName: '+cleanfile(fileParts[0])+'.'+fileParts[1]);
	  	$.log('response: '+response); 
	  	var logohtml = "";
			logohtml += "<img src='"+(imgStorage)+(logoFile)+"' id='logoFileImg' />"; 
		$.log('logohtml: '+logohtml);
		$("#logoHolder").html(logohtml);  
		//
		// save logo file name in db  
		showSave();
		data = new Object();
		data['logo_file'] = imgStorage+logoFile;
		$.ajax({
			//this is the php file that processes the data
			url: 'actions/site-settings/logo_file.save.php',	
			type: "POST",	
			data: data,		
			cache: false,
			success: function (res) {
				hideSave();			
				if (res.substr(0,7) == 'success') {					 
					$.log(res);
					//
					// get rid of the old logo
					if (lastLogo != undefined && lastLogo != logoFile) {
						$.ajax({
				          type: 'POST',
				          url:'inc/bigshow_file_remove.php',
				          data: ({'storageID':storageID, 'file':lastLogo})
				        }).done(function( msg ) {
						  $.log( "msg: " + msg );
						});
					}
						
				} else {
					alert('Sorry, unexpected error. Please try again later.');
				}			
			}		
		});
		//
   	},
  'onAllComplete'  : function(event,data) {
		$.log("filesUploaded: "+data.filesUploaded);
		$.log("data.errors: "+data.errors);
		if (data.errors > 0) {
      		$('#status-message').text("There was an error with the upload.");
		} else {
			$('#status-message').text("");
		}
    }
});

// ]]>
</script>