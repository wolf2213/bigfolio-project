<?
session_start(); 
//
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");
//
// check if bishow db tables are present and install if need be..
$hasBIGshow = false;
$result = mysql_list_tables($db_name) or die(mysql_error());
while ($row = mysql_fetch_row($result)) {
	if($row[0]=='bigshow_slideshows') {
		$hasBIGshow = true;
	}
}
if (!$hasBIGshow) {
	include_once("bigshow-setup.php");
}                                     
include_once("bigshow-source-update.php");
// 
$all_slideshows = get_all_bigshows();
$loop = 0; 
if ($_SESSION['server'] == 'bigshow') {
	$freeLimit = 3;
} else {
	$freeLimit = 5;
}
// 
$s = get_settings();
$isUnlimited = $_SESSION['user']['bigshow_subscription'] == '1';
//
?>
<div class="content_inner" id="">
	<div class="contentHeader">
		<h1>BIG Show Slideshows</h1>
	</div>  
	<div class="tip_message">
		BIG Show is a quick and easy way to create elegant HTML5 slideshows for clients. Simply create a slideshow here, then add images and music by clicking the edit link. 
		<br><br>
		<? if(!$isUnlimited){ ?>
			You can create up to <?=$freeLimit?> slideshows each with 50 images and music.
			<br>Unlimited use can be purchased <a href="#/bigshow/setup-subscription" target="bigshowBilling">here</a> for $99/year.
		<? } else { ?>
			To update the the credit card billed for the yearly subscription fee, click <a href="#/bigshow/update-subscription" target="bigshowBilling">here</a>. 
			<br>To cancel your BIG Show subscription, click <a href="#/bigshow/cancel-subscription" target="bigshowBilling">here</a>.
		<? } ?>
	</div> 
	<div id="slideshows"> 
		
		<? foreach($all_slideshows as $s) { ?>
		<? $loop++; ?>
		<? if ($loop <= $freeLimit || $isUnlimited) { ?>	
		<div class="slideshow" id="ss<?=$s['id']?>">
			<div class="deleteButton"></div> 
		    <input type="text" class="bfInputText ssName bfChangeListen" name="ss-<?=$s['id']?>" value="<?=$s['name']?>">
			<div class="editButton">
				<a href="#/bigshow/bigshow-slideshows/<?=$s['id']?>" class="editLink">
					edit
				</a>
				| 
				<?
				if ($_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == 'macbook' || $_SERVER['HTTP_HOST'] == 'bigshow.phpfogapp.com' || $_SERVER['HTTP_HOST'] == 'bfbs.phpfogapp.com') { 
					$ssURL = 'bigshow_dev/gallery/index.php?s='.$s['url'].'&u='.$_SESSION['user']['username'];
				} else if ($_SERVER['HTTP_HOST'] == 'www.bigsho.ws') {
					$ssURL = "http://" . $_SESSION['user']['username'] . ".bigsho.ws/" . $s['url'];
				} else {
					$ssURL = $_SESSION['user']['temp_url'].'gallery/index.php?s='.$s['url'];  
				}
				?>
				<a href="<?=$ssURL?>" class="viewLink" target="_blank">
					view
				</a>
			</div>
			<div class="bigshowImageCount">
			<?
			$images = get_bigshow_images($s['id']);
			echo count($images). " images";
			?>
			</div>
			<div style="clear:both;"></div> 
		</div>
		<? } ?> 
		<? } ?>
		
	</div> 
	<button id="newSlideshow">New Slideshow</button>
</div>


<script type="text/javascript">
// <![CDATA[                        

function slideshowsFunctionality() {
	
	//
	// rollovers
	$(".slideshow").unbind("mouseover");
	$(".slideshow").mouseover(function() {
	   	$(this).addClass("itemOver");
	});
	$(".slideshow").unbind("mouseout");
	$(".slideshow").mouseout(function() {
	   	$(this).removeClass("itemOver");
	});
	
	//
	// delete slideshow button
    $(".slideshow").find(".deleteButton").unbind("click");
    $(".slideshow").find(".deleteButton").click(function() { 
		var conf = confirm("Are you sure you want to delete this slideshow? This cannot be undone.");
		if (conf) {
			var ssID = $(this).parent().attr('id').substr(2); 
			$.log('ssID: '+ssID);
			var slideshowDiv = $(this).parent();
			//
			$.ajax({
				type: 'POST',
				url: 'actions/bigshow/delete-slideshow.php',
				data: ({'ssID':ssID})
			}).done(function( msg ) {
				$.log("msg: "+msg);
				slideshowDiv.fadeOut('fast', function() {
					slideshowDiv.remove();
				});
			});
		}
	});
	
} 

slideshowsFunctionality();

$("#newSlideshow").click(function() {
	$.ajax({
       	type: 'POST',
       	url:'actions/bigshow/add-slideshow.php',
       	data: ({'user':user})
    }).done(function( msg ) {
	   	if (msg.substr(0,7) == "SUCCESS") { 
			//
			var ssID = msg.substr(8);
			//
			var sshtml = "";
				sshtml += '<div class="slideshow" id="ss'+ssID+'">'; 
				sshtml += '<div class="deleteButton"></div>';
				sshtml += '<input type="text" class="bfInputText ssName bfChangeListen" name="ss-'+ssID+'" value="Enter slideshow name">';
				sshtml += '<div class="editButton">';
				sshtml += '<a href="#/bigshow/bigshow-slideshows/'+ssID+'" class="editLink">';
				sshtml += 'edit';
				sshtml += '</a>';
				sshtml += '</div>';
				sshtml += '<div style="clear:both;"></div>';
				sshtml += '</div>';
			//
			$("#slideshows").append(sshtml);
			//
			handleUI(); 
			slideshowsFunctionality();
			//
	   	} else if (msg == 'LIMIT REACHED') {
			alert("You've reached the current slideshow limit. Delete a slideshow first before creating a new one or upgrade to an unlimited subscription.")
		} else {
		   	alert('There was an error creating a new slideshow. Please try again later. If problems persist, please contact support.');
		}
	});
});


// ]]>
</script>