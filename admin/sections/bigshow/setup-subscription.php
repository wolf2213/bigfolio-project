<?
//
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
//include_once("../../inc/db.inc.php");
//include_once("../../inc/functions.inc.php");
//
?>
<div class="content_inner" id="">
	<div class="contentHeader">
		<h1>BIG Show Subscription</h1>
	</div>  
	<div class="tip_message">
		BIG Show Unlimited only costs $99/year. Unlimited includes an unlimited number of slideshows each with up to 100 images in them. Enter the card details you want to use for this subscription. Payment for this year's subscription will be processed immediately. Your subscription renews automatically one year from the date of purchase. Updating billing information or canceling your subscription can be done from the <a href="#/bigshow/bigshow-slideshows">main BIG Show admin page</a> once the subscription has been activated. 
		<div class="payment-errors">
		</div>
	</div> 
	<div id="subscriptionForm"> 
		<legend>Billing Information</legend> 
		<ul>
		<!-- <form action="" method="POST" id="payment-form"> -->  
		<div id="payment-form">
			<li>
				<label>Card Number</label>   
				<input type="text" size="20" autocomplete="off" class="card-number bfInputText" style="width: 160px;" />
			</li>
			<li>
				<label>CVC</label>    
				<input type="text" size="4" autocomplete="off" class="card-cvc bfInputText" style="width: 50px;" /> 
			</li>
		    <li> 
				<label>Expiration (MM/YYYY)</label>
		        <input type="text" size="2" class="card-expiry-month bfInputText" style="width: 20px;" />
		        <span style="float:left; padding: 10px;"> / </span>
		        <input type="text" size="4" class="card-expiry-year bfInputText" style="width: 40px;" />
			</li> 
			<li>
		    	<button type="submit" id="bigShowStripeSubmit" class="submit-button">Submit Payment</button> 
			</li>
		</div>
		<!-- </form> -->
		</ul>
	</div> 
</div> 

<script type="text/javascript" src="https://js.stripe.com/v1/"></script>
<script type="text/javascript" src="/inc/stripe-php/StripeKey.js"></script>

<script type="text/javascript">
// <![CDATA[                        

$(document).ready(function() {
	
	$(".content_inner").hide();   

	//$("#payment-form").submit(function(event) { 
	$("#bigShowStripeSubmit").click(function() {
		// disable the submit button to prevent repeated clicks
		$(this).attr("disabled", "disabled");

		Stripe.createToken({
		    number: $('.card-number').val(),
		    cvc: $('.card-cvc').val(),
		    exp_month: $('.card-expiry-month').val(),
		    exp_year: $('.card-expiry-year').val()
		}, stripeResponseHandler);

		// prevent the form from submitting with the default action
		return false;
	});
	
	function stripeResponseHandler(status, response) {
	    if (response.error) {
	        //show the errors on the form
	        $(".payment-errors").html(response.error.message);
			$("#bigShowStripeSubmit").removeAttr("disabled");
	    } else {
	        var form$ = $("#payment-form");
	        // token contains id, last4, and card type
	        var token = response['id'];
	        // insert the token into the form so it gets submitted to the server
	        //form$.append("<input type='hidden' name='stripeToken' value='" + token + "'/>"); 
	        showSave();
			var data = new Object();
			data['stripeToken'] = token;
	        // and submit  
			$.ajax({
				//this is the php file that processes the data
				url: 'actions/bigshow/setup-subscription.php',	

				//POST method is used
				type: "POST",

				//pass the data			
				data: data,		

				//Do not cache the page
				cache: false,

				//success
				success: function (res) { 
					$.log('subscriptionSetup: '+res);   		
					if (res.indexOf('subscriptionSuccess') > -1) {					 
						alert('Your payment was successfully processed. An Email has been sent confirming the subscription.');
						location.href = "#/bigshow/bigshow-slideshows";
					} else {
						$(".payment-errors").html("There was a problem with your credit card. Please verify you have entered everything correctly and try again.");
						$("#bigShowStripeSubmit").removeAttr("disabled");
					}
					hideSave();			
				}		
			});
	        //form$.get(0).submit();
	    }
	} 
	
	showSpinner();
	
	setTimeout(function() { 
		Stripe.setPublishableKey(StripeKey);
		$(".content_inner").show(); 
		hideSpinner();
	}, 2000);
	
});

// ]]>
</script>