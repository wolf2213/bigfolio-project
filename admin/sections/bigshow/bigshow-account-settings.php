<?
session_start(); 
//
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');

// Include files
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");
//
// check if bishow db tables are present and install if need be..
$hasBIGshow = false;
$result = mysql_list_tables($db_name) or die(mysql_error());
while ($row = mysql_fetch_row($result)) {
	if($row[0]=='bigshow_slideshows') {
		$hasBIGshow = true;
	}
}
if (!$hasBIGshow) {
	include_once("bigshow-setup.php");
}                                     
include_once("bigshow-source-update.php");
// 
$all_slideshows = get_all_bigshows();
$loop = 0; 
if ($_SESSION['server'] == 'bigshow') {
	$freeLimit = 3;
} else {
	$freeLimit = 5;
}
// 
$s = get_settings();
$isUnlimited = $_SESSION['user']['bigshow_subscription'] == '1';
//
$storageID = $_SESSION['user']['storage_id'];
$imgStorage = "http://cdn.bigfoliobigshow.com.s3.amazonaws.com/users/" . $storageID{0} . '/' . $storageID{1} . '/' . $storageID{2} . '/' . $storageID . '/';
//

//
// colors
//
$colorsArr       = array();
$colorsLabels    = array();
//
$colorsArr[]     = 'body_color'; 
$colorsLabels[]  = 'Background color';
//
$colorsArr[]     = 'title_color'; 
$colorsLabels[]  = 'Title color';  
//
$colorsArr[]     = 'text_color';
$colorsLabels[]  = 'Text color'; 
//
$colorsArr[]     = 'custom_color_1'; 
$colorsLabels[]  = 'Link color';
// 
$colorsArr[]     = 'custom_color_2';
$colorsLabels[]  = 'Link hover color';
//
$colorsArr[]     = 'app_border_color';  
$colorsLabels[]  = 'Border color';


//
// slideshow speed
$fvalues = unserialize($s['flash_vars']); 
if (!isset($fvalues['slideshow_speed'])) {
	$fvalues['slideshow_speed'] = 5;
}
?>

<div class="content_inner" id="">
	<div class="contentHeader">
		<h1>BIG Show Settings</h1>
	</div>  
	<div class="tip_message">
		BIG Show is a quick and easy way to create elegant HTML5 slideshows for clients.
		<br>
		Use this area to manage your account settings for each slideshow.
		<br><br>
		<? if(!$isUnlimited){ ?>
			You can create up to <?=$freeLimit?> slideshows each with 50 images and music.
			<br>Unlimited use can be purchased <a href="#/bigshow/setup-subscription" target="bigshowBilling">here</a> for $99/year.
		<? } else { ?>
			To update the the credit card billed for the yearly subscription fee, click <a href="#/bigshow/update-subscription" target="bigshowBilling">here</a>. 
			<br>To cancel your BIG Show subscription, click <a href="#/bigshow/cancel-subscription" target="bigshowBilling">here</a>.
		<? } ?>
	</div>
	
	<ul>
		
		<li class="divider"></li>
		<h2>Account settings</h2>
		<li>
			<label for="username">Username (<a href="javascript:;" class="bfToolTip" title="This is used for your subdomain.">?</a>)</label>
			<input name="username" type="text" id="username" value="<?= $_SESSION['user']['username'] ?>"  class="bfInputText" />
		</li>
		<li>
			<label for="email">Email (<a href="javascript:;" class="bfToolTip" title="This is the email associated with your account. It is used for password resets.">?</a>)</label>
			<input name="email" type="text" id="email" value="<?= $_SESSION['user']['email'] ?>"  class="bfInputText" />
		</li>
		<li>
			<label for="password">Password</label>
			<input name="password" type="password" id="password" value=""  class="bfInputText" />
		</li>
		<li>
			<label for="passwordConf">Password (confirm)</label>
			<input name="passwordConf" type="password" id="passwordConf" value=""  class="bfInputText" />
		</li>
		<li>
			<label for="errorMsg"></label>
			<div id="errorMsg" style="padding: 10px 0; float:left;"></div>
		</li>
	</ul>
	
</div>


<script type="text/javascript">
// <![CDATA[   

var storageID = "<?=$storageID?>";
var imgStorage = "<?=$imgStorage?>";   
$.log("imgStorage: "+imgStorage);   
var usernameCheckInt;
var emailCheckInt;
var passCheckInt;      
var clientID = <?=$_SESSION['user']['id']?>;  
       

function settingsFunctionality() {
	$("#username").focus(checkUsername);
	$("#username").blur(stopCheckingUsername);
	
	$("#email").focus(checkEmail);
	$("#email").blur(stopCheckingEmail);
	
	$("#password").focus(checkPasswords);
	$("#password").blur(stopCheckingPasswords);
	$("#passwordConf").focus(checkPasswords);
	$("#passwordConf").blur(stopCheckingPasswords);	
}

function checkUsername() {
	usernameCheckInt = setInterval(function() {
		var username = cleanfile($("#username").val());
		if (username.length < 4) {
			$("#errorMsg").html("<span style='color:#ff0000;'>Your username must be at least 4 characters long.</font>");
		} else if (username != $("#username").val()) {
			$("#errorMsg").html("<span style='color:#ff0000;'>Your username can only consist of letters and numbers.</font>");
		} else {
			$.ajax({
				type: 'POST',
				url: 'actions/bigshow/check-username.php',
				data: {'username':username,'clientID':clientID}
			}).done(function( msg ) {
				$.log("msg: "+msg);
				if (msg==1) {
					$("#errorMsg").html("<span style='color:#ff0000;'>That username is taken.</font>");
					$("#saveBtn").attr("disabled", "disabled");
					$("#saveBtn").css("opacity",.5);
				} else {
					$("#errorMsg").html("<span style='color:#8BC53F;'>That username is available.</font>");
					$("#saveBtn").removeAttr("disabled"); 
					$("#saveBtn").css("opacity",1);
				}
			});
		}
	}, 500);
}
function stopCheckingUsername() {
	clearInterval(usernameCheckInt);
}

function checkEmail() {
	emailCheckInt = setInterval(function() {
		var email = $("#email").val();
		if (email.indexOf("@") == -1 || email.indexOf(".", email.indexOf("@")) == -1) {
			$("#errorMsg").html("<span style='color:#ff0000;'>Please enter a valid email.</font>");
		} else {
			$.ajax({
				type: 'POST',
				url: 'actions/bigshow/check-email.php',
				data: {'email':email,'clientID':clientID}
			}).done(function( msg ) {
				$.log("msg: "+msg);
				if (msg==1) {
					$("#errorMsg").html("<span style='color:#ff0000;'>That email is already being used with another account.</font>");
					$("#saveBtn").attr("disabled", "disabled");
					$("#saveBtn").css("opacity",.5);
				} else {
					$("#errorMsg").html("");
					$("#saveBtn").removeAttr("disabled"); 
					$("#saveBtn").css("opacity",1);
				}
			});
		}
	}, 1000);
}
function stopCheckingEmail() {
	clearInterval(emailCheckInt);
}

function checkPasswords() {
	passCheckInt = setInterval(function() {
		if ($("#passwordConf").val() != $("#password").val()) {
			$("#errorMsg").html("<span style='color:#ff0000;'>Passwords do not match.</font>");
			$("#saveBtn").attr("disabled", "disabled");
			$("#saveBtn").css("opacity",.5);
		} else if ($("#password").val() == "") {
			$("#errorMsg").html("");
			$("#saveBtn").removeAttr("disabled"); 
			$("#saveBtn").css("opacity",1);
		} else {
			$("#errorMsg").html("<span style='color:#8BC53F;'>Passwords match.</font>");
			$("#saveBtn").removeAttr("disabled"); 
			$("#saveBtn").css("opacity",1);
		}	
	}, 500);
}
function stopCheckingPasswords() {
	clearInterval(passCheckInt);
} 

settingsFunctionality();

// ]]>
</script>