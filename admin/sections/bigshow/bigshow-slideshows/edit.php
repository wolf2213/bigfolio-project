<?
//
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../../inc/db.inc.php");
include_once("../../../inc/functions.inc.php");  
// 
$s = get_settings();
$fvalues = unserialize($s['flash_vars']);
//
$ssData = explode("-", $_GET['val']);  // separate val
for ($i=0; $i<count($ssData); $i++) {
	if (stristr($ssData[$i], ".php")) {
		$ssData[$i] = str_replace(".php", "", $galData[$i]); // strip .php from all instances
	}
}
//substr($_GET['val'], 0, -4)
//
$ssID = $ssData[0]; 
if (!isset($ssData[1])) { 
	if (isset($_SESSION['gallery_view_type'])) {
		$type = $_SESSION['gallery_view_type'];
	} else {
		$type = 'small'; 
		$thumbSize = 'thumb';
	}
} else {
	$type = $ssData[1]; 
}
if ($type == 'large') {
	$thumbSize = 'small';
	$viewLabel = 'Large thumbnails';
} else if ($type == 'small') {
	$thumbSize = 'thumb'; 
	$viewLabel = 'Small thumbnails';
} else if ($type == 'list') {
	$thumbSize = 'medium';
	$viewLabel = 'List';
}
//
$ss = get_bigshow($ssID);
$images = get_bigshow_images($ssID);
$storageID = $ss['storage_id'];  
$settings = json_decode($ss['settings']); 

if ($_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == 'macbook' || $_SERVER['HTTP_HOST'] == 'bigshow.phpfogapp.com' || $_SERVER['HTTP_HOST'] == 'bfbs.phpfogapp.com') { 
	$ssURL = 'bigshow_dev/gallery/index.php?s='.$ss['url'].'&u='.$_SESSION['user']['username'];
} else if ($_SERVER['HTTP_HOST'] == 'www.bigsho.ws') {
	$ssURL = "http://" . $_SESSION['user']['username'] . ".bigsho.ws/" . $ss['url'];
} else {
	$ssURL = $_SESSION['user']['temp_url'].'gallery/index.php?s='.$ss['url'];  
}   

$musicFiles = array();
if (isset($settings->music_files)) {
	$musicFiles = explode(",", $settings->music_files);
}

$imgStorage = "http://cdn.bigfoliobigshow.com.s3.amazonaws.com/users/" . $storageID{0} . '/' . $storageID{1} . '/' . $storageID{2} . '/' . $storageID . '/' . $ssID . '/';
 
//
?>
<div class="content_inner" id="slideshow">
	<div class="contentHeader">
		<h1>BIG Show Slideshow</h1>
	</div>  
	<div class="tip_message">
	    Use this area to administer content for this slideshow. View this slideshow here:<br>
		<a href="<?=$ssURL?>" target="_blank"><?=$ssURL?></a>
	</div> 
	<div id="settings">  
		<legend>Settings</legend>
		<ul>
			<input type="hidden" name="id" value="<?=$ss['id']?>" /> 
			<li class="divider"></li>
			<li>
				<label for="name">Slideshow name</label>
				<input name="name" id="name" class="bfInputText bfChangeListen" value="<?=$ss['name'];?>" />
			</li>
			<? if ($s['logo_file'] != "NONE" && $s['logo_file'] != "" && strtolower(substr($s['logo_file'], -3)) != 'swf') { ?>
			<?
			if (!isset($settings->show_logo)) {
				$settings->show_logo = 1;
			}
			?>
			<li>
				<label for="show_logo">Show logo</label>
				<div class="uiButtonSet">
					<label for="show_logo_yes" class="radioLabel">Yes</label>
					<input name="show_logo" id="show_logo_yes" type="radio" value="1" class="bfChangeListen" <? if ($settings->show_logo=='1') { ?>checked="checked"<? } ?> />
					<label for="show_logo_no" class="radioLabel">No</label>
					<input name="show_logo" id="show_logo_no" type="radio" value="0" class="bfChangeListen" <? if ($settings->show_logo=='0') { ?>checked="checked"<? } ?> />
				</div> 
				<div id="ssLogo" <? if ($settings->show_logo=='0') { ?>style="display:none;" <?}?>>
					<? if (substr($s['logo_file'], 0, 7) == 'http://') { ?>
						<img src="<?= $s['logo_file'] ?>" border="0" id="logoFileImg"/>
					<? } else { ?>
						<img src="<?= $_SESSION['user']['temp_url'] ?>/images/<?= $s['logo_file'] ?>" border="0" id="logoFileImg"/>
					<? } ?>
				</div>
			</li>
			<? } ?>
			<li>
				<label for="slideshows_speed">Slideshow speed (<a href="javascript:;" class="bfToolTip" title="This setting controls the speed of the slideshows. This is the amount of time in seconds an image shows before the next image loads. Loading times for individual images may vary.">?</a>)</label>
				<?
				if (isset($settings->slideshow_speed)) {
					$fval = $settings->slideshow_speed;
				} else if (isset($fvalues['slideshow_speed'])){
					$fval = $fvalues['slideshow_speed'];
				} else {
					$fval = 5;
				}
				?>
				<div class='bfSlider' title='0,20,<?=$fval?>'></div>
				<input type='text' class='bfInputText bfSliderLabel bfChangeListen' name='slideshow_speed' id='slideshow_speed' value='<?=$fval?>' />
			</li>
			<li class="divider"></li>
			<li>
				<?
				if (isset($settings->body_color)) {
					$fval = $settings->body_color;
				} else {
					$fval = $s['body_color'];
				}
				?>
				<label>Background color</label>
				<input type='hidden' name='body_color' id='body_color' value='<?=$fval?>' class='bfChangeListen' />
				<div class='bfSwatch' style='background-color:<?=$fval?>'></div>
			</li>
			<li>
				<?
				if (isset($settings->title_color)) {
					$fval = $settings->title_color;
				} else {
					$fval = $s['title_color'];
				}
				?>
				<label>Title color</label>
				<input type='hidden' name='title_color' id='title_color' value='<?=$fval?>' class='bfChangeListen' />
				<div class='bfSwatch' style='background-color:<?=$fval?>'></div>
			</li>
			<li>
				<?
				if (isset($settings->text_color)) {
					$fval = $settings->text_color;
				} else {
					$fval = $s['text_color'];
				}
				?>
				<label>Text color</label>
				<input type='hidden' name='text_color' id='text_color' value='<?=$fval?>' class='bfChangeListen' />
				<div class='bfSwatch' style='background-color:<?=$fval?>'></div>
			</li>
			<li>
				<?
				if (isset($settings->link_color)) {
					$fval = $settings->link_color;
				} else {
					$fval = $s['custom_color_1'];
				}
				?>
				<label>Link color</label>
				<input type='hidden' name='link_color' id='link_color' value='<?=$fval?>' class='bfChangeListen' />
				<div class='bfSwatch' style='background-color:<?=$fval?>'></div>
			</li>
			<li>
				<?
				if (isset($settings->link_hover_color)) {
					$fval = $settings->link_hover_color;
				} else {
					$fval = $s['custom_color_2'];
				}
				?>
				<label>Link hover color</label>
				<input type='hidden' name='link_hover_color' id='link_hover_color' value='<?=$fval?>' class='bfChangeListen' />
				<div class='bfSwatch' style='background-color:<?=$fval?>'></div>
			</li>
			<li>
				<?
				if (isset($settings->border_color)) {
					$fval = $settings->border_color;
				} else {
					$fval = $s['app_border_color'];
				}
				?>
				<label>Border color</label>
				<input type='hidden' name='border_color' id='border_color' value='<?=$fval?>' class='bfChangeListen' />
				<div class='bfSwatch' style='background-color:<?=$fval?>'></div>
			</li>
			<li>
				<?
				if (isset($settings->footer_color)) {
					$fval = $settings->footer_color;
				} else {
					$fval = $s['body_color'];
				}
				?>
				<label>Footer color</label>
				<input type='hidden' name='footer_color' id='footer_color' value='<?=$fval?>' class='bfChangeListen' />
				<div class='bfSwatch' style='background-color:<?=$fval?>'></div>
			</li>
			<li class="divider"></li>
			<li>                      
				<?
				if (!isset($settings->show_filenames)) {
					$settings->show_filenames = 0;
				}
				?>
				<label for="show_filenames">Show filenames</label>
				<div class="uiButtonSet">
					<label for="show_filenames_yes" class="radioLabel">Yes</label>
					<input name="show_filenames" id="show_filenames_yes" type="radio" value="1" class="bfChangeListen" <? if ($settings->show_filenames=='1') { ?>checked="checked"<? } ?> />
					<label for="show_filenames_no" class="radioLabel">No</label>
					<input name="show_filenames" id="show_filenames_no" type="radio" value="0" class="bfChangeListen" <? if ($settings->show_filenames=='0') { ?>checked="checked"<? } ?> />
				</div> 
			</li>
			<li>
				<label for="password">Password for this slideshow</label>
				<input type="text" class="bfInputText bfChangeListen" name="password" value="<?=$settings->password?>" />
			</li>
			<li class="divider"></li>
		</ul>
	</div>
	 
	<div style="clear:both;"></div>
	
	<legend>Images</legend>
	
	<div class="galleryUploader">
		<input name="galleryfiles" type="file" class="galleryfiles" id="galleryfiles" size="10" /> 
		<div id="status-message"></div>
		<div id="gallery-queue"></div>
	</div>
	
	<div class="galleryImages">
		<? foreach($images as $img) { 
			//
			$fileParts = getNameAndExtension($img['image_file']);
			//
			$prettyName = $img['image_file'];
		   	$html  = "";
         	$html .= '<div class="thumbItem bfSortable '.$type.'Item" id="img'.$img['image_id'].'">';
			$html .= '<div class="deleteButton"></div>';
			$html .= '<div style="clear:both;"></div>';
			$html .= '<div class="thumbFileLabel '.$type.'Label">'.$prettyName.'</div>';
			
			$html .= '<div class="thumbHolder '.$type.'Thumb">';
			$html .= '<a rel="galImages" href="'.$imgStorage.$img['image_file'].'" title="'.$img['image_caption'].'">';
			$html .= '<img src="'.$imgStorage.$fileParts[0].'_'.$thumbSize.'.'.$fileParts[1].'" title="'.$img['image_file'].'"/>';
			$html .= '</a>';
			$html .= '</div>';
			$html .= '<div class="'.$type.'ImageCaption imgCap">'; 
			$html .= '<textarea class="bfChangeListen" name="caption'.$img['image_id'].'">'.$img['image_caption'].'</textarea>';
			$html .= '</div>';
			$html .= '<input type="checkbox" class="imgCheck"/>';
			$html .= '<div class="captionButton"></div>';
			// 
			/*
			$html .= '<select name="align'.$img['image_id'].'" class="imageAlign">';
			//
				$html .= '<option value="center"';
				if ($img['align'] == 'center') { 
					$html .= 'selected=selected';
				}
				$html .= '>center</option>'; 
				//
				$html .= '<option value="left"';
				if ($img['align'] == 'left') { 
					$html .= 'selected=selected';
				}
				$html .= '>left</option>';
				//
				$html .= '<option value="right"';
				if ($img['align'] == 'right') { 
					$html .= 'selected=selected';
				}
				$html .= '>right</option>'; 
			//
			$html .= '</select>';
			*/ 
			//
			$html .= '</div>'; 
			echo $html; 
		} ?> 
	</div>
	<div style="clear:both;"></div> 
	
	<ul>
  	  <li class="divider"></li>
	</ul>
	
	<legend>Music</legend>
	
	<div class="musicUploader">
		<input name="musicfiles" type="file" class="musicfiles" id="musicfiles" size="10" /> 
		<div id="music-status-message"></div>
		<div id="music-queue"></div>
	</div>
	<input type="hidden" id="music_files" name="music_files" value="<?=$settings->music_files?>" />
	<div id="musicFilesHolder">
		<!-- music files here -->
		<?
		foreach($musicFiles as $file) {
			if ($file != "") {
				$html  = "";
	         	$html .= '<div class="musicItem bfSortable">';
				$html .= '<div class="deleteButton"></div>';
				$html .= '<div style="clear:both;"></div>';
				$html .= '<div class="musicFileLabel">';
				$html .= '<a href="'.$imgStorage.$file.'" target="_blank" title="'.$file.'">'.$file.'</a>';
				$html .= '</div>';
				$html .= '</div>'; 
				echo $html;
			}
		}
		?>
 	</div>

	<ul>
  	  <li class="divider"></li>
	</ul> 

	<legend>Footer</legend>
	<div class="tip_message">
	    Use this area to add text below the main image area of the slideshow. You can also add standards compatible embed or iframe code.
	</div>
	<!-- page text editor -->  
	<ul>
		<li id="bfTextEditor">
			<label for="pageContent">Footer text</label> 
			<div id="bfTextEditorControls"> 
				<a href="javascript:;" class="bfToolTip" title="Click this to apply link html to selected text within the editor. You will be prompted to enter a URL and target." id="linkInsert">&lt; link /&gt;</a>
				<a href="javascript:;" class="bfToolTip" title="Click this to apply bold formatting to selected text within the editor." id="boldInsert">&lt; bold /&gt;</a>
				<a href="javascript:;" class="bfToolTip" title="Click this to apply italic formatting to selected text within the editor." id="italicInsert">&lt; italic /&gt;</a>
			</div>
			<textarea name="footer" id="pageContent" class="bfPageText bfChangeListen bfMarkupText ssFooter"><?=editorCharReplacements($ss['footer']); ?></textarea>
		
			<div style="clear:both;"></div>
		</li> 
		<div class="tip_message">
		    If you have a <a href="http://disqus.com/">disqus</a> account, you can enter your forum <a href="http://docs.disqus.com/help/8/">shortname</a> here to add commenting and other social functionality to this slideshow. Sign up for a disqus account <a href="http://disqus.com/">here</a>.  
		</div>
		<li>
			<label for="disqusshortname">Forum shortname</label>
			<input type="text" class="bfInputText bfChangeListen" name="disqus_shortname" value="<?=$settings->disqus_shortname?>" />
		</li> 
	</ul>
</div> 

<div class="actionsPanel">
	<div class="icon">
	</div>
	<div class="inner">
		<ul>
			<li> 
				<label for="galleryView">View mode</label>
				<select name="viewMode" class="smallSelect" id="viewMode">
					<option><?=$viewLabel?></option>
					<option>-----------------</option>
					<option value="small">Small Thumbnails</option>
					<option value="large">Large Thumbnails</option>
					<option value="list">List</option>
				</select>
			</li>
			
			<li>
				<label for="bulkActions">Bulk actions</label>
		   		<select name="bulkActions" class="smallSelect" id="bulkActions">
					<option>Select One</option>
					<option>-----------------</option>
					<option value="delete">Delete Selected</option>
				</select> 
			</li>
			<li>
			   	<label for="selectActions">Select</label>
				<button id="selectAll">All</button>
				<button id="selectNone">None</button>
			</li>
			<li>
				<label for="sortByFileName">Sort by File Name</label>
				<button id="sortByFileName">Sort</button>
			</li>
		</ul>
	</div>
</div>


<script type="text/javascript">
// <![CDATA[ 

var storageID = "<?=$storageID?>";
var ssID = "<?=$ssID?>";
var imgStorage = "<?=$imgStorage?>"; 

var type = "<?=$type?>";
var thumbSize = "<?=$thumbSize?>";

var lastThumbChecked;



function slideshowFunctionality() {  
	
	
	$(".captionButton").unbind("click");
	$(".captionButton").click(function() {   
		$.log('caption display: '+$(this).parent().find(".imgCap").css("display"));
		if ($(this).parent().find(".imgCap").css("display") == "none") {
			$(this).parent().find(".imgCap").show();
			//$(this).parent().find(".thumbHolder").hide();
		} else {
			$(this).parent().find(".imgCap").hide();
			//$(this).parent().find(".thumbHolder").show();
		}
	});
	 
	
	$(".thumbItem").find(".deleteButton").unbind("click"); 
	$(".thumbItem").find(".deleteButton").click(function() {
		var save = confirm("Are you sure you want to delete this image from this slideshow? This cannot be undone.");
		if (save) { 
			removeImage($(this).parent()); 
		}
	});
	
	<? if ($type == 'list') { ?>
	$(".thumbFileLabel").unbind("click");
	$(".thumbFileLabel").click(function() {
    	$(this).parent().find(".thumbHolder").slideToggle('fast');
	});
	<? } ?>
	
	//
	// handle check box shift functionality
	$(".imgCheck").unbind("click");
	$(".imgCheck").click(function() {
		var parentID = $(this).parent().attr("id");
		var isChecked = $(this).attr("checked"); 
		//
		// check if shift key is down  
		if (!event.shiftKey) {
			lastThumbChecked = $(this).parent();
			return;
		}
		//
		// get index
		var thisIndex = $(".thumbItem").index($(this).parent());
		var lastIndex = $(".thumbItem").index(lastThumbChecked);
		//
		// determine if we should move up the dom or down
		if (thisIndex > lastIndex) {
			var start = lastIndex;
			var end = thisIndex;
		} else {
			var start = thisIndex;
			var end = lastIndex;
		} 
		
		for (var i=start; i<=end; i++) { 
			if (isChecked) {
				$(".thumbItem:eq("+i+")").find(".imgCheck").attr("checked", "checked");
			} else {
				$(".thumbItem:eq("+i+")").find(".imgCheck").removeAttr("checked");
			}
		}
		 
	}); 
	
	
	/* BEGIN TEXT EDITOR */
	$('#pageContent').keyup(update).mousedown(update).mousemove(update).mouseup(update);  //.imgCap
	var curSelection;
	function update(e) {
		// here we fetch our text range object
		curSelection = $(this).getSelection();//.text;
	}                                                 
	//
	// link insert
	$("#linkInsert").click(function(e) { 

		if (curSelection.start == curSelection.end) {
			alert('Please select some text first.');
			return;
		}

		var url = prompt("Enter the URL to link to:"); 
		if (url == "") {
			return;
		}
		var target = prompt("Enter the target window for the link. To open in a new window, enter _blank. To open in the same window, leave this blank.");

		var allText      = $("#pageContent").val();
		var selText      = $('#pageContent').getSelection();
		var replaceText  = ""; 
			replaceText += allText.substr(0, selText.start);
			replaceText += '<a href="'+url+'"';
			if (target != "") {
				replaceText += ' target="'+target+'"';
			}
			replaceText += '>'+selText.text+'</a>';
			replaceText += allText.substr(selText.end, allText.length); 

		$("#pageContent").val(replaceText);

	});
	//
	// bold insert
	$("#boldInsert").click(function(e) { 

		if (curSelection.start == curSelection.end) {
			alert('Please select some text first.');
			return;
		}

		var allText      = $("#pageContent").val();
		var selText      = $('#pageContent').getSelection();
		var replaceText  = ""; 
			replaceText += allText.substr(0, selText.start);
			replaceText += '<b>'+selText.text+'</b>';
			replaceText += allText.substr(selText.end, allText.length); 

		$("#pageContent").val(replaceText);

	});
	//
	// italic insert
	$("#italicInsert").click(function(e) { 

		if (curSelection.start == curSelection.end) {
			alert('Please select some text first.');
			return;
		}

		var allText      = $("#pageContent").val();
		var selText      = $('#pageContent').getSelection();
		var replaceText  = ""; 
			replaceText += allText.substr(0, selText.start);
			replaceText += '<i>'+selText.text+'</i>';
			replaceText += allText.substr(selText.end, allText.length); 

		$("#pageContent").val(replaceText);

	});
	//
	// image insert
	$("#imageInsert").click(function(e) {
		var imgURL = prompt("Enter the URL of the image:"); 
		if (imgURL == "") {
			return;
		}

		var allText      = $("#pageContent").val();
		var selText      = $('#pageContent').getSelection();
		var replaceText  = ""; 
			replaceText += allText.substr(0, selText.end);
			replaceText += '<img src="'+imgURL+'" alt="" />';
			replaceText += allText.substr(selText.end, allText.length); 

		$("#pageContent").val(replaceText);

	});
	
}
slideshowFunctionality();  

$(".galleryImages").sortable({
	update: function(event, ui) { 
		var d = new Object(); 
		d['imgList'] = new Array();
		$(this).find(".thumbItem").each(function() {
			d['imgList'].push($(this).attr("id").substr(3)); // pushes imgNNN minus the img
		});
		$.log("d['imgList']: "+d['imgList']);

		$.ajax({
  			type: 'POST',
  			url:'actions/bigshow/update-slideshow-images-order.php',
  			data: d
		}); 
	}
});


//
// gallery image upload  
$(".galleryfiles").uploadifive({
  'uploadScript'   : 'inc/bigshow_upload.php', 
  'method'         : 'post',
  'formData'       : {'storageID':storageID,'ssID':ssID},      
  'cancelImg'      : 'uploadifive-cancel.png',
  'multi'          : true,
  'auto'           : true,
  'fileExt'        : '*.jpg;*.jpeg;*.png;',
  'fileDesc'       : 'Image Files (.JPG, .JPEG, .PNG)',
  'queueID'        : 'gallery-queue', 
  'buttonText'     : 'UPLOAD IMAGES',
  'width'          : 140,
  'queueSizeLimit' : 100,
  'simUploadLimit' : 1,
  'removeCompleted': true,
  'onSelectOnce'   : function(event,data) {
      	//$('#status-message').text(data.filesSelected + 'uploading...');
		$('#status-message').text('uploading...');
    }, 
  'onUploadComplete'     : function(file, data) { 
	//	var fileParts = getNameAndExtension(fileObj['name']); 
	//	var imageFile = cleanfile(fileParts[0])+'.'+fileParts[1];
		var imageFile = file.name;
	//  	$.log('fileName: '+cleanfile(fileParts[0])+'.'+fileParts[1]);
	// 	$.log('response: '+response); 
		//
		addImageToGallery(imageFile);
		//
   	},
  'onAllComplete'  : function(event,data) {
		$.log("filesUploaded: "+data.filesUploaded);
		$.log("data.errors: "+data.errors);
		if (data.errors > 0) {
      		$('#status-message').text("There was an error with the upload.");
		} else {
			$('#status-message').text("");
		}
    }
});

$.log("using newest file");

function addImageToGallery(imageFile) { 
	
	$.ajax({
      type: 'POST',
      url:'actions/bigshow/add-image-to-slideshow.php',
      data: ({'ssID':ssID, 'imageFile':imageFile})
    }).done(function( msg ) {
	  	$.log( "msg: " + msg );
	 	if (msg.substr(0,7) == "SUCCESS") {
			//
			var imgID = msg.substr(8);
			//
		    var html = "";
			html += '<div class="thumbItem bfSortable '+type+'Item" id="img'+imgID+'">';
			html += '<div class="deleteButton"></div>';
			html += '<div style="clear:both;"></div>';
			html += '<div class="thumbFileLabel '+type+'Label">'+imageFile+'</div>';
			html += '<div class="thumbHolder '+type+'Thumb">';
			html += '<a rel="galImages" href="'+imgStorage+imageFile+'">';
			html += '<img src="'+imgStorage+imageFile+'_'+thumbSize+'.'+imageFile+'" title="'+imageFile+'"/>';
			html += '</a>';
			html += '</div>'; 
			html += '<div class="'+type+'ImageCaption imgCap">'; 
			html += '<textarea class="bfChangeListen" name="caption'+imgID+'"></textarea>';
			html += '</div>';
			html += '<input type="checkbox" class="imgCheck"/>';
			html += '<div class="captionButton"></div>';
			// 
			/*
			html += '<select name="align'+imgID+'" class="imageAlign">';
			html += '<option value="center" selected=selected>center</option>';
			html += '<option value="left">left</option>';
			html += '<option value="right">right</option>';  
			html += '</select>';
			*/
			//
			html += '</div>';
			//
			$(".galleryImages").append(html);
			handleUI();
			slideshowFunctionality();
	   	} else if (msg == 'LIMIT REACHED') { 
			//
			// now check for duplicates
			$.ajax({
	          type: 'POST',
	          url:'actions/bigshow/check-for-duplicate-images.php',
	          data: ({'imageFile':imageFile, 'ssID':ssID})
	        }).done(function( msg ) {
			  	$.log( "duplicates?: "+msg);  
				//
				// if no duplicates (0) then remove the file
				if (msg == '0') { 
					$.ajax({
			          type: 'POST',
			          url:'inc/bigshow_file_remove.php',
			          data: ({'storageID':storageID, 'ssID':ssID, 'file':imageFile})
			        }).done(function( msg ) {
					  $.log( "msg: " + msg );
					});
				} 
			});
			alert("You've reached the number of images allowed for each gallery. Delete some images first before adding more.");
		} else {
			alert("There was a problem adding the image "+imageFile+" to this slideshow. Please try again later. If problems persist, please contact support.");
		}
		
	});
} 

$(".viewModeButton").click(function() {
   	changeGalleryView($(this).val());
});
$("#viewMode").change(function() {
	changeGalleryView($(this).val());
});

function removeImage(thumb) { 
	var imageFile = thumb.find("img").attr("title");
   	var imageID = thumb.attr('id').substr(3);
	$.ajax({
      type: 'POST',
      url:'actions/bigshow/remove-image-from-slideshow.php',
      data: ({'imageID':imageID})
    }).done(function( msg ) {
	  	$.log( "msg: "+msg);
		if (msg.substr(0,7) == 'SUCCESS') {
			thumb.fadeOut("fast", function() {
			   thumb.remove(); 
			});
		
			//
			// now check for duplicates
			$.ajax({
	          type: 'POST',
	          url:'actions/bigshow/check-for-duplicate-images.php',
	          data: ({'imageFile':imageFile, 'ssID':ssID})
	        }).done(function( msg ) {
			  	$.log( "duplicates?: "+msg);  
				//
				// if no duplicates (0) then remove the file
				if (msg == '0') { 
					$.ajax({
			          type: 'POST',
			          url:'inc/bigshow_file_remove.php',
			          data: ({'storageID':storageID, 'ssID':ssID, 'file':imageFile})
			        }).done(function( msg ) {
					  $.log( "msg: " + msg );
					});
				} 
			});
			//
		} else {
			alert("There was an error removing the image from this gallery.");
		}
	});
}                         

function changeGalleryView(type) {
	var v = "#";
    for (var i=0; i<paths.length; i++) { 
	 	if (paths[i] != "") {
			if (i == paths.length - 1) {
				var curPath = paths[i].split('-');
				v += "/"+curPath[0] + '-' + type;
			} else {
				v += "/"+paths[i];
			} 
		}
	}
	$.log('changeGalleryView: '+type+', '+v);
	location.href = v;
}

$("#bulkActions").change(function() { 
	
   	if ($(this).val() == 'delete') {
		//
	    var save = confirm("Are you sure you want to delete these images from this gallery? This cannot be undone.");
		if (save) { 
			$(".thumbItem").each(function() {
				if ($(this).find(".imgCheck").attr("checked") == "checked") {
				   removeImage($(this)); 
				}
			});
		}
		
	} 
	
	// reset selection
	$(this).val("Select One");
});
$("#selectAll").click(function() {
	$(".imgCheck").attr("checked","checked");
});
$("#selectNone").click(function() {
	$(".imgCheck").removeAttr("checked");
});

$(".actionsPanel").find(".icon").click(function() {
	if ($(".actionsPanel").hasClass("showing")) {
		$(".actionsPanel").removeClass("showing");
		$(".actionsPanel").animate({
			"marginLeft":2
		}, 'fast');
	} else { 
		$(".actionsPanel").addClass("showing");
		$(".actionsPanel").animate({
			"marginLeft":-200
		}, 'fast');
	}
});

$("#sortByFileName").click(function() {
	var save = confirm("Are you sure you want to reorder all images in this gallery by filename? This cannot be undone.");
	if (save) { 
		showSave();
		$.ajax({
	      type: 'POST',
	      url:'actions/bigshow/reorder-slideshow-by-filename.php',
	      data: ({'ssID':ssID})
	    }).done(function( msg ) {
			$.log("msg: "+msg);
			hideSave();
			location.reload();
		})
	}
});


/* music */
//
// music uploader 
$("#musicfiles").uploadifive({
  'uploadScript'   : 'inc/bigshow_upload.php', 
  'method'         : 'post',
  'formData'       : {'storageID':storageID,'ssID':ssID},      
  'cancelImg'      : 'uploadifive-cancel.png',
  'multi'          : true,
  'auto'           : true,
  'fileExt'        : '*.mp3;',
  'fileDesc'       : 'MP3 Files (.mp3,)',
  'queueID'        : 'music-queue', 
  'buttonText'     : 'UPLOAD MP3S',
  'width'          : 125,
  'queueSizeLimit' : 3,
  'simUploadLimit' : 1,
  'removeCompleted': true,
  'onSelectOnce'   : function(event,data) {
      	//$('#status-message').text(data.filesSelected + 'uploading...');
		$('#music-status-message').text('uploading...');
    }, 
  'onUploadComplete'     : function(file, data) { 
	//	var fileParts = getNameAndExtension(fileObj['name']); 
	//	var musicFile = cleanfile(fileParts[0])+'.'+fileParts[1];
		var musicFile = file.name;
	//  	$.log('fileName: '+cleanfile(fileParts[0])+'.'+fileParts[1]);
	//  	$.log('response: '+response); 
		//
		var html = "";
		html += '<div class="musicItem bfSortable">';
		html += '<div class="deleteButton"></div>';
		html += '<div style="clear:both;"></div>';
		html += '<div class="musicFileLabel">'
		html += '<a href="'+imgStorage+musicFile+'" title="'+musicFile+'" target="_blank" title="'+musicFile+'">'+musicFile+'</a>';
		html += '</div>';
		//
		$("#musicFilesHolder").append(html);
		musicFunctionality(); 
		updateMusicOrder();
		//
	  	
   	},
  'onAllComplete'  : function(event,data) {
		$.log("filesUploaded: "+data.filesUploaded);
		$.log("data.errors: "+data.errors);
		if (data.errors > 0) {
      		$('#music-status-message').text("There was an error with the upload.");
		} else {
			$('#music-status-message').text("");
		}
    }
});

function musicFunctionality() {
	$("#musicFilesHolder").sortable(
		{
		    update: function(event, ui) { 
				updateMusicOrder();
      		}
		}
	); 
	$("#musicFilesHolder").find(".deleteButton").unbind("click");
	$("#musicFilesHolder").find(".deleteButton").click(function() { 
			//
			var save = confirm("Are you sure you want to delete this file? This cannot be undone");
			if (save) {
				//
			  	var fileName = $(this).parent().find('a').attr("title");
			    $.ajax({
		          type: 'POST',
		          url:'inc/bigshow_file_remove.php',
			      data: ({'storageID':storageID, 'ssID':ssID, 'file':fileName})
		        }).done(function( msg ) {
				  $.log( "msg: " + msg );
				});
				//
				var thumb = $(this).parent();
				thumb.fadeOut('fast', function() {
				    thumb.remove(); 
					updateMusicOrder();
				});
			}
			// 
	});
	$("#musicFilesHolder").disableSelection();
} 
function updateMusicOrder() {
	var imagesArr = new Array();
	$('#musicFilesHolder').find("a").each(function(index, value) {
		imagesArr.push($(this).attr("title"));
	}); 
	var imagesStr = imagesArr.join(',');
	$("#music_files").val(imagesStr);
	
}

musicFunctionality();


/* settings */

$("#show_logo_yes").click(function() {
   $("#ssLogo").slideDown('fast'); 
});

$("#show_logo_no").click(function() {
   $("#ssLogo").slideUp('fast'); 
});


// ]]>
</script>