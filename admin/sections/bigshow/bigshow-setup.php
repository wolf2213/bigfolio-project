<?
//
// create slideshows table
$q  = "CREATE TABLE IF NOT EXISTS `bigshow_slideshows` (";
$q .= "`id` INT( 5 ) NOT NULL AUTO_INCREMENT ,";
$q .= "`storage_id` VARCHAR( 255 ) NOT NULL ,";
$q .= "`name` VARCHAR( 500 ) NOT NULL ,";
$q .= "`url` VARCHAR( 500 ) NOT NULL ,";
$q .= "`header` TEXT NOT NULL ,";
$q .= "`footer` TEXT NOT NULL ,";
$q .= "`settings` TEXT NOT NULL ,";
$q .= "PRIMARY KEY (  `id` ) ,";
$q .= "INDEX (  `id` )";
$q .= ") ENGINE = MYISAM ;";

$setup = mysql_query($q)
	or die('Bad query'); 
	
// create slideshow images table
$q  = "CREATE TABLE IF NOT EXISTS `bigshow_images` (";
$q .= "`image_id` INT( 8 ) NOT NULL AUTO_INCREMENT ,";
$q .= "`slideshow_id` INT( 5 ) NOT NULL ,";
$q .= "`order_num` INT( 4 ) NOT NULL ,";
$q .= "`image_file` VARCHAR( 255 ) NOT NULL ,";
$q .= "`image_caption` TEXT NOT NULL ,";
$q .= "`align` VARCHAR( 20 ) NOT NULL ,";
$q .= "PRIMARY KEY (  `image_id` ) ,";
$q .= "INDEX (  `image_id` )";
$q .= ") ENGINE = MYISAM ;";

$setup = mysql_query($q)
	or die('Bad query');

//
?>