<?
//
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");
// 
$s = get_settings();
$fieldset = unserialize($s['contact_form_fields']);
if (count($fieldset < 6)) {
	// We should reset the contact fields here if they're broken
	
}
$template = get_property('template'); 
$templateType = get_property('TEMPLATE_TYPE');
//
// check whether there is add_contact_form functionality with this template 
$hasAddToPage = false;
$result = mysql_query("SHOW COLUMNS FROM pages");
if (!$result) {
    echo '<!-- CANNOT LIST PAGE FIELD NAMES: ' . mysql_error() . '-->';
} else if (mysql_num_rows($result) > 0) {
    while ($row = mysql_fetch_assoc($result)) { 
		//echo $row['Field'] . '<br>';
        if ($row['Field'] == 'add_contact_form') {
	    	$hasAddToPage = true;
		}	
    }
}
//
if (!$hasAddToPage == true) {
	// call curl call to get contact image existence
	$postArr = array();
	$postArr['u'] = $_SESSION['user']['email'];
	$postArr['p'] = $_SESSION['user']['password'];
	$url = SERVER . '_bfadmin/check_for_contact_image.php'; // 
	$contactImageExists = curl_post($url, $postArr, array());
}
// 
//$s['contact_form_extra'] = "<![CDATA[" . $s['contact_form_extra'] . ']]>';
if (substr($s['contact_form_extra'], 0, 9) == "<![CDATA[") {
   $s['contact_form_extra'] = substr($s['contact_form_extra'], 9, -3); 
}
?>
<div class="content_inner" id="contactForm">
	<div class="contentHeader">
		<h1>Contact Form -</h1>  <a href="https://vimeo.com/127729810" target="_blank"><img src="img/watch-this-video2.png" border="0" style="position:absolute; margin-left:240px; margin-top:-50px;" /></a>
	</div>  
	<div class="tip_message">
	    Use this area to control the contact form on your site. Your contact form allows up to 6 custom text fields and a comments field.
	</div>
	<div id="editPane">
		<div id="seoSettings" <?if(!isset($s['contact_meta_title'])){?>style="display:none;"<?}?>>
			<div id="metaSettings"> 
				<ul>
					<li class="divider"></li>
					<? if (isset($s['contact_meta_title'])) { ?>
					<?
					$key = 'contact_meta_title';
					$val = $s[$key];
					?>
					<li class="metaSetting">
						<label for="<?=$key?>"><?=ucfirst(str_replace("_", " ", $key))?></label>                                                             
						<input type="text" class="bfInputText bfChangeListen" name="<?=$key?>" value="<?=$val?>" />
					</li> 
					<? } ?>
					
					<? if (isset($s['contact_meta_description'])) { ?>
					<?
					$key = 'contact_meta_description';
					$val = $s[$key];
					?>
					<li class="metaSetting">
						<label for="<?=$key?>"><?=ucfirst(str_replace("_", " ", $key))?></label>                                                             
						<input type="text" class="bfInputText bfChangeListen" name="<?=$key?>" value="<?=$val?>" />
					</li> 
					<? } ?>
					<li class="divider"></li>
				</ul>
			</div>
		</div>
		<ul>
			<li><label>Text Fields (<a href="javascript:;" class="bfToolTip" title="Check/uncheck a text field box to turn the field on/off. You may also edit the label for any text field.">?</a>)</label></li>
			<? for ($i=0; $i<count($fieldset)-1; $i++) { ?>
			<li>
				<label class="contactFieldLabel"><?=$i+1 ?>.</label> 
				<input type="checkbox" class="bfCheckbox contactCheck bfChangeListen" name="field<?=$i?>" value="1" id="field<?=$i?>" <? if($fieldset[$i]["on"]) { ?>checked="checked" <? } ?> /> 
				<input type="text" class="bfInputText bfChangeListen" name="label<?=$i?>" value="<?=$fieldset[$i]["label"]?>" id="label<?=$i?>" />
			</li>
			<? } ?>
			<li><label>Comments Field</label></li>
			<li> 
				<label class="contactFieldLabel">7.</label>
				<input type="checkbox" class="bfCheckbox contactCheck bfChangeListen" name="field6" value="1" id="field6" <? if($fieldset[6]["on"]) { ?>checked="checked" <? } ?> /> 
				<input type="text" class="bfInputText bfChangeListen" name="label6" value="<?=$fieldset[6]["label"]?>" id="label6" />
			</li>
			<? if (!$hasAddToPage)  { ?> 
				
			<li class="divider"></li>
			
			<!-- page image uploader --> 
			<li> 
				<label for="pageImage">Page image</label>
				<div style="clear:both;"></div>
				<div id="deletePageImage" class="deleteButton" <? if (!$contactImageExists) { ?>style="display:none"<?}?>></div>
				<div id="pageImageHolder">
					<? if ($contactImageExists) { 
						$ur = $_SESSION['user']['temp_url'];
						?>
						<img src="<? echo($ur); ?>/gallery/medium/contact.jpg" border="0" id="pageImg" title="contact.jpg"/>
					<? } else { ?>
						<div class="tip_message">
						    There is currently no page image. You can upload one using the button below.
						</div>
					<? } ?>

			 	</div>

				<div style="clear:both;"></div>
				<input name="pageimagefile" type="file" id="pageimagefile" size="10" /> 
				<div id="status-message"></div> 
				<div id="page-image-queue"></div>
			</li>
			
			<li class="divider"></li> 
			
			<li>
			<label for="contact_flash">Add to main site</label>
			<div class="uiButtonSet">
				<label for="contact_flash_yes" class="radioLabel">Yes</label>
				<input type="radio" name="contact_flash" id="contact_flash_yes" value="1" class="bfChangeListen" <? if ($s['contact_form_flash'] == 1) { ?>checked="checked"<? } ?> />
				<label for="contact_flash_no" class="radioLabel">No</label>
				<input type="radio" name="contact_flash" id="contact_flash_no" value="0" class="bfChangeListen" <? if ($s['contact_form_flash'] == 0) { ?>checked="checked"<? } ?> />
			</div>
			</li> 
			                                       
			<? if ($template != 'desperado' && $templateType != 'HTML5') { // only show add to html mirror if the template isn't html...'?>
			<li>
			<label for="contact_html">Add to html mirror site</label>
			<div class="uiButtonSet">
				<label for="contact_html_yes" class="radioLabel">Yes</label>
				<input type="radio" name="contact_html" id="contact_html_yes" value="1" class="bfChangeListen" <? if ($s['contact_form_html'] == 1) { ?>checked="checked"<? } ?> />
				<label for="contact_html_no" class="radioLabel">No</label>
				<input type="radio" name="contact_html" id="contact_html_no" value="0" class="bfChangeListen" <? if ($s['contact_form_html'] == 0) { ?>checked="checked"<? } ?> />
			</div>
			</li> 
			<? } ?>
			
			<li>
			<label for="add_extra">Add the following text:</label>
			<div class="uiButtonSet">
				<label for="add_extra_yes" class="radioLabel">Yes</label>
				<input type="radio" name="add_extra" id="add_extra_yes" value="1" class="bfChangeListen" <? if ($s['add_extra'] == 1) { ?>checked="checked"<? } ?> />
				<label for="add_extra_no" class="radioLabel">No</label>
				<input type="radio" name="add_extra" id="add_extra_no" value="0" class="bfChangeListen" <? if ($s['add_extra'] == 0) { ?>checked="checked"<? } ?> />
			</div>
			</li>
			<li>
				<textarea class="bfTextarea bfChangeListen" name="contact_form_extra" id="contact_form_extra"><?=$s['contact_form_extra']?></textarea>
			</li> 
			
			<li>
				<!-- hard coded legacy settings that aren't really used anymore -->
				<input type="hidden" name="contact_form_email" value="1" id="contact_form_email" />
				<input type="hidden" name="add_phone" value="0" id="add_phone" />
				<input type="hidden" name="add_address" value="0" id="add_address" />
				<input type="hidden" name="add_email" value="0" id="add_email" />
			</li>
			
			<? } ?> 
			 
			<li class="divider"></li>
			<li>
			<label for="contact_form_sms">Send messages to my mobile phone (text message)</label>
			<div class="uiButtonSet">
				<label for="contact_form_sms_yes" class="radioLabel">Yes</label>
				<input type="radio" name="contact_form_sms" id="contact_form_sms_yes" value="1" class="bfChangeListen" <? if ($s['contact_form_sms'] == 1) { ?>checked="checked"<? } ?> />
				<label for="contact_form_sms_no" class="radioLabel">No</label>
				<input type="radio" name="contact_form_sms" id="contact_form_sms_no" value="0" class="bfChangeListen" <? if ($s['contact_form_sms'] == 0) { ?>checked="checked"<? } ?> />
			</div>
			</li>
		</ul>
	</div> 
</div>


<script type="text/javascript">
// <![CDATA[

//
// intro uploader 
$("#pageimagefile").uploadifive({
  'uploadScript'   : adminURL + '_bfadmin/file_upload.php', 
  'method'         : 'post',
  'formData'       : {'u':u,'p':p,'mode':'contact'},      
  'cancelImg'      : 'uploadifive-cancel.png',
  'multi'          : true,
  'auto'           : true,
  'fileExt'        : '*.jpg;',
  'fileDesc'       : 'JPEG Files (.JPG,)',
  'queueID'        : 'page-image-queue', 
  'buttonText'     : 'UPLOAD PAGE IMAGE',
  'width'          : 165,
  'queueSizeLimit' : 25,
  'simUploadLimit' : 1,
  'removeCompleted': true,
  'onSelectOnce'   : function(event,data) {
      	//$('#status-message').text(data.filesSelected + 'uploading...');
		$('#status-message').text('uploading...');
    }, 
  'onComplete'     : function(event, ID, fileObj, response, data) { 
	  	var pageimagehtml = "";
			pageimagehtml += "<img src='"+tempURL+"gallery/medium/contact.jpg' id='pageImg' title='contact.jpg'/>"; 
			
		$("#pageImageHolder").html(pageimagehtml);
		    
		$("#deletePageImage").show(); 
		//
		// save texture file name in db  
		//saveSection();
   	},
  'onAllComplete'  : function(event,data) {
		$.log("filesUploaded: "+data.filesUploaded);
		$.log("data.errors: "+data.errors);
		if (data.errors > 0) {
      		$('#status-message').text("There was an error with the upload.");
		} else {
			$('#status-message').text("");
		}
    }
}); 




$("#deletePageImage").click(function() {
	var save = confirm("Are you sure you want to delete this page image? This cannot be undone.");
	if (save) {
		// 
		var thumb = $("#pageImageHolder").find("img");
		thumb.fadeOut('fast', function() {
		    thumb.remove();
		}); 
		$(this).hide(); 
		// 
		//
	  	var imageFile = $("#pageImg").attr("title");
		$.ajax({
          type: 'POST',
          url:'actions/images/check-for-duplicate-images.php',
          data: ({'imageFile':imageFile})
        }).done(function( msg ) {
		  	$.log( "duplicates?: "+msg);  
			//
			// if no duplicates (0) then remove the file
			if (msg == '0') { 
				$.ajax({
		          type: 'POST',
		          url:adminURL + '_bfadmin/file_remove.php',
		          data: ({'u':u,'p':p, 'mode':'gallery', 'file':imageFile})
		        }).done(function( msg ) {
				  $.log( "msg: " + msg );
				});
			} 
		});
		//
		//saveSection();
	}
});

// ]]>
</script>