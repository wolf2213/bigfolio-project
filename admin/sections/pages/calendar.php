<?
//
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");
// 
$s = get_settings();
//
$template = get_property('template'); 
$templateType = get_property('TEMPLATE_TYPE');
//
// check whether there is add_contact_form functionality with this template 
$hasAddToPage = false;
$result = mysql_query("SHOW COLUMNS FROM pages");
if (!$result) {
    echo '<!-- CANNOT LIST PAGE FIELD NAMES: ' . mysql_error() . '-->';
} else if (mysql_num_rows($result) > 0) {
    while ($row = mysql_fetch_assoc($result)) { 
		//echo $row['Field'] . '<br>';
        if ($row['Field'] == 'add_calendar') {
	    	$hasAddToPage = true;
		}	
    }
}      

$result = mysql_list_tables($db_name) or die(mysql_error());
while ($row = mysql_fetch_row($result)) {
	if($row[0]=='calendar') {
		$deluxeCalendar = true;
	}
}
if (!$deluxeCalendar) { 
	$fvalues = unserialize($s['flash_vars']);
	$fvars = split(',',get_property('FLASH_VARS'));
	foreach($fvars as $var) {
		if ($var == 'calendar_title') {
			$calTitle = $fvalues[$var];
		} else if ($var == 'calendar_text') {
			$calText = $fvalues[$var];
		} else if ($var == 'unavailable_dates') {
			$unavailDates = $fvalues[$var];
		}
	}
}

?>
<div class="content_inner" id="contactForm">
	<div class="contentHeader">
		<h1>Calendar</h1>
	</div>  
	<div class="tip_message">
		<? if ($deluxeCalendar) { ?> 
	    Use this area to control the availability calendar form on your site. Simply click on a day to create new or change events. Changes to the calendar are saved immediately.
		<? } else { ?> 
		Enter your information into the fields and choose the dates on the calendar that you are unavailable.
		<? } ?>
	</div>
	<div id="editPane">
		<? if (!$hasAddToPage) { ?>
		<ul>
			<li>
				<label for="cal_title">Calendar Title</label>
				<input class="bfInputText bfChangeListen" name="cal_title" type="text" id="cal_title" value="<?=$calTitle?>"  />
			</li>
			<li>
				<label for="cal_text">Calendar Description</label>
				<textarea class="bfTextarea bfChangeListen" name="cal_text" cols="60" rows="3" id="cal_text"><?=$calText?></textarea>
			</li> 
			<li class="divider"></li>   
		</ul>                      
		<? } ?>
		<div id="calHolder">
		</div>
	</div> 
</div>


<script type="text/javascript">
// <![CDATA[
            
                                                   
var calDiv = "calHolder";
<? if ($deluxeCalendar) { ?>
	swfobject.embedSWF("sections/pages/calendar/cal_deluxe.swf", calDiv, "650", "480", '9.0.24', null, {}, {'quality':'high','wmode':'transparent','allowScriptAccess':'always'},{},function(event) {}); 
<? } else { ?> 
	swfobject.embedSWF("sections/pages/calendar/cal.swf", calDiv, "650", "480", '9.0.24', null, {'unavailable_dates':'<?=$unavailDates?>'}, {'quality':'high','wmode':'transparent','allowScriptAccess':'always'},{},function(event) {});
<? } ?> 

// ]]>
</script>