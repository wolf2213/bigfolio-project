<?
//
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../../inc/db.inc.php");
include_once("../../../inc/functions.inc.php");                          
//
$s = get_settings();
$fvalues = unserialize($s['flash_vars']);  
$template = get_property('template'); 
$templateType = get_property('TEMPLATE_TYPE');
//
$pid = $_GET['val'];
$page = get_page_info($pid);
$all_toppages = get_all_pages(); // top level pages for parent drop down 
//
$isRedirect = substr($page['page_content'], 0, 9) == 'REDIRECT:';
if ($isRedirect) {
	$redirectVal = substr($page['page_content'], 9); 
}
// 
?>
<div class="content_inner" id="page">
	<div class="contentHeader">
		<h1>Page</h1>
	</div>  
	<div class="tip_message">
	    Use this area to administer content to your page.
	</div>
	<div class="pageSettings">
		<ul>
			<input type="hidden" name="page_id" value="<?=$page['page_id']?>" />
			<li>
				<label for="pageTitle">Page title</label>
				<input type="text" class="bfInputText bfChangeListen" name="page_title" value="<?=$page['page_title']?>" />
			</li>
			<li>
				<label for="parent_id">Page parent</label>
				<select name="parent_id" class="smallSelect bfChangeListen" value="<?=$page['parent_id']?>">
					<option value="0">None (top level)</option>
					<? foreach ($all_toppages as $p) { ?>
						<? if ($p['page_id'] != $page['page_id']) { //page can't be a child of itself! '?>
						<option value="<?=$p['page_id']?>" <? if ($page['parent_id'] == $p['page_id']) { ?> selected=selected <? } ?>><?=$p['page_title'];?></option> 
						<? } ?>
					<? } ?>
				</select>
			</li>
			
			<div id="seoSettings" <?if(!isset($page['meta_title'])){?>style="display:none;"<?}?>>
				<li class="divider"></li>
				<div id="metaSettings"> 
					<ul>
					<? $page = get_page_info($pid); ?>
					<? while (list($key, $val) = each($page)) { ?>
						<? if (stristr($key, "meta_")) { ?>
						<li class="metaSetting">
							<label for="<?=$key?>"><?=ucfirst(str_replace("_", " ", $key))?></label>                                                             
							<input type="text" class="bfInputText bfChangeListen" name="<?=$key?>" value="<?=$val?>" />
						</li> 
						<? } ?>
					<? } ?> 
					</ul>
				</div>
			</div>
			
			<li class="divider"></li>
			
		 	<!-- page type -->
			<li>
				<label for="pageType">Page type</label>                    
				<select name="pageType" id="pageType" class="smallSelect bfChangeListen">
					<option value="text" <? if (!$isRedirect) { ?>selected=selected <? } ?>>Text</option>
					<option value="redirect" <? if ($isRedirect) { ?>selected=selected <? } ?>>Redirect</option>
				</select>
			</li>
			
			<!-- page text editor -->
			<li id="bfTextEditor" style="<? if ($isRedirect) {?>display:none;<? } ?>">
				<label for="pageContent">Page text</label> 
				<div id="bfTextEditorControls"> 
					<a href="javascript:;" class="bfToolTip" title="Click this to apply link html to selected text within the editor. You will be prompted to enter a URL and target." id="linkInsert">&lt; link /&gt;</a>
					<a href="javascript:;" class="bfToolTip" title="Click this to apply bold formatting to selected text within the editor." id="boldInsert">&lt; bold /&gt;</a>
					<a href="javascript:;" class="bfToolTip" title="Click this to apply italic formatting to selected text within the editor." id="italicInsert">&lt; italic /&gt;</a>
					<a href="javascript:;" class="bfToolTip" title="Click this to apply image html to selected text within the editor. You will be prompted to enter the image URL. If you want to link to an image in the image bank enter gallery/original/ followed by the image filename." id="imageInsert">&lt; image /&gt;</a>
					<? 
					if (get_property("propx_api_key") != "") { 
						$products = file_get_contents("http://propx.co/api/products?api_key=".get_property("propx_api_key"));
						$pjson = json_decode($products);
						if (count($pjson->products) > 0) {
					?>
					<a href="javascript:;" class="bfToolTip" title="Click this to select a product from your products listed under the ecommerce section. The product shortcode will be added to the page text, and the product will be displayed where the shortcode is added on the page text.">
						<select id="productInsert">
							<option>&lt; product /&gt;</option>
							<? for($i=0; $i<count($pjson->products); $i++) { ?>
							<option value="<?=$pjson->products[$i]->id;?>:<?=$pjson->products[$i]->name;?>"><?=$pjson->products[$i]->name;?> - $<?=$pjson->products[$i]->price;?>0</option>
							<? } ?>
						</select>
					</a>
					<? 
						} 
					} 
					?>
				</div>
				<textarea name="page_content" id="pageContent" class="bfPageText bfChangeListen bfMarkupText"><? if (!$isRedirect) {  echo editorCharReplacements($page['page_content']); } ?></textarea>
				
				<div style="clear:both;"></div>
				<? if (!stristr($page['page_content'], "<script ")) { ?>
				<a rel="markupPreview" id="markupPreview" href="#markupPreviewViewer" class="bfToolTip" title="Click here to view an approximate preview of the page text.">Preview HTML</a> 
				<? include_once('../../../inc/fonts-include.php'); ?>
				<style type="text/css"> 
				<!--
				/* tempalte specific page text/link/bg styles */
				<? if ($template == 'desperado') { ?>
					#pageMarkup { background: <?=$s['app_color'];?>; color:<?=$s['text_color']?>;}
					#pageMarkupSWF { background: <?=$s['app_color'];?>; color:<?=$s['text_color']?>;}
					#pageMarkup a { color: <?=$s['custom_color_1'];?> }
					#pageMarkup a:hover { color: <?=$s['custom_color_2'];?> } 
					#pageMarkup .hoverColor { color: <?=$s['custom_color_2'];?> }
				<? } else if ($template == 'aurora' || $template == 'lapine'){ ?>
					#pageMarkup { background: <?=$s['app_color'];?> color:<?=$s['custom_color_3']?>;}
					#pageMarkupSWF { background: <?=$s['app_color'];?> color:<?=$s['custom_color_3']?>;}
					#pageMarkup a { color: <?=$s['custom_color_3'];?>; text-deocration: underline; }
					#pageMarkup a:hover { color: <?=$s['custom_color_3'];?> text-deocration: underline; }
					#pageMarkup .hoverColor { color: <?=$s['custom_color_3'];?> }
				<? } else { ?> 
					#pageMarkupSWF { background: <?=$s['page_color']?>; color:<?=$s['text_color']?>; }
					#pageMarkup { background: <?=$s['page_color']?>; color:<?=$s['text_color']?>; }
					#pageMarkup a { color: <?=$s['custom_color_1'];?> }
					#pageMarkup a:hover { color: <?=$s['custom_color_2'];?> }
					#pageMarkup .hoverColor { color: <?=$s['custom_color_2'];?> }
				<? } ?> 
				
				<? if (isset($page['page_color']) && $page['page_color'] != "") { ?>
					#pageMarkup { background: <?=$page['page_color'];?>; }
				<? } ?>
				
					#pageMarkup {font-family: "<?=$s['page_font'];?>"; font-size: <?=14+$fvalues['page_font_modifier']?>px;}
				 
				-->
				</style>
				<div style="display:none;">
				<div id="markupPreviewViewer"> 
					<div id="previewDesc">
						This is an approximate preview of the page text. Page text on your actual site may have subtle differences.
					</div>
					<div id="pageMarkupSWF"></div> 	
					<div id="pageMarkup">
					<div class="inner">
						<?
						$pagePreview = str_replace("<!-- noformat on -->", "<!-- noformat on", $page['page_content']);
						$pagePreview = str_replace("<!-- noformat off -->", "noformat off -->", $pagePreview);
						echo charReplacements($pagePreview);
						?> 
					</div>
					<div class="hoverColor">
					</div> 
				</div>
				</div>
				<? } ?>
			</li> 
			
			<li id="bfRedirect" style="<? if (!$isRedirect) {?>display:none;<? } ?>">
				<label for="pageRedirect">Redirect URL (<a href="javascript:;" class="bfToolTip" title="Enter the URL of the page you want to link to (e.g. http://mydomain.com/blog/).">?</a>)</label>
				<input type="text" class="bfInputText bfChangeListen" name="pageRedirect" id="pageRedirect" value="<?=$redirectVal?>" />
			</li>  
			
			<li class="divider"></li> 
			
			
			<!-- page image uploader -->
			<div id="normalPageOptions" style="<? if ($isRedirect) {?>display:none;<? } ?>"> 
			<li> 
				<label for="pageImage">Page image</label>
				<div style="clear:both;"></div>
				<div id="deletePageImage" class="deleteButton" <? if ($page['page_image'] == "" || $page['page_image'] == "NONE") { ?>style="display:none"<?}?>></div>
				<div id="pageImageHolder">
					<? if ($page['page_image'] != '' && $page['page_image'] != 'NONE') { 
						$ur = $_SESSION['user']['temp_url'];
						?>
						<img src="<? echo($ur); ?>/gallery/medium/<?= $page['page_image'] ?>" border="0" id="pageImg" title="<?= $page['page_image'] ?>"/>
					<? } else { ?>
						<div class="tip_message">
						    There is currently no page image. You can upload one using the button below.
						</div>
					<? } ?>
					 
			 	</div>
			     
				<div style="clear:both;"></div>
			    <input type="hidden" name="page_image" id="pageImage" class="bfChangeListen" value="<?=$page['page_image']?>" />
				<input name="pageimagefile" type="file" id="pageimagefile" size="10" /> 
				<div id="status-message">
					
				</div> 
				<div id="page-image-queue">
				</div>
			</li>
			<li class="divider"></li>
			
			<? if (isset($page['page_color'])) { ?> 
			<li>
				<label>Page background color</label>
				<input type='hidden' name='page_color' id='pageColor' value='<?=$page['page_color']?>' class='bfChangeListen' />
				<div class='bfSwatch' style='background-color:<?=$page['page_color']?>'></div>
			</li>
			<? } ?> 
			
			<? if (isset($page['page_opacity'])) { ?>
			<li>
				<label>Page background opacity</label>
				<div class='bfSlider' title='0,100,<?=$page['page_opacity']?>'></div>
				<input type='text' class='bfInputText bfSliderLabel bfChangeListen' name='page_opacity' id='pageOpacity' value='<?=$page['page_opacity']?>' />
			</li>
			<? } ?>
			
			<? if (isset($page['page_texture'])) { ?> 
			<!-- page texture uploader --> 
			<li>
				<label for="pageImage">Page texture</label>
				<div style="clear:both;"></div>
				<div id="deletePageTexture" class="deleteButton" <? if ($page['page_texture'] == "" || $page['page_texture'] == "NONE") { ?>style="display:none"<?}?>></div>
				<div id="pageTextureHolder">
					<? if ($page['page_texture'] != '' && $page['page_texture'] != 'NONE') { 
						$ur = $_SESSION['user']['temp_url'];
						?>
						<img src="<? echo($ur); ?>/textures/texture_thumbnails/<?= $page['page_texture'] ?>" border="0" id="pageTxtr" title="<?= $page['page_texture'] ?>"/>
					<? } else { ?>
						<div class="tip_message">
						    There is currently no page texture. You can upload one using the button below.
						</div>
					<? } ?>
					 
			 	</div>
			     
				<div style="clear:both;"></div>
			    <input type="hidden" name="page_texture" id="pageTexture" class="bfChangeListen" value="<?=$page['page_texture']?>" />
				<input name="pagetexturefile" type="file" id="pagetexturefile" size="10" /> 
				<div id="texture-status-message">
					
				</div> 
				<div id="page-texture-queue">
				</div>
			</li>
			<? } ?>
			
			<? if (isset($page['page_text_align'])) { ?>
			<li>
				<label>Page text alignment (<a href="javascript:;" class="bfToolTip" title="The page image, if present, will sit opposite the page text alignment setting. If middle is selected, no page image will show.">?</a>)</label>
				<select name="page_text_align" id="pageTextAlign" class="smallSelect bfChangeListen">
					<option value="left" <?if($page['page_text_align'] == '' || $page['page_text_align'] == 'left'){?>selected="selected"<?}?>>Left</option>
					<? if ($template != "rubix") { ?>
					<option value="middle" <?if($page['page_text_align'] == 'middle'){?>selected="selected"<?}?>>Middle</option>
					<? } ?>
					<option value="right" <?if($page['page_text_align'] == 'right'){?>selected="selected"<?}?>>Right</option>
				</select>
			</li>
			<li class="divider"></li>
			<? } ?> 
			
			
			
			
			
			<? $page = get_page_info($pid); ?>
			<? while (list($key, $val) = each($page)) { ?>
				<? if (stristr($key, "show_") || stristr($key, "add_")) { ?>
				<li class="otherSetting">
					<label for="<?=$key?>"><?=ucfirst(str_replace("_", " ", $key))?></label>                                                             
					<div class="uiButtonSet">
						<label for="<?=$key?>Yes" class="radioLabel">Yes</label>
						<input name="<?=$key?>" id="<?=$key?>Yes" type="radio" value="1" class="bfChangeListen" <? if ($val==1) { ?>checked="checked"<? } ?> />
						<label for="<?=$key?>No" class="radioLabel">No</label>
						<input name="<?=$key?>" id="<?=$key?>No" type="radio" value="0" class="bfChangeListen" <? if ($val==0) { ?>checked="checked"<? } ?> />
					</div>
				</li> 
				<? } ?>
			<? } ?>
			
			</div> <!-- end #normalPageOptions -->
			
			<li class="otherSetting">
				<? 
				$key = 'page_hidden';
				$val = $page[$key];
				?>
				<label for="pageHidden">Hide this page</label>                                                             
				<div class="uiButtonSet">
					<label for="<?=$key?>Yes" class="radioLabel">Yes</label>
					<input name="<?=$key?>" id="<?=$key?>Yes" type="radio" value="1" class="bfChangeListen" <? if ($val==1) { ?>checked="checked"<? } ?> />
					<label for="<?=$key?>No" class="radioLabel">No</label>
					<input name="<?=$key?>" id="<?=$key?>No" type="radio" value="0" class="bfChangeListen" <? if ($val==0) { ?>checked="checked"<? } ?> />
				</div>
			</li>	
			
			<? if ($template != "rubix") { ?>
			
			<li class="otherSetting">
				<? 
				$key = 'include_html';
				$val = $page[$key];
				?>
				<label for="<?=$key?>">Show page in HTML/mobile version</label>                                                             
				<div class="uiButtonSet">
					<label for="<?=$key?>Yes" class="radioLabel">Yes</label>
					<input name="<?=$key?>" id="<?=$key?>Yes" type="radio" value="1" class="bfChangeListen" <? if ($val==1) { ?>checked="checked"<? } ?> />
					<label for="<?=$key?>No" class="radioLabel">No</label>
					<input name="<?=$key?>" id="<?=$key?>No" type="radio" value="0" class="bfChangeListen" <? if ($val==0) { ?>checked="checked"<? } ?> />
				</div>
			</li>
			
			<li class="otherSetting">
				<? 
				$key = 'alt_html';
				$val = $page[$key];
				?>
				<label for="<?=$key?>">Use alternate HTML/mobile content</label>                                                             
				<div class="uiButtonSet">
					<label for="<?=$key?>Yes" class="radioLabel">Yes</label>
					<input name="<?=$key?>" id="<?=$key?>Yes" type="radio" value="1" class="bfChangeListen" <? if ($val==1) { ?>checked="checked"<? } ?> />
					<label for="<?=$key?>No" class="radioLabel">No</label>
					<input name="<?=$key?>" id="<?=$key?>No" type="radio" value="0" class="bfChangeListen" <? if ($val==0) { ?>checked="checked"<? } ?> />
				</div>
			</li>
			<div style="clear:both;"></div> 
			<div id="alt_page_text" <? if($val==0) { ?> style="display:none;" <? } ?>>
			<li>
				<label for="pageTitle">Alternate title</label>
				<input type="text" class="bfInputText bfChangeListen" name="alt_title" value="<?=$page['alt_title']?>" />
			</li>
			<li>
				<label for="alt_content">Alternate page text</label>
				<textarea name="alt_content" id="altPageContent" class="bfPageText bfChangeListen bfMarkupText"><?  echo editorCharReplacements($page['alt_content']); ?></textarea>
			</li> 
			
			</div>
			
			<? } else { ?>
				<input name="include_html" value="1" type="hidden">
			<? } ?>
			
		</ul>
	</div> 
</div>

<script type="text/javascript">
// <![CDATA[

if ($("#metaSettings").find('.metaSetting').length > 0) {
	$("#seoSettings").show();
}

// page image upload 
$("#pageimagefile").uploadify({
  'uploader'       : 'js/plugins/uploadify-v2.1.4/uploadifyCS3.swf',
  'script'         : adminURL + '_bfadmin/file_upload.php', 
  'scriptData'     : {'u':u,'p':p,'mode':'gallery'},      
  'method'         : 'post',
  'cancelImg'      : 'js/plugins/uploadify-v2.1.4/cancel.png',
  'multi'          : true,
  'auto'           : true,
  'fileExt'        : '*.jpg;*.jpeg;*.png;',
  'fileDesc'       : 'Image Files (.JPG, .JPEG, .PNG)',
  'queueID'        : 'page-image-queue', 
  'buttonText'     : 'UPLOAD PAGE IMAGE',
  'width'          : 165,
  'queueSizeLimit' : 1,
  'simUploadLimit' : 1,
  'removeCompleted': true,
  'onSelectOnce'   : function(event,data) {
      	//$('#status-message').text(data.filesSelected + 'uploading...');
		$('#status-message').text('uploading...');
    }, 
  'onComplete'     : function(event, ID, fileObj, response, data) { 
		var fileParts = getNameAndExtension(fileObj['name']); 
		var pageImageFile = cleanfile(fileParts[0])+'.'+fileParts[1];
	  	$.log('fileName: '+cleanfile(fileParts[0])+'.'+fileParts[1]);
	  	$.log('response: '+response); 
	  	var pageimagehtml = "";
			pageimagehtml += "<img src='"+tempURL+"gallery/small/"+pageImageFile+"' id='pageImg' title='"+pageImageFile+"'/>"; 
			
		$("#pageImageHolder").html(pageimagehtml);
		    
		$("#pageImage").val(pageImageFile);
		$("#deletePageImage").show(); 
		//
		// save texture file name in db  
		//saveSection();
   	},
  'onAllComplete'  : function(event,data) {
		$.log("filesUploaded: "+data.filesUploaded);
		$.log("data.errors: "+data.errors);
		if (data.errors > 0) {
      		$('#status-message').text("There was an error with the upload.");
		} else {
			$('#status-message').text("");
		}
    }
});

$("#deletePageImage").click(function() {
	var save = confirm("Are you sure you want to delete this page image? This cannot be undone.");
	if (save) {
		// 
		$("#pageImage").val("");
		var thumb = $("#pageImageHolder").find("img");
		thumb.fadeOut('fast', function() {
		    thumb.remove();
		}); 
		$(this).hide(); 
		// 
		//
	  	var imageFile = $("#pageImg").attr("title");
		$.ajax({
          type: 'POST',
          url:'actions/images/check-for-duplicate-images.php',
          data: ({'imageFile':imageFile})
        }).done(function( msg ) {
		  	$.log( "duplicates?: "+msg);  
			//
			// if no duplicates (0) then remove the file
			if (msg == '0') { 
				$.ajax({
		          type: 'POST',
		          url:adminURL + '_bfadmin/file_remove.php',
		          data: ({'u':u,'p':p, 'mode':'gallery', 'file':imageFile})
		        }).done(function( msg ) {
				  $.log( "msg: " + msg );
				});
			} 
		});
		//
		//saveSection();
	}
});  


$("#pagetexturefile").uploadify({
  'uploader'       : 'js/plugins/uploadify-v2.1.4/uploadifyCS3.swf',
  'script'         : adminURL + '_bfadmin/file_upload.php', 
  'scriptData'     : {'u':u,'p':p,'mode':'pagetexture'},      
  'method'         : 'post',
  'cancelImg'      : 'js/plugins/uploadify-v2.1.4/cancel.png',
  'multi'          : true,
  'auto'           : true,
  'fileExt'        : '*.jpg;*.jpeg;*.png;',
  'fileDesc'       : 'Image Files (.JPG, .JPEG, .PNG)',
  'queueID'        : 'page-texture-queue', 
  'buttonText'     : 'UPLOAD PAGE TEXTURE',
  'width'          : 182,
  'queueSizeLimit' : 1,
  'simUploadLimit' : 1,
  'removeCompleted': true,
  'onSelectOnce'   : function(event,data) {
      	//$('#status-message').text(data.filesSelected + 'uploading...');
		$('#texture-status-message').text('uploading...');
    }, 
  'onComplete'     : function(event, ID, fileObj, response, data) { 
		var fileParts = getNameAndExtension(fileObj['name']); 
		var pageTextureFile = cleanfile(fileParts[0])+'.'+fileParts[1];
	  	$.log('fileName: '+cleanfile(fileParts[0])+'.'+fileParts[1]);
	  	$.log('response: '+response); 
	  	var pagetexturehtml = "";
			pagetexturehtml += "<img src='"+tempURL+"textures/texture_thumbnails/"+pageTextureFile+"' id='pageTxtr' title='"+pageTextureFile+"'/>"; 
			
		$("#pageTextureHolder").html(pagetexturehtml);
		    
		$("#pageTexture").val(pageTextureFile);
		$("#deletePageTexture").show(); 
		//
		// save texture file name in db  
		//saveSection();
   	},
  'onAllComplete'  : function(event,data) {
		$.log("filesUploaded: "+data.filesUploaded);
		$.log("data.errors: "+data.errors);
		if (data.errors > 0) {
      		$('#texture-status-message').text("There was an error with the upload.");
		} else {
			$('#texture-status-message').text("");
		}
    }
});

$("#deletePageTexture").click(function() {
	var save = confirm("Are you sure you want to delete this page texture? This cannot be undone.");
	if (save) {
		// 
		$("#pageTexture").val("");
		var thumb = $("#pageTextureHolder").find("img");
		thumb.fadeOut('fast', function() {
		    thumb.remove();
		}); 
		$(this).hide(); 
		// 
		//
	  	var imageFile = $("#pageTxtr").attr("title");
		$.ajax({
          type: 'POST',
          url:'actions/pages/check-for-duplicate-textures.php',
          data: ({'imageFile':imageFile})
        }).done(function( msg ) {
		  	$.log( "duplicates?: "+msg);  
			//
			// if no duplicates (0) then remove the file
			if (msg == '0') { 
				$.ajax({
		          type: 'POST',
		          url:adminURL + '_bfadmin/file_remove.php',
		          data: ({'u':u,'p':p, 'mode':'pagetexture', 'file':imageFile})
		        }).done(function( msg ) {
				  $.log( "msg: " + msg );
				});
			} 
		});
		//
		//saveSection();
	}
});



$("#pageType").change(function() {
	if ($(this).val() == 'text') {
		//
		$("#bfTextEditor").slideDown('fast');
		$("#normalPageOptions").slideDown('fast');
		$("#bfRedirect").slideUp('fast');  
		//
	} else if ($(this).val() == 'redirect') {
		//
		$("#bfTextEditor").slideUp('fast');
		$("#normalPageOptions").slideUp('fast');
		$("#bfRedirect").slideDown('fast');
		//
	}
});

/* BEGIN TEXT EDITOR */

$('#pageContent').keyup(update).mousedown(update).mousemove(update).mouseup(update);  //.imgCap
var curSelection;
function update(e) {
	// here we fetch our text range object
	curSelection = $(this).getSelection();//.text;
}                                                 
//
// link insert
$("#linkInsert").click(function(e) { 
	
	if (curSelection.start == curSelection.end) {
		alert('Please select some text first.');
		return;
	}
	
	var url = prompt("Enter the URL to link to:"); 
	if (url == "") {
		return;
	}
	var target = prompt("Enter the target window for the link. To open in a new window, enter _blank. To open in the same window, leave this blank.");
	
	var allText      = $("#pageContent").val();
	var selText      = $('#pageContent').getSelection();
	var replaceText  = ""; 
		replaceText += allText.substr(0, selText.start);
		replaceText += '<a href="'+url+'"';
		if (target != "") {
			replaceText += ' target="'+target+'"';
		}
		replaceText += '>'+selText.text+'</a>';
		replaceText += allText.substr(selText.end, allText.length); 
		
	$("#pageContent").val(replaceText);

});
//
// bold insert
$("#boldInsert").click(function(e) { 
	
	if (curSelection.start == curSelection.end) {
		alert('Please select some text first.');
		return;
	}
	
	var allText      = $("#pageContent").val();
	var selText      = $('#pageContent').getSelection();
	var replaceText  = ""; 
		replaceText += allText.substr(0, selText.start);
		replaceText += '<b>'+selText.text+'</b>';
		replaceText += allText.substr(selText.end, allText.length); 
		
	$("#pageContent").val(replaceText);

});
//
// italic insert
$("#italicInsert").click(function(e) { 
	
	if (curSelection.start == curSelection.end) {
		alert('Please select some text first.');
		return;
	}
	
	var allText      = $("#pageContent").val();
	var selText      = $('#pageContent').getSelection();
	var replaceText  = ""; 
		replaceText += allText.substr(0, selText.start);
		replaceText += '<i>'+selText.text+'</i>';
		replaceText += allText.substr(selText.end, allText.length); 
		
	$("#pageContent").val(replaceText);

});
//
// image insert
$("#imageInsert").click(function(e) {
	var imgURL = prompt("Enter the URL of the image:"); 
	if (imgURL == "") {
		return;
	}
	
	var allText      = $("#pageContent").val();
	var selText      = $('#pageContent').getSelection();
	var replaceText  = ""; 
		replaceText += allText.substr(0, selText.end);
		replaceText += '<img src="'+imgURL+'" alt="" />';
		replaceText += allText.substr(selText.end, allText.length); 
		
	$("#pageContent").val(replaceText);

});

$("#productInsert").change(function(e) {
	
	if ($(this).val() == "< product />") {
		return;
	}

	var allText      = $("#pageContent").val();
	var selText      = $('#pageContent').getSelection();
	var replaceText  = ""; 
		replaceText += allText.substr(0, selText.end);
		replaceText += '<!--rubixCartProduct:'+$(this).val()+'-->';
		replaceText += allText.substr(selText.end, allText.length); 
		
	$("#pageContent").val(replaceText);
	
	$(this).val("< product />");

});


//setInterval(updateMarkupPreview, 50);
$("#pageContent").change(function() {
	//updateMarkupPreview();
}); 

$("#markupPreview").click(function() {
	updateMarkupPreview();
	 
});
                       
function updateMarkupPreview() {
	var text = $("#pageContent").val().split('gallery/original/').join(tempURL+'gallery/original/'); // handle images referenced to the image bank
	if (text.indexOf("<script src") > -1) { return; }
		//text = text.split('<b>').join('<strong>');
		//text = text.split('</b>').join('</strong>');
	text = text.split("<!-- noformat on -->").join("<!-- noformat on ");
	text = text.split("<!-- noformat off -->").join("noformat off -->");
	$("#pageMarkup .inner").html(charReplacements(text)); 
	if (templateType != "HTML5" && template != "desperado") {
		$("#pageMarkup").css("overflow","hidden");
		swfobject.embedSWF("sections/pages/pageTextPreview.swf", "pageMarkupSWF", "100%", "100%", '9.0.24', null, {}, {'quality':'high','wmode':'transparent','allowScriptAccess':'always'},{},function(event) {$.log('testing 123...');});
	} else {
		$("#pageMarkupSWF").remove();
	}
}
updateMarkupPreview();

function getPreviewText() {
	return $("#pageMarkup .inner").html(); 
} 
function getPreviewCSS(val) { // val comes in as a:hover,color or just fontSize
	if (val.indexOf(',') > -1) {
		var vals = val.split(',');
		var adSel = vals[0];
		var prop = vals[1];
	} else { 
		var adSel = "";
		var prop = val;    
	}
	$.log("#pageMarkup "+adSel); 
	var returnVal = $("#pageMarkup "+adSel).css(prop);
	if (prop == 'color') {
		returnVal = rgb2hex(returnVal);   
	}                  
	return returnVal.split("'").join("");
}
function setPreviewHeight(val) {
	$("#pageMarkupSWF").height(val);
} 

/* END TEXT EDITOR */ 


// Alt content toggle
$("#alt_htmlYes").click(function() {
   $("#alt_page_text").slideDown('fast'); 
});
$("#alt_htmlNo").click(function() {
   $("#alt_page_text").slideUp('fast'); 
});

// ]]>
</script>
<!-- <script src="js/plugins/bfTextEditor.js"></script> -->
