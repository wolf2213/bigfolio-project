<?
//
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");
// 
$all_toppages = get_all_pages();
$all_subpages = get_all_sub_pages();
$loop = 0; 
?>
<div class="content_inner" id="pages">
	<div class="contentHeader">
		<h1>Pages -</h1>  <a href="https://vimeo.com/127842635" target="_blank"><img src="img/watch-this-video2.png" border="0" style="position:absolute; margin-left:115px; margin-top:-50px;" /></a>
	</div>  
	<div class="tip_message">
	    Use this area to create pages and subpages. Click the edit link next to each gallery to upload images to that gallery or adjust other settings for that gallery.
	</div> 
	<div id="toppages"> 
		
		<? foreach($all_toppages as $p) { ?>
		<? $loop++; ?>	
		<div class="page" id="page<?=$p['page_id']?>">
			<div class="deleteButton"></div> 
		    <input type="text" class="bfInputText pageName bfChangeListen" name="page-<?=$p['page_id']?>" value="<?=$p['page_title']?>">
			<div class="editButton">
				<a href="#/pages/pages/<?=$p['page_id']?>" class="editLink">
					edit
				</a>
			</div>
			<div class="plusMinusButton minus"></div>
			
			<div style="clear:both;"></div> 
			
			<div class="subpages"> 
			<? foreach($all_subpages as $sp) { ?> 
				<? if ($sp['parent_id'] == $p['page_id']) { ?>
				<div class="subpage" id="page<?=$sp['page_id']?>"> 
					<div class="deleteButton"></div>
					<input type="text" class="bfInputText subpageName bfChangeListen" name="page-<?=$sp['page_id']?>" value="<?=$sp['page_title']?>">
					<div class="editButton">
						<a href="#/pages/pages/<?=$sp['page_id']?>" class="editLink">
							edit
						</a>
					</div> 
					<div style="clear:both;"></div>
				</div>
				<? } ?>
			<? } ?>	
			</div> 
			 
			<button class="newSubpage">New Subpage</button>
			<div style="clear:both;"></div>
		</div>
		 
		<? } ?>
		
	</div> 
	<button id="newPage">New Page</button>
</div>


<script type="text/javascript">
// <![CDATA[

var numPages = <?=$loop?>;

$("#toppages").sortable({
	update: function(event, ui) {  
		var d = new Object();
		d['pageList'] = new Array();
		$(this).find(".page").each(function() {
			d['pageList'].push($(this).attr("id").substr(4)); // pushes pageNNN minus the page
		}); 
		$.log("d['pageList']: "+d['pageList']);
		
		$.ajax({
  			type: 'POST',
  			url:'actions/pages/update-pages-order.php',
  			data: d
		}).done(function( msg ) {
			$.log("page order success: "+msg);
		});
	}
});

$(".subpages").sortable({ 
    update: function(event, ui) { 
		var d = new Object(); 
		d['pageList'] = new Array();
		$(this).find(".subpage").each(function() {
			d['pageList'].push($(this).attr("id").substr(4)); // pushes pageNNN minus the page
		});
		$.log("d['pageList']: "+d['pageList']);
		
		$.ajax({
  			type: 'POST',
  			url:'actions/pages/update-pages-order.php',
  			data: d
		}).done(function( msg ) {
			$.log("subpage order success: "+msg);
		}); 
	}
}); 

function pagesFunctionality() {
	
	//
	// rollovers
	$(".page").unbind("mouseover");
	$(".page").mouseover(function() {
	   	$(this).addClass("itemOver");
	});
	$(".page").unbind("mouseout");
	$(".page").mouseout(function() {
	   	$(this).removeClass("itemOver");
	});
	$(".subpage").unbind("mouseover");
	$(".subpage").mouseover(function() {
	   	$(this).addClass("itemOver"); 
	});  
	$(".subpage").unbind("mouseout");
	$(".subpage").mouseout(function() {
	   	$(this).removeClass("itemOver");
	}); 
   
	
	//
	// delete page button
    $(".page").find(".deleteButton").unbind("click");
    $(".page").find(".deleteButton").click(function() {
		if ($(this).parent().find(".subpage").length > 0) {
			alert("Please remove all subpages associated with this page first.");
		} else { 
			var save = confirm("Are you sure you want to delete this page? This cannot be undone.");
			if (save) {
				var pageID = $(this).parent().attr('id').substr(4);
				var d = new Object();
				d['pageID'] = pageID;
				var pageDiv = $(this).parent();
				//
				$.ajax({
					type: 'POST',
					url: 'actions/pages/delete-page.php',
					data: d
				}).done(function( msg ) {
					$.log("msg: "+msg);
					pageDiv.fadeOut('fast', function() {
						pageDiv.remove();
					});
				});
			}
		}
	});
	//
	// delete gallery button
	$(".subpages").find(".deleteButton").unbind("click");
	$(".subpages").find(".deleteButton").click(function() { 
		//
		// check if they really want to delete it
		var save = confirm("Are you sure you want to delete this page? This cannot be undone.");
		if (save) {
	    	var pageID = $(this).parent().attr('id').substr(4);
			var d = new Object();
			d['pageID'] = pageID;
			var subpageDiv = $(this).parent();
			//
			$.ajax({
				type: 'POST',
				url:'actions/pages/delete-page.php',
				data: d
			}).done(function( msg ) {  
				$.log("msg: "+msg); 
				subpageDiv.fadeOut('fast', function() {
					subpageDiv.remove();
				});
			}); 
		}
	}); 
	
	//
	// show/hide category button (+/-) 
	$(".page").find(".plusMinusButton").unbind("click");
	$(".page").find(".plusMinusButton").click(function() {
	   	if ($(this).hasClass("minus")) {
		    $(this).removeClass("minus");
			$(this).addClass("plus");
			$(this).parent().find(".subpages").slideUp("fast");
		} else {
			$(this).addClass("minus");
			$(this).removeClass("plus");
			$(this).parent().find(".subpages").slideDown("fast");
		} 
	});
	
	//
	// new gallery button
	$(".newSubpage").unbind("click");
	$(".newSubpage").click(function() { 
		var pageDiv = $(this).parent().find(".subpages");
		//
		// check if the div is closed 
		if (!$(this).parent().find(".plusMinusButton").hasClass("minus")) {
			 $(this).parent().find(".plusMinusButton").click();
		}
		var parent_id = $(this).parent().attr('id').substr(4); 
		var order = $(this).parent().find(".subpage").length;
		$.log('parent_id: '+parent_id);
		$.ajax({
	       	type: 'POST',
	       	url:'actions/pages/add-page.php',
	       	data: ({'parent_id':parent_id, 'order':order})
	    }).done(function( msg ) {  
			$.log("msg: "+msg);
			//
			// returns SUCCESS:[gallery_ID]
		   	if (msg.substr(0,7) == "SUCCESS") {
				//
				var pageID = msg.substr(8); 
				//
				var pagehtml = "";
					pagehtml += '<div class="subpage" id="page'+pageID+'">'; 
					pagehtml += '<div class="deleteButton"></div>';
					pagehtml += '<input type="text" class="bfInputText galleryName" name="page-'+pageID+'" value="Enter subpage name">';
					pagehtml += '<div class="editButton">';
					pagehtml += '<a href="#/pages/pages/'+pageID+'" class="editLink">';
					pagehtml += 'edit';
					pagehtml += '</a>';
					pagehtml += '</div>'; 
					pagehtml += '<div style="clear:both;"></div>';
					pagehtml += '</div>';
				//
				pageDiv.append(pagehtml);
				//
				handleUI();  
				pagesFunctionality();
				//
				numCats++;
			} else if (msg.substr(0,5) == "QUOTA") {
				var quota = msg.substr(6); // msg returns QUOTA:N where N is the number allowed
				alert('The number of pages allowed for this site ('+quota+') has been met. To create a new page, an existing page must be removed first.');
		   	} else {
			   	alert('There was an error creating a new subpage. Please try again later. If problems persist, please contact support.');
			}
		});
	});
}
pagesFunctionality();

$("#newPage").click(function() {
	$.ajax({
       	type: 'POST',
       	url:'actions/pages/add-page.php',
       	data: ({'order':numPages})
    }).done(function( msg ) {
	   	if (msg.substr(0,7) == "SUCCESS") { 
			//
			var pageID = msg.substr(8);
			//
			var pagehtml = "";
				pagehtml += '<div class="page" id="page'+pageID+'">'; 
				pagehtml += '<div class="deleteButton"></div>';
				pagehtml += '<input type="text" class="bfInputText categoryName" name="page-'+pageID+'" value="Enter page name">';
                pagehtml += '<div class="editButton">';
				pagehtml += '<a href="#/pages/pages/'+pageID+'" class="editLink">';
				pagehtml += 'edit';
				pagehtml += '</a>';
				pagehtml += '</div>';
				
				pagehtml += '<div class="plusMinusButton minus"></div>';

				pagehtml += '<div style="clear:both;"></div>';
				pagehtml += '<div class="subpages">'; 
				pagehtml += '</div>'; 
				pagehtml += '<button class="newSubpage">New Subpage</button>';
				pagehtml += '<div style="clear:both;"></div>';
				pagehtml += '</div>';
			//
			$("#toppages").append(pagehtml);
			//
			handleUI(); 
			pagesFunctionality();
			//
			numPages++;
	   	} else if (msg.substr(0,5) == "QUOTA") {
				var quota = msg.substr(6); // msg returns QUOTA:N where N is the number allowed
				alert('The number of pages allowed for this site ('+quota+') has been met. To create a new page, an existing page must be removed first.');
		} else {
		   	alert('There was an error creating a new page. Please try again later. If problems persist, please contact support.');
		}
	});
});


// ]]>
</script>