<?
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");
//
$s = get_settings();
$fvalues = unserialize($s['flash_vars']);
$fvars = split(',',get_property('FLASH_VARS'));
//$fvars[] = "custom_color";
//$fvars[] = "custom_colors";

/*
VARS:


YORK
texture_file_name,content_width,content_height,app_alpha_0_to_100,intro_image_padding,gallery_image_padding,show_main_border,show_intro_image_border,show_gallery_image_border,show_page_image_border,menu_type,thumbnail_type,show_title_reflections,portfolio_menu_label,contact_menu_label,slideshow_speed,auto_start_slide_show,calendar_text,calendar_title,unavailable_dates,title_font_modifier,nav_font_modifier,page_font_modifier,social_media_orientation

*/

function hiddenVar($v) {
	$match = false;
	
	$hiddenVars = array();
	$hiddenVars[] = 'texture_file_name';
	$hiddenVars[] = 'Texture_File_Name';
	$hiddenVars[] = 'secondary_texture';
	$hiddenVars[] = 'texture_file';

	$hiddenVars[] = 'title_font_modifier';
	$hiddenVars[] = 'nav_font_modifier';
	$hiddenVars[] = 'page_font_modifier';

	$hiddenVars[] = 'unavailable_dates';
	$hiddenVars[] = 'calendar_title';                      
	$hiddenVars[] = 'calendar_text';
	
	foreach ($hiddenVars as $hv) {
		if ($v == $hv) {
			$match = true;
		}
	}
	
	return $match;
}


function getVarData($v) {
	
	$arr = array();
	$arr['name'] = ucfirst(str_replace("_", " ", $v));
		
	//
	if ($_SESSION['template'] == 'arch_cape') {
		include('../../inc/template_settings/arch_cape.settings.php');
		
	} else if ($_SESSION['template'] == 'atwood') {
		include('../../inc/template_settings/atwood.settings.php');

	} else if ($_SESSION['template'] == 'artistseries1') {
		include('../../inc/template_settings/artistseries1.settings.php');
		
	} else if ($_SESSION['template'] == 'aurora_way') {
		include('../../inc/template_settings/aurora.settings.php');

	} else if ($_SESSION['template'] == 'desperado') {
		include('../../inc/template_settings/desperado.settings.php');
		
	} else if ($_SESSION['template'] == 'bandon') {
		include('../../inc/template_settings/bandon.settings.php');
		
	} else if ($_SESSION['template'] == 'bond' || $_SESSION['template'] == 'york') { // for york or bond	
		include('../../inc/template_settings/york-bond.settings.php');  
		
	} else if ($_SESSION['template'] == 'burlington') {
		include('../../inc/template_settings/burlington.settings.php');
   
	} else if ($_SESSION['template'] == 'burnside') {
		include('../../inc/template_settings/burnside.settings.php');

	} else if ($_SESSION['template'] == 'butler') {
		include('../../inc/template_settings/butler.settings.php');

	} else if ($_SESSION['template'] == 'cascade_lakes') {
		include('../../inc/template_settings/cascade.settings.php');

	} else if ($_SESSION['template'] == 'edgewood' || $_SESSION['template'] == 'greenwood') {
		include('../../inc/template_settings/edgewood-greenwood.settings.php');

	} else if ($_SESSION['template'] == 'greer') {
		include('../../inc/template_settings/greer.settings.php');

	} else if ($_SESSION['template'] == 'lapine') {
		include('../../inc/template_settings/lapine.settings.php');

	} else if ($_SESSION['template'] == 'da' || $_SESSION['template'] == 'stark') { // le petit jardin (design aglow)
		include('../../inc/template_settings/stark.settings.php');
		//include('../../inc/template_settings/lepetitjardin.settings.php');

	} else if (	$_SESSION['template'] == 'nakoma') { 
	   	include('../../inc/template_settings/nakoma.settings.php');

	} else if (	$_SESSION['template'] == 'newport') { 
	   	include('../../inc/template_settings/newport.settings.php');

	} else if (	$_SESSION['template'] == 'northbelize') { 
	   	include('../../inc/template_settings/northbelize.settings.php');

	} else if (	$_SESSION['template'] == 'pioneer') { 
	   	include('../../inc/template_settings/pioneer.settings.php');

	} else if (	$_SESSION['template'] == 'sage') { 
	   	include('../../inc/template_settings/sage.settings.php');

	} else if (	$_SESSION['template'] == 'santiam') { 
	   	include('../../inc/template_settings/santiam.settings.php');

	} else if (	$_SESSION['template'] == 'wapato') { 
	   	include('../../inc/template_settings/wapato.settings.php');

	} else if ($_SESSION['template'] == 'boonesferry') {
		include('../../inc/template_settings/boonesferry.settings.php');
		
	} else if ($_SESSION['template'] == 'pearl') {
		include('../../inc/template_settings/pearl.settings.php');

	} else if ($_SESSION['template'] == 'shoreline') {
		include('../../inc/template_settings/shoreline.settings.php');

	} else if ($_SESSION['template'] == 'yorkhtml5') {
		include('../../inc/template_settings/yorkhtml5.settings.php');

	} else if ($_SESSION['template'] == 'burlingtonhtml5') {
		include('../../inc/template_settings/burlingtonhtml5.settings.php');

	} else if ($_SESSION['template'] == 'rubix') {
		include('../../inc/template_settings/rubix.settings.php');

	} else if ($_SESSION['template'] == 'design55') {
		include('../../inc/template_settings/design55.settings.php');

	} else if ($_SESSION['template'] == 'glowmont') {
		include('../../inc/template_settings/glowmont.settings.php');

	}
			
		
	return $arr;
	//

	/*	
	switch ($v) {
	    case "content_width":
			$arr['type'] = "slider";
			$arr['vals'] = "640,2560";
			$arr['tip'] = "blah blah blah";
	        break;
	
	    case "content_height":
			$arr['type'] = "slider";
			$arr['vals'] = "480,1440";
			$arr['tip'] = "blah blah blah";
	        break;
	
	    case "slideshow_speed" || "slide_show_speed":
			$arr['type'] = "slider";
			$arr['vals'] = "2,20";
			$arr['tip'] = "This setting controls the speed of intro and gallery slideshows. This is the amount of time in seconds an image shows before the next image loads. Loading times for individual images may vary.";
	        break;
	
		case "auto_start_slideshow" || "auto_start_slide_show":
			$arr['type'] = "radio";
			$arr['vals'] = array('yes','no');
			$arr['labels'] = array('On','Off');
			$arr['tip'] = "If turned on, this will start the slideshow automatically when entering a gallery.";
			break;
			
		case "custom_color":
			$arr['type'] = "color";
			$arr['tip'] = "Here is a custom color...";
			break;
			
		case "custom_colors":
			$arr['type'] = "colors";
			$arr['numColors'] = 5;
			$arr['tip'] = "Here are custom colors...";
			break;
			
		case "redirect_target":
			$arr['type'] = "dropdown";
			$arr['vals'] = array('_self','_blank','bfRedirect');
			$arr['labels'] = array('same window','new window','all redirects in the same new window');
			break;
			
		case "featured_category":
			$arr['type'] = "dropdown";
			$catsArr = get_all_categories();
			$catLabels = array('None');
			$catVals = array('');
			foreach($catsArr as $c) {
				$catLabels[] = $c['category_name'];
				$catVals[] = $c['category_name'];
			}
			$arr['vals'] = $catVals;
			$arr['labels'] = $catLabels;
			break;
			
		default: 
			$arr['type'] = "text";
	}
	return $arr;
	*/
}

function getVarMarkup($var) {
	$arr = getVarData($var);
	$m = "";
	
	$m .= "<li";
	
	if (hiddenVar($var) || $arr['type'] == 'hidden') {
		$m .=" style='display:none;' ";
	}
	
	$m .= ">";
	$m .= "<label for='var_$var'>";
	$m .= $arr['name'];
	
	if (isset($arr['tip'])) {
		$m .= " (<a href='javascript:;' class='bfToolTip' title='"; //
		$m .= $arr['tip'];
		$m .= "'>?</a>)";
	}
	
	$m .= "</label>";
	
	global $fvalues;
	$fval = $fvalues[$var];
	switch ($arr['type']) {
		
		case "slider":
			$m .= "<div class='bfSlider' title='$arr[vals],$fval'></div>";
			$m .= "<input type='text' class='bfInputText bfSliderLabel bfChangeListen' name='var_$var' id='var_$var' value='$fval' />";
			break;
			
		case "radio":
			$m .= "<div class='uiButtonSet'>";
			
			if ($fval == "") {
				//$fval = $arr['vals'][0];
			}
			for ($i=0; $i<count($arr['vals']); $i++) {
				
				$m .= "<label for='$var-$i' class='radioLabel'>";
				$m .= $arr['labels'][$i];
				$m .= "</label>";
				$m .= "<input type='radio' name='var_$var' id='$var-$i' value='";
				$m .= $arr['vals'][$i];
				$m .= "'";
				if (strtolower($fval) == strtolower($arr['vals'][$i])) {
					$m .= " checked='checked'";
				}
				$m .= " class='bfChangeListen' />";
				
			}
			$m .= "</div>";
			break;
			
		case "dropdown":
			$m .= "<select name='var_$var' id='var_$var' class='bfChangeListen'>";
			// do selected value
			if ($fval != "") {
				$m .= "<option value='$fval'>";
				for ($i=0; $i<count($arr['vals']); $i++) {
					if ($fval == $arr['vals'][$i]) {
						$m .= $arr['labels'][$i];
					}
				}
				$m .= "</option>";
				$m .= "<option value=''> </option>";
			}
			for ($i=0; $i<count($arr['vals']); $i++) {
				$m .= "<option value='";
				$m .= $arr['vals'][$i];
				$m .= "'>";
				$m .= $arr['labels'][$i];
				$m .= "</option>";
			}
			$m .= "</select>";
			break;
			
		case "color":
			//$m .= "<input type='text' class='bfInputText bfColor' name='var_$var' id='var_$var' value='$fval' />";
			$m .= "<input type='hidden' name='var_$var' id='var_$var' value='$fval' class='bfChangeListen' />";
			$m .= "<div class='bfSwatch' style='background-color:$fval'></div>";
			break;
			
		case "colors":
			//$m .= "<input type='text' class='bfInputText' name='var_$var' id='var_$var' value='$fval' />";
			$m .= "<input type='hidden' name='var_$var' id='var_$var' value='$fval' class='bfChangeListen' />";
			$vals = explode(",", $fval);
			//foreach($vals as $v) {
			for ($i=0; $i<$arr['numColors']; $i++) {
				if ($vals[$i] == "" || $vals[$i] == undefined) {
					$vals[$i] = "#FFFFFF";
				}
				$m .= "<div class='bfSwatchSmall' id='color$i' style='background-color:$vals[$i]' title='$vals[$i]'></div>";
			}
			break;
			
		default:
			$m .= "<input type='text' class='bfInputText bfChangeListen' name='var_$var' id='var_$var' value='$fval' />";
	}
	$m .= "</li>";

	return $m;
}

?>

<div class="contentHeader">
	<h1>Template Settings -</h1>  <a href="https://vimeo.com/146036093" target="_blank"><img src="img/watch-this-video2.png" border="0" style="position:absolute; margin-left:290px; margin-top:-50px;" /></a>
</div>

<div class="tip_message">These custom settings are specific to your template. For details, hover over the question mark next to each setting, or view the support page for your site design.</div>
<div id="editPane">
	<fieldset>
		<ul>
			<? 
			//
			foreach($fvars as $var) { 
				$m = getVarMarkup($var); 
				echo ($m);
			} 
			//
			?>
			<div style="clear: both;"></div>
		</ul>
	</fieldset>
</div>
