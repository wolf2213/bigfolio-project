<?
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");
//
$s = get_settings();
?>

<div class="contentHeader">
	<h1>Browser Settings -</h1>  <a href="https://vimeo.com/127729732" target="_blank"><img src="img/watch-this-video2.png" border="0" style="position:absolute; margin-left:290px; margin-top:-50px;" /></a>
</div>

<div class="tip_message">These settings control the meta information associated with your site. The browser title controls the title that appears on the top of the browser window or tab. Keywords should be separated by a comma. The description should describe your business and will appear in search results.</div>
<div id="editPane">
	<fieldset>
		<ul>
			<li>
				<label for="btitle">Browser title (<a href="javascript:;" class="bfToolTip" title="This is the title displayed at the top of the browser window. This is a crucial factor for SEO and should contain important keywords.">?</a>)</label>
				<input name="btitle" type="text" id="btitle" value="<?= $s['browser_title']; ?>" class="bfInputText bfChangeListen" />
			</li>
			<li>
				<label for="keywords">Keywords (<a href="javascript:;" class="bfToolTip" title="These are the keywords displayed in the meta tags of your site.">?</a>)</label>
				<textarea name="keywords" rows="6" cols="35" id="keywords" class="bfTextarea bfChangeListen"><?= $s['meta_keywords']; ?></textarea>
			</li>
			<li>
				<label for="description">Description (<a href="javascript:;" class="bfToolTip" title="This is the description of your site and is used by some search engines when displaying results.">?</a>)</label>
				<textarea name="description" rows="6" cols="35" id="description" class="bfTextarea bfChangeListen"><?= $s['meta_description']; ?></textarea>
			</li>
			<!--
			<li><label for="start_mode">Entry Page</label>
			<div class="uiButtonSet">
				<label for="start_mode_entry" class="radioLabel">On</label>
				<input name="start_mode" id="start_mode_entry" type="radio" value="ENTRY" class="bfChangeListen" <? if ($s['start_mode']=='ENTRY') { ?>checked="checked"<? } ?> />
				<label for="start_mode_flash" class="radioLabel">Off</label>
				<input name="start_mode" id="start_mode_flash" type="radio" value="FLASH" class="bfChangeListen" <? if ($s['start_mode']=='FLASH') { ?>checked="checked"<? } ?> />
			</div>
			</li>
			-->
				
			
		</ul>
	</fieldset>
</div>