<?
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");
//
$s = get_settings();

//
// get font files from remote server 
$postArr = array();
$postArr['u'] = $_SESSION['user']['email'];
$postArr['p'] = $_SESSION['user']['password'];
$postArr['mode'] = 'music';
$url = SERVER . '_bfadmin/files_list.php'; // 
$remoteFiles = curl_post($url, $postArr, array()); 
$remoteFiles = json_decode($remoteFiles); 


?>
<div class="contentHeader">
	<h1>Music -</h1>  <a href="https://vimeo.com/127981830" target="_blank"><img src="img/watch-this-video2.png" border="0" style="position:absolute; margin-left:115px; margin-top:-50px;" /></a>
</div>

<div class="tip_message">You can upload mp3s to play music on your site. Some players support displaying artist / song information if ID3 information is available. If no music is uploaded, the music player will not be present.</div>
<div id="editPane">
	<fieldset>
		<div id="musicFilesHolder">
			<!-- intro images here -->
			<?
			foreach($remoteFiles as $file) {
				if (substr($file, 2, 2) == '--') {
					$prettyName = substr($file, 4);
				} else {
					$prettyName = $file;
				}
				$html  = "";
             	$html .= '<div class="musicItem bfSortable">';
				$html .= '<div class="deleteButton"></div>';
				$html .= '<div style="clear:both;"></div>';
				$html .= '<div class="musicFileLabel">';
				$html .= '<a href="'.$_SESSION['user']['temp_url'].'music/'.$file.'" target="_blank" title="'.$file.'">'.$prettyName.'</a>';
				$html .= '</div>';
				$html .= '</div>'; 
				echo $html;
			}
			?>
	 	</div>
		<div style="clear:both;"></div>
		<input name="musicfiles" type="file" id="musicfiles" size="10" /> 
		<div id="status-message"></div>
		<div id="music-queue"></div>  
	</fieldset>
</div> 

<script type="text/javascript">
// <![CDATA[

//
// music uploader 
$("#musicfiles").uploadifive({
  'uploadScript'   : adminURL + '_bfadmin/file_upload.php', 
  'method'         : 'post',
  'formData'       : {'u':u,'p':p,'mode':'music'},      
  'cancelImg'      : 'uploadifive-cancel.png',
  'multi'          : true,
  'auto'           : true,
  'fileExt'        : '*.mp3;',
  'fileDesc'       : 'MP3 Files (.mp3,)',
  'queueID'        : 'music-queue', 
  'buttonText'     : 'UPLOAD MP3S',
  'width'          : 125,
  'queueSizeLimit' : 3,
  'simUploadLimit' : 1,
  'removeCompleted': true,
  'onSelectOnce'   : function(event,data) {
      	//$('#status-message').text(data.filesSelected + 'uploading...');
		$('#status-message').text('uploading...');
    }, 
  'onUploadComplete'     : function(file, data) { 
	//	var fileParts = getNameAndExtension(fileObj['name']); 
	//	var musicFile = cleanfile(fileParts[0])+'.'+fileParts[1];
		var musicFile = file.name;
	//  	$.log('fileName: '+cleanfile(fileParts[0])+'.'+fileParts[1]);
	//  	$.log('response: '+response); 
		//
		var html = "";
		html += '<div class="musicItem bfSortable">';
		html += '<div class="deleteButton"></div>';
		html += '<div style="clear:both;"></div>';
		html += '<div class="musicFileLabel">'
		html += '<a href="'+tempURL+'music/'+musicFile+'" title="'+musicFile+'" target="_blank" title="'+musicFile+'">'+musicFile+'</a>';
		html += '</div>';
		//
		$("#musicFilesHolder").append(html);
		//introFunctionality();
		//
	  	
   	},
  'onAllComplete'  : function(event,data) {
		$.log("filesUploaded: "+data.filesUploaded);
		$.log("data.errors: "+data.errors);
		if (data.errors > 0) {
      		$('#status-message').text("There was an error with the upload.");
		} else {
			$('#status-message').text("");
		}
    }
});




function musicFunctionality() {
	$("#musicFilesHolder").sortable(
		{
		    update: function(event, ui) { 
				updateMusicOrder();
      		}
		}
	); 
	$("#musicFilesHolder").find(".deleteButton").unbind("click");
	$("#musicFilesHolder").find(".deleteButton").click(function() { 
			//
			var save = confirm("Are you sure you want to delete this file? This cannot be undone");
			if (save) {
				//
			  	var fileName = $(this).parent().find('a').attr("title");
			    $.ajax({
		          type: 'POST',
		          url:adminURL + '_bfadmin/file_remove.php',
		          data: ({'u':u,'p':p, 'mode':'music', 'file':fileName})
		        }).done(function( msg ) {
				  $.log( "msg: " + msg );
				});
				//
				var thumb = $(this).parent();
				thumb.fadeOut('fast', function() {
				    thumb.remove();
				});
			}
			// 
	});
	function updateMusicOrder() {
		var imagesArr = new Array();
		$('#musicFilesHolder').find("a").each(function(index, value) {
			imagesArr.push($(this).attr("title"));
			var fileName = $(this).attr("title");
			if (fileName.substr(2,2) == '--') {
				if (index <= 9) {
					fileName = '0' + index + '--' + fileName.substr(4);
				} else {
					fileName = index + '--' + fileName.substr(4);
				}
			} else {
				if (index <= 9) {
					fileName = '0' + index + '--' + fileName;
				} else {
					fileName = index + '--' + fileName;
				}
			}                                      
			$(this).attr("title", fileName);
		}); 
		var imagesStr = imagesArr.join(',');
		$.ajax({
          type: 'POST',
          url:adminURL + '_bfadmin/music_order.php',
          data: ({'u':u,'p':p, 'music':imagesArr})
        }).done(function( msg ) {
		  $.log( "msg: " + msg );
		});
	}
	$("#musicFilesHolder").disableSelection();
}

musicFunctionality();   

// ]]>
</script>