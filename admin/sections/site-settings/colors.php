<?
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");
//
$s = get_settings(); 
//
$colorsArr       = array();
$colorsLabels    = array();
//
$colorsArr[]     = 'title_color'; 
$colorsLabels[]  = 'Title Color';  
//
$colorsArr[]     = 'text_color';
$colorsLabels[]  = 'Text Color'; 
//
$colorsArr[]     = 'body_color'; 
$colorsLabels[]  = 'Body Color';
//
$colorsArr[]     = 'app_color';  
$colorsLabels[]  = 'Secondary Color'; 
//
$colorsArr[]     = 'app_border_color';  
$colorsLabels[]  = 'Border Color'; 
//
$colorsArr[]     = 'page_color';
$colorsLabels[]  = 'Page Color';
//  
$colorsArr[]     = 'custom_color_1'; 
$colorsLabels[]  = 'Custom Color 1';
// 
$colorsArr[]     = 'custom_color_2';
$colorsLabels[]  = 'Custom Color 2';
// 
$colorsArr[]     = 'custom_color_3';
$colorsLabels[]  = 'Custom Color 3'; 

?>

<div class="contentHeader">
	<h1>Color Settings -</h1>  <a href="https://vimeo.com/146027625" target="_blank"><img src="img/watch-this-video2.png" border="0" style="position:absolute; margin-left:240px; margin-top:-50px;" /></a>
</div>

<div class="tip_message">Click on the color swatches to change the color. Drag the color sliders or enter the desired hex code in the color editor.</div>
<div id="editPane">
	<fieldset>
		<ul>
			<? for($i=0; $i<count($colorsArr); $i++) { ?> 
				<li>
					<label><?=$colorsLabels[$i];?></label>
					<input type='hidden' name='<?=$colorsArr[$i]?>' id='<?=$colorsArr[$i]?>' value='<?=$s[$colorsArr[$i]]?>' class='bfChangeListen' />
					<div class='bfSwatch' style='background-color:<?=$s[$colorsArr[$i]]?>'></div>
				</li>
			<? } ?>
			<div style="clear: both;"></div>
		</ul>
	</fieldset>
</div>
