<?
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");
//
$s = get_settings();
$fvalues = unserialize($s['flash_vars']);
$fvars = split(',',get_property('FLASH_VARS'));
foreach($fvars as $var) {
	if ($var == 'texture_file') {
		$textureFileName = $fvalues[$var];
	}
	if ($var == 'Texture_File_Name') {
		$textureFileName = $fvalues[$var];
	}
	if ($var == 'texture_file_name') {
		$textureFileName = $fvalues[$var];
	}
}

?>
<div class="contentHeader">
	<h1>Texture -</h1>  <a href="https://vimeo.com/127981972" target="_blank"><img src="img/watch-this-video2.png" border="0" style="position:absolute; margin-left:135px; margin-top:-50px;" /></a>
</div>

<div class="tip_message">You can upload a texture file here. We recommend images 1440 pixels wide up to 2600 pixels wide.</div>
<div id="editPane">
	<fieldset>
		<ul>
			<!-- texture uploader -->
			<li> 
				<div id="deleteTexture" class="deleteButton" <? if ($textureFileName == "") { ?>style="display:none"<?}?>></div>
				<div id="textureHolder">
					<? if ($textureFileName != '') { 
						$ur = $_SESSION['user']['temp_url'];
						?>
						<img src="<? echo($ur); ?>/gallery/medium/<?= $textureFileName ?>" border="0" id="textureFileImg" title="<?= $textureFileName ?>"/>
					<? } else { ?>
						<p>There is currently no texture file uploaded.</p>
					<? } ?>
					 
			 	</div>
			    
			    <div style="clear:both;"></div>
			    <input type="hidden" name="textureFileName" id="textureFileName" class="bfChangeListen" value="<?=$textureFileName?>" />
				<input name="texturefile" type="file" id="texturefile" size="10" /> 
				<div id="status-message">
					
				</div> 
				<div id="texture-queue">
				</div>
			</li>
		</ul>
	</fieldset>
</div>
            

<script type="text/javascript">
// <![CDATA[

// texture upload 
$("#texturefile").uploadifive({
  'uploadScript'   : adminURL + '_bfadmin/file_upload.php', 
  'method'         : 'post',
  'formData'       : {'u':u,'p':p,'mode':'texture'},      
  'cancelImg'      : 'uploadifive-cancel.png',
  'multi'          : true,
  'auto'           : true,
  'fileExt'        : '*.jpg;*.jpeg;*.png;',
  'fileDesc'       : 'Image Files (.JPG, .JPEG, .PNG)',
  'queueID'        : 'texture-queue', 
  'buttonText'     : 'UPLOAD TEXTURE',
  'width'          : 145,
  'queueSizeLimit' : 1,
  'simUploadLimit' : 1,
  'removeCompleted': true,
  'onSelectOnce'   : function(event,data) {
      	//$('#status-message').text(data.filesSelected + 'uploading...');
		$('#status-message').text('uploading...');
    }, 
  'onUploadComplete'     : function(file, data) { 
	//	var fileParts = getNameAndExtension(fileObj['name']); 
	//	var textureFile = cleanfile(fileParts[0])+'.'+fileParts[1];
		var textureFile = file.name;
	 // 	$.log('fileName: '+cleanfile(fileParts[0])+'.'+fileParts[1]);
	 // 	$.log('response: '+response); 
	  	var texturehtml = "";
			texturehtml += "<img src='"+tempURL+"gallery/medium/"+textureFile+"' id='textureFileImg' title='"+textureFile+"'/>"; 
			
		$("#textureHolder").html(texturehtml);
		    
		$("#textureFileName").val(textureFile);
		$("#deleteTexture").show(); 
		//
		// save texture file name in db  
		saveSection();
   	},
  'onAllComplete'  : function(event,data) {
		$.log("filesUploaded: "+data.filesUploaded);
		$.log("data.errors: "+data.errors);
		if (data.errors > 0) {
      		$('#status-message').text("There was an error with the upload.");
		} else {
			$('#status-message').text("");
		}
    }
});

$("#deleteTexture").click(function() {
	var save = confirm("Are you sure you want to delete this file? This cannot be undone");
	if (save) {
		//
	  	var fileName = $("#textureFileImg").attr("title");
	    $.ajax({
          type: 'POST',
          url:adminURL + '_bfadmin/file_remove.php',
          data: ({'u':u,'p':p, 'mode':'texture', 'file':fileName})
        }).done(function( msg ) {
		  $.log( "msg: " + msg );
		});
		// 
		$("#textureFileName").val("");
		var thumb = $("#textureHolder").find("img");
		thumb.fadeOut('fast', function() {
		    thumb.remove();
		}); 
		$(this).hide(); 
		//
		saveSection();
	}
});

// ]]>
</script>