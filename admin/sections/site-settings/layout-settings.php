<?
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");
//
$s = get_settings();
$fvalues = unserialize($s['flash_vars']);
$fvars = split(',',get_property('FLASH_VARS'));

$galTypes = split(',',get_property('GALLERY_MODES'));
$hasMasonry = false;
$hasFullscreen = false;
foreach($galTypes as $gt) {
	if ($gt == "masonry") {
		$hasMasonry = true;
	} else if ($gt == "fullscreen") {
		$hasFullscreen = true;
	}
}
?>

<div class="contentHeader">
	<h1>Layout</h1>
</div>

<div class="tip_message">Use this area to administer the layout of your site. Use the preview button at the bottom to preview your layout changes without affecting the current layout of your site.</div>
<div id="editPane">
	<fieldset>
		
		<!-- menu position -->
		<? $menu_position = $fvalues['menu_position']; ?>
		<? if ($menu_position == "") { $menu_position = "left"; } ?>
		<legend>Menu Position</legend>
		<div class="tip_message">The menu position setting changes where the menu will be positioned on the website relative to the main content.</div>
		<div class="bfLayoutSet" id="menu_position">
			<div class="bfLayoutChoice">
				<label for="menu_position_left" class="bfLayoutLabel">
					<img src="img/rubix-layout/menu-left.jpg" alt="" />
				</label>
				<br>
				<input type="radio" name="menu_position" id="menu_position_left" value="left" <?if($menu_position=='left'){?>checked<?}?>> Left
			</div>
			<div class="bfLayoutChoice">
				<label for="menu_position_top" class="bfLayoutLabel">
					<img src="img/rubix-layout/menu-top.jpg" alt="" />
				</label>
				<br>
				<input type="radio" name="menu_position" id="menu_position_top" value="top" <?if($menu_position=='top'){?>checked<?}?>> Top
			</div>
			<div class="bfLayoutChoice">
				<label for="menu_position_right" class="bfLayoutLabel">
					<img src="img/rubix-layout/menu-right.jpg" alt="" />
				</label>
				<br>
				<input type="radio" name="menu_position" id="menu_position_right" value="right" <?if($menu_position=='right'){?>checked<?}?>> Right
			</div>
		</div>
		<div style="clear: both;"></div>
		<!-- /menu position -->
		
		<!-- menu alignment -->
		<? $menu_alignment = $fvalues['menu_alignment']; ?>
		<? if ($menu_alignment == "") { $menu_alignment = "left"; } ?>
		<legend>Menu Text Alignment</legend>
		<div class="tip_message">The menu text alignment setting changes how the text of the menu items are aligned.</div>
		<div class="bfLayoutSet alignment_set" id="menu_alignment_topMenu" <?if($menu_position!='top'){?>style="display:none;"<?}?>>
			<div class="bfLayoutChoice">
				<label for="menuTop_alignment_left" class="bfLayoutLabel">
					<img src="img/rubix-layout/menu-t-align-left.jpg" alt="" />
				</label>
				<br>
				<input type="radio" name="menu_alignment" id="menuTop_alignment_left" value="left" <?if($menu_position=='top'&&$menu_alignment=='left'){?>checked<?}?>> Left
			</div>
			<div class="bfLayoutChoice">
				<label for="menuTop_alignment_center" class="bfLayoutLabel">
					<img src="img/rubix-layout/menu-t-align-center.jpg" alt="" />
				</label>
				<br>
				<input type="radio" name="menu_alignment" id="menuTop_alignment_center" value="center" <?if($menu_position=='top'&&$menu_alignment=='center'){?>checked<?}?>> Center
			</div>
			<div class="bfLayoutChoice">
				<label for="menuTop_alignment_right" class="bfLayoutLabel">
					<img src="img/rubix-layout/menu-t-align-right.jpg" alt="" />
				</label>
				<br>
				<input type="radio" name="menu_alignment" id="menuTop_alignment_right" value="right" <?if($menu_position=='top'&&$menu_alignment=='right'){?>checked<?}?>> Right
			</div>
		</div>
		<div style="clear: both;"></div>
		
		<div class="bfLayoutSet alignment_set" id="menu_alignment_leftMenu" <?if($menu_position!='left'){?>style="display:none;"<?}?>>
			<div class="bfLayoutChoice">
				<label for="menuLeft_alignment_left" class="bfLayoutLabel">
					<img src="img/rubix-layout/menu-l-align-left.jpg" alt="" />
				</label>
				<br>
				<input type="radio" name="menu_alignment" id="menuLeft_alignment_left" value="left" <?if($menu_position=='left'&&$menu_alignment=='left'){?>checked<?}?>> Left
			</div>
			<div class="bfLayoutChoice">
				<label for="menuLeft_alignment_center" class="bfLayoutLabel">
					<img src="img/rubix-layout/menu-l-align-center.jpg" alt="" />
				</label>
				<br>
				<input type="radio" name="menu_alignment" id="menuLeft_alignment_center" value="center" <?if($menu_position=='left'&&$menu_alignment=='center'){?>checked<?}?>> Center
			</div>
			<div class="bfLayoutChoice">
				<label for="menuLeft_alignment_right" class="bfLayoutLabel">
					<img src="img/rubix-layout/menu-l-align-right.jpg" alt="" />
				</label>
				<br>
				<input type="radio" name="menu_alignment" id="menuLeft_alignment_right" value="right" <?if($menu_position=='left'&&$menu_alignment=='right'){?>checked<?}?>> Right
			</div>
		</div>
		<div style="clear: both;"></div>
		
		<div class="bfLayoutSet alignment_set" id="menu_alignment_rightMenu" <?if($menu_position!='right'){?>style="display:none;"<?}?>>
			<div class="bfLayoutChoice">
				<label for="menuRight_alignment_left" class="bfLayoutLabel">
					<img src="img/rubix-layout/menu-r-align-left.jpg" alt="" />
				</label>
				<br>
				<input type="radio" name="menu_alignment" id="menuRight_alignment_left" value="left" <?if($menu_position=='right'&&$menu_alignment=='left'){?>checked<?}?>> Left
			</div>
			<div class="bfLayoutChoice">
				<label for="menuRight_alignment_center" class="bfLayoutLabel">
					<img src="img/rubix-layout/menu-r-align-center.jpg" alt="" />
				</label>
				<br>
				<input type="radio" name="menu_alignment" id="menuRight_alignment_center" value="center" <?if($menu_position=='right'&&$menu_alignment=='center'){?>checked<?}?>> Center
			</div>
			<div class="bfLayoutChoice">
				<label for="menuRight_alignment_right" class="bfLayoutLabel">
					<img src="img/rubix-layout/menu-r-align-right.jpg" alt="" />
				</label>
				<br>
				<input type="radio" name="menu_alignment" id="menuRight_alignment_right" value="right" <?if($menu_position=='right'&&$menu_alignment=='right'){?>checked<?}?>> Right
			</div>
		</div>
		<div style="clear: both;"></div>
		<!-- /menu alignment -->
		
		<!-- gallery mode -->
		<? $gallery_mode = $fvalues['gallery_mode']; ?>
		<? if ($gallery_mode == "") { $gallery_mode = "large"; } ?>
		<legend>Gallery Mode</legend>
		<div class="tip_message">The gallery mode setting changes the way gallery images show. Single mode shows the gallery images one at a time. Horizontal and vertical modes show the gallery images in a scrollable area that scroll horizontally or vertically. Fullscreen mode makes gallery images fill the available window space. Masonry mode sets the galleries as a large, fluid layout of thumbnails.</div>
		<div class="bfLayoutSet" id="gallery_mode">
			<div class="bfLayoutChoice">
				<label for="gallery_mode_single" class="bfLayoutLabel">
					<img src="img/rubix-layout/gallery-single.jpg" alt="" />
				</label>
				<br>
				<input type="radio" name="gallery_mode" id="gallery_mode_single" value="large" <?if($gallery_mode=='large'){?>checked<?}?>> Single
			</div>
			<div class="bfLayoutChoice">
				<label for="gallery_mode_horizontal" class="bfLayoutLabel">
					<img src="img/rubix-layout/gallery-horizontal.jpg" alt="" />
				</label>
				<br>
				<input type="radio" name="gallery_mode" id="gallery_mode_horizontal" value="horizontal" <?if($gallery_mode=='horizontal'){?>checked<?}?>> Horizontal
			</div>
			<div class="bfLayoutChoice">
				<label for="gallery_mode_vertical" class="bfLayoutLabel">
					<img src="img/rubix-layout/gallery-vertical.jpg" alt="" />
				</label>
				<br>
				<input type="radio" name="gallery_mode" id="gallery_mode_vertical" value="vertical" <?if($gallery_mode=='vertical'){?>checked<?}?>> Vertical
			</div>
			<? if ($hasFullscreen) { ?>
			<div class="bfLayoutChoice">
				<label for="gallery_mode_fullscreen" class="bfLayoutLabel">
					<a href='javascript:;' class='bfToolTip' title='This will make gallery images fill the available space. Please note that cropping will occur on images where the viewer&apos;s browser window aspect ratio does not match the image&apos;s. Fullscreen mode reverts to single gallery mode on the mobile site.'>
					<img src="img/rubix-layout/gallery-fullscreen.jpg" alt="" />
					</a>
				</label>
				<br>
				<input type="radio" name="gallery_mode" id="gallery_mode_fullscreen" value="fullscreen" <?if($gallery_mode=='fullscreen'){?>checked<?}?>> Fullscreen
			</div>
			<? } ?>
			<? if ($hasMasonry) { ?>
			<div class="bfLayoutChoice">
				<label for="gallery_mode_masonry" class="bfLayoutLabel">
					<img src="img/rubix-layout/gallery-masonry.jpg" alt="" />
				</label>
				<br>
				<input type="radio" name="gallery_mode" id="gallery_mode_masonry" value="masonry" <?if($gallery_mode=='masonry'){?>checked<?}?>> Masonry
			</div>
			<? } ?>
		</div>
		<div style="clear: both;"></div>
		<!-- /gallery mode -->
		
		<!-- thumbnail mode -->
		<? $thumbnail_mode = $fvalues['thumbnail_mode']; ?>
		<? if ($thumbnail_mode == "") { $thumbnail_mode = "bottom"; } ?>
		<legend>Thumbnails Mode</legend>
		<div class="tip_message">The thumbnails mode setting changes the way thumbnials show relative to the main content.</div>
		<div class="bfLayoutSet" id="thumbnail_mode" <?if($gallery_mode=='masonry') { ?>style="display:none;"<?}?>>
			<div class="bfLayoutChoice">
				<label for="thumbnail_mode_bottom" class="bfLayoutLabel">
					<img src="img/rubix-layout/thumbs-bottom.jpg" alt="" />
				</label>
				<br>
				<input type="radio" name="thumbnail_mode" id="thumbnail_mode_bottom" value="bottom" <?if($thumbnail_mode=='bottom'){?>checked<?}?>> Bottom
			</div>
			<div class="bfLayoutChoice">
				<label for="thumbnail_mode_left" class="bfLayoutLabel">
					<img src="img/rubix-layout/thumbs-left.jpg" alt="" />
				</label>
				<br>
				<input type="radio" name="thumbnail_mode" id="thumbnail_mode_left" value="left" <?if($thumbnail_mode=='left'){?>checked<?}?>> Left
			</div>
			<div class="bfLayoutChoice">
				<label for="thumbnail_mode_right" class="bfLayoutLabel">
					<img src="img/rubix-layout/thumbs-right.jpg" alt="" />
				</label>
				<br>
				<input type="radio" name="thumbnail_mode" id="thumbnail_mode_right" value="right" <?if($thumbnail_mode=='right'){?>checked<?}?>> Right
			</div>
			<div class="bfLayoutChoice">
				<label for="thumbnail_mode_overlay" class="bfLayoutLabel">
					<img src="img/rubix-layout/thumbs-overlay.jpg" alt="" />
				</label>
				<br>
				<input type="radio" name="thumbnail_mode" id="thumbnail_mode_overlay" value="overlay" <?if($thumbnail_mode=='overlay'){?>checked<?}?>> Overlay
			</div>
			<div class="bfLayoutChoice">
				<label for="thumbnail_mode_none" class="bfLayoutLabel">
					<img src="img/rubix-layout/thumbs-none.jpg" alt="" />
				</label>
				<br>
				<input type="radio" name="thumbnail_mode" id="thumbnail_mode_none" value="none" <?if($thumbnail_mode=='none'){?>checked<?}?>> None
			</div>
			
		</div>
		<div class="bfLayoutSet" id="masonry_thumbnail_mode" <?if($gallery_mode!='masonry') { ?>style="display:none;"<?}?>>
			<div class="bfLayoutChoice">
				<label for="thumbnail_mode_static" class="bfLayoutLabel">
					<img src="img/rubix-layout/thumbs-masonry-static.jpg" alt="" />
				</label>
				<br>
				<input type="radio" name="thumbnail_mode" id="thumbnail_mode_static" value="static" <?if($thumbnail_mode=='static'){?>checked<?}?>> Static size
			</div>
			<div class="bfLayoutChoice">
				<label for="thumbnail_mode_fluid" class="bfLayoutLabel">
					<img src="img/rubix-layout/thumbs-masonry-fluid.jpg" alt="" />
				</label>
				<br>
				<input type="radio" name="thumbnail_mode" id="thumbnail_mode_fluid" value="fluid" <?if($thumbnail_mode=='fluid'){?>checked<?}?>> Fluid size
			</div>
			
		</div>
		<div style="clear: both;"></div>
		<!-- /thumbnail mode -->
		
		<button id="layoutPreviewBtn">Preview Layout</button>
		
	</fieldset>
</div>

<script type="text/javascript">
// <![CDATA[

$('input[name="menu_position"]').click(function() {
	var positionVal = $(this).val();
	$(".alignment_set").each(function() {
		if ($(this).attr("id") == "menu_alignment_"+positionVal+"Menu") {
			$(this).slideDown("fast");
		} else if ($(this).css("display") != "none") {
			$(this).slideUp("fast");
		}
	});
});

$('input[name="gallery_mode"]').click(function() {
	var modeVal = $(this).val();
	if (modeVal == "masonry") {
		$("#masonry_thumbnail_mode").slideDown("fast");
		$("#thumbnail_mode").slideUp("fast");
	} else {
		$("#masonry_thumbnail_mode").slideUp("fast");
		$("#thumbnail_mode").slideDown("fast");
	}
})

$('#layoutPreviewBtn').click(function() {
	var layoutPreviewURL = previewURL + "main.php?previewMode=true&";
	$('input').each(function(i,v) {
		if ($(this).attr('name') != undefined && $(this).attr("checked") == "checked") {
			layoutPreviewURL += $(this).attr("name") + "=" + $(this).val() + "&";
		}
	});
	window.open(layoutPreviewURL.substr(0,layoutPreviewURL.length-1), 'layoutPreview');
});

// ]]>
</script>
