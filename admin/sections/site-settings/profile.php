<?
session_start();
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");
//
$s = get_settings();
$shareSettings = unserialize($s['share_settings']);
$template = get_property('template'); 
$templateType = get_property('TEMPLATE_TYPE');
?>

<div class="contentHeader">
	<h1>Profile -</h1>  <a href="https://vimeo.com/127842962" target="_blank"><img src="img/watch-this-video2.png" border="0" style="position:absolute; margin-left:120px; margin-top:-50px;" /></a>
</div>

<div class="tip_message">Enter your business details as these will appear in various places throughout your admin and your site.</div>
<fieldset>
	<legend>Business Details</legend>
	<ul>
		<li>
			<label for="name">Full name</label>
			<input name="name" type="text" id="name" value="<?= $s['owner_name'] ?>"  class="bfInputText bfChangeListen" />
		</li>
		<li>
			<label for="bizname">Business name</label>
			<input name="bizname" type="text" id="bizname" value="<?= $s['business_name'] ?>"  class="bfInputText bfChangeListen" />
		</li>
		
		<li>
			<label for="email">Email (<a href="javascript:;" class="bfToolTip" title="Your contact form submissions will be sent to this address.">?</a>)</label>
			<input name="email" type="text" id="email" value="<?= $s['email'] ?>"  class="bfInputText bfChangeListen" />
		</li>
		<li>
			<label for="phone">Phone</label>
			<input name="phone" type="text" id="phone" value="<?= $s['phone'] ?>"  class="bfInputText bfChangeListen" />
		</li>
		<li>
			<label for="mobile">Mobile phone (<a href="javascript:;" class="bfToolTip" title="Your contact form submissions can also be sent to your mobile phone. Enter numbers only (including area code), no spaces or dashes.">?</a>)</label>
			<input name="mobile_phone" type="text" id="mobile" value="<?=$s['mobile_phone']?>"  class="bfInputText bfChangeListen" />
		</li>
		<li>
			<label for="mdomain">Mobile provider</label>
			<select name="mobile_domain" id="mdomain" onchange="setMobile()" class="smallSelect bfChangeListen">
				<? if($s['mobile_provider'] != '') { ?>
					<option value="<?=$s['mobile_domain']?>"><?=$s['mobile_provider']?></option>
				<? } ?>
				<? include('../../inc/mobile.inc.php'); ?>
			</select>
			<input type="hidden" name="mobile_provider" id="mname" value="<?=$s['mobile_provider']?>">
		</li>
		<li>
			<label for="address">Address</label>
			<textarea name="address" rows="4" id="address" class="bfTextarea bfChangeListen"><?= trim($s['street_address']) ?></textarea>
		</li>
	</ul>
</fieldset>
<fieldset>
	<legend>Other Information</legend>
	<ul>
		<li>
			<label for="proof">Proofing</label>
			
			<div class="uiButtonSet">
				<input name="proof" type="radio" id="nextproof_radio" value="NEXTPROOF" class="bfChangeListen" <? if ($s['proofing']=='NEXTPROOF') { ?>checked="checked"<? } ?> />
				<label for="nextproof_radio" class="radioLabel">NextProof</label>
			
				<input name="proof" type="radio" id="smugmug_radio" value="SMUGMUG" class="bfChangeListen" <? if ($s['proofing']=='SMUGMUG') { ?>checked="checked"<? } ?> />
				<label for="smugmug_radio" class="radioLabel">SmugMug</label>
			
				<input name="proof" type="radio" id="pictage" value="PICTAGE" class="bfChangeListen" <? if ($s['proofing']=='PICTAGE') { ?>checked="checked"<? } ?> />
				<label for="pictage" class="radioLabel">Pictage</label>
			
                <input name="proof" type="radio" id="none" value="NONE" class="bfChangeListen" <? if ($s['proofing']=='NONE') { ?>checked="checked"<? } ?> />
				<label for="none" class="radioLabel">None</label>
			</div>
			
		</li>
		<li>
			<label for="proof_id">Service ID (<a href="javascript:;" class="bfToolTip" title="Enter your NextProof URL (format: <em>http://user.nextproof.com</em>) or 5 character Pictage ID (format: <em>AB123</em>). You can then add events to any page of your site.">?</a>)</label>
			<input name="proof_id" type="text" id="proof_id" value="<?= $s['proofing_id']; ?>" class="bfInputText" />
		</li>
		<li>
			<label for="copyright">Copyright message (<a href="javascript:;" class="bfToolTip" title="This text will be displayed at the bottom of your site.">?</a>)</label>
			<input name="copyright" type="text" id="copyright" value="<?= $s['copyright']; ?>" class="bfInputText bfChangeListen" />
		</li>
		<? if ($template != 'desperado' && $templateType != 'HTML5') { ?>
		<li>
			<label for="email_a_friend">Email a friend (<a href="javascript:;" class="bfToolTip" title="This determines whether you want the Email A Friend A Photo option added to your website.">?</a>)</label>
			<div class="uiButtonSet">
				<label for="email_a_friend_on" class="radioLabel">On</label>
				<input type="radio" name="email_a_friend" id="email_a_friend_on" value="1" class="bfChangeListen" <? if ($s['email_a_friend']=='1') { ?>checked="checked"<? } ?> />
				<label for="email_a_friend_off" class="radioLabel">Off</label>
				<input type="radio" name="email_a_friend" id="email_a_friend_off" value="0" class="bfChangeListen" <? if ($s['email_a_friend']=='0') { ?>checked="checked"<? } ?> />
			</div>
		</li>
		<? } ?>
		<!--
		<li>
			<label for="email_subj">Email subject (<a href="javascript:;" class="bfToolTip" title="This text will be the subject of the email when someone sends a picture from your website.">?</a>)</label><input name="email_subj" type="text" id="email_subj" value="<?= $s['email_subj']; ?>" class="bfInputText" />
		</li>
		<li>
			<label for="email_msg">Email message (<a href="javascript:;" class="bfToolTip" title="This text will be the message attached to the email when someone sends a picture from your website.">?</a>)</label>
			<textarea name="email_msg" type="text" id="email_msg" class="bfTextarea"><?= $s['email_msg']; ?></textarea>
		</li>
		
		<? if (isset($s['social_media'])) { ?>
		<li>
			<label for="social_media">Social Media Icons (<a href="javascript:;" class="bfToolTip" title="This section allows you to upload social media icons and link to your profiles. Upload icons (.png or .jpg) and enter the URL associated with the uploaded icon.">?</a>)</label>
			<textarea name="social_media" type="text" id="social_media" class="bfTextarea bfChangeListen" style="display:none;"><?= $s['social_media']; ?></textarea>
		</li>
		<div id="socialMediaIcons"> 
		<? $smediaArr = explode("\n", $s['social_media']); ?>  
		<? foreach ($smediaArr as $smi) {
			if ($smi!="") {
				$itemArr = explode(',',$smi);  
				$imgName = $itemArr[0];
				$imgPath = $_SESSION['user']['temp_url'] . '/gallery/original/' . $imgName;
				$imgURL = $itemArr[1];
				?>
				<li class="bfSortable socialItem"> 
					<div class="deleteButton"></div>
					<div class="smediaIcon">
						<img src="<?=$imgPath?>" title="<?=$imgName?>"/>
					</div> 
					<input name="socialURL" type="text" value="<?= $imgURL ?>" class="bfInputText bfChangeListen" />
		    	</li>
				<? $count++; ?>
			<? } ?>
		<? } ?> 
		</div> 
		<div style="clear:both;"></div> 
		<input name="socialfile" type="file" id="socialfile" size="10" />
		<div id="status-message"></div>
		<div id="social-queue"></div>
		
		<? } ?>
		<div class="tip_message">You can add facebook like and twitter share buttons to any of the four corners of your site. Because of the technologies facebook and twitter use, they can only be added to the corners and will not appear when in flash full screen mode.</div>
		<li>
			<label for="share_buttons">Share Buttons</label> 
			
			<input type="checkbox" name="facebook_button" id="facebook_button" value="1" class="bfCheckbox bfChangeListen" <? if ($shareSettings['facebook']==1) { ?>checked="checked"<? } ?> />
			<label for="facebook_button" class="checkboxLabel">Facebook</label>
			
			<input type="checkbox" name="twitter_button" id="twitter_button" value="1" class="bfCheckbox bfChangeListen" <? if ($shareSettings['twitter']==1) { ?>checked="checked"<? } ?> />
			<label for="twitter_button" class="checkboxLabel">Twitter</label>
			
		</li>
		<li>
			<label for="share_align">Share Alignment</label>
			<div class="uiButtonSet">
				<label for="share_align_TL" class="radioLabel">Top Left</label>
				<input name="share_align" type="radio" value="TL" id="share_align_TL" class="bfChangeListen" <? if ($shareSettings['share_align']=='TL') { ?>checked="checked"<? } ?> />
			
				<label for="share_align_TR" class="radioLabel">Top Right</label>
				<input name="share_align" type="radio" value="TR" id="share_align_TR" class="bfChangeListen" <? if ($shareSettings['share_align']=='TR') { ?>checked="checked"<? } ?> /> 
			
				<label for="share_align_BL" class="radioLabel">Bottom Left</label>
				<input name="share_align" type="radio" value="BL" id="share_align_BL" class="bfChangeListen" <? if ($shareSettings['share_align']=='BL') { ?>checked="checked"<? } ?> />
			
                   <label for="share_align_BR" class="radioLabel">Bottom Right</label>
				<input name="share_align" type="radio" value="BR" id="share_align_BR" class="bfChangeListen" <? if ($shareSettings['share_align']=='BR') { ?>checked="checked"<? } ?> />
			</div>
		</li>
		<li>
			<label for="share_url">Share URL (<a href="javascript:;" class="bfToolTip" title="Enter the desired URL to share and/or like. Typically this is your domain name (http://www.mydomain.com/).  Do not enter any button code generated by either Facebook or Twitter; button codes are imported automatically.">?</a>)</label>
			<input name="share_url" type="text" id="share_url" value="<?=$shareSettings['share_url']?>" class="bfInputText bfChangeListen"/>
		</li>
		-->    
	</ul>
</fieldset> 


<script type="text/javascript">
// <![CDATA[

function setMobile() {
	var mobile_name = $("#mdomain").find("option:selected").text();//.attr("selectedIndex");
	$("#mname").attr("value", mobile_name);
	/*
	var mobj = document.site_form.mdomain;
	var mName = mobj.options[mobj.selectedIndex].text; 
	document.site_form.mname.value = mName;
	*/
}

// ]]>
</script>