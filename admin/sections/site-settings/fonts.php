<?
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");
//
$s = get_settings();
$fvalues = unserialize($s['flash_vars']);
$fvars = split(',',get_property('FLASH_VARS')); 
// 

$currentTitleFont = ($s['title_font'] == '') ? 'Not set' : $s['title_font'];
$currentNavFont = ($s['navigation_font'] == '') ? 'Not set' : $s['navigation_font'];
$currentPageFont = ($s['page_font'] == '') ? 'Not set' : $s['page_font'];  

$fvars = split(',',get_property('FLASH_VARS'));
foreach($fvars as $var) {
	if ($var == 'title_font_modifier') {
		$check = true;
		$av = 0 + $fvalues[$var];
	}
	if ($var == 'nav_font_modifier') {
		$bv = 0 + $fvalues[$var];
	}
	if ($var == 'page_font_modifier') {
		$cv = 0 + $fvalues[$var];
	}
}

?>

<div class="contentHeader">
	<h1>Fonts -</h1>  <a href="https://vimeo.com/127730238" target="_blank"><img src="img/watch-this-video2.png" border="0" style="position:absolute; margin-left:115px; margin-top:-50px;" /></a>
</div>

<div class="tip_message">These custom settings are specific to your template. For details, hover over the question mark next to each setting, or view the support page for your site design. Need a custom font? We can install one or more for you for a small fee.</div>
<div id="editPane">
	<fieldset>
		<!-- <legend>Font Settings</legend> --> 
		<ul>
			
			<li class="divider"></li>
			<h2>Title Font</h2>
			<li>
				<label for="title_font">Font options</label>
				<select name="title_font" class="fontSelect bfChangeListen">
					<option><?= $currentTitleFont ?></option>
					<option value="<?= $currentTitleFont ?>">-----</option>
					<? echo get_font_selection('t'); ?>
				</select> 
			</li>
			<li class="fontPreview"> 
				<label>Preview</label>
				<div id="title_font_preview">
					The quick brown fox jumps over the lazy dog.
				</div>
			</li> 
			<? if ($check) { ?>
			<li>
				<label for="title_font_modifier">Font size modifier (<a href="javascript:;" class="bfToolTip" title="This setting is added to the default setting for all items using the title font.">?</a>)</label>
				<div class='bfSlider' title='-100,100,<?=$av?>'></div>
				<input type='text' class='bfInputText bfSliderLabel bfChangeListen' name='title_font_modifier' id='title_font_modifier' value='<?=$av?>' />
			</li>
			<? } ?>
			 
			<li class="divider"></li>
			<h2>Navigation Font</h2>
			<li><label for="navigation_font">Font options</label>
				<select name="navigation_font" class="fontSelect bfChangeListen">
					<option><?= $currentNavFont ?></option>
					<option value="<?= $currentNavFont ?>">-----</option>
					<? echo get_font_selection('n'); ?>
				</select>
			</li>
			<li class="fontPreview"> 
				<label>Preview</label>
				<div id="navigation_font_preview">
					The quick brown fox jumps over the lazy dog.
				</div>
			</li>
			<? if ($check) { ?> 
				
			<li>
				<label for="nav_font_modifier">Font size modifier (<a href="javascript:;" class="bfToolTip" title="This setting is added to the default setting for all items using the navigation font.">?</a>)</label>
				<div class='bfSlider' title='-100,100,<?=$bv?>'></div>
				<input type='text' class='bfInputText bfSliderLabel bfChangeListen' name='nav_font_modifier' id='nav_font_modifier' value='<?=$bv?>' />
			</li>
			<? } ?>
			 
			<li class="divider"></li>
			<h2>Page Font</h2>
			<li><label for="page_font">Font options</label>
				<select name="page_font" class="fontSelect bfChangeListen">
					<option><?= $currentPageFont ?></option>
					<option value="<?= $currentPageFont ?>">-----</option>
					<? echo get_font_selection('p'); ?>
				</select>
			</li> 
			<li class="fontPreview"> 
				<label>Preview</label>
				<div id="page_font_preview">
					The quick brown fox jumps over the lazy dog.
				</div>
			</li>
			<? if ($check) { ?>
			<li>
				<label for="page_font_modifier">Font size modifier (<a href="javascript:;" class="bfToolTip" title="This setting is added to the default setting for all items using the page font.">?</a>)</label>
				<div class='bfSlider' title='-100,100,<?=$cv?>'></div>
				<input type='text' class='bfInputText bfSliderLabel bfChangeListen' name='page_font_modifier' id='page_font_modifier' value='<?=$cv?>' />
			</li>
			<? } ?>   
			
			<li class="divider"></li>
			
		</ul>
	</fieldset>
</div> 
<? include_once('../../inc/fonts-include.php'); ?>
<script type="text/javascript">
// <![CDATA[

//
$(".fontSelect").change(function() {
	var font = $(this).val().split("&").join('[[ampersand]]');
	var id = $(this).attr("name") + "_preview"; 
	updateFontPreview('fonts/', font, id);
});  

function updateFontPreview(dir, font, divID) {
	$.log('updateFontPreview: '+font+', dir:'+dir+', div:'+divID); 
	var file = font + '.swf'; 
	if (templateType == "HTML5" || template == "desperado") {  
		$("#"+divID).css('fontFamily', font);
	} else {
		swfobject.embedSWF("font_preview.swf", divID, "400", "70", '9.0.24', null, {'cur_file':file, 'cur_dir':dir}, {'quality':'high','wmode':'transparent','allowScriptAccess':'always'},{},function(event) {}); 
	}
}

updateFontPreview('fonts/','<?=$currentTitleFont?>','title_font_preview');
updateFontPreview('fonts/','<?=$currentNavFont?>','navigation_font_preview');
updateFontPreview('fonts/','<?=$currentPageFont?>','page_font_preview'); 

// ]]>
</script>
