<?
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");
//
$s = get_settings();
$shareSettings = unserialize($s['share_settings']);

?>
<div class="contentHeader">
	<h1>Title &amp; Logo -</h1>  <a href="https://vimeo.com/127730632" target="_blank"><img src="img/watch-this-video2.png" border="0" style="position:absolute; margin-left:210px; margin-top:-50px;" /></a>
</div>

<div class="tip_message">These settings control what type of title will appear on your website. For more details, view the support page for your site design.</div>
<div id="editPane">
	<fieldset>
		<legend><strong class="sub-title">Choose Your Title</strong></legend>
		<ul>
			<li>
				<input name="title_mode" id="title_mode_profile" type="radio" value="PROFILE" class="settingsRadio bfChangeListen" <? if($s['title_mode'] == 'PROFILE') { ?>checked="checked"<? } ?> />
				<label for="title_mode_profile"><?= $s['business_name'] ?></label>
            </li>
			<li>
				<input name="title_mode" id="title_mode_custom" type="radio" value="CUSTOM" class="settingsRadio bfChangeListen" <? if($s['title_mode'] == 'CUSTOM') { ?>checked="checked"<? } ?> /> 
				<label for="title_mode_custom">the following text:</label>
				
				<input class="bfInputText bfChangeListen" name="site_title" type="text" id="site_title" value="<?= $s['site_title'] ?>" />
			</li>
			<li>
				<input name="title_mode" id="title_mode_logo" type="radio" value="LOGO" class="settingsRadio bfChangeListen" <? if($s['title_mode'] == 'LOGO') { ?>checked="checked"<? } ?> />
				<label for="title_mode_logo">my logo (<a href="javascript:;" class="bfToolTip"  title="You can upload a JPEG, PNG, or SWF file. Please check the support pages for the correct logo dimensions. Note: the dotted border that appears around uploaded logos on this page is not part of the logo file itself.">?</a>)</label>
			</li> 
			
			<!-- logo uploader -->
			<li> 
				<div id="logoHolder">
					<? if ($s['logo_file'] != '') {
					   	$ur = $_SESSION['user']['temp_url']; 
					   	if (strtolower(substr($s['logo_file'], -3)) != 'swf') {
 					?>
						<img src="<? echo($ur); ?>/images/<?= $s['logo_file'] ?>" border="0" id="logoFileImg"/>
					<? } else { ?>
						<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0">
							<param name="movie" value="<?= $ur ?>/images/<?= $logofile ?>" />
							<param name="quality" value="high" />
							<embed src="<?= $ur ?>/images/<?= $logofile ?>" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="<?= $w ?>" height="<?= $h ?>"></embed>
						</object>
					<? } ?> 
					<? } ?>
			 	</div>
				
				<input name="logofile" type="file" id="logofile" size="10" /> 
				<div id="status-message">
					
				</div> 
				<div id="logo-queue">
				</div>
			</li>
			<li>
				<input name="title_mode" id="title_mode_none"type="radio" value="NONE" class="settingsRadio bfChangeListen" <? if($s['title_mode'] == 'NONE') { ?>checked="checked"<? } ?> />
            	<label for="title_mode_none">no title</label>
			</li>
		</ul>
	</fieldset>
</div> 

<script type="text/javascript">
// <![CDATA[

// logo upload 
$("#logofile").uploadifive({
  'uploadScript'   : adminURL + '_bfadmin/file_upload.php', 
  'method'         : 'post',
  'formData'       : {'u':u,'p':p,'mode':'logo'},      
  'cancelImg'      : 'uploadifive-cancel.png',
  'multi'          : true,
  'auto'           : true,
  'fileExt'        : '*.jpg;*.jpeg;*.gif;*.png;*.swf;',
  'fileDesc'       : 'Image and Flash Files (.JPG, .JPEG, .GIF, .PNG, .SWF)',
  'queueID'        : 'logo-queue', 
  'buttonText'     : 'UPLOAD LOGO',
  'width'          : 125,
  'queueSizeLimit' : 1,
  'simUploadLimit' : 1,
  'removeCompleted': true,
  'onSelectOnce'   : function(event,data) {
      	//$('#status-message').text(data.filesSelected + 'uploading...');
		$('#status-message').text('uploading...');
    }, 
  'onUploadComplete'     : function(file, data) { 
	//	var fileParts = getNameAndExtension(fileObj['name']); 
	//	var logoFile = cleanfile(fileParts[0])+'.'+fileParts[1];
		var logoFile = file.name;
	 // 	$.log('fileName: '+cleanfile(fileParts[0])+'.'+fileParts[1]);
	//  	$.log('response: '+response); 
	  	var logohtml = "";
			logohtml += "<img src='"+tempURL+"images/"+logoFile+"' id='logoFileImg' />"; 
		$.log('logohtml: '+logohtml);
		$("#logoHolder").html(logohtml);  
		$("#title_mode_logo").attr("checked", "checked");
		//
		// save logo file name in db  
		showSave();
		data = new Object();
		data['logo_file'] = logoFile;
		$.ajax({
			//this is the php file that processes the data
			url: 'actions/site-settings/logo_file.save.php',	
			type: "POST",	
			data: data,		
			cache: false,
			success: function (res) {			
				if (res.substr(0,7) == 'success') {					 
					$.log(res);
					settingsChanged = false;
				} else {
					alert('Sorry, unexpected error. Please try again later.');
				}			
			}		
		});
		saveSection();
		//hideSave();
   	},
  'onAllComplete'  : function(event,data) {
		$.log("filesUploaded: "+data.filesUploaded);
		$.log("data.errors: "+data.errors);
		if (data.errors > 0) {
      		$('#status-message').text("There was an error with the upload.");
		} else {
			$('#status-message').text("");
		}
    }
});

// ]]>
</script>