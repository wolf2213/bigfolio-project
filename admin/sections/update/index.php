<?
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");
//
$s = get_settings();
$fvalues = unserialize($s['flash_vars']);
$fvars = split(',',get_property('FLASH_VARS'));

$galTypes = split(',',get_property('GALLERY_MODES'));
$hasMasonry = false;
$hasFullscreen = false;
foreach($galTypes as $gt) {
	if ($gt == "masonry") {
		$hasMasonry = true;
	} else if ($gt == "fullscreen") {
		$hasFullscreen = true;
	}
}
$template = get_property('template'); 
if (file_exists("../../updates/".$template."/_update.php")) {
	include("../../updates/".$template."/_update.php");
}
$curVersion = intval(get_property('RUBIX_VERSION'));
if ($curVersion == "") { $curVersion = 1; }
//stristr($template,"rubix")
?>

<div class="contentHeader">
	<h1>Update</h1>
</div>

<? //echo "update version: ".$update['version']."<br>"; ?>
<? //echo "update version: ".$curVersion."<br>"; ?>
<? if ($update['version'] > $curVersion) { ?>
<div class="tip_message">An update is available for your site. Please review the information below.</div>
<div id="editPane">
	<fieldset>
		<div class="tip_message">
			This update includes the following updates:
			<? //echo count($update['version_notes']); ?>
			<ul style="list-style-type:disc; padding:20px; width:auto;">
			<?
			for ($i=$curVersion+1; $i<=count($update['version_notes']); $i++) {
				$vu = explode("\n", $update['version_notes'][$i]);
				foreach($vu as $v) {
					if ($v != "") {
						echo "<li>".$v."</li>";
					}
				}
			}
			?>
			</ul>
			<span style="color:#ff0000;">Please note: applying this update will update your site to the most recent version. If any custom work has been done to your site, this update will overwrite those customizations. If you have any questions or concerns, please submit a ticket <a href="http://support.bigfolio.com">here</a>.</span>
		</div>

		<button id="updateTemplate">Update</button>
	</fieldset>
</div>
<script type="text/javascript">
// <![CDATA[


$("#updateTemplate").click(function() {
	showSave();
	$.ajax({
      type: 'POST',
      url:'updates/<?=$template?>/_update.php',
      data: ({'update':"1"})
    }).done(function( msg ) {
	  	$.log("update: "+msg);
		hideSave();
		if (msg.indexOf("error")==-1) {
			location.reload();
		} else {
			$("#editPane .tip_message").html('<span style="color:#ff0000;">'+msg.split("\n").join("<br>")+'<br>Please contact support to complete your update.</span>');
		}
	})
});


// ]]>
</script>
<? } else { ?>
<div class="tip_message">Your site is currently up to date.</div>
<? } ?>
