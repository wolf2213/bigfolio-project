<?
//
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../../inc/db.inc.php");
include_once("../../../inc/functions.inc.php");
//
$s = get_settings();
$template = get_property('template'); 
$fvalues = unserialize($s['flash_vars']);
//
$galData = explode("-", $_GET['val']);  // separate val
for ($i=0; $i<count($galData); $i++) {
	if (stristr($galData[$i], ".php")) {
		$galData[$i] = str_replace(".php", "", $galData[$i]); // strip .php from all instances
	}
}
//substr($_GET['val'], 0, -4)
//
$gid = $galData[0]; 
if (!isset($galData[1])) { 
	if (isset($_SESSION['gallery_view_type'])) {
		$type = $_SESSION['gallery_view_type'];
	} else {
		$type = 'small'; 
		$thumbSize = 'thumb';
	}
} else {
	$type = $galData[1]; 
}
if ($type == 'large') {
	$thumbSize = 'small';
	$viewLabel = 'Large thumbnails';
} else if ($type == 'small') {
	$thumbSize = 'thumb'; 
	$viewLabel = 'Small thumbnails';
} else if ($type == 'list') {
	$thumbSize = 'medium';
	$viewLabel = 'List';
} 

//
// store view type for future visits
$_SESSION['gallery_view_type'] = $type;

//
$images = get_gallery_images($gid);
$gallery = get_gallery($gid);
//
function hasProtectedGalleries() {
	return true;
}
//                           
// get cats used for move to menu
$cats = get_all_categories();
//
?>
<div class="content_inner galleryContent">
	<div class="contentHeader">
		<h1>Image Gallery</h1>
	</div>  
	<div class="tip_message">
	    Use this area to add images, reorder images, or change other settings associated with this gallery.
	</div>
	<div class="gallerySettings"> 
		<ul>
			<input type="hidden" name="galID" value="<?=$gallery['gallery_id']?>" />
			<li>
				<label for="galleryName">Gallery name</label>
				<input type="text" class="bfInputText bfChangeListen" name="galleryName" value="<?=$gallery['gallery_name']?>" />
			</li>
			<?if(isset($gallery['subtitle'])){?>
			<li>
				<label for="subtitle">Subtitle</label>
				<input type="text" class="bfInputText bfChangeListen" name="subtitle" value="<?=$gallery['subtitle']?>" />
			</li>
			<?}?>
			<div id="seoSettings" <?if(!isset($gallery['meta_title'])){?>style="display:none;"<?}?>>
				<li class="divider"></li>
				<div id="metaSettings"> 
					<? while (list($key, $val) = each($gallery)) { ?>
						<? if (stristr($key, "meta_")) { ?>
						<li class="metaSetting">
							<label for="<?=$key?>"><?=ucfirst(str_replace("_", " ", $key))?></label>                                                             
							<input type="text" class="bfInputText bfChangeListen" name="<?=$key?>" value="<?=$val?>" />
						</li> 
						<? } ?>
					<? } ?>
				</div>
				<li class="divider"></li>
			</div>
			<li>
				<label for="galleryHidden">Hide this gallery</label>                                                             
				<div class="uiButtonSet">
					<label for="galleryHiddenYes" class="radioLabel">Yes</label>
					<input name="galleryHidden" id="galleryHiddenYes" type="radio" value="1" class="bfChangeListen" <? if ($gallery['gallery_hidden']==1) { ?>checked="checked"<? } ?> />
					<label for="galleryHiddenNo" class="radioLabel">No</label>
					<input name="galleryHidden" id="galleryHiddenNo" type="radio" value="0" class="bfChangeListen" <? if ($gallery['gallery_hidden']==0) { ?>checked="checked"<? } ?> />
				</div>
			</li>
			<li>
			<? if (hasProtectedGalleries()) { ?>
				<label for="galleryPassword">Password for this gallery</label>
				<input type="text" class="bfInputText bfChangeListen" name="galleryPassword" value="<?=$gallery['password']?>" />
			<? } ?> 
			</li>
			<? if ($template != "rubix" || ($fvalues['gallery_mode'] == 'large')) { ?>
			<li>
				<label for="globalAlign">Global alignment</label>                                                             
				<div class="uiButtonSet">
					<label for="globalAlignLeft" class="radioLabel">Left</label>
					<input name="globalAlign" id="globalAlignLeft" type="radio" value="left" class="bfChangeListen" <? if ($gallery['gallery_align']=="left") { ?>checked="checked"<? } ?> />
					<label for="globalAlignCenter" class="radioLabel">Center</label>
					<input name="globalAlign" id="globalAlignCenter" type="radio" value="center" class="bfChangeListen" <? if ($gallery['gallery_align']=="center") { ?>checked="checked"<? } ?> />
					<label for="globalAlignRight" class="radioLabel">Right</label>
					<input name="globalAlign" id="globalAlignRight" type="radio" value="right" class="bfChangeListen" <? if ($gallery['gallery_align']=="right") { ?>checked="checked"<? } ?> />
					<label for="globalAlignNone" class="radioLabel">None</label>
					<input name="globalAlign" id="globalAlignNone" type="radio" value="" class="bfChangeListen" <? if ($gallery['gallery_align']=="") { ?>checked="checked"<? } ?> />
				</div>
			</li>
			<? } ?>
			<!--
			<li>
				<button id="otherToggle">Advanced Settings</button>
			</li>
			-->
			<li class="divider"></li>
		</ul> 
	</div>
	
	<div id="otherSettings" style="display:block;"> 
		<ul>
		<? $gallery = get_gallery($gid); ?>
		<? while (list($key, $val) = each($gallery)) { ?>
			<? if (stristr($key, "show_")) { ?>
			<li class="otherSetting">
				<label for="<?=$key?>"><?=ucfirst(str_replace("_", " ", $key))?></label>                                                             
				<div class="uiButtonSet">
					<label for="<?=$key?>Yes" class="radioLabel">Yes</label>
					<input name="<?=$key?>" id="<?=$key?>Yes" type="radio" value="1" class="bfChangeListen" <? if ($val==1) { ?>checked="checked"<? } ?> />
					<label for="<?=$key?>No" class="radioLabel">No</label>
					<input name="<?=$key?>" id="<?=$key?>No" type="radio" value="0" class="bfChangeListen" <? if ($val==0) { ?>checked="checked"<? } ?> />
				</div>
			</li> 
			<? } ?>
		<? } ?> 
		<li class="divider"></li> 
		</ul>
	</div>
	
	<div id="imageEditor">    
		<div class="deleteButton"></div>
		<h2>Image Editor</h2>
		<div id="imageEditorApp">
		</div>
		<ul>
			<li class="divider"></li>
		</ul>
	</div>
	   
	<div style="clear:both;"></div>
	



<BR><BR>
<!--<input class="galleryUploader" type="file" name="file_upload" />

<br /> -->

	 
<div class="galleryUploader">
		<a href="javascript:;" class="bfToolTip">


		<input name="galleryfiles" type="file" class="galleryfiles" id="gal<?=$gallery['gallery_id']?>" size="10" />
		</a> 
		<div id="status-message"></div>
		<div id="gallery-queue"></div>
	</div>
	
	<div class="galleryImages">
		<? foreach($images as $img) { 
			$prettyName = $img['image_file'];
		   	$html  = "";
         	$html .= '<div class="thumbItem bfSortable '.$type.'Item" id="img'.$img['image_id'].'">';
			$html .= '<div class="deleteButton"></div>';
			$html .= '<div style="clear:both;"></div>';
			$html .= '<div class="thumbFileLabel '.$type.'Label">'.$prettyName.'</div>';
			
			$html .= '<div class="thumbHolder '.$type.'Thumb">';
			$html .= '<a rel="galImages" href="'.$_SESSION['user']['temp_url'].'gallery/original/'.$img['image_file'].'" title="'.str_replace('"', '&quot;', $img['image_caption']).'">';
			$html .= '<img src="'.$_SESSION['user']['temp_url'].'gallery/'.$thumbSize.'/'.$img['image_file'].'" title="'.$img['image_file'].'"/>';
			$html .= '</a>';
			$html .= '</div>';
			$html .= '<div class="'.$type.'ImageCaption imgCap">'; 
			$html .= '<textarea class="bfChangeListen" name="caption'.$img['image_id'].'">'.$img['image_caption'].'</textarea>';
			$html .= '</div>';
			$html .= '<input type="checkbox" class="imgCheck"/>';
			$html .= '<div class="captionButton"></div>';
			//
			if ($template != "rubix" || ($fvalues['gallery_mode'] == 'large')) {
				$html .= '<select name="align'.$img['image_id'].'" class="imageAlign">';
				//
					$html .= '<option value="center"';
					if ($img['align'] == 'center') { 
						$html .= 'selected=selected';
					}
					$html .= '>center</option>'; 
					//
					$html .= '<option value="left"';
					if ($img['align'] == 'left') { 
						$html .= 'selected=selected';
					}
					$html .= '>left</option>';
					//
					$html .= '<option value="right"';
					if ($img['align'] == 'right') { 
						$html .= 'selected=selected';
					}
					$html .= '>right</option>'; 
				//
				$html .= '</select>';
			} 
			//
			$html .= '</div>'; 
			echo $html; 
		} ?> 
	</div>
	<div style="clear:both;"></div>
</div>

<div class="actionsPanel">
	<div class="icon">
	</div>
	<div class="inner">
		<ul>
			<li> 
				<label for="galleryView">View mode</label>
				<select name="viewMode" class="smallSelect" id="viewMode">
					<option><?=$viewLabel?></option>
					<option>-----------------</option>
					<option value="small">Small Thumbnails</option>
					<option value="large">Large Thumbnails</option>
					<option value="list">List</option>
				</select>
			</li>
			
			<li>
				<label for="bulkActions">Bulk actions</label>
		   		<select name="bulkActions" class="smallSelect" id="bulkActions">
					<option>Select One</option>
					<option>-----------------</option>
					<option value="delete">Delete Selected</option>
					<option value="combine">Edit/Combine Selected</option>
				</select> 
			</li> 
			
			<li>
				<label for="moveTo">Move to</label>
				<select name="moveTo" class="smallSelect" id="moveTo">
					<option>Select One</option>
					<option>-----------------</option>
					<? foreach($cats as $c) { ?>
						<? $gals = get_galleries($c['category_id']); ?>
						<? foreach($gals as $g) { ?>
							<? if ($gallery['gallery_id'] != $g['gallery_id']) { ?>
							<option value="<?=$g['gallery_id'];?>"><?=$c['category_name']?>/<?=$g['gallery_name']?></option> 
							<? } ?>
						<? } ?>
					<? } ?>
				</select>
			</li>
			
			<li>
			   	<label for="selectActions">Select</label>
				<button id="selectAll">All</button>
				<button id="selectNone">None</button>
			</li>
			
			<li>
				<label for="sortByFileName">Sort by File Name</label>
				<button id="sortByFileName">Sort</button>
			</li>
			
		</ul>
	</div>
</div>

<script type="text/javascript">
// <![CDATA[  

var type = "<?=$type?>";
var thumbSize = "<?=$thumbSize?>";
var galID = "<?=$gallery['gallery_id']?>"; 
var lastThumbChecked; 

if ($("#metaSettings").find('.metaSetting').length > 0) {
	$("#seoSettings").show();
}


$(document).ready(function() {
	$("#imageEditor").hide();
	
	<? if ($gallery['gallery_align'] != "") { ?>
		$(".imageAlign").hide();             
	<? } ?>
});

$("#globalAlignLeft").click(hideImageAlign);
$("#globalAlignCenter").click(hideImageAlign);
$("#globalAlignRight").click(hideImageAlign);
$("#globalAlignNone").click(showImageAlign);

function hideImageAlign() {
	$(".imageAlign").hide();
}
function showImageAlign() {
	$(".imageAlign").show();
}

$("#otherToggle").click(function() {
 	$("#otherSettings").slideToggle('fast');
}); 
if ($("#otherSettings").find('.otherSetting').length > 0) {
	$("#otherToggle").show();
}

function galleryFunctionality() {  
	
	
	$(".captionButton").unbind("click");
	$(".captionButton").click(function() {   
		$.log('caption display: '+$(this).parent().find(".imgCap").css("display"));
		if ($(this).parent().find(".imgCap").css("display") == "none") {
			$(this).parent().find(".imgCap").show();
			//$(this).parent().find(".thumbHolder").hide();
		} else {
			$(this).parent().find(".imgCap").hide();
			//$(this).parent().find(".thumbHolder").show();
		}
	});
	 
	
	$(".thumbItem").find(".deleteButton").unbind("click"); 
	$(".thumbItem").find(".deleteButton").click(function() {
		var save = confirm("Are you sure you want to delete this image from this gallery? This cannot be undone.");
		if (save) { 
			removeImage($(this).parent()); 
		}
	});
	
	<? if ($type == 'list') { ?>
	$(".thumbFileLabel").unbind("click");
	$(".thumbFileLabel").click(function() {
    	$(this).parent().find(".thumbHolder").slideToggle('fast');
	});
	<? } ?>
	
	//
	// handle check box shift functionality
	$(".imgCheck").unbind("click");
	$(".imgCheck").click(function() {
		var parentID = $(this).parent().attr("id");
		var isChecked = $(this).attr("checked"); 
		//
		// check if shift key is down  
		if (!event.shiftKey) {
			lastThumbChecked = $(this).parent();
			return;
		}
		//
		// get index
		var thisIndex = $(".thumbItem").index($(this).parent());
		var lastIndex = $(".thumbItem").index(lastThumbChecked);
		//
		// determine if we should move up the dom or down
		if (thisIndex > lastIndex) {
			var start = lastIndex;
			var end = thisIndex;
		} else {
			var start = thisIndex;
			var end = lastIndex;
		} 
		
		for (var i=start; i<=end; i++) { 
			if (isChecked) {
				$(".thumbItem:eq("+i+")").find(".imgCheck").attr("checked", "checked");
			} else {
				$(".thumbItem:eq("+i+")").find(".imgCheck").removeAttr("checked");
			}
		}
		 
	});
	
}
galleryFunctionality();  

$(".galleryImages").sortable({
	update: function(event, ui) { 
		var d = new Object(); 
		d['imgList'] = new Array();
		$(this).find(".thumbItem").each(function() {
			d['imgList'].push($(this).attr("id").substr(3)); // pushes imgNNN minus the img
		});
		$.log("d['imgList']: "+d['imgList']);

		$.ajax({
  			type: 'POST',
  			url:'actions/images/update-gallery-images-order.php',
  			data: d
		}); 
	}
});




//
// gallery image upload 
$(".galleryfiles").uploadifive({
//$(".galleryfiles").uploadify({
  'uploadScript'   : adminURL + '_bfadmin/file_upload.php', 
  'method'         : 'post',  
  'formData'       : {'u':u,'p':p,'mode':'gallery'},  
  'cancelImg'      : 'uploadifive-cancel.png',
  'multi'          : true,
  'auto'           : true,
  'fileExt'        : '*.jpg;*.jpeg;*.png;*.gif;',
  'fileDesc'       : 'Image Files (.JPG, .JPEG, .PNG, .GIF)',
  'queueID'        : 'gallery-queue', 
  'buttonText'     : 'UPLOAD IMAGES',
  'width'          : 140,
  'queueSizeLimit' : 100,
  'simUploadLimit' : 1,
  'removeCompleted': true,
  'onSelectOnce'   : function(event,data) {
      	//$('#status-message').text(data.filesSelected + 'uploading...');
		$('#status-message').text('uploading...');
    }, 
  'onUploadComplete'     : function(file, data) { 
		//var fileParts = getNameAndExtension(fileObj['name']); 
		// var imageFile = cleanfile(fileParts[0])+'.'+fileParts[1];
		var imageFile = file.name;
	  	//$.log('fileName: '+cleanfile(fileParts[0])+'.'+fileParts[1]);
	  //	$.log('response: '+response); 
		//
		addImageToGallery(imageFile);
		//
   	},
  'onAllComplete'  : function(event,data) {
		$.log("filesUploaded: "+data.filesUploaded);
		$.log("data.errors: "+data.errors);
		if (data.errors > 0) {
      		$('#status-message').text("There was an error with the upload.");
		} else {
			$('#status-message').text("");
		}
    }
});

function addImageToGallery(imageFile) {
	$.ajax({
      type: 'POST',
      url:'actions/images/add-image-to-gallery.php',
      data: ({'galID':galID, 'imageFile':imageFile})
    }).done(function( msg ) {
	  	$.log( "msg: " + msg );
	 	if (msg.substr(0,7) == "SUCCESS") { 
			//
			var imgID = msg.substr(8);
			//
		    var html = "";
			html += '<div class="thumbItem bfSortable '+type+'Item" id="img'+imgID+'">';
			html += '<div class="deleteButton"></div>';
			html += '<div style="clear:both;"></div>';
			html += '<div class="thumbFileLabel '+type+'Label">'+imageFile+'</div>';
			html += '<div class="thumbHolder '+type+'Thumb">';
			html += '<a rel="galImages" href="'+tempURL+'gallery/original/'+imageFile+'">';
			html += '<img src="'+tempURL+'gallery/'+thumbSize+'/'+imageFile+'" title="'+imageFile+'"/>';
			html += '</a>';
			html += '</div>'; 
			html += '<div class="'+type+'ImageCaption imgCap">'; 
			html += '<textarea class="bfChangeListen" name="caption'+imgID+'"></textarea>';
			html += '</div>';
			html += '<input type="checkbox" class="imgCheck"/>';
			html += '<div class="captionButton"></div>';
			//
			html += '<select name="align'+imgID+'" class="imageAlign">';
			html += '<option value="center" selected=selected>center</option>';
			html += '<option value="left">left</option>';
			html += '<option value="right">right</option>';  
			html += '</select>';
			//
			html += '</div>';
			//
			$(".galleryImages").append(html);
			handleUI();
			galleryFunctionality();
		} else if (msg.substr(0,5) == "QUOTA") {
			var quota = msg.substr(6); // msg returns QUOTA:N where N is the number allowed
			alert('The number of images allowed in a gallery for this site ('+quota+') has been met. To add more images, some existing images must be removed first.');
		} else {
			alert("There was a problem adding the image "+imageFile+" to this gallery. Please try again later. If problems persist, please contact support.")
		}
		
	});
}

$(".viewModeButton").click(function() {
   	changeGalleryView($(this).val());
});
$("#viewMode").change(function() {
	changeGalleryView($(this).val());
});

function removeImage(thumb) { 
	var imageFile = thumb.find("img").attr("title");
   	var imageID = thumb.attr('id').substr(3);
	$.ajax({
      type: 'POST',
      url:'actions/images/remove-image-from-gallery.php',
      data: ({'imageID':imageID})
    }).done(function( msg ) {
	  	$.log( "msg: "+msg);
		if (msg.substr(0,7) == 'SUCCESS') {
			thumb.fadeOut("fast", function() {
			   thumb.remove(); 
			});
		
			//
			// now check for duplicates
			$.ajax({
	          type: 'POST',
	          url:'actions/images/check-for-duplicate-images.php',
	          data: ({'imageFile':imageFile})
	        }).done(function( msg ) {
			  	$.log( "duplicates?: "+msg);  
				//
				// if no duplicates (0) then remove the file
				if (msg == '0') { 
					$.ajax({
			          type: 'POST',
			          url:adminURL + '_bfadmin/file_remove.php',
			          data: ({'u':u,'p':p, 'mode':'gallery', 'file':imageFile})
			        }).done(function( msg ) {
					  $.log( "msg: " + msg );
					});
				} 
			});
			//
		} else {
			alert("There was an error removing the image from this gallery.");
		}
	});
}                         

function changeGalleryView(type) {
	var v = "#";
    for (var i=0; i<paths.length; i++) { 
	 	if (paths[i] != "") {
			if (i == paths.length - 1) {
				var curPath = paths[i].split('-');
				v += "/"+curPath[0] + '-' + type;
			} else {
				v += "/"+paths[i];
			} 
		}
	}
	location.href = v;
}

$("#bulkActions").change(function() { 
	
   	if ($(this).val() == 'delete') {
		//
	    var save = confirm("Are you sure you want to delete these images from this gallery? This cannot be undone.");
		if (save) { 
			$(".thumbItem").each(function() {
				if ($(this).find(".imgCheck").attr("checked") == "checked") {
				   removeImage($(this)); 
				}
			});
		}
		
	} else if ($(this).val() == 'combine'){
		// 
		var imagesArr = new Array();
		$(".thumbItem").each(function() {
			if ($(this).find(".imgCheck").attr("checked") == "checked") {
			 	imagesArr.push($(this).find("img").attr("title"));
			}
		}); 
		$.log("imagesArr: "+imagesArr);
		if (imagesArr.length <= 4) {               
			if ($("#imageEditor").css("display") == "none") {
				$("#imageEditor").slideToggle("fast");           
			}
			$.scrollTo($("#imageEditor").offset().top - 50, 'slow');
			var divID = "imageEditorApp";
			swfobject.embedSWF("img-editor/imageEditorApp.swf", divID, "100%", "100%", '9.0.24', null, {'imagesArr':imagesArr.join(','), "u":u, "p":p, "basePath":serverURL+"gallery/original/", "adminURL":adminURL}, {'quality':'high','wmode':'transparent','allowScriptAccess':'sameDomain'},{},function(event) {}); 
			
		} else {
			alert("Please select 4 or fewer images to combine.")
		}
		//
	}
	
	// reset selection
	$(this).val("Select One");
});

$("#moveTo").change(function() {
	$.log("Add to gallery: "+$(this).val()+', '+Number($(this).val()).toString()); 
	if (Number($(this).val()).toString() != "NaN") { 
		var newGalID = $(this).val(); 
		var count = 0; 
		var total = 0;
		//
		$(".thumbItem").each(function() {
			if ($(this).find(".imgCheck").attr("checked") == "checked") {
				total++; 
			}
		});
		//
		$(".thumbItem").each(function() {
			if ($(this).find(".imgCheck").attr("checked") == "checked") {
				var imageFile = $(this).find("img").attr("title"); 
				var imageID = $(this).attr("id").substr(3); // imgXXX
				$.log("imageFile: "+imageFile);
			   	$.ajax({
			      type: 'POST',
			      url:'actions/images/move-image-to-gallery.php',
			      data: ({'curGalID':galID, 'newGalID':newGalID, 'imageFile':imageFile, 'imageID':imageID})
			    }).done(function( msg ) {
				 	$.log('move msg: '+msg);
					count++;  
					$.log("count:"+count+", total:"+total);
                   	if (count == total) { 
 						hideSave();
		            	location.href = "#/images/galleries/"+newGalID; 
				   	}	
				}); 
			}
		});
		//
		if (total==0) {  
			alert('Please select images to add.');
		} else {
			showSave();
		}
		// 
	}
});

$("#selectAll").click(function() {
	$(".imgCheck").attr("checked","checked");
});
$("#selectNone").click(function() {
	$(".imgCheck").removeAttr("checked");
});

$(".actionsPanel").find(".icon").click(function() {
	if ($(".actionsPanel").hasClass("showing")) {
		$(".actionsPanel").removeClass("showing");
		$(".actionsPanel").animate({
			"marginLeft":2
		}, 'fast');
	} else { 
		$(".actionsPanel").addClass("showing");
		$(".actionsPanel").animate({
			"marginLeft":-200
		}, 'fast');
	}
});

$("#imageEditor").find(".deleteButton").click(function() { 
	var conf = confirm("Are you sure you want to close this editor? All data you're currently working on in the editor will be lost.");
	if (conf) {
		if ($("#imageEditor").css("display") != "none") {
			$("#imageEditor").slideToggle("slow");           
		}
	}
});

$.ajax({
  type: 'POST',
  url:adminURL + '_bfadmin/write_policy_file.php',
  data: ({'u':u,'p':p})
}).done(function( msg ) {
  $.log( "crossdomain?: " + msg );
});

function editorFileSaved(fileName) {
	$.log("editorFile: "+fileName);   
   	if ($("#imageEditor").css("display") != "none") { 
		$("#imageEditor").slideToggle("slow", function() {
			$("#imageEditorApp").html("");
		});
	}
    //
	var fileParts = getNameAndExtension(fileName); 
	var imageFile = cleanfile(fileParts[0])+'.'+fileParts[1];
  	$.log('fileName: '+cleanfile(fileParts[0])+'.'+fileParts[1]); 
	//
	addImageToGallery(imageFile);
	// 
	$.scrollTo($(".galleryUploader").offset().top, 'slow');
	//
}

$("#sortByFileName").click(function() {
	var save = confirm("Are you sure you want to reorder all images in this gallery by filename? This cannot be undone.");
	if (save) { 
		showSave();
		$.ajax({
	      type: 'POST',
	      url:'actions/images/reorder-gallery-by-filename.php',
	      data: ({'galID':galID})
	    }).done(function( msg ) {
			$.log("msg: "+msg);
			hideSave();
			location.reload();
		})
	}
});

// ]]>
</script>