<?
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");
//
$s = get_settings();

//
// get font files from remote server 
$postArr = array();
$postArr['u'] = $_SESSION['user']['email'];
$postArr['p'] = $_SESSION['user']['password'];
$postArr['mode'] = 'intro';
$url = SERVER . '_bfadmin/files_list.php'; // 
$remoteFiles = curl_post($url, $postArr, array()); 
$remoteFiles = json_decode($remoteFiles); 


?>
<div class="contentHeader">
	<h1>Home / Intro -</h1>  <a href="https://vimeo.com/127730425" target="_blank"><img src="img/watch-this-video2.png" border="0" style="position:absolute; margin-left:210px; margin-top:-50px;" /></a>
</div>

<div class="tip_message">This area allows you to upload 1 or more images for your home page / introductory slideshow. If only one image is uploaded no slideshow will occur. If more than one image is uploaded, your site will cycle through the images at the designated slideshow speed in <a href='#/site-settings/template-settings'>Template Settings</a>. Drag and drop images to reorder.</div>
<div id="editPane">
	<fieldset>
		<div id="homeSlideshowImages">
			<!-- intro images here -->
			<?
			foreach($remoteFiles as $file) {
				if (substr($file, 2, 2) == '--') {
					$prettyName = substr($file, 4);
				} else {
					$prettyName = $file;
				}
				$html  = "";
             	$html .= '<div class="thumbItem bfSortable">';
				$html .= '<div class="deleteButton"></div>';
				$html .= '<div style="clear:both;"></div>';
				$html .= '<div class="thumbFileLabel">'.$prettyName.'</div>';
				$html .= '<div class="thumbHolder">';
				$html .= '<img src="'.$_SESSION['user']['temp_url'].'extra/intro_thumbnails/'.$file.'" title="'.$file.'"/>';
				$html .= '</div>';
				$html .= '</div>'; 
				echo $html;
			}
			?>
	 	</div>
		<div style="clear:both;"></div>
		<a href="javascript:;" class="bfToolTip">
		<input name="introfiles" type="file" id="introfiles" size="10" />
		</a> 
		<div id="status-message"></div>
		<div id="intro-queue"></div>  
	</fieldset>
</div> 

<script type="text/javascript">
// <![CDATA[

//
// intro uploader 
$("#introfiles").uploadifive({
  'uploadScript'   : adminURL + '_bfadmin/file_upload.php', 
  'method'         : 'post',
  'formData'       : {'u':u,'p':p,'mode':'intro'},      
  'cancelImg'      : 'uploadifive-cancel.png',
  'multi'          : true,
  'auto'           : true,
  'fileExt'        : '*.jpg;',
  'fileDesc'       : 'JPEG Files (.JPG,)',
  'queueID'        : 'intro-queue', 
  'buttonText'     : 'UPLOAD IMAGES',
  'width'          : 135,
  'queueSizeLimit' : 25,
  'simUploadLimit' : 1,
  'removeCompleted': true,
  'onSelectOnce'   : function(event,data) {
      	//$('#status-message').text(data.filesSelected + 'uploading...');
		$('#status-message').text('uploading...');
    }, 
  'onUploadComplete'     : function(file, data) { 
		//var fileParts = getNameAndExtension(fileObj['name']); 
		//var imageFile = cleanfile(fileParts[0])+'.'+fileParts[1];
		var imageFile = file.name;
	  //	$.log('fileName: '+cleanfile(fileParts[0])+'.'+fileParts[1]);
	  //	$.log('response: '+response); 
		//
		var html = "";
		html += '<div class="thumbItem bfSortable">';
		html += '<div class="deleteButton"></div>';
		html += '<div style="clear:both;"></div>';
		html += '<div class="thumbFileLabel">'+imageFile+'</div>';
		html += '<div class="thumbHolder">';
		html += '<img src="'+tempURL+'extra/intro_thumbnails/'+imageFile+'" title="'+imageFile+'"/>';
		html += '</div>';
		html += '</div>';
		//
		$("#homeSlideshowImages").append(html);
		introFunctionality();
		//
	  	
   	},
  'onAllComplete'  : function(event,data) {
		$.log("filesUploaded: "+data.filesUploaded);
		$.log("data.errors: "+data.errors);
		if (data.errors > 0) {
      		$('#status-message').text("There was an error with the upload.");
		} else {
			$('#status-message').text("");
		}
    }
}); 

function introFunctionality() {
	$("#homeSlideshowImages").sortable(
		{
		    update: function(event, ui) { 
				updateIntroOrder();
      		}
		}
	); 
	$("#homeSlideshowImages").find(".deleteButton").unbind("click");
	$("#homeSlideshowImages").find(".deleteButton").click(function() { 
			//
			var save = confirm("Are you sure you want to delete this file? This cannot be undone");
			if (save) {
				//
			  	var fileName = $(this).parent().find('img').attr("title");
			    $.ajax({
		          type: 'POST',
		          url:adminURL + '_bfadmin/file_remove.php',
		          data: ({'u':u,'p':p, 'mode':'intro', 'file':fileName})
		        }).done(function( msg ) {
				  $.log( "msg: " + msg );
				});
				//
				var thumb = $(this).parent();
				thumb.fadeOut('fast', function() {
				    thumb.remove();
				});
			}
			// 
	});
	function updateIntroOrder() {
		var imagesArr = new Array();
		$('#homeSlideshowImages').find(".thumbItem").find("img").each(function(index, value) {
			imagesArr.push($(this).attr("title"));
			var fileName = $(this).attr("title");
			if (fileName.substr(2,2) == '--') {
				if (index <= 9) {
					fileName = '0' + index + '--' + fileName.substr(4);
				} else {
					fileName = index + '--' + fileName.substr(4);
				}
			} else {
				if (index <= 9) {
					fileName = '0' + index + '--' + fileName;
				} else {
					fileName = index + '--' + fileName;
				}
			}                                      
			$(this).attr("title", fileName);
		}); 
		var imagesStr = imagesArr.join(',');
		$.ajax({
          type: 'POST',
          url:adminURL + '_bfadmin/intro_order.php',
          data: ({'u':u,'p':p, 'intro':imagesArr})
        }).done(function( msg ) {
		  $.log( "msg: " + msg );
		});
	}
	$("#homeSlideshowImages").disableSelection();
}

introFunctionality();  

// ]]>
</script>

