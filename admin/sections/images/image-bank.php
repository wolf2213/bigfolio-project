<?
//
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");
//
//substr($_GET['val'], 0, -4)
//
if (!isset($_GET['viewMode']) || $_GET['viewMode'] == "") { 
	if (isset($_SESSION['gallery_view_type'])) {
		$type = $_SESSION['gallery_view_type'];
	} else {
		$type = 'small'; 
		$thumbSize = 'thumb';
	}
} else {
	$type = $_GET['viewMode']; 
}
if ($type == 'large') {
	$thumbSize = 'small';
	$viewLabel = 'Large thumbnails';
} else if ($type == 'small') {
	$thumbSize = 'thumb'; 
	$viewLabel = 'Small thumbnails';
} else if ($type == 'list') {
	$thumbSize = 'medium';
	$viewLabel = 'List';
}

$cats = get_all_categories(); 

//
// store view type for future visits
$_SESSION['gallery_view_type'] = $type;

// 
//
// get font files from remote server 
$postArr = array();
$postArr['u'] = $_SESSION['user']['email'];
$postArr['p'] = $_SESSION['user']['password'];
$postArr['mode'] = 'gallery';
$url = SERVER . '_bfadmin/files_list.php'; // 
$remoteFiles = curl_post($url, $postArr, array()); 
$images = json_decode($remoteFiles); 
//sort($images, SORT_STRING); 
//
?>
<div class="content_inner imageBank">
	<div class="contentHeader">
		<h1>Image Bank -</h1>  <a href="https://vimeo.com/146453152" target="_blank"><img src="img/watch-this-video2.png" border="0" style="position:absolute; margin-left:210px; margin-top:-50px;" /></a>
	</div>  
	<div class="tip_message">
	    This area stores all images associated with all galleries. Use the uploader here to add images to the image bank or overwrite existing images. Please note that changes to the image bank affects all galleries; If an image is assigned to one gallery or multiple galleries, any changes to that image (update or delete) will affect all galleries where that image is used.
	</div>
	
	<div id="imageEditor">    
		<div class="deleteButton"></div>
		<h2>Image Editor</h2>
		<div id="imageEditorApp">
		</div>
		<ul>
			<li class="divider"></li>
		</ul>
	</div>
	   
	<div style="clear:both;"></div>
	 
	<div class="galleryUploader"> 
		<a href="javascript:;" class="bfToolTip">
		<input name="galleryfiles" type="file" class="galleryfiles" id="gal<?=$gallery['gallery_id']?>" size="10" />
		</a> 
		<div id="status-message"></div>
		<div id="gallery-queue"></div>
	</div>
	
	<div class="galleryImages">
		<? foreach($images as $img) { 
			$prettyName = $img;
		   	$html  = "";
         	$html .= '<div class="thumbItem '.$type.'Item" id="'.str_replace(".","",$img).'">';
			$html .= '<div class="deleteButton"></div>';
			$html .= '<div style="clear:both;"></div>';
			$html .= '<div class="thumbFileLabel '.$type.'Label">'.$prettyName.'</div>';
			
			$html .= '<div class="thumbHolder '.$type.'Thumb">';
			$html .= '<a rel="galImages" href="'.$_SESSION['user']['temp_url'].'gallery/original/'.$img.'">';
			$html .= '<img src="'.$_SESSION['user']['temp_url'].'gallery/'.$thumbSize.'/'.$img.'" title="'.$img.'"/>';
			$html .= '</a>';
			$html .= '</div>';  
			
			$html .= '<input type="checkbox" class="imgCheck"/>';
			$html .= '</div>'; 
			echo $html; 
		} ?> 
	</div>
	<div style="clear:both;"></div>
</div>

<div class="actionsPanel">
	<div class="icon">
	</div>
	<div class="inner">
		<ul>
			<li> 
				<label for="galleryView">View mode</label>
				<select name="viewMode" class="smallSelect" id="viewMode">
					<option><?=$viewLabel?></option>
					<option>-----------------</option>
					<option value="small">Small Thumbnails</option>
					<option value="large">Large Thumbnails</option>
					<option value="list">List</option>
				</select>
			</li>
			
			<li>
				<label for="bulkActions">Bulk actions</label>
		   		<select name="bulkActions" class="smallSelect" id="bulkActions">
					<option>Select One</option>
					<option>-----------------</option>
					<option value="delete">Delete Selected</option>
					<option value="combine">Edit/Combine Selected</option>
				</select> 
			</li>
			
			<li>
				<label for="addTo">Add to</label>
				<select name="addTo" class="smallSelect" id="addTo">
					<option>Select One</option>
					<option>-----------------</option>
					<? foreach($cats as $c) { ?>
						<? $gals = get_galleries($c['category_id']); ?>
						<? foreach($gals as $g) { ?>
							<option value="<?=$g['gallery_id'];?>"><?=$c['category_name']?>/<?=$g['gallery_name']?></option>
						<? } ?>
					<? } ?>
				</select>
			</li>
				
			<li>
			   	<label for="selectActions">Select</label>
				<button id="selectAll">All</button>
				<button id="selectNone">None</button>
			</li>
		</ul>
	</div>
</div>

<script type="text/javascript">
// <![CDATA[  

var type = "<?=$type?>";
var thumbSize = "<?=$thumbSize?>";
var galID; 
var lastThumbChecked; 

$(document).ready(function() {
	$("#imageEditor").hide();
	
});

function galleryFunctionality() { 
	
	$(".thumbItem").find(".deleteButton").unbind("click"); 
	$(".thumbItem").find(".deleteButton").click(function() {
		var save = confirm("Are you sure you want to delete this image from the image bank and any galleries where it may appear? This cannot be undone.");
		if (save) { 
			removeImage($(this).parent()); 
		}
	});
	
	<? if ($type == 'list') { ?>
	$(".thumbFileLabel").unbind("click");
	$(".thumbFileLabel").click(function() {
    	$(this).parent().find(".thumbHolder").slideToggle('fast');
	});
	<? } ?>
	
	//
	// handle check box shift functionality
	$(".imgCheck").unbind("click");
	$(".imgCheck").click(function() {
		var parentID = $(this).parent().attr("id");
		var isChecked = $(this).attr("checked"); 
		//
		// check if shift key is down  
		if (!event.shiftKey) {
			lastThumbChecked = $(this).parent();
			return;
		}
		//
		// get index
		var thisIndex = $(".thumbItem").index($(this).parent());
		var lastIndex = $(".thumbItem").index(lastThumbChecked);
		//
		// determine if we should move up the dom or down
		if (thisIndex > lastIndex) {
			var start = lastIndex;
			var end = thisIndex;
		} else {
			var start = thisIndex;
			var end = lastIndex;
		} 
		
		for (var i=start; i<=end; i++) { 
			if (isChecked) {
				$(".thumbItem:eq("+i+")").find(".imgCheck").attr("checked", "checked");
			} else {
				$(".thumbItem:eq("+i+")").find(".imgCheck").removeAttr("checked");
			}
		}
		 
	});
	
}
galleryFunctionality();  

//
// gallery image upload 
$(".galleryfiles").uploadifive({
//$(".galleryfiles").uploadify({
  'uploadScript'   : adminURL + '_bfadmin/file_upload.php', 
  'method'         : 'post',  
  'formData'     : {'u':u,'p':p,'mode':'gallery','galID':galID}, 
  'cancelImg'      : 'uploadifive-cancel.png',
  'multi'          : true,
  'auto'           : true,
  'fileExt'        : '*.jpg;*.jpeg;*.png;',
  'fileDesc'       : 'Image Files (.JPG, .JPEG, .PNG)',
  'queueID'        : 'gallery-queue', 
  'buttonText'     : 'UPLOAD IMAGES',
  'width'          : 140,
  'queueSizeLimit' : 100,
  'simUploadLimit' : 1,
  'removeCompleted': true,
  'onSelectOnce'   : function(event,data) {
      	//$('#status-message').text(data.filesSelected + 'uploading...');
		$('#status-message').text('uploading...');
    }, 
  'onComplete'     : function(event, ID, fileObj, response, data) { 
		var fileParts = getNameAndExtension(fileObj['name']); 
		var imageFile = cleanfile(fileParts[0])+'.'+fileParts[1];
	  	$.log('fileName: '+cleanfile(fileParts[0])+'.'+fileParts[1]);
	  	$.log('response: '+response); 
		//
		addToImageBank(imageFile);
		//
   	},
  'onAllComplete'  : function(event,data) {
		$.log("filesUploaded: "+data.filesUploaded);
		$.log("data.errors: "+data.errors);
		if (data.errors > 0) {
      		$('#status-message').text("There was an error with the upload.");
		} else {
			$('#status-message').text("");
		}
    }
});

function addToImageBank(imageFile) {
	//  
	var thumbID = imageFile.split(".").join(""); //getNameAndExtension(imageFile)[0]; //imageFile.substr(0, imageFile.length-4);
	$.log("addToImageBank: "+imageFile+', '+$("#"+thumbID).length);
	if ($("#"+thumbID).length > 0) { 
		//
		var d = new Date();
		$("#"+thumbID).find("img").attr("src", tempURL+'gallery/'+thumbSize+'/'+imageFile+"?time="+d.getTime());
		$.scrollTo($("#"+thumbID).offset().top - 50, 'slow');
		var curColor = $("#"+thumbID).css("borderColor"); 
		//$("#"+thumbID).css("borderColor", "#8bc53f"); 
		$("#"+thumbID).delay(620).fadeTo('slow', .5).fadeTo('slow', 1).fadeTo('slow', .5).fadeTo('slow', 1);
		//
	} else { 
		//
	    var html = "";
		html += '<div class="thumbItem '+type+'Item" id="'+imageFile.substr(0, imageFile.length-4)+'">';
		html += '<div class="deleteButton"></div>';
		html += '<div style="clear:both;"></div>';
		html += '<div class="thumbFileLabel '+type+'Label">'+imageFile+'</div>';
		html += '<div class="thumbHolder '+type+'Thumb">';
		html += '<a rel="galImages" href="'+tempURL+'gallery/original/'+imageFile+'">';
		html += '<img src="'+tempURL+'gallery/'+thumbSize+'/'+imageFile+'" title="'+imageFile+'"/>';
		html += '</a>';
		html += '</div>'; 
		html += '<input type="checkbox" class="imgCheck"/>';
		html += '</div>';
		//
		$(".galleryImages").append(html);
		handleUI();
		galleryFunctionality(); 
	}
	   
}

$(".viewModeButton").click(function() {
   	changeGalleryView($(this).val());
});
$("#viewMode").change(function() {
	changeGalleryView($(this).val());
});

function removeImage(thumb) { 
	var imageFile = thumb.find("img").attr("title");
   	var imageID = thumb.attr('id').substr(3);
	$.ajax({
      type: 'POST',
      url:'actions/images/remove-image-from-all-galleries.php',
      data: ({'imageFile':imageFile})
    }).done(function( msg ) {
	  	$.log( "msg: "+msg);
		if (msg.substr(0,7) == 'SUCCESS') {
			thumb.fadeOut("fast", function() {
			   thumb.remove(); 
			});
		
			$.ajax({
	          type: 'POST',
	          url:adminURL + '_bfadmin/file_remove.php',
	          data: ({'u':u,'p':p, 'mode':'gallery', 'file':imageFile})
	        }).done(function( msg ) {
			  $.log( "msg: " + msg );
			});
			//
		} else {
			alert("There was an error removing the image.");
		}
	});
}                         

function changeGalleryView(type) {
	var v = "#";
    for (var i=0; i<paths.length; i++) { 
	 	if (paths[i] != "") {
			if (i == paths.length - 1) {
				v += "/image-bank-" + type;
			} else {
				v += "/"+paths[i];
			} 
		}
	}
	location.href = v;
}

$("#bulkActions").change(function() { 
	
   	if ($(this).val() == 'delete') {
		//
	    var save = confirm("Are you sure you want to delete these images from the image bank and any galleries where they appear? This cannot be undone.");
		if (save) { 
			$(".thumbItem").each(function() {
				if ($(this).find(".imgCheck").attr("checked") == "checked") {
				   removeImage($(this)); 
				}
			});
		}
		
	} else if ($(this).val() == 'combine'){
		// 
		var imagesArr = new Array();
		$(".thumbItem").each(function() {
			if ($(this).find(".imgCheck").attr("checked") == "checked") {
			 	imagesArr.push($(this).find("img").attr("title"));
			}
		}); 
		$.log("imagesArr: "+imagesArr);
		if (imagesArr.length <= 4) {               
			if ($("#imageEditor").css("display") == "none") {
				$("#imageEditor").slideToggle("fast");           
			}
			$.scrollTo($("#imageEditor").offset().top - 50, 'slow');
			var divID = "imageEditorApp";
			swfobject.embedSWF("img-editor/imageEditorApp.swf", divID, "100%", "100%", '9.0.24', null, {'imagesArr':imagesArr.join(','), "u":u, "p":p, "basePath":serverURL+"gallery/original/", "adminURL":adminURL}, {'quality':'high','wmode':'transparent','allowScriptAccess':'always'},{},function(event) {}); 
			
		} else {
			alert("Please select 4 or fewer images to combine.")
		}
		//
	}
	
	// reset selection
	$(this).val("Select One");
});


$("#addTo").change(function() {
	$.log("Add to gallery: "+$(this).val()+', '+Number($(this).val()).toString()); 
	if (Number($(this).val()).toString() != "NaN") { 
		var galID = $(this).val(); 
		var count = 0; 
		var total = 0;
		//
		$(".thumbItem").each(function() {
			if ($(this).find(".imgCheck").attr("checked") == "checked") {
				total++; 
				var imageFile = $(this).find("img").attr("title");
				$.log("imageFile: "+imageFile);
			   	$.ajax({
			      type: 'POST',
			      url:'actions/images/add-image-to-gallery.php',
			      data: ({'galID':galID, 'imageFile':imageFile})
			    }).done(function( msg ) { 
					count++;
                   	if (count == total) { 
 						hideSave();
		            	location.href = "#/images/galleries/"+galID; 
				   	}	
				}); 
			}
		});
		//
		if (total==0) {  
			alert('Please select images to add.');
		} else {
			showSave();
		}
		// 
	}
});


$("#selectAll").click(function() {
	$(".imgCheck").attr("checked","checked");
});
$("#selectNone").click(function() {
	$(".imgCheck").removeAttr("checked");
});  

$(".actionsPanel").find(".icon").click(function() {
	if ($(".actionsPanel").hasClass("showing")) {
		$(".actionsPanel").removeClass("showing");
		$(".actionsPanel").animate({
			"marginLeft":2
		}, 'fast');
	} else { 
		$(".actionsPanel").addClass("showing");
		$(".actionsPanel").animate({
			"marginLeft":-200
		}, 'fast');
	}
});

$("#imageEditor").find(".deleteButton").click(function() { 
	var conf = confirm("Are you sure you want to close this editor? All data you're currently working on in the editor will be lost.");
	if (conf) {
		if ($("#imageEditor").css("display") != "none") {
			$("#imageEditor").slideToggle("slow");           
		}
	}
});

$.ajax({
  type: 'POST',
  url:adminURL + '_bfadmin/write_policy_file.php',
  data: ({'u':u,'p':p})
}).done(function( msg ) {
  $.log( "crossdomain?: " + msg );
});

function editorFileSaved(fileName) {
	$.log("editorFile: "+fileName);   
   	if ($("#imageEditor").css("display") != "none") { 
		$("#imageEditor").slideToggle("slow", function() {
			$("#imageEditorApp").html("");
		});
	}
    //
	var fileParts = getNameAndExtension(fileName); 
	var imageFile = cleanfile(fileParts[0])+'.'+fileParts[1];
  	$.log('fileName: '+cleanfile(fileParts[0])+'.'+fileParts[1]); 
	//
	addToImageBank(imageFile);
	// 
	$.scrollTo($(".galleryUploader").offset().top, 'slow');
	//
}

// ]]>
</script>
