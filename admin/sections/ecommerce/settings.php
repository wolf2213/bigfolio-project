<?
session_start();
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");
//
$s = get_settings();
$fvalues = unserialize($s['flash_vars']);
$template = get_property('template'); 
$propx_api_key = get_property("propx_api_key");
$templateType = get_property('TEMPLATE_TYPE');
?>

<div class="contentHeader">
	<h1>Ecommerce Settings</h1>
</div>

<div class="tip_message">Enter your propx api key here to activate the ecommerce functionality. You can find your propx API key by <a href="http://propx.co/users/sign_in" target="propx">signing in</a> or <a href="http://propx.co/users/sign_up" target="propx">signing up</a> for an account and visiting your <a href="http://propx.co/profile" target="propx">profile page</a>. You also must also <a href="http://propx.co/users/auth/stripe_connect" target="stripe">connect your stripe account</a> before you are able to sell products through your website.
	
</div>
<fieldset>
	<ul>
		<li>
			<label for="propx_api_key">PROpx API key</label>
			<input name="propx_api_key" type="text" id="propx_api_key" value="<?= $propx_api_key ?>"  class="bfInputText bfChangeListen" />
		</li>
		<li>
			<label for="rubix_cart_theme">Cart style (<a href="javascript:;" class="bfToolTip" title="This setting controls the style of ecommerce related elements.">?</a>)</label>
			<select id="rubix_cart_theme" name="rubix_cart_theme">
				<option><?=$fvalues['rubix_cart_theme']?></option>
				<? if ($fvalues['rubix_cart_theme'] != "rectangular") { ?>
					<option>rectangular</option>
				<? } ?> 
				<? if ($fvalues['rubix_cart_theme'] != "rounded") { ?>
					<option>rounded</option>
				<? } ?>
				<? if ($fvalues['rubix_cart_theme'] != "rectangular outline") { ?>
					<option>rectangular outline</option>
				<? } ?>
				<? if ($fvalues['rubix_cart_theme'] != "rounded outline") { ?>
					<option>rounded outline</option>
				<? } ?>
			</select>
		</li>
	</ul>
</fieldset> 
