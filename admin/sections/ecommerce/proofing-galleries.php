<?
//
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");
//
$collections = file_get_contents("http://propx.co/api/collections?api_key=".get_property("propx_api_key")."&skinny=1");
//echo "collections: ".$collections;
$s = get_settings();
$cjson = json_decode($collections);
?>
<div class="content_inner" id="galleries">
	<div class="contentHeader">
		<h1>Galleries</h1>
	</div>  
	<div class="tip_message">
	    Use this area to create proofing galleries. Click the edit link next to each gallery to upload images to that gallery or adjust other settings for that gallery.
	</div>  
	<div id="categories">
		<?
		$loop = 0;
		for($i=0; $i<count($cjson->collections); $i++) {
			$c = $cjson->collections[$i];
			if (strtolower($cjson->collections[$i]->name) != "photo stream") {
			?>
			<div class="category collection imageCat" id="col<?=$c->id?>">
				<div class="deleteButton"></div> 
			    <input type="text" class="bfInputText categoryName bfChangeListen" name="col-<?=$c->id?>" value="<?=$c->name?>">
				<div class="editButton">
					<a href="#/ecommerce/proofing-galleries/<?=$c->id?>" class="editLink">
						edit
					</a>
					|
					<? 
					$galURL = $_SESSION['user']['temp_url'];
					if ($s['start_mode'] != "FLASH") {
						$galURL .= "main.php"; 
					}
					$galURL .= "#!/proofs/".$c->id."/".$c->name."/1";
					?>
					<a href="<?=$galURL?>" class="viewLink" target="_blank">
						view
					</a>
				</div>
				<div style="clear:both;"></div>
			</div> 
			<? } ?>
		<? } ?>
	</div> 
	<button id="newGallery">New Gallery</button>
</div>


<script type="text/javascript">
// <![CDATA[

var numCats = <?=$loop?>;

/*
$("#categories").sortable({
	update: function(event, ui) {  
		var d = new Object();
		d['catList'] = new Array();
		$(this).find(".category").each(function() {
			d['catList'].push($(this).attr("id").substr(3)); // pushes catNNN minus the cat
		}); 
		$.log("d['catList']: "+d['catList']);
		
		$.ajax({
  			type: 'POST',
  			url:'actions/images/update-categories-order.php',
  			data: d
		});
	}
});
*/

function galleriesFunctionality() {
	
	//
	// rollovers
	/*
	$(".category").unbind("mouseover");
	$(".category").mouseover(function() {
	   	$(this).addClass("itemOver");
	});
	$(".category").unbind("mouseout");
	$(".category").mouseout(function() {
	   	$(this).removeClass("itemOver");
	});
	*/
	
	//
	// delete category button
    $(".category").find(".deleteButton").unbind("click");
    $(".category").find(".deleteButton").click(function() {
		var save = confirm("Are you sure you want to delete this gallery and its contents? This cannot be undone.");
		if (save) {
			var colID = $(this).parent().attr('id').substr(3);
			var d = new Object();
			d['colID'] = colID;
			var categoryDiv = $(this).parent();
			//
			$.ajax({
				type: 'POST',
				url: 'actions/ecommerce/delete-collection.php',
				data: d
			}).done(function( msg ) {
				$.log("msg: "+msg);
				categoryDiv.fadeOut('fast', function() {
					categoryDiv.remove();
				});
			});
		}
	});
}
galleriesFunctionality();

$("#newGallery").click(function() {
	$.ajax({
       	type: 'POST',
       	url:'actions/ecommerce/add-image-collection.php',
       	data: ({'order':numCats})
    }).done(function( msg ) {
	   	if (msg.substr(0,7) == "SUCCESS") { 
			//
			var catID = msg.substr(8);
			$.log("catID: "+catID);
			//
			var cathtml = "";
				cathtml += '<div class="category collection imageCat" id="col'+catID+'">'; 
				cathtml += '<div class="deleteButton"></div>';
				cathtml += '<input type="text" class="bfInputText categoryName" name="col-'+catID+'" value="Enter gallery name">';
				cathtml += '<div class="editButton"><a href="#/ecommerce/proofing-galleries/'+catID+'" class="editLink">edit</a></div>';
				cathtml += '<div style="clear:both;"></div>';
				cathtml += '</div>';
			//
			$("#categories").append(cathtml);
			//
			handleUI(); 
			galleriesFunctionality();
			//
			numCats++;
	   	} else if (msg.substr(0,5) == "QUOTA") {
			var quota = msg.substr(6); // msg returns QUOTA:N where N is the number allowed
			alert('The number of categories allowed for this site ('+quota+') has been met. To create a new category, an existing category must be removed first.');
		} else {
		   	alert('There was an error creating a new collection. Please try again later. If problems persist, please contact support.');
		}
	});
});

<? if (get_property("propx_api_key") == "") { ?>
alert("You must first enter your ProPX API key before being able to administer any content.");
window.location = "#/ecommerce/settings";
<? } ?>

// ]]>
</script>