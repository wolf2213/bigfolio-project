<?
//
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");
//
$products = file_get_contents("http://propx.co/api/products?api_key=".get_property("propx_api_key"));
$pjson = json_decode($products);
?>
<div class="content_inner" id="galleries">
	<div class="contentHeader">
		<h1>Products</h1>
	</div>  
	<div class="tip_message">
	    Use this area to create products that can be assigned to proofing galleries and pages.
	</div>  
	<div id="productsList">
		<?
		$loop = 0;
		for($i=0; $i<count($pjson->products); $i++) {
			$c = $pjson->products[$i];
		?>
		<div class="category product imageCat" id="pro<?=$c->id?>">
			<div class="deleteButton"></div> 
		    <input type="text" class="bfInputText categoryName bfChangeListen" name="pro-<?=$c->id?>" value="<?=$c->name?>">
			<input type="text" class="bfInputText productPrice categoryName bfChangeListen" name="price-pro<?=$c->id?>" value="<?=$c->price?>0">
			<div style="clear:both;"></div>
			<div class="productDescr">
				<textarea class="bfInputText bfChangeListen" name="descr-pro<?=$c->id?>"><?=$c->description?></textarea>
			</div>
			<div style="clear:both;"></div>
		</div> 
		<? } ?>
	</div> 
	<button id="newProduct">New Product</button>
</div>


<script type="text/javascript">
// <![CDATA[

var numCats = <?=$loop?>;

/*
$("#categories").sortable({
	update: function(event, ui) {  
		var d = new Object();
		d['catList'] = new Array();
		$(this).find(".category").each(function() {
			d['catList'].push($(this).attr("id").substr(3)); // pushes catNNN minus the cat
		}); 
		$.log("d['catList']: "+d['catList']);
		
		$.ajax({
  			type: 'POST',
  			url:'actions/images/update-categories-order.php',
  			data: d
		});
	}
});
*/

function galleriesFunctionality() {
	
	//
	// rollovers
	/*
	$(".category").unbind("mouseover");
	$(".category").mouseover(function() {
	   	$(this).addClass("itemOver");
	});
	$(".category").unbind("mouseout");
	$(".category").mouseout(function() {
	   	$(this).removeClass("itemOver");
	});
	*/
	
	//
	// delete category button
    $(".category").find(".deleteButton").unbind("click");
    $(".category").find(".deleteButton").click(function() {
		var save = confirm("Are you sure you want to delete this product? This cannot be undone and it will be immediately removed.");
		if (save) {
			var productID = $(this).parent().attr('id').substr(3);
			var d = new Object();
			d['productID'] = productID;
			var categoryDiv = $(this).parent();
			//
			$.ajax({
				type: 'POST',
				url: 'actions/ecommerce/delete-product.php',
				data: d
			}).done(function( msg ) {
				$.log("msg: "+msg);
				categoryDiv.fadeOut('fast', function() {
					categoryDiv.remove();
				});
			});
		}
	});
}
galleriesFunctionality();

$("#newProduct").click(function() {
	$.ajax({
       	type: 'POST',
       	url:'actions/ecommerce/add-product.php',
       	data: ({})
    }).done(function( msg ) {
		$.log("msg: "+msg);
	   	if (msg.substr(0,7) == "SUCCESS") { 
			//
			var catID = msg.substr(8);
			//
			var cathtml = "";
				cathtml += '<div class="category product imageCat" id="pro'+catID+'">'; 
				cathtml += '<div class="deleteButton"></div>';
				cathtml += '<input type="text" class="bfInputText categoryName" name="pro-'+catID+'" placeholder="Name" value="">';
				cathtml += '<input type="text" class="bfInputText productPrice categoryName bfChangeListen" name="price-pro'+catID+'" placeholder="Price" value="">';
				cathtml += '<div style="clear:both;"></div>';
				cathtml += '<div class="productDescr"><textarea class="bfInputText bfChangeListen" name="descr-pro'+catID+'" placeholder="Description"></textarea></div>';
				cathtml += '<div style="clear:both;"></div>';
				cathtml += '</div>';
			//
			$("#productsList").append(cathtml);
			//
			handleUI(); 
			galleriesFunctionality();
			//
			numCats++;
	   	} else if (msg.substr(0,5) == "QUOTA") {
			var quota = msg.substr(6); // msg returns QUOTA:N where N is the number allowed
			alert('The number of categories allowed for this site ('+quota+') has been met. To create a new category, an existing category must be removed first.');
		} else {
		   	alert('There was an error creating a new collection. Please try again later. If problems persist, please contact support.');
		}
	});
});

<? if (get_property("propx_api_key") == "") { ?>
alert("You must first enter your ProPX API key before being able to administer any content.");
window.location = "#/ecommerce/settings";
<? } ?>

// ]]>
</script>