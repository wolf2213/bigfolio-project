<?
//
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../../inc/db.inc.php");
include_once("../../../inc/functions.inc.php");
//
$s = get_settings();
$template = get_property('template'); 
$fvalues = unserialize($s['flash_vars']);
//
$galData = explode("-", $_GET['val']);  // separate val
for ($i=0; $i<count($galData); $i++) {
	if (stristr($galData[$i], ".php")) {
		$galData[$i] = str_replace(".php", "", $galData[$i]); // strip .php from all instances
	}
}
//substr($_GET['val'], 0, -4)
//
$gid = $galData[0]; 
if (!isset($galData[1])) { 
	if (isset($_SESSION['gallery_view_type'])) {
		$type = $_SESSION['gallery_view_type'];
	} else {
		$type = 'small'; 
		$thumbSize = 'thumb';
	}
} else {
	$type = $galData[1]; 
}
if ($type == 'large') {
	$thumbSize = 'small';
	$viewLabel = 'Large thumbnails';
} else if ($type == 'small') {
	$thumbSize = 'thumb'; 
	$viewLabel = 'Small thumbnails';
} else if ($type == 'list') {
	$thumbSize = 'medium';
	$viewLabel = 'List';
} 

//
// store view type for future visits
$_SESSION['gallery_view_type'] = $type;

//
//$images = get_gallery_images($gid);
$gallery = file_get_contents("http://propx.co/api/collections/".$gid."?api_key=".get_property("propx_api_key"));
//echo "gallery: $gallery";
$gallery = json_decode($gallery);
$images = $gallery->images;
//
$products = file_get_contents("http://propx.co/api/products?api_key=".get_property("propx_api_key"));
$pjson = json_decode($products);
//                           
// get cats used for move to menu
$cats = get_all_categories();
//
?>
<div class="content_inner galleryContent">
	<div class="contentHeader">
		<h1>Proofing Gallery</h1>
	</div>  
	<div class="tip_message">
	    Use this area to add images, reorder images, or change other settings associated with this gallery.
	</div>
	<div class="gallerySettings"> 
		<ul>
			<input type="hidden" name="galID" value="<?=$gallery->id?>" />
			<? 
			$galURL = $_SESSION['user']['temp_url'];
			if ($s['start_mode'] != "FLASH") {
				$galURL .= "main.php"; 
			}
			$galURL .= "#!/proofs/".$gallery->id."/".$gallery->name."/1";
			?>
			<li>
				<label>View this gallery</label>
				<a href="<?=$galURL?>" target="_blank" style="padding:10px 0;display:inline-block;"><?=$galURL?></a>
			</li>
			<li>
				<label for="galleryName">Gallery name</label>
				<input type="text" class="bfInputText bfChangeListen" name="galleryName" value="<?=$gallery->name?>" />
			</li>
			<li>
			<? if (isset($gallery->password)) { ?>
				<label for="galleryPassword">Password for this gallery</label>
				<input type="text" class="bfInputText bfChangeListen" name="galleryPassword" value="<?=$gallery->password?>" />
			<? } ?> 
			</li>
			<li class="divider"></li>
			<li>
				<? if (count($pjson->products) > 0) { ?>
				<label>Add a product to this gallery (<a href="javascript:;" class="bfToolTip" title="Select a product from the drop down to add to this gallery. Products can be created in the Ecommerce > Products section of the admin.">?</a>)</label>
				<select id="addProductDD">
					<option></option>
					<? for($i=0; $i<count($pjson->products); $i++) { ?>
					<option value="<?=$pjson->products[$i]->id;?>:<?=$pjson->products[$i]->name;?>:<?=$pjson->products[$i]->price;?>"><?=$pjson->products[$i]->name;?> - <?=$pjson->products[$i]->price;?></option>
					<? } ?>
					<option value="addAll">Add all products</option>
				</select>
				<? } else { ?>
				<label style="width:100%">Create a product to add to this gallery <a href="#/ecommerce/products">here</a>.</label>
				<? } ?>
			</li>
			<ul id="galleryProductsList">
				<? $productIDs = array(); ?>
				<? for($i=0; $i<count($gallery->products); $i++) { ?>
					<? $productIDs[] = $gallery->products[$i]->id; ?>
					<li data-productID='<?=$gallery->products[$i]->id?>'><?=$gallery->products[$i]->name?> - $<?=$gallery->products[$i]->price?>0<span class='deleteButton'></span></li>
				<? } ?>
			</ul>
			<input id="galleryProducts" name="galleryProducts" type="hidden" value="<?=implode(",",$productIDs)?>">
			<li class="divider"></li>
		</ul> 
	</div>
	
	<div style="clear:both;"></div>
	 
	<div class="galleryUploader">
		<a href="javascript:;" class="bfToolTip" title="Make sure to size your images according to the specifications for your template before uploading. This will shorten loading time on your site and allow the most control over how your images look on your website. Images should be saved for the web. We recommend using 70%-80% compression.">
		<input name="galleryfiles" type="file" class="galleryfiles" id="gal<?=$gallery->id?>" size="10" />
		</a> 
		<div id="status-message"></div>
		<div id="gallery-queue"></div>
	</div>
	
	<div class="galleryImages">
		<? for($i=0; $i<count($gallery->images); $i++) {
			$img = $gallery->images[$i];
			$prettyName = $img->small_url;
			//if ($img->caption == null) { $img->caption = ""; }
		   	$html  = "";
         	$html .= '<div class="thumbItem bfSortable '.$type.'Item" id="img'.$img->id.'">';
			$html .= '<div class="deleteButton"></div>';
			$html .= '<div style="clear:both;"></div>';
			$html .= '<div class="thumbFileLabel '.$type.'Label">'.$prettyName.'</div>';
			
			$html .= '<div class="thumbHolder '.$type.'Thumb">';
			$html .= '<a rel="galImages" href="'.$img->large_url.'" title="'.str_replace('"', '&quot;', $img->caption).'">';
			$html .= '<img src="'.$img->small_url.'" title="'.$img->small_url.'"/>';
			$html .= '</a>';
			$html .= '</div>';
			$html .= '<div class="'.$type.'ImageCaption imgCap">'; 
			$html .= '<textarea class="bfChangeListen" name="caption'.$img->id.'">'.$img->caption.'</textarea>';
			$html .= '</div>';
			$html .= '<input type="checkbox" class="imgCheck"/>';
			$html .= '<div class="captionButton"></div>';
			$html .= '</div>'; 
			echo $html; 
		} ?> 
	</div>
	<div style="clear:both;"></div>
</div>

<div class="actionsPanel">
	<div class="icon">
	</div>
	<div class="inner">
		<ul>
			<li> 
				<label for="galleryView">View mode</label>
				<select name="viewMode" class="smallSelect" id="viewMode">
					<option><?=$viewLabel?></option>
					<option>-----------------</option>
					<option value="small">Small Thumbnails</option>
					<option value="large">Large Thumbnails</option>
					<option value="list">List</option>
				</select>
			</li>
			
			<li>
				<label for="bulkActions">Bulk actions</label>
		   		<select name="bulkActions" class="smallSelect" id="bulkActions">
					<option>Select One</option>
					<option>-----------------</option>
					<option value="delete">Delete Selected</option>
				</select> 
			</li> 
			
			
			<li>
			   	<label for="selectActions">Select</label>
				<button id="selectAll">All</button>
				<button id="selectNone">None</button>
			</li>
		</ul>
	</div>
</div>

<script type="text/javascript">
// <![CDATA[  
var type = "<?=$type?>";
var thumbSize = "<?=$thumbSize?>";
var galID = "<?=$gallery->id?>"; 
var lastThumbChecked; 

if ($("#metaSettings").find('.metaSetting').length > 0) {
	$("#seoSettings").show();
}

$(document).ready(function() {
	
	<? //if ($gallery['gallery_align'] != "") { ?>
		$(".imageAlign").hide();             
	<? //} ?>
});

$("#globalAlignLeft").click(hideImageAlign);
$("#globalAlignCenter").click(hideImageAlign);
$("#globalAlignRight").click(hideImageAlign);
$("#globalAlignNone").click(showImageAlign);

function hideImageAlign() {
	$(".imageAlign").hide();
}
function showImageAlign() {
	$(".imageAlign").show();
}

$("#otherToggle").click(function() {
 	$("#otherSettings").slideToggle('fast');
}); 
if ($("#otherSettings").find('.otherSetting').length > 0) {
	$("#otherToggle").show();
}

function galleryFunctionality() {  
	
	
	$(".captionButton").unbind("click");
	$(".captionButton").click(function() {   
		$.log('caption display: '+$(this).parent().find(".imgCap").css("display"));
		if ($(this).parent().find(".imgCap").css("display") == "none") {
			$(this).parent().find(".imgCap").show();
			//$(this).parent().find(".thumbHolder").hide();
		} else {
			$(this).parent().find(".imgCap").hide();
			//$(this).parent().find(".thumbHolder").show();
		}
	});
	 
	
	$(".thumbItem").find(".deleteButton").unbind("click"); 
	$(".thumbItem").find(".deleteButton").click(function() {
		var save = confirm("Are you sure you want to delete this image from this gallery? This cannot be undone.");
		if (save) { 
			removeImage($(this).parent()); 
		}
	});
	
	<? if ($type == 'list') { ?>
	$(".thumbFileLabel").unbind("click");
	$(".thumbFileLabel").click(function() {
    	$(this).parent().find(".thumbHolder").slideToggle('fast');
	});
	<? } ?>
	
	//
	// handle check box shift functionality
	$(".imgCheck").unbind("click");
	$(".imgCheck").click(function() {
		var parentID = $(this).parent().attr("id");
		var isChecked = $(this).attr("checked"); 
		//
		// check if shift key is down  
		if (!event.shiftKey) {
			lastThumbChecked = $(this).parent();
			return;
		}
		//
		// get index
		var thisIndex = $(".thumbItem").index($(this).parent());
		var lastIndex = $(".thumbItem").index(lastThumbChecked);
		//
		// determine if we should move up the dom or down
		if (thisIndex > lastIndex) {
			var start = lastIndex;
			var end = thisIndex;
		} else {
			var start = thisIndex;
			var end = lastIndex;
		} 
		
		for (var i=start; i<=end; i++) { 
			if (isChecked) {
				$(".thumbItem:eq("+i+")").find(".imgCheck").attr("checked", "checked");
			} else {
				$(".thumbItem:eq("+i+")").find(".imgCheck").removeAttr("checked");
			}
		}
		 
	});
	
}
galleryFunctionality();  


$(".galleryImages").sortable({
	update: function(event, ui) { 
		var d = new Object(); 
		d['galID'] = galID;
		d['imgList'] = new Array();
		$(this).find(".thumbItem").each(function() {
			d['imgList'].push($(this).attr("id").substr(3)); // pushes imgNNN minus the img
		});
		$.log("d['imgList']: "+d['imgList']);

		$.ajax({
  			type: 'POST',
  			url:'actions/ecommerce/update-collection-images-order.php',
  			data: d
		}).done(function( msg ) {
			$.log( "msg: " + msg );
		}); 
	}
});


<?
include("../../../inc/S3/S3_config.php");
// THIS YOU CHANGE
$aws_access_key = awsAccessKey; // your acces key to Amazon services (get if from https://portal.aws.amazon.com/gp/aws/securityCredentials)
$aws_secret_key = awsSecretKey; // secret access key (get it from https://portal.aws.amazon.com/gp/aws/securityCredentials)

$userstamp =  $_SESSION['user']['db_user'][0].'/'.$_SESSION['user']['db_user'][1].'/'.$_SESSION['user']['db_user'][2].'/'.substr($_SESSION['user']['db_user'], 0, 5);

$bucket = 'cdn.bigfolio12.com'; // the name you've chosen for the bucket
$uniqueID = uniqid();
$key = 'rubiximport/'.$userstamp.'/${filename}'; // the folder and adress where the file will be uploaded; ${filename} will be replaced by original file name (the folder needs to be public on S3!)
$success_action_redirect = 'http://example.com/success.html'; // URL that you will be redirected to when the file will be successfully uploaded
$content_type = ''; // limit accepted content types; empty will disable the filter; for example: 'image/', 'image/png'
$acl = 'public-read'; // private or public-read

// THIS YOU DON'T
$year = date(Y) + 10;
$policy = '{ "expiration": "2100-12-01T12:00:00.000Z",
"conditions": [
  {"acl": "public-read" },
  {"bucket": "'.$bucket.'" },
  {"success_action_status" : "201"},
	["starts-with", "$Filename", "" ],
    ["starts-with", "$filename", "" ],
    ["starts-with", "$folder", "" ],
    ["starts-with", "$key", "rubiximport/'.$userstamp.'/"],
	["starts-with", "$fileExt", ""],
    ["content-length-range", 1, 209715200]
  ]
}';
$signature = base64_encode(hash_hmac('sha1', base64_encode($policy), $aws_secret_key, true));
?>
//
// gallery image upload 
// http://<?=$bucket?>.s3.amazonaws.com/
// adminURL + '_bfadmin/post_tester.php'

var tempUploadLocation = "http://<?=$bucket?>.s3.amazonaws.com/rubiximport/<?=$userstamp?>/";
 
$('.galleryfiles').uploadify({
      'fileDataName' 	: 'file',
      'uploader'     	: 'js/plugins/uploadify-v2.1.4/uploadifyCS3.swf',
      'script'       	: 'http://<?=$bucket?>.s3.amazonaws.com/',
      'cancelImg'    	: 'js/plugins/uploadify-v2.1.4/cancel.png',
      'auto'         	: true,
	  'multi'           : true,
	  'fileExt'         : '*.jpg;*.jpeg;',
	  'fileDesc'        : 'Image Files (.JPG, .JPEG)',
	  'queueID'         : 'gallery-queue',
	  'buttonText'     : 'UPLOAD IMAGES',
	  'width'          : 140,
	  'queueSizeLimit' : 100,
	  'simUploadLimit' : 1,
	  'removeCompleted': true,
      'onError' 		: function(errorObj, q, f, err) { $.log("errorObj.type: "+errorObj.type) },
	  'onSelectOnce'   : function(event,data) {
	      	//$('#status-message').text(data.filesSelected + 'uploading...');
			$('#status-message').text('uploading...');
	    }, 
	  'onComplete'     : function(event, ID, fileObj, response, data) { 
			$.log("response: "+response);
			//var rxml = $.parseXML(response);
			var xml = response,
			  xmlDoc = $.parseXML( xml ),
			  $xml = $( xmlDoc );
			var file = "http://s3.amazonaws.com/" + $xml.find("Bucket").text() + "/" + $xml.find("Key").text();
			//
			addImageToGallery(tempUploadLocation+fileObj['name']); //file); //
			//
	   	},
	  'onAllComplete'  : function(event,data) {
			$.log("filesUploaded: "+data.filesUploaded);
			$.log("data.errors: "+data.errors);
			if (data.errors > 0) {
	      		$('#status-message').text("There was an error with the upload.");
			} else {
				$('#status-message').text("");
			}
	    },
      'scriptData' 		: {
         	AWSAccessKeyId: "<?=$aws_access_key?>",
         	key: "rubiximport/<?=$userstamp?>/${filename}",
         	acl: "public-read",
         	policy: "<?=base64_encode($policy);?>",
         	signature: "<?=$signature;?>",
            success_action_status: '201'
       }

});

function addImageToGallery(imageFile) {
	$.log("addImageToGallery: "+imageFile);
	$.ajax({
      type: 'POST',
      url:'actions/ecommerce/add-image-to-collection.php',
      data: ({'galID':galID, 'imageFile':imageFile})
    }).done(function( msg ) {
	  	$.log( "msg: " + msg );
	 	if (msg.substr(0,7) == "SUCCESS") { 
			//
			var imgID = ""; //msg.substr(8);
			//
		    var html = "";
			html += '<div class="thumbItem bfSortable '+type+'Item" id="img'+imgID+'">';
			html += '<div class="deleteButton"></div>';
			html += '<div style="clear:both;"></div>';
			html += '<div class="thumbFileLabel '+type+'Label">'+imageFile+'</div>';
			html += '<div class="thumbHolder '+type+'Thumb">';
			html += '<a rel="galImages" href="'+imageFile+'">';
			html += '<img src="'+imageFile+'"/>';
			html += '</a>';
			html += '</div>'; 
			html += '<div class="'+type+'ImageCaption imgCap">'; 
			html += '<textarea class="bfChangeListen" name="caption'+imgID+'"></textarea>';
			html += '</div>';
			html += '<input type="checkbox" class="imgCheck"/>';
			html += '<div class="captionButton"></div>';
			//
			html += '</div>';
			//
			$(".galleryImages").append(html);
			handleUI();
			galleryFunctionality();
		} else if (msg.substr(0,5) == "QUOTA") {
			var quota = msg.substr(6); // msg returns QUOTA:N where N is the number allowed
			alert('The number of images allowed in a gallery for this site ('+quota+') has been met. To add more images, some existing images must be removed first.');
		} else {
			alert("There was a problem adding the image "+imageFile+" to this gallery. Please try again later. If problems persist, please contact support.")
		}
		
	});
}

$(".viewModeButton").click(function() {
   	changeGalleryView($(this).val());
});
$("#viewMode").change(function() {
	changeGalleryView($(this).val());
});

function addProduct(val) {
	var product = val.split(":");
	var curProducts = $("#galleryProducts").val().split(",");
	if (curProducts[0] == "") {
		curProducts.splice(0,1);
	}
	var found = false;
	for (var i=0; i<curProducts.length; i++) {
		if (Number(curProducts[i]) == Number(product[0])) {
			found = true;
		}
	}
	if (!found) {
		curProducts.push(product[0]);
		$("#galleryProducts").val(curProducts.join(","));
		$("#galleryProductsList").append("<li data-productID='"+product[0]+"'>"+product[1]+" - $"+product[2]+"0<span class='deleteButton'></span></li>");
		handleProductListFunctionality();
	}
}

$("#addProductDD").change(function() {
	if ($(this).val() == "addAll") {
		$("#addProductDD").find("option").each(function() {
			if ($(this).val().indexOf(":") > -1) { addProduct($(this).val()); }
		})
	} else {
		addProduct($(this).val());
	}
	$(this).val("");
});

function handleProductListFunctionality() {
	$("#galleryProductsList .deleteButton").unbind("click");
	$("#galleryProductsList .deleteButton").click(function() {
		var pdiv = $(this).parent();
		var pid = pdiv.attr("data-productID");
		$.log("pid: "+pid);
		var curProducts = $("#galleryProducts").val().split(",");
		for (var i=0; i<curProducts.length; i++) {
			if (Number(curProducts[i]) == Number(pid)) {
				curProducts.splice(i,1);
				pdiv.fadeOut("fast", function() {
					pdiv.remove();
				});
				break;
			}
		}
		$("#galleryProducts").val(curProducts.join(","));
	});
}
handleProductListFunctionality();

$("#galleryProductsList")

function removeImage(thumb) { 
	//var imageFile = thumb.find("img").attr("title");
   	var imageID = thumb.attr('id').substr(3);
	$.ajax({
      type: 'POST',
      url:'actions/ecommerce/remove-images-from-collection.php',
      data: ({'images':imageID,'galID':galID})
    }).done(function( msg ) {
	  	$.log( "msg: "+msg);
		if (msg.substr(0,7) == 'SUCCESS') {
			thumb.fadeOut("fast", function() {
			   thumb.remove(); 
			});
		} else {
			alert("There was an error removing the image from this gallery.");
		}
	});
}                         

function changeGalleryView(type) {
	var v = "#";
    for (var i=0; i<paths.length; i++) { 
	 	if (paths[i] != "") {
			if (i == paths.length - 1) {
				var curPath = paths[i].split('-');
				v += "/"+curPath[0] + '-' + type;
			} else {
				v += "/"+paths[i];
			} 
		}
	}
	location.href = v;
}

$("#bulkActions").change(function() { 
	
   	if ($(this).val() == 'delete') {
		//
	    var save = confirm("Are you sure you want to delete these images from this gallery? This cannot be undone.");
		if (save) { 
			var images = "";
			var thumbIDs = new Array();
			$(".thumbItem").each(function() {
				if ($(this).find(".imgCheck").attr("checked") == "checked") {
					if (images != "") {
						images += ",";
					}
					images += $(this).attr('id').substr(3);
					thumbIDs.push($(this).attr('id'));
				}
			});
			$.ajax({
		      type: 'POST',
		      url:'actions/ecommerce/remove-images-from-collection.php',
		      data: ({'images':images,'galID':galID})
		    }).done(function( msg ) {
			  	$.log( "msg: "+msg);
				if (msg.substr(0,7) == 'SUCCESS') {
					for (var i=0; i<thumbIDs.length; i++) {
						$("#"+thumbIDs[i]).remove();
					}
				} else {
					alert("There was an error removing the image from this gallery.");
				}
			});
		}
		
	}
	
	// reset selection
	$(this).val("Select One");
});

$("#moveTo").change(function() {
	$.log("Add to gallery: "+$(this).val()+', '+Number($(this).val()).toString()); 
	if (Number($(this).val()).toString() != "NaN") { 
		var newGalID = $(this).val(); 
		var count = 0; 
		var total = 0;
		//
		$(".thumbItem").each(function() {
			if ($(this).find(".imgCheck").attr("checked") == "checked") {
				total++; 
			}
		});
		//
		$(".thumbItem").each(function() {
			if ($(this).find(".imgCheck").attr("checked") == "checked") {
				var imageFile = $(this).find("img").attr("title"); 
				var imageID = $(this).attr("id").substr(3); // imgXXX
				$.log("imageFile: "+imageFile);
			   	$.ajax({
			      type: 'POST',
			      url:'actions/images/move-image-to-gallery.php',
			      data: ({'curGalID':galID, 'newGalID':newGalID, 'imageFile':imageFile, 'imageID':imageID})
			    }).done(function( msg ) {
				 	$.log('move msg: '+msg);
					count++;  
					$.log("count:"+count+", total:"+total);
                   	if (count == total) { 
 						hideSave();
		            	location.href = "#/images/galleries/"+newGalID; 
				   	}	
				}); 
			}
		});
		//
		if (total==0) {  
			alert('Please select images to add.');
		} else {
			showSave();
		}
		// 
	}
});

$("#selectAll").click(function() {
	$(".imgCheck").attr("checked","checked");
});
$("#selectNone").click(function() {
	$(".imgCheck").removeAttr("checked");
});

$(".actionsPanel").find(".icon").click(function() {
	if ($(".actionsPanel").hasClass("showing")) {
		$(".actionsPanel").removeClass("showing");
		$(".actionsPanel").animate({
			"marginLeft":2
		}, 'fast');
	} else { 
		$(".actionsPanel").addClass("showing");
		$(".actionsPanel").animate({
			"marginLeft":-200
		}, 'fast');
	}
});

$("#imageEditor").find(".deleteButton").click(function() { 
	var conf = confirm("Are you sure you want to close this editor? All data you're currently working on in the editor will be lost.");
	if (conf) {
		if ($("#imageEditor").css("display") != "none") {
			$("#imageEditor").slideToggle("slow");           
		}
	}
});

$.ajax({
  type: 'POST',
  url:adminURL + '_bfadmin/write_policy_file.php',
  data: ({'u':u,'p':p})
}).done(function( msg ) {
  $.log( "crossdomain?: " + msg );
});

function editorFileSaved(fileName) {
	$.log("editorFile: "+fileName);   
   	if ($("#imageEditor").css("display") != "none") { 
		$("#imageEditor").slideToggle("slow", function() {
			$("#imageEditorApp").html("");
		});
	}
    //
	var fileParts = getNameAndExtension(fileName); 
	var imageFile = cleanfile(fileParts[0])+'.'+fileParts[1];
  	$.log('fileName: '+cleanfile(fileParts[0])+'.'+fileParts[1]); 
	//
	addImageToGallery(imageFile);
	// 
	$.scrollTo($(".galleryUploader").offset().top, 'slow');
	//
}

// ]]>
</script>