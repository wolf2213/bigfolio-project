<?
//
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");
$api_key = get_property("propx_api_key"); //"MXd8vPUqMxvTNeJpJwCy"; //"1pVzDZNzruUikYwZAvQp"; //
//
$transactions = file_get_contents("http://propx.co/api/transactions?api_key=".$api_key); //
$tjson = json_decode($transactions);
//
/*
$invoices = file_get_contents("http://propx.co/api/invoices?api_key=".$api_key);
$ijson = json_decode($invoices);
*/
//
?>
<div class="content_inner" id="galleries">
	<div class="contentHeader">
		<h1>Orders</h1>
	</div>  
	<div class="tip_message">
	    Use this area to review orders submitted through your site.	
<!--		
		invoices: <?=$invoices;?>
		
		transactions: <?=$transactions;?>
-->
	</div> 
	<div class="orderSettings"> 
		<ul>
		<li>
			<label for="sortMonth">Sort by Month</label>
			<select id="sortMonth">
			</select> 
		<li>
		</ul>
	</div>
	<div id="ordersList">
		<?
		$lastYear = "";
		$year = "";
		$lastMonth = "";
		$month = "";
		for($i=count($tjson->transactions)-1; $i>=0; $i--) {
			$t = $tjson->transactions[$i];
			if (stristr($t->transaction_type,"invoice")) { ?>
				<? if (date("Y",$t->timestamp) != $year) { ?>
				<? if ($year != "") { ?>
				</div><!-- /.yearGroup -->
				<? } ?>
				<? $year = date("Y",$t->timestamp); ?>
				<div class="yearGroup" data-year="<?=$year?>">	
				<? } ?>
				<? if (date("n",$t->timestamp) != $month) { ?>
				<? if ($month != "") { ?>
				</div><!-- /.monthGroup -->
				<? } ?>
				<? $month = date("n",$t->timestamp); ?>
				<div class="monthGroup" data-month="<?=$month?>">	
				<? } ?>
				<?
				//$invoiceID = $t->invoice_id;
				$invoice = $t->invoices; //array_filter($ijson->invoices, function ($e) use (&$invoiceID) { return $e->id == $invoiceID; });
				?>
				<div class="category order imageCat" id="ord<?=$t->id?>">
					<?
					/*
					$lineItems = '[{"id":472,"product_id":24,"quantity":1,"total":"460.0","product_name":"Framed 12x18 print","description":null,"url":"http://s3.amazonaws.com/cdn.bigfolio12.com/images/000/058/869/bcb5ff07a0a27e6ac1b03a4f7fd48ae036002392/open-uri20131014-522-b9ia13_small.?1381787781"},{"id":471,"product_id":7,"quantity":1,"total":"260.0","product_name":"Canvas print -18x24","description":null,"url":"http://s3.amazonaws.com/cdn.bigfolio12.com/images/000/058/869/bcb5ff07a0a27e6ac1b03a4f7fd48ae036002392/open-uri20131014-522-b9ia13_small.?1381787781"},{"id":470,"product_id":25,"quantity":1,"total":"30.0","product_name":"Wallet sized prints","description":null,"url":"http://s3.amazonaws.com/cdn.bigfolio12.com/images/000/058/867/49e18b7bdd053a32b164787cf4033b3e2b58abe3/open-uri20131014-10523-1gk2r5n_small.?1381787774"}]';
					*/
					//$invoice->line_items = array();
					$invoice->line_items = json_decode($lineItems);
					?>
					<? if (count($invoice->line_items) > 0) { ?>
					
					<? } ?>
					<div class="date">
						Purchase date: <?=date("M j Y",$t->timestamp); //date("M j Y @ h:i:s a T",$t->timestamp)?>
					</div>
					<div class="info">
						<ul>
							<li>Name: John Doe</li>
							<li>Email: <a href="mailto:jd@domain.com">jd@domain.com</a></li>
							<li>Phone: 777-777-7777</li>
							<li>Address: 123 E West St, New York, NY 10021</li>
							<!--<li><?=$t->invoice_name?></li>-->
							<li>Amount: $<?=$t->amount?></li>
							<li><a class="bfToolTip" title="View more detailed payment information or issue a refund within the Stripe payment log for this payment." href="https://manage.stripe.com/payments/<?=$t->stripe_id?>" target="_blank">View transaction in Stripe</a></li>
						</ul>
						<?//=$invoice[0]->url?>
					</div>
					<? if (count($invoice->line_items) > 0) { ?>
					<div class="plusMinusButton plus"></div>
					<div style="clear:both;"></div>
					<div class="lineitems">
						<table class="table table-bordered">
						<thead>
						<tr>
						<th>Preview</th>
						<th>Product</th>
						<th>Quantity</th>
						<th>Unit Price</th>
						<th></th>
						</tr>
						</thead>
						<tbody>
						<? foreach($invoice->line_items as $li) { ?>
							<?
							//$imgjson = file_get_contents("http://propx.co/api/images/".$li->id."?api_key=".$api_key);
							//$img = json_decode($imgjson);
							?>
							<tr>
							<td>
							<?if($li->url!=""){?>
							<img src="<?=$li->url?>" alt="">
							<?}?>
							</td>
							<td><?=$li->product_name?></td>
							<td><?=$li->quantity?></td>
							<td>$<?=$li->total?>0</td>
							<td>$<?=$li->total*$li->quantity?></td>
							</tr>
						<? } ?>
						<tr>
						<th class="total" colspan="4">Total</th>
						<th>$<?=$t->amount?></th>
						</tr>
						</tbody>
						</table>
					</div>	
					<? } ?>
					<div style="clear:both;"></div>
				</div>
			<? } ?> 
		<? } ?>
		</div><!-- /.monthGroup -->
		</div><!-- /.yearGroup -->
	</div> 
</div>


<script type="text/javascript">
// <![CDATA[
$(".order").find(".plusMinusButton").unbind("click");
$(".order").find(".plusMinusButton").click(function() {
   	if ($(this).hasClass("minus")) {
	    $(this).removeClass("minus");
		$(this).addClass("plus");
		$(this).parent().find(".lineitems").slideUp("fast");
	} else {
		$(this).addClass("minus");
		$(this).removeClass("plus");
		$(this).parent().find(".lineitems").slideDown("fast");
	} 
});

$("#sortMonth").change(function() {
	var vals = $(this).val().split(',');
	var monthNum = vals[0];
	var yearNum = vals[1];
	$(".yearGroup").each(function() {
		if ($(this).attr("data-year") == yearNum) {	
			$(this).slideDown("fast");
			$(this).find(".monthGroup").each(function() {
				if ($(this).attr("data-month") == monthNum) {
					$(this).slideDown("fast");
				} else {
					$(this).slideUp("fast");
				}
			});
		} else {
			$(this).slideUp("fast");
		}
	});
});

$(".yearGroup").each(function() {
	var y = $(this).attr("data-year");
	var months = new Array("", "January","February","March","April","May","June","July","August","September","October","November","December");
	$(this).find(".monthGroup").each(function() {
		$("#sortMonth").append("<option value='"+$(this).attr("data-month")+","+y+"'>"+months[$(this).attr("data-month")]+" "+y+" - ("+$(this).find(".order").length+" orders)</option>");
	});
});

<? if (get_property("propx_api_key") == "") { ?>
alert("You must first enter your ProPX API key before being able to administer any content.");
window.location = "#/ecommerce/settings";
<? } ?>

// ]]>
</script>