<?
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php"); 
//
session_start();
//
// Settings
$s = get_settings(); 

?>

<div class="contentHeader">
	<h1>Advanced Style for SmugMug Customization -</h1>  <a href="https://vimeo.com/146453339" target="_blank"><img src="img/watch-this-video2.png" border="0" style="position:absolute; margin-left:700px; margin-top:-50px;" /></a></h1>
</div>


  <div class="tip_message">
    <p>You can use the code below to style your SmugMug site to match your portfolio. To use the code below, visit the 
      Advanced Site-wide Customization screen: </p>
    <ol>
      <li>Login to your SmugMug account</li>
      <li>From the Control Panel, select the Customize Tab</li>
      <li>Visit the Advanced Site-wide Customization screen</li>
    </ol>
  </div>
<ul>
  <li class="divider"></li>
  <li>
  <div class="smugmug_code">
      <h2>CSS</h2>
      <div class="tip_message">Paste this into the CSS code area of SmugMug's Advanced Site-Wide Customization page.</div>
      <textarea id="smugmug_css_code" style="width:650px;height:150px;">div#homepage, div#category {
background:<?=$s['body_color']?>;
}
div#homepage div#breadCrumbTrail {
display:none;
}
div#breadCrumbTrail {
padding-left:10px;
}
div#header_wrapper {
background:<?=$s['body_color']?>;
}
.box, .boxBottom {
background:<?=$s['body_color']?>;
}</textarea>
  </div>
  </li>

  <li class="divider"></li>
  <li>
  <div class="smugmug_code">
      <h2>Head Tag</h2>
      <div class="tip_message">Paste this into the Head Tag area of SmugMug's Advanced Site-Wide Customization page.</div>
      <textarea id="smugmug_head_tag_code" style="width:650px;height:50px;"><script src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script> 
<script src="http://cloud.bigfolio.com/smugmug/jquery.backgrounder.js"></script></textarea>
  </div>
  </li>
  
  <? if ($s['title_mode'] == 'LOGO') { ?>
  <li class="divider"></li>
  <li>
	  <div class="smugmug_code">
	      <h2>Custom Header</h2>
	      <div class="tip_message">Paste this into the Custom Header area of SmugMug's Advanced Site-Wide Customization page. 
	        This will display your site's logo on your SmugMug pages.</div>
	      <textarea id="smugmug_custom_header_code" style="width:650px;height:150px;"><div id="header_wrapper" class="top_border bottom_border">
<div id="header">
<img src="<?=$_SESSION['user']['url'] ?>images/<?=$s['logo_file'] ?>" alt="<?=$s['business_name'] ?>" />
<div id="toolbar" class="nav">
<a href="<?=$_SESSION['user']['url'] ?>">Portfolio Home</a>
</div>
<div class="spacer"></div>
</div>
</div>
		  </textarea>
	  </div> 
  </li>
  <? } ?> 
</ul>

<script type="text/javascript">
// <![CDATA[


// ]]>
</script>
