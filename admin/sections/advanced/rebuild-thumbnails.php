<?
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");
//
// Settings
$s = get_settings();  

//
// get font files from remote server 
$postArr = array();
$postArr['u'] = $_SESSION['user']['email'];
$postArr['p'] = $_SESSION['user']['password'];
$postArr['mode'] = 'gallery';
$url = SERVER . '_bfadmin/files_list.php'; // 
$remoteFiles = curl_post($url, $postArr, array()); 
$remoteFiles = json_decode($remoteFiles);  

?>

<div class="contentHeader">
	<h1>Rebuild Thumbnails -</h1>  <a href="https://vimeo.com/146453339" target="_blank"><img src="img/watch-this-video2.png" border="0" style="position:absolute; margin-left:320px; margin-top:-50px;" /></a>
</div>

<div class="tip_message">
	This feature allows you to recreate thumbnails for all images in your image bank.<br /><br />You only need to rebuild your thumbnails for these images if you have uploaded images and thumbnails are not displaying in your image bank<br /><br />
	    <span class="warning" style="display: block; background: #ffcccc; padding: 5px;">Caution: If you have any custom thumbnails they will be overwritten, and this action cannot be undone.</span>
	<br />This process may take a few minutes depending on the number of images you have. Please do not navigate away from this page or refresh until it is complete.
</div>
<div id="editPane">
	<ul id="filesHolder" style="display:none;">
	<?
	$count = 0; 
	foreach($remoteFiles as $file) {
		if (substr($file, 2, 2) == '--') {
			$prettyName = substr($file, 4);
		} else {
			$prettyName = $file;
		}
		$html  = "";
     	$html .= '<li id="rebuildImg'.$count.'" class="rebuildImg">';
		$html .= $file;
		$html .= '</li>';
		$count++;
		echo $html;
	} ?>
	</ul>
	<div id="rebuildMessage" class="tip_message" style="display:none;">
		Rebuilding...
   	</div>                   
	<div id="curThumb" style="display:none; margin-bottom:20px;">
		
	</div>
	<button id="rebuild">Rebuild Thumbnails</button>
	<div style="clear:both;"></div>
</div> 
<script type="text/javascript">
// <![CDATA[ 

$("#rebuild").click(function() {
	$(this).hide();
 	$("#rebuildMessage").show();
	$("#curThumb").show(); 
	rebuildThumbnails(0);
});  

function rebuildThumbnails(num) {
	$.log('rebuildThumbnails: '+num);
	if (num >= $(".rebuildImg").length) {
		//
		// done
	   	$("#rebuild").show();
	    $("#rebuildMessage").html($(".rebuildImg").length+" thumbnails were rebuilt."); 
		$("#curThumb").delay(1000).fadeOut('slow');
	} else { 
		//
		var fileName = $("#rebuildImg"+num).text();
		$("#rebuildMessage").html("Rebuilding "+num+" of "+$(".rebuildImg").length);
		//
		$.ajax({
          	type: 'POST',
          	url:adminURL + '_bfadmin/rebuild_thumbnail.php',
          	data: ({'u':u,'p':p, 'file':fileName})
        }).done(function( msg ) {
		  	$.log( "msg: " + msg );
			var d = new Date(); 
		  	var imghtml = "<img src='"+tempURL+"gallery/small/"+fileName+"?time="+d.getTime()+"' />"; 
		  	$("#curThumb").html(imghtml);
		  	num++;
			rebuildThumbnails(num);
		});
	}
}

// ]]>
</script>
