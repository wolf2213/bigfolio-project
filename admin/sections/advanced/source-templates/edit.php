<?
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../../inc/db.inc.php");
include_once("../../../inc/functions.inc.php");
//
// Settings
$s = get_settings();

$template = $_GET['val']; 
$t = get_source_template($template);


?>

<div class="contentHeader">
	<h1>Editing: <?=str_replace('_',' ',$template)?></h1>
</div>

<div class="tip_message">
	Editing this area can cause your site to not function properly. Changes made here cannot be undone. If you need help, please contact <a href="http://support.bigfolio.com" target="_blank">support</a>.
</div>
<div id="editPane">
	<ul>
		<li>
			<label>Source</label>
			<textarea name="content" class="bfChangeListen" id="sourceContent"><?= htmlspecialchars($t['contents']);?></textarea>
			<input type="hidden" name="id" value="<?= $t['template_id']?>" />
		</li>
	</ul>
</div> 
<script type="text/javascript">
// <![CDATA[

// ]]>
</script>
