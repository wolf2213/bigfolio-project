<?
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php"); 
//
session_start();
//
// Settings
$s = get_settings(); 
$cats = get_all_categories();

// DB Connect for import info
//*****
$ilink = mysql_connect(DB_HOST, DB_USER, DB_PASS) or mysql_error();
$iconn = mysql_select_db(DB_NAME);
// See if there's an existing import for this user 
$db = $_SESSION['user']['db_name'];
$q  = "select * from `import_jobs` where `finished` = 0 and `db` = '$db'";
$check = mysql_query($q);
$rows = mysql_num_rows($check);
//***** 

?>

<div class="contentHeader">
	<h1>Import an Existing Site <sup>BETA</sup></h1>
</div>

<div class="tip_message">
	  <div id="importProgress" <? if ($rows > 0) { ?> style="display:none;" <? } ?>>
		  <h2>Import in Progress</h2>
		  <p>Your import is currently being processed. 
		    Your pages and galleries will be available in your site shortly.</p> 
	  </div> 
	  <div id="importGuide" <? if ($rows != 0) { ?> style="display:none;" <? } ?>> 
		  <p>You can import pages and galleries from some existing websites.</p>
		  <p>Compatible sites:</p>
		  <br>BluDomain Lucy
		  <br>BluDomain Grace
		  <br>BluDomain Lucille
		  <br>BluDomain Jinky
		  <br>BluDomain Madison
		  <br>BluDomain Audrey
	  </div>
</div>  
  <ul id="importSettings" <? if ($rows != 0) { ?> style="display:none;" <? } ?>>
	<li>
		<label>Type of site</label><br />
    	<select name="import_type" id="import_type">
      		<option value="lucy">BluDomain "Lucy"</option>
      		<option value="grace">BluDomain "Grace"</option>
  			<option value="lucille">BluDomain "Lucille"</option>
  			<option value="jinky">BluDomain "Jinky"</option>
  			<option value="madison">BluDomain "Madison"</option>
  			<option value="audrey">BluDomain "Audrey"</option>
    	</select> 
	</li>
	
	<li>
		<label>Choose a category for your imported galleries</label><br />
    	<select name="import_category" id="import_category">
  		<? foreach ($cats as $c) { ?>
		  <option value="<?= $c['category_id'] ?>"><?= $c['category_name'] ?></option>
		<? } ?>
    	</select>
	</li>
  
  	<li>
		<label>URL of site to import</label><br />
  		<input type="text" name="import_url" value="http://example.com" id="import_url" class="bfInputText"/> <div style="clear:both; margin-left: 220px;"><sub> (be sure to include the http://)</sub></div>
	</li>
  </ul> 

<button id="importSubmit">Submit</button>

<script type="text/javascript">
// <![CDATA[

$("#importSubmit").click(function() {
	var data = new Object();
	data['import_type'] = $("#import_type").val();
	data['import_category'] = $("#import_category").val();
	data['import_url'] = $("#import_url").val(); 
	$.ajax({
		//this is the php file that processes the data
		url: 'actions/advanced/import.save.php',	

		//POST method is used
		type: "POST",

		//pass the data			
		data: data,		

		//Do not cache the page
		cache: false,
           
		//success
		success: function (res) { 
			$.log('saveRoute: '+res);   		
			if (res.substr(0,7) == 'success') {					 
				$("#importProgress").slideDown('fast');
			    $("#importGuide").slideUp('fast');
				$("#importSettings").slideUp('fast');
			} else {
				alert(res);
			}			
		}		
	});
});

// ]]>
</script>
