<?
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");
//
$templates = get_templates();
if (isset($_GET['t'])) {
	$t = get_source_template($_GET['t']);
}

$s = get_settings();   

?>

<div class="contentHeader">
	<h1>Source Templates -</h1>  <a href="https://vimeo.com/146453339" target="_blank"><img src="img/watch-this-video2.png" border="0" style="position:absolute; margin-left:300px; margin-top:-50px;" /></a>
</div>

<div class="tip_message">
	Click on the template you want to edit.
</div>
<div id="editPane">
	
	<div id="sourceTemplates">
	<? $templateType = get_property('TEMPLATE_TYPE'); ?>
	<? foreach($templates as $file) {
		if (($templateType == 'HTML5' && $file['template_id'] != "MAIN_PAGE") || $templateType != 'HTML5') {
			$html  = "";
	     	$html .= '<div class="sourceTemplate">'; 
			if ($file['template_id'] != "MAIN_PAGE" && $file['template_id'] != "MAIN_ORIGINAL" && $file['template_id'] != "ENTRY_PAGE") {
				$html .= '<div class="deleteButton"></div>';                                      
			}
			$html .= '<div style="clear:both;"></div>';
			$html .= '<div class="sourceTemplateName">';
			// 
			$html .= '<a href="#/advanced/source-templates/'.$file['template_id'].'" title="'.$file['template_id'].'">';
			$html .= str_replace('_',' ',$file['template_id']);
			$html .= '</a>';
			//
			$html .= '</div>';
			$html .= '</div>'; 
			echo $html;
		}
	} ?>
	</div>
	<div style="clear:both;"></div>
	<button id="newTemplate">New Template</button>   
	<ul> 
		<li class="divider"></li>
		<li><label for="start_mode">Entry Page</label>
		<div class="uiButtonSet">
			<label for="start_mode_entry" class="radioLabel">On</label>
			<input name="start_mode" id="start_mode_entry" type="radio" value="ENTRY" class="bfChangeListen" <? if ($s['start_mode']=='ENTRY') { ?>checked="checked"<? } ?> />
			<label for="start_mode_flash" class="radioLabel">Off</label>
			<input name="start_mode" id="start_mode_flash" type="radio" value="FLASH" class="bfChangeListen" <? if ($s['start_mode']=='FLASH') { ?>checked="checked"<? } ?> />
		</div>
		</li>
	</ul>  
	<div style="clear:both;"></div>
</div> 
<script type="text/javascript">
// <![CDATA[ 

$("#newTemplate").click(function() {  
	var templateID = prompt("Enter the template name:"); 
	if (templateID.length <= 4) {
		alert("Please enter a template name longer than 4 characters.");
		return;
	} else if (templateID.toUpperCase().split(" ").join("_") == "MAIN_PAGE" || templateID.toUpperCase().split(" ").join("_") == "ENTRY_PAGE") {
		alert("That is a reserved template name. Please choose a different name.");
		return;
	}
	$.ajax({
       	type: 'POST',
       	url:'actions/advanced/create-source-template.php',
       	data: ({'templateID':templateID})
    }).done(function( msg ) {
	   	if (msg.substr(0,7) == "SUCCESS") {  
			//
			var templateName = msg.substr(8);
			//
			var tempaltehtml = "";
	     		tempaltehtml += '<div class="sourceTemplate">'; 
				tempaltehtml += '<div class="deleteButton"></div>';                                      
				tempaltehtml += '<div style="clear:both;"></div>';
				tempaltehtml += '<div class="sourceTemplateName">';
				// 
				tempaltehtml += '<a href="#/advanced/source-templates/'+templateName+'" title="'+templateName+'">';
				tempaltehtml += templateName.split("_").join(" ");
			 	tempaltehtml += '</a>';
				//
				tempaltehtml += '</div>';
			 	tempaltehtml += '</div>';
			//
			$("#sourceTemplates").append(tempaltehtml);
			//
			handleUI(); 
			sourceFunctionality();
	   	} else {
		   	alert('There was an error creating a new template. Please try again later. If problems persist, please contact support.');
		}
	});
});


function sourceFunctionality() {
	$("#sourceTemplates").unbind("click");  
	$("#sourceTemplates").find(".deleteButton").click(function() {
	   	var check = confirm("Are you sure you want to delete this? This cannot be undone.");
	 	if (check) {
		 	var item = $(this).parent();
			var templateID = item.find("a").attr("title");
			$.ajax({
		      type: 'POST',
		      url:'actions/advanced/remove-source-template.php',
		      data: ({'templateID':templateID})
		    }).done(function( msg ) {
			  	$.log( "msg: "+msg);
				if (msg.substr(0,7) == 'SUCCESS') {
					item.fadeOut("fast", function() {
					   item.remove(); 
					});
				} else {
					alert("There was an error removing the source template.");
				}
			});
		}
	});
}
sourceFunctionality();
// ]]>
</script>
