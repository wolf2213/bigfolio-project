<?
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");
//
// Settings
$s = get_settings();  

//
// get font files from remote server 
$postArr = array();
$postArr['u'] = $_SESSION['user']['email'];
$postArr['p'] = $_SESSION['user']['password'];
$postArr['mode'] = 'extra';
$url = SERVER . '_bfadmin/files_list.php'; // 
$remoteFiles = curl_post($url, $postArr, array()); 
$remoteFiles = json_decode($remoteFiles);  

?>

<div class="contentHeader">
	<h1>Extra Files -</h1>  <a href="https://vimeo.com/146453339" target="_blank"><img src="img/watch-this-video2.png" border="0" style="position:absolute; margin-left:180px; margin-top:-50px;" /></a>
</div>

<div class="tip_message">
	Your site includes a folder called <em>extra</em>. You can use this folder to store miscellaneous files, such as a PDF file. Please note that .jpg files should be stored in the image bank and video files should be stored in the video bank if present.
</div>
<div id="editPane">
	<div id="extraFilesHolder">
	<? foreach($remoteFiles as $file) {
		if (substr($file, 2, 2) == '--') {
			$prettyName = substr($file, 4);
		} else {
			$prettyName = $file;
		}
		$html  = "";
     	$html .= '<div class="extraFile">';
		$html .= '<div class="deleteButton"></div>';
		$html .= '<div style="clear:both;"></div>';
		$html .= '<div class="extraFileLabel">';
		$html .= '<a href="'.$_SESSION['user']['temp_url'].'extra/'.$file.'" target="_blank" title="'.$file.'">'.$prettyName.'</a>';
		$html .= '</div>';
		$html .= '</div>'; 
		echo $html;
	} ?>
	</div>
	<div style="clear:both;"></div>
	<input name="extrafiles" type="file" id="extrafiles" size="10" /> 
	<div id="status-message"></div>
	<div id="extra-queue"></div>
</div> 
<script type="text/javascript">
// <![CDATA[

//
// music uploader   
//array('jpeg','gif','png','pdf','doc','docx','ppt','txt','html','css','mp3','zip');
$("#extrafiles").uploadifive({
  'uploadScript'   : adminURL + '_bfadmin/file_upload.php',
  'method'         : 'post',
  'formData'       : {'u':u,'p':p,'mode':'extra'},      
  'cancelImg'      : 'uploadifive-cancel.png',
  'multi'          : true,
  'auto'           : true,
  'fileExt'        : '*.gif;*.png;*.pdf;*.doc;*.docx;*.ppt;*.txt;*.html;*.css;*.mp3.;*.zip;',
  'fileDesc'       : '',
  'queueID'        : 'extra-queue', 
  'buttonText'     : 'UPLOAD FILES',
  'width'          : 132,
  'queueSizeLimit' : 3,
  'simUploadLimit' : 1,
  'removeCompleted': true,
  'onSelectOnce'   : function(event,data) {
      	//$('#status-message').text(data.filesSelected + 'uploading...');
		$('#status-message').text('uploading...');
    }, 
  'onUploadComplete'     : function(file, data) { 
	//	var fileParts = getNameAndExtension(fileObj['name']); 
	//	var extraFile = cleanfile(fileParts[0])+'.'+fileParts[1];
		var extraFile = file.name;
	//  	$.log('fileName: '+cleanfile(fileParts[0])+'.'+fileParts[1]);
	//  	$.log('response: '+response);                            
		//
		if (fileParts[1] == 'jpg' || fileParts[1] == 'm4v' || fileParts[1] == 'mov' || fileParts[1] == 'mp4') {
			alert("Sorry that file type is not accepted here. If you need to upload .jpg or video files, please use the image bank or video bank respectively.");
			return;
		}
		//
		var html = "";
		html += '<div class="extraFile">';
		html += '<div class="deleteButton"></div>';
		html += '<div style="clear:both;"></div>';
		html += '<div class="extraFileLabel">'
		html += '<a href="'+tempURL+'extra/'+extraFile+'" title="'+extraFile+'" target="_blank" title="'+extraFile+'">'+extraFile+'</a>';
		html += '</div>';
		//
		$("#extraFilesHolder").append(html);
		extraFunctionality();
		//
	  	
   	},
  'onAllComplete'  : function(event,data) {
		$.log("filesUploaded: "+data.filesUploaded);
		$.log("data.errors: "+data.errors);
		if (data.errors > 0) {
      		$('#status-message').text("There was an error with the upload.");
		} else {
			$('#status-message').text("");
		}
    }
});




function extraFunctionality() {
	$("#extraFilesHolder").find(".deleteButton").unbind("click");
	$("#extraFilesHolder").find(".deleteButton").click(function() { 
			//
			var save = confirm("Are you sure you want to delete this file? This cannot be undone");
			if (save) {
				//
			  	var fileName = $(this).parent().find('a').attr("title");
			    $.ajax({
		          type: 'POST',
		          url:adminURL + '_bfadmin/file_remove.php',
		          data: ({'u':u,'p':p, 'mode':'extra', 'file':fileName})
		        }).done(function( msg ) {
				  $.log( "msg: " + msg );
				});
				//
				var thumb = $(this).parent();
				thumb.fadeOut('fast', function() {
				    thumb.remove();
				});
			}
			// 
	});
	
}

extraFunctionality();

// ]]>
</script>
