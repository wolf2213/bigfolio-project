<?
// Include files
include('db.inc.php');
include('define.php');
include('functions.inc.php');
session_start();
authorize_user();
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$pmode = $_POST['mode'];
	$sval = $_POST['Submit'];
	switch ($pmode) {
		case 'pages':
			if ($_POST['deletePage'] != 0) {
				$deleteID = $_POST['deletePage'];
				$success = delete_page($deleteID);
				mark_update();
				header("Location: ../page_admin.php?mode=pages");
			}
			if ($_POST['cgMode'] == 'pages') {
				$title = $_POST['new_page_title'];
				$hidden = 0;
				$order = $_POST['newPageOrder'];
				$q = "insert into pages (page_title, page_hidden, order_num) 
					  values ('$title', '$hidden', '$order')";
				$ins = mysql_query($q)
					or die(mysql_error());
				$key = mysql_insert_id();
				mark_update();
				header("Location: ../page_admin.php");
			}
			if ($_POST['cgMode'] == 'subpages') {
				$a = 'name_subpage_' . $_POST['pNum'];
				$b = 'newSubPageOrder_' . $_POST['pNum'];
				$title = $_POST[$a];
				$pId = $_POST['pId'];
				$order = $_POST[$b];
				$q = "insert into pages (page_title, parent_id, order_num) 
					  values ('$title', '$pId', '$order')";
				$ins = mysql_query($q)
					or die(mysql_error());
				$key = mysql_insert_id();
				mark_update();
				header("Location: ../page_admin.php");
			}
			if ($sval == 'Continue') {
				$title = $_POST['page_title'];
				$img = $_POST['page_image'];
				$par = $_POST['page_parent'];
				$order = $_POST['page_order'];
				$hidden = 0; // Pages are not hidden by default
				$q = "insert into pages (page_title, parent_id, order_num, page_image, page_hidden) 
					  values ('$title', '$par', '$order', '$img', '$hidden')";
				$ins = mysql_query($q)
					or die(mysql_error());
				$key = mysql_insert_id();
				mark_update();
				header("Location: ../page_admin.php?mode=edit&pid=$key");
			} 
			break;
			case 'subpages':
				if ($_POST['deletePage'] != 0) {
					$deleteID = $_POST['deletePage'];
					$success = delete_page($deleteID);
					mark_update();
					header("Location: ../page_admin.php?mode=subpages");
				}
				if ($sval == 'Continue') {
					$title = $_POST['page_title'];
					$img = $_POST['page_image'];
					$par = $_POST['page_parent_id'];
					$order = $_POST['page_order'];
					$q = "insert into pages (page_title, parent_id, order_num, page_image) 
						  values ('$title', '$par', '$order', '$img')";
					$ins = mysql_query($q)
						or die(mysql_error());
					$key = mysql_insert_id();
					mark_update();
					header("Location: ../page_admin.php?mode=subedit&pid=$key");
				} 
				break;
		case 'contact':
			if ($sval == 'Save Changes' || $sval == '') {
				$fieldset = array();
				for ($i=0;$i<7;$i++) {
					$item = array();
					$item["label"] = $_POST['label'.$i];
					$item["on"] = (isset($_POST['field'.$i])) ? 1 : 0;
					array_push($fieldset, $item);
				}
				$fieldset_s = serialize($fieldset);
				$cf = $_POST['contact_flash']; // Contact form is always on for Flash 
				$ch = $_POST['contact_html'];
				$add = $_POST['add_address'];
				$em = $_POST['add_email'];
				$ph = $_POST['add_phone'];
				$ex = $_POST['add_extra'];
				$extra = $_POST['contact_form_extra'];
				$sende = 1; // Always send to email address
				$sends = $_POST['contact_form_sms'];
				// reorder other pages
				$q = "update settings set contact_form_flash = '$cf', contact_form_html = '$ch', 
					  contact_form_email = '$sende', contact_form_sms = '$sends',
					  add_address = '$add', add_email = '$em', add_phone = '$ph', add_extra = '$ex',
					  contact_form_extra = '$extra',
					  contact_form_fields = '$fieldset_s'
					  where settings_id = 1";
				$upd = mysql_query($q)
					or die(mysql_error());		
				mark_update();		
				header("Location: ../page_admin.php?mode=$pmode");
			} else if ($sval == 'Upload') {
				//*****
				// DB Connect for single sign on
				//*****
				$link = mysql_connect(DB_HOST, DB_USER, DB_PASS) or mysql_error();
				$conn = mysql_select_db(DB_NAME);
				//*****
				
				$id = $_SESSION['user']['id'];
				$t = $_SESSION['user']['token'];
				$q = "select * from clients where id = '$id' and token = '$t' ";
				$get = mysql_query($q)
					or die('Bad query');
				$u = mysql_fetch_array($get, MYSQL_ASSOC);
				$file_path = $u['path'];
				
				$file_temp = $_FILES['contactfile']['tmp_name'];
				$file_name = 'contact.jpg';
				
				$filestatus = move_uploaded_file($file_temp,$file_path . "gallery/original/" . $file_name);
				$orig = $file_path . "gallery/original/" . $file_name;
				fitimage($orig, $file_path . "gallery/thumb/" . $file_name, 100, 100);
				fitimage($orig, $file_path . "gallery/small/" . $file_name, 300, 200);
				fitimage($orig, $file_path . "gallery/medium/" . $file_name, 600, 400);
				fitimage($orig, $file_path . "gallery/large/" . $file_name, 900, 600);
				fitimage($orig, $file_path . "gallery/square/" . $file_name, 75, 75, true);
				
				header("Location: ../page_admin.php?mode=contact");
			}
			break;
		case 'edit':
			if ($sval == 'Save Changes') {
				$pageid = $_POST['mypage'];
				$img = $_POST['page_image'];
				$title = $_POST['page_title'];
				$body = $_POST['page_text_html'];
				$html = $_POST['include_html'];
				$alt = $_POST['alt_html'];
				$atitle = $_POST['html_title'];
				$abody = $_POST['html_text'];
				$proof = $_POST['add_proofing'];
				$bg_mode = $_POST['bg_mode'];
				$bg_image = $_POST['bg_image'];
				$bg_alpha = $_POST['bg_alpha'];
				$hidden = $_POST['page_hidden'];
				if (substr($_POST['page_color_val'],0,1) == "#") {
					$color = $_POST['page_color_val'];
				} else {
					$color = "#" . $_POST['page_color_val'];
				}
				$opac = $_POST['page-opac'];
				if ($_SESSION['template'] == 'burnside' || $_SESSION['template'] == 'butler' || $_SESSION['template'] == 'atwood' || $_SESSION['template'] == 'york' || $_SESSION['template_name'] == 'Cascade Lakes Deluxe') {
					$q = "update pages set page_image = '$img', page_title = '$title', page_content = '$body', 
						  include_html = '$html', alt_html = '$alt', alt_title = '$atitle', 
						  alt_content = '$abody', add_proofing = '$proof', bg_mode = '$bg_mode', bg_alpha = '$bg_alpha', 
						  bg_image = '$bg_image', page_hidden = '$hidden', page_color = '$color', page_opacity = '$opac' where page_id = '$pageid'";
				} else {
					$q = "update pages set page_image = '$img', page_title = '$title', page_content = '$body', 
						  include_html = '$html', alt_html = '$alt', alt_title = '$atitle', 
						  alt_content = '$abody', add_proofing = '$proof', bg_mode = '$bg_mode', bg_alpha = '$bg_alpha', 
						  bg_image = '$bg_image', page_hidden = '$hidden' where page_id = '$pageid'";
				}
				
				$upd = mysql_query($q)
					or die(mysql_error());
				mark_update();
				header("Location: ../page_admin.php?mode=edit&pid=" . $pageid);
			} else if ($sval == 'Upload') {
				
				//*****
				// DB Connect for single sign on
				//*****
				$link = mysql_connect(DB_HOST, DB_USER, DB_PASS) or mysql_error();
				$conn = mysql_select_db(DB_NAME);
				//*****
				
				$id = $_SESSION['user']['id'];
				$t = $_SESSION['user']['token'];
				$q = "select * from clients where id = '$id' and token = '$t' ";
				$get = mysql_query($q)
					or die(mysql_error());
				$u = mysql_fetch_array($get, MYSQL_ASSOC);
				$file_path = $u['path'];
				
				$file_temp = $_FILES['mainimagefile']['tmp_name'];
				$file_name = $_FILES['mainimagefile']['name'];
				
				$filestatus = move_uploaded_file($file_temp,$file_path . "gallery/original/" . $file_name);
				$orig = $file_path . "gallery/original/" . $file_name;
				fitimage($orig, $file_path . "gallery/thumb/" . $file_name, 100, 100);
				fitimage($orig, $file_path . "gallery/small/" . $file_name, 300, 200);
				fitimage($orig, $file_path . "gallery/medium/" . $file_name, 600, 400);
				fitimage($orig, $file_path . "gallery/large/" . $file_name, 900, 600);
				fitimage($orig, $file_path . "gallery/square/" . $file_name, 75, 75, true);
				
				include('db.inc.php');
				
				$pid = $_POST['mainpid'];
				
				$q = "update pages set page_image = '$file_name' where page_id = '$pid'";
				$upd = mysql_query($q)
					or die(mysql_error());
				mark_update();
				header("Location: ../page_admin.php?mode=edit&pid=" . $pid);

			} else if ($sval == 'Upload Texture') {
				//*****
				// DB Connect for single sign on
				//*****
				$link = mysql_connect(DB_HOST, DB_USER, DB_PASS) or mysql_error();
				$conn = mysql_select_db(DB_NAME);
				//*****
				
				$id = $_SESSION['user']['id'];
				$t = $_SESSION['user']['token'];
				$q = "select * from clients where id = '$id' and token = '$t' ";
				$get = mysql_query($q)
					or die(mysql_error());
				$u = mysql_fetch_array($get, MYSQL_ASSOC);
				$file_path = $u['path'];
				
				$file_temp = $_FILES['maintexturefile']['tmp_name'];
				$file_name = $_FILES['maintexturefile']['name'];
				
				$filestatus = move_uploaded_file($file_temp,$file_path . "textures/" . $file_name);
				
				include('db.inc.php');
				
				$pid = $_POST['mainpid'];
				
				$q = "update pages set page_texture = '$file_name' where page_id = '$pid'";
				$upd = mysql_query($q)
					or die(mysql_error());
				mark_update();
				header("Location: ../page_admin.php?mode=edit&pid=" . $pid);
			} else if ($_POST['deletePageImage'] == '1') {
				$pid = $_POST['mainpid'];
				$q = "update pages set page_image = '' where page_id = '$pid'";
				$upd = mysql_query($q)
					or die(mysql_error());
				mark_update();
				header("Location: ../page_admin.php?mode=edit&pid=" . $pid);
			} else {
				$pageid = $_POST['mypage'];
				$img = $_POST['page_image'];
				$title = $_POST['page_title'];
				$body = $_POST['page_text_html'];
				$html = $_POST['include_html'];
				$alt = $_POST['alt_html'];
				$atitle = $_POST['html_title'];
				$abody = $_POST['html_text'];
				$proof = $_POST['add_proofing'];
				$bg_mode = $_POST['bg_mode'];
				$bg_image = $_POST['bg_image'];
				$bg_alpha = $_POST['bg_alpha'];
				$hidden = $_POST['page_hidden'];
				if (substr($_POST['page_color_val'],0,1) == "#") {
					$color = $_POST['page_color_val'];
				} else {
					$color = "#" . $_POST['page_color_val'];
				}
				$opac = $_POST['page-opac'];
				if ($_SESSION['template'] == 'burnside' || $_SESSION['template'] == 'butler' || $_SESSION['template'] == 'atwood' || $_SESSION['template'] == 'york' || $_SESSION['template_name'] == 'Cascade Lakes Deluxe') {
					$q = "update pages set page_image = '$img', page_title = '$title', page_content = '$body', 
						  include_html = '$html', alt_html = '$alt', alt_title = '$atitle', 
						  alt_content = '$abody', add_proofing = '$proof', bg_mode = '$bg_mode', bg_alpha = '$bg_alpha', 
						  bg_image = '$bg_image', page_hidden = '$hidden', page_color = '$color', page_opacity = '$opac' where page_id = '$pageid'";
				} else {
					$q = "update pages set page_image = '$img', page_title = '$title', page_content = '$body', 
						  include_html = '$html', alt_html = '$alt', alt_title = '$atitle', 
						  alt_content = '$abody', add_proofing = '$proof', bg_mode = '$bg_mode', bg_alpha = '$bg_alpha', 
						  bg_image = '$bg_image', page_hidden = '$hidden' where page_id = '$pageid'";	
				}
				$upd = mysql_query($q)
					or die(mysql_error());
				mark_update();
				header("Location: ../page_admin.php?mode=edit&pid=" . $pageid);
			}
			break;
			case 'subedit':
				if ($sval == 'Save Changes') {
					$pageid = $_POST['mypage'];
					$pid = $_POST['page_parent_id'];
					$img = $_POST['page_image'];
					$title = $_POST['page_title'];
					$body = $_POST['page_text_html'];
					$html = $_POST['include_html'];
					$alt = $_POST['alt_html'];
					$atitle = $_POST['html_title'];
					$abody = $_POST['html_text'];
					$proof = $_POST['add_proofing'];
					$bg_mode = $_POST['bg_mode'];
					$bg_image = $_POST['bg_image'];
					$bg_alpha = $_POST['bg_alpha'];
					$hidden = $_POST['page_hidden'];
					if (substr($_POST['page_color_val'],0,1) == "#") {
						$color = $_POST['page_color_val'];
					} else {
						$color = "#" . $_POST['page_color_val'];
					}
					$opac = $_POST['page-opac'];
					if ($_SESSION['template'] == 'burnside' || $_SESSION['template'] == 'butler' || $_SESSION['template'] == 'atwood' || $_SESSION['template'] == 'york' || $_SESSION['template_name'] == 'Cascade Lakes Deluxe') {
						$q = "update pages set page_image = '$img', page_title = '$title', page_content = '$body', 
							  include_html = '$html', alt_html = '$alt', alt_title = '$atitle', 
							  alt_content = '$abody', add_proofing = '$proof', bg_mode = '$bg_mode', bg_alpha = '$bg_alpha', 
							  bg_image = '$bg_image', page_hidden = '$hidden', page_color = '$color', page_opacity = '$opac' where page_id = '$pageid'";
					} else {
						$q = "update pages set page_image = '$img', page_title = '$title', page_content = '$body', 
							  include_html = '$html', alt_html = '$alt', alt_title = '$atitle', 
							  alt_content = '$abody', add_proofing = '$proof', bg_mode = '$bg_mode', bg_alpha = '$bg_alpha', 
							  bg_image = '$bg_image', page_hidden = '$hidden' where page_id = '$pageid'";
					}
					$upd = mysql_query($q)
						or die(mysql_error());
					mark_update();
					header("Location: ../page_admin.php");
				} else if ($sval == 'Upload') {
					
					//*****
					// DB Connect for single sign on
					//*****
					$link = mysql_connect(DB_HOST, DB_USER, DB_PASS) or mysql_error();
					$conn = mysql_select_db(DB_NAME);
					//*****
					
					$id = $_SESSION['user']['id'];
					$t = $_SESSION['user']['token'];
					$q = "select * from clients where id = '$id' and token = '$t' ";
					$get = mysql_query($q)
						or die(mysql_error());
					$u = mysql_fetch_array($get, MYSQL_ASSOC);
					$file_path = $u['path'];
					
					$file_temp = $_FILES['mainimagefile']['tmp_name'];
					$file_name = $_FILES['mainimagefile']['name'];
					
					$filestatus = move_uploaded_file($file_temp,$file_path . "gallery/original/" . $file_name);
					$orig = $file_path . "gallery/original/" . $file_name;
					fitimage($orig, $file_path . "gallery/thumb/" . $file_name, 100, 100);
					fitimage($orig, $file_path . "gallery/small/" . $file_name, 300, 200);
					fitimage($orig, $file_path . "gallery/medium/" . $file_name, 600, 400);
					fitimage($orig, $file_path . "gallery/large/" . $file_name, 900, 600);
					fitimage($orig, $file_path . "gallery/square/" . $file_name, 75, 75, true);
					
					include('db.inc.php');
					
					$pid = $_POST['mainpid'];
					
					$q = "update pages set page_image = '$file_name' where page_id = '$pid'";
					$upd = mysql_query($q)
						or die(mysql_error());
					mark_update();
					header("Location: ../page_admin.php?mode=subedit&pid=" . $pid);

				} else if ($sval == 'Upload Texture') {
						//*****
						// DB Connect for single sign on
						//*****
						$link = mysql_connect(DB_HOST, DB_USER, DB_PASS) or mysql_error();
						$conn = mysql_select_db(DB_NAME);
						//*****

						$id = $_SESSION['user']['id'];
						$t = $_SESSION['user']['token'];
						$q = "select * from clients where id = '$id' and token = '$t' ";
						$get = mysql_query($q)
							or die(mysql_error());
						$u = mysql_fetch_array($get, MYSQL_ASSOC);
						$file_path = $u['path'];

						$file_temp = $_FILES['maintexturefile']['tmp_name'];
						$file_name = $_FILES['maintexturefile']['name'];

						$filestatus = move_uploaded_file($file_temp,$file_path . "textures/" . $file_name);

						include('db.inc.php');

						$pid = $_POST['mainpid'];

						$q = "update pages set page_texture = '$file_name' where page_id = '$pid'";
						$upd = mysql_query($q)
							or die(mysql_error());
						mark_update();
						header("Location: ../page_admin.php?mode=subedit&pid=" . $pid);
				} else if ($_POST['deletePageImage'] == '1') {
					$pid = $_POST['mypage'];
					$q = "update pages set page_image = '' where page_id = '$pid'";
					$upd = mysql_query($q)
						or die(mysql_error());
					mark_update();
					header("Location: ../page_admin.php?mode=subedit&pid=" . $pid);
				} else {
					$pageid = $_POST['mypage'];
					$pid = $_POST['page_parent_id'];
					$img = $_POST['page_image'];
					$title = $_POST['page_title'];
					$body = $_POST['page_text_html'];
					$html = $_POST['include_html'];
					$alt = $_POST['alt_html'];
					$atitle = $_POST['html_title'];
					$abody = $_POST['html_text'];
					$proof = $_POST['add_proofing'];
					$bg_mode = $_POST['bg_mode'];
					$bg_image = $_POST['bg_image'];
					$bg_alpha = $_POST['bg_alpha'];
					$hidden = $_POST['page_hidden'];
					if (substr($_POST['page_color_val'],0,1) == "#") {
						$color = $_POST['page_color_val'];
					} else {
						$color = "#" . $_POST['page_color_val'];
					}
					$opac = $_POST['page-opac'];
					if ($_SESSION['template'] == 'burnside' || $_SESSION['template'] == 'butler' || $_SESSION['template'] == 'atwood' || $_SESSION['template'] == 'york' || $_SESSION['template_name'] == 'Cascade Lakes Deluxe') {
						$q = "update pages set page_image = '$img', page_title = '$title', page_content = '$body', 
							  include_html = '$html', alt_html = '$alt', alt_title = '$atitle', 
							  alt_content = '$abody', add_proofing = '$proof', bg_mode = '$bg_mode', bg_alpha = '$bg_alpha', 
							  bg_image = '$bg_image', page_hidden = '$hidden', page_color = '$color', page_opacity = '$opac' where page_id = '$pageid'";
					} else {
						$q = "update pages set page_image = '$img', page_title = '$title', page_content = '$body', 
							  include_html = '$html', alt_html = '$alt', alt_title = '$atitle', 
							  alt_content = '$abody', add_proofing = '$proof', bg_mode = '$bg_mode', bg_alpha = '$bg_alpha', 
							  bg_image = '$bg_image', page_hidden = '$hidden' where page_id = '$pageid'";	
					}
					$upd = mysql_query($q)
						or die(mysql_error());
					mark_update();
					header("Location: ../page_admin.php");
				}
				break;
	}			
}
?>