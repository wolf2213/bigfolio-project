<?
// Include files
include('db.inc.php');
include('functions.inc.php');
session_start();
authorize_user();
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$pmode = $_POST['mode'];
	$sval = $_POST['Submit'];
	switch ($pmode) {
		case 'upload':
			if ($sval == 'Upload') {
				$uploaddir = $_SESSION['user']['path'] . 'gallery/original/';
				for ($i = 0; $i < $_POST['numFiles']; $i++) {
					$uploadSuccess = move_uploaded_file($_FILES['userFile']['tmp_name'][$i], $uploaddir . $_FILES['userFile']['name'][$i]);
					if ($uploadSuccess) {
						// Create thumb
						$thumbfile = $_SESSION['user']['path'] . "gallery/thumb/".$_FILES['userFile']['name'][$i];
						$thumbsuccess = thumbnail($uploaddir.$_FILES['userFile']['name'][$i],$thumbfile,50);
						// Create small
						$thumbfile = $_SESSION['user']['path'] . "gallery/small/".$_FILES['userFile']['name'][$i];
						$thumbsuccess = thumbnail($uploaddir.$_FILES['userFile']['name'][$i],$thumbfile,180);
					}
				}
				// Mark update
				mark_update();
				header("Location: ../video_admin.php?mode=images");
			}
			break;
        case 'multi':
            if ($sval == 'Upload') {
                // Zip file will go in the upload dir (/gallery/upload)
				$uploaddir = "../../gallery/";
				// Rel path to zip file
				$zipfile = $uploaddir.$_FILES['zipFile']['name'];
				// Upload .zip
                $result = move_uploaded_file($_FILES['zipFile']['tmp_name'],$zipfile);
                // Absolute path for zip file
                $zipabs = '../../gallery/'.$_FILES['zipFile']['name'];
                // Absolute path to upload dir
                $upath = '../../gallery/upload';
                // Execute (via shell) unzip
                if ($result === true) {
                    $output = shell_exec("unzip -oj $zipabs -d $upath");
                }
                // Now traverse through the upload directory and copy/make thumb
                if ($handle = opendir('../../gallery/upload')) {
                    while (false !== ($file = readdir($handle))) {
                        if (substr($file,0,1) != '.' && (ereg(".swf",strtolower($file)) || ereg(".jpg",strtolower($file)) || ereg(".jpeg",strtolower($file)))) {
                            $fpath = '../../gallery/upload/'.$file;
                            $gpath = '../../gallery/'.$file;
                            $tpath = '../../gallery/thumbs/'.$file;
							$smpath = '../../gallery/small/'.$file;
                            $isize = getimagesize($fpath);
                            copy($fpath, $gpath);
                            if ($isize[2] == 2) {
                                $tok = thumbnail($fpath, $tpath, 50);
								$smok = thumbnail($fpath, $smpath, 180);
                            }
                            // clean up
                            unlink($fpath);
                        }
                    }
                }
                // Mark update
				mark_update();
                header("Location: ../video_admin.php?mode=images");
            }
            break;
		case 'bank':
			if ($_POST['deleteBankImage'] == '1') {
				$fileName = $_POST['deleteBankFile'];
				$gdir = $_SESSION['user']['path'] . 'videos/';
				// remove file
				if (file_exists($gdir.$fileName)) {
					unlink($gdir.$fileName);
				}
				mark_update();
				header("Location: ../video_admin.php?mode=bank");
			} else if ($sval == 'Go' && $_POST['image_action'] == 'del') { 
				$list = $_POST['ibank'];
				foreach($list as $image) {
					$gdir = $_SESSION['user']['path'] . 'gallery/original/';
					$sqdir = $_SESSION['user']['path'] . 'gallery/square/';
					$sdir = $_SESSION['user']['path'] . 'gallery/small/';
					$mdir = $_SESSION['user']['path'] . 'gallery/medium/';
					$ldir = $_SESSION['user']['path'] . 'gallery/large/';
					$tdir = $_SESSION['user']['path'] . 'gallery/thumb/';
					// get image info
					$isize = getimagesize($gdir.$image);
					// remove thumb file	
					if ($isize[2] == 2 && file_exists($tdir.$image)) {
						unlink($sqdir.$image);
						unlink($sdir.$image);
						unlink($mdir.$image);
						unlink($ldir.$image);
						unlink($tdir.$image);
					} else if (($isize[2] == 4 || $isize[2] == 13) && file_exists($tdir.$image.'.jpg')) {
						unlink($sqdir.$image.'.jpg');
						unlink($sdir.$image.'.jpg');
						unlink($mdir.$image.'.jpg');
						unlink($ldir.$image.'.jpg');
						unlink($tdir.$image.'.jpg');
					}
					// remove file
					if (file_exists($gdir.$image)) {
						unlink($gdir.$image);
					}
				}
				// Mark update
				mark_update();
				header("Location: ../video_admin.php");
			} else if ($sval == 'Save Changes') {
				$gid = $_POST['gallery'];
				$name = $_POST['name'];
				$cid = $_POST['category'];
				// Hidden and password protected
				$hidden = (isset($_POST['gallery_hidden'])) ? '1' : '0';
				$password = (isset($_POST['gallery_protect']) && strlen($_POST['gallery_password']) > 0) ? $_POST['gallery_password'] : '';
				// Rename gallery
				$q = "update v_galleries set gallery_name = '$name', category_id = '$cid', gallery_hidden = '$hidden', password = '$password' where gallery_id = '$gid'";
				$upd = mysql_query($q)
					or die(mysql_error());
				// Mark update
				mark_update();
				header("Location: ../video_edit.php?gid=$gid");
			} else if ($sval == 'Save Password') {
				$gid = $_POST['gallery'];
				$pass = $_POST['pass'];
				$q = "update v_galleries set password = '$pass' where gallery_id = '$gid'";
				$upd = mysql_query($q)
					or die(mysql_error());
				// Mark update
				mark_update();
				header("Location: ../video_edit.php?gid=$gid");
			} else if ($_POST['image_action'] == 'add') { 
				$list = $_POST['ibank'];
				$gid = $_POST['gallery'];
				$current = get_gallery_images($gid);				
				$or=count($current); // Order starts at current image count of gallery 
				foreach ($list as $img) {
					$q = "insert into videos(gallery_id,order_num,video_file) 
						  values ('$gid','$or','$img')";
					$ins = mysql_query($q)
						or die(mysql_error());
					$or++;
				}
				// Mark update
				mark_update();
				header("Location: ../video_edit.php?gid=$gid");
			} else if ($_POST['image_action'] == 'delete') { 
				$iid = $_POST['deleteImage'];
				$gid = $_POST['gallery'];
				$ok = delete_gallery_image($iid,$gid);	
				// Mark update
				mark_update();			
				header("Location: ../video_edit.php?gid=$gid");
			} else {
				while (list($key, $val) = each($_POST)) {
					echo "$key => $val\n";
				}
			}			
			break;
		case 'categories':
			if ($_POST['deleteCat'] > 0) {
				$cid = $_POST['deleteCat'];
				$ok = remove_category($cid);							
			} else if ($sval == 'Save') {
				// creating new category
				$cat = $_POST['name'];
				$order = $_POST['order'];
				$dupes = 2;
				if ($cat != '') {
					if (category_exists($cat)) {
						$ren = $cat . ' ' . $dupes;
						while(category_exists($ren)) {
							$dupes++;
							$ren = $cat . ' ' . $dupes;
						}				
						$cat = $ren;		
					}
					$q = "insert into v_categories (category_name, order_num) values ('$cat','$order')";
					$ins = mysql_query($q)
						or die(mysql_error());
				}
			} else if ($sval == 'Go') {
				$id = $_POST['renameID'];
				$rename = $_POST['rename'.$id];
				$q = "update v_categories set category_name = '$rename' where category_id = '$id'";
				$update = mysql_query($q)
					or die(mysql_error());
			}
			// Mark update
			mark_update();
			header("Location: ../video_admin.php?mode=categories");
			break;
		case 'editcat': 
			if ($sval == 'Save Changes') {
				$name = $_POST['name'];
				$id = $_POST['id'];
				$q = "update v_categories set category_name = '$name' where category_id = '$id'";
				$upd = mysql_query($q)
					or die(mysql_error());
				// Mark update
				mark_update();
				header("Location: ../video_admin.php?mode=categories");
			}
			break;
		case 'videos':
			if ($_POST['deleteGal'] > 0) {
				$gid = $_POST['deleteGal'];
				$ok = remove_video($gid);
				header("Location: ../video_admin.php?mode=videos");						
			} else if ($_POST['image_action'] == 'add') {
				$list = $_POST['ibank'];
				$gid = $_POST['gallery'];
				$current = get_videos($gid);				
				$or=count($current); // Order starts at current image count of gallery 
				foreach ($list as $img) {
					$q = "insert into videos(gallery_id,order_num,video_file) 
						  values ('$gid','$or','$img')";
					$ins = mysql_query($q)
						or die(mysql_error());
					$or++;
				}
				// Mark update
				mark_update();
				header("Location: ../video_edit.php?gid=$gid");
			} else if ($_POST['image_action'] == 'delete') {
				$iid = $_POST['deleteImage'];
				$gid = $_POST['gallery'];
				$ok = delete_gallery_video($iid,$gid);	
				// Mark update
				mark_update();			
				header("Location: ../video_edit.php?gid=$gid");
			} else if ($_POST['cgMode'] == 'galleries') {
				$a = 'name_gal_' . $_POST['galNum'];
				$name = $_POST[$a];
				$cid = $_POST['the_cat_id'];
				$q = "select count(*) as num from v_galleries where category_id = '$cid'";
				$query = mysql_query($q)
					or die(mysql_error());
				$row = mysql_fetch_array($query,MYSQL_ASSOC);
				$galorder = $row['num']+1;
				// Hidden and password protected
				$hidden = (isset($_POST['gallery_hidden'])) ? '1' : '0';
				$password = (isset($_POST['gallery_protect']) && strlen($_POST['gallery_password']) > 0) ? $_POST['gallery_password'] : '';
				// Now, insert gallery
				$q = "insert into v_galleries (category_id, gallery_name, order_num, gallery_hidden, password) values ('$cid','$name','$galorder','$hidden','$password')";
				$ins = mysql_query($q)
					or die(mysql_error());
					header("Location: ../video_admin.php?mode=videos");	
			} else if ($sval == 'Go') {
				$id = $_POST['renameID'];
				$rename = $_POST['rename'.$id];
				$q = "update v_galleries set gallery_name = '$rename' where gallery_id = '$id'";
				$update = mysql_query($q)
					or die(mysql_error());
					header("Location: ../video_admin.php?mode=videos");	
			} else 	if ($_POST['deleteCat'] > 0) {
				$cid = $_POST['deleteCat'];
				$ok = remove_vcategory($cid);
				header("Location: ../video_admin.php?mode=videos");	
			} else if ($_POST['cgMode'] == 'cats') {
				$cat = $_POST['name'];
				$order = $_POST['order'];
				$dupes = 2;
				// if ($cat != '') {
					if (category_exists($cat) && $cat != '') {
						$ren = $cat . ' ' . $dupes;
						while(category_exists($ren)) {
							$dupes++;
							$ren = $cat . ' ' . $dupes;
						}				
						$cat = $ren;		
					}
					$q = "insert into v_categories (category_name, order_num) values ('$cat','$order')";
					$ins = mysql_query($q)
						or die(mysql_error());
						header("Location: ../video_admin.php?mode=videos");	
			//	}
			} else if ($_POST['cgMode'] == 'editcats') {
				$a = 'e_name_' . $_POST['galNum'];
				$name = $_POST[$a];
				$cid = $_POST['the_cat_id'];
				$q = "update v_categories set category_name = '$name' where category_id = '$cid'";
				$upd = mysql_query($q)
					or die(mysql_error());
					header("Location: ../video_admin.php?mode=videos");	
			} else if ($_POST['cgMode'] == 'anchorGals') {
				$id = $_POST['anchorGal'];
				$q = "select * from v_categories where category_id = '$id'";
				$query = mysql_query($q)
					or die(mysql_error());
				$row = mysql_fetch_array($query,MYSQL_ASSOC);
				$which = $row['anchor'];
				if($which == 0) {
					$which = 1;
				} else {
					$which = 0;
				}
				$q = "update v_categories set anchor = '$which' where category_id = '$id'";
				$upd = mysql_query($q)
					or die(mysql_error());
					header("Location: ../video_admin.php?mode=videos");	
			}
			break;
		case 'image_details':
			$file = $_POST['file'];
			$cap = $_POST['caption'];
			$q = "update videos set image_caption = '$cap' where video_file = '$file'";
			$upd = mysql_query($q)
				or die(mysql_error());
			// Mark update
			mark_update();
			// Redirect
			header("Location: ../video_admin.php");
			break;
		case 'gal_rename':
			$id = $_POST['gal_id'];
			$pass = $_POST['gal_pass'];
			$hid = $_POST['gal_hidden'];
			if ($hid == 'on') {
				$hid = '1';
			} else {
				$hid = '0';
			}
			$redirect = $_POST['gal_redirect'];
			$name = $_POST['gal_name'];
			$q = "update v_galleries set gallery_name = '$name', password = '$pass', gallery_hidden = '$hid', gallery_redirect = '$redirect' where gallery_id = '$id'";
			$upd = mysql_query($q)
				or die(mysql_error());
			// Mark update
			mark_update();
			// Redirect
			header("Location: ../video_edit.php?gid=".$id);
			break;
	}
}
?>
