<?
// This file reorders the page via AJAX call
$return = '';
include('define.php');
if (!isset($_POST['intro'])) {
	$return = '<p>Error</p>';
} else {
	$order = 1;
	foreach($_POST['intro'] as $c) {
		$f = explode('--', $c);
		// See if the images already have been sorted (they will have a NUM--XYZ format with a "--" in the name)
		if (count($f) == 1) {
		  $fileName = $f[0];
		} else {
		  $f = array_slice($f, 1);
		  $fileName = implode('--', $f);
		}
		// Set the order and pad it 
		if ($order <= 9) {
			$num = '0' . $order;
		} else {
			$num = $order;
		}		
		// Set the file names
		$f = $_SESSION['user']['path'] . 'extra/' . $num . '--' . $fileName;		
		$oldFileName = $_SESSION['user']['path'] . 'extra/' . $c;
		$r = rename($oldFileName , $f);
		$order++;
	}
	$return .= '<p>Intro order updated.</p>';
}
echo $return;
?>