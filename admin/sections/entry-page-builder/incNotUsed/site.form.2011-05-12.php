<?
// Include files
include('db.inc.php');
include('define.php');
include('functions.inc.php');
session_start();
authorize_user();
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$pmode = $_POST['mode'];
	$sval = $_POST['Submit'];
	switch ($pmode) {
		case 'profile':
			$name = $_POST['name'];
			$biz = $_POST['bizname'];
			$em = $_POST['email'];
			$ph = $_POST['phone'];
			$mob = $_POST['mobile_phone'];
			$mdom = $_POST['mobile_domain'];
			$mprov = $_POST['mobile_provider'];
			$add = $_POST['address'];
			$proof = $_POST['proof'];
			$proofid = $_POST['proof_id'];
			if ($proof == 'NEXTPROOF' && substr($proofid,0,7) != 'http://') {
				$proofid = 'http://'.$proofid;
			}
			$copyright = $_POST['copyright'];
			$email_a_friend = $_POST['email_a_friend'];
			$esubj = $_POST['email_subj'];
			$emsg = $_POST['email_msg'];
			$q = "update settings set owner_name = '$name', business_name = '$biz', 
				  email = '$em', phone = '$ph', mobile_phone = '$mob', mobile_domain = '$mdom', mobile_provider = '$mprov', 
				  street_address = '$add', proofing = '$proof', proofing_id = '$proofid', copyright = '$copyright', email_a_friend = '$email_a_friend', email_subj = '$esubj', email_msg = '$emsg'";
			if (isset($_POST['social_media'])) {
				$smedia = $_POST['social_media'];
				$q .= ", social_media = '$smedia'";
			}
			$q .= " where settings_id = 1";
			$upd = mysql_query($q)
				or die(mysql_error());
			$_SESSION['message'] = "Your site was updated successfully.";
			header("Location: ../site_admin.php?mode=$pmode");
			break;
		case 'browser':
			//if ($sval == 'Save Changes') {
				$q = "update settings set browser_title = '$_POST[btitle]', meta_keywords = '$_POST[keywords]', 
					  meta_description = '$_POST[description]', start_mode = '$_POST[start_mode]' 
					  where settings_id = 1";
				$upd = mysql_query($q)
					or die(mysql_error());
				$_SESSION['message'] = "Your site was updated successfully.";				
				header("Location: ../site_admin.php?mode=$pmode");
			//}
			break;
		case 'title': 
			if (isset($_POST['Upload'])) {
				//*****
				// DB Connect for single sign on
				//*****
				$link = mysql_connect(DB_HOST, DB_USER, DB_PASS) or mysql_error();
				$conn = mysql_select_db(DB_NAME);
				//*****

				$id = $_SESSION['user']['id'];
				$t = $_SESSION['user']['token'];
				$q = "select * from clients where id = '$id' and token = '$t' ";
				$get = mysql_query($q)
					or die('Bad query');
				$u = mysql_fetch_array($get, MYSQL_ASSOC);
				$file_path = $u['path'];

				$file_temp = $_FILES['logofile']['tmp_name'];
				$file_name = $_FILES['logofile']['name'];
 			
				$filestatus = move_uploaded_file($file_temp,$file_path . "images/" . $file_name);
				// Set logo file in database
				include('db.inc.php');
				$q = "update settings set logo_file = '$file_name' where settings_id = 1";
				$upd = mysql_query($q)
					or die(mysql_error());
				header("Location: ../site_admin.php?mode=$pmode");
			}  else if ($sval == 'Save Changes') {
				// Set title settings in db
				$q = "update settings set title_mode = '$_POST[title_mode]', site_title='$_POST[site_title]'
					  where settings_id = 1";
				$upd = mysql_query($q)
					or die(mysql_error());
				header("Location: ../site_admin.php?mode=$pmode");
			} else {
				$q = "update settings set title_mode = '$_POST[title_mode]', site_title='$_POST[site_title]'
					  where settings_id = 1";
				$upd = mysql_query($q)
					or die(mysql_error());
				header("Location: ../site_admin.php?mode=$pmode");
			}
			break;
		case 'colors':
			if ($sval == 'Upload Image') {
				$file_temp = $_FILES['palettefile']['tmp_name'];
				$file_name = $_FILES['palettefile']['name'];
				//
				$filestatus = move_uploaded_file($file_temp,$_SESSION['user']['path'] . "images/palette.jpg");
				//
				header("Location: ../site_admin.php?mode=$pmode");
			} else {
			$page = '#'.$_POST['page_color'];
			$body = '#'.$_POST['body_color'];
			$app = '#'.$_POST['second_color'];
			$title = '#'.$_POST['title_color'];
			$border = '#'.$_POST['border_color'];
			$text = '#'.$_POST['text_color'];
			$custom_a = '#'.$_POST['custom1'];
			$custom_b = '#'.$_POST['custom2'];
			$custom_c = '#'.$_POST['custom3'];
			$q = "update settings set 
				  page_color = '$page', 
				  body_color = '$body', 
				  app_color = '$app', 
				  title_color = '$title',
				  app_border_color = '$border',
				  text_color='$text',
				  custom_color_1='$custom_a',
				  custom_color_2='$custom_b',
				  custom_color_3='$custom_c'
				  where settings_id = 1";
			$upd = mysql_query($q)
				or die(mysql_error());				
			header("Location: ../site_admin.php?mode=$pmode");
			}
			break;
		case 'fonts':
				//if ($_POST['modifier'] == 'Save Modifiers') {
					$t = $_POST['title_font_modifier'];
					$n = $_POST['nav_font_modifier'];
					$p = $_POST['page_font_modifier'];
					
					$s = get_settings();
					$fvalues = unserialize($s['flash_vars']);
					$fvars = split(',',get_property('FLASH_VARS'));
					$update = array();
					foreach ($fvars as $var) {
						if ($var == 'title_font_modifier') {
							$update[$var] = $t;
						} else if ($var == 'nav_font_modifier') {
							$update[$var] = $n;
						} else if ($var == 'page_font_modifier')  {
							$update[$var] = $p;
						} else {
							$update[$var] = $fvalues[$var];
						}
					}
					$update_ser = serialize($update);
					$q = "update `settings` set flash_vars = '$update_ser' where settings_id = '1'";
					$ins = mysql_query($q) or die (mysql_error());
				//} else {
					$sTitle = $_POST['title_font'];
					$nItems = $_POST['navigation_font'];
					$pTitles =$_POST['page_font'];
					$q = "update settings set 
						  title_font = '$sTitle', 
						  navigation_font = '$nItems', 
						  page_font = '$pTitles'
						  where settings_id = 1";
					$upd = mysql_query($q)
						or die(mysql_error());		
				//}
				header("Location: ../site_admin.php?mode=$pmode");
				break;
		case 'background':
			if ($sval == 'Upload Texture') {
				
				$file_name = $_FILES['xfile']['name'];

				$file_path = $_SESSION['user']['path'];
				$filestatus = move_uploaded_file($_FILES['xfile']['tmp_name'],$file_path . "gallery/original/" . $file_name);
				$orig = $file_path . "gallery/original/" . $file_name;
				
				fitimage($orig, $file_path . "gallery/thumb/" . $file_name, 100, 100);
				fitimage($orig, $file_path . "gallery/small/" . $file_name, 300, 200);
				fitimage($orig, $file_path . "gallery/medium/" . $file_name, 600, 400);
				fitimage($orig, $file_path . "gallery/large/" . $file_name, 900, 600);
				fitimage($orig, $file_path . "gallery/square/" . $file_name, 75, 75, true);
				
				$s = get_settings();
				$fvalues = unserialize($s['flash_vars']);
				$fvars = split(',',get_property('FLASH_VARS'));
				$update = array();
				foreach ($fvars as $var) {
					if ($var == 'texture_file') {
						$update[$var] = $_FILES['xfile']['name'];
					} else if ($var == 'Texture_File_Name') {
						$update[$var] = $_FILES['xfile']['name'];
					} else if ($var == 'texture_file_name') {
						$update[$var] = $_FILES['xfile']['name'];
					} else {
						$update[$var] = $fvalues[$var];
					}
				}
				$update_ser = serialize($update);
				$q = "update `settings` set flash_vars = '$update_ser' where settings_id = '1'";
				$ins = mysql_query($q) or die (mysql_error());
				header("Location: ../site_admin.php?mode=$pmode");
			} else if ($sval == 'Upload Secondary Texture') {
					//*****
					// DB Connect for single sign on
					//*****
					$link = mysql_connect(DB_HOST, DB_USER, DB_PASS) or mysql_error();
					$conn = mysql_select_db(DB_NAME);
					//*****

					$id = $_SESSION['user']['id'];
					$t = $_SESSION['user']['token'];
					$q = "select * from clients where id = '$id' and token = '$t' ";
					$get = mysql_query($q)
						or die(mysql_error());
					$u = mysql_fetch_array($get, MYSQL_ASSOC);
					$file_path = $u['path'];

					$file_temp = $_FILES['secondarytexturefile']['tmp_name'];
					$file_name = $_FILES['secondarytexturefile']['name'];

					$filestatus = move_uploaded_file($file_temp,$file_path . "secondary_textures/" . $file_name);

					include('db.inc.php');

					$s = get_settings();
					$fvalues = unserialize($s['flash_vars']);
					$fvars = split(',',get_property('FLASH_VARS'));
					$update = array();
					foreach ($fvars as $var) {
						if ($var == 'secondary_texture') {
							$update[$var] = $_FILES['secondarytexturefile']['name'];
						} else {
							$update[$var] = $fvalues[$var];
						}
					}
					$update_ser = serialize($update);
					$q = "update `settings` set flash_vars = '$update_ser' where settings_id = '1'";
					$ins = mysql_query($q) or die (mysql_error());
					header("Location: ../site_admin.php?mode=$pmode");
			} else if ($sval == 'Upload') {
				if ($_SESSION['template'] != 'lapine' && $_SESSION['template'] != 'bandon') {
							
					$uploaddir = $_SESSION['user']['path'] . 'extra/';
					$filename = $uploaddir . $_FILES['yfile']['name'];
					$uploadSuccess = move_uploaded_file($_FILES['yfile']['tmp_name'], $filename);
				
					$introFileNum = count(get_intro_files());
				
					if ($introFileNum == 0 || empty($introFileNum)) {
						$introFileNumStr = '01';
					}
				
					if (strlen($introFileNum) == 1) {
						$introFileNumStr = '0' . $introFileNum;
					}
					
					$introFileNumStr = $introFileNumStr . '_';
					$newFileName = $uploaddir . $introFileNumStr . $_FILES['yfile']['name'];
					$r = rename($filename , $newFileName);
				
					if ($introFileNum > 1) {
						$q = "update settings set background_image = '0' where settings_id = 1";
						$upd = mysql_query($q)
							or die(mysql_error());
					} else if ($introFileNum == 1) {
						$q = "update settings set background_image = '1' where settings_id = 1";
						$upd = mysql_query($q)
							or die(mysql_error());
						
						$e = get_extra_files();
						$copy = copy($newFileName, $_SESSION['user']['path'] . 'images/' . $e[0][0]); //copy($_SESSION['user']['path'] . 'extra/' . $e[0][0], $_SESSION['user']['path'] . 'images/' . $e[0][0]);
						$rename = rename( $_SESSION['user']['path'] . 'images/' . $e[0][0], $_SESSION['user']['path'] . 'images/background.jpg' );
						$thumbok = fitimage($_SESSION['user']['path'] . 'images/background.jpg', $_SESSION['user']['path'] . "images/background.small.jpg",600,400);	
					}
				
				} else {
					
					$uploaddir = $_SESSION['user']['path'] . 'images/';
					$filename = $uploaddir . $_FILES['yfile']['name'];
					$uploadSuccess = move_uploaded_file($_FILES['yfile']['tmp_name'], $filename);
					$rename = rename( $_SESSION['user']['path'] . 'images/' . $_FILES['yfile']['name'], $_SESSION['user']['path'] . 'images/background.jpg' );
					$thumbok = fitimage($_SESSION['user']['path'] . 'images/background.jpg', $_SESSION['user']['path'] . "images/background.small.jpg",600,400);
					
				}
				header("Location: ../site_admin.php?mode=$pmode");
			} else if ($sval == 'Save Changes' || $sval == '') {			
				$q = "update settings set background_image = '$_POST[showimage]' where settings_id = 1";
				$upd = mysql_query($q)
					or die(mysql_error());
				header("Location: ../site_admin.php?mode=$pmode");
			}
			//echo($_SESSION['user']['path']);
			break;
		case 'entry':
			//*****
			// DB Connect for single sign on
			//*****
			$link = mysql_connect(DB_HOST, DB_USER, DB_PASS) or mysql_error();
			$conn = mysql_select_db(DB_NAME);
			//*****
			
			$id = $_SESSION['user']['id'];
			$t = $_SESSION['user']['token'];
			$q = "select * from clients where id = '$id' and token = '$t' ";
			$get = mysql_query($q)
				or die('Bad query');
			$u = mysql_fetch_array($get, MYSQL_ASSOC);
			$file_path = $u['path'];
			
			$file_temp = $_FILES['userFile']['tmp_name'];
			$file_name = $_FILES['userFile']['name'];
			
			$filestatus = move_uploaded_file($file_temp,$file_path . "intro/intro.swf");
			
			header("Location: ../site_admin.php?mode=$pmode");
			break;
		case 'vars':
			$s = get_settings();
			$fvars = split(',',get_property('FLASH_VARS'));
			$update = array();
			foreach ($fvars as $var) {
				$val = $_POST['var_'.$var];
				$val = str_replace("\'", "", $val);
				$val = str_replace('\"', '', $val);
				$update[$var] = $val;
			}
			$update_ser = serialize($update);
			$q = "update `settings` set flash_vars = '$update_ser' where settings_id = '1'";
			$ins = mysql_query($q)
				or die(mysql_error());
			header("Location: ../site_admin.php?mode=$pmode");
			break;
		case 'music':
			if ($sval == 'delMusic') {
				$directory = $_SESSION['user']['path'] . 'music/';
				$file = $directory . $_POST['delMusicFile'];
				echo $file;
				$success = @unlink($file);
				//header("Location: ../site_admin.php?mode=music");
			} else {
				$file_temp = $_FILES['musicfile']['tmp_name'];
				$file_name = $_FILES['musicfile']['name'];
				//
				$filestatus = move_uploaded_file($file_temp,$_SESSION['user']['path'] . "music/" . $file_name);
				//
				header("Location: ../site_admin.php?mode=$pmode");
			}
			break;
		case 'delMusic':
		
			$directory = $_SESSION['user']['path'] . 'music/';
			$file = $directory . $_POST['delMusicFile'];
			$success = @unlink($file);
			header("Location: ../site_admin.php?mode=music");
			break;
			
		case 'del':
				
			$directory = $_SESSION['user']['path'] . 'extra/';
			$file = $directory . $_POST['delIntroFile'];
			$success = @unlink($file);
			
			$introFileNum = count(get_intro_files());
			if ($introFileNum > 1) {
				$q = "update settings set background_image = '0' where settings_id = 1";
				$upd = mysql_query($q)
					or die(mysql_error());
			} else if ($introFileNum == 1) {
				$q = "update settings set background_image = '1' where settings_id = 1";
				$upd = mysql_query($q)
					or die(mysql_error());
					
				$e = get_extra_files();
				$copy = copy($_SESSION['user']['path'] . 'extra/' . $e[0][0], $_SESSION['user']['path'] . 'images/' . $e[0][0]);
				$rename = rename( $_SESSION['user']['path'] . 'images/' . $e[0][0], $_SESSION['user']['path'] . 'images/background.jpg' );
			} else if ($introFileNum < 1) {
				$q = "update settings set background_image = '0' where settings_id = 1";
				$upd = mysql_query($q)
					or die(mysql_error());
			}
			
			header("Location: ../site_admin.php?mode=background");
			break;
			
		case 'delTex':
			$s = get_settings();
			$fvalues = unserialize($s['flash_vars']);
			$fvars = split(',',get_property('FLASH_VARS'));
			$update = array();
			foreach ($fvars as $var) {
				if ($var == 'texture_file') {
					$update[$var] = '';
				} else if ($var == 'Texture_File_Name') {
					$update[$var] = '';
				} else if ($var == 'texture_file_name') {
					$update[$var] = '';
				} else {
					$update[$var] = $fvalues[$var];
				}
			}
			$update_ser = serialize($update);
			$q = "update `settings` set flash_vars = '$update_ser' where settings_id = '1'";
			$ins = mysql_query($q) or die (mysql_error());
			header("Location: ../site_admin.php?mode=background");
			break;
	}			
}
?>