<?
// Include files
include('db.inc.php');
include('functions.inc.php');
session_start();
authorize_user();
// Turn off timeout for this - in case of thumb processing
set_time_limit(0);
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$mode = $_POST['mode'];
	$sub = $_POST['Submit'];
	$a = $_POST['amode'];
	if ($mode == 'extra') {
		if ($sub = 'Upload') {
			// User is uploading an extra file
			// move it to the extra folder
			$uploaddir = $_SESSION['user']['path'] . 'extra/';
			$filename = $uploaddir . $_FILES['xfile']['name'];
			$uploadSuccess = move_uploaded_file($_FILES['xfile']['tmp_name'], $filename);
			// Redirect back to page
			header("Location: ../advanced_admin.php?mode=extra");
		}
	} else if ($mode == 'del')  {
		// user is deleting an extra file
		$directory = $_SESSION['user']['path'] . 'extra/';
		$file = $directory . $_POST['delFile'];
		$success = @unlink($file);
		// Redirect back to extra mode of adv. page
		header("Location: ../advanced_admin.php?mode=extra");
	} else if ($mode == 'import') {
	  $url = $_POST['import_url'];
	  $type = $_POST['import_type'];
	  $c = $_POST['import_category'];
	  $test = preg_match('@^(http://|https://)[a-zA-Z0-9\./]+$@i',$url);
	  if ($test == 0) {
	    $_SESSION['import_return_value'] = "It doesn't appear you entered a valid URL. Don't forget the http://";
  	  header("Location: ../advanced_admin.php?mode=import&success=1");
	  } else {
	    $path = $_SESSION['user']['path'];
	    $type = $_POST['import_type'];
	    $db_user = $_SESSION['user']['db_user'];
      $db_pass = $_SESSION['user']['db_pass'];
      $db_name = $_SESSION['user']['db_name'];
      
      include('inc/define.php');
      //*****
    	// DB Connect for import info 
    	//*****
    	$ilink = mysql_connect(DB_HOST, DB_USER, DB_PASS) or mysql_error();
    	$iconn = mysql_select_db(DB_NAME);
    	// Insert the import job
    	$q  = "INSERT INTO `import_jobs` (`id`,`url`,`template`,`category`,`path`,`dbu`,`dbp`,`db`,`created_at`) VALUES (NULL,'$url', '$type', '$c', '$path', '$db_user', '$db_pass', '$db_name', NOW())";
    	$ins = mysql_query($q);
    	//*****
      
  	  header("Location: ../advanced_admin.php?mode=import");
	  }
	} else if ($mode == 'thumbs') {
		// Rebuild the thumbnails
		$source = $_SESSION['user']['path'] . "gallery/original/";
		// Traverse the source dir and copy files
		if ($handle = opendir($source)) {
			$counter = 0;
			$filesArr = array();
			while (false !== ($file = readdir($handle))) {
				$filesArr[] = $file;
			}
			closedir($handle);
			sort($filesArr);
			foreach($filesArr as $file) {
			//while (false !== ($file = readdir($handle))) {
				//echo('file there already: '.(!file_exists($_SESSION['user']['path'] . "gallery/thumb/" . $file)));
				if (ereg(".jpg",strtolower($file)) or ereg(".jpeg",strtolower($file)) or ereg(".gif",strtolower($file)) or ereg(".png",strtolower($file))) {
					$orig = $_SESSION['user']['path'] . "gallery/original/" . $file;
					//if (!file_exists($_SESSION['user']['path'] . "gallery/thumb/" . $file)) {
						//echo('not there: '.$file.'<br/>');
						
						$origw=imagesx($orig); 
				    	$origh=imagesy($orig);
				        
				        //if ($origw > 2000 || $origh > 1333) {
						
							fitimage($orig, $_SESSION['user']['path'] . "gallery/thumb/" . $file, 100, 100);
							fitimage($orig, $_SESSION['user']['path'] . "gallery/small/" . $file, 300, 200);
							fitimage($orig, $_SESSION['user']['path'] . "gallery/medium/" . $file, 600, 400);
							fitimage($orig, $_SESSION['user']['path'] . "gallery/large/" . $file, 900, 600);
							fitimage($orig, $_SESSION['user']['path'] . "gallery/square/" . $file, 75, 75, true);
						
						//} 
					//
					//echo('testing...');
					//} else {
					   // echo('already there: '.$file.'<br/>');
					//}
					
					$counter++;
				}
			}
		}
		// Redirect back to thumbs mode of adv. page
		header("Location: ../advanced_admin.php?mode=thumbs&success=$counter");
	} else if ($mode == 'templates') {
		$id = $_POST['id'];
		$stuff = $_POST['content'];
		if ($id == 'NEW') {
			$name = strtoupper(preg_replace("/\W/", "_", $_POST['tname']));
			$type = $_POST['ttype'];
			if (strlen($name) < 4) { 
				// Template name (ID) must be at least 4 characters
				header("Location: ../advanced_admin.php?mode=templates");
			} else {
				create_new_template($name, $type, $stuff);
			}
		} else {
			save_template($id,$stuff);
		}
		header("Location: ../advanced_admin.php?mode=templates");
	} else if ($mode == 'code') {
		// This is coming from the Stats page and we'll just update the 
		// stats code in settings
		$code = $_POST['statscode'];
		$q = "update `settings` set stats_code = '$code' where settings_id = '1'";
		$ins = mysql_query($q)
			or die(mysql_error());
		header("Location: ../stats_admin.php?mode=code");
	} else if ($mode == 'vars') {
		$s = get_settings();
		$fvars = split(',',get_property('FLASH_VARS'));
		$update = array();
		foreach ($fvars as $var) {
			$update[$var] = $_POST['var_'.$var];
		}
		$update_ser = serialize($update);
		$q = "update `settings` set flash_vars = '$update_ser' where settings_id = '1'";
		$ins = mysql_query($q)
			or die(mysql_error());
		header("Location: ../advanced_admin.php?mode=vars");
	}
}
?>