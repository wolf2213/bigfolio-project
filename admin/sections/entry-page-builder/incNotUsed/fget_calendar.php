<?
include('db.inc.php');
include('define.php');
include('functions.inc.php');
session_start();
authorize_user();

$events = get_calendar_events();
header("Content-type: text/xml");
$calXML  = "<?xml version=\"1.0\"?>\n";
$calXML .= "<site>\n";
$calXML .= "\t<calendar_events>\n";
foreach($events as $event_data) {
	$calXML .= "\t\t<event>\n";
	while(list($key, $val) = each($event_data)) {
		$calXML .= "\t\t\t<$key>$val</$key>\n";
	}
	$calXML .= "\t\t</event>\n";
}
$calXML .= "\t</calendar_events>\n";
$calXML .= "</site>\n";
echo $calXML;

?>