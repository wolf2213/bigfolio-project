<?
// Include files
include('db.inc.php');
include('define.php');
include('functions.inc.php');
include('ical.php');
session_start();
authorize_user();
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$pmode = $_POST['mode'];
	$sval = $_POST['Submit'];
	switch ($pmode) {
		case 'pages':
			if ($_POST['deletePage'] != 0) {
				$deleteID = $_POST['deletePage'];
				$success = delete_page($deleteID);
				mark_update();
				header("Location: ../page_admin.php?mode=pages");
			}
			if ($_POST['cgMode'] == 'pages') {
				$title = $_POST['new_page_title'];
				$hidden = 0;
				$order = $_POST['newPageOrder'];
				$q = "insert into pages (page_title, page_hidden, order_num) 
					  values ('$title', '$hidden', '$order')";
				$ins = mysql_query($q)
					or die(mysql_error());
				$key = mysql_insert_id();
				mark_update();
				header("Location: ../page_admin.php");
			}
			if ($_POST['cgMode'] == 'subpages') {
				$a = 'name_subpage_' . $_POST['pNum'];
				$b = 'newSubPageOrder_' . $_POST['pNum'];
				$title = $_POST[$a];
				$pId = $_POST['pId'];
				$order = $_POST[$b];
				$q = "insert into pages (page_title, parent_id, order_num) 
					  values ('$title', '$pId', '$order')";
				$ins = mysql_query($q)
					or die(mysql_error());
				$key = mysql_insert_id();
				mark_update();
				header("Location: ../page_admin.php");
			}
			if ($sval == 'Continue') {
				$title = $_POST['page_title'];
				$img = $_POST['page_image'];
				$par = $_POST['page_parent'];
				$order = $_POST['page_order'];
				$hidden = 0; // Pages are not hidden by default
				$q = "insert into pages (page_title, parent_id, order_num, page_image, page_hidden) 
					  values ('$title', '$par', '$order', '$img', '$hidden')";
				$ins = mysql_query($q)
					or die(mysql_error());
				$key = mysql_insert_id();
				mark_update();
				header("Location: ../page_admin.php?mode=edit&pid=$key");
			} 
			break;
			case 'subpages':
				if ($_POST['deletePage'] != 0) {
					$deleteID = $_POST['deletePage'];
					$success = delete_page($deleteID);
					mark_update();
					header("Location: ../page_admin.php?mode=subpages");
				}
				if ($sval == 'Continue') {
					$title = $_POST['page_title'];
					$img = $_POST['page_image'];
					$par = $_POST['page_parent_id'];
					$order = $_POST['page_order'];
					$q = "insert into pages (page_title, parent_id, order_num, page_image) 
						  values ('$title', '$par', '$order', '$img')";
					$ins = mysql_query($q)
						or die(mysql_error());
					$key = mysql_insert_id();
					mark_update();
					header("Location: ../page_admin.php?mode=subedit&pid=$key");
				} 
				break;
		case 'contact':
			if ($sval == 'Save Changes' || $sval == '') {
				$fieldset = array();
				for ($i=0;$i<7;$i++) {
					$item = array();
					$item["label"] = $_POST['label'.$i];
					$item["on"] = (isset($_POST['field'.$i])) ? 1 : 0;
					array_push($fieldset, $item);
				}
				$fieldset_s = serialize($fieldset);
				$cf = $_POST['contact_flash']; // Contact form is always on for Flash 
				$ch = $_POST['contact_html'];
				$add = $_POST['add_address'];
				$em = $_POST['add_email'];
				$ph = $_POST['add_phone'];
				$ex = $_POST['add_extra'];
				$extra = $_POST['contact_form_extra'];
				$sende = 1; // Always send to email address
				$sends = $_POST['contact_form_sms'];
				// reorder other pages
				$q = "update settings set contact_form_flash = '$cf', contact_form_html = '$ch', 
					  contact_form_email = '$sende', contact_form_sms = '$sends',
					  add_address = '$add', add_email = '$em', add_phone = '$ph', add_extra = '$ex',
					  contact_form_extra = '$extra',
					  contact_form_fields = '$fieldset_s'
					  where settings_id = 1";
				$upd = mysql_query($q)
					or die(mysql_error());		
				mark_update();		
				header("Location: ../page_admin.php?mode=$pmode");
			} else if ($sval == 'Upload') {
				//*****
				// DB Connect for single sign on
				//*****
				$link = mysql_connect(DB_HOST, DB_USER, DB_PASS) or mysql_error();
				$conn = mysql_select_db(DB_NAME);
				//*****
				
				$id = $_SESSION['user']['id'];
				$t = $_SESSION['user']['token'];
				$q = "select * from clients where id = '$id' and token = '$t' ";
				$get = mysql_query($q)
					or die('Bad query');
				$u = mysql_fetch_array($get, MYSQL_ASSOC);
				$file_path = $u['path'];
				
				$file_temp = $_FILES['contactfile']['tmp_name'];
				$file_name = 'contact.jpg';
				
				$filestatus = move_uploaded_file($file_temp,$file_path . "gallery/original/" . $file_name);
				$orig = $file_path . "gallery/original/" . $file_name;
				fitimage($orig, $file_path . "gallery/thumb/" . $file_name, 100, 100);
				fitimage($orig, $file_path . "gallery/small/" . $file_name, 300, 200);
				fitimage($orig, $file_path . "gallery/medium/" . $file_name, 600, 400);
				fitimage($orig, $file_path . "gallery/large/" . $file_name, 900, 600);
				fitimage($orig, $file_path . "gallery/square/" . $file_name, 75, 75, true);
				
				header("Location: ../page_admin.php?mode=contact");
			}
			break;
		case 'edit':
			if ($sval == 'Save Changes') {
				$pageid = $_POST['mypage'];
				$img = $_POST['page_image'];
				$title = $_POST['page_title'];
				$body = $_POST['page_text_html'];
				$html = $_POST['include_html'];
				$alt = $_POST['alt_html'];
				$atitle = $_POST['html_title'];
				$abody = $_POST['html_text'];
				$proof = $_POST['add_proofing'];
				$bg_mode = $_POST['bg_mode'];
				$bg_image = $_POST['bg_image'];
				$bg_alpha = $_POST['bg_alpha'];
				$hidden = $_POST['page_hidden'];
				$add_contact_form = $_POST['add_contact_form'];
				$add_calendar = $_POST['add_calendar'];
				if (substr($_POST['page_color_val'],0,1) == "#") {
					$color = $_POST['page_color_val'];
				} else {
					$color = "#" . $_POST['page_color_val'];
				}
				$opac = $_POST['page-opac'];
				if ($_SESSION['template'] == 'burnside' || $_SESSION['template'] == 'butler' || $_SESSION['template'] == 'atwood' || $_SESSION['template'] == 'york' || $_SESSION['template_name'] == 'Cascade Lakes Deluxe') {
					$q = "update pages set page_image = '$img', page_title = '$title', page_content = '$body', 
						  include_html = '$html', alt_html = '$alt', alt_title = '$atitle', 
						  alt_content = '$abody', add_proofing = '$proof', bg_mode = '$bg_mode', bg_alpha = '$bg_alpha', 
						  bg_image = '$bg_image', page_hidden = '$hidden', page_color = '$color', page_opacity = '$opac' where page_id = '$pageid'";
				} else if ($_SESSION['template'] == 'burlington' || $_SESSION['template'] == 'arch_cape' || $_SESSION['template'] == 'greer' || $_SESSION['template'] == 'nakoma' || $_SESSION['template'] == 'northbelize' || $_SESSION['template'] == 'santiam' || $_SESSION['template'] == 'wapato') {
					$q = "update pages set page_image = '$img', page_title = '$title', page_content = '$body', 
						  include_html = '$html', alt_html = '$alt', alt_title = '$atitle', 
						  alt_content = '$abody', add_proofing = '$proof', bg_mode = '$bg_mode', bg_alpha = '$bg_alpha', 
						  bg_image = '$bg_image', page_hidden = '$hidden', add_contact_form = '$add_contact_form', add_calendar = '$add_calendar' where page_id = '$pageid'";
				} else if (substr($_SESSION['template'], 0, 6) == 'artist' || $_SESSION['template'] == 'sage' || $_SESSION['template'] == 'da' || $_SESSION['template'] == 'pioneer') {
					$q = "update pages set page_image = '$img', page_title = '$title', page_content = '$body', 
						  include_html = '$html', alt_html = '$alt', alt_title = '$atitle', 
						  alt_content = '$abody', add_proofing = '$proof', bg_mode = '$bg_mode', bg_alpha = '$bg_alpha', 
						  bg_image = '$bg_image', page_hidden = '$hidden', page_color = '$color', page_opacity = '$opac', add_contact_form = '$add_contact_form', add_calendar = '$add_calendar'";
					if (isset($_POST['grass_flag'])) {
						$show_grass = $_POST['show_grass'];
						$q .= ", show_grass = '$show_grass'";
					}
					if (isset($_POST['birds_flag'])) {
						$show_birds = $_POST['show_birds'];
						$q .= ", show_birds = '$show_birds'";
					}
					if (isset($_POST['flowers_flag'])) {
						$show_flowers = $_POST['show_flowers'];
						$q .= ", show_flowers = '$show_flowers'";
					}
					if (isset($_POST['tree_flag'])) {
						$show_tree = $_POST['show_tree'];
						$q .= ", show_tree = '$show_tree'";
					}
					if (isset($_POST['clouds_flag'])) {
						$show_clouds = $_POST['show_clouds'];
						$q .= ", show_clouds = '$show_clouds'";
					}
					if (isset($_POST['balloons_flag'])) {
						$show_balloons = $_POST['show_balloons'];
						$q .= ", show_balloons = '$show_balloons'";
					}
					if (isset($_POST['kite_flag'])) {
						$show_kite = $_POST['show_kite'];
						$q .= ", show_kite = '$show_kite'";
					}
					if (isset($_POST['wagon_flag'])) {
						$show_wagon = $_POST['show_wagon'];
						$q .= ", show_wagon = '$show_wagon'";
					}
					if (isset($_POST['clothesline_flag'])) {
						$show_clothesline = $_POST['show_clothesline'];
						$q .= ", show_clothesline = '$show_clothesline'";
					}
					if (isset($_POST['pinwheels_flag'])) {
						$show_pinwheels = $_POST['show_pinwheels'];
						$q .= ", show_pinwheels = '$show_pinwheels'";
					}
					$q .= "where page_id = '$pageid'";
				} else {
					$q = "update pages set page_image = '$img', page_title = '$title', page_content = '$body', 
						  include_html = '$html', alt_html = '$alt', alt_title = '$atitle', 
						  alt_content = '$abody', add_proofing = '$proof', bg_mode = '$bg_mode', bg_alpha = '$bg_alpha', 
						  bg_image = '$bg_image', page_hidden = '$hidden' where page_id = '$pageid'";
				}
				
				$upd = mysql_query($q)
					or die(mysql_error());
				mark_update();
				header("Location: ../page_admin.php?mode=edit&pid=" . $pageid);
			} else if ($sval == 'Upload') {
				
				//*****
				// DB Connect for single sign on
				//*****
				$link = mysql_connect(DB_HOST, DB_USER, DB_PASS) or mysql_error();
				$conn = mysql_select_db(DB_NAME);
				//*****
				
				$id = $_SESSION['user']['id'];
				$t = $_SESSION['user']['token'];
				$q = "select * from clients where id = '$id' and token = '$t' ";
				$get = mysql_query($q)
					or die(mysql_error());
				$u = mysql_fetch_array($get, MYSQL_ASSOC);
				$file_path = $u['path'];
				
				$file_temp = $_FILES['mainimagefile']['tmp_name'];
				$file_name = $_FILES['mainimagefile']['name'];
				
				$filestatus = move_uploaded_file($file_temp,$file_path . "gallery/original/" . $file_name);
				$orig = $file_path . "gallery/original/" . $file_name;
				fitimage($orig, $file_path . "gallery/thumb/" . $file_name, 100, 100);
				fitimage($orig, $file_path . "gallery/small/" . $file_name, 300, 200);
				fitimage($orig, $file_path . "gallery/medium/" . $file_name, 600, 400);
				fitimage($orig, $file_path . "gallery/large/" . $file_name, 900, 600);
				fitimage($orig, $file_path . "gallery/square/" . $file_name, 75, 75, true);
				
				include('db.inc.php');
				
				$pid = $_POST['mainpid'];
				
				$q = "update pages set page_image = '$file_name' where page_id = '$pid'";
				$upd = mysql_query($q)
					or die(mysql_error());
				mark_update();
				/*
				echo('$file_temp: '.$file_temp);
				echo('$file_path: '.file_exists($file_path).'<br />');
				echo('$filestatus: '.$filestatus);
				echo('exists: '.file_exists($orig));
				*/
				header("Location: ../page_admin.php?mode=edit&pid=" . $pid);

			} else if ($sval == 'Upload Texture') {
				//*****
				// DB Connect for single sign on
				//*****
				$link = mysql_connect(DB_HOST, DB_USER, DB_PASS) or mysql_error();
				$conn = mysql_select_db(DB_NAME);
				//*****
				
				$id = $_SESSION['user']['id'];
				$t = $_SESSION['user']['token'];
				$q = "select * from clients where id = '$id' and token = '$t' ";
				$get = mysql_query($q)
					or die(mysql_error());
				$u = mysql_fetch_array($get, MYSQL_ASSOC);
				$file_path = $u['path'];
				
				$file_temp = $_FILES['maintexturefile']['tmp_name'];
				$file_name = $_FILES['maintexturefile']['name'];
				
				$filestatus = move_uploaded_file($file_temp,$file_path . "textures/" . $file_name);
				
				include('db.inc.php');
				
				$pid = $_POST['mainpid'];
				
				$q = "update pages set page_texture = '$file_name' where page_id = '$pid'";
				$upd = mysql_query($q)
					or die(mysql_error());
				mark_update();
				header("Location: ../page_admin.php?mode=edit&pid=" . $pid);
			} else if ($_POST['deletePageImage'] == '1') {
				$pid = $_POST['mainpid'];
				$q = "update pages set page_image = '' where page_id = '$pid'";
				$upd = mysql_query($q)
					or die(mysql_error());
				mark_update();
				header("Location: ../page_admin.php?mode=edit&pid=" . $pid);
			} else {
				$pageid = $_POST['mypage'];
				$img = $_POST['page_image'];
				$title = $_POST['page_title'];
				$body = $_POST['page_text_html'];
				$html = $_POST['include_html'];
				$alt = $_POST['alt_html'];
				$atitle = $_POST['html_title'];
				$abody = $_POST['html_text'];
				$proof = $_POST['add_proofing'];
				$bg_mode = $_POST['bg_mode'];
				$bg_image = $_POST['bg_image'];
				$bg_alpha = $_POST['bg_alpha'];
				$hidden = $_POST['page_hidden'];
				$add_contact_form = $_POST['add_contact_form'];
				$add_calendar = $_POST['add_calendar'];
				if (substr($_POST['page_color_val'],0,1) == "#") {
					$color = $_POST['page_color_val'];
				} else {
					$color = "#" . $_POST['page_color_val'];
				}
				$opac = $_POST['page-opac'];
				if ($_SESSION['template'] == 'burnside' || $_SESSION['template'] == 'butler' || $_SESSION['template'] == 'atwood' || $_SESSION['template'] == 'york' || $_SESSION['template_name'] == 'Cascade Lakes Deluxe') {
					$q = "update pages set page_image = '$img', page_title = '$title', page_content = '$body', 
						  include_html = '$html', alt_html = '$alt', alt_title = '$atitle', 
						  alt_content = '$abody', add_proofing = '$proof', bg_mode = '$bg_mode', bg_alpha = '$bg_alpha', 
						  bg_image = '$bg_image', page_hidden = '$hidden', page_color = '$color', page_opacity = '$opac' where page_id = '$pageid'";
				} else if ($_SESSION['template'] == 'burlington' || $_SESSION['template'] == 'arch_cape' || $_SESSION['template'] == 'greer' || $_SESSION['template'] == 'nakoma' || $_SESSION['template'] == 'northbelize' || $_SESSION['template'] == 'santiam' || $_SESSION['template'] == 'wapato') {
					$q = "update pages set page_image = '$img', page_title = '$title', page_content = '$body', 
						  include_html = '$html', alt_html = '$alt', alt_title = '$atitle', 
						  alt_content = '$abody', add_proofing = '$proof', bg_mode = '$bg_mode', bg_alpha = '$bg_alpha', 
						  bg_image = '$bg_image', page_hidden = '$hidden', add_contact_form = '$add_contact_form', add_calendar = '$add_calendar' where page_id = '$pageid'";
				} else if (substr($_SESSION['template'], 0, 6) == 'artist' || $_SESSION['template'] == 'sage' || $_SESSION['template'] == 'da' || $_SESSION['template'] == 'pioneer') {
					$q = "update pages set page_image = '$img', page_title = '$title', page_content = '$body', 
						  include_html = '$html', alt_html = '$alt', alt_title = '$atitle', 
						  alt_content = '$abody', add_proofing = '$proof', bg_mode = '$bg_mode', bg_alpha = '$bg_alpha', 
						  bg_image = '$bg_image', page_hidden = '$hidden', page_color = '$color', page_opacity = '$opac', add_contact_form = '$add_contact_form', add_calendar = '$add_calendar'";
					if (isset($_POST['grass_flag'])) {
						$show_grass = $_POST['show_grass'];
						$q .= ", show_grass = '$show_grass'";
					}
					if (isset($_POST['birds_flag'])) {
						$show_birds = $_POST['show_birds'];
						$q .= ", show_birds = '$show_birds'";
					}
					if (isset($_POST['flowers_flag'])) {
						$show_flowers = $_POST['show_flowers'];
						$q .= ", show_flowers = '$show_flowers'";
					}
					if (isset($_POST['tree_flag'])) {
						$show_tree = $_POST['show_tree'];
						$q .= ", show_tree = '$show_tree'";
					}
					if (isset($_POST['clouds_flag'])) {
						$show_clouds = $_POST['show_clouds'];
						$q .= ", show_clouds = '$show_clouds'";
					}
					if (isset($_POST['balloons_flag'])) {
						$show_balloons = $_POST['show_balloons'];
						$q .= ", show_balloons = '$show_balloons'";
					}
					if (isset($_POST['kite_flag'])) {
						$show_kite = $_POST['show_kite'];
						$q .= ", show_kite = '$show_kite'";
					}
					if (isset($_POST['wagon_flag'])) {
						$show_wagon = $_POST['show_wagon'];
						$q .= ", show_wagon = '$show_wagon'";
					}
					if (isset($_POST['clothesline_flag'])) {
						$show_clothesline = $_POST['show_clothesline'];
						$q .= ", show_clothesline = '$show_clothesline'";
					}
					if (isset($_POST['pinwheels_flag'])) {
						$show_pinwheels = $_POST['show_pinwheels'];
						$q .= ", show_pinwheels = '$show_pinwheels'";
					}
					$q .= "where page_id = '$pageid'";
				} else {
					$q = "update pages set page_image = '$img', page_title = '$title', page_content = '$body', 
						  include_html = '$html', alt_html = '$alt', alt_title = '$atitle', 
						  alt_content = '$abody', add_proofing = '$proof', bg_mode = '$bg_mode', bg_alpha = '$bg_alpha', 
						  bg_image = '$bg_image', page_hidden = '$hidden' where page_id = '$pageid'";	
				}
				$upd = mysql_query($q)
					or die(mysql_error());
				mark_update();
				header("Location: ../page_admin.php?mode=edit&pid=" . $pageid);
			}
			break;
			case 'subedit':
				if ($sval == 'Save Changes') {
					$pageid = $_POST['mypage'];
					$pid = $_POST['page_parent_id'];
					$img = $_POST['page_image'];
					$title = $_POST['page_title'];
					$body = $_POST['page_text_html'];
					$html = $_POST['include_html'];
					$alt = $_POST['alt_html'];
					$atitle = $_POST['html_title'];
					$abody = $_POST['html_text'];
					$proof = $_POST['add_proofing'];
					$bg_mode = $_POST['bg_mode'];
					$bg_image = $_POST['bg_image'];
					$bg_alpha = $_POST['bg_alpha'];
					$hidden = $_POST['page_hidden'];
					$add_contact_form = $_POST['add_contact_form'];
					$add_calendar = $_POST['add_calendar'];
					if (substr($_POST['page_color_val'],0,1) == "#") {
						$color = $_POST['page_color_val'];
					} else {
						$color = "#" . $_POST['page_color_val'];
					}
					$opac = $_POST['page-opac'];
					if ($_SESSION['template'] == 'burnside' || $_SESSION['template'] == 'butler' || $_SESSION['template'] == 'atwood' || $_SESSION['template'] == 'york' || $_SESSION['template_name'] == 'Cascade Lakes Deluxe') {
						$q = "update pages set page_image = '$img', page_title = '$title', page_content = '$body', parent_id = '$pid',
							  include_html = '$html', alt_html = '$alt', alt_title = '$atitle', 
							  alt_content = '$abody', add_proofing = '$proof', bg_mode = '$bg_mode', bg_alpha = '$bg_alpha', 
							  bg_image = '$bg_image', page_hidden = '$hidden', page_color = '$color', page_opacity = '$opac' where page_id = '$pageid'";
					} else if ($_SESSION['template'] == 'burlington' || $_SESSION['template'] == 'arch_cape' || $_SESSION['template'] == 'greer' || $_SESSION['template'] == 'nakoma' || $_SESSION['template'] == 'northbelize' || $_SESSION['template'] == 'santiam' || $_SESSION['template'] == 'wapato') {
						$q = "update pages set page_image = '$img', page_title = '$title', page_content = '$body', parent_id = '$pid',
							  include_html = '$html', alt_html = '$alt', alt_title = '$atitle', 
							  alt_content = '$abody', add_proofing = '$proof', bg_mode = '$bg_mode', bg_alpha = '$bg_alpha', 
							  bg_image = '$bg_image', page_hidden = '$hidden', add_contact_form = '$add_contact_form', add_calendar = '$add_calendar' where page_id = '$pageid'";
					} else if (substr($_SESSION['template'], 0, 6) == 'artist' || $_SESSION['template'] == 'sage' || $_SESSION['template'] == 'da' || $_SESSION['template'] == 'pioneer') {
						$q = "update pages set page_image = '$img', page_title = '$title', page_content = '$body', 
							  include_html = '$html', alt_html = '$alt', alt_title = '$atitle', 
							  alt_content = '$abody', add_proofing = '$proof', bg_mode = '$bg_mode', bg_alpha = '$bg_alpha', 
							  bg_image = '$bg_image', page_hidden = '$hidden', page_color = '$color', page_opacity = '$opac', add_contact_form = '$add_contact_form', add_calendar = '$add_calendar'";
						if (isset($_POST['grass_flag'])) {
							$show_grass = $_POST['show_grass'];
							$q .= ", show_grass = '$show_grass'";
						}
						if (isset($_POST['birds_flag'])) {
							$show_birds = $_POST['show_birds'];
							$q .= ", show_birds = '$show_birds'";
						}
						if (isset($_POST['flowers_flag'])) {
							$show_flowers = $_POST['show_flowers'];
							$q .= ", show_flowers = '$show_flowers'";
						}
						if (isset($_POST['tree_flag'])) {
							$show_tree = $_POST['show_tree'];
							$q .= ", show_tree = '$show_tree'";
						}
						if (isset($_POST['clouds_flag'])) {
							$show_clouds = $_POST['show_clouds'];
							$q .= ", show_clouds = '$show_clouds'";
						}
						if (isset($_POST['balloons_flag'])) {
							$show_balloons = $_POST['show_balloons'];
							$q .= ", show_balloons = '$show_balloons'";
						}
						if (isset($_POST['kite_flag'])) {
							$show_kite = $_POST['show_kite'];
							$q .= ", show_kite = '$show_kite'";
						}
						if (isset($_POST['wagon_flag'])) {
							$show_wagon = $_POST['show_wagon'];
							$q .= ", show_wagon = '$show_wagon'";
						}
						if (isset($_POST['clothesline_flag'])) {
							$show_clothesline = $_POST['show_clothesline'];
							$q .= ", show_clothesline = '$show_clothesline'";
						}
						if (isset($_POST['pinwheels_flag'])) {
							$show_pinwheels = $_POST['show_pinwheels'];
							$q .= ", show_pinwheels = '$show_pinwheels'";
						}
						$q .= "where page_id = '$pageid'";
					} else {
						$q = "update pages set page_image = '$img', page_title = '$title', page_content = '$body', parent_id = '$pid',
							  include_html = '$html', alt_html = '$alt', alt_title = '$atitle', 
							  alt_content = '$abody', add_proofing = '$proof', bg_mode = '$bg_mode', bg_alpha = '$bg_alpha', 
							  bg_image = '$bg_image', page_hidden = '$hidden' where page_id = '$pageid'";
					}
					$upd = mysql_query($q)
						or die(mysql_error());
					mark_update();
					header("Location: ../page_admin.php");
				} else if ($sval == 'Upload') {
					
					//*****
					// DB Connect for single sign on
					//*****
					$link = mysql_connect(DB_HOST, DB_USER, DB_PASS) or mysql_error();
					$conn = mysql_select_db(DB_NAME);
					//*****
					
					$id = $_SESSION['user']['id'];
					$t = $_SESSION['user']['token'];
					$q = "select * from clients where id = '$id' and token = '$t' ";
					$get = mysql_query($q)
						or die(mysql_error());
					$u = mysql_fetch_array($get, MYSQL_ASSOC);
					$file_path = $u['path'];
					
					$file_temp = $_FILES['mainimagefile']['tmp_name'];
					$file_name = $_FILES['mainimagefile']['name'];
					
					$filestatus = move_uploaded_file($file_temp,$file_path . "gallery/original/" . $file_name);
					$orig = $file_path . "gallery/original/" . $file_name;
					fitimage($orig, $file_path . "gallery/thumb/" . $file_name, 100, 100);
					fitimage($orig, $file_path . "gallery/small/" . $file_name, 300, 200);
					fitimage($orig, $file_path . "gallery/medium/" . $file_name, 600, 400);
					fitimage($orig, $file_path . "gallery/large/" . $file_name, 900, 600);
					fitimage($orig, $file_path . "gallery/square/" . $file_name, 75, 75, true);
					
					include('db.inc.php');
					
					$pid = $_POST['mainpid'];
					
					$q = "update pages set page_image = '$file_name' where page_id = '$pid'";
					$upd = mysql_query($q)
						or die(mysql_error());
					mark_update();
					header("Location: ../page_admin.php?mode=subedit&pid=" . $pid);

				} else if ($sval == 'Upload Texture') {
						//*****
						// DB Connect for single sign on
						//*****
						$link = mysql_connect(DB_HOST, DB_USER, DB_PASS) or mysql_error();
						$conn = mysql_select_db(DB_NAME);
						//*****

						$id = $_SESSION['user']['id'];
						$t = $_SESSION['user']['token'];
						$q = "select * from clients where id = '$id' and token = '$t' ";
						$get = mysql_query($q)
							or die(mysql_error());
						$u = mysql_fetch_array($get, MYSQL_ASSOC);
						$file_path = $u['path'];

						$file_temp = $_FILES['maintexturefile']['tmp_name'];
						$file_name = $_FILES['maintexturefile']['name'];

						$filestatus = move_uploaded_file($file_temp,$file_path . "textures/" . $file_name);

						include('db.inc.php');

						$pid = $_POST['mainpid'];

						$q = "update pages set page_texture = '$file_name' where page_id = '$pid'";
						$upd = mysql_query($q)
							or die(mysql_error());
						mark_update();
						header("Location: ../page_admin.php?mode=subedit&pid=" . $pid);
				} else if ($_POST['deletePageImage'] == '1') {
					$pid = $_POST['mypage'];
					$q = "update pages set page_image = '' where page_id = '$pid'";
					$upd = mysql_query($q)
						or die(mysql_error());
					mark_update();
					header("Location: ../page_admin.php?mode=subedit&pid=" . $pid);
				} else {
					$pageid = $_POST['mypage'];
					$pid = $_POST['page_parent_id'];
					$img = $_POST['page_image'];
					$title = $_POST['page_title'];
					$body = $_POST['page_text_html'];
					$html = $_POST['include_html'];
					$alt = $_POST['alt_html'];
					$atitle = $_POST['html_title'];
					$abody = $_POST['html_text'];
					$proof = $_POST['add_proofing'];
					$bg_mode = $_POST['bg_mode'];
					$bg_image = $_POST['bg_image'];
					$bg_alpha = $_POST['bg_alpha'];
					$hidden = $_POST['page_hidden'];
					$add_contact_form = $_POST['add_contact_form'];
					$add_calendar = $_POST['add_calendar'];
					if (substr($_POST['page_color_val'],0,1) == "#") {
						$color = $_POST['page_color_val'];
					} else {
						$color = "#" . $_POST['page_color_val'];
					}
					$opac = $_POST['page-opac'];
					if ($_SESSION['template'] == 'burnside' || $_SESSION['template'] == 'butler' || $_SESSION['template'] == 'atwood' || $_SESSION['template'] == 'york' || $_SESSION['template_name'] == 'Cascade Lakes Deluxe') {
						$q = "update pages set page_image = '$img', page_title = '$title', page_content = '$body', parent_id = '$pid',
							  include_html = '$html', alt_html = '$alt', alt_title = '$atitle', 
							  alt_content = '$abody', add_proofing = '$proof', bg_mode = '$bg_mode', bg_alpha = '$bg_alpha', 
							  bg_image = '$bg_image', page_hidden = '$hidden', page_color = '$color', page_opacity = '$opac' where page_id = '$pageid'";
					} else if ($_SESSION['template'] == 'burlington' || $_SESSION['template'] == 'arch_cape' || $_SESSION['template'] == 'greer' || $_SESSION['template'] == 'nakoma' || $_SESSION['template'] == 'northbelize' || $_SESSION['template'] == 'santiam' || $_SESSION['template'] == 'wapato') {
						$q = "update pages set page_image = '$img', page_title = '$title', page_content = '$body', parent_id = '$pid',
							  include_html = '$html', alt_html = '$alt', alt_title = '$atitle', 
							  alt_content = '$abody', add_proofing = '$proof', bg_mode = '$bg_mode', bg_alpha = '$bg_alpha', 
							  bg_image = '$bg_image', page_hidden = '$hidden', add_contact_form = '$add_contact_form', add_calendar = '$add_calendar' where page_id = '$pageid'";
					} else if (substr($_SESSION['template'], 0, 6) == 'artist' || $_SESSION['template'] == 'sage' || $_SESSION['template'] == 'da' || $_SESSION['template'] == 'pioneer') {
						$q = "update pages set page_image = '$img', page_title = '$title', page_content = '$body', 
							  include_html = '$html', alt_html = '$alt', alt_title = '$atitle', 
							  alt_content = '$abody', add_proofing = '$proof', bg_mode = '$bg_mode', bg_alpha = '$bg_alpha', 
							  bg_image = '$bg_image', page_hidden = '$hidden', page_color = '$color', page_opacity = '$opac', add_contact_form = '$add_contact_form', add_calendar = '$add_calendar'";
						if (isset($_POST['grass_flag'])) {
							$show_grass = $_POST['show_grass'];
							$q .= ", show_grass = '$show_grass'";
						}
						if (isset($_POST['birds_flag'])) {
							$show_birds = $_POST['show_birds'];
							$q .= ", show_birds = '$show_birds'";
						}
						if (isset($_POST['flowers_flag'])) {
							$show_flowers = $_POST['show_flowers'];
							$q .= ", show_flowers = '$show_flowers'";
						}
						if (isset($_POST['tree_flag'])) {
							$show_tree = $_POST['show_tree'];
							$q .= ", show_tree = '$show_tree'";
						}
						if (isset($_POST['clouds_flag'])) {
							$show_clouds = $_POST['show_clouds'];
							$q .= ", show_clouds = '$show_clouds'";
						}
						if (isset($_POST['balloons_flag'])) {
							$show_balloons = $_POST['show_balloons'];
							$q .= ", show_balloons = '$show_balloons'";
						}
						if (isset($_POST['kite_flag'])) {
							$show_kite = $_POST['show_kite'];
							$q .= ", show_kite = '$show_kite'";
						}
						if (isset($_POST['wagon_flag'])) {
							$show_wagon = $_POST['show_wagon'];
							$q .= ", show_wagon = '$show_wagon'";
						}
						if (isset($_POST['clothesline_flag'])) {
							$show_clothesline = $_POST['show_clothesline'];
							$q .= ", show_clothesline = '$show_clothesline'";
						}
						if (isset($_POST['pinwheels_flag'])) {
							$show_pinwheels = $_POST['show_pinwheels'];
							$q .= ", show_pinwheels = '$show_pinwheels'";
						}
						$q .= "where page_id = '$pageid'";
					} else {
						$q = "update pages set page_image = '$img', page_title = '$title', page_content = '$body', parent_id = '$pid',
							  include_html = '$html', alt_html = '$alt', alt_title = '$atitle', 
							  alt_content = '$abody', add_proofing = '$proof', bg_mode = '$bg_mode', bg_alpha = '$bg_alpha', 
							  bg_image = '$bg_image', page_hidden = '$hidden' where page_id = '$pageid'";	
					}
					$upd = mysql_query($q)
						or die(mysql_error());
					mark_update();
					header("Location: ../page_admin.php");
				}
				break;
				case 'calendar':
					//echo('ics file submitted...'.$sval);
					//echo('clear_events?: '.$_POST['clear_events']);
					if ($sval == 'Upload' && substr($_FILES['icalfile']['name'], strlen($_FILES['icalfile']['name'])-3) == 'ics') { 
						//*****
						// DB Connect for single sign on
						//*****
						
						$link = mysql_connect(DB_HOST, DB_USER, DB_PASS) or mysql_error();
						$conn = mysql_select_db(DB_NAME);
						//

						$id = $_SESSION['user']['id'];
						$t = $_SESSION['user']['token'];
						$q = "select * from clients where id = '$id' and token = '$t' ";
						$get = mysql_query($q)
							or die(mysql_error());
						$u = mysql_fetch_array($get, MYSQL_ASSOC);
						$file_path = $u['path'];
						

						$file_temp = $_FILES['icalfile']['tmp_name'];
						$file_name = $_FILES['icalfile']['name'];
						$filestatus = move_uploaded_file($file_temp,$file_path . "extra/" . $file_name);
						
						include('db.inc.php');
						
						$caltest = new ical;
						$cal = $caltest->parse($file_path . "extra/" . $file_name);

						$calData = $caltest->get_sort_event_list();

						$eventsArr = array();
						foreach ($calData as $dataArr)
						{
							$eventItemCount = 0;
							//
							while(list($key, $val) = each($dataArr)) {  
								//echo("$key --- ".is_array($val).'<br />');
								if (($key == 'DTSTART' || $key == 'DTEND' || $key == 'RRULE') && is_array($val)) {
									while(list($key2, $val2) = each($val)) {  
										if ($key == 'DTSTART') {
											$eventObj->start_val = $val2; 
										} else if ($key == 'DTEND') {
											$eventObj->end_val = $val2;
									    }
										if ($key2 == 'unixtime') {
											//echo("unixtime - $key : ".date ("Y-m-d_H-i-s",$val2)."<br />");
											if ($key == 'DTSTART') {
												$eventObj->start_date = date ("m/d/Y",$val2);
												$eventObj->start_time = date ("Hi",$val2);
											} else if ($key == 'DTEND'){
												$eventObj->end_date = date ("m/d/Y",$val2);
												$eventObj->end_time = date ("Hi",$val2);
											}
										} else {
											//echo($key."_".$key2." : ".$val2."<br />");
											//$valStr = $key.'_'.$key2;
											//echo($valStr."<br />");
											if ($key2 == 'FREQ' && $key == 'RRULE') {
												$eventObj->rrule_freq = $val2;
											} else if ($key2 == 'INTERVAL' && $key == 'RRULE') {
												$eventObj->rrule_interval = $val2;
											} else if ($key2 == 'COUNT' && $key == 'RRULE') {
												$eventObj->rrule_count = $val2;
											}
										}
									}
								} else {
									if ($key == 'SUMMARY') {
										$eventObj->summary = addslashes($val);
									} else if ($key == 'LOCATION') {
										$eventObj->location = addslashes($val);
									} else if ($key == 'DTSTART') { 
										$eventObj->start_val = $val;
										$eventObj->start_date = date ("m/d/Y",$val);
										$eventObj->start_time = date ("Hi",$val);
									} else if ($key == 'DTEND') { 
										$eventObj->end_val = $val; 
										$eventObj->end_time = date ("Hi",$val);
										$eventObj->end_date = date ("m/d/Y",$val);
									   	
									}
									//$isArr = $val == 'ArrayDESCRIPTION:';
									//echo("$key : ".$val."<br />");
								}
							}
							//echo('<br />');
							if ($eventObj->end_val < $eventObj->start_val) { 
								//echo('end date is before start date');
								  //$eventObj->end_val = $eventObj->start_val + $eventObj->end_val; 
								  //$eventObj->end_date = date ("m/d/Y",$eventObj->end_val);
								  //$eventObj->end_time = date ("Hi",$eventObj->end_val);
							}
							$eventsArr[] = $eventObj;
							unset($eventObj);
						}
						//
						$import_id = get_calendar_import_id();
						$import_time = time();//date("m/d/Y_H:i:s");
						//echo('import time: '.$import_time.'<br /><br />');
						//
						if ($_POST['clear_events'] == 'yes') {
							$q = "delete from calendar where id >= '0'";
							$sel = mysql_query($q)
								or die(mysql_error());
						}
						//
						$counter = 0;
						foreach ($eventsArr as $eventObj) {

							while(list($key, $val) = each($eventObj)) {
								//echo("$key: $val");
								//echo('<br />');
							}
							//echo('<br />');

                            
							$q = "insert into calendar set start_date = '$eventObj->start_date', start_time = '$eventObj->start_time', end_date = '$eventObj->end_date', end_time = '$eventObj->end_time', summary = '$eventObj->summary', location = '$eventObj->location', rrule_freq = '$eventObj->rrule_freq', rrule_interval = '$eventObj->rrule_interval', rrule_count = '$eventObj->rrule_count', import_id = '$import_id', import_time = '$import_time'"; // 
							$upd = mysql_query($q)
								or die(mysql_error());
							$counter++;
							


						}
						//echo("$counter were imported");
						//mark_update();
						if ($_SESSION['template'] != 'lapine' && $_SESSION['template'] != 'bandon' && $_SESSION['template'] != 'burlington' && $_SESSION['template'] != 'arch_cape' && $_SESSION['template'] != 'greer' && $_SESSION['template'] != 'nakoma' && $_SESSION['template'] != 'northbelize' && $_SESSION['template'] != 'sage' && substr($_SESSION['template'], 0, 6) != 'artist' && $_SESSION['template'] != 'da' && $_SESSION['template'] != 'wapato') {
					    	header("Location: ../page_admin.php?mode=calendar&importCount=$counter");
						} else {
							header("Location: ../calendar.php?importCount=$counter");
						}
					} else {
						if ($_SESSION['template'] != 'lapine' && $_SESSION['template'] != 'bandon' && $_SESSION['template'] != 'burlington' && $_SESSION['template'] != 'arch_cape' && $_SESSION['template'] != 'greer' && $_SESSION['template'] != 'nakoma' && $_SESSION['template'] != 'northbelize' && $_SESSION['template'] != 'sage' && substr($_SESSION['template'], 0, 6) != 'artist' && $_SESSION['template'] != 'da' && $_SESSION['template'] != 'wapato') {
							header("Location: ../page_admin.php?mode=calendar&error=noics");
						} else {
							header("Location: ../calendar.php?error=noics");
						}
					}
				break;
	}			
}
?>