<?
$session_id = isset($_GET["PHPSESSID"]) ? $_GET["PHPSESSID"] : false;
if ($session_id) {
	session_id($session_id);
}
session_start();

$upload_path = str_replace("\\", "/", realpath(dirname($_SERVER['SCRIPT_FILENAME']))) . "/../../gallery/";

// Get clean name 
$filename = cleanfile($_FILES["Filedata"]["name"]);

// Handle the upload
if (!move_uploaded_file($_FILES["Filedata"]["tmp_name"], $upload_path . $filename)) {
	header("HTTP/1.0 500 Internal Server Error");
}

// chmod the file
chmod($upload_path . $filename, 0755);

// thumb file
thumbnail("../../gallery/" . $filename, "../../gallery/thumbs/" . $filename, 50);
// small file
thumbnail("../../gallery/" . $filename, "../../gallery/small/" . $filename, 180);

function thumbnail($image,$thumb,$size) {
	if (function_exists('imagecreatefromjpeg')) {
		$src_img = imagecreatefromjpeg($image); 
    	$origw=imagesx($src_img); 
    	$origh=imagesy($src_img); 
    	if ($origw > $origh) {
            $new_h = $size;
            $diff = $origh/$new_h;
            $new_w = $origw/$diff;
        } else {
            $new_w = $size;
            $diff = $origw/$new_w;
            $new_h = $origh/$diff;
        }
		if ($size > 50) {
			$dst_img = imagecreatetruecolor($new_w, $new_h);
		} else {
    		$dst_img = imagecreatetruecolor($size,$size);
		}
    	imagecopyresampled($dst_img,$src_img,0,0,0,0,$new_w,$new_h,imagesx($src_img),imagesy($src_img)); 
   		imagejpeg($dst_img, $thumb, 75);
		imagedestroy($src_img);
		imagedestroy($dst_img);
	} 
    return true; 
}
function cleanfile($name) {
	return preg_replace("/[^a-zA-Z0-9\-_\.]+/", "-", $name);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title>SWFUpload Revision 5 Demo</title>
</head>
<body>
	<p><?=$upload_path?></p>
</body>
</html>