<?
// This processes the replace form from image details
include('functions.inc.php');
// POST vars
$orig = $_POST['image'];
$item = $_POST['itemtype'];
$uploaddir = "../../gallery/thumbs/"; // replacing thumb only
$filename = $uploaddir.$orig.'.jpg';
// Copy file to proper location
if ($item == 'thumb') {
    $tmpname = '../../gallery/thumbs/tmpthumb.jpg';
    $uploadSuccess = move_uploaded_file($_FILES['rfile']['tmp_name'], $tmpname);
    $isize = getimagesize($tmpname);
    if ($isize[0] != 50 || $isize[1] != 50) {
        @unlink($tmpname);
    } else {
        @unlink($filename);
        rename($tmpname, $filename);
    }
}
// Redirect back to image details
$url = '../swf_details.php?i='.$orig;
header("Location: $url");
?>
