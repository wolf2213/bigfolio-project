<?
// Include files
include('db.inc.php');
include('functions.inc.php');
session_start();
authorize_user();
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$pmode = $_POST['mode'];
	$sval = $_POST['Submit'];
	switch ($pmode) {
		case 'upload':
			if ($sval == 'Upload') {
				$uploaddir = $_SESSION['user']['path'] . 'gallery/original/';
				for ($i = 0; $i < $_POST['numFiles']; $i++) {
					$uploadSuccess = move_uploaded_file($_FILES['userFile']['tmp_name'][$i], $uploaddir . $_FILES['userFile']['name'][$i]);
					if ($uploadSuccess) {
						// Create thumb
						$thumbfile = $_SESSION['user']['path'] . "gallery/thumb/".$_FILES['userFile']['name'][$i];
						$thumbsuccess = thumbnail($uploaddir.$_FILES['userFile']['name'][$i],$thumbfile,50);
						// Create small
						$thumbfile = $_SESSION['user']['path'] . "gallery/small/".$_FILES['userFile']['name'][$i];
						$thumbsuccess = thumbnail($uploaddir.$_FILES['userFile']['name'][$i],$thumbfile,180);
					}
				}
				// Mark update
				mark_update();
				header("Location: ../gallery_admin.php?mode=images");
			}
			break;
        case 'multi':
            if ($sval == 'Upload') {
                // Zip file will go in the upload dir (/gallery/upload)
				$uploaddir = "../../gallery/";
				// Rel path to zip file
				$zipfile = $uploaddir.$_FILES['zipFile']['name'];
				// Upload .zip
                $result = move_uploaded_file($_FILES['zipFile']['tmp_name'],$zipfile);
                // Absolute path for zip file
                $zipabs = '../../gallery/'.$_FILES['zipFile']['name'];
                // Absolute path to upload dir
                $upath = '../../gallery/upload';
                // Execute (via shell) unzip
                if ($result === true) {
                    $output = shell_exec("unzip -oj $zipabs -d $upath");
                }
                // Now traverse through the upload directory and copy/make thumb
                if ($handle = opendir('../../gallery/upload')) {
                    while (false !== ($file = readdir($handle))) {
                        if (substr($file,0,1) != '.' && (ereg(".swf",strtolower($file)) || ereg(".jpg",strtolower($file)) || ereg(".jpeg",strtolower($file)))) {
                            $fpath = '../../gallery/upload/'.$file;
                            $gpath = '../../gallery/'.$file;
                            $tpath = '../../gallery/thumbs/'.$file;
							$smpath = '../../gallery/small/'.$file;
                            $isize = getimagesize($fpath);
                            copy($fpath, $gpath);
                            if ($isize[2] == 2) {
                                $tok = thumbnail($fpath, $tpath, 50);
								$smok = thumbnail($fpath, $smpath, 180);
                            }
                            // clean up
                            unlink($fpath);
                        }
                    }
                }
                // Mark update
				mark_update();
                header("Location: ../gallery_admin.php?mode=images");
            }
            break;
		case 'images':
			if ($_POST['deleteBankImage'] == '1') {
				$fileName = $_POST['deleteBankFile'];
				$gdir = $_SESSION['user']['path'] . 'gallery/original/';
				$sqdir = $_SESSION['user']['path'] . 'gallery/square/';
				$sdir = $_SESSION['user']['path'] . 'gallery/small/';
				$mdir = $_SESSION['user']['path'] . 'gallery/medium/';
				$ldir = $_SESSION['user']['path'] . 'gallery/large/';
				$tdir = $_SESSION['user']['path'] . 'gallery/thumb/';
				// get image info
				$isize = getimagesize($gdir.$fileName);
				// remove thumb file	
				if (file_exists($tdir.$fileName)) { //$isize[2] == 2 && 
					unlink($sqdir.$fileName);
					unlink($sdir.$fileName);
					unlink($mdir.$fileName);
					unlink($ldir.$fileName);
					unlink($tdir.$fileName);
				} else if (($isize[2] == 4 || $isize[2] == 13) && file_exists($tdir.$fileName.'.jpg')) {
					unlink($sqdir.$fileName.'.jpg');
					unlink($sdir.$fileName.'.jpg');
					unlink($mdir.$fileName.'.jpg');
					unlink($ldir.$fileName.'.jpg');
					unlink($tdir.$fileName.'.jpg');
				}
				// remove file
				if (file_exists($gdir.$fileName)) {
					unlink($gdir.$fileName);
				}
				mark_update();
				header("Location: ../gallery_admin.php");
			} else if ($sval == 'Go' && $_POST['image_action'] == 'del') { 
				$list = $_POST['ibank'];
				foreach($list as $image) {
					$gdir = $_SESSION['user']['path'] . 'gallery/original/';
					$sqdir = $_SESSION['user']['path'] . 'gallery/square/';
					$sdir = $_SESSION['user']['path'] . 'gallery/small/';
					$mdir = $_SESSION['user']['path'] . 'gallery/medium/';
					$ldir = $_SESSION['user']['path'] . 'gallery/large/';
					$tdir = $_SESSION['user']['path'] . 'gallery/thumb/';
					// get image info
					$isize = getimagesize($gdir.$image);
					// remove thumb file	
					if (file_exists($tdir.$image)) { //$isize[2] == 2 && 
						unlink($sqdir.$image);
						unlink($sdir.$image);
						unlink($mdir.$image);
						unlink($ldir.$image);
						unlink($tdir.$image);
					} else if (($isize[2] == 4 || $isize[2] == 13) && file_exists($tdir.$image.'.jpg')) {
						unlink($sqdir.$image.'.jpg');
						unlink($sdir.$image.'.jpg');
						unlink($mdir.$image.'.jpg');
						unlink($ldir.$image.'.jpg');
						unlink($tdir.$image.'.jpg');
					}
					// remove file
					if (file_exists($gdir.$image)) {
						unlink($gdir.$image);
					}
				}
				// Mark update
				mark_update();
				header("Location: ../gallery_admin.php");
			} else if ($sval == 'Save Changes') {
				$gid = $_POST['gallery'];
				$name = $_POST['name'];
				$cid = $_POST['category'];
				// Hidden and password protected
				$hidden = (isset($_POST['gallery_hidden'])) ? '1' : '0';
				$password = (isset($_POST['gallery_protect']) && strlen($_POST['gallery_password']) > 0) ? $_POST['gallery_password'] : '';
				// Rename gallery
				$q = "update galleries set gallery_name = '$name', category_id = '$cid', gallery_hidden = '$hidden', password = '$password' where gallery_id = '$gid'";
				$upd = mysql_query($q)
					or die(mysql_error());
				// Mark update
				mark_update();
				header("Location: ../gallery_edit.php?gid=$gid");
			} else if ($sval == 'Save Password') {
				$gid = $_POST['gallery'];
				$pass = $_POST['pass'];
				$q = "update galleries set password = '$pass' where gallery_id = '$gid'";
				$upd = mysql_query($q)
					or die(mysql_error());
				// Mark update
				mark_update();
				header("Location: ../gallery_edit.php?gid=$gid");
			} else if ($_POST['image_action'] == 'add') {
				if ($_SESSION['hasFolders'] == 'true') {
					if ($_POST['move'] == 'Move') {
						$list = $_POST['ibank'];
						$mvf = $_POST['move_to_folder'];

						foreach ($list as $img) {
							$q = "select * from folder_images where file = '$img'";
							$sel = mysql_query($q)
								or die(mysql_error());
							$row = mysql_fetch_array($sel, MYSQL_ASSOC);
							if (empty($row['id'])) {
								$q = "insert into folder_images(file,folder_id) 
									  values ('$img','$mvf')";
								$ins = mysql_query($q)
									or die(mysql_error());
							} else {
								$q = "update folder_images set folder_id = '$mvf' where file = '$img'";
								$upd = mysql_query($q)
									or die(mysql_error());
							}
						}

						header("Location: ../gallery_admin.php?mode=images");
					} else if ($_POST['combine'] == 'Combine'){
						$list = $_POST['ibank'];
						$numImages = 0;
						$imgVars = '';
						foreach ($list as $img) {
							$imgVars .= '&image'.$numImages.'='.$img;
							$numImages++;
						}
						header("Location: ../image_editor.php?numImages=".$numImages.$imgVars);
					} else {
						$list = $_POST['ibank'];
						$gid = $_POST['gallery'];
						$current = get_gallery_images($gid);				
						$or=count($current); // Order starts at current image count of gallery 
						$file_path = $_SESSION['user']['path'];
						foreach ($list as $img) {
							//
							$q = "select * from gallery_images where image_file = '$img'";
							$sel = mysql_query($q)
								or die(mysql_error());
							$s = mysql_fetch_array($sel, MYSQL_ASSOC);
							$order = $s['order_num'];
							//
							if ($s['gallery_id'] == '999999999') {
								$caption = $s['caption'];
								//$q = "insert into gallery_images(gallery_id,order_num,image_file,image_caption) 
								  //	values ('$gid','$or','$img', '$caption')";
								$q = "update gallery_images set gallery_id = '$gid' where image_file = '$img'";
								$ins = mysql_query($q)
									or die(mysql_error());
								$q = "update gallery_images set order_num = '$or' where image_file = '$img'";
								$ins = mysql_query($q)
									or die(mysql_error());
							} else {
								//
								$orig = $file_path . "gallery/original/" . $img;
								$exif = exif_read_data($orig,0,true);
								//
								$caption = '';
								foreach($exif as $key=>$section) {
							    	foreach($section as $name=>$val) {
										if ($key == 'IFD0') {
							        		if ($name == 'ImageDescription') {
												$caption = $val;
												$caption = str_replace("'", "\'", $caption);
												$caption = str_replace('"', '\"', $caption);
											}
										}
							    	}
								}
								$caption = convert_to_numeric_values($caption);
								$q = "insert into gallery_images(gallery_id,order_num,image_file,image_caption) 
								  	values ('$gid','$or','$img', '$caption')";
								$ins = mysql_query($q)
									or die(mysql_error());
							}
							$or++;
						}
						// Mark update
						mark_update();
						header("Location: ../gallery_edit.php?gid=$gid");
					}
				} else {
					$list = $_POST['ibank'];
					$gid = $_POST['gallery'];
					$current = get_gallery_images($gid);				
					$or=count($current); // Order starts at current image count of gallery 
					foreach ($list as $img) {
						$q = "insert into gallery_images(gallery_id,order_num,image_file) 
							  values ('$gid','$or','$img')";
						$ins = mysql_query($q)
							or die(mysql_error());
						$or++;
					}
					// Mark update
					mark_update();
					header("Location: ../gallery_edit.php?gid=$gid");
				}
			} else if ($_POST['image_action'] == 'delete') { 
				$iid = $_POST['deleteImage'];
				$gid = $_POST['gallery'];
				$ok = delete_gallery_image($iid,$gid);	
				// Mark update
				mark_update();			
				header("Location: ../gallery_edit.php?gid=$gid");
			} else {
				while (list($key, $val) = each($_POST)) {
					echo "$key => $val\n";
				}
			}			
			break;
		case 'categories':
			if ($_POST['deleteCat'] > 0) {
				$cid = $_POST['deleteCat'];
				$ok = remove_category($cid);							
			} else if ($sval == 'Save') {
				// creating new category
				$cat = $_POST['name'];
				$order = $_POST['order'];
				$dupes = 2;
				if ($cat != '') {
					if (category_exists($cat)) {
						$ren = $cat . ' ' . $dupes;
						while(category_exists($ren)) {
							$dupes++;
							$ren = $cat . ' ' . $dupes;
						}				
						$cat = $ren;		
					}
					$q = "insert into categories (category_name, order_num) values ('$cat','$order')";
					$ins = mysql_query($q)
						or die(mysql_error());
				}
			} else if ($sval == 'Go') {
				$id = $_POST['renameID'];
				$rename = $_POST['rename'.$id];
				$q = "update categories set category_name = '$rename' where category_id = '$id'";
				$update = mysql_query($q)
					or die(mysql_error());
			}
			// Mark update
			mark_update();
			header("Location: ../gallery_admin.php?mode=categories");
			break;
		case 'editcat': 
			if ($sval == 'Save Changes') {
				$name = $_POST['name'];
				$id = $_POST['id'];
				$q = "update categories set category_name = '$name' where category_id = '$id'";
				$upd = mysql_query($q)
					or die(mysql_error());
				// Mark update
				mark_update();
				header("Location: ../gallery_admin.php?mode=categories");
			}
			break;
		case 'galleries':
			if ($_POST['deleteGal'] > 0) {
				$gid = $_POST['deleteGal'];
				$ok = remove_gallery($gid);							
			} else if ($_POST['cgMode'] == 'galleries') {
				$a = 'name_gal_' . $_POST['galNum'];
				$name = $_POST[$a];
				$cid = $_POST['the_cat_id'];
				$q = "select count(*) as num from galleries where category_id = '$cid'";
				$query = mysql_query($q)
					or die(mysql_error());
				$row = mysql_fetch_array($query,MYSQL_ASSOC);
				$galorder = $row['num']+1;
				// Hidden and password protected
				$hidden = (isset($_POST['gallery_hidden'])) ? '1' : '0';
				$password = (isset($_POST['gallery_protect']) && strlen($_POST['gallery_password']) > 0) ? $_POST['gallery_password'] : '';
				// Now, insert gallery
				$q = "insert into galleries (category_id, gallery_name, order_num, gallery_hidden, password) values ('$cid','$name','$galorder','$hidden','$password')";
				$ins = mysql_query($q)
					or die(mysql_error());
				//
				// make sure we un-anchor the category if there is more than one gallery
				$id = $_POST['the_cat_id'];
				$q = "select * from categories where category_id = '$id'";
				$query = mysql_query($q)
					or die(mysql_error());
				$row = mysql_fetch_array($query,MYSQL_ASSOC);
				$which = $row['anchor'];
				$which = 0;
				$q = "update categories set anchor = '$which' where category_id = '$id'";
				$upd = mysql_query($q)
					or die(mysql_error());
				//
			} else if ($sval == 'Go') {
				$id = $_POST['renameID'];
				$rename = $_POST['rename'.$id];
				$q = "update galleries set gallery_name = '$rename' where gallery_id = '$id'";
				$update = mysql_query($q)
					or die(mysql_error());
			} else 	if ($_POST['deleteCat'] > 0) {
				$cid = $_POST['deleteCat'];
				$ok = remove_category($cid);
			} else if ($_POST['cgMode'] == 'cats') {
				$cat = $_POST['name'];
				$order = $_POST['order'];
				$dupes = 2;
				if ($cat != '') {
					if (category_exists($cat)) {
						$ren = $cat . ' ' . $dupes;
						while(category_exists($ren)) {
							$dupes++;
							$ren = $cat . ' ' . $dupes;
						}				
						$cat = $ren;		
					}
					$q = "insert into categories (category_name, order_num) values ('$cat','$order')";
					$ins = mysql_query($q)
						or die(mysql_error());
				}
			} else if ($_POST['cgMode'] == 'editcats') {
				$a = 'e_name_' . $_POST['galNum'];
				$name = $_POST[$a];
				$cid = $_POST['the_cat_id'];
				$q = "update categories set category_name = '$name' where category_id = '$cid'";
				$upd = mysql_query($q)
					or die(mysql_error());
			} else if ($_POST['cgMode'] == 'anchorGals') {
				$id = $_POST['anchorGal'];
				$q = "select * from categories where category_id = '$id'";
				$query = mysql_query($q)
					or die(mysql_error());
				$row = mysql_fetch_array($query,MYSQL_ASSOC);
				$which = $row['anchor'];
				if($which == 0) {
					$which = 1;
				} else {
					$which = 0;
				}
				$q = "update categories set anchor = '$which' where category_id = '$id'";
				$upd = mysql_query($q)
					or die(mysql_error());
			}
			// Mark update
			mark_update();
			// Redirect
			header("Location: ../gallery_admin.php?mode=galleries");
			break;
		case 'image_details':
			$file = $_POST['file'];
			$cap = $_POST['caption'];
			$cap = convert_to_numeric_values($cap);
			$q = "update gallery_images set image_caption = '$cap' where image_file = '$file'";
			$upd = mysql_query($q)
				or die(mysql_error());
			// Mark update
			mark_update();
			// Update notice
			$_SESSION['notice'] = 'Your image caption has been updated';
			// Redirect
			header("Location: ../display/popup/image_details.php?img=$file");
			break;
		case 'gal_rename':
			if ($_POST['Submit'] == 'Upload Thumb') {
				$id = $_POST['gal_id'];
				
				$file_temp = $_FILES['galThumb']['tmp_name'];
				$file_name = $_POST['gal_name'] . '.jpg';

				$file_path = $_SESSION['user']['path'];
				$filestatus = move_uploaded_file($file_temp,$file_path . "gallery/original/" . $file_name);
				$orig = $file_path . "gallery/original/" . $file_name;
				
				fitimage($orig, $file_path . "gallery/thumb/" . $file_name, 100, 100);
				fitimage($orig, $file_path . "gallery/small/" . $file_name, 300, 200);
				fitimage($orig, $file_path . "gallery/medium/" . $file_name, 600, 400);
				fitimage($orig, $file_path . "gallery/large/" . $file_name, 900, 600);
				fitimage($orig, $file_path . "gallery/square/" . $file_name, 75, 75, true);
				// Update notice
  			$_SESSION['notice'] = 'Your gallery has been updated';
				header("Location: ../display/popup/gal_settings.php?id=".$id);
			} else {
				$id = $_POST['gal_id'];
				$name = $_POST['gal_name'];
				$pass = $_POST['gal_pass'];
				if ($_SESSION['user']['email'] == 'info@herzogimages.com' || $_SESSION['template'] == 'york' || $_SESSION['template'] == 'arch_cape' || $_SESSION['template'] == 'aurora_way' || $_SESSION['template'] == 'newport' || $_SESSION['template'] == 'burnside' || $_SESSION['template'] == 'santiam' || $_SESSION['template'] == 'burlington' || $_SESSION['template'] == 'da' || substr($_SESSION['template'], 0, 6) == 'artist' || $_SESSION['template'] == 'greer'  || $_SESSION['template'] == 'pioneer' || $_SESSION['template'] == 'northbelize' || $_SESSION['template'] == 'santiam') {
					$q = "update galleries set gallery_name = '$name', password = '$pass'";
				} else {
					$q = "update galleries set gallery_name = '$name'";	
				}
				if (isset($_POST['gal_redirect'])) {
					$redirect = $_POST['gal_redirect'];
					$q .= ", gallery_redirect = '$redirect'";
				}
				if (isset($_POST['grass_flag'])) {
					$show_grass = $_POST['show_grass']==1;
					$q .= ", show_grass = '$show_grass'";
				}
				if (isset($_POST['birds_flag'])) {
					$show_birds = $_POST['show_birds']==1;
					$q .= ", show_birds = '$show_birds'";
				}
				if (isset($_POST['flowers_flag'])) {
					$show_flowers = $_POST['show_flowers']==1;
					$q .= ", show_flowers = '$show_flowers'";
				}
				if (isset($_POST['tree_flag'])) {
					$show_tree = $_POST['show_tree']==1;
					$q .= ", show_tree = '$show_tree'";
				}
				if (isset($_POST['clouds_flag'])) {
					$show_clouds = $_POST['show_clouds']==1;
					$q .= ", show_clouds = '$show_clouds'";
				}
				if (isset($_POST['balloons_flag'])) {
					$show_balloons = $_POST['show_balloons']==1;
					$q .= ", show_balloons = '$show_balloons'";
				}
				if (isset($_POST['kite_flag'])) {
					$show_kite = $_POST['show_kite']==1;
					$q .= ", show_kite = '$show_kite'";
				}
				if (isset($_POST['wagon_flag'])) {
					$show_wagon = $_POST['show_wagon']==1;
					$q .= ", show_wagon = '$show_wagon'";
				}
				if (isset($_POST['clothesline_flag'])) {
					$show_clothesline = $_POST['show_clothesline']==1;
					$q .= ", show_clothesline = '$show_clothesline'";
				}
				if (isset($_POST['pinwheels_flag'])) {
					$show_pinwheels = $_POST['show_pinwheels']==1;
					$q .= ", show_pinwheels = '$show_pinwheels'";
				}
				$q .= " where gallery_id = '$id'";
				$upd = mysql_query($q)
					or die(mysql_error());
				// Mark update
				mark_update();
				// Update notice
  			$_SESSION['notice'] = 'Your gallery has been updated';
				// Redirect
				header("Location: ../display/popup/gal_settings.php?id=".$id);
			}
			break;
	}
}
?>
