<?
// This processes the replace form from image details
include('db.inc.php');
include('functions.inc.php');
session_start();
authorize_user();
// POST vars
$orig = $_POST['image'];
$item = $_POST['itemtype'];
// Are we replacing the image or the thumb
$uploaddir = "../../gallery/";
if ($item == 'thumb') {
    $uploaddir = "../../gallery/thumbs/"; // replacing thumb only
}
$filename = $uploaddir.$orig;
$thumbname = "../../gallery/thumbs/".$orig;
$smname = "../../gallery/small/".$orig;
// Copy file to proper location
// Now, if this was the image, create a new thumbnail
if ($item == 'image') {
    $uploadSuccess = move_uploaded_file($_FILES['rfile']['tmp_name'], $filename);
    $thumbsuccess = thumbnail($filename, $thumbname, 50);
	$smsuccess = thumbnail($filename, $smname, 180);
} else if ($item == 'thumb') {
    $tmpname = '../../gallery/thumbs/tmpthumb.jpg';
    $uploadSuccess = move_uploaded_file($_FILES['rfile']['tmp_name'], $tmpname);
    $isize = getimagesize($tmpname);
    if ($isize[0] != 50 || $isize[1] != 50) {
        @unlink($tmpname);
    } else {
        @unlink($filename);
        rename($tmpname, $filename);
    }
}
// Redirect back to image details
$url = '../image_details.php?i='.$orig;
// Mark update
mark_update();
header("Location: $url");
?>
