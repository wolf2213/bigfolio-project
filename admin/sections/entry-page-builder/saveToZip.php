<?php	
	header("Content-type : text/html");
	
	function in_multiarray($elem, $array)
    {
        $top = sizeof($array) - 1;
        $bottom = 0;
        while($bottom <= $top)
        {
            if($array[$bottom] == $elem)
                return true;
            else 
                if(is_array($array[$bottom]))
                    if(in_multiarray($elem, ($array[$bottom])))
                        return true;
                    
            $bottom++;
        }        
        return false;
    }
	
	$htmlTemplateStart = <<<DOC
<!doctype html>
<!--[if lt IE 7 ]> <html class="no-js ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Entry Page Editor</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="/favicon.ico">
	<link href='http://fonts.googleapis.com/css?family=Droid+Sans&v1' rel='stylesheet' type='text/css'>
	
	<style type="text/css">
		html, body, div, span, object, iframe,
		h1, h2, h3, h4, h5, h6, p, blockquote, pre,
		abbr, address, cite, code, del, dfn, em, img, ins, kbd, q, samp,
		small, strong, sub, sup, var, b, i, dl, dt, dd, ol, ul, li,
		fieldset, form, label, legend,
		table, caption, tbody, tfoot, thead, tr, th, td,
		article, aside, canvas, details, figcaption, figure,
		footer, header, hgroup, menu, nav, section, summary,
		time, mark, audio, video {
		  margin: 0;
		  padding: 0;
		  border: 0;
		  font-size: 100%;
		  font: inherit;
		  vertical-align: baseline;
		}
		
		article, aside, details, figcaption, figure,
		footer, header, hgroup, menu, nav, section {
		  display: block;
		}
		
		blockquote, q { quotes: none; }
		blockquote:before, blockquote:after,
		q:before, q:after { content: ""; content: none; }
		ins { background-color: #ff9; color: #000; text-decoration: none; }
		mark { background-color: #ff9; color: #000; font-style: italic; font-weight: bold; }
		del { text-decoration: line-through; }
		abbr[title], dfn[title] { border-bottom: 1px dotted; cursor: help; }
		table { border-collapse: collapse; border-spacing: 0; }
		hr { display: block; height: 1px; border: 0; border-top: 1px solid #ccc; margin: 1em 0; padding: 0; }
		input, select { vertical-align: middle; }
		
		body { font:13px/1.231 sans-serif; *font-size:small; }
		select, input, textarea, button { font:99% sans-serif; }
		pre, code, kbd, samp { font-family: monospace, sans-serif; }
		
		html { overflow-y: scroll; }
		a:hover, a:active { outline: none; }
		ul, ol { margin-left: 2em; }
		ol { list-style-type: decimal; }
		nav ul, nav li { margin: 0; list-style:none; list-style-image: none; }
		small { font-size: 85%; }
		strong, th { font-weight: bold; }
		td { vertical-align: top; }
		sub, sup { font-size: 75%; line-height: 0; position: relative; }
		sup { top: -0.5em; }
		sub { bottom: -0.25em; }
		
		pre { white-space: pre; white-space: pre-wrap; word-wrap: break-word; padding: 15px; }
		textarea { overflow: auto; } 
		.ie6 legend, .ie7 legend { margin-left: -7px; } 
		input[type="radio"] { vertical-align: text-bottom; }
		input[type="checkbox"] { vertical-align: bottom; }
		.ie7 input[type="checkbox"] { vertical-align: baseline; }
		.ie6 input { vertical-align: text-bottom; }
		label, input[type="button"], input[type="submit"], input[type="image"], button { cursor: pointer; }
		button, input, select, textarea { margin: 0; }
		input:valid, textarea:valid   {  }
		input:invalid, textarea:invalid { border-radius: 1px; -moz-box-shadow: 0px 0px 5px red; -webkit-box-shadow: 0px 0px 5px red; box-shadow: 0px 0px 5px red; }
		.no-boxshadow input:invalid, .no-boxshadow textarea:invalid { background-color: #f0dddd; }		
		
		::-moz-selection{ background: #FF5E99; color:#fff; text-shadow: none; }
		::selection { background:#FF5E99; color:#fff; text-shadow: none; }
		a:link { -webkit-tap-highlight-color: #FF5E99; }
		button {  width: auto; overflow: visible; }
		.ie7 img { -ms-interpolation-mode: bicubic; }
		
		body, select, input, textarea { color: #444; }
		h1, h2, h3, h4, h5, h6 { font-weight: bold; }
		a, a:active, a:visited { color: #607890; }
		a:hover { color: #036; }		
									
		div#content { clear: left; width: 940px; margin: 0px auto; }
		.entry_page_section { clear: both; padding: 10px 0px; width: 100%; overflow: hidden; }	
		.logo .entry_page_section_content,
			.one_image .entry_page_section_content, 
			.two_images .entry_page_section_content, 
			.three_images .entry_page_section_content, 
			.four_images .entry_page_section_content 
			{ text-align: center; }
		.two_images .entry_page_section_content span,
			.three_images .entry_page_section_content span,
			.four_images .entry_page_section_content span
			{display: inline-block;}				
		.image_label { display: block; font-size: 14px; }
		.text_image .text_by_image { float: left; width: 50%; }
		.text_image .text_by_image > div { margin: 40px auto; width: 90%; }
		.text_image .image_by_text { float: right; text-align: center; width: 50%; }
		.image_text .text_by_image { float: right; width: 50%; }
		.image_text .text_by_image > div { margin: 40px auto; width: 90%; }
		.image_text .image_by_text { float: left; text-align: center; width: 50%; }
		.text .entry_page_section_content { padding: 40px 100px; }
		.minimeFeedDate { font-size: 12px; margin: 3px; }
		.rss_feed h4 { margin: 0px auto; padding: 0px; font-size: 18px; width: 600px; }
		.minimeFeed { margin: 5px auto; padding: 0px; width: 600px; }
			.minimeFeed li { cursor: default; list-style: none; margin-top: 5px; padding: 5px; border: 1px solid #D7D7D7; }
				.minimeFeed li:hover { cursor: default; background-color: #EBEBEB; border: 1px solid #949494; }
			.minimeFeed a:hover { font-style: normal; text-decoration: underline; }
			.minimeFeed a:active { color: #00AE57; }
			.minimeFeed a, a:visited { color: #006C36; text-decoration: none; font-style: italic; }
		.tweet_list { overflow-y: hidden; width: 600px; margin: 0 auto; }
			.tweet_list li { overflow-y: auto; overflow-x: hidden; padding: 0.5em; }
			.tweet_list .tweet_avatar {	padding-right: .5em; float: left; }				
		.copyright .entry_page_section_content { text-align: center; padding: 40px 40px 10px; }
		.image_link_button { display: none; }
		#contentBackground { display: none; }				
	</style>
</head>

<body>
	<div id="content">
DOC;

$htmlTemplateEnd = <<<DOC
	</div>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.js"></script>
	<script type="text/javascript" src="js/jquery.tweet.js"></script>
	<script type="text/javascript" src="js/jquery.minimefeed.min.js"></script>
	<script type="text/javascript" src="js/jquery.backgrounder.js"></script>
	
	<script type="text/javascript">
		$(".rss_feed").each(			
			function(index) {
				$(this).children().remove();
				$(this).minimeFeed($(this).attr("title"));
			}
		);
		$(".twitter").each(			
			function(index) {
				$(this).children().remove();
				$(this).tweet({
					avatar_size : 40,
					count : 5,
					username : [$(this).attr("title")],
					loading_text : "loading..."
				}).bind("loaded",  function() {
					$("#content").css("color", "#" + $("#text_color_placeholder").val());
					$("#content a:link").css("color", "#" + $("#link_color_placeholder").val());
					$("#content").css("font-family", $("#font_placeholder").val());	
				};				    
			}
		);
		
		$("body").css("background-color", "#" + $("#background_color_placeholder").val());
		$("#content").css("width", $("#page_width_placeholder").val());		
		
		$("style").append("#content {color: #" + $("#text_color_placeholder").val() + "; font-family: " + $("#font_placeholder").val() + "; }");
		$("style").append("#content a, a:visited {color: #" + $("#link_color_placeholder").val() + ";}");
				
		$(window).load(						
			function() {
				var contentWidth = $("#content").width();
				$(".two_images:not(#two_images_section_template), .three_images:not(#three_images_section_template), .four_images:not(#four_images_section_template)").each(
					function() {
						var totalImagesWidth = 0;				
						$(".entry_page_section_content span div img", $(this)).each (
							function() {
								totalImagesWidth += $(this).width();
							}
						)
						var spacerWidth = Math.floor((contentWidth - totalImagesWidth) / ($(".entry_page_section_content span div img", $(this)).length + 1));			
						$(".imageSpacer", $(this)).css("width", spacerWidth);
					}
				)
				
				$('#contentBackground').backgrounder({element : 'body'});
			}
		);
		
		$('#contentBackground').backgrounder({element : 'body'});
		
	</script> 
	
</body>
</html>
DOC;
	
	$entryPage = $htmlTemplateStart."\n".stripslashes(html_entity_decode($_POST["saveToZipSource"]))."\n".$htmlTemplateEnd;
	
	$entryPageDOM = new DOMDocument();
	$entryPageDOM->loadHTML($entryPage);
	
	$entryPageSectionsNode = $entryPageDOM->getElementById("entry_page_sections");
	$entryPageSectionNodes = $entryPageSectionsNode->childNodes;
	for ($i = 0; $i < $entryPageSectionNodes->length; $i++) {
		$currentNode = $entryPageSectionNodes->item($i);
		if ($currentNode->nodeType == 1) {
			$innerNodes = $currentNode->childNodes;
			for ($j = 0; $j < $innerNodes->length; $j++) {
				$currentInnerNode = $innerNodes->item($j);
				if ($currentInnerNode->nodeType == 1 && $currentInnerNode->getAttribute("class") == "entry_page_section_buttons") {
					$currentNode->removeChild($currentInnerNode);
					break;
				}
			}
		}
	}
	
	$entryPageTitleNode = $entryPageDOM->getElementsByTagName("title")->item(0);
	$entryPageTitleNode->removeChild($entryPageTitleNode->firstChild);
	$entryPageTitleNode->appendChild(new DOMText(stripslashes($_POST["saveToZipTitle"])));
	
	$headNode = $entryPageDOM->getElementsByTagName("head")->item(0);	
	$metaDescriptionNode = $entryPageDOM->createElement("meta");
	$metaDescriptionNode->setAttribute("name", "description");
	$metaDescriptionNode->setAttribute("content", stripslashes($_POST["saveToZipDescription"]));
	$headNode->appendChild($metaDescriptionNode);
	$metaKeywordsNode = $entryPageDOM->createElement("meta");
	$metaKeywordsNode->setAttribute("name", "keywords");
	$metaKeywordsNode->setAttribute("content", stripslashes($_POST["saveToZipKeywords"]));
	$headNode->appendChild($metaKeywordsNode);
	
	$entryPage = $entryPageDOM->saveHTML();
	
	$zip = new ZipArchive();
	$filename = "./EntryPage.zip";
	
	$zip->open($filename, ZIPARCHIVE::CREATE);
	
	$zip->addEmptyDir("js");
	$jsFiles = glob("js/*");
	foreach ($jsFiles as $jsFile)
		$zip->addFile($jsFile);
	
	preg_match_all("/images\/\S+[(jpg)(jpeg)(gif)(png)(tif)(tiff)(pic)(pict)(pct)]/i",$entryPage, $matches);
	$zip->addEmptyDir("images");
	$imageFiles = glob("images/*");
	foreach ($imageFiles as $imageFile) {
		if (in_multiarray($imageFile, $matches)) {
			$zip->addFile($imageFile);
		}
	}	
	
	$zip->addFromString("index.php", $entryPage);
	$zip->close();	
	
	if (file_exists($filename)) {
		header('Content-Type: application/octet-stream');
	    header('Content-Description: File Transfer');	    
	    header('Content-Disposition: attachment; filename='.basename($filename));
	    header('Content-Transfer-Encoding: binary');
	    header('Expires: 0');
	    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	    header('Pragma: public');
	    header('Content-Length: ' . filesize($filename));
	    ob_clean();
	    flush();
	    readfile($filename);
	    unlink($filename);
	    exit;
	}
?>