<?php 
	header("Content-type : text/html; charset=UTF-8");
	error_reporting(E_ERROR | E_PARSE);
   	
	include('../../inc/db.inc.php');
	include('../../inc/functions.inc.php');
	
	$bgcolor = $_POST['backgroundColorVal'];
	$textcolor = $_POST['textColorVal'];
	$linkcolor = $_POST['linkColorVal'];
	$fontFam = $_POST['fontVal']; 
	//
	$htmlTemplateStart = <<<DOC
<!doctype html>
<!--[if lt IE 7 ]> <html class="no-js ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
	<title>Entry Page Editor</title>
	<meta name="viewport" content="width=1000">
	<link rel="shortcut icon" href="/favicon.ico">
	<link rel="stylesheet" href="entry/style.css?v=2">
	<script language="javascript">
	<!--
	function openwindow(theURL) {
	window.open(theURL,"02",'top=0,left=0,width='+(screen.availWidth-10)+',height='+(screen.availHeight-28)+',toolbar=no,location=no,status=no,menubar=no,resizable=no');
	} 
	//-->
	</script>
	<style type="text/css">
		body {
			background: $bgcolor;
			background-attachment: fixed;
			color: $textcolor;
			font-family: $fontFam;
		}                         
		a, a:hover, a:visited, a:hover:visited {
			color: $linkcolor;
		}
		a {
			text-decoration: none;
		}                         
		a:hover {
			text-decoration: underline;
		}
		div#entryContent { clear: left; width: 940px; margin: 0px auto; }
		.entry_page_section { clear: both; padding: 10px 0px; width: 100%; overflow: hidden; }	
		.logo .entry_page_section_content,
			.one_image .entry_page_section_content, 
			.two_images .entry_page_section_content, 
			.three_images .entry_page_section_content, 
			.four_images .entry_page_section_content 
			{ text-align: center; }
		.two_images .entry_page_section_content span,
			.three_images .entry_page_section_content span,
			.four_images .entry_page_section_content span
			{display: inline-block;} 
		.entry_page_section_content div {
			margin: 5px;
		}   			
		.image_label { display: block; font-size: 14px; padding: 5px; }
		.text_image .text_by_image { float: left; width: 49%; }
		.text_image .text_by_image > div { margin: 40px auto; width: 90%; }
		.text_image .image_by_text { float: right; text-align: center; width: 49%; }
		.image_text .text_by_image { float: right; width: 49%; }
		.image_text .text_by_image > div { margin: 40px auto; width: 90%; }
		.image_text .image_by_text { float: left; text-align: center; width: 49%; }
		.text .entry_page_section_content { padding: 40px 100px; }
		.minimeFeedDate { font-size: 12px; margin: 3px; }
		.rss_feed h4 { margin: 0px auto; padding: 0px; font-size: 18px; width: 600px; }
		.minimeFeed { margin: 5px auto; padding: 0px; width: 600px; }
			.minimeFeed li { cursor: default; list-style: none; margin-top: 5px; padding: 5px; border: 1px solid #D7D7D7; }
		.tweet_list { overflow-y: hidden; width: 600px; margin: 0 auto; }
			.tweet_list li { overflow-y: auto; overflow-x: hidden; padding: 0.5em; }
			.tweet_list .tweet_avatar {	padding-right: .5em; float: left; }				
		.copyright .entry_page_section_content { text-align: center; padding: 40px 40px 10px; }
		.image_link_button { display: none; }
		#contentBackground { display: none; }
		#entryContent { padding: 30px; }  
		.imageSpacer { width:0px; }  			
	</style>
</head>

<body>
	<div id="entryContent">
DOC;

$htmlTemplateEnd = <<<DOC
	</div>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.js"></script>
	<script type="text/javascript" src="entry/jquery.tweet.js"></script>
	<script type="text/javascript" src="entry/jquery.minimefeed.min.js"></script>
	<script type="text/javascript" src="entry/jquery.backgrounder.js"></script>

	<script type="text/javascript">
		$(".rss_feed").each(			
			function(index) {
				$(this).children().remove();
				$(this).minimeFeed($(this).attr("title"));
			}
		);
		$(".twitter").each(			
			function(index) {
				$(this).children().remove();
				$(this).tweet({
					avatar_size : 40,
					count : 5,
					username : [$(this).attr("title")],
					loading_text : "loading..."
				}).bind("loaded",  function() {
					$("#content").css("color", "#" + $("#text_color_placeholder").val());
					$("#content a:link").css("color", "#" + $("#link_color_placeholder").val());
					$("#content").css("font-family", $("#font_placeholder").val());	
				});				    
			}
		);
		
		$(window).load(						
			function() {
				var contentWidth = $("#content").width();
				$(".two_images:not(#two_images_section_template), .three_images:not(#three_images_section_template), .four_images:not(#four_images_section_template)").each(
					function() {
						var totalImagesWidth = 0;				
						$(".entry_page_section_content span div img", $(this)).each (
							function() {
								totalImagesWidth += $(this).width();
							}
						)
						var spacerWidth = Math.floor((contentWidth - totalImagesWidth) / ($(".entry_page_section_content span div img", $(this)).length + 1));			
						//$(".imageSpacer", $(this)).css("width", spacerWidth);
					}
				)
				
				//$('#contentBackground').backgrounder({element : 'body'});
				if ($('#contentBackground').find("img").length > 0) {
					$('#contentBackground').backgrounder({element : 'body'});
				}				
			}
		);
		
		
	</script> 
	
</body>
</html>
DOC;
	$_POST["previewSource"] = mb_convert_encoding($_POST["previewSource"], 'HTML-ENTITIES', 'UTF-8');
	$_POST["previewSource"] = stripslashes(html_entity_decode($_POST["previewSource"]));
	$_POST["previewSource"] = explode("<!--itemToRemove-->", $_POST["previewSource"]);
	$even = true;
	foreach($_POST["previewSource"] as $subSource) {
		if ($even) {
			$tempSource .= $subSource;
		}   
		$even = !$even;
	} 
	$_POST["previewSource"] = $tempSource;
	$removeItems = array(); 
	$removeItems[] = 'onclick="event.preventDefault();"'; 
	$removeItems[] = 'title="Click inside text area to edit. Click outside to finish editing."';
	$_POST["previewSource"] = str_replace($removeItems,'',$_POST["previewSource"]);
	//
	$entryPage = $htmlTemplateStart."\n".$_POST["previewSource"]."\n".$htmlTemplateEnd;
	
	$entryPageDOM = new DOMDocument();
	$entryPageDOM->loadHTML($entryPage);
	
	$entryPageSectionsNode = $entryPageDOM->getElementById("entry_page_sections");
	$entryPageSectionNodes = $entryPageSectionsNode->childNodes;
	for ($i = 0; $i < $entryPageSectionNodes->length; $i++) {
		$currentNode = $entryPageSectionNodes->item($i);
		if ($currentNode->nodeType == 1) {
			$innerNodes = $currentNode->childNodes;
			for ($j = 0; $j < $innerNodes->length; $j++) {
				$currentInnerNode = $innerNodes->item($j);
				if ($currentInnerNode->nodeType == 1 && $currentInnerNode->getAttribute("class") == "entry_page_section_buttons") {
					$currentNode->removeChild($currentInnerNode);
					break;
				}
			}
		}
	}
	
	$entryPageTitleNode = $entryPageDOM->getElementsByTagName("title")->item(0);
	$entryPageTitleNode->removeChild($entryPageTitleNode->firstChild);
	$entryPageTitleNode->appendChild(new DOMText(stripslashes($_POST["previewTitle"])));
	
	$headNode = $entryPageDOM->getElementsByTagName("head")->item(0);	
	$metaDescriptionNode = $entryPageDOM->createElement("meta");
	$metaDescriptionNode->setAttribute("name", "description");
	$metaDescriptionNode->setAttribute("content", stripslashes($_POST["previewDescription"]));
	$headNode->appendChild($metaDescriptionNode);
	$metaKeywordsNode = $entryPageDOM->createElement("meta");
	$metaKeywordsNode->setAttribute("name", "keywords");
	$metaKeywordsNode->setAttribute("content", stripslashes($_POST["previewKeywords"]));
	$headNode->appendChild($metaKeywordsNode);
	
	$entryPage = $entryPageDOM->saveHTML();
	$entryPage = str_replace("&Acirc;",'',$entryPage);
	
	echo $entryPage;
?>