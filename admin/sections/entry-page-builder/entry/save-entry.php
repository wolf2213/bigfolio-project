<?php
	header("Content-type : text/html");
	include('../../../inc/db.inc.php');
	include('../../../inc/functions.inc.php');
	include_once 'markdown.php';
	session_start();  
	
	// call remote files copy
	$postArr = array();
	$postArr['u'] = $_SESSION['user']['email'];
	$postArr['p'] = $_SESSION['user']['password'];
	$postArr['mode'] = 'gallery';
	$url = SERVER . '_bfadmin/write_entry_files.php'; // 
	$postResults = curl_post($url, $postArr, array());  
	
	$bgcolor = $_POST['backgroundColorVal'];
	$textcolor = $_POST['textColorVal'];
	$linkcolor = $_POST['linkColorVal'];
	$fontFam = $_POST['fontVal']; 
	
	$htmlTemplateStart = <<<DOC
<!doctype html>
<!--[if lt IE 7 ]> <html class="no-js ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<title>Entry Page Editor</title>
	<meta name="viewport" content="width=1000">
	<link rel="shortcut icon" href="/favicon.ico">
	<link rel="stylesheet" href="extra/style.css?v=2">
	<script>
	<!--
	function openwindow(theURL) {
	window.open(theURL,"02",'top=0,left=0,width='+(screen.availWidth-10)+',height='+(screen.availHeight-28)+',toolbar=no,location=no,status=no,menubar=no,resizable=no');
	} 
	//-->
	</script>
	<style type="text/css">
		body {
			background: $bgcolor;
			color: $textcolor;
			font-family: $fontFam;
		}                         
		a, a:hover, a:visited, a:hover:visited {
			color: $linkcolor;
		}                     
		a {
			text-decoration: none;
		}                         
		a:hover {
			text-decoration: underline;
		}
		div#entryContent { clear: left; width: 940px; margin: 0px auto; }
		.entry_page_section { clear: both; padding: 10px 0px; width: 100%; overflow: hidden; }	
		.logo .entry_page_section_content,
			.one_image .entry_page_section_content, 
			.two_images .entry_page_section_content, 
			.three_images .entry_page_section_content, 
			.four_images .entry_page_section_content 
			{ text-align: center; }
		.two_images .entry_page_section_content span,
			.three_images .entry_page_section_content span,
			.four_images .entry_page_section_content span
			{display: inline-block;}
		.entry_page_section_content div {
			margin: 5px;
		}			
		.image_label { display: block; font-size: 14px; padding: 5px; }
		.text_image .text_by_image { float: left; width: 49%; }
		.text_image .text_by_image > div { margin: 40px auto; width: 90%; }
		.text_image .image_by_text { float: right; text-align: center; width: 49%; }
		.image_text .text_by_image { float: right; width: 49%; }
		.image_text .text_by_image > div { margin: 40px auto; width: 90%; }
		.image_text .image_by_text { float: left; text-align: center; width: 49%; }
		.text .entry_page_section_content { padding: 40px 100px; }
		.minimeFeedDate { font-size: 12px; margin: 3px; }
		.rss_feed h4 { margin: 0px auto; padding: 0px; font-size: 18px; width: 600px; }
		.minimeFeed { margin: 5px auto; padding: 0px; width: 600px; }
			.minimeFeed li { cursor: default; list-style: none; margin-top: 5px; padding: 5px; border: 1px solid #D7D7D7; }
				.minimeFeed li:hover { cursor: default; background-color: #EBEBEB; border: 1px solid #949494; }
			.minimeFeed a:hover { font-style: normal; text-decoration: underline; }
			.minimeFeed a:active { color: #00AE57; }
			.minimeFeed a, a:visited { color: #006C36; text-decoration: none; font-style: italic; }
		.tweet_list { overflow-y: hidden; width: 600px; margin: 0 auto; }
			.tweet_list li { overflow-y: auto; overflow-x: hidden; padding: 0.5em; }
			.tweet_list .tweet_avatar {	padding-right: .5em; float: left; }			
		.copyright .entry_page_section_content { text-align: center; padding: 40px 40px 10px; }
		.image_link_button { display: none; }
		#contentBackground { display: none; }
		#entryContent { padding: 30px; }	
		.imageSpacer { width:0px; }			
	</style>
	<link rel="stylesheet" href="styles/?s=CUSTOM_ENTRY_CSS" type="text/css" media="screen" title="no title" />
</head>

<body>
<!--BFentryPageBuilder2-->
	<div id="entryContent">
DOC;

$htmlTemplateEnd = <<<DOC
	
	</div>
<!-- end entryContent -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.js"></script>
	<script type="text/javascript" src="extra/jquery.tweet.js"></script>
	<script type="text/javascript" src="extra/jquery.minimefeed.min.js"></script>
	<script type="text/javascript" src="extra/jquery.backgrounder.js"></script>	

	<script type="text/javascript">
		$(".rss_feed").each(			
			function(index) {
				$(this).children().remove();
				$(this).minimeFeed($(this).attr("title"));
			}
		);
		$(".twitter").each(			
			function(index) {
				$(this).children().remove();
				$(this).tweet({
					avatar_size : 40,
					count : 5,
					username : [$(this).attr("title")],
					loading_text : "loading..."
				}).bind("loaded",  function() {
					$("#content").css("color", "#" + $("#text_color_placeholder").val());
					$("#content a:link").css("color", "#" + $("#link_color_placeholder").val());
					$("#content").css("font-family", $("#font_placeholder").val());	
				});				    
			}
		);
		
		$("body").css("background-color", "#" + $("#background_color_placeholder").val());
		$("#content").css("width", $("#page_width_placeholder").val());		
		
		//$("style").append("#content {color: #" + $("#text_color_placeholder").val() + "; font-family: " + $("#font_placeholder").val() + "; }");
		//$("style").append("#content a, a:visited {color: #" + $("#link_color_placeholder").val() + ";}");
		
		$(window).load(						
			function() {
				var contentWidth = $("#content").width();
				$(".two_images:not(#two_images_section_template), .three_images:not(#three_images_section_template), .four_images:not(#four_images_section_template)").each(
					function() {
						var totalImagesWidth = 0;				
						$(".entry_page_section_content span div img", $(this)).each (
							function() {
								totalImagesWidth += $(this).width();
							}
						)
						var spacerWidth = Math.floor((contentWidth - totalImagesWidth) / ($(".entry_page_section_content span div img", $(this)).length + 1));			
						//$(".imageSpacer", $(this)).css("width", spacerWidth);
					}
				)
				
				//$('#contentBackground').backgrounder({element : 'body'}); 
				if ($('#contentBackground').find("img").length > 0) {
					$('#contentBackground').backgrounder({element : 'body'});
				};
			}
		);
		
		
	</script> 
%%STATS%%
</body>
</html>
DOC;
	 
	$_POST["source"] = mb_convert_encoding($_POST["source"], 'HTML-ENTITIES', 'UTF-8');
	$_POST["source"] = stripslashes(html_entity_decode($_POST["source"]));
	$_POST["source"] = explode("<!--itemToRemove-->", $_POST["source"]);
	$even = true;
	foreach($_POST["source"] as $subSource) {
		if ($even) {
			$tempSource .= $subSource;
		}   
		$even = !$even;
	} 
	$_POST["source"] = $tempSource;
	$removeItems = array(); 
	$removeItems[] = 'onclick="event.preventDefault();"';
	$removeItems[] = 'title="Click inside text area to edit. Click outside to finish editing."';
	$removeItems[] = "Â";
	$removeItems[] = "&Acirc;";
	$_POST["source"] = str_replace($removeItems,'',$_POST["source"]);
	//
	$entryPage = $htmlTemplateStart."\n".$_POST["source"]."\n".$htmlTemplateEnd;
	
	$entryPageDOM = new DOMDocument();
	$entryPageDOM->loadHTML($entryPage);
	
	$entryPageSectionsNode = $entryPageDOM->getElementById("entry_page_sections");
	$entryPageSectionNodes = $entryPageSectionsNode->childNodes;
	for ($i = 0; $i < $entryPageSectionNodes->length; $i++) {
		$currentNode = $entryPageSectionNodes->item($i);
		if ($currentNode->nodeType == 1) {
			$innerNodes = $currentNode->childNodes;
			for ($j = 0; $j < $innerNodes->length; $j++) {
				$currentInnerNode = $innerNodes->item($j);
				if ($currentInnerNode->nodeType == 1 && $currentInnerNode->getAttribute("class") == "entry_page_section_buttons") {
					$currentNode->removeChild($currentInnerNode);
					break;
				}
			}
		}
	}
	
	$entryPageTitleNode = $entryPageDOM->getElementsByTagName("title")->item(0);
	$entryPageTitleNode->removeChild($entryPageTitleNode->firstChild);
	$entryPageTitleNode->appendChild(new DOMText(stripslashes($_POST["title"])));
	
	$headNode = $entryPageDOM->getElementsByTagName("head")->item(0);	
	$metaDescriptionNode = $entryPageDOM->createElement("meta");
	$metaDescriptionNode->setAttribute("name", "description");
	$metaDescriptionNode->setAttribute("content", stripslashes($_POST["description"]));
	$headNode->appendChild($metaDescriptionNode);
	$metaKeywordsNode = $entryPageDOM->createElement("meta");
	$metaKeywordsNode->setAttribute("name", "keywords");
	$metaKeywordsNode->setAttribute("content", stripslashes($_POST["keywords"]));
	$headNode->appendChild($metaKeywordsNode);
	
	$entryPage = $entryPageDOM->saveHTML();
	$entryPage = str_replace("&Acirc;",'',$entryPage);
	
	$id = create_new_template('ENTRY_PAGE', 'HTML', addslashes($entryPage));
	
	$splitId = preg_split("/[:_]/", $id);
	$entry_page_title = "Prior to " . $splitId[5] . "/" . $splitId[6] . "/" . $splitId[4] . " @ " . $splitId[7] . ":" . $splitId[8] . ":" . $splitId[9]; 
	
	// save start mode
	$startMode = $_POST['startMode'];
	$q = "update settings set start_mode = '$startMode' 
		  where settings_id = 1";
	$upd = mysql_query($q)
		or die(mysql_error());
	
	echo '<option value="' . htmlspecialchars($id) . '" >' . htmlspecialchars($entry_page_title) . '</option>';
	//echo 'post results: '.$postResults;			
?>