<?php 

error_reporting(E_ERROR | E_PARSE);

header("Content-type : text/html");
include_once('../../../inc/db.inc.php');
include_once('../../../inc/functions.inc.php'); 

include_once 'markdown.php';
session_start();
$url = $_SESSION['user']['url'];
$path = $_SESSION['user']['path'];
//$s = get_settings();

$row = get_source_template($_POST['id']);
$entryPageDOM = new DOMDocument();
if (stristr($row['contents'], '<div id="entryContent">') != false) {
	$entryPageDOM->loadHTML(html_entity_decode($row['contents']));
	$contentElement = $entryPageDOM->getElementById("entryContent");
	echo $entryPageDOM->saveXML($contentElement); 
} else {
	echo '';
}   

?>