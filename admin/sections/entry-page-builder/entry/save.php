<?
header("Content-type : text/html");
include('../inc/db.inc.php');
include('../inc/functions.inc.php');
include_once 'markdown.php';
session_start();
$url = $_SESSION['user']['url'];
$path = $_SESSION['user']['path'];
$s = get_settings();

// Save the entry css template
$css = file_get_contents('./entry-shell.css');
reset($_POST);
while (list($key, $val) = each($_POST)) {
    $find = '<!--'.$key.'-->';
		$replace = $val;
		$css = str_replace($find, $replace, $css);
}
// Having trouble with the styles in db, so we're gonna save this to an extra/ file
file_put_contents($path.'extra/entry-styles.css',$css);

// Get the shell html file
$shell = file_get_contents('./entry-shell.html');

// Replace image block
$num = $_POST['entry_num_images'];
$block = file_get_contents('./_image-'.$num.'.html');
$shell = str_replace('<!--image_block-->', $block, $shell);

$cid = $num == '1' ? 'container-single' : 'container-multi';
$shell = str_replace('<!--container_id-->', $cid, $shell);

// Do the title
if ($_POST['entry_title_mode'] == 'file') {
    $logo  = "images/" . $s['logo_file'];
    $title = "<h1 id=\"hd-logo\"><img src=\"$logo\" /></h1>";
} else if ($_POST['entry_title_mode'] == 'text') {
    $str = $_POST['entry_title_text'];
    $title = "<h1 id=\"hd-title\">$str</h1>";
} else {
    $title = '';
}
$shell = str_replace('<!--title_tag-->', $title, $shell);

// Check for twitter and add 
$jqLoaded = false;
if ($_POST['tweets'] == 'yes') {
  
  $jq = file_get_contents('./_jquery.html');
  $shell = str_replace('<!--jquery-->', $jq, $shell);
  $jqLoaded = true;
  
  
  $tw = file_get_contents('./_twitter.html');
  $shell = str_replace('<!--twitter-->', $tw, $shell);
} else {
  $shell = str_replace('<!--twitter-->', '', $shell);
}

// Check for blog
if ($_POST['blog'] == 'yes') {
  copy('./rss_proxy.php', $path.'extra/rss_proxy.php');
  
  if (!$jqLoaded) {
    $jq = file_get_contents('./_jquery.html');
    $shell = str_replace('<!--jquery-->', $jq, $shell);
    $jqLoaded = true;
  }
  
  $tw = file_get_contents('./_blog.html');
  $shell = str_replace('<!--blog-->', $tw, $shell);
  
} else {
  $shell = str_replace('<!--blog-->', '', $shell);
}

// Markdown the description
$_POST['entry_markdown'] = Markdown($_POST['entry_markdown']);


// Replace all values
reset($_POST);
while (list($key, $val) = each($_POST)) {
    $find = '<!--'.$key.'-->';
		$replace = stripslashes($val);
		$shell = str_replace($find, $replace, $shell);
}

$shell = addslashes($shell);

// Backup old entry page
$q = "delete from `templates` where `template_id` = 'ENTRY_PAGE_BACKUP'";
$my = mysql_query($q)
  or die(mysql_error());
  
$q = "update `templates` set `template_id` = 'ENTRY_PAGE_BACKUP' where `template_id` = 'ENTRY_PAGE'";
$my = mysql_query($q)
  or die(mysql_error());
  
$q = "insert into `templates` (template_id, template_type, contents) values ('ENTRY_PAGE', 'HTML', '$shell')";
$my = mysql_query($q)
  or die(mysql_error());
echo $template;
?>