function loadPreview() {
	var selected = $('#num_images');
	var num = selected.val();

	// Update the hidden field we use to save
	$('#entry_num_images').val(num);
	
	// Load the iframe preview
	$('#preview').html('<iframe id="preview-iframe" />');
	$('#preview-iframe').attr('src', 'entry/rpc-preview.php?images='+num);
	previewLoaded = true;
	
	
	// We need to get all the appropriate values here
	for (var i=1; i<4; i++) {
		if (i <= num) {
			$('#image-'+i).show();                                                
			
			$('#columns_'+i).val(num);
			$('#file_name_'+i).val('entry_image-'+num+'-'+i+'.jpg');
			
			
		} else {
			$('#image-'+i).hide();
		}
	}
}

function previewImage(data) {
	// alert(data);
	rArr = data.split('|');
	var entryTarget = '#image-'+rArr[1]+'-'+rArr[2];
	$('#preview-iframe').contents().find(entryTarget).attr("src",rArr[0] + '?' + (new Date()).getTime() );
}

function checkBlog() {
	if ($('#blog').attr('checked')) {
		$('#blog').val('yes');
		$('#blog_feed_container').show();
		$('#preview-iframe').contents().find('#blog-posts').show();
	} else {
		$('#blog').val('no');
		$('#blog_feed_container').hide();
		$('#preview-iframe').contents().find('#blog-posts').hide();
	}
}
function checkTweets() {
	if ($('#tweets').attr('checked')) {
		$('#tweets').val('yes');
		$('#tweet_user_container').show();
		$('#preview-iframe').contents().find('#twitter-preview').show();
	} else {
		$('#tweets').val('no');
		$('#tweet_user_container').hide();
		$('#preview-iframe').contents().find('#twitter-preview').hide();
	}
}
function doTwitter() {
	var userName = $('#twitter_user').val();
	
	$('#preview-iframe').contents().find('#twitter').getTwitter({
		userName: userName,
		numTweets: 5,
		loaderText: "Loading tweets...",
		slideIn: true,
		showHeading: true,
		headingText: "Latest Tweets",
		showProfileLink: true
	});
}

function updateLink(num) {
	var selected = $("#link_"+num);
	var box = $('#url_'+num+'_container');
	var t = selected.val();
	if (t == 'url') {
		$('#url_'+num).val('http://');
		box.show('fast', function() {
		  $('#url_'+num).select();
		});
	} else {
		box.hide('slow');
		var link = t == 'main' ? "main.php" : t == 'html' ? './html/' : "javascript:openwindow('main.php');";
		$('#url_'+num).val(link);
	}
}
function updateLogo() {
	// Get the value
	var val = $('#logo').val();
	// Update the hidden field for saving
	$('#entry_title_mode').val(val);
	// Show/hide the appropriate title elements in the preview
	if (val == 'file') {
		$('#preview-iframe').contents().find('#hd-logo').show();
		$('#preview-iframe').contents().find('#hd-title').hide();
	} else {
		$('#preview-iframe').contents().find('#hd-logo').hide();
		$('#preview-iframe').contents().find('#hd-title').show();
	}
}
function updateLabel(num) {
	var selected = $('#num_images');
	var cols = selected.val();
	var val = $('#label_'+num).val();
	var label = $('#preview-iframe').contents().find('#label-'+cols+'-'+num);
	label.html('<h2><a href="http://www.yahoo.com">'+val+'</a></h2>');
}

function updateColors() {
	var bColor = $('#body_color').val();
	$('#preview-iframe').contents().find('html').css({'background' : '#' + bColor})
	$('#entry_body_color').val(bColor);
	
	var tColor = $('#text_color').val();
	$('#preview-iframe').contents().find('html').css({'color' : '#' + tColor})
	$('#entry_text_color').val(tColor);
	
	var lColor = $('#link_color').val();
	$('#preview-iframe').contents().find('.fake-link').css({'color' : '#' + lColor})
	$('#preview-iframe').contents().find('a').css({'color' : '#' + lColor})
	$('#entry_link_color').val(lColor);
	
}
function updateFonts() {
	var tFont = $('#title_font').val();
	$('#preview-iframe').contents().find('h1').css({'font-family' : tFont})
	
	var lFont = $('#label_font').val();
	$('#preview-iframe').contents().find('.fake-link').css({'font-family' : lFont})
	
	var bFont = $('#body_font').val();
	$('#preview-iframe').contents().find('#description').css({'font-family' : bFont})
	$('#preview-iframe').contents().find('#blog-posts').css({'font-family' : bFont})
	$('#preview-iframe').contents().find('#entry-footer').css({'font-family' : bFont})
	$('#preview-iframe').contents().find('#twitter-preview').css({'font-family' : bFont})

}
function save() {
	var html = $('#preview-iframe').contents().find('body').html();
	
	// Update description with inner of preview
	var desc = $('#preview-iframe').contents().find('#description').html();
	$('#entry_description').val(desc)
	
	var inputs = Array();
	
	var dataString = '&';
	
	$('#settings_form')
	  .find(':input') // Find all the input elements under those.
	  .each(function(i) {
	  	// alert(this.name + " = " + this.val());
	  	dataString += this.name + '=' + $('#'+this.name).val() + '&';
	  });
	
	$('#colors_form')
	  .find(':input') // Find all the input elements under those.
	  .each(function(i) {
	  	// alert(this.name + " = " + this.val());
	  	dataString += this.name + '=' + $('#'+this.name).val() + '&';
	  });
	
	$('#fonts_form')
	  .find(':input') // Find all the input elements under those.
	  .each(function(i) {
	  	// alert(this.name + " = " + this.val());
	  	dataString += this.name + '=' + $('#'+this.name).val() + '&';
	  });
	
	$('#image_form_1')
	  .find(':input') // Find all the input elements under those.
	  .each(function(i) {
	  	// alert(this.name + " = " + this.val());
	  	dataString += this.name + '=' + $('#'+this.name).val() + '&';
	  });
	
	$('#image_form_2')
	  .find(':input') // Find all the input elements under those.
	  .each(function(i) {
	  	// alert(this.name + " = " + this.val());
	  	dataString += this.name + '=' + $('#'+this.name).val() + '&';
	  });
	
	$('#image_form_3')
	  .find(':input') // Find all the input elements under those.
	  .each(function(i) {
	  	// alert(this.name + " = " + this.val());
	  	dataString += this.name + '=' + $('#'+this.name).val() + '&';
	  });
	
	$('#import_form')
	  .find(':input') // Find all the input elements under those.
	  .each(function(i) {
	  	// alert(this.name + " = " + this.val());
	  	dataString += this.name + '=' + $('#'+this.name).val() + '&';
	  });
	
	$.ajax({
	  type: "POST",
	  url: "entry/save.php",
	  data: dataString,
	  dataType: "html",
	  success: function(msg){
		$.get("entry/complete.html", function(data){
      	  jQuery.facebox(data);
    	});
	  }
	});
	
}
