<?
// Includes
include('../inc/db.inc.php');
include('../inc/functions.inc.php');
include('entry_functions.php');
// Settings
$s = get_settings();
$url = (substr($_SESSION['user']['url'],-1) == '/') ? $_SESSION['user']['url'] : $_SESSION['user']['url'] . '/';

$logo = $url . 'images/' . $s['logo_file'];
$path = $_SESSION['user']['path'] . 'images/';
$extra = $url . 'extra/';
// Let's see if any of the entry images are already there ... 
$imageAA = file_exists($path.'entry_image-1-1.jpg') ? $extra.'entry_image-1-1.jpg' : 'entry-sample-1-1.jpg';
$imageBA = file_exists($path.'entry_image-2-1.jpg') ? $extra.'entry_image-2-1.jpg' : 'entry-sample-2-1.jpg';
$imageBB = file_exists($path.'entry_image-2-2.jpg') ? $extra.'entry_image-2-2.jpg' : 'entry-sample-2-2.jpg';
$imageCA = file_exists($path.'entry_image-3-1.jpg') ? $extra.'entry_image-3-1.jpg' : 'entry-sample-3-1.jpg';
$imageCB = file_exists($path.'entry_image-3-2.jpg') ? $extra.'entry_image-3-2.jpg' : 'entry-sample-3-2.jpg';
$imageCC = file_exists($path.'entry_image-3-3.jpg') ? $extra.'entry_image-3-3.jpg' : 'entry-sample-3-3.jpg';
// Container ID
$containerID = $_GET['images'] > 1 ? 'container-multi' : 'container-single';

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" 
 "http://www.w3.org/TR/html4/strict.dtd"> 
<html> 
<head> 
   <title>Entry Page Preview</title> 
   <link rel="stylesheet" type="text/css" href="preview.css"> 
   <link rel="stylesheet" href="facebox.css" type="text/css" media="screen" />
   
   <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js"></script>
   <script type="text/javascript" src="jquery.inplace.js"></script>
   <script type="text/javascript" src="facebox.js"></script>
   <script type="text/javascript">
       $(document).ready(function(){
   
           $(".edit-desc").editInPlace({
               url: "text-save.php",
               params: "ajax=yes&item=description",
               bg_over: "#cff",
               field_type: "textarea",
               textarea_rows: "5",
               textarea_cols: "100",
               saving_image: "ajax-loader.gif",
               callback: function(original_element, html){
                 $(parent.document).find("#entry_description").val(html);
                 return(html);
               }
           });
           
           $("#label-3-1").editInPlace({
             url: "text-save.php",
             params: "ajax=yes&item=label_3_1",
             callback: function(original_element, html){
               $(parent.document).find("#entry_label_1").val(html);
               return(html);
             }
           });
           $("#label-3-2").editInPlace({
             url: "text-save.php",
             params: "ajax=yes&item=label_3_2",
             callback: function(original_element, html){
               $(parent.document).find("#entry_label_2").val(html);
               return(html);
             }
           });
           $("#label-3-3").editInPlace({
             url: "text-save.php",
             params: "ajax=yes&item=label_3_3",
             callback: function(original_element, html){
               $(parent.document).find("#entry_label_3").val(html);
               return(html);
             }
           });
           $("#label-2-1").editInPlace({
             url: "text-save.php",
             params: "ajax=yes&item=label_2_1",
             callback: function(original_element, html){
               $(parent.document).find("#entry_label_1").val(html);
               return(html);
             }
           });
           $("#label-2-2").editInPlace({
             url: "text-save.php",
             params: "ajax=yes&item=label_2_2",
             callback: function(original_element, html){
               $(parent.document).find("#entry_label_2").val(html);
               return(html);
             }
           });
           $("#label-1-1").editInPlace({
             url: "text-save.php",
             params: "ajax=yes&item=label_1_1",
             callback: function(original_element, html){
               $(parent.document).find("#entry_label_1").val(html);
               return(html);
             }
           });
           
           $("#hd-title").editInPlace({
             url: "text-save.php",
             params: "ajax=yes&item=title",
             callback: function(original_element, html){
               $(parent.document).find("#entry_title_text").val(html);
               return(html);
             }
           });
            
           $("#browser_title").editInPlace({
              url: "text-save.php",
              params: "ajax=yes&item=browser_title",
              callback: function(original_element, html){
                $(parent.document).find("#entry_browser_title").val(html);
                $("#title_tip").hide();
                return(html);
              }
           });
           
           $('a[rel*=facebox]').facebox();
           
       });
   </script>
   
</head> 
<body> 
<div id="browser_title_container"><div id="browser_title">Click to edit your browser title ...</div> 
  <div id="title_tip"><a href="titles.html" rel="facebox">Writing a good browser title</a></div></div>
<div id="<?= $containerID ?>"> 
	   
    <div id="hd">
      <h1 id="hd-logo" style="display:none;"><img src="<?= $logo ?>" /></h1>
      <h1 id="hd-title" style="display:none;">Your Title</h1>
    </div> 

    <? if ($_GET['images'] == 1) { ?>
    <div id="image-inner-1">
      <div class="i-one first"><img src="<?= $imageAA ?>" id="image-1-1" /></div> 
    </div>
    <div id="label-inner-1" class="single-inner">
      <div id="label-1-1" class="l-one fake-link">Label 1</div>  
    </div>
    <? } ?>
    
    <? if ($_GET['images'] == 3) { ?>
    <div id="image-inner-3" class="multi-inner">
      <div class="i-third first"><img src="<?= $imageCA ?>" id="image-3-1" /></div> 
	    <div class="i-third"><img src="<?= $imageCB ?>" id="image-3-2" /></div> 
	    <div class="i-third"><img src="<?= $imageCC ?>" id="image-3-3" /></div> 
    </div>
    <div id="label-inner-3" class="multi-inner">
      <div id="label-3-1" class="l-third first fake-link">Label 1</div> 
	    <div id="label-3-2" class="l-third fake-link">Label 2</div> 
	    <div id="label-3-3" class="l-third fake-link">Label 3</div> 
    </div>
    <? } ?>

    <? if ($_GET['images'] == 2) { ?>
    <div id="image-inner-2">
      <div class="i-half first"><img src="<?= $imageBA ?>" id="image-2-1" /></div> 
	    <div class="i-half"><img src="<?= $imageBB ?>" id="image-2-2" /></div> 
    </div>
    <div id="label-inner-2" class="multi-inner">
      <div id="label-2-1" class="l-half first fake-link">Label 1</div> 
	    <div id="label-2-2" class="l-half fake-link">Label 2</div> 
    </div>
    <? } ?>
    
    <div id="description" class="edit-desc">Insert a 1-2 paragraph description of your business here. For optimal SEO, include references to the regions, cities, and venues where you frequently do business. You may also wish to include your contact information or links to additional sites (such as your client access page, Facebook profile, or Flickr account).<br /><br /><?=trim($s['meta_description'])?></div>
    
    <div id="blog-posts" style="display:none;">
      <h2 id="blog-header">Recent blog posts</h2>
      <p><em>Your most recent blog posts will be displayed here ... </em></p>
    </div>
    
    <div id="twitter-preview" style="display:none;">
      <h2>Latest Tweets</h2>
      <p><em>Your most recent tweets will be displayed here ... </em></p>
    </div>
    
    <div id="entry-footer">&copy; 2009 <?= $s['business_name'] ?> &bull; All rights reserved</div>

</div> 
</body> 
</html>