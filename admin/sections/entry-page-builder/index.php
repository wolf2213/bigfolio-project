<?php
if (get_magic_quotes_gpc()) {
    $process = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    while (list($key, $val) = each($process)) {
        foreach ($val as $k => $v) {
            unset($process[$key][$k]);
            if (is_array($v)) {
                $process[$key][stripslashes($k)] = $v;
                $process[] = &$process[$key][stripslashes($k)];
            } else {
                $process[$key][stripslashes($k)] = stripslashes($v);
            }
        }
    }
    unset($process);
}

// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');

// Include files
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");
//include ('entry/entry_functions.php');

session_start();

//authorize_user();
$s = get_settings();  
if ($s['logo_file'] != "" && strtolower(substr($s['logo_file'], -3)) != 'swf') {
	$logoAvailable = true;                                                          
}
$url = TEMP_URL . $_SESSION['user']['db_user'];

$fonts = array(	'Arial/Helvetica' => 'Arial, Helvetica, sans-serif',
				'Lucida/Monaco' => "'Lucida Console', Monaco, monospace",
				'Georgia' => 'Georgia, serif',
				'Tahoma/Geneva' => 'Tahoma, Geneva, sans-serif',
				'Verdana/Geneva' => 'Verdana, Geneva, sans-serif',
				'Times' => "'Times New Roman', Times, serif");
								
$entry_pages = get_source_templates('ENTRY_PAGE');
?>
<style type="text/css">
    #entryMain { width: 940px !important; height: 100%;} 
	h2 { font-size: large; }
	h3 { margin-bottom: 5px;}
	h4 { font-size: 15px; margin-top: 5px; }
	#edit { clear: both; margin: 20px 0px 5px 0px; border-top: 1px solid #f2f2f2; overflow: hidden;}
		#edit > div > div { width: 300px; float: left; margin: 20px 20px 20px 0px; padding-right: 20px; border-right: 1px solid #f2f2f2; min-height: 100%; }
		#edit div#backgroundImageSection { border: none; margin-left: 20px; width: 250px; }
		#edit input, #edit textarea { width: 280px !important; } 
		#edit textarea { height: 50px !important; }
	#page_title { width: 225px; }
	ul { list-style-type: none; }
	li { margin: 5px 0px; }
	.smaller { font-size: smaller; }
	#edit li label { float: left; margin-right: 0.5em; text-align: left; width: 160px; }
	#page_width_slider { margin: 7px 20px 0px 5px; }
	#page_selector_section { vertical-align: middle; padding: 10px 0px; float: left; }
		#page_selector_section + div { float: right; margin-bottom: 7px; }				
	.fine_print { display: block; float: right; font-size: x-small; margin-top: -2px; text-align: right; }
	.entry_page_section_buttons { position: absolute; margin-top:-10px;}
		.entry_page_section_buttons div { position: static; width: 940px; }
		.entry_page_section_buttons button { opacity: 0.7; filter: alpha(opacity = 70); }
	.add_section_button { vertical-align: top; }
	.delete_section_button { float: right; }
	#initial_buttons { clear: both; margin-top: 10px;}
	.move_section_handle { margin: 5px 0px 0px 0px; opacity: 0.5; filter: alpha(opacity = 50); cursor: move; z-index: 999; }
	.uploadable_image { cursor: crosshair; }
	.image_link_button { cursor: pointer; }
	.image_upload_form form { margin: 15px 0px 0px 20px; }
	form li { margin: 5px 0px 0px 0px; }
	#image_link_form form { margin: 15px 0px 0px 20px; }
	#load_content_dialog { text-align: center; }
		#load_content_dialog div { margin: 20px 0px 0px; }
	#save_successful_dialog { text-align: center; }
		#save_successful_dialog div { margin: 20px 0px 0px; }			 
	#save_content_dialog { text-align: center; }
		#save_content_dialog div { margin: 20px 0px 0px; }			 
	#image_uploading_dialog { text-align: center; }
		#image_uploading_dialog div { margin: 20px 0px 0px; }
	#previewButton { margin-left: 5px; }	
	#currentBackground { font-weight: bold; }
	#backgroundImage img { margin-top: 3px; width: 250px; }				
						
	.entry_page_section { clear: both; padding: 10px 0px; width: 100%; overflow: hidden; }	
	.logo .entry_page_section_content,
		.one_image .entry_page_section_content, 
		.two_images .entry_page_section_content, 
		.three_images .entry_page_section_content, 
		.four_images .entry_page_section_content 
		{ text-align: center; }
	.two_images .entry_page_section_content span,
		.three_images .entry_page_section_content span,
		.four_images .entry_page_section_content span
		{display: inline-block;}
	.entry_page_section_content div {
		margin: 5px;
	}
	.entry_page_section_content {
		z-index: 20;
	}	
	.image_label { display: block; font-size: 14px; padding: 5px;}
	.text_image .text_by_image { float: left; width: 49%; }
	.text_image .text_by_image > div { margin: 40px auto; width: 90%; }
	.text_image .image_by_text { float: right; text-align: center; width: 49%; }
	.image_text .text_by_image { float: right; width: 49%; }
	.image_text .text_by_image > div { margin: 40px auto; width: 90%; }
	.image_text .image_by_text { float: left; text-align: center; width: 49%; }
	.text .entry_page_section_content { padding: 40px 100px; }
	.minimeFeedDate { font-size: 12px; margin: 3px; }
	.rss_feed h4 { margin: 0px auto; padding: 0px; font-size: 18px; width: 600px; }
	.minimeFeed { margin: 5px auto; padding: 0px; width: 600px; }
		.minimeFeed li { cursor: default; list-style: none; margin-top: 5px; padding: 5px; border: 1px solid #D7D7D7; }
			.minimeFeed li:hover { cursor: default; background-color: #EBEBEB; border: 1px solid #949494; }
		.minimeFeed a:hover { font-style: normal; text-decoration: underline; }
		.minimeFeed a:active { color: #00AE57; }
		.minimeFeed a, a:visited { color: #006C36; text-decoration: none; font-style: italic; }			
	.tweet_list { overflow-y: hidden; width: 600px; margin: 0 auto; }
		.tweet_list li { overflow-y: auto; overflow-x: hidden; padding: 0.5em; }
		.tweet_list .tweet_avatar {	padding-right: .5em; float: left; }		
	.copyright .entry_page_section_content { text-align: center; padding: 40px 40px 10px; }
	#contentBackground { display: none; }
	#entryContent { clear: both; width: 940px; padding: 20px; border: 1px dotted #000;}
	#entryContent a, #entryContent a:active, #entryContent a:visited { color: #607890; }
	#entryContent a:hover { color: #036; }
	#entryContent .uploadable_image {  } 
	#font_selector { float: left;}
	.imageSpacer { width: 2px; }			
</style>

<div class="content_inner" id="entryMain">
	<div class="contentHeader">
		<h1>Entry Page Builder -</h1>  <a href="https://vimeo.com/127730137" target="_blank"><img src="img/watch-this-video2.png" border="0" style="position:absolute; margin-left:320px; margin-top:-50px;" /></a>
	</div>
	<div class="tip_message">Use this area to build an entry page for your site. Any entry page can be used to direct visitors to your portfolio site, blog, or other link. Use the various text area options to add SEO text. You can also edit the entry page source in the Advanced > Source Templates section.
		<div id="entryWarning">
		</div>	
	</div>
	
	<ul> 
		<li><label for="start_mode">Entry Page</label>
		<div class="uiButtonSet">
			<label for="start_mode_entry" class="radioLabel">On</label>
			<input name="start_mode" id="start_mode_entry" type="radio" value="ENTRY" class="bfChangeListen" <? if ($s['start_mode']=='ENTRY') { ?>checked="checked"<? } ?> />
			<label for="start_mode_flash" class="radioLabel">Off</label>
			<input name="start_mode" id="start_mode_flash" type="radio" value="FLASH" class="bfChangeListen" <? if ($s['start_mode']=='FLASH') { ?>checked="checked"<? } ?> />
		</div>
		</li>
		<li>
		<div id="page_selector_section">
			<?php
			if(mysql_num_rows($entry_pages) > 0) {
				echo '<label>Select version:</label>';
				echo '<select id="page_selector">';
				echo '<option value="ENTRY_PAGE" >Current Entry Page</option>';
				// Ignore the last row (number of rows minus one) since we already took care of the current entry page
				for($i = 0; $i < mysql_num_rows($entry_pages) - 1; $i++) {
					$entry_page = mysql_fetch_array($entry_pages);
					$splitId = preg_split("/[:_]/", $entry_page["template_id"]);
					$entry_page_title = "Prior to " . $splitId[5] . "/" . $splitId[6] . "/" . $splitId[4] . " @ " . $splitId[7] . ":" . $splitId[8] . ":" . $splitId[9];
					echo '<option value="' . htmlspecialchars($entry_page["template_id"]) . '" >' . htmlspecialchars($entry_page_title) . '</option>';
				}
				echo '</select>';
			}
			?>
		</div>
		</li>
	</ul>
	<div>
		<!--
		<button id="previewButton" onclick="preview()">
			Preview
		</button>
		<button class="save_button" onclick="save();">
			Save
		</button>
		<button id="saveToZipButton" onclick="saveToZip()">
			Save to ZIP File
		</button>
		-->					
	</div>
	<div id="edit">
		<h4>Settings<span class="smaller"> <a href="#" onclick="toggleStyleEditor()">show/hide</a></span></h4>
		<div>
			<div>
				<ul>
					<!--
					<li>
						<h3>Page Width: <span id="page_width">940</span>px</h3>
					</li>
				
					<li>
						<div id="page_width_slider" ></div>
					</li>
					<br /> 
					-->   						
					<h3>Colors and Fonts</h3>
					<li>
						<!--
						<label for="background_color">Bkgd Color: #</label>
						<input id="background_color" name="background_color" type="text">
						-->
						<label>Background Color</label>
						<input type='hidden' name='background_color' id='background_color' value='' class='bfChangeListen' />
						<div class='bfEntrySwatch' id="background_color_swatch"></div>
					</li>
					<li>
						<!--
						<label for="text_color">Text Color: #</label>
						<input id="text_color" name="text_color" type="text">
						-->
						<label>Text Color</label>
						<input type='hidden' name='text_color' id='text_color' value='' class='bfChangeListen' />
						<div class='bfEntrySwatch' id="text_color_swatch"></div>
					</li>
					<li>
						<!--
						<label for="link_color">Link Color: #</label>
						<input id="link_color" name="link_color" type="text">
						-->
						<label>Link Color</label>
						<input type='hidden' name='link_color' id='link_color' value='' class='bfChangeListen' />
						<div class='bfEntrySwatch' id="link_color_swatch"></div>
					</li>
					<li>
						<label>Font</label>
						<select id="font_selector" name="font">
							<?php
							foreach($fonts as $description => $value) {
								echo '<option value="' . htmlspecialchars($value) . '" >' . htmlspecialchars($description) . '</option>';
							}
							?>
						</select>
					</li>
					<li class="divider"></li>
				 	<li>
					<h3>Background Image <span class="smaller">(<a href="javascript:clearBackgroundImage()">clear</a>)</span></h3>							
					<div id="entry_background_uploadify" title="Upload">
						<div class="entryUploader">
							<input name="entrybackgroundfile" type="file" class="entrybackgroundfile" id="entrybackgroundfile" size="10" /> 
							<div id="background-status-message"></div>
							<div id="background-entry-queue"></div>
						</div>
					</div>
					<br />
					<span class="smaller">Current background: <span id="currentBackground"></span></span>
					<div id="backgroundImage"></div>
					</li>
				</ul>		
			</div>						
			<div>						
				<ul>
					<h3>Page Title in Browser:</h3>
					<li>
						<input id="page_title" name="page_title" type="text" class="bfInputText bfChangeListen" /><br /><br />
					</li>
					<h3>Description:</h3>								
					<li>
						<textarea id="meta_description" name="meta_description" class="bfTextarea bfChangeListen"></textarea>
					</li>
					<h3>Keywords:</h3>
					<li>
						<textarea id="meta_keywords" name="meta_keywords" class="bfTextarea bfChangeListen"></textarea><br /><span class="fine_print">(comma separated)</span>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div id="entryContent">
		<div id="entry_page_sections"></div>
		
		<input type="hidden" name="page_title_placeholder" id="page_title_placeholder" />
		<input type="hidden" name="page_width_placeholder" id="page_width_placeholder" />
		<input type="hidden" name="meta_description_placeholder" id="meta_description_placeholder" />
		<input type="hidden" name="meta_keywords_placeholder" id="meta_keywords_placeholder" />
		<input type="hidden" name="background_color_placeholder" id="background_color_placeholder" />
		<input type="hidden" name="text_color_placeholder" id="text_color_placeholder" />
		<input type="hidden" name="link_color_placeholder" id="link_color_placeholder" />
		<input type="hidden" name="font_placeholder" id="font_placeholder" />
		<input type="hidden" name="background_image_placeholder" id="background_image_placeholder" />
		
		<div id="contentBackground"></div>							
	</div>				
	<div id="initial_buttons">
		<button id="initial_add_section_button" class="add_section_button">
			Add Section
		</button> 
		<button id="previewButton" onclick="preview()">
			Preview
		</button>
		<!-- 
		<button id="clearAll" onclick="clearAllEntryContent()">
			Clear
		</button>
		
		<br />
		<br />
		<button class="save_button" onclick="save();">
			Save
		</button> 
		<button id="previewButton" onclick="preview()">
			Preview
		</button>
		-->   				
	</div>
	
	<!-- Hidden Section Templates --> 
	<?
	if ($logoAvailable) {  
	$ur = $_SESSION['user']['temp_url']; 
	?>
	<div id="logo_section_template" class="entry_page_section logo one_image">
		<div class="entry_page_section_buttons" >
			<div>
				<img src="sections/entry-page-builder/entry/move_handle.png" class="move_section_handle" />
				<button class="delete_section_button">
					Delete Section
				</button>
			</div>
		</div>
		<div class="entry_page_section_content">
			<img src="<? echo($ur); ?>/images/<?= $s['logo_file'] ?>" class="uploadable_image" alt="Logo" />
		</div>
	</div>
	<? } ?>
	<div id="one_image_section_template" class="entry_page_section one_image">
		<div class="entry_page_section_buttons" >
			<div>
				<img src="sections/entry-page-builder/entry/move_handle.png" class="move_section_handle" />
				<button class="delete_section_button">
					Delete Section
				</button>
			</div>
		</div>
		<div class="entry_page_section_content">
			<div>
				<a ><img src="sections/entry-page-builder/images/entry-sample-1-1.jpg" class="uploadable_image" alt="Sample Image Description" /></a>
			</div>
			<a ><span class="image_label editable">Label 1</span></a>
			<!--itemToRemoveMaybe--><img src="sections/entry-page-builder/entry/link.png" class="image_link_button" title="Click to Edit Image Label Link and Image Description" alt=""/><!--itemToRemoveMaybe-->
		</div>
	</div>
	<div id="two_images_section_template" class="entry_page_section two_images">
		<div class="entry_page_section_buttons" >
			<div>
				<img src="sections/entry-page-builder/entry/move_handle.png" class="move_section_handle" />
				<button class="delete_section_button">
					Delete Section
				</button>
			</div>
		</div>
		<div class="entry_page_section_content">						
			<span>
				<span>
					<a ><img src="sections/entry-page-builder/images/entry-sample-2-1.jpg" class="uploadable_image" alt="Sample Image Description" /></a>
				</span>
				<br>
				<a ><span class="image_label editable">Label 1</span></a>
				<!--itemToRemoveMaybe--><img src="sections/entry-page-builder/entry/link.png" class="image_link_button" title="Click to Edit Image Label Link and Image Description" alt=""/><!--itemToRemoveMaybe-->
			</span><span class="imageSpacer"></span><span>
				<span>
					<a ><img src="sections/entry-page-builder/images/entry-sample-2-2.jpg" class="uploadable_image" alt="Sample Image Description" /></a>
				</span>
				<br>
				<a ><span class="image_label editable">Label 2</span></a>
				<!--itemToRemoveMaybe--><img src="sections/entry-page-builder/entry/link.png" class="image_link_button" title="Click to Edit Image Label Link and Image Description" alt=""/><!--itemToRemoveMaybe-->
			</span>
		</div>
	</div>
	<div id="three_images_section_template" class="entry_page_section three_images">
		<div class="entry_page_section_buttons" >
			<div>
				<img src="sections/entry-page-builder/entry/move_handle.png" class="move_section_handle" />
				<button class="delete_section_button">
					Delete Section
				</button>
			</div>
		</div>
		<div class="entry_page_section_content">
			<span>
				<span>
					<a ><img src="sections/entry-page-builder/images/entry-sample-3-1.jpg" class="uploadable_image" alt="Sample Image Description" /></a>
				</span>
				<br>
				<a ><span class="image_label editable">Label 1</span></a>
				<!--itemToRemoveMaybe--><img src="sections/entry-page-builder/entry/link.png" class="image_link_button" title="Click to Edit Image Label Link and Image Description" alt=""/><!--itemToRemoveMaybe-->
			</span><span class="imageSpacer"></span><span>
				<span>
					<a ><img src="sections/entry-page-builder/images/entry-sample-3-2.jpg" class="uploadable_image" alt="Sample Image Description" /></a>
				</span>
				<br>
				<a ><span class="image_label editable">Label 2</span></a>
				<!--itemToRemoveMaybe--><img src="sections/entry-page-builder/entry/link.png" class="image_link_button" title="Click to Edit Image Label Link and Image Description" alt=""/><!--itemToRemoveMaybe-->
			</span><span class="imageSpacer"></span><span>
				<span>
					<a ><img src="sections/entry-page-builder/images/entry-sample-3-3.jpg" class="uploadable_image" alt="Sample Image Description" /></a>
				</span>
				<br>
				<a ><span class="image_label editable">Label 3</span></a>
				<!--itemToRemoveMaybe--><img src="sections/entry-page-builder/entry/link.png" class="image_link_button" title="Click to Edit Image Label Link and Image Description" alt=""/><!--itemToRemoveMaybe-->
			</span>
		</div>
	</div>
	<div id="four_images_section_template" class="entry_page_section four_images">
		<div class="entry_page_section_buttons" >
			<div>
				<img src="sections/entry-page-builder/entry/move_handle.png" class="move_section_handle" />
				<button class="delete_section_button">
					Delete Section
				</button>
			</div>
		</div>
		<div class="entry_page_section_content">
			<span>
				<span>
					<a ><img src="sections/entry-page-builder/images/entry-sample-4-1.jpg" class="uploadable_image" alt="Sample Image Description" /></a>
				</span>
				<br>
				<a ><span class="image_label editable">Label 1</span></a>
				<!--itemToRemoveMaybe--><img src="sections/entry-page-builder/entry/link.png" class="image_link_button" title="Click to Edit Image Label Link and Image Description" alt=""/><!--itemToRemoveMaybe-->
			</span><span class="imageSpacer"></span><span>
				<span>
					<a ><img src="sections/entry-page-builder/images/entry-sample-4-2.jpg" class="uploadable_image" alt="Sample Image Description" /></a>
				</span>
				<br>
				<a ><span class="image_label editable">Label 2</span></a>
				<!--itemToRemoveMaybe--><img src="sections/entry-page-builder/entry/link.png" class="image_link_button" title="Click to Edit Image Label Link and Image Description" alt=""/><!--itemToRemoveMaybe-->
			</span><span class="imageSpacer"></span><span>
				<span>
					<a ><img src="sections/entry-page-builder/images/entry-sample-4-3.jpg" class="uploadable_image" alt="Sample Image Description" /></a>
				</span>
				<br>
				<a ><span class="image_label editable">Label 3</span></a>
				<!--itemToRemoveMaybe--><img src="sections/entry-page-builder/entry/link.png" class="image_link_button" title="Click to Edit Image Label Link and Image Description" alt=""/><!--itemToRemoveMaybe-->
			</span><span class="imageSpacer"></span><span>
				<span>
					<a ><img src="sections/entry-page-builder/images/entry-sample-4-1.jpg" class="uploadable_image" alt="Sample Image Description" /></a>
				</span>
				<br>
				<a ><span class="image_label editable">Label 4</span></a>
				<!--itemToRemoveMaybe--><img src="sections/entry-page-builder/entry/link.png" class="image_link_button" title="Click to Edit Image Label Link and Image Description" alt=""/><!--itemToRemoveMaybe-->
			</span>
		</span>
		</div>
	</div>
	<div id="image_text_section_template" class="entry_page_section image_text">
		<div class="entry_page_section_buttons" >
			<div>
				<img src="sections/entry-page-builder/entry/move_handle.png" class="move_section_handle" />
				<button class="delete_section_button">
					Delete Section
				</button>
			</div>
		</div>
		<div class="entry_page_section_content" title="Click inside text area to edit. Click outside to finish editing.">
			<div class="image_by_text">
				<a ><img src="sections/entry-page-builder/images/entry-sample-2-1.jpg" class="uploadable_image" /></a>
				<br />
				<a ><span class="image_label editable">Label 1</span></a>
				<!--itemToRemoveMaybe--><img src="sections/entry-page-builder/entry/link.png" class="image_link_button" title="Click to Edit Image Label Link and Image Description" alt=""/><!--itemToRemoveMaybe-->
			</div>
			<div class="text_by_image">
				<div class="editableTextarea">Insert a 1-2 paragraph description of your business here. For optimal SEO, include references to the regions, cities, and venues where you frequently do business. You may also wish to include your contact information or links to additional sites (such as your client access page, Facebook profile, or Flickr account).</div>
			</div>
		</div>
	</div>
	<div id="text_image_section_template" class="entry_page_section text_image">
		<div class="entry_page_section_buttons" >
			<div>
				<img src="sections/entry-page-builder/entry/move_handle.png" class="move_section_handle" />
				<button class="delete_section_button">
					Delete Section
				</button>
			</div>
		</div>
		<div class="entry_page_section_content" title="Click inside text area to edit. Click outside to finish editing.">
			<div class="text_by_image">
				<div class="editableTextarea">Insert a 1-2 paragraph description of your business here. For optimal SEO, include references to the regions, cities, and venues where you frequently do business. You may also wish to include your contact information or links to additional sites (such as your client access page, Facebook profile, or Flickr account).</div>
			</div>
			<div class="image_by_text">
				<a ><img src="sections/entry-page-builder/images/entry-sample-2-1.jpg" class="uploadable_image" /></a>
				<br />
				<a ><span class="image_label editable">Label 1</span></a>
				<!--itemToRemoveMaybe--><img src="sections/entry-page-builder/entry/link.png" class="image_link_button" title="Click to Edit Image Label Link and Image Description" alt=""/><!--itemToRemoveMaybe-->
			</div>
		</div>
	</div>
	<div id="text_section_template" class="entry_page_section text">
		<div class="entry_page_section_buttons" >
			<div>
				<img src="sections/entry-page-builder/entry/move_handle.png" class="move_section_handle" />
				<button class="delete_section_button">
					Delete Section
				</button>
			</div>
		</div>
		<div class="entry_page_section_content" title="Click inside text area to edit. Click outside to finish editing.">Insert a 1-2 paragraph description of your business here. For optimal SEO, include references to the regions, cities, and venues where you frequently do business. You may also wish to include your contact information or links to additional sites (such as your client access page, Facebook profile, or Flickr account).</div>
	</div>
	<div id="rss_feed_section_template" class="entry_page_section">
		<div class="entry_page_section_buttons" >
			<div>
				<img src="sections/entry-page-builder/entry/move_handle.png" class="move_section_handle" />
				<button class="delete_section_button">
					Delete Section
				</button>
			</div>
		</div>
		<div class="entry_page_section_content rss_feed"></div>
	</div>
	<div id="twitter_section_template" class="entry_page_section">
		<div class="entry_page_section_buttons" >
			<div>
				<img src="sections/entry-page-builder/entry/move_handle.png" class="move_section_handle" />
				<button class="delete_section_button">
					Delete Section
				</button>
			</div>
		</div>
		<div class="entry_page_section_content twitter"></div>
	</div>
	<div id="copyright_section_template" class="entry_page_section copyright">
		<div class="entry_page_section_buttons" >
			<div>
				<img src="sections/entry-page-builder/entry/move_handle.png" class="move_section_handle" />
				<button class="delete_section_button">
					Delete Section
				</button>
			</div>
		</div>
		<div class="entry_page_section_content editable">All content &copy; <?=date("Y")?></div>
	</div>
	<!-- Hidden Dialog Forms -->
	<div id="section_menu" title="Add Section">
		<form>
			<fieldset>
				<ul> 
					<? if ($logoAvailable) { ?>
					<li>
						<input type="radio" name="section_type" value="logo" id="logo_radio" checked="checked" />
						<label for="logo_radio"> Logo </label>
					</li>
					<? } ?>
					<li>
						<input type="radio" name="section_type" value="one_image" id="one_image_radio" />
						<label for="one_image_radio"> 1 Image </label>
					</li>
					<li>
						<input type="radio" name="section_type" value="two_images" id="two_images_radio" />
						<label for="two_images_radio"> 2 Images </label>
					</li>
					<li>
						<input type="radio" name="section_type" value="three_images" id="three_images_radio" />
						<label for="three_images_radio"> 3 Images </label>
					</li>
					<li>
						<input type="radio" name="section_type" value="four_images" id="four_images_radio" />
						<label for="four_images_radio"> 4 Images </label>
					</li>
					<li>
						<input type="radio" name="section_type" value="image_text" id="image_text_radio" />
						<label for="image_text_radio"> 1 Image / Text </label>
					</li>
					<li>
						<input type="radio" name="section_type" value="text_image" id="text_image_radio" />
						<label for="text_image_radio"> Text / 1 Image </label>
					</li>
					<li>
						<input type="radio" name="section_type" value="text" id="text_radio" />
						<label for="text_radio"> Text </label>
					</li>
					<!--
					<li>
						<input type="radio" name="section_type" value="rss_feed" id="rss_feed_radio" />
						<label for="rss_feed_radio"> RSS Feed </label>
						<div>
							<input type="text" name="rss_feed_url" id="rss_feed_url" value="http://" />
						</div>
					</li>
					
					<li>
						<input type="radio" name="section_type" value="twitter" id="twitter_radio" />
						<label for="twitter_radio"> Twitter Feed </label>
						<div>
							<input type="text" name="twitter_handle" id="twitter_handle" value="@" />
						</div>
					</li>
					-->
					<li>
						<input type="radio" name="section_type" value="copyright" id="copyright_radio" />
						<label for="copyright_radio"> Copyright </label>
					</li>
				</ul>
			</fieldset>
		</form>
	</div>
	<div id="one_image_upload_form" class="image_upload_form" title="Select Image to Upload" >
		<form action="sections/entry-page-builder/entry/ajaxupload.php" method="post" name="one_image_upload_form" enctype="multipart/form-data">
			<input type="hidden" name="maxSize" value="9999999999" />
			<input type="hidden" name="maxW" value="750" />
			<input type="hidden" name="maxH" value="400" />
			<input type="hidden" name="fullPath" value="sections/entry-page-builder/images/" />
			<input type="hidden" name="relPath" value="../images/" />
			<input type="hidden" name="filename" value="filename" />
			<input type="hidden" name="type" value="oneImageUpload" />
			<p>
				<input type="file" name="filename"
				onchange="
				ajaxUpload(
				this.form,
				'sections/entry-page-builder/entry//ajaxupload.php?filename=name&amp;maxSize=9999999999&amp;fullPath=http://localhost/entrypageeditor/images/&amp;relPath=../images/&amp;colorR=255&amp;colorG=255&amp;colorB=255&amp;maxW=750&amp;maxH=400',
				'upload_area',
				'File Uploading Please Wait...&lt;br /&gt;&lt;img src=\'images/loader_light_blue.gif\' width=\'128\' height=\'15\' border=\'0\' /&gt;',
				'&lt;img src=\'images/error.gif\' width=\'16\' height=\'16\' border=\'0\' /&gt; Error in Upload, check settings and path info in source code.');
				$( '#one_image_upload_form' ).dialog( 'close' );
				$('#image_uploading_dialog').dialog('open');
				return false;" />
			</p>
		</form>
	</div>
	<div id="two_image_upload_form" class="image_upload_form" title="Select Image to Upload" >
		<form action="sections/entry-page-builder/entry/ajaxupload.php" method="post" name="two_image_upload_form" enctype="multipart/form-data">
			<input type="hidden" name="maxSize" value="9999999999" />
			<input type="hidden" name="maxW" value="450" />
			<input type="hidden" name="maxH" value="300" />
			<input type="hidden" name="fullPath" value="sections/entry-page-builder/images/" />
			<input type="hidden" name="relPath" value="../images/" />
			<input type="hidden" name="filename" value="filename" />
			<input type="hidden" name="type" value="twoImageUpload" />
			<p>
				<input type="file" name="filename"
				onchange="
				ajaxUpload(
				this.form,
				'sections/entry-page-builder/entry//ajaxupload.php?filename=name&amp;maxSize=9999999999&amp;fullPath=http://localhost/entrypageeditor/images/&amp;relPath=../images/&amp;colorR=255&amp;colorG=255&amp;colorB=255&amp;maxW=450&amp;maxH=300',
				'upload_area',
				'File Uploading Please Wait...&lt;br /&gt;&lt;img src=\'images/loader_light_blue.gif\' width=\'128\' height=\'15\' border=\'0\' /&gt;',
				'&lt;img src=\'images/error.gif\' width=\'16\' height=\'16\' border=\'0\' /&gt; Error in Upload, check settings and path info in source code.');
				$( '#two_image_upload_form' ).dialog( 'close' );
				$('#image_uploading_dialog').dialog('open');
				return false;" />
			</p>
		</form>
	</div>
	<div id="three_image_upload_form" class="image_upload_form" title="Select Image to Upload" >
		<form action="sections/entry-page-builder/entry/ajaxupload.php" method="post" name="three_image_upload_form" enctype="multipart/form-data">
			<input type="hidden" name="maxSize" value="9999999999" />
			<input type="hidden" name="maxW" value="300" />
			<input type="hidden" name="maxH" value="200" />
			<input type="hidden" name="fullPath" value="sections/entry-page-builder/images/" />
			<input type="hidden" name="relPath" value="../images/" />
			<input type="hidden" name="filename" value="filename" />
			<input type="hidden" name="type" value="threeImageUpload" />
			<p>
				<input type="file" name="filename"
				onchange="
				ajaxUpload(
				this.form,
				'sections/entry-page-builder/entry//ajaxupload.php?filename=name&amp;maxSize=9999999999&amp;fullPath=http://localhost/entrypageeditor/images/&amp;relPath=../images/&amp;colorR=255&amp;colorG=255&amp;colorB=255&amp;maxW=300&amp;maxH=200',
				'upload_area',
				'File Uploading Please Wait...&lt;br /&gt;&lt;img src=\'images/loader_light_blue.gif\' width=\'128\' height=\'15\' border=\'0\' /&gt;',
				'&lt;img src=\'images/error.gif\' width=\'16\' height=\'16\' border=\'0\' /&gt; Error in Upload, check settings and path info in source code.');
				$( '#three_image_upload_form' ).dialog( 'close' );
				$('#image_uploading_dialog').dialog('open');
				return false;" />
			</p>
		</form>
	</div>
	<div id="four_image_upload_form" class="image_upload_form" title="Select Image to Upload" >
		<form action="sections/entry-page-builder/entry/ajaxupload.php" method="post" name="four_image_upload_form" enctype="multipart/form-data">
			<input type="hidden" name="maxSize" value="9999999999" />
			<input type="hidden" name="maxW" value="225" />
			<input type="hidden" name="maxH" value="150" />
			<input type="hidden" name="fullPath" value="sections/entry-page-builder/images/" />
			<input type="hidden" name="relPath" value="../images/" />
			<input type="hidden" name="filename" value="filename" />
			<input type="hidden" name="type" value="fourImageUpload" />
			<p>
				<input type="file" name="filename"
				onchange="
				ajaxUpload(
				this.form,
				'sections/entry-page-builder/entry//ajaxupload.php?filename=name&amp;maxSize=9999999999&amp;fullPath=http://localhost/entrypageeditor/images/&amp;relPath=../images/&amp;colorR=255&amp;colorG=255&amp;colorB=255&amp;maxW=225&amp;maxH=150',
				'upload_area',
				'File Uploading Please Wait...&lt;br /&gt;&lt;img src=\'images/loader_light_blue.gif\' width=\'128\' height=\'15\' border=\'0\' /&gt;',
				'&lt;img src=\'images/error.gif\' width=\'16\' height=\'16\' border=\'0\' /&gt; Error in Upload, check settings and path info in source code.');
				$( '#four_image_upload_form' ).dialog( 'close' );
				$('#image_uploading_dialog').dialog('open');
				return false;" />
			</p>
		</form>
	</div>
	<div id="image_uploading_dialog">
		<div>
			<strong>Uploading...</strong>
		</div>
		<img src="sections/entry-page-builder/entry/ajax-loader.gif" />
	</div>
	<div id="image_link_form" title="Edit Image Properties" >
		<form>
			<fieldset>
				<ul>
					<li>
						<label>Link: </label>
						<select id="linkType">
							<option>My Site</option>
							<option>My Site (full-screen)</option>
							<option>My Site (HTML Mirror)</option>
							<option>Other URL</option>
						</select>
					</li>
					<li id="other_url_textbox">
						<input id="image_other_url_input" type="text" value="http://" size="30" />
					</li>
					<li>
						<br />
						<label for="image_title">Image description: </label>
						<input id="image_description_input" name="image_title" type="text" size="30" />
					</li>
				</ul>
			</fieldset>
		</form>
	</div>
	<form id="previewForm" name="previewForm" action="sections/entry-page-builder/preview.php" method="post" target="_blank">
		<input id="previewFont" name="fontVal" type="hidden" />
		<input id="previewBackgroundColor" name="backgroundColorVal" type="hidden" />
		<input id="previewTextColor" name="textColorVal" type="hidden" />
		<input id="previewLinkColor" name="linkColorVal" type="hidden" />
		<input id="previewSource" name="previewSource" type="hidden" />
		<input id="previewTitle" name="previewTitle" type="hidden" />
		<input id="previewDescription" name="previewDescription" type="hidden" />
		<input id="previewKeywords" name="previewKeywords" type="hidden" />
	</form>
	<form id="saveToZipForm" name="saveToZipForm" action="saveToZip.php" method="post" target="_blank"> 
		<input id="saveToZipFont" name="fontVal" type="hidden" />
		<input id="saveToZipBackgroundColor" name="backgroundColorVal" type="hidden" />
		<input id="saveToZipTextColor" name="textColorVal" type="hidden" />
		<input id="saveToZipLinkColor" name="linkColorVal" type="hidden" />
		<input id="saveToZipSource" name="saveToZipSource" type="hidden" />
		<input id="saveToZipTitle" name="saveToZipTitle" type="hidden" />
		<input id="saveToZipDescription" name="saveToZipDescription" type="hidden" />
		<input id="saveToZipKeywords" name="saveToZipKeywords" type="hidden" />
	</form>
	<div id="save_content_dialog" title="Saving Content">
		<div>
			<strong>Saving...</strong>
		</div>
		<img src="sections/entry-page-builder/entry/ajax-loader.gif" />
	</div>
	<div id="save_successful_dialog">
		<div>
			<strong>Save successful.</strong>
		</div>
	</div>
	<div id="load_content_dialog" title="Loading">
		<div>
			<strong>Loading preview...</strong>
		</div>
		<img src="sections/entry-page-builder/entry/ajax-loader.gif" />
	</div>
	<div id="entry_uploadify" title="Upload">
		<div class="entryUploader">
			<input name="entryfiles" type="file" class="entryfiles" id="entryfiles" size="10" /> 
			<div id="status-message"></div>
			<div id="entry-queue"></div>
		</div>
	</div>
</div>


<script type="text/javascript" src="sections/entry-page-builder/entry/jquery.jeditable.mini.js"></script>
<script type="text/javascript" src="sections/entry-page-builder/entry/jquery.form.js"></script>
<script type="text/javascript" src="sections/entry-page-builder/js/jquery.tweet.js"></script>
<script type="text/javascript" src="sections/entry-page-builder/js/jquery.minimefeed.min.js"></script>
<script type="text/javascript" src="sections/entry-page-builder/js/jquery.backgrounder.js"></script>
<script type="text/javascript" src="sections/entry-page-builder/entry/ajaxupload.js"></script>
<script type="text/javascript" src="sections/entry-page-builder/entry/ajaxuploadBackground.js"></script>
<script type="text/javascript" src="sections/entry-page-builder/entryPage.js"></script>

<script type="text/javascript">
// <![CDATA[


         
// ]]>
</script>