// Initially hidden components
$( "div[id$=section_template]" ).hide();
$( "#image_other_url_input" ).parent().hide();
$( "#rss_feed_url" ).parent().hide();
$( "#twitter_handle" ).parent().hide();
$( "#currentBackground" ).parent().hide();


// initialize 	(need to create separate function so initialization can happen every time a new content section is loaded)
function initialize() { 
	
	
	$(".image_label").parent().click(function(event) {
		  return false;
	});
	
	// initialize buttons
	$( ".add_section_button" )
	.click(function(event) { 
		$( "#section_menu" ).dialog("open", {
			position:[$( ".add_section_button" ).offset().left, $( ".add_section_button" ).offset().top]
		});        

		$.log("open section menu now!");

		var thisButton = $(this);

		$( "#section_menu" ).dialog({ 
			buttons : {
				"Add Section" : function() {
					var sectionToClone = "#" + $( "#section_menu input[name=section_type]:checked").val() + "_section_template";

					var clonedSection = $( sectionToClone ).clone(true).removeAttr("id");

					switch ($( "#section_menu input[name=section_type]:checked").val()) {
						case "text":
							$(".entry_page_section_content", clonedSection).addClass("editableTextarea");
							break;
						case "text_image":
						case "image_text":
							//$(".entry_page_section_content .text_by_image", clonedSection).addClass("editableTextarea");
							break;
						case "rss_feed":
							$(".rss_feed", clonedSection).minimeFeed($( "#section_menu input[name=rss_feed_url]").val()).bind("loaded", function() {
								$("#entryContent").css("color", "#" + $('#text_color').val());
								$("#entryContent a:link").css("color", "#" + $('#link_color').val());
								$("#entryContent").css("font-family", $('#font_selector').val());
							});						
							$(".rss_feed", clonedSection).attr("title", $("#section_menu input[name=rss_feed_url]").val());
							break;
						case "twitter":
							$(".twitter", clonedSection).tweet({
								avatar_size : 40,
								count : 5,
								username : [$( "#section_menu input[name=twitter_handle]").val()],								
								loading_text : "loading..."
							}).bind("loaded", function() {
								$("#entryContent").css("color", "#" + $('#text_color').val());
								$("#entryContent a:link").css("color", "#" + $('#link_color').val());
								$("#entryContent").css("font-family", $('#font_selector').val());
							});							
							$(".twitter", clonedSection).attr("title", [$( "#section_menu input[name=twitter_handle]").val()]);
							break;
					}

					$( ".add_section_button", clonedSection)
					.button({
						icons : {
							primary : "ui-icon-plusthick",
							secondary : "ui-icon-circle-arrow-n"
						}
					});
					$( ".delete_section_button", clonedSection)
					.button({
						icons : {
							primary : "ui-icon-closethick"
						}
					});
					
					$('.editable', clonedSection).editable('sections/entry-page-builder/entry/text-save.php', {
						onblur : 'submit'
					}); 
					$('.editableTextarea', clonedSection).editable('sections/entry-page-builder/entry/text-save.php', {
						type : 'textarea',
						onblur : 'submit'
					});

					if(thisButton.attr("id") == "initial_add_section_button")
						thisButton.parent().prev().children('#entry_page_sections').append(clonedSection.fadeIn());
					else
						thisButton.parent().parent().parent().before(clonedSection.fadeIn());
					
					$("#entryContent").css("background-color", "#" + $('#background_color').val());
					$("#entryContent").css("color", "#" + $('#text_color').val());
					$("#entryContent a:link").css("color", "#" + $('#link_color').val());
					$("#entryContent").css("font-family", $('#font_selector').val());

					initialize();
                    
					settingsChanged = true;
					$( this ).dialog("close");
				},
				Cancel : function() {
					$( this ).dialog("close");
				}
			}
		});
	});
	 
	
	$('.editable').editable('sections/entry-page-builder/entry/text-save.php', {
		onblur : 'submit'
	});
	
	$('.editableTextarea').editable('sections/entry-page-builder/entry/text-save.php', {
		type : 'textarea',
		onblur : 'submit'
	});
	

	$( ".delete_section_button" )
	.unbind('click')
	.click(function() {
		var thisButton = $(this);
		if(confirm('Are you sure you want to delete this section?'))
			thisButton.parent().parent().parent().slideUp(function() {
				$(this).remove();
			});
			settingsChanged = true;
	});
	$( ".entry_page_section_buttons button" )
	.mouseover( function() {
	this.style.opacity=1;
	this.filters.alpha.opacity=100;
	})
	.mouseout(function() {
		this.style.opacity = 0.7;
		this.filters.alpha.opacity = 70;
	});
	$( ".move_section_handle" )
	.mouseover( function() {
	this.style.opacity=1;
	this.filters.alpha.opacity=100;
	})
	.mouseout(function() {
		this.style.opacity = 0.5;
		this.filters.alpha.opacity = 50;
	});
	// initialize sortables
	$( "#entry_page_sections" ).sortable({
		revert : true,
		/*axis : "y",*/
		distance : 30,
		opacity : 0.7,
		handle : '.move_section_handle',
		update: function(event, ui) { 
			settingsChanged = true;
		}
	});	
	
	$( ".image_link_button" ).click(function() {
		var thisButton = $(this);
		$( "#image_other_url_input" ).attr("value", thisButton.prev().attr("href"));
		$( "#image_description_input" ).attr("value", thisButton.parent().find("img.uploadable_image").attr("alt"));

        switch(thisButton.parent().find("a").attr("href")) {
	    	case "main.php":
				$("#linkType", "My Site").attr("selected","selected");
				break;
			case "javascript:openwindow('main.php')": 
				$("#linkType", "My Site (full-screen)").attr("selected","selected");
				break;
			case "html": 
				$("#linkType", "My Site (HTML Mirror)").attr("selected","selected");
				break;
			default :
				$("#linkType", "Other URL").attr("selected","selected");
		}

		$( "#image_link_form" ).dialog("open");

		$( "#image_link_form" ).dialog({
			buttons : {
				"OK" : function() {
					switch ($("option:selected", $(this)).text()) {
						case "Other URL":
							var linkVal = $( "#image_other_url_input" ).val();
							break;
						case "My Site (HTML Mirror)": 
							var linkVal = "html/";
							break;
						case "My Site (full-screen)": 
							var linkVal = "javascript:openwindow('main.php')";
							break;
						case "My Site": 
							var linkVal = "main.php";
							break;
					}
                    //thisButton.prev().attr("href", linkVal);
					thisButton.parent().find("a").attr("href", linkVal); // updates image link and text link
					//thisButton.parent().children().children("img").prev().attr("href", linkVal);
					thisButton.parent().find("img.uploadable_image").attr("alt", $( "#image_description_input" ).val());

					$( this ).dialog("close");
				},
				"Cancel" : function() {
					$( this ).dialog("close");
				}
			}
		});
	}); 
	
	$(".uploadable_image").unbind("click");
	$(".uploadable_image").click(function() { 
		curUploadingImg = $(this); 
	 	$("#entry_uploadify").dialog("open");
	});
	
	$("#entryContent").css("background-color", "#" + $('#background_color').val());
	$("#entryContent").css("color", "#" + $('#text_color').val());
	$("#entryContent a:link").css("color", "#" + $('#link_color').val());
	$("#entryContent").css("font-family", $('#font_selector').val());
	
	$(".entry_page_section").find("a").each(function() {
		$(this).attr("onclick", "event.preventDefault();")
	}); 
	
	$("#entryContent").css("backgroundColor", $('#background_color').val());
	$('#background_color_swatch').css("backgroundColor", $('#background_color').val());
	$('#background_color_swatch').each(function() {
		//
		var theSwatch = $(this);
		var theInput = $(this).parent().find("input");
		//                   
		$(this).ColorPicker({
			color: theInput.val(),
			onShow: function (colpkr) {
				$(colpkr).fadeIn('fast');
				return false;
			},
			onHide: function (colpkr) {
				$(colpkr).fadeOut('fast');
				return false;
			},
			onChange: function (hsb, hex, rgb) {
				theInput.val("#" + hex);
				theSwatch.css('backgroundColor', '#' + hex);
				settingsChanged = true;
				$("#entryContent").css("backgroundColor", '#' + hex);
			}
		});
	});
	
	$("#entryContent").css("color", $('#text_color').val());
	$('#text_color_swatch').css("backgroundColor", $('#text_color').val());
	$('#text_color_swatch').each(function() {
		//
		var theSwatch = $(this);
		var theInput = $(this).parent().find("input");
		//                   
		$(this).ColorPicker({
			color: theInput.val(),
			onShow: function (colpkr) {
				$(colpkr).fadeIn('fast');
				return false;
			},
			onHide: function (colpkr) {
				$(colpkr).fadeOut('fast');
				return false;
			},
			onChange: function (hsb, hex, rgb) {
				theInput.val("#" + hex);
				theSwatch.css('backgroundColor', '#' + hex);
				settingsChanged = true;
				$("#entryContent").css("color", '#' + hex);
			}
		});
	});
	
	$("#entryContent a, #entryContent a:hover").css("color", $('#link_color').val());
	$('#link_color_swatch').css("backgroundColor", $('#link_color').val());
	$('#link_color_swatch').each(function() {
		//
		var theSwatch = $(this);
		var theInput = $(this).parent().find("input");
		//                   
		$(this).ColorPicker({
			color: theInput.val(),
			onShow: function (colpkr) {
				$(colpkr).fadeIn('fast');
				return false;
			},
			onHide: function (colpkr) {
				$(colpkr).fadeOut('fast');
				return false;
			},
			onChange: function (hsb, hex, rgb) {
				theInput.val("#" + hex);
				theSwatch.css('backgroundColor', '#' + hex);
				settingsChanged = true;
				$("#entryContent a, #entryContent a:hover").css("color", '#' + hex);
			}
		});
	});
	
	spaceImages();
}

// Load and intialize initial content
loadEntryPage($('#page_selector'));

// Button initializations that only need to happen once
$('#initial_add_section_button')
.button({
	icons : {
		primary : "ui-icon-plusthick",
		secondary : "ui-icon-circle-arrow-n"
	}
});

$( ".save_button" )
.button({
	icons : {
		primary : "ui-icon-disk"
	}
});

$( "#previewButton" )
.button({
	icons : {
		primary : "ui-icon-search"
	}
});

$( "#saveToZipButton" )
.button({
	icons : {
		primary : "ui-icon-disk"
	}
});

// Upload One Image Dialog
$( ".image_upload_form" ).dialog({
	autoOpen : false,
	modal : true,
	resizable : false,
	height : 100
}); 

// Entry image uploadify
$("#entry_uploadify").dialog({
	autoOpen : false,
	modal : true,
	resizable : false,
	height: 200,
	width: 400   
});

// Selection Menu Dialog
$( "#section_menu" ).dialog({
	autoOpen : false,
	modal : true,
	resizable : false
});

// Image Link Dialog
$( "#image_link_form" ).dialog({
	autoOpen : false,
	modal : true,
	resizable : false
});

$( "#section_menu input[name=section_type]" ).click(function() {
	if($(this).val() == "rss_feed") {
		$( "#rss_feed_url" ).parent().slideDown();
		$( "#twitter_handle" ).parent().slideUp();
	} else if($(this).val() == "twitter") {
		$( "#rss_feed_url" ).parent().slideUp();
		$( "#twitter_handle" ).parent().slideDown();
	} else {
		$( "#rss_feed_url" ).parent().slideUp();
		$( "#twitter_handle" ).parent().slideUp();
	}
});
$( "select", "#image_link_form" ).change(function() {
	if($("option:selected", $(this)).text() == "Other URL")
		$(this).parent().next().slideDown();
	else
		$(this).parent().next().slideUp();
});
// Colors
/*
$('#background_color').ColorPicker({onSubmit: function(hsb, hex, rgb) { $('#background_color').val(hex); $("#entryContent").css("background-color", "#" + hex);},
onBeforeShow: function () {$(this).ColorPickerSetColor(this.value);}
})
.bind('keyup', function() {$(this).ColorPickerSetColor(this.value);
});
$('#text_color').ColorPicker({onSubmit: function(hsb, hex, rgb) { $('#text_color').val(hex); $("#entryContent").css("color", "#" + hex);},
onBeforeShow: function () {$(this).ColorPickerSetColor(this.value);}
})
.bind('keyup', function() {$(this).ColorPickerSetColor(this.value);
});
$('#link_color').ColorPicker({onSubmit: function(hsb, hex, rgb) { $('#link_color').val(hex); $("#entryContent a:link").css("color", "#" + hex);},
onBeforeShow: function () {$(this).ColorPickerSetColor(this.value);}
})
.bind('keyup', function() {$(this).ColorPickerSetColor(this.value);
}); 
*/
/*
$("#background_color").change(function() {
	 $("#entryContent").css("background-color", $(this).val());
})
*/
// Fonts
$('#font_selector').change(function() {
	$("#entryContent").css("font-family", $("option:selected", $(this)).attr("value"));
});
// RSS Feed
$('.rss_feed').each(function() {
	$(this).minimeFeed($(this).text());
});

// Page Width Slider
$('#page_width_slider')
.slider({
	min : 940,
	max : 1180,
	step : 20,
	value : 940,

	change : function(event, ui) {
		$("#entryMain, .entry_page_section_buttons div").css("width", ui.value);
		$("#page_width").text($('#page_width_slider').slider("value"));
		spaceImages();
	}
});

// Load content Dialog
$( "#load_content_dialog" ).dialog({
	autoOpen : false,
	modal : true,
	resizable : false
});

$('#page_selector').change(function() {
	loadEntryPage($(this));
});
function loadEntryPage(selector) {
	if($(selector).children().size() > 0) {
		$('#load_content_dialog').dialog("open");
		$.log('entry page selected: '+$("option:selected", selector).attr("value"));
		$.ajax({
			url : 'sections/entry-page-builder/entry/load-entry.php',
			data : {
				id : $("option:selected", selector).attr("value")
			},
			type : 'post',
			success : function(output) { 
				//$.log('loaded content: '+output); 
				if (output.indexOf('id="entryContent"') > -1) { 
					$('#entryContent').replaceWith(output);
				} else {
					alert('Sorry, the current entry page is not compatible with the entry page builder.');
					$("#entryWarning").html("<br>WARNING: Saving any changes here will overwrite your current entry page which is not compatible with the entry page builder. To edit your current entry page code, click <a href='#/advanced/source-templates/ENTRY_PAGE'>here</a>.")
				}
				
				$(".entry_page_section_content", "#entryContent").before('<div class="entry_page_section_buttons" ><div><img src="sections/entry-page-builder/entry/move_handle.png" class="move_section_handle" /><button class="delete_section_button">Delete Section</button></div></div>');

				$( ".add_section_button")
					.button({
						icons : {
							primary : "ui-icon-plusthick",
							secondary : "ui-icon-circle-arrow-n"
						}
					});
					
				$( ".delete_section_button")
				.button({
					icons : {
						primary : "ui-icon-closethick"
					}
				});
				

				$("#page_title").val($("#page_title_placeholder").val());
				$("#page_width_slider").slider("value",$("#page_width_placeholder").val());
				$("#meta_description").val($("#meta_description_placeholder").val());
				$("#meta_keywords").val($("#meta_keywords_placeholder").val());
				$("#background_color").val($("#background_color_placeholder").val());
				$("#text_color").val($("#text_color_placeholder").val());
				$("#link_color").val($("#link_color_placeholder").val());
				$("#font_selector").val($("#font_placeholder").val());
				$("#currentBackground").text($("#background_image_placeholder").val());
				$("#backgroundImage").html($("#contentBackground").html());
				
				initialize();	
				
				spaceImages();
				
				$('#load_content_dialog').dialog("close");
			}
		});
	}
	else {
		initialize();
	}
}

$( "#image_uploading_dialog" ).dialog({
	autoOpen : false,
	modal : true,
	resizable : false
});


$( "#save_content_dialog" ).dialog({
	autoOpen : false,
	modal : true,
	resizable : false
});

$( "#save_successful_dialog" ).dialog({
	autoOpen : false,
	modal : true,
	resizable : false,
	buttons : {
		'OK' : function() {
			$(this).dialog("close");
		} 
	}
});

$(window).load(
	function() {
		spaceImages();
	}
)

function saveEntry(callback) {
	
	$("#contentBackground").html($("#backgroundImage").html());

	$("#page_title_placeholder").val($("#page_title").val());
	$("#page_width_placeholder").val($("#page_width_slider").slider("value"));
	$("#meta_description_placeholder").val($("#meta_description").val());
	$("#meta_keywords_placeholder").val($("#meta_keywords").val());
	$("#text_color_placeholder").val($("#text_color").val());
	$("#background_color_placeholder").val($("#background_color").val());
	$("#link_color_placeholder").val($("#link_color").val());
	$("#font_placeholder").val($("#font_selector").val());
	$("#background_image_placeholder").val($("#currentBackground").text());
   
 	var startMode = $("#start_mode_entry").attr("checked") == "checked"? "ENTRY" : "FLASH";
	$.log("startMode: "+startMode);

	$.ajax({
		url : 'sections/entry-page-builder/entry/save-entry.php',
		data : { 
			startMode : startMode,
			fontVal : $("#font_placeholder").val(),
			backgroundColorVal : $("#background_color_placeholder").val(),
			textColorVal : $("#text_color_placeholder").val(),
			linkColorVal : $("#link_color_placeholder").val(),
			source : $("#entryContent").html(),
			title : $("#page_title").val(),
			description : $("#meta_description").val(),
			keywords : $("#meta_keywords").val()
		},
		type : 'post',
		success : function(output) { 
			$.log('save: '+output);
			$('#page_selector').children(":first").after(output);
			$('#page_selector').val('ENTRY_PAGE');
			$("#save_content_dialog").dialog("close");
			
			if($("#page_selector_section").html().trim().length === 0) {
				$("#page_selector_section").html('Select version: <select id="page_selector"><option value="ENTRY_PAGE" >Current Entry Page</option></select>');
				$('#page_selector').change(function() {
					loadEntryPage($(this));
				});
			}
			
			settingsChanged = false; 
			if (callback) {callback();}
			hideSave();
		}
	});
};

function preview() {
	$("#contentBackground").html($("#backgroundImage").html());
	
	$("#page_title_placeholder").val($("#page_title").val());
	$("#page_width_placeholder").val($("#page_width_slider").slider("value"));
	$("#meta_description_placeholder").val($("#meta_description").val());
	$("#meta_keywords_placeholder").val($("#meta_keywords").val());
	$("#background_color_placeholder").val($("#background_color").val());
	$("#text_color_placeholder").val($("#text_color").val());
	$("#link_color_placeholder").val($("#link_color").val());
	$("#font_placeholder").val($("#font_selector").val()); 
	
	$("#previewFont").val($("#font_placeholder").val());
	$("#previewBackgroundColor").val($("#background_color").val());
	$("#previewTextColor").val($("#text_color").val());
	$("#previewLinkColor").val($("#link_color").val());
	
	$("#previewSource").val($("#entryContent").html());
	$("#previewTitle").val($("#page_title").val());
	$("#previewDescription").val($("#meta_description").val());
	$("#previewKeywords").val($("#meta_keywords").val());
	
	if ($.browser.safari) {
    	document.forms["previewForm"].action += '?t=' + new Date().getTime();
    }
	$("#previewForm").submit();	
};

function saveToZip() {
	$("#contentBackground").html($("#backgroundImage").html());
	
	$("#page_title_placeholder").val($("#page_title").val());
	$("#page_width_placeholder").val($("#page_width_slider").slider("value"));
	$("#meta_description_placeholder").val($("#meta_description").val());
	$("#meta_keywords_placeholder").val($("#meta_keywords").val());
	$("#background_color_placeholder").val($("#background_color").val());
	$("#text_color_placeholder").val($("#text_color").val());
	$("#link_color_placeholder").val($("#link_color").val());
	$("#font_placeholder").val($("#font_selector").val());
	
	$("#saveToZipSource").val($("#entryContent").html());
	$("#saveToZipTitle").val($("#page_title").val());
	$("#saveToZipDescription").val($("#meta_description").val());
	$("#saveToZipKeywords").val($("#meta_keywords").val());
	
	if ($.browser.safari) {
    	document.forms["saveToZipForm"].action += '?t=' + new Date().getTime();
    }
	$("#saveToZipForm").submit();	
};

function oneImageUpload(thisCurrentImage) {
	window.currentImage = thisCurrentImage;
	//$( "#one_image_upload_form" ).dialog("open"); 
	$("#entry_uploadify").dialog("open");
};

function twoImageUpload(thisCurrentImage) {
	window.currentImage = thisCurrentImage; 
	//$( "#two_image_upload_form" ).dialog("open"); 
	$("#entry_uploadify").dialog("open");
};

function threeImageUpload(thisCurrentImage) {
	window.currentImage = thisCurrentImage;
	//$( "#three_image_upload_form" ).dialog("open");
	$("#entry_uploadify").dialog("open");
};

function fourImageUpload(thisCurrentImage) {
	window.currentImage = thisCurrentImage;
	//$( "#four_image_upload_form" ).dialog("open");
	$("#entry_uploadify").dialog("open");
};

function uploadFinished() {
	var oldInput = document.getElementById("filename");     
    var newInput = document.createElement("input"); 
     
    newInput.type = "file"; 
    newInput.id = oldInput.id; 
    newInput.name = oldInput.name;
    newInput.onchange = oldInput.onchange;
    
    oldInput.parentNode.replaceChild(newInput, oldInput); 	

	spaceImages();
	$( '#image_uploading_dialog' ).dialog( 'close' );
};

function spaceImages() { 
   /*
	var contentWidth = $("#entryContent").width();
	
	$(".two_images:not(#two_images_section_template), .three_images:not(#three_images_section_template), .four_images:not(#four_images_section_template)").each(
		function() {
			var totalImagesWidth = 0;
			
			$(".entry_page_section_content span div img", $(this)).each (
				function() {
					totalImagesWidth += $(this).width();
				}
			)
			var spacerWidth = Math.floor((contentWidth - totalImagesWidth) / ($(".entry_page_section_content span div img", $(this)).length + 1));			
			
			$(".imageSpacer", $(this)).css("width", spacerWidth);
		}
	) 
   */ 
}

function setBackgroundDisplayName() {	
	var text = $('#filename').val();
	var splitText = text.split("/");
	if (splitText.length > 1) {
		$('#currentBackground').text(splitText[splitText.length - 1]);
		$('#currentBackground').parent().show();
		return;
	}
	
	splitText = text.split("\\");
	if (splitText.length > 1) {
		$('#currentBackground').text(splitText[splitText.length - 1]);
		$('#currentBackground').parent().show();
		return;
	}
	
	$('#currentBackground').text(text);
}

function clearBackgroundImage() {
/*	if(event.preventDefault)
		event.preventDefault();
	else
		event.returnValue = false;
*/	
	$('#currentBackground').parent().hide();
	$('#backgroundImage').html("");
}

function toggleStyleEditor() {
	if(event.preventDefault)
		event.preventDefault();
	else
		event.returnValue = false;
	
	$('#edit > div').slideToggle();
}

var curUploadingImg;

//
// entry image upload 
if ($(".entryfiles").length == 1) {
$(".entryfiles").uploadify({
  'uploader'       : 'js/plugins/uploadify-v2.1.4/uploadifyCS3.swf',
  'script'         : adminURL + '_bfadmin/file_upload.php', 
  'scriptData'     : {'u':u,'p':p,'mode':'logo'},      
  'method'         : 'post',
  'cancelImg'      : 'js/plugins/uploadify-v2.1.4/cancel.png',
  'multi'          : true,
  'auto'           : true,
  'fileExt'        : '*.jpg;*.jpeg;*.png;',
  'fileDesc'       : 'Image Files (.JPG, .JPEG, .PNG)',
  'queueID'        : 'entry-queue', 
  'buttonText'     : 'UPLOAD IMAGE',
  'width'          : 140,
  'queueSizeLimit' : 100,
  'simUploadLimit' : 1,
  'removeCompleted': true,
  'onSelectOnce'   : function(event,data) {
      	//$('#status-message').text(data.filesSelected + 'uploading...');
		$('#status-message').text('uploading...');
    }, 
  'onComplete'     : function(event, ID, fileObj, response, data) { 
		var fileParts = getNameAndExtension(fileObj['name']); 
		var imageFile = cleanfile(fileParts[0])+'.'+fileParts[1];
	  	$.log('fileName: '+cleanfile(fileParts[0])+'.'+fileParts[1]);
	  	$.log('response: '+response); 
		//
		//addImageToGallery(imageFile); 
		var d = new Date();
		var imgsrc = tempURL+"images/"+imageFile+"?time="+d.getTime();
		curUploadingImg.attr("src", imgsrc);
		//
   	},
  'onAllComplete'  : function(event,data) {
		$.log("filesUploaded: "+data.filesUploaded);
		$.log("data.errors: "+data.errors);
		if (data.errors > 0) {
      		$('#status-message').text("There was an error with the upload.");
		} else {
			$('#status-message').text("");
		}
    }
});
}

//
// entry background image upload 
$(".entrybackgroundfile").uploadify({
  'uploader'       : 'js/plugins/uploadify-v2.1.4/uploadifyCS3.swf',
  'script'         : adminURL + '_bfadmin/file_upload.php', 
  'scriptData'     : {'u':u,'p':p,'mode':'logo'},      
  'method'         : 'post',
  'cancelImg'      : 'js/plugins/uploadify-v2.1.4/cancel.png',
  'multi'          : true,
  'auto'           : true,
  'fileExt'        : '*.jpg;*.jpeg;*.png;',
  'fileDesc'       : 'Image Files (.JPG, .JPEG, .PNG)',
  'queueID'        : 'background-entry-queue', 
  'buttonText'     : 'UPLOAD IMAGE',
  'width'          : 135,
  'queueSizeLimit' : 100,
  'simUploadLimit' : 1,
  'removeCompleted': true,
  'onSelectOnce'   : function(event,data) {
      	//$('#status-message').text(data.filesSelected + 'uploading...');
		$('#background-status-message').text('uploading...');
    }, 
  'onComplete'     : function(event, ID, fileObj, response, data) { 
		var fileParts = getNameAndExtension(fileObj['name']); 
		var imageFile = cleanfile(fileParts[0])+'.'+fileParts[1];
	  	$.log('fileName: '+cleanfile(fileParts[0])+'.'+fileParts[1]);
	  	$.log('response: '+response); 
		//
		//addImageToGallery(imageFile);
		var backgroundImageHTML = "";
		backgroundImageHTML += "<img src='"+tempURL+"images/"+imageFile+"' />";
		$("#backgroundImage").html(backgroundImageHTML);
		curUploadingImg.attr("src", imgsrc); 
		$("#background_image_placeholder").val(tempURL+"images/"+imageFile);
		//
   	},
  'onAllComplete'  : function(event,data) {
		$.log("filesUploaded: "+data.filesUploaded);
		$.log("data.errors: "+data.errors);
		if (data.errors > 0) {
      		$('#background-status-message').text("There was an error with the upload.");
		} else {
			$('#background-status-message').text("");
		}
    }
}); 
/*
$("#clearAll").button({
	icons : {
		primary : "ui-icon-closethick"
	}
});
function clearAllEntryContent() {
	if(confirm('Are you sure you want to delete all current entry content?'))
		$("#entryContent").html("");
}
*/