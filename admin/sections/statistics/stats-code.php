<?
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");
//
// Settings
$s = get_settings();

?>

<div class="contentHeader">
	<h1>Statistics Code -</h1>  <a href="https://vimeo.com/146453430" target="_blank"><img src="img/watch-this-video2.png" border="0" style="position:absolute; margin-left:260px; margin-top:-50px;" /></a>
</div>

<div class="tip_message">
	You can install statistics code from <a href="http://www.google.com/analytics" target="_blank">Google Analytics</a>, <a href="http://www.statcounter.com/" target="_blank">StatCounter</a> or 
		other JavaScript-based services. 
		Simply paste the code below and save.
</div>
<div id="editPane">
	<ul>
		<li>
			<label>Stats Code</label>
			<textarea name="statscode" class="bfChangeListen" id="statscode"><?= $s['stats_code']?></textarea>
		</li>
	</ul>
</div> 
<script type="text/javascript">
// <![CDATA[

// ]]>
</script>
