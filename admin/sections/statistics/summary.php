<?
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");
//
// Settings
$s = get_settings();
// Get various stats
$today = get_visits_by_day(date('Y').'-'.date('m').'-'.date('d'));
$allcount = count_all_visits();
$recentCount = 20;
if ($allcount < $recentCount) {
	$recentCount = $allcount;
}
$recent = get_all_visits($recentCount);
$m = date('m');
$y = date('Y');

//
//$month = get_visits_by_month($m, $y);
$month_count = get_num_visits_by_month($m, $y);

$m--;
if ($m<1){
	$m =12;
	$y--;
}
//$last_month = get_visits_by_month($m,$y);
$last_month_count = get_num_visits_by_month($m, $y);

$searches = get_search_queries();
$refs = get_referrer_visits();

?>

<div class="contentHeader">
	<h1>Statistics Summary -</h1>  <a href="https://vimeo.com/146453430" target="_blank"><img src="img/watch-this-video2.png" border="0" style="position:absolute; margin-left:320px; margin-top:-50px;" /></a>
</div>

<div class="tip_message">These are basic statistics for your site. For robust website statistics, we recommend 
	<a href="http://google.com/analytics" target="_blank">Google Analytics</a> or <a href="http://haveamint.com" target="_blank">Mint</a>.</div>
<div id="editPane">
	<ul>
	   
	<table border="0" class="stats">
		<tr><th class="vert" width='120'>Visits today	 </th><td><?= count($today)?></td>      </tr>
		<tr><th class="vert" width='120'>Visits this month</th><td><?= $month_count?></td>      </tr>
		<tr><th class="vert" width='120'>Visits last month</th><td><?= $last_month_count?></td> </tr>
		<tr><th class="vert" width='120'>Total visits	 </th><td><?= $allcount?></td>          </tr>
	</table>
	
	<li class="divider"></li>
	
	<h2>Recent Visitors</h2>
	<p><em>Here are details of the last <?=count($recent)?> visitors to your site</em></p>
	<table border="0" class="stats">
		<tr>
			<th width='120'>Date</th>
			<th width='120'>IP Address</th>
			<th width='120'>Browser / Platform</th>
			<th width='287'>Referrer</th>
		</tr>
		<? foreach ($recent as $v) { ?>
		<tr><td><?=$v['date_string']?></td>
		<td><?=$v['ip']?></td>
		<td><a class='bfToolTip' title='<?=$v['raw_user_agent']?>'><?=ucfirst($v['browser'])?></a> / <?=ucfirst($v['platform'])?></td>
		<td>
			<a href='<?=$v['referrer']?>' target='_blank' <?if(strlen($v['referrer']) > 40) {?>class='bfToolTip' title="<?=$v['referrer']?>"<?}?>>
			<?=substr($v['referrer'],0,40)?>
			<?if(strlen($v['referrer']) > 40) echo '&#0133;';?>
			</a>
		</td>
		</tr>
		<? } ?>
	</table> 
	
	<li class="divider"></li> 
	
	<h2>Recent Referrers</h2>
	<p><em>Visitors recently reached your site from a link on the following pages.</em></p>
	<table border="0" class="stats">
		<tr>
			<th>Date</th>
			<th>Referrer</th>
		</tr>
		<? foreach ($refs as $v) { ?>
		<tr>
			<td><?=$v['date_string']?></td>
			<td>
				<a href='<?=$v['referrer']?>' target='_blank' <?if(strlen($v['referrer']) > 40) {?>class='bfToolTip' title="<?=$v['referrer']?>"<?}?>>
				<?=substr($v['referrer'],0,40)?>
				<?if(strlen($v['referrer']) > 40) echo '&#0133;';?>
				</a>
		   	</td>
		</tr>
		<? } ?>
	</table>
	
	<li class="divider"></li>
	
	<h2>Recent Search Queries</h2>
	<p><em>Visitors recently found your site using the sites and search queries listed below.</em></p>
	<table border="0" class="stats">
		<tr>
			<th>Date</th>
			<th>Search term</th>
			<th>Referrer</th>
		</tr>
		<? foreach ($searches as $v) { ?>
		<tr>
			<td><?=$v['date_string']?></td>
			<td><?= urldecode($v['search_string'])?></td>
			<td><abbr title="<?=$v['referrer'] ?>"><a href="<?=$v['raw_referrer'] ?>" target="_blank"><?=substr($v['referrer'],0,20) ?></abbr><?if(strlen($v['referrer']) > 20) echo '&#0133;';?></a></td>
		<? } ?>
	</table>
	
	</ul>
	
</div> 
<script type="text/javascript">
// <![CDATA[

// ]]>
</script>
