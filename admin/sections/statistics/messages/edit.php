<?
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../../inc/db.inc.php");
include_once("../../../inc/functions.inc.php");
//
// Settings
$s = get_settings();

///////////////////////////////////////
/////////  Message Vars  //////////////
///////////////////////////////////////
//
// Get messages
$messageallcount = count_all_messages();
if (isset($_GET['val'])) {
	$start = $_GET['val'];
} else {
	$start = 0;
}
//
// number of messages to get from database
$recentCount = 20;
if ($messageallcount < $recentCount) {
	$recentCount = $messageallcount;
}
//
// get messages
$messages = get_form_submissions($start, $recentCount);
$cellCount = 0;
//
// make previous and next page link variables
$prev = $start - 20;
if ($prev < 0) {
	$prev = 0;
}
$next = $start + 20;
//
// establish how many pages of message there are
$curPage = floor($start / 20);
$numPages = ceil($messageallcount / 20);
$pagesPerScreen = 20;
if ($pagesPerScreen > $numPages) {
	$pagesPerScreen = $numPages;
}
if ($curPage <= 10) {
	$startPageLink = 0;
} else if ($curPage >= $numPages - 10) {
	$startPageLink = $numPages - 20;
} else {
	$startPageLink = $curPage - 10;
}
$pageCount = 0;

?>

<div class="contentHeader">
	<h1>Contact Form Messages -</h1>  <a href="https://vimeo.com/146453430" target="_blank"><img src="img/watch-this-video2.png" border="0" style="position:absolute; margin-left:400px; margin-top:-50px;" /></a>
</div>

<div class="tip_message">
	Here are details for messages <?=$start + 1?> to <?=count($messages) + $start?> of <?=$messageallcount?> total submitted message(s).
</div>
<div id="editPane">
	<ul>
		<? if ($numPages > 0) { ?>
			<li class="messagesTabulation">
			Pages:
			<? while ($pageCount<$pagesPerScreen) {
				if ($curPage == $startPageLink) { ?>
					&nbsp;<?= $startPageLink+1 ?>&nbsp;
				<? } else { ?>
					&nbsp;<a href="#/statistics/messages/<?= ($startPageLink * 20) ?>"><?= $startPageLink+1 ?></a>&nbsp;
				<? } ?>
			<? $pageCount++;
			   $startPageLink++;
			} ?>
			</li>
			<li class="messagesNav">
			<? if ($start > 0) { ?>
				<a href="#/statistics/messages/<?=$prev?>">< Previous 20</a>&nbsp;&nbsp;&nbsp;		
			<? } if ($messageallcount > $next) { ?>
				<a href="#/statistics/messages/<?=$next?>">Next 20 ></a>
			<? } ?>
			</li>
			<div style="clear:both;"></div>
		<? } ?>
		<table border="0" class="stats">
			<tr>
				<th width="120">Date</th>
				<th >Message</th>
			</tr>
			<? foreach ($messages as $m) { 
				if ($cellCount == 1) {
					$cellCount = 0; ?>
				<tr style="border:1px solid #B3B3B3">
				<td style="background-color:#F7F7F7"><?=$m['date_string']?></td>
				<td style="background-color:#F7F7F7"><?=str_replace(array("\n\r","\r","\n"), "<br>", $m['body'])?></td></tr>
			<? } else { 
					$cellCount = 1;
				?>
				<tr>
				<td><?=$m['date_string']?></td>
				<td><?=str_replace(array("\n\r", "\r","\n"), "<br />", $m['body'])?></td></tr>
			<? } ?>
			<? } ?>
		</table>
		<? if ($numPages > 0) { ?>
			<li> 
			<li class="messagesTabulation">
			Pages:
			<? 
			$pageCount = 0;
			$startPageLink = 0;
			 while ($pageCount<$pagesPerScreen) {
				if ($curPage == $startPageLink) { ?>
					&nbsp;<?= $startPageLink+1 ?>&nbsp;
				<? } else { ?>
					&nbsp;<a href="#/statistics/messages/<?= ($startPageLink * 20) ?>"><?= $startPageLink+1 ?></a>&nbsp;
				<? } ?>
			<? $pageCount++;
			   $startPageLink++;
			} ?>
			</li>
			<li class="messagesNav">
			<? if ($start > 0) { ?>
				<a href="#/statistics/messages/<?=$prev?>">< Previous 20</a>&nbsp;&nbsp;&nbsp;		
			<? } if ($messageallcount > $next) { ?>
				<a href="#/statistics/messages/<?=$next?>">Next 20 ></a>
			<? } ?>
			</li>
		<? } ?>
	</ul>
</div> 
<script type="text/javascript">
// <![CDATA[

// ]]>
</script>
