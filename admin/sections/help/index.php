<?


?>

<div class="contentHeader">
	<h1>Get Help</h1>
</div>
<div class="tip_message">
	We have several different ways to get the support that you need. Choose the one that works the best for you.
</div>
<!--
<h2><a href="http://s3.amazonaws.com/bf_videos/admin_tutorial_v1.mov">Watch the Video</a></h2>
<p>We've prepared a short video to help you get acquainted with our web-based control panel. You can also <a href="http://s3.amazonaws.com/bf_videos/admin_tutorial_v1.mov">download the video for your iPod/iPhone</a>. </p>
--> 
<div id="help">
<ul>
	<li class="divider"></li>
	<li>
		<h2><a href="http://bigfolio.com/help/">Visit Our Support Wiki</a></h2>
		<p>Many common questions can be answered by <a href="http://bigfolio.com/help/">visiting our support Wiki</a>. </p> 
	</li>
    <li class="divider"></li>
	<li>
		<h2><a href="http://support.bigfolio.com/tickets/new">Submit a Support Ticket</a></h2>
		<p>Support tickets are routed to multiple staff members, ensuring you get the quickest response possible. Support ticket updates are emailed to you and can be reviewed in a browser.</p>
		<a href="http://bigfolio.com/help/">Submit a Support Ticket &rarr;</a>
	</li> 
	<li class="divider"></li>
	<li>
		<h2><a href="http://www.bigfolio.com/contact">Phone Support</a></h2>
		<p>Telephone support is available during normal business hours (Pacific Time). </p>
		<a href="http://www.bigfolio.com/contact">Contact Us &rarr;</a> 
	</li> 
	<li class="divider"></li>
</ul> 
</div>

<script type="text/javascript">
// <![CDATA[


// ]]>
</script>
