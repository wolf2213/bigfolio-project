<?
//
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");
//
$cats = get_all_vcategories();
?>
<div class="content_inner" id="galleries">
	<div class="contentHeader">
		<h1>Video Galleries</h1>
	</div>  
	<div class="tip_message">
	    Use this area to create categories and galleries within those galleries. Click the edit link next to each gallery to upload videos to that gallery, or adjust other settings for that gallery.
	</div>  
	<div id="categories">
		<?
		$loop = 0;
		foreach($cats as $c) {
			$gals = get_vgalleries($c['category_id']);
			$agal = count($gals);
			$loop++;
		?>
		<div class="category imageCat" id="cat<?=$c['category_id']?>">
			<div class="deleteButton"></div> 
		    <input type="text" class="bfInputText categoryName bfChangeListen" name="cat-<?=$c['category_id']?>" value="<?=$c['category_name']?>">
			<div class="plusMinusButton minus"></div>
			<a href="javascript:;" class="bfToolTip" title="Click to toggle the anchor feature of this category. If turned on, the first gallery in this category will load when the category is selected. In some cases, all galleries will be hidden as well.">
				<div class="anchor <? if ($c['anchor']=='1') { ?>anchorOn<? } ?>"></div>
			</a>
			
			<div style="clear:both;"></div>
			<div class="galleries"> 
			<? foreach($gals as $g) { ?>
				<div class="gallery imageGal" id="gal<?=$g['gallery_id']?>"> 
					<div class="deleteButton"></div>
					<input type="text" class="bfInputText galleryName bfChangeListen" name="gal-<?=$g['gallery_id']?>" value="<?=$g['gallery_name']?>">
					<div class="editButton">
						<a href="#/videos/video-galleries/<?=$g['gallery_id']?>" class="editLink">
							edit
						</a>
					</div> 
					<div style="clear:both;"></div>
				</div>
			<? } ?>	
			</div>  
			<button class="newGallery">New Gallery</button>
			<div style="clear:both;"></div>
		</div> 
		<? } ?>
	</div> 
	<button id="newCategory">New Category</button>
</div>


<script type="text/javascript">
// <![CDATA[

var numCats = <?=$loop?>;

$("#categories").sortable({
	update: function(event, ui) {  
		var d = new Object();
		d['catList'] = new Array();
		$(this).find(".category").each(function() {
			d['catList'].push($(this).attr("id").substr(3)); // pushes catNNN minus the cat
		}); 
		$.log("d['catList']: "+d['catList']);
		
		$.ajax({
  			type: 'POST',
  			url:'actions/videos/update-video-categories-order.php',
  			data: d
		});
	}
});

$(".galleries").sortable({ 
    update: function(event, ui) { 
		var d = new Object(); 
		d['galList'] = new Array();
		$(this).find(".gallery").each(function() {
			d['galList'].push($(this).attr("id").substr(3)); // pushes galNNN minus the gal
		});
		$.log("d['galList']: "+d['galList']);
		
		$.ajax({
  			type: 'POST',
  			url:'actions/videos/update-video-galleries-order.php',
  			data: d
		}); 
	}
}); 

function galleriesFunctionality() {
	
	//
	// rollovers
	$(".category").unbind("mouseover");
	$(".category").mouseover(function() {
	   	$(this).addClass("itemOver");
	});
	$(".category").unbind("mouseout");
	$(".category").mouseout(function() {
	   	$(this).removeClass("itemOver");
	});
	$(".gallery").unbind("mouseover");
	$(".gallery").mouseover(function() {
	   	$(this).addClass("itemOver"); 
	});  
	$(".gallery").unbind("mouseout");
	$(".gallery").mouseout(function() {
	   	$(this).removeClass("itemOver");
	}); 
	
	//
	// anchor button  
	$(".anchor").unbind("click");
	$(".anchor").click(function() {
	   	if ($(this).hasClass("anchorOn")) {
		    $(this).removeClass("anchorOn"); 
			var anchor = 0;
		} else {
			$(this).addClass("anchorOn"); 
			var anchor = 1;
		}
		var catID = $(this).parent().parent().attr('id').substr(3);
		$.log('id: '+catID);
		//
		var d = new Object();
		d['catID'] = catID;
		d['anchor'] = anchor; 

		$.ajax({
			type: 'POST',
			url:'actions/videos/set-video-category-anchor.php',
			data: d
		});
		//
	});
	
	//
	// delete category button
    $(".category").find(".deleteButton").unbind("click");
    $(".category").find(".deleteButton").click(function() {
		if ($(this).parent().find(".gallery").length > 0) {
			alert("Please remove all galleries associated with this category first.");
		} else {
			var catID = $(this).parent().attr('id').substr(3);
			var d = new Object();
			d['catID'] = catID;
			var categoryDiv = $(this).parent();
			//
			$.ajax({
				type: 'POST',
				url: 'actions/videos/delete-video-category.php',
				data: d
			}).done(function( msg ) {
				$.log("msg: "+msg);
				categoryDiv.fadeOut('fast', function() {
					categoryDiv.remove();
				});
			});
		}
	});
	//
	// delete gallery button
	$(".gallery").find(".deleteButton").unbind("click");
	$(".gallery").find(".deleteButton").click(function() { 
		//
		// check if they really want to delete it
		var save = confirm("Are you sure you want to delete this gallery and its contents? This cannot be undone.");
		if (save) {
	    	var galID = $(this).parent().attr('id').substr(3);
			var d = new Object();
			d['galID'] = galID;
			var galleryDiv = $(this).parent();
			//
			$.ajax({
				type: 'POST',
				url:'actions/videos/delete-video-gallery.php',
				data: d
			}).done(function( msg ) {  
				$.log("msg: "+msg); 
				galleryDiv.fadeOut('fast', function() {
					galleryDiv.remove();
				});
			}); 
		}
	}); 
	
	//
	// show/hide category button (+/-) 
	$(".category").find(".plusMinusButton").unbind("click");
	$(".category").find(".plusMinusButton").click(function() {
	   	if ($(this).hasClass("minus")) {
		    $(this).removeClass("minus");
			$(this).addClass("plus");
			$(this).parent().find(".galleries").slideUp("fast");
		} else {
			$(this).addClass("minus");
			$(this).removeClass("plus");
			$(this).parent().find(".galleries").slideDown("fast");
		} 
	});
	
	//
	// new gallery button
	$(".newGallery").unbind("click");
	$(".newGallery").click(function() { 
		var galDiv = $(this).parent().find(".galleries");
		//
		// check if the div is closed 
		if (!$(this).parent().find(".plusMinusButton").hasClass("minus")) {
			 $(this).parent().find(".plusMinusButton").click();
		}
		var cat_id = $(this).parent().attr('id').substr(3);
		$.log('cat_id: '+cat_id);
		$.ajax({
	       	type: 'POST',
	       	url:'actions/videos/add-video-gallery.php',
	       	data: ({'the_cat_id':cat_id})
	    }).done(function( msg ) {  
			$.log("msg: "+msg);
			//
			// returns SUCCESS:[gallery_ID]
		   	if (msg.substr(0,7) == "SUCCESS") {
				//
				var galID = msg.substr(8); 
				//
				var galhtml = "";
					galhtml += '<div class="gallery imageGal" id="gal'+galID+'">'; 
					galhtml += '<div class="deleteButton"></div>';
					galhtml += '<input type="text" class="bfInputText galleryName" name="gal-'+galID+'" value="Enter gallery name">';
					galhtml += '<div class="editButton">';
					galhtml += '<a href="#/videos/video-galleries/'+galID+'" class="editLink">';
					galhtml += 'edit';
					galhtml += '</a>';
					galhtml += '</div>'; 
					galhtml += '<div style="clear:both;"></div>';
					galhtml += '</div>';
				//
				galDiv.append(galhtml);
				//
				handleUI();  
				galleriesFunctionality();
				//
				numCats++;
		   	} else {
			   	alert('There was an error creating a new gallery. Please try again later. If problems persist, please contact support.');
			}
		});
	});
}
galleriesFunctionality();

$("#newCategory").click(function() {
	$.ajax({
       	type: 'POST',
       	url:'actions/videos/add-video-category.php',
       	data: ({'order':numCats})
    }).done(function( msg ) {
	   	if (msg.substr(0,7) == "SUCCESS") { 
			//
			var catID = msg.substr(8);
			//
			var cathtml = "";
				cathtml += '<div class="category imageCat" id="cat'+catID+'">'; 
				cathtml += '<div class="deleteButton"></div>';
				cathtml += '<input type="text" class="bfInputText categoryName" name="cat-'+catID+'" value="Enter category name">';
                cathtml += '<div class="plusMinusButton minus"></div>';

				cathtml += '<a href="javascript:;" class="bfToolTip" title="Click to toggle the anchor feature of this category. If turned on, the first gallery in this category will load when the category is selected. In some cases, all galleries will be hidden as well.">';
				cathtml += '<div class="anchor"></div>';
				cathtml += '</a>';

				cathtml += '<div style="clear:both;"></div>';
				cathtml += '<div class="galleries">'; 
				cathtml += '</div>'; 
				cathtml += '<button class="newGallery">New Gallery</button>';
				cathtml += '<div style="clear:both;"></div>';
				cathtml += '</div>';
			//
			$("#categories").append(cathtml);
			//
			handleUI(); 
			galleriesFunctionality();
			//
			numCats++;
	   	} else {
		   	alert('There was an error creating a new category. Please try again later. If problems persist, please contact support.');
		}
	});
});



// ]]>
</script>