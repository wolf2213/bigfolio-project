<?
//
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");
//
//substr($_GET['val'], 0, -4)
//
if (!isset($_GET['viewMode']) || $_GET['viewMode'] == "") { 
	if (isset($_SESSION['gallery_view_type'])) {
		$type = $_SESSION['gallery_view_type'];
	} else {
		$type = 'small'; 
		$thumbSize = 'thumb';
	}
} else {
	$type = $_GET['viewMode']; 
}
if ($type == 'large') {
	$thumbSize = 'small';
	$viewLabel = 'Large thumbnails';
} else if ($type == 'small') {
	$thumbSize = 'thumb'; 
	$viewLabel = 'Small thumbnails';
} else if ($type == 'list') {
	$thumbSize = 'medium';
	$viewLabel = 'List';
}

$cats = get_all_vcategories(); 

//
// store view type for future visits
$_SESSION['gallery_view_type'] = $type;

// 
//
// get font files from remote server 
$postArr = array();
$postArr['u'] = $_SESSION['user']['email'];
$postArr['p'] = $_SESSION['user']['password'];
$postArr['mode'] = 'video';
$url = SERVER . '_bfadmin/files_list.php'; // 
$remoteFiles = curl_post($url, $postArr, array()); 
$images = json_decode($remoteFiles); 
//sort($images, SORT_STRING); 
//
?>
<div class="content_inner videoBank">
	<div class="contentHeader">
		<h1>Video Bank</h1>
	</div>  
	<div class="tip_message">
	    This area stores all videos associated with all video galleries. Use the uploader here to add videos to the video bank or overwrite existing videos. Please note that changes to the video bank affects all video galleries; If a video is assigned to one gallery or multiple galleries, any changes to that video (update or delete) will affect all video galleries where that video is used.  
	</div>  
	
	<div class="galleryUploader">
		<input name="galleryfiles" type="file" class="galleryfiles" id="gal<?=$gallery['gallery_id']?>" size="10" /> 
		<div id="status-message"></div>
		<div id="gallery-queue"></div>
	</div>                          
	
	<div style="clear:both;"></div>
	<div class="galleryImages">
		<? foreach($images as $img) { 
			$prettyName = $img;
		   	$html  = "";
         	$html .= '<div class="thumbItem '.$type.'Item" id="'.str_replace(".","",$img).'">';
			$html .= '<div class="deleteButton"></div>';
			$html .= '<div style="clear:both;"></div>';
			$html .= '<div class="thumbFileLabel '.$type.'Label">'.$prettyName.'</div>';
			
			$html .= '<div class="thumbHolder '.$type.'Thumb">';
			$html .= '<a rel="galVideos" href="sections/videos/video-details.php?f='.$_SESSION['user']['temp_url'].'videos/'.$img.'&vf='.$img.'">';
			$html .= '<img src="img/default-video-thumb.jpg" title="'.$img.'"/>';
			$html .= '</a>';
			$html .= '</div>';  
			
			$html .= '<input type="checkbox" class="imgCheck"/>';
			$html .= '</div>'; 
			echo $html; 
		} ?> 
	</div>
	<div style="clear:both;"></div>
</div>

<div class="actionsPanel">
	<div class="icon">
	</div>
	<div class="inner">
		<ul>
			<li> 
				<label for="galleryView">View mode</label>
				<select name="viewMode" class="smallSelect" id="viewMode">
					<option><?=$viewLabel?></option>
					<option>-----------------</option>
					<option value="small">Small Thumbnails</option>
					<option value="large">Large Thumbnails</option>
					<option value="list">List</option>
				</select>
			</li>
			
			<li>
				<label for="bulkActions">Bulk actions</label>
		   		<select name="bulkActions" class="smallSelect" id="bulkActions">
					<option>Select One</option>
					<option>-----------------</option>
					<option value="delete">Delete Selected</option>
				</select> 
			</li>
			
			<li>
				<label for="addTo">Add to</label>
				<select name="addTo" class="smallSelect" id="addTo">
					<option>Select One</option>
					<option>-----------------</option>
					<? foreach($cats as $c) { ?>
						<? $gals = get_vgalleries($c['category_id']); ?>
						<? foreach($gals as $g) { ?>
							<option value="<?=$g['gallery_id'];?>"><?=$c['category_name']?>/<?=$g['gallery_name']?></option>
						<? } ?>
					<? } ?>
				</select>
			</li>
				
			<li>
			   	<label for="selectActions">Select</label>
				<button id="selectAll">All</button>
				<button id="selectNone">None</button>
			</li>
		</ul>
	</div>
</div>

<script type="text/javascript">
// <![CDATA[  

var type = "<?=$type?>";
var thumbSize = "<?=$thumbSize?>";
var galID; 
var lastThumbChecked; 


function galleryFunctionality() { 
	
	$(".thumbItem").find(".deleteButton").unbind("click"); 
	$(".thumbItem").find(".deleteButton").click(function() {
		var save = confirm("Are you sure you want to delete this video from the video bank and any video galleries where it may appear? This cannot be undone.");
		if (save) { 
			removeVideo($(this).parent()); 
		}
	});
	
	<? if ($type == 'list') { ?>
	$(".thumbFileLabel").unbind("click");
	$(".thumbFileLabel").click(function() {
    	$(this).parent().find(".thumbHolder").slideToggle('fast');
	});
	<? } ?>
	
	//
	// handle check box shift functionality
	$(".imgCheck").unbind("click");
	$(".imgCheck").click(function() {
		var parentID = $(this).parent().attr("id");
		var isChecked = $(this).attr("checked"); 
		//
		// check if shift key is down  
		if (!event.shiftKey) {
			lastThumbChecked = $(this).parent();
			return;
		}
		//
		// get index
		var thisIndex = $(".thumbItem").index($(this).parent());
		var lastIndex = $(".thumbItem").index(lastThumbChecked);
		//
		// determine if we should move up the dom or down
		if (thisIndex > lastIndex) {
			var start = lastIndex;
			var end = thisIndex;
		} else {
			var start = thisIndex;
			var end = lastIndex;
		} 
		
		for (var i=start; i<=end; i++) { 
			if (isChecked) {
				$(".thumbItem:eq("+i+")").find(".imgCheck").attr("checked", "checked");
			} else {
				$(".thumbItem:eq("+i+")").find(".imgCheck").removeAttr("checked");
			}
		}
		 
	});
	
}
galleryFunctionality();  

//
// gallery image upload 
$(".galleryfiles").uploadify({
  'uploader'       : 'js/plugins/uploadify-v2.1.4/uploadifyCS3.swf',
  'script'         : adminURL + '_bfadmin/file_upload.php', 
  'scriptData'     : {'u':u,'p':p,'mode':'video'},      
  'method'         : 'post',
  'cancelImg'      : 'js/plugins/uploadify-v2.1.4/cancel.png',
  'multi'          : true,
  'auto'           : true,
  'fileExt'        : '*.mp4;*.mov;*.m4v;*.flv;',
  'fileDesc'       : 'Video Files (.MP4, .MOV, .M4V, .FLV)',
  'queueID'        : 'gallery-queue', 
  'buttonText'     : 'UPLOAD VIDEOS',
  'width'          : 140,
  'queueSizeLimit' : 100,
  'simUploadLimit' : 1,
  'removeCompleted': true,
  'onSelectOnce'   : function(event,data) {
      	//$('#status-message').text(data.filesSelected + 'uploading...');
		$('#status-message').text('uploading...');
    }, 
  'onComplete'     : function(event, ID, fileObj, response, data) { 
		var fileParts = getNameAndExtension(fileObj['name']); 
		var imageFile = cleanfile(fileParts[0])+'.'+fileParts[1];
	  	$.log('fileName: '+cleanfile(fileParts[0])+'.'+fileParts[1]);
	  	$.log('response: '+response); 
		//
		addToVideoBank(imageFile);
		//
   	},
  'onAllComplete'  : function(event,data) {
		$.log("filesUploaded: "+data.filesUploaded);
		$.log("data.errors: "+data.errors);
		if (data.errors > 0) {
      		$('#status-message').text("There was an error with the upload.");
		} else {
			$('#status-message').text("");
		}
    }
});

function addToVideoBank(videoFile) {
	//  
	var thumbID = videoFile.split(".").join(); //videoFile.substr(0, videoFile.length-4);
	$.log("addToVideoBank: "+videoFile+', '+$("#"+thumbID).length);
	if ($("#"+thumbID).length > 0) { 
		//
		$("#"+thumbID).delay(620).fadeTo('slow', .5).fadeTo('slow', 1).fadeTo('slow', .5).fadeTo('slow', 1);
		//
	} else { 
		//
	    var html = "";
		html += '<div class="thumbItem '+type+'Item" id="'+videoFile.substr(0, videoFile.length-4)+'">';
		html += '<div class="deleteButton"></div>';
		html += '<div style="clear:both;"></div>';
		html += '<div class="thumbFileLabel '+type+'Label">'+videoFile+'</div>';
		html += '<div class="thumbHolder '+type+'Thumb">';
		html += '<a rel="galVideos" href="sections/videos/video-details.php?f='+tempURL+'videos/'+videoFile+'&vf='+videoFile+'">';
		html += '<img src="img/default-video-thumb.jpg" title="'+videoFile+'"/>';
		html += '</a>';
		html += '</div>'; 
		html += '<input type="checkbox" class="imgCheck"/>';
		html += '</div>';
		//
		$(".galleryImages").append(html); 
		handleUI();
		galleryFunctionality(); 
	}
	   
}

$(".viewModeButton").click(function() {
   	changeGalleryView($(this).val());
});
$("#viewMode").change(function() {
	changeGalleryView($(this).val());
});

function removeVideo(thumb) { 
	var videoFile = thumb.find("img").attr("title");
   	var imageID = thumb.attr('id').substr(3);
	$.ajax({
      type: 'POST',
      url:'actions/videos/remove-video-from-all-galleries.php',
      data: ({'videoFile':videoFile})
    }).done(function( msg ) {
	  	$.log( "msg: "+msg);
		if (msg.substr(0,7) == 'SUCCESS') {
			thumb.fadeOut("fast", function() {
			   thumb.remove(); 
			});
		
			$.ajax({
	          type: 'POST',
	          url:adminURL + '_bfadmin/file_remove.php',
	          data: ({'u':u,'p':p, 'mode':'video', 'file':videoFile})
	        }).done(function( msg ) {
			  $.log( "msg: " + msg );
			});
			//
		} else {
			alert("There was an error removing the video.");
		}
	});
}                         

function changeGalleryView(type) {
	var v = "#";
    for (var i=0; i<paths.length; i++) { 
	 	if (paths[i] != "") {
			if (i == paths.length - 1) {
				v += "/video-bank-" + type;
			} else {
				v += "/"+paths[i];
			} 
		}
	}
	location.href = v;
}

$("#bulkActions").change(function() { 
	
   	if ($(this).val() == 'delete') {
		//
	    var save = confirm("Are you sure you want to delete these videos from the video bank and any video galleries where they appear? This cannot be undone.");
		if (save) { 
			$(".thumbItem").each(function() {
				if ($(this).find(".imgCheck").attr("checked") == "checked") {
				   removeVideo($(this)); 
				}
			});
		}
		
	}
	
	// reset selection
	$(this).val("Select One");
});


$("#addTo").change(function() {
	$.log("Add to gallery: "+$(this).val()+', '+Number($(this).val()).toString()); 
	if (Number($(this).val()).toString() != "NaN") { 
		var galID = $(this).val(); 
		var count = 0; 
		var total = 0;
		//
		$(".thumbItem").each(function() {
			if ($(this).find(".imgCheck").attr("checked") == "checked") {
				total++; 
				var videoFile = $(this).find("img").attr("title");
				$.log("videoFile: "+videoFile);
			   	$.ajax({
			      type: 'POST',
			      url:'actions/videos/add-video-to-gallery.php',
			      data: ({'galID':galID, 'videoFile':videoFile})
			    }).done(function( msg ) { 
					count++;
                   	if (count == total) { 
 						hideSave();
		            	location.href = "#/videos/video-galleries/"+galID; 
				   	}	
				}); 
			}
		});
		//
		if (total==0) {  
			alert('Please select videos to add.');
		} else {
			showSave();
		}
		// 
	}
});


$("#selectAll").click(function() {
	$(".imgCheck").attr("checked","checked");
});
$("#selectNone").click(function() {
	$(".imgCheck").removeAttr("checked");
});  

$(".actionsPanel").find(".icon").click(function() {
	if ($(".actionsPanel").hasClass("showing")) {
		$(".actionsPanel").removeClass("showing");
		$(".actionsPanel").animate({
			"marginLeft":2
		}, 'fast');
	} else { 
		$(".actionsPanel").addClass("showing");
		$(".actionsPanel").animate({
			"marginLeft":-200
		}, 'fast');
	}
});

$.ajax({
  type: 'POST',
  url:adminURL + '_bfadmin/write_policy_file.php',
  data: ({'u':u,'p':p})
}).done(function( msg ) {
  $.log( "crossdomain?: " + msg );
});


// ]]>
</script>