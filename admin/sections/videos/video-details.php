<?
$fullVideoFile = $_GET['f'];
$videoFile = $_GET['vf'];
$videoID = $_GET['vid'];
?>

<div class="tip_message">
    Video preview: <?=$_GET['vf'];?>
</div>

<div id="videoPreview">
</div>
 
<? if (isset($videoID)) { ?>
<div class="tip_message">
    You can upload a thumbnail for your video here.
</div>

<div class="thumbnailUploader">
	<input name="thumbnailfiles" type="file" class="thumbnailfiles" id="gal<?=$gallery['gallery_id']?>" size="10" /> 
	<div id="status-message"></div>
	<div id="thumbnail-queue"></div>
</div>

<? } ?>

<script type="text/javascript">
// <![CDATA[ 

var file = "<?=$fullVideoFile?>";

var d = Date();

swfobject.embedSWF("video-player/bfVideoPlayer.swf", "videoPreview", "640", "390", '9.0.24', null, {'video_file':file+"?t="}, {'quality':'high','wmode':'opaque','allowScriptAccess':'always','allowFullScreen':'true'},{},function(event) {});


<? if (isset($videoID)) { ?>
	
var video_id = <?=$videoID?>;
$.log('video_id: '+video_id);
var video_file = "<?=$videoFile?>";
//
// gallery image upload  
$(".thumbnailfiles").uploadify({
  'uploader'       : 'js/plugins/uploadify-v2.1.4/uploadifyCS3.swf',
  'script'         : adminURL + '_bfadmin/file_upload.php', 
  'scriptData'     : {'u':u,'p':p,'mode':'gallery'},      
  'method'         : 'post',
  'cancelImg'      : 'js/plugins/uploadify-v2.1.4/cancel.png',
  'multi'          : true,
  'auto'           : true,
  'fileExt'        : '*.jpg;*.jpeg;*.png;',
  'fileDesc'       : 'Image Files (.JPG, .JPEG, .PNG)',
  'queueID'        : 'thumbnail-queue', 
  'buttonText'     : 'UPLOAD THUMBNAIL',
  'width'          : 160,
  'queueSizeLimit' : 1,
  'simUploadLimit' : 1,
  'removeCompleted': true,
  'onSelectOnce'   : function(event,data) {
      	//$('#status-message').text(data.filesSelected + 'uploading...');
		$('#status-message').text('uploading...');
    }, 
  'onComplete'     : function(event, ID, fileObj, response, data) { 
		var fileParts = getNameAndExtension(fileObj['name']); 
		var imageFile = cleanfile(fileParts[0])+'.'+fileParts[1];
	  	$.log('fileName: '+cleanfile(fileParts[0])+'.'+fileParts[1]);
	  	$.log('response: '+response); 
		//
		updateVideoThumbnail(imageFile);
		//
   	},
  'onAllComplete'  : function(event,data) {
		$.log("filesUploaded: "+data.filesUploaded);
		$.log("data.errors: "+data.errors);
		if (data.errors > 0) {
      		$('#status-message').text("There was an error with the upload.");
		} else {
			$('#status-message').text("");
		}
    }
});

function updateVideoThumbnail(imageFile) {
	//
	$.ajax({
      type: 'POST',
      url:'actions/videos/add-thumbnail-to-video.php',
      data: ({'videoID':video_id, 'imageFile':imageFile})
    }).done(function( msg ) {
	  	$.log( "msg: " + msg );
	 	if (msg.substr(0,7) == "SUCCESS") { 
			//
			var vidItem = "#vid"+video_id;
			var d = new Date();
			$(vidItem).find('img').attr('src', tempURL + 'gallery/' + thumbSize + '/' + imageFile + '?t=' + d.getTime());
			$(vidItem).delay(620).fadeTo('slow', .5).fadeTo('slow', 1).fadeTo('slow', .5).fadeTo('slow', 1);
			//
			$.fancybox.close();
			//
		} else {
			alert("There was a problem adding the thumbnail "+imageFile+" to this video. Please try again later. If problems persist, please contact support.")
		}
		
	});
}
<? } ?>

// ]]>
</script>