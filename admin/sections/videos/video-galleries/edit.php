<?
//
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../../inc/db.inc.php");
include_once("../../../inc/functions.inc.php");
//
$galData = explode("-", $_GET['val']);  // separate val
for ($i=0; $i<count($galData); $i++) {
	if (stristr($galData[$i], ".php")) {
		$galData[$i] = str_replace(".php", "", $galData[$i]); // strip .php from all instances
	}
}
//substr($_GET['val'], 0, -4)
//
$gid = $galData[0]; 
if (!isset($galData[1])) { 
	if (isset($_SESSION['gallery_view_type'])) {
		$type = $_SESSION['gallery_view_type'];
	} else {
		$type = 'small'; 
		$thumbSize = 'thumb';
	}
} else {
	$type = $galData[1]; 
}
if ($type == 'large') {
	$thumbSize = 'small';
	$viewLabel = 'Large thumbnails';
} else if ($type == 'small') {
	$thumbSize = 'thumb'; 
	$viewLabel = 'Small thumbnails';
} else if ($type == 'list') {
	$thumbSize = 'medium';
	$viewLabel = 'List';
} 

//
// store view type for future visits
$_SESSION['gallery_view_type'] = $type;

//
$images = get_videos($gid);
$gallery = get_vgallery($gid);
//
function hasProtectedGalleries() {
	return true;
}
//
?>
<div class="content_inner galleryContent">
	<div class="contentHeader">
		<h1>Video Gallery</h1>
	</div>  
	<div class="tip_message">
	    Use this area to add videos, reorder videos, or change other settings associated with this gallery.
	</div>
	<div class="gallerySettings"> 
		<ul>
			<input type="hidden" name="galID" value="<?=$gallery['gallery_id']?>" />
			<li>
				<label for="galleryName">Gallery name</label>
				<input type="text" class="bfInputText bfChangeListen" name="galleryName" value="<?=$gallery['gallery_name']?>" />
			</li>
			<li>
				<label for="galleryHidden">Hide this gallery</label>                                                             
				<div class="uiButtonSet">
					<label for="galleryHiddenYes" class="radioLabel">Yes</label>
					<input name="galleryHidden" id="galleryHiddenYes" type="radio" value="1" class="bfChangeListen" <? if ($gallery['gallery_hidden']==1) { ?>checked="checked"<? } ?> />
					<label for="galleryHiddenNo" class="radioLabel">No</label>
					<input name="galleryHidden" id="galleryHiddenNo" type="radio" value="0" class="bfChangeListen" <? if ($gallery['gallery_hidden']==0) { ?>checked="checked"<? } ?> />
				</div>
			</li>
			<li>
			<? if (hasProtectedGalleries()) { ?>
				<label for="galleryPassword">Password for this gallery</label>
				<input type="text" class="bfInputText bfChangeListen" name="galleryPassword" value="<?=$gallery['password']?>" />
			<? } ?> 
			</li>
			<li>
				<button id="otherToggle">Advanced Settings</button>
			</li>
			<li class="divider"></li>
		</ul> 
	</div>
	
	<div id="otherSettings"> 
		<ul>
		<? while (list($key, $val) = each($gallery)) { ?>
			<? if (stristr($key, "show_")) { ?>
			<li class="otherSetting">
				<label for="galleryHidden"><?=ucfirst(str_replace("_", " ", $key))?></label>                                                             
				<div class="uiButtonSet">
					<label for="<?=$key?>Yes" class="radioLabel">Yes</label>
					<input name="<?=$key?>" id="<?=$key?>Yes" type="radio" value="1" class="bfChangeListen" <? if ($val==1) { ?>checked="checked"<? } ?> />
					<label for="<?=$key?>No" class="radioLabel">No</label>
					<input name="<?=$key?>" id="<?=$key?>No" type="radio" value="0" class="bfChangeListen" <? if ($val==0) { ?>checked="checked"<? } ?> />
				</div>
			</li> 
			<? } ?>
		<? } ?> 
		<li class="divider"></li> 
		</ul>
	</div>
	 
    <div class="galleryUploader">
		<input name="galleryfiles" type="file" class="galleryfiles" id="gal<?=$gallery['gallery_id']?>" size="10" /> 
		<div id="status-message"></div>
		<div id="gallery-queue"></div>
	</div>
  	
	<div style="clear:both;"></div>
	<div class="galleryImages">
		<? foreach($images as $img) { 
			$prettyName = $img['video_file'];
		   	$html  = "";
         	$html .= '<div class="thumbItem bfSortable '.$type.'Item" id="vid'.$img['video_id'].'">';
			$html .= '<div class="deleteButton"></div>';
			$html .= '<div style="clear:both;"></div>';
			$html .= '<div class="thumbFileLabel '.$type.'Label">'.$prettyName.'</div>';
			
			$html .= '<div class="thumbHolder '.$type.'Thumb">';
			$html .= '<a rel="galVideos" href="sections/videos/video-details.php?f='.$_SESSION['user']['temp_url'].'videos/'.$img['video_file'].'&vf='.$img['video_file'].'&vid='.$img['video_id'].'">';
			if ($img['thumb'] != "") {
				$html .= '<img src="'.$_SESSION['user']['temp_url'].'gallery/'.$thumbSize.'/'.$img['thumb'].'" title="'.$img['video_file'].'"/>';
			} else {
				$html .= '<img src="img/default-video-thumb.jpg" title="'.$img['video_file'].'"/>';
			}
			$html .= '</a>';
			$html .= '</div>';
            $html .= '<a rel="galVideos" href="sections/videos/video-details.php?f='.$_SESSION['user']['temp_url'].'videos/'.$img['video_file'].'&vf='.$img['video_file'].'&vid='.$img['video_id'].'" class="thumbUploadLink">Upload Thumbnail</a>';

			$html .= '<input type="checkbox" class="imgCheck"/>';
			
			$html .= '</div>'; 
			echo $html; 
		} ?> 
	</div>
	<div style="clear:both;"></div>
</div>

<div class="actionsPanel">
	<div class="icon">
	</div>
	<div class="inner">
		<ul>
			<li> 
				<label for="galleryView">View mode</label>
				<select name="viewMode" class="smallSelect" id="viewMode">
					<option><?=$viewLabel?></option>
					<option>-----------------</option>
					<option value="small">Small Thumbnails</option>
					<option value="large">Large Thumbnails</option>
					<option value="list">List</option>
				</select>
			</li>
			
			<li>
				<label for="bulkActions">Bulk actions</label>
		   		<select name="bulkActions" class="smallSelect" id="bulkActions">
					<option>Select One</option>
					<option>-----------------</option>
					<option value="delete">Delete Selected</option>
					<option value="combine">Edit/Combine Selected</option>
				</select> 
			</li>
			<li>
			   	<label for="selectActions">Select</label>
				<button id="selectAll">All</button>
				<button id="selectNone">None</button>
			</li>
		</ul>
	</div>
</div>

<script type="text/javascript">
// <![CDATA[  

var type = "<?=$type?>";
var thumbSize = "<?=$thumbSize?>";
var galID = "<?=$gallery['gallery_id']?>"; 
var lastThumbChecked; 

$("#otherToggle").click(function() {
 	$("#otherSettings").slideToggle('fast');
}); 
if ($("#otherSettings").find('.otherSetting').length > 0) {
	$("#otherToggle").show();
}

function galleryFunctionality() {  
	
	
	$(".captionButton").unbind("click");
	$(".captionButton").click(function() {   
		$.log('caption display: '+$(this).parent().find(".imgCap").css("display"));
		if ($(this).parent().find(".imgCap").css("display") == "none") {
			$(this).parent().find(".imgCap").show();
			//$(this).parent().find(".thumbHolder").hide();
		} else {
			$(this).parent().find(".imgCap").hide();
			//$(this).parent().find(".thumbHolder").show();
		}
	});
	
	
	/*
	$('.imgCap textarea').keyup(update).mousedown(update).mousemove(update).mouseup(update);  //.imgCap

	function update(e) {
	
		// here we fetch our text range object
		var range = $(this).getSelection().text;
		//$.log("range: "+range);
	}
	*/ 
	
	$(".thumbItem").find(".deleteButton").unbind("click"); 
	$(".thumbItem").find(".deleteButton").click(function() {
		var save = confirm("Are you sure you want to delete this image from this gallery? This cannot be undone.");
		if (save) { 
			removeVideo($(this).parent()); 
		}
	});
	
	<? if ($type == 'list') { ?>
	$(".thumbFileLabel").unbind("click");
	$(".thumbFileLabel").click(function() {
    	$(this).parent().find(".thumbHolder").slideToggle('fast');
	});
	<? } ?>
	
	//
	// handle check box shift functionality
	$(".imgCheck").unbind("click");
	$(".imgCheck").click(function() {
		var parentID = $(this).parent().attr("id");
		var isChecked = $(this).attr("checked"); 
		//
		// check if shift key is down  
		if (!event.shiftKey) {
			lastThumbChecked = $(this).parent();
			return;
		}
		//
		// get index
		var thisIndex = $(".thumbItem").index($(this).parent());
		var lastIndex = $(".thumbItem").index(lastThumbChecked);
		//
		// determine if we should move up the dom or down
		if (thisIndex > lastIndex) {
			var start = lastIndex;
			var end = thisIndex;
		} else {
			var start = thisIndex;
			var end = lastIndex;
		} 
		
		for (var i=start; i<=end; i++) { 
			if (isChecked) {
				$(".thumbItem:eq("+i+")").find(".imgCheck").attr("checked", "checked");
			} else {
				$(".thumbItem:eq("+i+")").find(".imgCheck").removeAttr("checked");
			}
		}
		 
	});
	
}
galleryFunctionality();  

$(".galleryImages").sortable({
	update: function(event, ui) { 
		var d = new Object(); 
		d['imgList'] = new Array();
		$(this).find(".thumbItem").each(function() {
			d['imgList'].push($(this).attr("id").substr(3)); // pushes imgNNN minus the img
		});
		$.log("d['imgList']: "+d['imgList']);

		$.ajax({
  			type: 'POST',
  			url:'actions/videos/update-gallery-videos-order.php',
  			data: d
		}); 
	}
});

//
// gallery image upload 
$(".galleryfiles").uploadify({
  'uploader'       : 'js/plugins/uploadify-v2.1.4/uploadifyCS3.swf',
  'script'         : adminURL + '_bfadmin/file_upload.php', 
  'scriptData'     : {'u':u,'p':p,'mode':'video'},      
  'method'         : 'post',
  'cancelImg'      : 'js/plugins/uploadify-v2.1.4/cancel.png',
  'multi'          : true,
  'auto'           : true,
  'fileExt'        : '*.mp4;*.mov;*.m4v;*.flv;',
  'fileDesc'       : 'Video Files (.MP4, .MOV, .M4V, .FLV)',
  'queueID'        : 'gallery-queue', 
  'buttonText'     : 'UPLOAD VIDEOS',
  'width'          : 140,
  'queueSizeLimit' : 100,
  'simUploadLimit' : 1,
  'removeCompleted': true,
  'sizeLimit'      : 32000000,
  'onSelectOnce'   : function(event,data) {
      	//$('#status-message').text(data.filesSelected + 'uploading...');
		$('#status-message').text('uploading...');
    }, 
  'onComplete'     : function(event, ID, fileObj, response, data) { 
		var fileParts = getNameAndExtension(fileObj['name']); 
		var imageFile = cleanfile(fileParts[0])+'.'+fileParts[1];
	  	$.log('fileName: '+cleanfile(fileParts[0])+'.'+fileParts[1]);
	  	$.log('response: '+response); 
		//
		addVideoToGallery(imageFile);
		//
   	},
  'onAllComplete'  : function(event,data) {
		$.log("filesUploaded: "+data.filesUploaded);
		$.log("data.errors: "+data.errors);
		if (data.errors > 0) {
      		$('#status-message').text("There was an error with the upload.");
		} else {
			$('#status-message').text("");
		}
    }
});

function addVideoToGallery(videoFile) {
	$.ajax({
      type: 'POST',
      url:'actions/videos/add-video-to-gallery.php',
      data: ({'galID':galID, 'videoFile':videoFile})
    }).done(function( msg ) {
	  	$.log( "msg: " + msg );
	 	if (msg.substr(0,7) == "SUCCESS") { 
			//
			var vidID = msg.substr(8);
			//
		    var html = "";
			html += '<div class="thumbItem bfSortable '+type+'Item" id="vid'+vidID+'">';
			html += '<div class="deleteButton"></div>';
			html += '<div style="clear:both;"></div>';
			html += '<div class="thumbFileLabel '+type+'Label">'+videoFile+'</div>';
			html += '<div class="thumbHolder '+type+'Thumb">';
			html += '<a rel="galVideos" href="sections/videos/video-details.php?f='+tempURL+'videos/'+videoFile+'&vf='+videoFile+'&vid='+vidID+'">';
			html += '<img src="img/default-video-thumb.jpg" title="'+videoFile+'"/>';
			html += '</a>';
			html += '</div>'; 
			html += '<a rel="galVideos" href="sections/videos/video-details.php?f='+tempURL+'videos/'+videoFile+'&vf='+videoFile+'&vid='+vidID+'" class="thumbUploadLink">Upload Thumbnail</a>';
			html += '<input type="checkbox" class="imgCheck"/>';
			html += '</div>';
			//
			$(".galleryImages").append(html);
			handleUI();
			galleryFunctionality();
		} else {
			alert("There was a problem adding the video "+videoFile+" to this gallery. Please try again later. If problems persist, please contact support.");
		}
		
	});
}

$(".viewModeButton").click(function() {
   	changeGalleryView($(this).val());
});
$("#viewMode").change(function() {
	changeGalleryView($(this).val());
});

function removeVideo(thumb) { 
	var videoFile = thumb.find("img").attr("title");
   	var videoID = thumb.attr('id').substr(3);   
	$.log("removeVideo: "+videoFile+', '+videoID);
	$.ajax({
      type: 'POST',
      url:'actions/videos/remove-video-from-gallery.php',
      data: ({'videoID':videoID})
    }).done(function( msg ) {
	  	$.log( "msg: "+msg);
		if (msg.substr(0,7) == 'SUCCESS') {
			thumb.fadeOut("fast", function() {
			   thumb.remove(); 
			});
		
			//
			// now check for duplicates
			$.ajax({
	          type: 'POST',
	          url:'actions/videos/check-for-duplicate-videos.php',
	          data: ({'videoFile':videoFile})
	        }).done(function( msg ) {
			  	$.log( "duplicates?: "+msg);  
				//
				// if no duplicates (0) then remove the file
				if (msg == '0') { 
					$.ajax({
			          type: 'POST',
			          url:adminURL + '_bfadmin/file_remove.php',
			          data: ({'u':u,'p':p, 'mode':'video', 'file':videoFile})
			        }).done(function( msg ) {
					  $.log( "msg: " + msg );
					});
				} 
			});
			//
		} else {
			alert("There was an error removing the video from this gallery.");
		}
	});
}                         

function changeGalleryView(type) {
	var v = "#";
    for (var i=0; i<paths.length; i++) { 
	 	if (paths[i] != "") {
			if (i == paths.length - 1) {
				var curPath = paths[i].split('-');
				v += "/"+curPath[0] + '-' + type;
			} else {
				v += "/"+paths[i];
			} 
		}
	}
	location.href = v;
}

$("#bulkActions").change(function() { 
	
   	if ($(this).val() == 'delete') {
		//
	    var save = confirm("Are you sure you want to delete these images from this gallery? This cannot be undone.");
		if (save) { 
			$(".thumbItem").each(function() {
				if ($(this).find(".imgCheck").attr("checked") == "checked") {
				   removeVideo($(this)); 
				}
			});
		}
		
	}
	
	// reset selection
	$(this).val("Select One");
});
$("#selectAll").click(function() {
	$(".imgCheck").attr("checked","checked");
});
$("#selectNone").click(function() {
	$(".imgCheck").removeAttr("checked");
});

$(".actionsPanel").find(".icon").click(function() {
	if ($(".actionsPanel").hasClass("showing")) {
		$(".actionsPanel").removeClass("showing");
		$(".actionsPanel").animate({
			"marginLeft":2
		}, 'fast');
	} else { 
		$(".actionsPanel").addClass("showing");
		$(".actionsPanel").animate({
			"marginLeft":-200
		}, 'fast');
	}
});

$.ajax({
  type: 'POST',
  url:adminURL + '_bfadmin/write_policy_file.php',
  data: ({'u':u,'p':p})
}).done(function( msg ) {
  $.log( "crossdomain?: " + msg );
});


// ]]>
</script>