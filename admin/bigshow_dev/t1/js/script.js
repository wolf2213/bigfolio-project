var curNum = 0;
var curImgNum = 0;
var ssOn = true;
var ssInt; 
var currentTrack = 0;

$(document).ready(function() { 
	
	$(this).bind("contextmenu", function(e) {
        e.preventDefault();
    });
	   
	$.address.change(function(event) {
		//
		// check for changed settings not saved 
		var paths = event.value.split("/");
		handleContent(paths);
		//
    });
	// 
	
	// hide things
	$("#leftNav").fadeTo(0,0);
	$("#rightNav").fadeTo(0,0);
	$("#bigshowFooter").fadeTo(0,0);
	$("#container").fadeTo(0,0);
	
	if (isProtected) {
		drawX("passwordCloseBtn", 12, 12, 2, $("a.toplink").css("color"), 2, "x");
		$("#BFPasswordBox").find(".closeBtn").click(function() {
			$(this).parent().hide();
		});
		$("#BFPasswordBox").show();
	} else {
		buildLayout();
	}                       
	
});

function validateSlideshow() {
	//var pass = prompt("Enter the password for this slideshow."); 
	var pass = $("#bfGalPass").val(); 
	var username = $("#username").val();
	if (username == undefined) {
		var postData = {'validate':1,'s':ssURL,'password':pass}; 
	} else {
		var postData = {'validate':1,'s':ssURL,'password':pass,'u':username};
	}
	$.log("pass: "+pass);
	$.ajax({
      type: 'POST',
      url:'index.php',
      data: postData
    }).done(function( msg ) {
	  	$.log( "correctpassword?: "+msg);  
		//
		// if no duplicates (0) then remove the file
		if (msg.substr(0,1) == '1') { 
			buildLayout(); 
			$(window).load();
		} else {
			/*
			$("#BFPasswordBox").hide();
			$("#container").html("<div style='text-align: center; margin-top:40px;'>Incorrect password. Reload the page to try again.</div>");
			$("#container").fadeTo(0,1);
			*/
			alert("Incorrect password. Please try again.");
		} 
	});
} 

function buildLayout() {
	
	$("#container").addClass("ready");
	$("#BFPasswordBox").hide();

	// leftArrow functionality
	drawArrow($("#leftArrow").attr('id'), 50, 80, 10, link_color, 2, 'left', 'back');
	$("#leftArrow").mouseover(function() {
		drawArrow($(this).attr('id'), 50, 80, 10, link_hover_color, 2, 'left', 'back');
	});
	$("#leftArrow").mouseout(function() {
		drawArrow($(this).attr('id'), 50, 80, 10, link_color, 2, 'left', 'back');
	});
	$("#leftArrow").click(function() { 
		slideshowOff();
	 	previousImage();
	});  
	//

	// rightArrow functionality
	drawArrow($("#rightArrow").attr('id'), 50, 80, 10, link_color, 2, 'right', 'next');
	$("#rightArrow").mouseover(function() {
		drawArrow($(this).attr('id'), 50, 80, 10, link_hover_color, 2, 'right', 'next');
	});
	$("#rightArrow").mouseout(function() {
		drawArrow($(this).attr('id'), 50, 80, 10, link_color, 2, 'right', 'next');
	});
	$("#rightArrow").click(function() { 
		slideshowOff();
	 	nextImage();
	});  
	// 

	// leftThumb
	drawArrow($("#thumbPrevArrow").attr('id'), 45, 66, 12, footer_color, 2, 'left', 'back');
	drawArrow($("#thumbNextArrow").attr('id'), 45, 66, 12, footer_color, 2, 'right', 'next');
	//
	$(".footerBtn").mouseover(function() {
		if ($(this).hasClass("disabled")) { return; }
	 	$(this).css("backgroundColor", link_hover_color);
	});
	$(".footerBtn").mouseout(function() { 
		if ($(this).hasClass("disabled")) { return; }
	 	$(this).css("backgroundColor", link_color);
	});
	//
	$("#thumbPrev").click(function() {
		if ($(this).hasClass("disabled")) { return; }
	    scrollThumbs('backward');
	});
	$("#thumbNext").click(function() { 
		if ($(this).hasClass("disabled")) { return; }
	    scrollThumbs('forward');
	}); 

	//
	// loading icon	
	var sqw = 5;
	var sqh = 5;
	var radius = 2; 

	var loadingIconWidth = $("#loadingIcon").width()/2 - radius;
	var loadingIconHeight = $("#loadingIcon").height()/2 - radius;
	var angles = new Array(Math.PI/2, 3*Math.PI/4, Math.PI, -3*Math.PI/4, -Math.PI/2, -Math.PI/4, 0, Math.PI/4);

 	for (var i=0; i<angles.length; i++) { 
		/*
		var canvas = document.getElementById("square"+i);
	    var context = canvas.getContext("2d");
	    var topLeftCornerX = loadingIconWidth + loadingIconWidth * Math.cos(angles[i]);
	    var topLeftCornerY = loadingIconHeight + loadingIconHeight * Math.sin(angles[i]);
		console.log("x,y: "+topLeftCornerX+', '+topLeftCornerY)
	    var width = sqw;
	    var height = sqh;
    
	    context.beginPath();
	    context.rect(topLeftCornerX, topLeftCornerY, width, height);
	    context.fillStyle = link_color;
	    context.fill(); 
		*/
		var canvas = document.getElementById("square"+i); 
		if (canvas.getContext){ 
	   	    var context = canvas.getContext("2d");
		    var centerX = loadingIconWidth + radius + loadingIconWidth * Math.cos(angles[i]);
		    var centerY = loadingIconHeight + radius + loadingIconHeight * Math.sin(angles[i]);

		    context.beginPath();
		    context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
		    context.fillStyle = link_color;
		    context.fill(); 
		}
	}  

	drawSlideshowIcon("pause");

	$("#slideshowBtn").mouseout(function() { 
		if ($(this).hasClass("disabled")) { return; }
	 	$(this).css("backgroundColor", link_color);
	});
	$("#slideshowBtn").mouseover(function() {   
		if ($(this).hasClass("disabled")) { return; }
	 	$(this).css("backgroundColor", link_hover_color);
	});
	$("#slideshowBtn").click(function() {  
		if (!ssOn) {
			slideshowOn();
		} else {
			slideshowOff();
		}
	
	});

	//
	// size everything
	//handleResize();  
	if (music_files.length > 0) { 
		$("#musicBtn").click(toggleAudio);
		setupAudioPlayer(music_files[currentTrack]);          
	}
	//
	//
	$(window).load(function() { 
		//
		//
		$("#leftNav").fadeTo('fast', 1);
		$("#rightNav").fadeTo('fast', 1);
		$("#bigshowFooter").fadeTo('fast', 1);
		handleResize(); 
		findThumb(); 

		//
	}); 
	
	$(window).resize(function() {
	   handleResize(); 
	});
	
	//
	// now handle content
	$("#container").fadeTo(0,1);
	var paths = $.address.value().split("/");
	handleContent(paths);
}

function thumbClick() {
	$(window).scrollTo( 0, 800, {queue:true} );
	slideshowOff();
}

function drawSlideshowIcon(type) {
	if (type == 'play') {
		// draw slideshow play button
	    var canvas = document.getElementById("slideshowIcon");
		if (canvas && canvas.getContext){

		  	// use getContext to use the canvas for drawing
		  	var ctx = canvas.getContext('2d');
			var w = 45;
			var h = 66;
			canvas.width = w;
			canvas.height = h;
			var hpad = 15;
			var vpad = 20; 
			var color = footer_color;
			ctx.clearRect(0,0,w,h);
			ctx.beginPath(); 
			ctx.lineCap = "butt";  

			ctx.moveTo(hpad, vpad);
			ctx.lineTo(w-hpad, Math.round((h)/2));
			ctx.lineTo(hpad,h-vpad);
			ctx.closePath(); 

			ctx.fillStyle = color; 
			ctx.fill();
	

		} else {
			$("#slideshowBtn").html("play");
		}
	
	} else { // draw pause
		
		// draw slideshow pause button
		var canvas = document.getElementById("slideshowIcon");
		if (canvas && canvas.getContext){

		  	// use getContext to use the canvas for drawing
		  	var ctx = canvas.getContext('2d');
			var w = 45;
			var h = 66;  
			canvas.width = w;
			canvas.height = h;
			var hpad = 15;
			var vpad = 20;
			ctx.beginPath();
		   	ctx.rect(hpad, vpad, 5, h-vpad*2);
		    ctx.rect(w - hpad - 5, vpad, 5, h-vpad*2);
		    ctx.fillStyle = footer_color;
		    ctx.fill();

		} else {
			$("#slideshowBtn").html("pause");
		}
	
	}
}

function slideshowOff() {  
	ssOn = false; 
	clearTimeout(ssInt);
	drawSlideshowIcon("play");
	//$("#playIcon").show();
	//$("#pauseIcon").hide();
} 

function slideshowOn() { 
	ssOn = true;  
	nextImage();
	drawSlideshowIcon("pause");
	//$("#pauseIcon").show();
	//$("#playIcon").hide();
}

function previousImage() {
	var paths = $.address.value().split("/");
   	var curNum = paths[1];
	if (curNum == "") {
		curNum = 1;
	}
	curNum--;
   	if (curNum < 1) {
		curNum = $("#bigshowThumbs").find("li").length;
	} 
	$.address.value("/"+curNum);
}

function nextImage() {
	var paths = $.address.value().split("/");
   	var curNum = paths[1];  
	if (curNum == "") {
		curNum = 1;
	}
	curNum++; 
   	if (curNum > $("#bigshowThumbs").find("li").length) {
		curNum = 1;  
	}
	$.address.value("/"+curNum);
} 

function drawX(id, w, h, pad, color, thickness, altText) {
	var canvas = document.getElementById(id);  
	canvas.width = w;
	canvas.height = h;

	// Make sure we don't execute when canvas isn't supported
	if (canvas.getContext){

	  	// use getContext to use the canvas for drawing
	  	var ctx = canvas.getContext('2d');
		
		ctx.clearRect(0,0,w,h);
		ctx.beginPath(); 
		ctx.lineCap = "butt";
		ctx.strokeStyle = color; //gradObj;  
	
		ctx.moveTo(pad, pad);
		ctx.lineTo(w-pad, h-pad);
		ctx.moveTo(w-pad, pad);
		ctx.lineTo(pad, h-pad);
		
		ctx.lineWidth = thickness;
		ctx.stroke();
	
	} else {   
		$("#"+id).parent().html(altText);
	}
}

function drawArrow(id, w, h, pad, color, thickness, direction, altText) {
	var canvas = document.getElementById(id);  
	canvas.width = w;
	canvas.height = h;

	// Make sure we don't execute when canvas isn't supported
	if (canvas.getContext){

	  	// use getContext to use the canvas for drawing
	  	var ctx = canvas.getContext('2d');
		
		ctx.clearRect(0,0,w,h);
		ctx.beginPath(); 
		ctx.lineCap = "butt";
		ctx.strokeStyle = color; //gradObj;  
	
		if (direction == 'right') {
			ctx.moveTo(pad, pad);
			ctx.lineTo(w-pad, Math.round((h)/2));
			ctx.lineTo(pad,h-pad); 
		} else if (direction == 'left') { 
			ctx.lineTo(w-pad, pad)
		    ctx.lineTo(pad, Math.round((h)/2));
		    ctx.lineTo(w-pad,h-pad); 
		}
		ctx.lineWidth = thickness;
		ctx.stroke();
	
	} else {   
		$("#"+id).html(altText);
	}
} 

function animateLoader() { 
	if ($("#loadingIcon").hasClass("running")) {
		return;
	}
	var t = 500; 
	$("#loadingIcon").addClass("running");
	for (var i=0; i<$(".loaderSquare").length; i++) { 
		if (i < $(".loaderSquare").length - 1) {
			$("#square"+i).delay(i * t/4).fadeTo(t, 0).fadeTo(t, 1);           
		} else {
			$("#square"+i).delay(i * t/4).fadeTo(t, 0).fadeTo(t, 1, 
				function() {
					$("#loadingIcon").removeClass("running");
				   	if ($("#loadingIcon").css("display") != "none") {
						animateLoader();
					}
				});
		}
	}
} 
function showLoader() {
	$("#loadingIcon").show();
	animateLoader();
}
function hideLoader() {
	$("#loadingIcon").hide();
}


function handleContent(paths) {  
	if ($("#container").hasClass("ready")) {
		clearTimeout(ssInt);
		loadImage(paths[1]);
	}
} 

function scrollThumbs(direction) {
	//
	if ($("#bigshowThumbs ul").width() < $("#bigshowThumbs").width()) {
		return;
	}
	//
	if (direction == "backward") {  
		var leftMargin = $("#bigshowThumbs ul").getVal("marginLeft") + $("#bigshowThumbs").width();
		if (leftMargin > 0) {
			leftMargin = 0;
		} 
	} else if (direction == "forward") {
		var leftMargin = $("#bigshowThumbs ul").getVal("marginLeft") - $("#bigshowThumbs").width();
		if (leftMargin + $("#bigshowThumbs ul").width() < $("#bigshowThumbs").width()) {
			leftMargin = $("#bigshowThumbs").width() - $("#bigshowThumbs ul").width();
		}
	}  
	//
	$("#bigshowThumbs ul").animate({
		"marginLeft": leftMargin
	}, 'slow', function() { checkThumbArrows() });
}
function findThumb() { 
	//
	if ($("#bigshowThumbs ul").width() < $("#bigshowThumbs").width()) {
		return;
	}
	//
	var paths = $.address.value().split("/");
	var num = paths[1];
	if (paths[1] == "") {
		num = 1;
	}
	num--;
	//  
	var theThumb = $("#bigshowThumbs li:eq("+num+")"); //$("#bigshowThumbs li").get(num);
	var leftMargin = $("#bigshowThumbs ul").getVal("marginLeft");  
	var thumbPosition = theThumb.position().left - $("#bigshowThumbs").position().left - leftMargin;  
	if (Number(thumbPosition) + Number(theThumb.width()) > $("#bigshowThumbs").width() - leftMargin) {
		leftMargin = $("#bigshowThumbs").width() - Number(thumbPosition) - Number(theThumb.width());
	} else if (-thumbPosition > leftMargin) { // && thumbPosition < $("#bigshowThumbs").width()) {
		leftMargin = -thumbPosition+1;
	} 
	$("#bigshowThumbs ul").animate({
		"marginLeft": leftMargin
	}, 'slow', function() { checkThumbArrows() }); 
}
function checkThumbArrows() {                                     
	if ($("#bigshowThumbs ul").width() > $("#bigshowThumbs").width()) { 
		var leftMargin = $("#bigshowThumbs ul").getVal("marginLeft"); 
		if (leftMargin >= 0) {
			leftMargin = 0; 
			$("#thumbPrev").mouseout();
			$("#thumbPrev").addClass("disabled");
			$("#thumbPrev").fadeTo('fast', .5);
		} else { 
			$("#thumbPrev").removeClass("disabled");
			$("#thumbPrev").fadeTo('fast', 1);
		}   
	
		if (leftMargin + $("#bigshowThumbs ul").width() <= $("#bigshowThumbs").width()) {
			leftMargin = $("#bigshowThumbs").width() - $("#bigshowThumbs ul").width();
			$("#thumbNext").mouseout();
			$("#thumbNext").addClass("disabled");
			$("#thumbNext").fadeTo('fast', .5);
		} else {
			$("#thumbNext").removeClass("disabled");
			$("#thumbNext").fadeTo('fast', 1);
		}   
	} else {
		var leftMargin = 0;                 
		
		$("#thumbPrev").mouseout();
		$("#thumbPrev").addClass("disabled");
		$("#thumbPrev").fadeTo('fast', .5);
		
		$("#thumbNext").mouseout();
		$("#thumbNext").addClass("disabled");
		$("#thumbNext").fadeTo('fast', .5);
	}
	$("#bigshowThumbs ul").css("marginLeft", leftMargin);
}

function loadImage(num) { 
	//
	//console.log('num: '+num);
	showLoader();
	if (num == "") {
		num = 1;
	}
   	num--;  
	var nextNum = num+1;
	if (nextNum >= $("#bigshowThumbs ul li").length) {
		nextNum = 0;
	}
	//console.log('num: '+num);
   	//
	var imgURL = slideshowStorage + $("#bigshowThumbs ul li").eq(num).find("a").attr('title');
	var imgCaption = $("#bigshowThumbs ul li").eq(num).find("img").attr('alt');
	if (show_filenames) {
		if (imgCaption != "") {
			imgCaption += "<br>";
		}
		imgCaption += $("#bigshowThumbs ul li").eq(num).find("a").attr('title').substr(0, $("#bigshowThumbs ul li").eq(num).find("a").attr('title').length-4);
	} 
	var imgID = 'galImg'+curImgNum; 
	curNum = num; 
	var lastImgID = 'galImg'+(curImgNum-1);
	curImgNum++;
	var imgHTML  = "<div class='bigshowImage'>"; 
		imgHTML += "<img src='"+imgURL+"' alt='' class='galImg' id='"+imgID+"'/>";
		imgHTML += "</div>";
	//
	$("#bigshowImages").append(imgHTML);  
	//
	// check for img loaded
	var imgLoaded = false; 
	var curImg = $("#"+imgID); 
	curImg.addClass("loading");
	var lastImg = $("#"+lastImgID); 
	if (!lastImg.hasClass("loading")) {
		lastImg.fadeTo('fast',.2);        
	}
	//console.log('lastImgID: '+lastImgID);
	//console.log("lastImg.length: "+lastImg.length);
	$("#"+imgID).load(function(){
		//console.log("curImgNum: "+(curImgNum-1));
		//console.log("thisNum: "+Number($("#"+imgID).attr('id').substr(6)));
		if (imgLoaded || curImgNum-1 != Number($("#"+imgID).attr('id').substr(6))) {
			return;
		} 
		hideLoader();
		imgLoaded = true; 
		curImg.removeClass("loading"); 
		curImg.fadeTo(0,0);
		curImg.show();  
		curImg.delay(600).fadeTo('slow', 1); 
		
		if (ssOn) {
			ssInt = setTimeout(nextImage, slideshow_speed * 1000);
		} 
		if (lastImg.length > 0) { 
			lastImg.fadeOut('slow', function() {
				var thisNum = Number(lastImg.attr('id').substr(6)); 
				cleanUpImages(thisNum);
			});
		}                                     
		
		$("#bigshowCaption").html(imgCaption);
		handleResize();  
		
		var imgURL = slideshowStorage + $("#bigshowThumbs ul li").eq(nextNum).find("a").attr('title');
		$("#bigshowPreload").html("<img src='"+imgURL+"' alt='' />");
	});
	if ($("#"+imgID)[0].complete){ 
		//console.log("curImgNum: "+(curImgNum-1));
		//console.log("thisNum: "+Number($("#"+imgID).attr('id').substr(6)));
		if (imgLoaded || curImgNum-1 != Number($("#"+imgID).attr('id').substr(6))) {
			return;
		}
		hideLoader();
		imgLoaded = true;
		curImg.removeClass("loading");
		curImg.fadeTo(0,0);
		curImg.show();
		curImg.delay(600).fadeTo('slow', 1); 
		   
		if (ssOn) {
			ssInt = setTimeout(nextImage, slideshow_speed * 1000);
		}
		if (lastImg.length > 0) {
			lastImg.fadeOut('slow', function() {
				var thisNum = Number(lastImg.attr('id').substr(6)); 
				cleanUpImages(thisNum); 
			}); 
		}  
		
		$("#bigshowCaption").html(imgCaption);
		handleResize();
		
		var imgURL = slideshowStorage + $("#bigshowThumbs ul li").eq(nextNum).find("a").attr('title');
		$("#bigshowPreload").html("<img src='"+imgURL+"' alt='' />");
	} 
	//
	findThumb();
	//
}

function cleanUpImages(num) { 
	for (var i=num; i>=0; i--) { 
		if ($("#galImg"+i).length > 0) {
			$("#galImg"+i).remove(); 
			$("#galImg"+i).parent().remove();
		}
	}
} 

function handleSlideshowImages() {     
	var pad = 20;
	var maxWidth = $(window).width() - $("#leftNav").width() - pad - $("#rightNav").width() - pad;
	var maxHeight = $(window).height() - $("header").height() - Math.abs($("#bigshowFooter").getVal("marginTop")) - $("#bigshowCaption").height() - $("#bigshowCaption").getVal("paddingTop") - $("#bigshowCaption").getVal("paddingBottom");  
	$("#bigshowImages").width($(window).width());
	$("#bigshowImages").height(maxHeight);
	//$("#bigshowImages").css("marginLeft", $("#leftArrow").width() + pad);
	$(".galImg").css("maxWidth", maxWidth - pad * 4 - $(".galImg").getVal("paddingLeft") * 2);
	$(".galImg").css("maxHeight", maxHeight - pad * 2 - $(".galImg").getVal("paddingTop") * 2);
	$(".galImg").each(function() {
		if ($(this).height() != 0 && $(this).height() != undefined) {            
			var leftMargin = $("#leftNav").width() + pad + Math.round((maxWidth - $(this).width() - $(".galImg").getVal("paddingTop") * 2) / 2);
			$(this).css('marginLeft', leftMargin);
		}
		if ($(this).width() != 0 && $(this).width() != undefined) {
	 		var topMargin = Math.round((maxHeight - $(this).height() - $(".galImg").getVal("paddingTop") * 2) / 2);
			$(this).css('marginTop', topMargin);
		}
	});
}  

function handleResize() { 
	$('img').bind('dragstart', function(event) { event.preventDefault(); });
	// 
	var pad = 20;  
	//
	$("#bigshowFooter").css("marginTop", -$("#bigshowFooter").height() - $("#bigshowFooter").getVal("paddingTop") -  $("#bigshowFooter").getVal("paddingBottom")); 
	$("#bigshowFooter").width($(window).width());
	var footerBtnWidths = -1;
	$(".footerBtn").each(function() {
		footerBtnWidths += $(this).width() + 1;
	});
	$("#bigshowThumbs").width($(window).width() - footerBtnWidths - $("#bigshowFooter").getVal("paddingLeft") * 2); 
	var thumbsWidth = 0;
	$("#bigshowThumbs").find("li").each(function() {
		thumbsWidth += $(this).width() + $(this).getVal("marginLeft");
	});
	$("#bigshowThumbs").find("ul").width(thumbsWidth); 
	//
	var maxWidth = $(window).width();
	var maxHeight = $(window).height() - $("header").height() - Math.abs($("#bigshowFooter").getVal("marginTop"));
	//
	handleSlideshowImages(); 
	//
	$("#leftNav").css("marginLeft", pad);
	$("#leftNav").css("marginTop", Math.round((maxHeight - $("#leftNav").height())/2));
	$("#rightNav").css("marginLeft", Math.round(maxWidth - $("#rightNav").width() - pad));
	$("#rightNav").css("marginTop", Math.round((maxHeight - $("#rightNav").height())/2)); 
	// 
	checkThumbArrows();
}

function setupAudioPlayer(fileName) {
	//
	$("#audioPlayer").addClass('isActive');
	$("#audioPlayer").jPlayer( {
	    ready: function () {
	      $(this).jPlayer("setMedia", {
	        mp3: slideshowStorage+fileName
	      }).jPlayer("play");
	    },
		ended: function() { 
		  handleNextTrack();
	  	},
	    supplied: "mp3",
	    swfPath: sourceHost + sourceTheme + "/js/",
		solution: 'html, flash',
		preload: 'metadata'
	}); 
	$.log(sourceHost + "/" + sourceTheme + "/js");
	$.log('setupAudioPlayer: '+fileName);
}

function loadNewAudio(fileName) {
	$.log('loadNewAudio: '+fileName);
	$("#audioPlayer").jPlayer("setMedia", {
	    mp3: slideshowStorage+fileName
	}).jPlayer("play");
	//
	if ($("#audioIcon").hasClass("paused")) {
		$("#audioIcon").removeClass("paused");
		$("#audioPlayer").jPlayer('play');
		animateAudioIcon();
	}
}

function handleNextTrack() {
	currentTrack++;
	if (currentTrack >= music_files.length) {
		currentTrack = 0;
	}
	loadNewAudio(music_files[currentTrack]);
}

function handlePreviousTrack() {
	currentTrack--;
	if (currentTrack < 0) {
		currentTrack = music_files.length - 1;
	}
	loadNewAudio(music_files[currentTrack]);
}

function toggleAudio() {
	if ($("#musicBtn").hasClass("paused")) {
		$("#musicBtn").removeClass("paused");
		$("#musicBtn").fadeTo('fast', 1); 
		$("#audioPlayer").jPlayer('play');
		audioOn = true;
	} else {
		$("#musicBtn").addClass("paused");
		$("#musicBtn").fadeTo('fast', .5); 
		$("#audioPlayer").jPlayer('pause');
		audioOn = false;
	}
}

function autoPauseAudio() {
	if ($("#audioPlayer").hasClass('isActive') && !$("#musicBtn").hasClass("paused")) {
		$("#audioPlayer").addClass('autoPaused');
		$("#musicBtn").hide();
		toggleAudio();
	}
}

$(window).focus(function(){
	if ($("#audioPlayer").hasClass("autoPaused") && (music_files.length > 0)) {
		$("#audioPlayer").removeClass('autoPaused');
		toggleAudio();
	}
});
$(window).blur(function() {
	autoPauseAudio();	
});


