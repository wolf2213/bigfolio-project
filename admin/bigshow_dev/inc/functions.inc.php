<?
function get_property($prop,$trueval=true,$falseval=false) {
	$q = "select * from properties where property_id='$prop'";
	$sel = mysql_query($q)
		or die(mysql_error());
	$row = mysql_fetch_array($sel,MYSQL_ASSOC);
	if ($row['property_value'] == 1) {
		return $trueval;
	} else if ($row['property_value'] == 1) {
		return $falseval;
	} else {
		return $row['property_value'];
	}
}
function get_template($name, $settings) {
	$q = "select * from templates where template_id = '$name'";
	$sel = mysql_query($q)
		or die(mysql_error());
	$row = mysql_fetch_array($sel, MYSQL_ASSOC);
	$contents = $row['contents'];
	// Set timestamp as string
	$settings['last_updated'] = date('M j Y',$settings['last_updated']);
	// Loop through settings and insert any necessary values
	// Settings should match fields in settings table enclosed by double brackets
	// Like '[[browser_title]]'
	while (list($key, $val) = each($settings)) {
		$find = '[['.$key.']]';
		$replace = $val;
		$contents = str_replace($find, $replace, $contents);
	}
	$fvalues = unserialize($settings['flash_vars']);
	$fvars = split(',',get_property('FLASH_VARS'));
	foreach($fvars as $var) {
		$find = '[['.$var.']]';
		$replace = $fvalues[$var];
		$contents = str_replace($find, $replace, $contents);
	}
	// Loop through properties and insert their values from the properties table
	$q = "select * from properties";
	$sel = mysql_query($q)
		or die(mysql_error());
	while ($row = mysql_fetch_array($sel, MYSQL_ASSOC)) {
		$find = '%%'.$row['property_id'].'%%';
		$replace = $row['property_value'];
		$contents = str_replace($find, $replace, $contents);
	}	
	// Now add custom Flash vars
	$var_output = "";
	$fvalues = unserialize($settings['flash_vars']);
	$fvars = split(',',get_property('FLASH_VARS'));
	foreach($fvars as $var) {
		$val = $fvalues[$var];
		$var_output .= "so.addVariable(\"$var\",\"$val\");\n";
	}
	$contents = str_replace('%%VARS%%',$var_output,$contents);
	// Stats code
	$contents = str_replace('%%STATS%%',$settings['stats_code'],$contents);
	return $contents;
}
function get_entry_page($s) {
    $file = './inc/entry.tmp.html';
    $entry = file_get_contents($file);
    return $entry;
}
function get_detect_page($s) {
    $file = './inc/detect.tmp.html';
    $entry = file_get_contents($file);
    return $entry;
}
function thumbnail($image,$thumb,$thumb_height) { 
	if (function_exists('imagecreatefromjpeg')) {
		$src_img = imagecreatefromjpeg($image); 
    	$origw=imagesx($src_img); 
    	$origh=imagesy($src_img); 
    	$new_h = $thumb_height;
		$diff = $origh/$new_h;
		$new_w = $origw/$diff; 
    	$dst_img = imagecreatetruecolor($new_w,$new_h); 
    	imagecopyresampled($dst_img,$src_img,0,0,0,0,$new_w,$new_h,imagesx($src_img),imagesy($src_img)); 
   		imagejpeg($dst_img, $thumb, 60); 
		imagedestroy($src_img);
		imagedestroy($dst_img);
	} 
    return true; 
} 
function fitimage($image,$w,$h) {
	if (function_exists('imagecreatefromjpeg')) {
		// Get size of image
		$size = getimagesize($image);
		$old_w = $size[0];
		$old_h = $size[1];
		// Find lowest denominator
		// based on image being landscape or portrait
		if ($old_w<$old_h) {
			// Image is portrait
			$new_h = $h;
			$scale = $new_h/$old_h;
			$new_w = round($old_w*$scale);
		} else {
			// Image is landscape
			$new_w = $w;
			$scale = $new_w/$old_w;
			$new_h = round($old_h*$scale);
		}
		$src_img = imagecreatefromjpeg($image); 
    	$dst_img = imagecreatetruecolor($new_w,$new_h); 
    	imagecopyresampled($dst_img,$src_img,0,0,0,0,$new_w,$new_h,imagesx($src_img),imagesy($src_img)); 
   		imagejpeg($dst_img, $image,70); 
		imagedestroy($src_img);
		imagedestroy($dst_img);
	} 
    return true; 
}
// Function for showing active tab (in row 2)
function is_active($m, $l) {
	if ($m == $l) {
		return "class=\"active\"";
	} else {
		return "";
	}
}
// Page functions
function get_settings($id) {
	if (!isset($id)) {
		$id=1;
	}
	$q = "select * from settings where settings_id = $id";
	$sel = mysql_query($q)
		or die(mysql_error());	
	$s = mysql_fetch_array($sel, MYSQL_ASSOC);
	return $s;
}
function save_visit() {
	include('classPHPSniff.php');
	// Saves the site visit to the visits table
	// Get browser and other info
	$url = addslashes($_SERVER['REQUEST_URI']);
	$ip = getenv("REMOTE_ADDR");
	$browser = new phpSniff();
	$br = $browser->property('long_name');
	$ver = $browser->property('version');
	$platform = $browser->property('platform');
	$browser_raw = $_SERVER['HTTP_USER_AGENT'];
	$rawref = getenv("HTTP_REFERER");
	$parseRef = parse_referer($rawref);
	$ref = $parseRef[0];
	$search = $parseRef[1];
	$q = "insert into 
		visits(date,url,ip,browser,browser_version,platform,raw_user_agent,raw_referrer,referrer,search_string) 
		values (Now(),'$url','$ip','$br','$ver','$platform','$browser_raw','$rawref','$ref','$search')";
	$ins = mysql_query($q)
		or die(mysql_error());	
	return true;
}
function parse_referer($referer) {
	$return = array();
	// Get url (up until ?)
	$qStart = strpos($referer,'?');
	if ($qStart !== false) {
		$return[]=substr($referer,0,$qStart);
	} else {
		$return[]=$referer;
	}
	// Get position of q= string
	$searchStart = strpos($referer,'q=');
	if ($searchStart !== false) {
		$searchStart += 2;
		$searchEnd = strpos($referer,'&',$searchStart);
		if ($searchEnd === false) {
			$return[] = substr($referer,$searchStart);
		} else {
			$return[] = substr($referer,$searchStart,($searchEnd-$searchStart));
		}
	} else $return[] = '';
	return $return;
}
function get_sub_pages($pid=false) {
	if ($pid === false) {
		$q = "select * from pages order by parent_id, order_num";
	} else {
		$q = "select * from pages where parent_id='$pid' order by order_num";
	}
	$sel = mysql_query($q)
		or die(mysql_error());
	$pgArr = array();
	while ($pg = mysql_fetch_array($sel, MYSQL_ASSOC)) {
		// Markdown content
		$pg['page_content_html'] = '';
		$textile = new Textile;
		// $pg['page_content_html'] = $textile->TextileThis($pg['page_content']);
		// $pg['page_content_html'] = str_replace("\t","",$pg['page_content_html']);
		$pg['page_content_html'] = xml_entities($pg['page_content']);
		// Check for Pictage link
		if ($pg['add_proofing'] == 1) {
			$pset = get_proof_settings();
			if ($pset['proofing'] == 'PICTAGE') {
				$pg['page_content_html'] .= get_pictage($pset['proofing_id']);			
			}
			if ($pset['proofing'] == 'EVENTPIX') {
				$pg['page_content_html'] .= get_eventpix($pset['proofing_id']);			
			}
			if ($pset['proofing'] == 'SMUGMUG') {
			  $pg['page_content_html'] .= get_smugmug($pset['proofing_id']);
			}
		}
		if ($pg['add_pictage']==1) {
			$pg['page_content'] = $pg['page_content'] . get_pictage();
		}
		if ($pg['add_eventpix']==1) {
			$pg['page_content'] = $pg['page_content'] . get_eventpix();
		}
		$pgArr[] = $pg;
	}	
	return $pgArr;
}
function get_smugmug($pcode) {
  include_once './lastRSS.php';
  // create lastRSS object
  $rss = new lastRSS;
  // setup transparent cache
  $rss->cache_dir = './cache';
  $rss->cache_time = 3600; // one hour
  $body = '';
  if ($rs = $rss->get("http://api.smugmug.com/hack/feed.mg?Type=nickname&Data={$pcode}&format=rss200")) {
    $body = '<br><br>';
    $numItems = count($rs['items']);
    foreach($rs['items'] as $item) {
      $body .= "<p><a href=\"{$item['link']}\">{$item['title']}</a></p><br>\n";
    }
  }  
  return $body;
}
function get_pictage($pcode) {
	$pictURL = "http://external.pictage.com/external/PHTINTEG?photog=".$pcode;
	$body = '<br><br>';
	// create a new cURL resource
	$ch = curl_init();
	// set URL and other appropriate options
	curl_setopt($ch, CURLOPT_URL, $pictURL);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	// grab URL and pass it to the browser
	$body = curl_exec($ch);
	// close cURL resource, and free up system resources
	curl_close($ch);
	$body = explode('<title>Pictage</title>', $body);
	return $body[1];
}
function get_nextproof($ncode) {
	$pictURL = $ncode . '/api/galleries.html';
	$body = '<br><br>';
	// create a new cURL resource
	$ch = curl_init();
	// set URL and other appropriate options
	curl_setopt($ch, CURLOPT_URL, $pictURL);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	// grab URL and pass it to the browser
	$body = curl_exec($ch);
	// close cURL resource, and free up system resources
	curl_close($ch);
	$body = str_replace('">', '" target="_blank">', $body);
	return $body;
}
function get_eventpix($epcode) {
	$pictURL = "http://ws.bighead.net/eventpix/getEvents.taf?userid=".$epcode;
	$body = '<br><br>';
	$xfile = '';
	// create a new cURL resource
	$ch = curl_init();
	// set URL and other appropriate options
	curl_setopt($ch, CURLOPT_URL, $pictURL);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	// grab URL and pass it to the browser
	$xfile = curl_exec($ch);
	// close cURL resource, and free up system resources
	curl_close($ch);
	// Parse xml contents
	$p = xml_parser_create();
	xml_parse_into_struct($p, $xfile, $vals, $tags);
	xml_parser_free($p);
	$names = array();
	$links = array();
	foreach ($tags as $key=>$val) {		
		if ($key == 'NAME') {
			foreach($val as $index) {
				$names[] = $vals[$index]['value'];
			}
		} else if ($key == 'LINK') {
			foreach($val as $index) {
				$links[] = $vals[$index]['value'];
			}
		}
	}
	for ($i=0; $i<count($names); $i++) {
		$contents .= "<a href=\"$links[$i]\" target=\"_blank\">$names[$i]</a><br>";
	}
	return $contents;
}
function get_proof_settings() {
	$q="select proofing, proofing_id from settings where settings_id=1";
	$sel = mysql_query($q)
		or die(mysql_error());
	$row=mysql_fetch_array($sel, MYSQL_ASSOC);
	return $row;
}
function get_categories() {
	$q = "select * from categories order by order_num asc";
	$selg = mysql_query($q)
		or die(mysql_error());
	$gArr = array();
	while ($g = mysql_fetch_array($selg, MYSQL_ASSOC)) {
		$gArr[] = $g;
	}	
	return $gArr;
}
function get_galleries_by_category ($cid) {
	$q = "select * from galleries where category_id = '$cid' order by order_num asc";
	$selg = mysql_query($q)
		or die(mysql_error());
	$gArr = array();
	while ($g = mysql_fetch_array($selg, MYSQL_ASSOC)) {
		$gArr[] = $g;
	}	
	return $gArr;
}
function get_gallery_files($gid) {
	$q = "select * from gallery_images where gallery_id='$gid' order by order_num";
	$sel = mysql_query($q)
		or die(mysql_error());
	$pgArr = array();
	while ($pg = mysql_fetch_array($sel, MYSQL_ASSOC)) {
		$pgArr[] = $pg;
	}	
	return $pgArr;
}
function parse_camel_case($string){
	$split = preg_split("/(?<=[a-z]) (?=[A-Z])/x", $string);
	return implode(' ',$split);
}
function xml_entities($str) {
    $xml = array('&#34;','&#38;','&#38;','&#60;','&#62;','&#160;','&#161;','&#162;','&#163;','&#164;','&#165;','&#166;','&#167;','&#168;','&#169;','&#170;','&#171;','&#172;','&#173;','&#174;','&#175;','&#176;','&#177;','&#178;','&#179;','&#180;','&#181;','&#182;','&#183;','&#184;','&#185;','&#186;','&#187;','&#188;','&#189;','&#190;','&#191;','&#192;','&#193;','&#194;','&#195;','&#196;','&#197;','&#198;','&#199;','&#200;','&#201;','&#202;','&#203;','&#204;','&#205;','&#206;','&#207;','&#208;','&#209;','&#210;','&#211;','&#212;','&#213;','&#214;','&#215;','&#216;','&#217;','&#218;','&#219;','&#220;','&#221;','&#222;','&#223;','&#224;','&#225;','&#226;','&#227;','&#228;','&#229;','&#230;','&#231;','&#232;','&#233;','&#234;','&#235;','&#236;','&#237;','&#238;','&#239;','&#240;','&#241;','&#242;','&#243;','&#244;','&#245;','&#246;','&#247;','&#248;','&#249;','&#250;','&#251;','&#252;','&#253;','&#254;','&#255;');
    $html = array('&quot;','&amp;','&amp;','&lt;','&gt;','&nbsp;','&iexcl;','&cent;','&pound;','&curren;','&yen;','&brvbar;','&sect;','&uml;','&copy;','&ordf;','&laquo;','&not;','&shy;','&reg;','&macr;','&deg;','&plusmn;','&sup2;','&sup3;','&acute;','&micro;','&para;','&middot;','&cedil;','&sup1;','&ordm;','&raquo;','&frac14;','&frac12;','&frac34;','&iquest;','&Agrave;','&Aacute;','&Acirc;','&Atilde;','&Auml;','&Aring;','&AElig;','&Ccedil;','&Egrave;','&Eacute;','&Ecirc;','&Euml;','&Igrave;','&Iacute;','&Icirc;','&Iuml;','&ETH;','&Ntilde;','&Ograve;','&Oacute;','&Ocirc;','&Otilde;','&Ouml;','&times;','&Oslash;','&Ugrave;','&Uacute;','&Ucirc;','&Uuml;','&Yacute;','&THORN;','&szlig;','&agrave;','&aacute;','&acirc;','&atilde;','&auml;','&aring;','&aelig;','&ccedil;','&egrave;','&eacute;','&ecirc;','&euml;','&igrave;','&iacute;','&icirc;','&iuml;','&eth;','&ntilde;','&ograve;','&oacute;','&ocirc;','&otilde;','&ouml;','&divide;','&oslash;','&ugrave;','&uacute;','&ucirc;','&uuml;','&yacute;','&thorn;','&yuml;');
    $str = str_replace($html,$xml,$str);
    $str = str_replace($html,$xml,$str);
    return $str;
}
function is_iphone() {
	$ua = strtolower($_SERVER['HTTP_USER_AGENT']);
	$isIphone = false;
	if (strpos($ua, 'iphone') !== false) {
		$isIphone = true;
	}
	return $isIphone;
}
function is_mobile() {
	$isMobile = false;
	$isBot = false;
	
	$op = strtolower($_SERVER['HTTP_X_OPERAMINI_PHONE']);
	$ua = strtolower($_SERVER['HTTP_USER_AGENT']);
	$ac = strtolower($_SERVER['HTTP_ACCEPT']);
	$ip = $_SERVER['REMOTE_ADDR'];
	
	$isMobile = strpos($ac, 'application/vnd.wap.xhtml+xml') !== false
	        || $op != ''
	        || strpos($ua, 'sony') !== false 
	        || strpos($ua, 'symbian') !== false 
	        || strpos($ua, 'nokia') !== false 
	        || strpos($ua, 'samsung') !== false 
	        || strpos($ua, 'mobile') !== false
	        || strpos($ua, 'windows ce') !== false
	        || strpos($ua, 'epoc') !== false
	        || strpos($ua, 'opera mini') !== false
	        || strpos($ua, 'nitro') !== false
	        || strpos($ua, 'j2me') !== false
	        || strpos($ua, 'midp-') !== false
	        || strpos($ua, 'cldc-') !== false
	        || strpos($ua, 'netfront') !== false
	        || strpos($ua, 'mot') !== false
	        || strpos($ua, 'up.browser') !== false
	        || strpos($ua, 'up.link') !== false
	        || strpos($ua, 'audiovox') !== false
	        || strpos($ua, 'blackberry') !== false
	        || strpos($ua, 'ericsson,') !== false
	        || strpos($ua, 'panasonic') !== false
	        || strpos($ua, 'philips') !== false
	        || strpos($ua, 'sanyo') !== false
	        || strpos($ua, 'sharp') !== false
	        || strpos($ua, 'sie-') !== false
	        || strpos($ua, 'portalmmm') !== false
	        || strpos($ua, 'blazer') !== false
	        || strpos($ua, 'avantgo') !== false
	        || strpos($ua, 'danger') !== false
	        || strpos($ua, 'palm') !== false
	        || strpos($ua, 'series60') !== false
	        || strpos($ua, 'palmsource') !== false
	        || strpos($ua, 'pocketpc') !== false
	        || strpos($ua, 'smartphone') !== false
	        || strpos($ua, 'rover') !== false
	        || strpos($ua, 'ipaq') !== false
	        || strpos($ua, 'au-mic,') !== false
	        || strpos($ua, 'alcatel') !== false
	        || strpos($ua, 'ericy') !== false
	        || strpos($ua, 'up.link') !== false
	        || strpos($ua, 'vodafone/') !== false
	        || strpos($ua, 'wap1.') !== false
	        || strpos($ua, 'wap2.') !== false;
	
	return $isMobile? true:false;
}
function get_vcategories() {
	$q = "select * from v_categories order by order_num asc";
	$selg = mysql_query($q)
		or die(mysql_error());
	$gArr = array();
	while ($g = mysql_fetch_array($selg, MYSQL_ASSOC)) {
		$gArr[] = $g;
	}	
	return $gArr;
}
function get_vgalleries_by_vcategory ($cid) {
	$q = "select * from v_galleries where category_id = '$cid' order by order_num asc";
	$selg = mysql_query($q)
		or die(mysql_error());
	$gArr = array();
	while ($g = mysql_fetch_array($selg, MYSQL_ASSOC)) {
		$gArr[] = $g;
	}	
	return $gArr;
}
function get_video_files($gid) {
	$q = "select * from videos where gallery_id='$gid' order by order_num";
	$sel = mysql_query($q)
		or die(mysql_error());
	$pgArr = array();
	while ($pg = mysql_fetch_array($sel, MYSQL_ASSOC)) {
		$pgArr[] = $pg;
	}	
	return $pgArr;
}
function check_for_deluxe_calendar($db_name) {
	$deluxeCalendar = false;
	$result = mysql_list_tables($db_name) or die(mysql_error());
	while ($row = mysql_fetch_row($result)) {
		if($row[0]=='calendar') {
			$deluxeCalendar = true;
		}
	}
	return $deluxeCalendar;
}
function get_calendar_events() {
	$q = "select * from calendar where id >= 0 order by start_date, start_time asc";
	$sel = mysql_query($q)
		or die(mysql_error());	
	$eventsArr = array();
	while ($e = mysql_fetch_array($sel, MYSQL_ASSOC)) {
		$eventsArr[] = $e;
	}
	return $eventsArr;
}
?>