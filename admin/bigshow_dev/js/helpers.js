jQuery.log = function( text ){
    if( (window['console'] !== undefined) ){
        console.log( text );
    }
}

$.fn.getVal = function(prop) {
	return Number($(this).css(prop).substr(0, $(this).css(prop).length-2));
}