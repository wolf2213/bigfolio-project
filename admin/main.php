<?
if ($_SESSION['server'] == 'bigshow') {
	
} else {
	$template = get_property('TEMPLATE');
	$templateType = get_property('TEMPLATE_TYPE'); 
}

$urlArr = explode("/",$_SESSION['user']['url']);
$domain = $urlArr[2]; 
$liveCheck = ""; //file_get_contents("http://dnscheck.bigfolio.com/?host=".$domain);
echo '<!-- LIVE CHECK: '.$liveCheck . '-->'; 
error_reporting(0);
?>
<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7 ]> <html class="no-js ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
       Remove this if you use the .htaccess -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>BIG Folio Admin 4</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile viewport optimized: j.mp/bplateviewport -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Place favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
  <link rel="shortcut icon" href="/favicon.ico">
  <link rel="apple-touch-icon" href="/apple-touch-icon.png">


  <!-- CSS: implied media="all" -->
  <link rel="stylesheet" href="css/style.css?v=2">
  <link rel="stylesheet" href="css/colorpicker.css?v=2">
  <link rel="stylesheet" href="css/bf4.css?v=2">
  <link rel="stylesheet" href="js/plugins/uploadify-v2.1.4/uploadify.css">
  <link rel="stylesheet" href="js/plugins/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
  <!--[if lte IE 9 ]>
  <link rel="stylesheet" href="css/ie.css?v=2">
  <![endif]-->
  <link rel="stylesheet" href="css/custom-theme/jquery-ui-1.8.16.custom.css?v=2">
  <link rel="stylesheet" type="text/css" href="css/uploadifive.css">

  <!-- Uncomment if you are specifically targeting less enabled mobile browsers
  <link rel="stylesheet" media="handheld" href="css/handheld.css?v=2">  -->

  <!-- All JavaScript at the bottom, except for Modernizr which enables HTML5 elements & feature detects -->
  <script src="js/libs/modernizr-1.7.min.js"></script>

</head>

<body>

  <div id="container">
	<div id="navigation">
    	<div id="logo">
			<img src="img/bf_logo_2014.png" alt="BIG Folio">
		</div>
		<? include_once('menu.php'); ?>
	</div>
	<div class="save-preview">
		<? if ($_SESSION['server'] == 'bigshow') { ?>
		<button id="saveBtn">Save</button>
		<button id="logoutBtn">Log Out</button>
		<? } else { ?>
       
        <a href="https://bigfolio2.bigfolio.com/#/welcome">
        	<button id="startBtn">Get Started</button>
        </a>
        
		<button id="saveBtn">Save</button>
		<button id="viewBtn">View Site</button>
		<a href="http://bigfolio.com/payment/" target="_blank">
			<button id="makeLiveBtn">Go Live</button> 
		</a>
		<button id="logoutBtn">Log Out</button>
		<? } ?>
	</div> 
    <div id="main" role="main">
		<div id="content">
		</div>
		<div id="loaderSpinner">
		</div> 
    </div>
	
    <footer>

    </footer>
<div style="clear:both;"></div>
  </div> <!--! end of #container -->
  
  <div style="clear:both;"></div>
  <div id="saveOverlay">
	<div class="saveMsg">
		Saving
	</div>
  </div> 
  <!-- JavaScript at the bottom for fast page loading -->

  <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if necessary -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
  <script>window.jQuery || document.write("<script src='js/libs/jquery-1.6.2.min.js'>\x3C/script>")</script>
  <script src="js/jquery.uploadifive.js" type="text/javascript"></script>



  <script>
	var u = "<?=$_SESSION['user']['email']?>";
	var user = "<?=$_SESSION['user']['db_user']?>"; 
	var p = "<?=$_SESSION['user']['password']?>";
	var tempURL = "<?=$_SESSION['user']['temp_url']?>";
	var serverURL = "<?=$_SESSION['user']['server_url']?>";
	var overrideURL = "<?=$_SESSION['user']['temp_url_override']?>"; 
	var previewURL = "<?=$_SESSION['user']['temp_url']?>"; 
	var adminURL = "<?=SERVER?>"; //"http://sandberg.bigfolio4.com/";
	var template = "<?=$template?>";
	var templateType = "<?=$templateType?>";
  </script>

  <!-- scripts concatenated and minified via ant build script
  <script src="js/plugins.js"></script> -->
  <!-- <script src="js/plugins/jquery.dimensions.min.js"></script> -->
  <script src="js/plugins/jquery.delegate.js"></script>
  <script src="js/plugins/jquery.easing.1.2.js"></script>
  <script src="js/plugins/jquery.address-1.4.min.js"></script>
  <script src="js/plugins/jquery-ui-1.8.16.custom.min.js"></script>
  <script src="js/plugins/jquery.tooltip.min.js"></script> 
  <script src="js/plugins/colorpicker/colorpicker.js"></script>
  <script src="js/plugins/colorpicker/eye.js"></script>
  <script src="js/plugins/colorpicker/layout.js"></script>
  <script src="js/plugins/colorpicker/utils.js"></script>
  <script src="js/plugins/jquery-fieldselection.min.js"></script>  

  <!--<script type="text/javascript" src="js/plugins/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>-->
  <script type="text/javascript" src="js/plugins/fancybox/jquery.fancybox-1.3.4.pack.js"></script>

  <script src="js/plugins/uploadify-v2.1.4/jquery.uploadify.v2.1.4.min.js"></script>
  <script src="js/plugins/uploadify-v2.1.4/swfobject.js"></script>

  <script src="js/plugins/jquery.scrollTo-1.4.2-min.js"></script>

  <script src="js/helpers.js"></script>
  <script src="js/script.js"></script> 
  
 

  <!-- end scripts-->


  <!--[if lt IE 7 ]>
    <script src="js/libs/dd_belatedpng.js"></script>
    <script>DD_belatedPNG.fix("img, .png_bg"); // Fix any <img> or .png_bg bg-images. Also, please read goo.gl/mZiyb </script>
  <![endif]-->


<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33048620-1']);
  _gaq.push(['_setDomainName', 'bigfolio4.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<div style="clear:both;"></div>
</body>
</html>
