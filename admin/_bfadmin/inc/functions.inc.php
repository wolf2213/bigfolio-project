<?
function cleanfile($name) {
	$name = preg_replace("/[^a-zA-Z0-9\-_\.]+/", "-", $name);
	return preg_replace("/[-]+/", "-", $name);
}

function thumbnail($image,$thumb,$width,$height,$crop=false) {
	if (function_exists('imagecreatefromjpeg')) {
		$ext = explode('.', $image);
		if ($ext[count($ext)-1] == 'png' || $ext[count($ext)-1] == 'PNG') {
			$src_img = imagecreatefrompng($image); 
		} else {
			$src_img = imagecreatefromjpeg($image); 
		}
		
    	$origw=imagesx($src_img); 
    	$origh=imagesy($src_img); 

		$diff = $origw / $width;
		
		if ($origh / $diff > $height) {
			$new_h = $height;
			$diff = $origh / $height;
			$new_w = $origw / $diff;
		} else {
			$new_w = $width;
			$new_h = $origh / $diff;
		}
		

		if ($crop) {
			$new_w = $width*2;
			$new_h = $height*2;
			$dst_img = imagecreatetruecolor($width,$height);
		} else {
			$dst_img = imagecreatetruecolor($new_w, $new_h);
		}
		if ($ext[count($ext)-1] == 'png' || $ext[count($ext)-1] == 'PNG') {
			imagealphablending($dst_img, false);
		}
    	imagecopyresampled($dst_img,$src_img,0,0,0,0,$new_w,$new_h,imagesx($src_img),imagesy($src_img)); 
		if ($ext[count($ext)-1] == 'png' || $ext[count($ext)-1] == 'PNG') {
			imagesavealpha($dst_img, true);
			imagepng($dst_img, $thumb, 9, NULL);
		} else {
   			imagejpeg($dst_img, $thumb, 85);
		}
		imagedestroy($src_img);
		imagedestroy($dst_img);
	} 
    return true; 
}  
?>