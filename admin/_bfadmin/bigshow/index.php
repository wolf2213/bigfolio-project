<?   

include_once('../inc/db.inc.php'); 
include('../inc/functions.inc.php');

$siteSettings = get_settings();

/* bigshow stuff */ 
function get_bigshow_by_url($url) {
	$q = "select * from bigshow_slideshows where url='$url'";
	$sel = mysql_query($q)
		or die(mysql_error());
	$row = mysql_fetch_array($sel,MYSQL_ASSOC);
	return $row;
}
function get_bigshow($sid) {
	$q = "select * from bigshow_slideshows where id='$sid'";
	$sel = mysql_query($q)
		or die(mysql_error());
	$row = mysql_fetch_array($sel,MYSQL_ASSOC);
	return $row;
} 
function get_bigshow_images($sid) {
	$q = "select * from bigshow_images where slideshow_id = '$sid' order by order_num asc";
	$sel = mysql_query($q)
		or die(mysql_error());
	$r = array();
	while ($row = mysql_fetch_array($sel, MYSQL_ASSOC)) {
		$r[] = $row;
	}
	return $r;
} 
function getNameAndExtension($file) {
	$arr = explode('.', $file);
	$name = ''; 
	for ($i=0; $i<count($arr)-1; $i++) {
		$name .= $arr[$i]; 
		if ($i<count($arr)-2) {
			$name .= ".";
		}
	} 
	$nameArr = array(); 
	$nameArr[0] = $name;
	$nameArr[1] = $arr[count($arr)-1];                
	return $nameArr;
}

//
// get bigshow data 
if (!isset($_REQUEST['s'])) {
	die('<!-- no slideshow setting -->');
}
if (is_int($_REQUEST['s'])) {  
	$ss = get_bigshow(intval($_REQUEST['s'])); // get slideshow by id number if it is an integer
} else {
	$ss = get_bigshow_by_url($_REQUEST['s']); // otherwise get slideshow by url
}
$settings = json_decode($ss['settings']);
$images = get_bigshow_images($ss['id']); 

if (!isset($settings->theme)) {
	$settings->theme = "t1";
}
                                                    
if ($_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == 'macbook') { 
	$sourceHost = "../"; //"http://localhost/BIGFolio/_bf_admin4/"                             
} else {
	$sourceHost = "http://cdn.bigfoliobigshow.com.s3.amazonaws.com/source/";
} 
//$sourceHost = "../";

$storageID = $ss['storage_id']; 
$imgStorage = "http://cdn.bigfoliobigshow.com.s3.amazonaws.com/users/" . $storageID{0} . '/' . $storageID{1} . '/' . $storageID{2} . '/' . $storageID . '/' . $ss['id'] . '/'; 

$musicFiles = array();
if (isset($settings->music_files) && $settings->music_files != "") {
	$musicFiles = explode(",", $settings->music_files);
} 

if (isset($_POST['validate'])) { 
	// check password 
	$thePass = $_POST['password'];            
	$s = $_REQUEST['s']; 
	if ($settings->password == $thePass) {
		die("1"); //" $settings->password, $thePass, $s");
	} else {
		die("0"); //" $settings->password, $thePass, $s");
	}	
} else {
	if ($settings->password != "") {
		$isProtected = "true";
	} else {
		$isProtected = "false";
	} 
}

?>
<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7 ]> <html class="no-js ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
       Remove this if you use the .htaccess -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title><?=$ss['name']?></title>

  <!-- Mobile viewport optimized: j.mp/bplateviewport -->
  <meta name="viewport" content="width=960">

  <!-- Place favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
  <link rel="shortcut icon" href="/favicon.ico">

  <!-- CSS: implied media="all" -->
  <link rel="stylesheet" href="<?=$sourceHost . $settings->theme?>/css/style.css?v=2">
  <style type="text/css">
	<!--
	body {
		background-color: <?=$settings->body_color?>;
		color: <?=$settings->text_color?>;
		font-family: Helvetica, Arial, sans-serif;
		font-weight: 100;                         
	}
	a, a:visited {
	  	color: <?=$settings->link_color?>;
	}
	a:hover, a:visited:hover {
	  	color: <?=$settings->link_hover_color?>;
	}
	div#slideshowTitle {
		color: <?=$settings->title_color?>;
	}
	div#bigshowFooter {
		background: <?=$settings->footer_color?>;
	}
	div.footerBtn {
		color: <?=$settings->footer_color?>;
		font-weight: lighter;
		background: <?=$settings->link_color?>;
	}
	.galImg {
		border: 1px solid <?=$settings->border_color?>;
	}
	#BFPasswordBox {
		background: <?=$settings->footer_color?>;
		color: <?=$settings->text_color?>;
	}
	-->
  </style>

  <!-- Uncomment if you are specifically targeting less enabled mobile browsers 
  <link rel="stylesheet" media="handheld" href="<?=$sourceHost . $settings->theme?>/css/handheld.css?v=2">-->  

  <!-- All JavaScript at the bottom, except for Modernizr which enables HTML5 elements & feature detects -->
  <script src="<?=$sourceHost?>js/libs/modernizr-1.7.min.js"></script>

</head>

<body>

  <div id="container">
    <header>
   	<? if ($settings->show_logo == 1) { ?>
    <div id="bigshowLogo">
	   <a href="../"><img src="../images/<?=$siteSettings['logo_file']?>" alt="" /></a>
	</div> 
	<? } ?>
	<? if ($settings->show_title == 1) { ?>
	<div id="slideshowTitle"> 
	   <h1><?=$ss['name']?></h1>
	</div> 
	<? } ?>
    </header>
    <div id="main" role="main">
	 	<div id="leftNav">
			<canvas id="leftArrow">
			</canvas>
		</div> 
		<div id="rightNav"> 
			<canvas id="rightArrow">
			</canvas> 
		</div>
		<div id="bigshowImages">
		
		</div>
		<div id="bigshowCaption">
		</div> 
		<div id="loadingIcon">
			<canvas id="square0" class="loaderSquare"></canvas>
			<canvas id="square1" class="loaderSquare"></canvas>
			<canvas id="square2" class="loaderSquare"></canvas>
			<canvas id="square3" class="loaderSquare"></canvas>
			<canvas id="square4" class="loaderSquare"></canvas>
			<canvas id="square5" class="loaderSquare"></canvas>
			<canvas id="square6" class="loaderSquare"></canvas>
			<canvas id="square7" class="loaderSquare"></canvas>
		</div> 
		<? if ($ss['footer'] != "" || $settings->disqus_shortname != "") { ?>
		<div id="footerText"> 
			<?=$ss['footer'];?> 
			<? if ($settings->disqus_shortname != "") { ?>
				<div id="disqus_thread"></div>
				<script type="text/javascript">
				    /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
				    var disqus_shortname = '<?=$settings->disqus_shortname?>'; // required: replace example with your forum shortname

				    /* * * DON'T EDIT BELOW THIS LINE * * */
				    (function() {
				        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
				        dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';
				        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
				    })();
				</script>
				<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
				<a href="http://disqus.com" class="dsq-brlink">blog comments powered by <span class="logo-disqus">Disqus</span></a>
			<? } ?>
		</div>
		<? } ?>
	</div>
    <footer>
    	<div id="bigshowFooter">
			<div id="thumbPrev" class="footerBtn"> 
				<canvas id="thumbPrevArrow">
				</canvas>
			</div>
			<div id="bigshowPreload">
			</div>
			<div id="bigshowThumbs">
				<ul>
				<? $count = 0; ?> 
				<? foreach($images as $img) { ?>
					<?
					$count++; 
					$fileParts = getNameAndExtension($img['image_file']);
					$thumbSize = "thumb"; 
					?>
					<li <? if ($count == 1) { ?> class="firstThumb" <? } else if ($count == count($images)) { ?> class="lastThumb" <? } ?>>
						<a href="#/<?=$count?>" title="<?=$img['image_file']?>" onclick="thumbClick();">
							<img src="<?=$imgStorage . $fileParts[0]?>_<?=$thumbSize?>.<?=$fileParts[1]?>" alt="<?=$img['image_caption'];?>" />
						</a>
					</li>
				<? } ?>	
				</ul> 
			</div>
			<div id="thumbNext" class="footerBtn">
				<canvas id="thumbNextArrow">
				</canvas>
			</div> 
			
			<div id="slideshowBtn" class="footerBtn">
				<canvas id="slideshowIcon"></canvas>
<!--			<canvas id="pauseIcon"></canvas>
				<canvas id="playIcon"></canvas> -->
			</div>
			
			<? if (count($musicFiles) > 0) { ?>
			<div id="musicBtn" class="footerBtn">
				<div class="musicIcon">
				&#x266A;                
				</div.>
			</div>
			<? } ?>
			
		</div>
    </footer>
  <div id="audioPlayer"></div>
  </div> <!--! end of #container -->

  <div id="BFPasswordBox">
	<div class="closeBtn"> 
	<canvas id="passwordCloseBtn">
	</canvas>             
	</div>
	<div class="msg">Enter the password for this gallery.</div>
	<br>
	<form action="javascript:validateSlideshow();" name="slideshowValidator">
	<input type="password" name="galPass" id="bfGalPass" class="bfFormInput"/>
	<input type="submit" id="bfGalPassSumbit" /> 
	</form>
  </div>


  <!-- JavaScript at the bottom for fast page loading -->

  <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if necessary -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.js"></script>
  <script>window.jQuery || document.write("<script src='<?=$sourceHost?>js/libs/jquery-1.6.2.min.js'>\x3C/script>")</script>


  <!-- scripts concatenated and minified via ant build script-->
  <script src="<?=$sourceHost ?>js/helpers.js"></script>
  <script src="<?=$sourceHost . $settings->theme?>/js/plugins.js"></script>
  <script src="<?=$sourceHost . $settings->theme?>/js/script.js"></script>
  <!-- end scripts-->


  <!--[if lt IE 7 ]>
    <script src="<?=$sourceHost?>js/libs/dd_belatedpng.js"></script>
    <script>DD_belatedPNG.fix("img, .png_bg"); // Fix any <img> or .png_bg bg-images. Also, please read goo.gl/mZiyb </script>
  <![endif]-->

   
  <script type="text/javascript">
  // <![CDATA[   

    var ssID               = <?=$ss['id']?>; 
	var ssURL              = "<?=$ss['url']?>";
	var isProtected        = <?=$isProtected?>;

	var sourceHost         = "<?=$sourceHost?>";
	var sourceTheme        = "<?=$settings->theme?>";
	
	var slideshowStorage   = "<?=$imgStorage?>";
	
 	var slideshow_speed    = <?=$settings->slideshow_speed?>; 

	var music_files        = new Array();
	<? foreach ($musicFiles as $f) { ?>
	music_files.push("<?=$f?>");
	<? } ?>
	
	var body_color         = "<?=$settings->body_color?>";
	var text_color         = "<?=$settings->text_color?>";
	var link_color         = "<?=$settings->link_color?>";
	var link_hover_color   = "<?=$settings->link_hover_color?>";
	var title_color        = "<?=$settings->title_color?>"; 
	var footer_color       = "<?=$settings->footer_color?>";
	var border_color       = "<?=$settings->border_color?>";
	
	var show_filenames     = <?=$settings->show_filenames?>;

  // ]]>
  </script>
</body>
</html>