<?
// Include files
include('../../inc/db.inc.php');
include('../../inc/define.php');
include('../../inc/functions.inc.php');
session_start();
authorize_user();

$name = $_POST['name'];
$biz = $_POST['bizname'];
$em = $_POST['email'];
$ph = $_POST['phone'];
$mob = $_POST['mobile_phone'];
$mdom = $_POST['mobile_domain'];
$mprov = $_POST['mobile_provider'];
$add = $_POST['address'];
$proof = $_POST['proof'];
$proofid = $_POST['proof_id'];
if ($proof == 'NEXTPROOF' && substr($proofid,0,7) != 'http://') {
	$proofid = 'http://'.$proofid;
}
$copyright = $_POST['copyright'];
$email_a_friend = $_POST['email_a_friend'];
$esubj = $_POST['email_subj'];
$emsg = $_POST['email_msg'];
$q = "update settings set owner_name = '$name', business_name = '$biz', 
	  email = '$em', phone = '$ph', mobile_phone = '$mob', mobile_domain = '$mdom', mobile_provider = '$mprov', 
	  street_address = '$add', proofing = '$proof', proofing_id = '$proofid', copyright = '$copyright', email_a_friend = '$email_a_friend', email_subj = '$esubj', email_msg = '$emsg'";
if (isset($_POST['social_media'])) {
	$smedia = $_POST['social_media'];
	$q .= ", social_media = '$smedia'";
}
$q .= " where settings_id = 1";
$upd = mysql_query($q)
	or die(mysql_error());
// 
if (isset($_POST['facebook_button']) || isset($_POST['twitter_button'])) {
	//
	// see if original exists
	$t = get_source_template('MAIN_ORIGINAL');
	if (!is_array($t)) {
	   	$t = get_source_template('MAIN_PAGE'); 
		$templateID = $t['template_id'];
		$templateContent = $t['contents'];  
		//
		// save a backup of the original 
		$d = date("U"); //date("m/d/Y-U");
		create_new_template('MAIN_ORIGINAL', 'HTML', addslashes($templateContent)); 
	} else {
		$templateID = 'MAIN_PAGE';
		$templateContent = $t['contents'];
	} 
	//
	// first add opaque window mode 
	$templateArr = explode('so.write("container");', $templateContent);   
	$soAdd = 'so.addParam("wmode", "opaque");';
	$soAdd .= "\n";
	$soAdd .= 'so.write("container");';
	$templateContent = implode($soAdd, $templateArr);
	//
	// next add css 
	$templateArr = explode('#container {', $templateContent);   
	$heightOffset = "-30px;";                                 
	if (isset($_POST['facebook_button']) && isset($_POST['twitter_button'])) {  
		$heightOffset = "-50px";
	}
	//
	if ($_POST['share_align'] == 'TL') {  
		$css = "#shareButtons { position:absolute; width:100px; margin-top:10px; margin-left:10px; }";
	} else if ($_POST['share_align'] == 'TR') { 
		$css = "#shareButtons { position:absolute; width:100px; left:100%; margin-top:10px; margin-left:-110px; }";
	} else if ($_POST['share_align'] == 'BL') {
		$css = "#shareButtons { position:absolute; width:100px; top:100%; margin-top:$heightOffset; margin-left:10px; }";
	} else {
		$css = "#shareButtons { position:absolute; width:100px; top:100%; left:100%; margin-top:$heightOffset; margin-left:-110px; }";
	}
	$css .= "\n#container {"; 
	$templateContent = implode($css, $templateArr);
	// 
	// now add the new div with the share buttons
	$templateArr = explode('<div id="container">', $templateContent);
	$shareURL = $_POST['share_url']; 
	$shareCode = '<div id="shareButtons">';
	if (isset($_POST['facebook_button'])) {
		$shareCode .= "\n";
		$shareCode .= '<script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like href="'.$shareURL.'" layout="button_count" show_faces="true" width="450" font=""></fb:like>';  
		$shareCode .= "<br /><!-- testing -->";
	}
	if (isset($_POST['twitter_button'])) {   
		$shareCode .= "\n";
		$shareCode .= '<a href="http://twitter.com/share" class="twitter-share-button" data-url="'.$shareURL.'" data-count="horizontal">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>';
	}
	$shareCode .= "\n</div>\n";
	$shareCode .= '<div id="container">';
	$templateContent = implode($shareCode, $templateArr);
	//
	// now save
	save_template($templateID,addslashes($templateContent));
	//
	// save settings for profile page
	$shareSettings = array();
	if (isset($_POST['facebook_button'])) {
		$shareSettings['facebook'] = 1;
	} else {
		$shareSettings['facebook'] = 0;
	}
	if (isset($_POST['twitter_button'])) {
		$shareSettings['twitter'] = 1;
	} else {
		$shareSettings['twitter'] = 0;
	}
	$shareSettings['share_url'] = $shareURL;
	$shareSettings['share_align'] = $_POST['share_align'];
	$share_serialized = serialize($shareSettings);
	//
	$s = get_settings();
	if (!isset($s['share_settings'])) {
		$q = "ALTER TABLE  `settings` ADD  `share_settings` VARCHAR( 255 ) NOT NULL";
		$ins = mysql_query($q) or die (mysql_error());
	}
	//
	$q = "update `settings` set share_settings = '$share_serialized' where settings_id = '1'";
	$ins = mysql_query($q) or die (mysql_error());
} else if (!isset($_POST['facebook_button']) && !isset($_POST['twitter_button'])) {
	// 
	// restore original if it exists
	$t = get_source_template('MAIN_ORIGINAL');
	if (is_array($t)) {
		$templateID = 'MAIN_PAGE';
		$templateContent = $t['contents'];
		save_template($templateID,addslashes($templateContent));
	}
	//
	// save settings for profile page
	$shareSettings = array();
	$shareSettings['facebook'] = 0;
	$shareSettings['twitter'] = 0;
	$shareSettings['share_url'] = $_POST['share_url'];
	$shareSettings['share_align'] = $_POST['share_align'];
	$share_serialized = serialize($shareSettings);
	//
	$s = get_settings();
	if (!isset($s['share_settings'])) {
		$q = "ALTER TABLE  `settings` ADD  `share_settings` VARCHAR( 255 ) NOT NULL";
		$ins = mysql_query($q) or die (mysql_error());
	}
	//
	$q = "update `settings` set share_settings = '$share_serialized' where settings_id = '1'";
	$ins = mysql_query($q) or die (mysql_error());
}
                                                        
//
// save stripe email
if (isset($_SESSION['user']['bigshow_stripe_email'])) { 
	//*****
	// DB Connect for single sign on
	//*****
	$link = mysql_connect(DB_HOST, DB_USER, DB_PASS) or mysql_error();
	$conn = mysql_select_db(DB_NAME);
	//*****     
	if (!isset($_SESSION['user']['bigshow_stripe_email'])) {
		$q = "ALTER TABLE  `clients` ADD  `bigshow_stripe_email` VARCHAR( 40 ) NOT NULL";
		$ins = mysql_query($q) or die (mysql_error());
	} 
	$clientID = $_SESSION['user']['id'];  
	$q = "update `clients` set bigshow_stripe_email = '$em' where id = '$clientID'";
	$ins = mysql_query($q) or die (mysql_error());
	$_SESSION['user']['bigshow_stripe_email'] = $em;
}

die("success");

?>