<?
// Include files
include('../../inc/db.inc.php');
include('../../inc/define.php');
include('../../inc/functions.inc.php');
session_start();
authorize_user();

if (isset($_POST['social_media'])) { 
	$smedia = $_POST['social_media'];
	$q = "update settings set social_media = '$smedia' where settings_id = 1"; 
	$upd = mysql_query($q)
		or die(mysql_error());
}
// 
if (isset($_POST['facebook_button']) || isset($_POST['twitter_button'])) {
	//
	// see if original exists
	$t = get_source_template('MAIN_ORIGINAL');
	if (!is_array($t)) {
	   	$t = get_source_template('MAIN_PAGE'); 
		$templateID = $t['template_id'];
		$templateContent = $t['contents'];  
		//
		// save a backup of the original 
		$d = date("U"); //date("m/d/Y-U");
		create_new_template('MAIN_ORIGINAL', 'HTML', addslashes($templateContent)); 
	} else {
		$templateID = 'MAIN_PAGE';
		$templateContent = $t['contents'];
	} 
	//
	// first add opaque window mode 
	$templateArr = explode('so.write("container");', $templateContent);   
	$soAdd = 'so.addParam("wmode", "opaque");';
	$soAdd .= "\n";
	$soAdd .= 'so.write("container");';
	$templateContent = implode($soAdd, $templateArr);
	//
	// next add css 
	$templateArr = explode('#container {', $templateContent);   
	$heightOffset = "-30px;";                                 
	if (isset($_POST['facebook_button']) && isset($_POST['twitter_button'])) {  
		$heightOffset = "-50px";
	}
	//
	if ($_POST['share_align'] == 'TL') {  
		$css = "#shareButtons { position:absolute; width:100px; margin-top:10px; margin-left:10px; }";
	} else if ($_POST['share_align'] == 'TR') { 
		$css = "#shareButtons { position:absolute; width:100px; left:100%; margin-top:10px; margin-left:-110px; }";
	} else if ($_POST['share_align'] == 'BL') {
		$css = "#shareButtons { position:absolute; width:100px; top:100%; margin-top:$heightOffset; margin-left:10px; }";
	} else {
		$css = "#shareButtons { position:absolute; width:100px; top:100%; left:100%; margin-top:$heightOffset; margin-left:-110px; }";
	}
	$css .= "\n#container {"; 
	$templateContent = implode($css, $templateArr);
	// 
	// now add the new div with the share buttons
	$templateArr = explode('<div id="container">', $templateContent);
	$shareURL = $_POST['share_url']; 
	$shareCode = '<div id="shareButtons">';
	if (isset($_POST['facebook_button'])) {
		$shareCode .= "\n";
		$shareCode .= '<script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like href="'.$shareURL.'" layout="button_count" show_faces="true" width="450" font=""></fb:like>';  
		$shareCode .= "<br /><!-- testing -->";
	}
	if (isset($_POST['twitter_button'])) {   
		$shareCode .= "\n";
		$shareCode .= '<a href="http://twitter.com/share" class="twitter-share-button" data-url="'.$shareURL.'" data-count="horizontal">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>';
	}
	$shareCode .= "\n</div>\n";
	$shareCode .= '<div id="container">';
	$templateContent = implode($shareCode, $templateArr);
	//
	// now save
	save_template($templateID,addslashes($templateContent));
	//
	// save settings for profile page
	$shareSettings = array();
	if (isset($_POST['facebook_button'])) {
		$shareSettings['facebook'] = 1;
	} else {
		$shareSettings['facebook'] = 0;
	}
	if (isset($_POST['twitter_button'])) {
		$shareSettings['twitter'] = 1;
	} else {
		$shareSettings['twitter'] = 0;
	}
	$shareSettings['share_url'] = $shareURL;
	$shareSettings['share_align'] = $_POST['share_align'];
	$share_serialized = serialize($shareSettings);
	//
	$s = get_settings();
	if (!isset($s['share_settings'])) {
		$q = "ALTER TABLE  `settings` ADD  `share_settings` VARCHAR( 255 ) NOT NULL";
		$ins = mysql_query($q) or die (mysql_error());
	}
	//
	$q = "update `settings` set share_settings = '$share_serialized' where settings_id = '1'";
	$ins = mysql_query($q) or die (mysql_error());
} else if (!isset($_POST['facebook_button']) && !isset($_POST['twitter_button'])) {
	// 
	// restore original if it exists
	$t = get_source_template('MAIN_ORIGINAL');
	if (is_array($t)) {
		$templateID = 'MAIN_PAGE';
		$templateContent = $t['contents'];
		save_template($templateID,addslashes($templateContent));
	}
	//
	// save settings for profile page
	$shareSettings = array();
	$shareSettings['facebook'] = 0;
	$shareSettings['twitter'] = 0;
	$shareSettings['share_url'] = $_POST['share_url'];
	$shareSettings['share_align'] = $_POST['share_align'];
	$share_serialized = serialize($shareSettings);
	//
	$s = get_settings();
	if (!isset($s['share_settings'])) {
		$q = "ALTER TABLE  `settings` ADD  `share_settings` VARCHAR( 255 ) NOT NULL";
		$ins = mysql_query($q) or die (mysql_error());
	}
	//
	$q = "update `settings` set share_settings = '$share_serialized' where settings_id = '1'";
	$ins = mysql_query($q) or die (mysql_error());
}

die("success");

?>