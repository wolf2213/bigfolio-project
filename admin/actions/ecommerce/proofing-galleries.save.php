<? 
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");

// rename categories and galleries
while (list($key, $val) = each($_POST)) {
	if (substr($key, 0, 3) == 'col') { // category key is something like cat-NN
		//
		// update category 
		$colID = substr($key, 4);
		$postArr = array();
		$postArr["name"] = $val;
		$postArr["api_key"] = get_property("propx_api_key"); 
		$url = "http://propx.co/api/collections/".$colID;
		$response = curl_post($url, $postArr, array());
	}
}  
	
die('success');
?>