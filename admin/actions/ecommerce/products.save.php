<? 
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");

// rename categories and galleries
$response = "";
while (list($key, $val) = each($_POST)) {
	if (substr($key, 0, 3) == 'pro') { // category key is something like cat-NN
		//
		// update category 
		$proID = substr($key, 4);
		$postArr = array();
		$postArr["name"] = $val;
		$url = "http://propx.co/api/products/".$proID;
		$response .= $url."\n";
		$options = array();
		$defaults = array( 
		    CURLOPT_POST => 1, 
		    CURLOPT_HEADER => 0, 
		    CURLOPT_URL => $url, 
		    CURLOPT_FRESH_CONNECT => 1, 
		    CURLOPT_RETURNTRANSFER => 1, 
		    CURLOPT_FORBID_REUSE => 1, 
		    CURLOPT_TIMEOUT => 4, 
		    CURLOPT_POSTFIELDS => "api_key=".get_property("propx_api_key")."&name=".stripslashes($val)."&description=".stripslashes($_POST['descr-pro'.$proID])."&price=".$_POST['price-pro'.$proID],
			CURLOPT_CUSTOMREQUEST => "PUT"
		); 

		$ch = curl_init(); 
		curl_setopt_array($ch, ($options + $defaults)); 
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));
		if( ! $response .= curl_exec($ch)) 
		{ 
		    trigger_error(curl_error($ch)); 
		} 
		curl_close($ch);

	}
}  
	
die('success'.$response);
?>