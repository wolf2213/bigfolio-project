<? 
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../../inc/db.inc.php");
include_once("../../../inc/functions.inc.php");

$galID = $_POST['galID'];
$galName = $_POST['galleryName'];
$productsArr = explode(",", $_POST['galleryProducts']);
$productsArrReq = "";
foreach($productsArr as $p) {
	$productsArrReq .= "&products[]=".$p;
}
/*
$postArr = array();
$postArr["name"] = $galName;
$postArr["api_key"] = get_property("propx_api_key"); 
//$postArr["products"] = $productsArr;
$url = "http://propx.co/api/collections/".$galID;
$response = curl_post($url, $postArr, array());
*/

//
$url = "http://propx.co/api/collections/".$_POST['galID'];
//
$options = array();
$defaults = array( 
    CURLOPT_POST => 1, 
    CURLOPT_HEADER => 0, 
    CURLOPT_URL => $url, 
    CURLOPT_FRESH_CONNECT => 1, 
    CURLOPT_RETURNTRANSFER => 1, 
    CURLOPT_FORBID_REUSE => 1, 
    CURLOPT_TIMEOUT => 4, 
    CURLOPT_POSTFIELDS => "api_key=".get_property("propx_api_key")."&name=".$galName.$productsArrReq
); 

$ch = curl_init(); 
curl_setopt_array($ch, ($options + $defaults)); 
//curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));
if( ! $response = curl_exec($ch)) 
{ 
    trigger_error(curl_error($ch)); 
} 
curl_close($ch);

$json = json_decode($response);

while (list($key, $val) = each($_POST)) {
	if (substr($key, 0, 7) == 'caption') { // caption key is something like captionNNN
		//
		// update captions for each 
		$imageID = substr($key, 7);                              
		$caption = $val;
		$postArr["caption"] = $caption;
		$postArr["api_key"] = get_property("propx_api_key"); 
		$url = "http://propx.co/api/images/".$imageID;
		$response = curl_post($url, $postArr, array());
		/*
		$cq = "update gallery_images set image_caption = '$caption' where image_id = '$imageID'";
		$upd = mysql_query($cq)
			or die(mysql_error());
			*/
	}
}
//
	
die('success:'.url.", ".$response.", products request: ".$productsArrReq.", ".$_POST['galleryProducts']);
?>