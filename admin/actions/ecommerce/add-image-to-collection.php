<?

function http_build_query_for_curl( $arrays, &$new = array(), $prefix = null ) {

    if ( is_object( $arrays ) ) {
        $arrays = get_object_vars( $arrays );
    }

    foreach ( $arrays AS $key => $value ) {
        $k = isset( $prefix ) ? $prefix . '[' . $key . ']' : $key;
        if ( is_array( $value ) OR is_object( $value )  ) { //
            http_build_query_for_curl( $value, $new, $k );
        } else {
            $new[$k] = $value;
        }
    }
}

//http://propx.co/api/collections/{COLLECTION ID}/import
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");
//
$url = "http://propx.co/api/collections/".$_POST['galID']."/import";
//
$options = array();
http_build_query_for_curl($postArr, $post);
$defaults = array( 
    CURLOPT_POST => 1, 
    CURLOPT_HEADER => 0, 
    CURLOPT_URL => $url, 
    CURLOPT_FRESH_CONNECT => 1, 
    CURLOPT_RETURNTRANSFER => 1, 
    CURLOPT_FORBID_REUSE => 1, 
    CURLOPT_TIMEOUT => 4, 
    CURLOPT_POSTFIELDS => "api_key=".get_property("propx_api_key")."&images[]=".str_replace(" ", "%20", $_POST['imageFile'])
); 

$ch = curl_init(); 
curl_setopt_array($ch, ($options + $defaults)); 
curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));
if( ! $response = curl_exec($ch)) 
{ 
    trigger_error(curl_error($ch)); 
} 
curl_close($ch);

$json = json_decode($response);
//
echo("SUCCESS:".", ".$url.", ".$response);
//
?>