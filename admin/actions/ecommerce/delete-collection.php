<?
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");
//
$url = "http://propx.co/api/collections/".$_POST['colID'];
$postArr = array();
$postArr["images"] = array();
$postArr["api_key"] = get_property("propx_api_key"); 
$options = array();
$defaults = array( 
    CURLOPT_POST => 1, 
    CURLOPT_HEADER => 0, 
    CURLOPT_URL => $url, 
    CURLOPT_FRESH_CONNECT => 1, 
    CURLOPT_RETURNTRANSFER => 1, 
    CURLOPT_FORBID_REUSE => 1, 
    CURLOPT_TIMEOUT => 4, 
    CURLOPT_POSTFIELDS => "api_key=".get_property("propx_api_key"),
	CURLOPT_CUSTOMREQUEST => "DELETE"
);
$ch = curl_init(); 
curl_setopt_array($ch, ($options + $defaults)); 
curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));
if( ! $response = curl_exec($ch)) 
{ 
    trigger_error(curl_error($ch)); 
} 
curl_close($ch);

//$json = json_decode($response);
//
die("SUCCESS:".$url."\n".$response);
?>