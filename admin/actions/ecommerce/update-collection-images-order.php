<? 
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");

$galID = $_POST['galID'];
//
$url = "http://propx.co/api/collections/$galID/sort";
//

$options = array();
$postArr = array();
$postArr["api_key"] = get_property("propx_api_key");
$postArr["images"] = array();
foreach($_POST["imgList"] as $img) {
	$postArr["images"][] = intval($img);
}

$data_string = json_encode($postArr);
$ch = curl_init($url); 
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
    'Content-Type: application/json',                                                                                
    'Content-Length: ' . strlen($data_string))                                                                       
);
if( ! $response = curl_exec($ch)) 
{ 
    trigger_error(curl_error($ch)); 
} 
curl_close($ch);

/*
$postArr = array();
$postArr["api_key"] = get_property("propx_api_key");
$postArr["images"] = array();
foreach($_POST["imgList"] as $img) {
	$postArr["images"][] = intval($img);
}
$response = curl_post($url, json_encode($postArr), array());

$json = json_decode($response);
*/

	
die('success:'.$url.", ".$data_string.", ".$response);
?>