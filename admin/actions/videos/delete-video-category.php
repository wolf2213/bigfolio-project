<? 
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');
// Include files
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");


// This file reorders the page via AJAX call
// include db
$return = '';
if (isset($_POST['catID'])) {  
	$return .= remove_vcategory($_POST['catID']);
}
   
die("SUCCESS:$return");
?>