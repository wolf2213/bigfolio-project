<?
//
// Prevent caching
header('Cache-Control: no-cache');
header('Pragma: no-cache'); 

$path = dirname(__FILE__);
if (stristr($path, "bfbs.phpfogapp.com") || stristr($path, "bigshow.bigfolio.com")) {
	session_start();
	$_SESSION['server'] = "bigshow";
}

include_once("../../inc/define.php");
include_once("../../inc/functions.inc.php");
// include stripe
require_once('../../inc/stripe-php/lib/Stripe.php'); 

//*****
// DB Connect for single sign on
//*****
$link = mysql_connect(DB_HOST, DB_USER, DB_PASS) or mysql_error();
$conn = mysql_select_db(DB_NAME);
//*****

//
$q = "select * from `clients` limit 1";
$sel = mysql_query($q) 
	or die(mysql_error());
$rowTest = mysql_fetch_array($sel, MYSQL_ASSOC);
if (!isset($rowTest['bigshow_upcoming_payment_notification_sent'])) {
	$q = "ALTER TABLE  `clients` ADD  `bigshow_upcoming_payment_notification_sent` TINYINT(1) NOT NULL";
	$sel = mysql_query($q) 
		or die(mysql_error());
}

include("../../inc/stripe-php/StripeKey.php");

// get 30 day time window
$unixNow        = time();
$unixMonth      = (30 * 24 * 60 * 60);
$unixNextMonth  = ($unixNow+$unixMonth);
$billYear       = intval(date('Y',$unixNextMonth));
$billMonth      = intval(date('m',$unixNextMonth));
$loginURL = ADMIN_URL . "login.php";

echo "$billMonth / $billYear";

//echo 'unixNow: '.$unixNow.', '.date('Y-m-d',$unixNow).'<br>';
//echo 'unixMonth: '.($unixNow+$unixMonth).', '.date('Y-m-d', $unixNow+$unixMonth).'<br>';

$q = "select * from clients where bigshow_subscription = 1 and bigshow_period_end > $unixNow and bigshow_period_end <= $unixNextMonth and bigshow_upcoming_payment_notification_sent != 1 order by id asc";
$sel = mysql_query($q)
	or die(mysql_error());
$clientsRows = array();
while ($row = mysql_fetch_array($sel, MYSQL_ASSOC)) {
	$clientsRows[] = $row;
}

echo "clients found:".count($clientsRows);

//
// include sendgrid
require_once('../../inc/mailer/class.phpmailer.php');
require_once('../../inc/mailer/class.smtp.php');
require_once('../../inc/mailer/smtp.php');

foreach($clientsRows as $cr) {
	//
	echo 'bigshow_period_end: '.$cr['bigshow_period_end'].'<br>';
	$stripeCustomer = Stripe_Customer::retrieve($cr["bigshow_stripe_customer"]);
	//
	// get card info for email
	$last4 = $stripeCustomer->active_card->last4;
	$exp_month = $stripeCustomer->active_card->exp_month;
	$exp_year = $stripeCustomer->active_card->exp_year;
	$nextAmount = $stripeCustomer->next_recurring_charge->amount / 100;
	$nextBillDate = $stripeCustomer->next_recurring_charge->date;
	echo "$exp_month / $exp_year<br>";
	$billNiceDate = date("F jS, Y", $cr['bigshow_period_end']);
	//
	// make message
	$message  = "Hello,\n\nThis is a friendly, automated reminder that your credit card ending in $last4 will be billed on $billNiceDate for $$nextAmount for your yearly BIG Show Unlimited subscription. ";
	if ($exp_year < $billYear || ($exp_month < $billMonth && $exp_year == $billYear)) {
		$message .= "According to our records, <font style='color:#FF0000'>the card on file is expired or may expire before the next bill is processed.</font> ";
		$message .= "You will need to update your credit card information from the <a href='$loginURL'>BIG Show admin</a>.\n";
	} else {
		$message .= "If needed, you can update your credit card information from the <a href='$loginURL'>BIG Show admin</a>.\n";
	}
	$message .= "\nIf you have any questions or concerns, please submit a ticket at <a href='http://support.bigfolio.com'>http://support.bigfolio.com</a>.\n\n";
	$message .= "Thanks,\n\n";
	$message .= "BIG Folio";

	//
	// send activation email via sendgrid
	$mail             = new PHPMailer();
	// SMTP Settings
	$mail->IsSMTP(); // telling the class to use SMTP
	$mail->Host       = "smtp.sendgrid.net";      // SMTP server
	$mail->SMTPDebug  = 2;                        // enables SMTP debug information (for testing)
	$mail->SMTPAuth   = true;                     // enable SMTP authentication
	$mail->Host       = "smtp.sendgrid.net";      // sets the SMTP server
	$mail->Port       = 25;                       // set the SMTP port 
	$mail->Username   = $_SMTP_USER; // SMTP account username
	$mail->Password   = $_SMTP_PASS; // SMTP account password

	$mail->SetFrom('bigshow@bigfolio.com', 'bigshow@bigfolio.com');
	$mail->Subject    = "Reminder :: BIG Show Unlimited subscription";
	$mail->AltBody    = $message;
	$mail->MsgHTML(nl2br($message));
	$mail->AddAddress($cr['bigshow_stripe_email']); 
	$mail->AddBCC('bigshow@bigfolio.com'); 

	// Send it ... 
	$result = $mail->Send();
	
	//
	// now set notification flag so this message isn't sent again, create column if necessary
	$clientID = $cr['id'];
	$q = "update `clients` set bigshow_upcoming_payment_notification_sent = '1' where id = '$clientID'";
	$ins = mysql_query($q) or die (mysql_error());
	
	//
	echo 'client: '.$cr['email'].'<br>';
}

?>