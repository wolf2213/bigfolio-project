<?
//
// removes all slideshows and images besides the 1st 10, and any images in the 1st 10 beyond the alotted 50/slideshow

function remove_unlimited_subscription($user) {   
	//
	// make delete jobs if needed
	$q  = 'CREATE TABLE IF NOT EXISTS `bigshow_delete_jobs` (';
	$q .= '`id` int(5) NOT NULL AUTO_INCREMENT,';
	$q .= '`storage_id` varchar(255) NOT NULL,';
	$q .= '`slideshow_id` int(5) NOT NULL,';
	$q .= '`filename` varchar(500) NOT NULL,';
	$q .= 'PRIMARY KEY (`id`),';
	$q .= 'KEY `id` (`id`)';
	$q .= ') ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=258 ;';
	$setup = mysql_query($q)
		or die('Bad query');	
	
	$clientID = $user['id'];
	//
	// remove subscription settings for db
	$q = "update `clients` set bigshow_subscription = '-1', bigshow_period_end = '0', bigshow_upcoming_payment_notification_sent = '0' where id = '$clientID'";
	$ins = mysql_query($q) 
		or die (mysql_error());
			
	if ($_SESSION['server'] == 'bigshow') {
		$freeLimit = 3;
	} else {
		$freeLimit = 5;
	}
	//
	// remove slideshows and images beyond limit
	if ($_SESSION['server'] == 'bigshow') {
		$storageID = $user['storage_id'];
		$q = "select * from bigshow_slideshows where storage_id = '$storageID' order by id";
	} else {
		//
		// connect to client db
		$db_type = "mysql";
		$db_host = IP; 
		$db_user = $user['db_user'];
		$db_pass = $user['db_pass'];
		$db_name = $user['db_name'];
		if ($db_type == "mysql") {
			$link = mysql_connect($db_host, $db_user, $db_pass) or mysql_error();
			$conn = mysql_select_db($db_name);
		}
		//
		$q = "select * from bigshow_slideshows order by id";
	}
	$sel = mysql_query($q)
		or die(mysql_error());
	$all_slideshows = array();
	while ($ss = mysql_fetch_array($sel, MYSQL_ASSOC)) {
		$all_slideshows[] = $ss;
	}
	$slideshowCount = 0;
	
	$remove_echo = "number of slideshows: ".count($all_slideshows)."\n";
	
	foreach ($all_slideshows as $ss) {
		
		if ($_SESSION['server'] == 'bigshow') {
			// already connected to db with bigshow_slideshows and bigshow_images tables
		} else {
			//
			// connect to client db
			$db_host = IP; 
			$db_user = $user['db_user'];
			$db_pass = $user['db_pass'];
			$db_name = $user['db_name'];
			if ($db_type == "mysql") {
				$link = mysql_connect($db_host, $db_user, $db_pass) or mysql_error();
				$conn = mysql_select_db($db_name);
			}
		}
		
		$sid = $ss['id'];
		$ss = get_bigshow($sid);
		$images = get_bigshow_images($sid); 
		$storageID = $ss['storage_id'];
		$settings = json_decode($ss['settings']);
		
		$remove_echo .= "number of images: ".count($images)."\n";
		
		//
		// first remove all image references as needed
		$imageCount = 0;
		foreach($images as $img) {
			if ($slideshowCount >= $freeLimit || $imageCount >= 50) {
				//
				// remove reference from db
				$remove_echo .= $img['image_id'].', '.$img['image_file']."\n";
				$imageID = $img['image_id'];
				$q = "delete from bigshow_images where image_id = '$imageID'";
				$get = mysql_query($q)
					or die('Bad query1');
			}
			$imageCount++;
		}
		
		if ($slideshowCount >= $freeLimit) {
			remove_slideshow($sid);
			$remove_echo .= "removed slideshow: $sid\n\n";
		}
		
		
		//*****
		// DB Connect for single sign on
		//*****
		$link = mysql_connect(DB_HOST, DB_USER, DB_PASS) or mysql_error();
		$conn = mysql_select_db(DB_NAME);
		//*****
		
		//
		// now add image references to delete jobs table
		$imageCount = 0;
		foreach($images as $img) {
			if ($slideshowCount >= 10 || $imageCount >= 50) {
				//
				// insert file into delete jobs table for future deletion
				$file = $img['image_file'];
				$q = "insert into `bigshow_delete_jobs` (`storage_id`,`slideshow_id`,`filename`) values ('$storageID','$sid','$file')";
				$get = mysql_query($q)
					or die('Bad query2');
			}
			$imageCount++;
		}

		if (isset($settings->music_files) && $slideshowCount >= 10) {
			$musicFiles = explode(",", $settings->music_files);
			foreach($musicFiles as $file) {
				//
				// insert file into delete jobs table for future deletion
				$q = "insert into `bigshow_delete_jobs` (`storage_id`,`slideshow_id`,`filename`) values ('$storageID','$sid','$file')";
				$get = mysql_query($q)
					or die('Bad query3');
			}
		}
		$slideshowCount++;
		
	}
}
?>