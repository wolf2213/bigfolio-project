<?

require_once('../../inc/stripe-php/lib/Stripe.php'); 
//include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");

//*****
// DB Connect for single sign on
//*****
$link = mysql_connect(DB_HOST, DB_USER, DB_PASS) or mysql_error();
$conn = mysql_select_db(DB_NAME);
//*****

include("../../inc/stripe-php/StripeKey.php");
$token = $_POST['stripeToken'];

try {

	$cu = Stripe_Customer::retrieve($_SESSION['user']['bigshow_stripe_customer']);
	$cu->card = $token; // obtained with Stripe.js
	$cu->save();
	//
/*
	$q = "update `settings` set bigshow_stripe_token = '$token' where settings_id = '1'";
	$ins = mysql_query($q) or die (mysql_error());
*/

	//
	// get date
	$niceDate = date("F jS Y", $_SESSION['user']['bigshow_period_end']);

	//
	// make message
	$message  = "You've successfully updated the card used for your BIG Show Unlimited subscription. Thanks!\n\n";
	$message .= "The card you entered will be charged $99 on <strong>$niceDate</strong> for the next year of your subscription.\n\n";
	$message .= "Thanks,\n\n";
	$message .= "BIG Folio";

	//
	// include sendgrid
	require_once('../../inc/mailer/class.phpmailer.php');
	require_once('../../inc/mailer/class.smtp.php');
	require_once('../../inc/mailer/smtp.php');

	//
	// send activation email via sendgrid
	$mail             = new PHPMailer();
	// SMTP Settings
	$mail->IsSMTP(); // telling the class to use SMTP
	$mail->Host       = "smtp.sendgrid.net";      // SMTP server
	$mail->SMTPDebug  = 2;                        // enables SMTP debug information (for testing)
	$mail->SMTPAuth   = true;                     // enable SMTP authentication
	$mail->Host       = "smtp.sendgrid.net";      // sets the SMTP server
	$mail->Port       = 25;                       // set the SMTP port 
	$mail->Username   = $_SMTP_USER; // SMTP account username
	$mail->Password   = $_SMTP_PASS; // SMTP account password

	$mail->SetFrom('bigshow@bigfolio.com', 'bigshow@bigfolio.com');
	$mail->Subject    = "BIG Show Unlimited subscription card updated";
	$mail->AltBody    = $message;
	$mail->MsgHTML(nl2br($message));
	$mail->AddAddress($_SESSION['user']['email']);
	$mail->AddBCC("bigshow@bigfolio.com");

	// Send it ... 
	$result = $mail->Send();

	die('subscriptionSuccess'); 

} catch (Stripe_CardError $e) {   
	die('subscritionFailure:'.$e);
}

?>