<?
session_start();

require_once('../../inc/stripe-php/lib/Stripe.php'); 
include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");
$s = get_settings();

//*****
// DB Connect for single sign on
//*****
$link = mysql_connect(DB_HOST, DB_USER, DB_PASS) or mysql_error();
$conn = mysql_select_db(DB_NAME);
//*****

include("../../inc/stripe-php/StripeKey.php");
$token = $_POST['stripeToken'];

try {  
	
	$settingsID = 1;
	$clientID = $_SESSION['user']['id'];
	if ($_SESSION['server'] == 'bigshow') {
		$settingsID = $clientsID;
		$stripeEmail = $_SESSION['user']['email'];
	} else {
		$stripeEmail = $s['email'];
	}
	
	$customer = Stripe_Customer::create(array(
	  "card" => $token,
	  "plan" => "bigshowyearly",
	  "email" => $stripeEmail)
	);

	$customer = json_decode($customer); 
	$custID = $customer->id;

/*
	if (!isset($s['bigshow_stripe_token'])) {
		$q = "ALTER TABLE  `settings` ADD  `bigshow_stripe_token` VARCHAR( 40 ) NOT NULL";
		$ins = mysql_query($q) or die (mysql_error());
	}
	//
	$q = "update `settings` set bigshow_stripe_token = '$token' where settings_id = '$settingsID'";
	$ins = mysql_query($q) or die (mysql_error());
*/

	if (!isset($_SESSION['user']['bigshow_stripe_customer'])) {
		$q = "ALTER TABLE  `clients` ADD  `bigshow_stripe_customer` VARCHAR( 40 ) NOT NULL";
		$ins = mysql_query($q) or die (mysql_error());
	}
	$q = "update `clients` set bigshow_stripe_customer = '$custID' where id = '$clientID'";
	$ins = mysql_query($q) or die (mysql_error());
	$_SESSION['user']['bigshow_stripe_customer'] = $custID;


	if (!isset($_SESSION['user']['bigshow_stripe_email'])) {
		$q = "ALTER TABLE  `clients` ADD  `bigshow_stripe_email` VARCHAR( 40 ) NOT NULL";
		$ins = mysql_query($q) or die (mysql_error());
	}
	$q = "update `clients` set bigshow_stripe_email = '$stripeEmail' where id = '$clientID'";
	$ins = mysql_query($q) or die (mysql_error());
	$_SESSION['user']['bigshow_stripe_email'] = $stripeEmail;


	if (!isset($_SESSION['user']['bigshow_subscription'])) {
		$q = "ALTER TABLE  `clients` ADD  `bigshow_subscription` TINYINT( 1 ) NOT NULL";
		$ins = mysql_query($q) or die (mysql_error());
	} 
	$q = "update `clients` set bigshow_subscription = '1' where id = '$clientID'";
	$ins = mysql_query($q) or die (mysql_error()); 
	$_SESSION['user']['bigshow_subscription'] = 1;


	if (!isset($_SESSION['user']['bigshow_period_end'])) {
		$q = "ALTER TABLE  `clients` ADD  `bigshow_period_end` INT( 12 ) NOT NULL";
		$ins = mysql_query($q) or die (mysql_error());
	}
	$CPE = $customer->subscription->current_period_end;
	$q = "update `clients` set bigshow_period_end = '$CPE' where id = '$clientID'";
	$ins = mysql_query($q) or die (mysql_error());
	$_SESSION['user']['bigshow_period_end'] = $CPE;  
	

	if (!isset($_SESSION['user']['bigshow_upcoming_payment_notification_sent'])) {
		$q = "ALTER TABLE  `clients` ADD  `bigshow_upcoming_payment_notification_sent` TINYINT(1) NOT NULL";
		$sel = mysql_query($q) 
			or die(mysql_error());  
	}                               
	$q = "update `clients` set bigshow_upcoming_payment_notification_sent = '0' where id = '$clientID'";
	$ins = mysql_query($q) or die (mysql_error());
	$_SESSION['user']['bigshow_upcoming_payment_notification_sent'] = 0;

	//
	// get date
	$niceDate = date("F jS", $CPE);

	//
	// make message
	$message  = "You've successfully signed up for BIG Show Unlimited. Thanks!\n\n";
	$message .= "The card you entered will be charged $99 every year on $niceDate while the subscription is active.\n\n";
	$message .= "You can update your credit card information or cancel your subscription before the next charge from the control panel.\n\n";
	$message .= "Thanks,\n\n";
	$message .= "BIG Folio";

	//
	// include sendgrid
	require_once('../../inc/mailer/class.phpmailer.php');
	require_once('../../inc/mailer/class.smtp.php');
	require_once('../../inc/mailer/smtp.php');

	//
	// send activation email via sendgrid
	$mail             = new PHPMailer();
	// SMTP Settings
	$mail->IsSMTP(); // telling the class to use SMTP
	$mail->Host       = "smtp.sendgrid.net";      // SMTP server
	$mail->SMTPDebug  = 2;                        // enables SMTP debug information (for testing)
	$mail->SMTPAuth   = true;                     // enable SMTP authentication
	$mail->Host       = "smtp.sendgrid.net";      // sets the SMTP server
	$mail->Port       = 25;                       // set the SMTP port 
	$mail->Username   = $_SMTP_USER; // SMTP account username
	$mail->Password   = $_SMTP_PASS; // SMTP account password

	$mail->SetFrom('bigshow@bigfolio.com', 'bigshow@bigfolio.com');
	$mail->Subject    = "BIG Show Unlimited subscription purchased";
	$mail->AltBody    = $message;
	$mail->MsgHTML(nl2br($message));
	$mail->AddAddress($stripeEmail);
	$mail->AddBCC("bigshow@bigfolio.com");

	// Send it ... 
	$result = $mail->Send();

	die('subscriptionSuccess'); 
	
} catch (Stripe_CardError $e) {   
	die('subscritionFailure:'.$e);
}

?>