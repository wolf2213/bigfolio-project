<?
session_start();

require_once('../../inc/stripe-php/lib/Stripe.php'); 
//include_once("../../inc/db.inc.php");
include_once("../../inc/functions.inc.php");

//*****
// DB Connect for single sign on
//*****
$link = mysql_connect(DB_HOST, DB_USER, DB_PASS) or mysql_error();
$conn = mysql_select_db(DB_NAME);
//*****

$clientID = $_SESSION['user']['id'];

include("../../inc/stripe-php/StripeKey.php");
$token = $_POST['stripeToken'];

$cu = Stripe_Customer::retrieve($_SESSION['user']["bigshow_stripe_customer"]);
$cu->cancelSubscription();
//
/*
$q = "update `clients` set bigshow_stripe_token = '' where settings_id = '1'";
$ins = mysql_query($q) or die (mysql_error());
*/
$_SESSION['user']['bigshow_subscription'] = 0;

$q = "update `clients` set bigshow_subscription = '0', bigshow_period_end = '0', bigshow_upcoming_payment_notification_sent = '0' where id = '$clientID'";
$ins = mysql_query($q) or die (mysql_error()); 

//
// make message
$loginURL = ADMIN_URL . "login.php";
$message  = "Hello,\n\nYour BIG Show Unlimited subscription has been cancelled. Should you want to purchase BIG Show Unlimited again, you can do so from the <a href='$loginURL'>BIG Show admin</a>.\n";
$message .= "Thanks,\n\n";
$message .= "BIG Folio";

//
// include sendgrid
require_once('../../inc/mailer/class.phpmailer.php');
require_once('../../inc/mailer/class.smtp.php');
require_once('../../inc/mailer/smtp.php');

//
// send activation email via sendgrid
$mail             = new PHPMailer();
// SMTP Settings
$mail->IsSMTP(); // telling the class to use SMTP
$mail->Host       = "smtp.sendgrid.net";      // SMTP server
$mail->SMTPDebug  = 0;                        // enables SMTP debug information (for testing)
$mail->SMTPAuth   = true;                     // enable SMTP authentication
$mail->Host       = "smtp.sendgrid.net";      // sets the SMTP server
$mail->Port       = 25;                       // set the SMTP port 
$mail->Username   = $_SMTP_USER; // SMTP account username
$mail->Password   = $_SMTP_PASS; // SMTP account password

$mail->SetFrom('bigshow@bigfolio.com', 'bigshow@bigfolio.com');
$mail->Subject    = "BIG Show Unlimited subscription cancelled";
$mail->AltBody    = $message;
$mail->MsgHTML(nl2br($message));
$mail->AddBCC("bigshow@bigfolio.com");
$mail->AddAddress($_SESSION['user']['bigshow_stripe_email']);

// Send it ... 
$result = $mail->Send();

die('success:'); 

?>