<?
include_once("../../inc/define.php");
include_once("../../inc/functions.inc.php");
//
// include stripe
require_once('../../inc/stripe-php/lib/Stripe.php');
//
// include sendgrid
require_once('../../inc/mailer/class.phpmailer.php');
require_once('../../inc/mailer/class.smtp.php');
require_once('../../inc/mailer/smtp.php'); 

$loginURL = ADMIN_URL . "login.php"; 

//*****
// DB Connect for single sign on
//*****
$link = mysql_connect(DB_HOST, DB_USER, DB_PASS) or mysql_error();
$conn = mysql_select_db(DB_NAME);
//*****

include("../../inc/stripe-php/StripeKey.php");

// retrieve the request's body and parse it as JSON
$body = @file_get_contents('php://input');
$event_json = json_decode($body);

//
// make message
try {
    // for extra security, retrieve from the Stripe API, this will fail in Test Webhooks
    $event_id = $event_json->{'id'};
    $event = Stripe_Event::retrieve($event_id);

	$sub = "stripe event: $event->type";
	$message = "STRIPE EVENT: $event->type\n\n";
    
	//
	// send email reminder if billing fails
	
    if ($event->type == 'invoice.payment_failed') {
		//
		$stripeCustomer = $event_json->data->object->customer;
		$q = "select * from `clients` where `bigshow_stripe_customer` = '$stripeCustomer'";
		$sel = mysql_query($q) 
			or die(mysql_error());
		if (mysql_num_rows($sel) > 0) {
			$user = mysql_fetch_array($sel, MYSQL_ASSOC);
			//
			$stripeCustomer = Stripe_Customer::retrieve($stripeCustomer);
			//
			// get card info for email
			$last4 = $stripeCustomer->active_card->last4;
			$exp_month = $stripeCustomer->active_card->exp_month;
			$exp_year = $stripeCustomer->active_card->exp_year;
			$nextAmount = $stripeCustomer->next_recurring_charge->amount / 100;
			$nextBillDate = $stripeCustomer->next_recurring_charge->date;
			$billYear       = intval(date('Y',$user['bigshow_period_end']));
			$billMonth      = intval(date('m',$user['bigshow_period_end']));
			//
			// make message
			$message  = "Hello,\n\nWe attempted to run your credit card ending in $last4 for $$nextAmount for your yearly BIG Show Unlimited subscription, but the transaction failed. ";
			if ($exp_year < $billYear || ($exp_month < $billMonth && $exp_year == $billYear)) {
				$message .= "According to our records, <font style='color:#FF0000'>the card on file is expired or may expire before the next bill is processed.</font> ";
				$message .= "You will need to update your credit card information from the <a href='$loginURL'>BIG Show admin</a>.\n";
			} else {
				$message .= "Please check with your bank, and, if needed, update your credit card information from the <a href='$loginURL'>BIG Show admin</a>.\n";
			}
			$message .= "\nWe will attempt to run the card on file 3 times before deleting slideshows beyond the first 10 and any slideshow images that exceed the 50 image limit per slideshow.";
			$message .= "If you have any questions or concerns, please submit a ticket at <a href='http://support.bigfolio.com'>http://support.bigfolio.com</a>.\n\n";
			$message .= "Thanks,\n\n";
			$message .= "BIG Folio";

			//
			// send activation email via sendgrid
			$mail             = new PHPMailer();
			// SMTP Settings
			$mail->IsSMTP(); // telling the class to use SMTP
			$mail->Host       = "smtp.sendgrid.net";      // SMTP server
			$mail->SMTPDebug  = 2;                        // enables SMTP debug information (for testing)
			$mail->SMTPAuth   = true;                     // enable SMTP authentication
			$mail->Host       = "smtp.sendgrid.net";      // sets the SMTP server
			$mail->Port       = 25;                       // set the SMTP port 
			$mail->Username   = $_SMTP_USER; // SMTP account username
			$mail->Password   = $_SMTP_PASS; // SMTP account password

			$mail->SetFrom('bigshow@bigfolio.com', 'bigshow@bigfolio.com');
			$mail->Subject    = "BIG Show Unlimited subscription payment failed";
			$mail->AltBody    = $message;
			$mail->MsgHTML(nl2br($message));
			$mail->AddBCC("bigshow@bigfolio.com");
			$mail->AddAddress($user['bigshow_stripe_email']);

			// Send it ... 
			$result = $mail->Send();
			//
		}
		//
    }

	//
	// check for canceled account once subscription payment fails 3 times
	// (status switches from "past_due" to "cancel")
	if ($event->type == 'customer.subscription.deleted') { //customer.subscription.updated
		//
		//if ($event_json->data->object->status == 'cancel') {
			$stripeCustomer = $event_json->data->object->customer;
			$message = "";
			
			// SOMETHING IS FAILING HERE
			$q = "select * from `clients` where `bigshow_stripe_customer` = '$stripeCustomer'";
			$sel = mysql_query($q) 
				or $message .= mysql_error();
			if (mysql_num_rows($sel) > 0) {
				$user = mysql_fetch_array($sel, MYSQL_ASSOC);
				
				//
				// make message
				$message  = "Hello,\n\nYour BIG Show Unlimited subscription has been removed. You can still access your account with the same login, but you are now limited to 10 slideshows each with up to 50 images. Should you want to purchase BIG Show Unlimited again, you can do so from the <a href='$loginURL'>BIG Show admin</a>.\n\n";
				$message .= "Thanks,\n\n";
				$message .= "BIG Folio";

				//
				// send activation email via sendgrid
				$mail             = new PHPMailer();
				// SMTP Settings
				$mail->IsSMTP(); // telling the class to use SMTP
				$mail->Host       = "smtp.sendgrid.net";      // SMTP server
				$mail->SMTPDebug  = 2;                        // enables SMTP debug information (for testing)
				$mail->SMTPAuth   = true;                     // enable SMTP authentication
				$mail->Host       = "smtp.sendgrid.net";      // sets the SMTP server
				$mail->Port       = 25;                       // set the SMTP port 
				$mail->Username   = $_SMTP_USER; // SMTP account username
				$mail->Password   = $_SMTP_PASS; // SMTP account password

				$mail->SetFrom('bigshow@bigfolio.com', 'bigshow@bigfolio.com');
				$mail->Subject    = "BIG Show Unlimited subscription removed";
				$mail->AltBody    = $message;
				$mail->MsgHTML(nl2br($message));
				$mail->AddBCC("bigshow@bigfolio.com");
				$mail->AddAddress($user['bigshow_stripe_email']);
				//$mail->AddAddress("bigshow@bigfolio.com");

				// Send it ... 
				$result = $mail->Send();
				
				//
				// now get rid of stuff...
				require_once("remove_unlimited_subscription.php");
				remove_unlimited_subscription($user);
			}
		//
    }
    
    // update current period end, notification, and any other info if billing succeeds
    if ($event->type == 'invoice.payment_succeeded') {
		// 
		$stripeCustomer = $event_json->data->object->customer;
		$q = "select * from `clients` where `bigshow_stripe_customer` = '$stripeCustomer'";
		$sel = mysql_query($q) 
			or die(mysql_error());
		if (mysql_num_rows($sel) > 0) {
			$user = mysql_fetch_array($sel, MYSQL_ASSOC);     
			//
			if (!isset($user['bigshow_upcoming_payment_notification_sent'])) {
				$q = "ALTER TABLE  `clients` ADD  `bigshow_upcoming_payment_notification_sent` TINYINT(1) NOT NULL";
				$sel = mysql_query($q) 
					or die(mysql_error());
			}
			//
			$clientID = $user['id'];
			$stripeCustomer = Stripe_Customer::retrieve($stripeCustomer);
			$CPE = $stripeCustomer->subscription->current_period_end;
			$q = "update `clients` set bigshow_period_end = '$CPE', bigshow_upcoming_payment_notification_sent = '0' where id = '$clientID'";
			$ins = mysql_query($q)
				or die (mysql_error());
			//
			//
			// make message
			$message  = "Your yearly BIG Show Unlimited fee of $99 has been successfully processed.\n\n";
			$message .= "Thanks,\n\n";
			$message .= "BIG Folio";

			//
			// send activation email via sendgrid
			$mail             = new PHPMailer();
			// SMTP Settings
			$mail->IsSMTP(); // telling the class to use SMTP
			$mail->Host       = "smtp.sendgrid.net";      // SMTP server
			$mail->SMTPDebug  = 2;                        // enables SMTP debug information (for testing)
			$mail->SMTPAuth   = true;                     // enable SMTP authentication
			$mail->Host       = "smtp.sendgrid.net";      // sets the SMTP server
			$mail->Port       = 25;                       // set the SMTP port 
			$mail->Username   = $_SMTP_USER; // SMTP account username
			$mail->Password   = $_SMTP_PASS; // SMTP account password

			$mail->SetFrom('bigshow@bigfolio.com', 'bigshow@bigfolio.com');
			$mail->Subject    = "BIG Show Unlimited subscription purchased";
			$mail->AltBody    = $message;
			$mail->MsgHTML(nl2br($message));
			$mail->AddBCC("bigshow@bigfolio.com");
			$mail->AddAddress($user['bigshow_stripe_email']);

			// Send it ... 
			$result = $mail->Send();
		}  
		//
    }
        
} catch (Stripe_InvalidRequestError $e) {   
	$sub = "stripe event retrieval error: ".$event_json->type; 
	$message = "ERROR LOOKING UP EVENT ID:";
}

/*
$message .= "\n\nevent id: ".$event_json->id." \nevent customer: ".$event_json->data->object->customer;
$message .= "\n\nraw json: ".$body;

//
$mail             = new PHPMailer();
// SMTP Settings
$mail->IsSMTP(); // telling the class to use SMTP
$mail->Host       = "smtp.sendgrid.net";      // SMTP server
$mail->SMTPDebug  = 2;                        // enables SMTP debug information (for testing)
$mail->SMTPAuth   = true;                     // enable SMTP authentication
$mail->Host       = "smtp.sendgrid.net";      // sets the SMTP server
$mail->Port       = 25;                       // set the SMTP port 
$mail->Username   = $_SMTP_USER; // SMTP account username
$mail->Password   = $_SMTP_PASS; // SMTP account password

$mail->SetFrom('bigshow@bigfolio.com', 'bigshow@bigfolio.com');
$mail->Subject    = $sub;
$mail->AltBody    = $message;
$mail->MsgHTML(nl2br($message));
$mail->AddAddress("bigshow@bigfolio.com");

// Send it ... 
$result = $mail->Send();
*/

?>