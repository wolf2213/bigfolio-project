<?  

require_once('../../inc/mailer/class.phpmailer.php');
require_once('../../inc/mailer/class.smtp.php');
require_once('../../inc/mailer/smtp.php'); 

$path = dirname(__FILE__); 
$server = $_SERVER['HTTP_HOST'];
//
// make message
$message  = "path: $path\n";
$message .= "server: $server\n";

//
// send activation email via sendgrid
$mail             = new PHPMailer();
// SMTP Settings
$mail->IsSMTP(); // telling the class to use SMTP
$mail->Host       = "smtp.sendgrid.net";      // SMTP server
$mail->SMTPDebug  = 2;                        // enables SMTP debug information (for testing)
$mail->SMTPAuth   = true;                     // enable SMTP authentication
$mail->Host       = "smtp.sendgrid.net";      // sets the SMTP server
$mail->Port       = 25;                       // set the SMTP port 
$mail->Username   = $_SMTP_USER; // SMTP account username
$mail->Password   = $_SMTP_PASS; // SMTP account password

$mail->SetFrom('bigshow@bigfolio.com', 'bigshow@bigfolio.com');
$mail->Subject    = "cron test";
$mail->AltBody    = $message;
$mail->MsgHTML(nl2br($message));
$mail->AddAddress('bigshow@bigfolio.com');

// Send it ... 
$result = $mail->Send();
?>