<?
session_start();
$_SESSION['server'] = "rickey";
include('../inc/db.inc.php');
if ($_SERVER['REQUEST_METHOD'] == "POST") {
	$link = mysql_connect(DB_HOST, DB_USER, DB_PASS) or mysql_error();
	$conn = mysql_select_db(DB_NAME);
	//
	$q = "SELECT * FROM  `clients` WHERE  `url` NOT LIKE '%bigfoliodemo.us%'";
	$sel = mysql_query($q)
		or die(mysql_error());
	$r = array();
	while ($row = mysql_fetch_array($sel, MYSQL_ASSOC)) {
		$r[] = $row;
	}
	//
	$clients = array();
	foreach($r as $row) {
		//
		// connect to site db
		$link = mysql_connect(DB_HOST, $row["db_user"], $row["db_pass"]) or mysql_error();
		$conn = mysql_select_db($row["db_name"]);
		//
		// check if this is a rubix site
		$q = "select * from properties where property_value='rubix'";
		$sel = mysql_query($q);
			//or die(mysql_error());
		if (mysql_num_rows($sel)) {
			//
			// get settings
			$q = "select * from settings where settings_id = 1";
			$sel = mysql_query($q)
				or die(mysql_error());
			$s = mysql_fetch_array($sel, MYSQL_ASSOC);
			$vars = unserialize($s["flash_vars"]);
			$match = true;
			if (!file_exists($row["path"]."rubix_index.php")) {
				$match = false;
			}
			if ($_POST["menu_position"] != "" && $_POST["menu_position"] != $vars["menu_position"]) {
				$match = false;
			}
			if ($_POST["menu_alignment"] != "" && $_POST["menu_alignment"] != $vars["menu_alignment"]) {
				$match = false;
			}
			if ($_POST["gallery_mode"] != "" && $_POST["gallery_mode"] != $vars["gallery_mode"]) {
				$match = false;
			}
			if ($_POST["thumbnail_mode"] != "" && $_POST["thumbnail_mode"] != $vars["thumbnail_mode"]) {
				$match = false;
			}
			if ($match) { 
				$isLive = FALSE;
				if (stristr($row['url'], 'bigfoliodemo') === FALSE && stristr($row['path'], 'subdomains') === FALSE) {
					$urlArr = explode("/",$row['url']);
					$domain = $urlArr[2]; 
					$liveCheck = gethostbyname($domain);

					if (stristr($liveCheck, IP)) { 
						$isLive = TRUE;
						$row['temp_url'] = $row['url'] . 'main.php';
					} else {
						$row['temp_url'] = 'http://' . IP . '/~' . $row['db_user'] . '/main.php';
					} 
				} else {
					$isLive = FALSE;
					$row['temp_url'] = $row['url'] . 'main.php';
				}
				$clients[] = $row; 
			}
		}
	}
	//
}
?>
<html>
<head>
</head>
<style>
	#rubixQueryForm, #results {
		width: 600px;
		margin: 40px auto;
		font-family: Helvetica, Arial, sans-serif;
	}
	label, input {
		display: block;
	}
</style>
<body>
  <form id="rubixQueryForm" method="post">
	
	<label for="menu_position">menu position</label>
	<select name="menu_position">
		<option></option>
		<option>left</option>
		<option>top</option>
		<option>right</option>
	</select>
	
	<label for="menu_alignment">menu alignment</label>
	<select name="menu_alignment">
		<option></option>
		<option>left</option>
		<option>center</option>
		<option>right</option>
	</select>
	
	<label for="gallery_mode">gallery mode</label>
	<select name="gallery_mode">
		<option></option>
		<option>large</option>
		<option>horizontal</option>
		<option>vertical</option>
		<option>masonry</option>
		<option>fullscreen</option>
	</select>
	
	<label for="thumbnail_mode">thumbnail mode</label>
	<select name="thumbnail_mode">
		<option></option>
		<option>left</option>
		<option>bottom</option>
		<option>right</option>
		<option>overlay</option>
		<option>none</option>
	</select>
	
	<!--<input type="checkbox" name="videoEmbed">has video embedded-->
	
	<input type="submit" id="submit"></input>
	
  </form>
  <div id="results">
	<? if (isset($clients) && count($clients) > 0) { ?>
		Showing results matching:
		<? 
		while (list($key, $val) = each($_POST)) {
			if ($val != "") {
				echo "<br>$key: $val";
			}
		}
		?>
		<ul>
		<? foreach($clients as $client) { ?>
			<li><a href="<?=$client["temp_url"]?>" target="_blank"><?=$client["url"]?></a></li>
		<? } ?>
		</ul>
	<? } else { ?>
		No matches found.
	<? } ?>
  </div>

</body>
</html>