<?php
session_start();            
define("ADMIN_VERSION","4.02");
$signupSuccess = false;
$verifySuccess = false;
$_SESSION['server'] = "bigshow";
//
// db stuff
include('inc/define.php');
$_SESSION['user']['db_user'] = DB_USER;
$_SESSION['user']['db_pass'] = DB_PASS;
$_SESSION['user']['db_name'] = DB_NAME;
include("inc/db.inc.php");
include("inc/functions.inc.php");
$err = "";
//
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	//
	// vars
	$u = $_POST['username'];
	$e = $_POST['email'];
	$p = $_POST['pass'];
	$r = $_POST['retype'];
	
	//
	// check if username is long enough
	if (strlen($u) < 4) {
		$err .= 'Please enter a username that is at least 4 characters long.<br>';
	}
	//
	// check if username is taken
	$q = "select * from clients where username = '$u'";
	$sel = mysql_query($q)
		or die(mysql_error());
	if (mysql_num_rows($sel)) {
		$err .= 'Sorry, that username is not available.<br>';
		$u = "";
	}
	
	//
	// check if email is valid
	function check_email_address($email) {
	  // First, we check that there's one @ symbol, 
	  // and that the lengths are right.
	  if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email)) {
	    // Email invalid because wrong number of characters 
	    // in one section or wrong number of @ symbols.
	    return false;
	  }
	  // Split it into sections to make life easier
	  $email_array = explode("@", $email);
	  $local_array = explode(".", $email_array[0]);
	  for ($i = 0; $i < sizeof($local_array); $i++) {
	    if (!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])) {
	      return false;
	    }
	  }
	  // Check if domain is IP. If not, 
	  // it should be valid domain name
	  if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1])) {
	    $domain_array = explode(".", $email_array[1]);
	    if (sizeof($domain_array) < 2) {
	        return false; // Not enough parts to domain
	    }
	    for ($i = 0; $i < sizeof($domain_array); $i++) {
	      if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i])) {
	        return false;
	      }
	    }
	  }
	  return true;
	}
	if (!check_email_address($e)) {
		$err .= 'Please enter a valid email address.<br>';
		$e = "";
	} 
	
	//
	// check if email is used already
	$q = "select * from clients where email = '$e'";
	$sel = mysql_query($q)
		or die(mysql_error());
	if (mysql_num_rows($sel)) {
		$err .= 'Sorry, that email is being used with another account.<br>';
		$e = "";
	}
	
	//
	// check that passwords match
	if ($p != $r) {
		$err .= 'Please make sure the passwords match.<br>';
		$p = "";
		$r = "";
	}
	
	//
	// check signupcode
	if ($_POST['signupcode'] != 'bingobango') {
		$err .= 'Sorry, that was not a valid signup code.';
	}
	
	//
	// check for no errors and make user
	if ($err == "") {
		$signupSuccess = true;
		$s = md5(uniqid());
		$vcode = sha1(uniqid());
		$p = sha1($p);
		$q = "insert into `clients` (`username`,`email`,`password`,`storage_id`,`verify_code`,`signup_date`) values ('$u','$e','$p','$s','$vcode','NOW()')";
		$ins = mysql_query($q)
			or die(mysql_error());
			
		//
		// get encoded var for activation email
		$str = $u . "&" . $vcode;
		$str = base64_encode($str);
		
		//
		// make message
		$message  = "You've successfully signed up for a BIG Show account. To complete the signup process, click on the link below.\n\n";
		$message .= "<a href='".ADMIN_URL."bigshow-signup.php?x=$str' target='_blank'>".ADMIN_URL."bigshow-signup.php?x=$str</a>";
		
		//
		// include sendgrid
		require_once('inc/mailer/class.phpmailer.php');
		require_once('inc/mailer/class.smtp.php');
		require_once('inc/mailer/smtp.php');
		
		//
		// send activation email via sendgrid
		$mail             = new PHPMailer();
		// SMTP Settings
		$mail->IsSMTP(); // telling the class to use SMTP
		$mail->Host       = "smtp.sendgrid.net";      // SMTP server
		$mail->SMTPDebug  = 0;                        // enables SMTP debug information (for testing)
		$mail->SMTPAuth   = true;                     // enable SMTP authentication
		$mail->Host       = "smtp.sendgrid.net";      // sets the SMTP server
		$mail->Port       = 25;                       // set the SMTP port 
		$mail->Username   = $_SMTP_USER; // SMTP account username
		$mail->Password   = $_SMTP_PASS; // SMTP account password

		$mail->SetFrom('bigshow@bigfolio.com', 'bigshow@bigfolio.com');
		$mail->Subject    = "Welcome to BIG Show";
		$mail->AltBody    = $message;
		$mail->MsgHTML(nl2br($message));
		$mail->AddAddress($e);

		// Send it ... 
		$result = $mail->Send();
	}
} else if (isset($_GET['x'])) {
	$str = base64_decode($_GET['x']);
	$str = explode('&', $str);
	$u = $str[0];
	$vc = $str[1];
	//
	$q = "select * from `clients` where `username` = '$u' and `verify_code` = '$vc'";
	$sel = mysql_query($q) 
		or die(mysql_error());
	if (mysql_num_rows($sel) == 0) {
		$err = "There was a problem with the verification link used.";
	} else {
		$user = mysql_fetch_array($sel, MYSQL_ASSOC);
		$uid = $user['id'];
		$q = "update `clients` set verify_code = '', active = 1 where id = $uid";
		$sel = mysql_query($q) 
			or die(mysql_error());
		$verifySuccess = true;
		//
		// setup settings row
		$q = "INSERT INTO `settings` (`settings_id`, `body_color`, `app_border_color`, `title_color`, `text_color`, `custom_color_1`, `custom_color_2`, `logo_file`, `flash_vars`, `copyright`) VALUES ($uid, '#ffffff', '#b8b8b8', '#dedede', '#47473f', '#485b70', '#6b86a3', '', '', '');";
		$sel = mysql_query($q) 
			or die(mysql_error());
	}
	//
	
} else if (isset($_GET['verify'])) {
	$signupSuccess = true;
}
?>
<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7 ]> <html class="no-js ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
       Remove this if you use the .htaccess -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>BIG Folio Admin 4</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile viewport optimized: j.mp/bplateviewport -->
  <meta name="viewport" content="width=device-width">

  <!-- Place favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
  <link rel="shortcut icon" href="/favicon.ico">


  <!-- CSS: implied media="all" -->
  <link rel="stylesheet" href="css/style.css?v=2">
  <link rel="stylesheet" href="css/bf4_login.css?v=2">
  <link rel="stylesheet" href="css/custom-theme/jquery-ui-1.8.16.custom.css?v=2">

  <!-- Uncomment if you are specifically targeting less enabled mobile browsers
  <link rel="stylesheet" media="handheld" href="css/handheld.css?v=2">  -->

  <!-- All JavaScript at the bottom, except for Modernizr which enables HTML5 elements & feature detects -->
  <script src="js/libs/modernizr-1.7.min.js"></script>

</head>

<body>

  <div id="signupContainer">
    <header>
	<div id="logo">
		<img src="img/logo-top.png" alt="BIG Folio">
	</div>
    </header>
    <div id="main" role="main">
		<div id="content">
			<? if ($signupSuccess) { ?>
				
			<div id="signupSuccess">
				<h1>Welcome to BIG Show!</h1>
				<p>Check your inbox for an email with an activation link. Click the link to activate your account and build your first slideshow!</p>
				<p class="loginSmallText">*If you don't see it in your inbox, make sure to check your spam folder.</p>
			</div>
			
			<? } else if ($verifySuccess) { ?>
				
			<div id="signupSuccess">
				<h1>You're all set!</h1>
				<p><a href="login.php">Click here to log in.</a></p>
			</div>
			
			<? } else { ?>
				
			<? if (isset($err)) { ?>
			<div class="error">
				<?=$err?>
			</div>
			<? } ?>
			<form action="bigshow-signup.php" method="post" accept-charset="utf-8">
				<ul>
					<li>
						<label for="username">Username: (<a class='bfToolTip' title='This will be used for your account&apos;s subdomain. Please use only letters and numbers and choose a username that is at least 4 letters long.'>?</a>)</label>
						<input name="username" id="username" type="text" class="bfInputText" value="<?=$u?>"/>
					</li>
					<li>
						<label for="email">Email: (<a class='bfToolTip' title='An activation email will be sent to this address. Make sure to enter an email address you can check.'>?</a>)</label>
						<input name="email" id="email" type="text" class="bfInputText" value="<?=$e?>"/>
					</li>
					<li>
						<label for="password">Password:</label>
						<input name="pass" id="pass" type="password" class="bfInputText" value="<?=$p?>"/>
					</li>
					<li>
						<label for="retype">Re-type:</label>
						<input name="retype" id="retype" type="password" class="bfInputText" value="<?=$r?>"/>
					</li>
					<li>
						<label for="signupcode">Signup code:</label>
						<input name="signupcode" id="signupcode" type="password" class="bfInputText"/>
					</li>
					<li>
						<input type="submit" name="submit" id="submit" value="Sign up!" class="uiButton"/><br /> 
					</li>
					<hr>
					<div class="loginSmallText">
						Already have an account? Login <a href="login.php">here</a>.
					</div>
					<!--
					<li>
						<a href="forgot.php" class="forgot">Forgot your password?</a>
					</li>
					-->
				</ul>
			</form>
			<? } ?>
		</div>
    </div>
  </div> <!--! end of #signupContainer -->


  <!-- JavaScript at the bottom for fast page loading -->

  <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if necessary -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.js"></script>
  <script>window.jQuery || document.write("<script src='js/libs/jquery-1.6.2.min.js'>\x3C/script>")</script>


  <!-- scripts concatenated and minified via ant build script-->
  <script src="js/plugins.js"></script>
  <script src="js/plugins/jquery-ui-1.8.16.custom.min.js"></script>
  <script src="js/plugins/jquery.tooltip.min.js"></script> 
  <script src="js/signup_script.js"></script>
  <!-- end scripts-->


  <!--[if lt IE 7 ]>
    <script src="js/libs/dd_belatedpng.js"></script>
    <script>DD_belatedPNG.fix("img, .png_bg"); // Fix any <img> or .png_bg bg-images. Also, please read goo.gl/mZiyb </script>
  <![endif]-->


  <!-- mathiasbynens.be/notes/async-analytics-snippet Change UA-XXXXX-X to be your site's ID -->
  <script>
    var _gaq=[["_setAccount","UA-XXXXX-X"],["_trackPageview"]];
    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];g.async=1;
    g.src=("https:"==location.protocol?"//ssl":"//www")+".google-analytics.com/ga.js";
    s.parentNode.insertBefore(g,s)}(document,"script"));
  </script>


  <!--[if lt IE 7 ]>
    <script src="js/libs/dd_belatedpng.js"></script>
    <script>DD_belatedPNG.fix("img, .png_bg"); // Fix any <img> or .png_bg bg-images. Also, please read goo.gl/mZiyb </script>
  <![endif]-->


</body>
</html>