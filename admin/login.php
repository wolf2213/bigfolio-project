<?php
session_start();            
define("ADMIN_VERSION","4.03");

if (stristr($_SERVER['HTTP_HOST'], 'bigsho.ws') && $_SERVER['HTTP_HOST'] != 'www.bigsho.ws') {
	header("location: http://www.bigsho.ws/login.php");
}

if (!isset($_POST['server']) && $_SERVER['REQUEST_METHOD'] == "POST") {
	$hostArr = explode(".", $_SERVER['HTTP_HOST']);
	$_POST['server'] = $hostArr[0]; //
}

if ((isset($_POST['email']) && isset($_POST['pass']) && isset($_POST['server'])) || isset($_GET['x'])) {
	
	session_destroy();
	session_start();
	
	if (isset($_GET['x'])) {
		$str = $_GET['x'];
		$str = base64_decode($str);
		$str = explode('&', $str);
		$e = $str[0]; 
		
		$p = $str[1]; 
		$_SESSION['server'] = $str[2];
		//echo 'email: '.$e.'<br>';
		//echo 'password: '.$p.'<br>';
		//echo 'server: '.$_SESSION['server'].'<br>';
	} else {
		$e = $_POST['email'];
		$p = sha1($_POST['pass']);
		$_SESSION['server'] = $_POST['server'];
	}
	
	include('inc/define.php');
	
	 
	/*
	echo 'DB_HOST: '.DB_HOST;
	echo 'DB_USER: '.DB_USER;
	echo 'DB_PASS: '.DB_PASS;
	echo 'DB_NAME: '.DB_NAME;
	//die();  
	*/
	

	//*****
	// DB Connect for single sign on
	//*****
	$link = mysql_connect(DB_HOST, DB_USER, DB_PASS) or mysql_error();
	$conn = mysql_select_db(DB_NAME);
	//*****
	
	$q = "select * from `clients` where `email` = '$e' and `password` = '$p'";
	$sel = mysql_query($q) 
		or die(mysql_error());
	if (mysql_num_rows($sel) == 0) {
		$_SESSION["ERROR"] = "The email address or password you entered did not match";
		header("Location:login.php");
	} else { 
		//
		// get user
		$user = mysql_fetch_array($sel, MYSQL_ASSOC);
		$_SESSION['is_signed_in'] = true;
		$_SESSION['user'] = $user;
		
		echo $_SESSION['user']['url'];
		
		//
		// handle login type
		//echo 'server: '.$_SESSION['server'];
		if ($_SESSION['server'] == 'bigshow') {
			
			$_SESSION['user']['db_user'] = DB_USER;
			$_SESSION['user']['db_pass'] = DB_PASS;
			$_SESSION['user']['db_name'] = DB_NAME;
			include("inc/db.inc.php");
			include("inc/functions.inc.php");
			$s = get_settings();
			if ($_SESSION['user']['verify_code'] != "" && $_SESSION['user']['active'] == 0) {
				header("Location:./bigshow-signup.php?verify=0");
			} else if ($_SESSION['user']['verify_code'] != "" && $_SESSION['user']['active'] == 1) {
				header("Location:./bigshow-reset.php");
			} else if ($s['logo_file'] == "") {
				header("Location:./#/bigshow/bigshow-slideshow-settings");
			} else {
				header("Location:./#/bigshow/bigshow-slideshows");
			}
			
		} else {
		 
			//
			// figure temp url 
			$isLive = FALSE;
			if (stristr($_SESSION['user']['url'], 'bigfoliodemo') === FALSE && stristr($_SESSION['user']['path'], 'subdomains') === FALSE) {
				$urlArr = explode("/",$_SESSION['user']['url']);
				$domain = $urlArr[2]; 
				$liveCheck = gethostbyname($domain); //file_get_contents("http://dnscheck.bigfolio.com/?host=".$domain);
				if (substr($domain, 0, 4) != "www.") {
					//$domain = "www.".$domain;
					//$liveCheck .= file_get_contents("http://dnscheck.bigfolio.com/?host=".$domain);
				}
			 
				if (stristr($liveCheck, IP)) { 
					$isLive = TRUE;
					//$_SESSION['user']['temp_url'] = $_SESSION['user']['url'] . '/';
					//$_SESSION['user']['temp_url'] = $_SESSION['user']['db_user'] . '/';
					$_SESSION['user']['temp_url'] = $_SESSION['user']['url'] . '/';
					//$_SESSION['user']['temp_url'] = getenv('APACHE_RUN_USER');
					//$_SESSION['user']['temp_url'] = posix_getpwuid(getmyuid()); 
					//echo "Try1: " . $_SESSION['user']['temp_url'];
				} else {
					if ($_SESSION['user']['web_user'] <> '') {
						$_SESSION['user']['temp_url'] = 'http://' . IP . '/~' . $_SESSION['user']['web_user'] . '/';
						$_SESSION['user']['server_url'] = 'http://' . IP . '/~' . $_SESSION['user']['web_user'] . '/'; 
					} else {
					$_SESSION['user']['temp_url'] = 'http://' . IP . '/~' . $_SESSION['user']['db_user'] . '/';
					$_SESSION['user']['server_url'] = 'http://' . IP . '/~' . $_SESSION['user']['db_user'] . '/';
					}
					//$_SESSION['user']['temp_url'] =  'http://' . IP . '/~' . getenv('APACHE_RUN_USER');
					//$_SESSION['user']['temp_url'] =  'http://' . IP . '/~' . posix_getpwuid(getmyuid());
					//echo "Try2: " . $_SESSION['user']['temp_url'];
				}
				//$_SESSION['user']['server_url'] = 'http://' . IP . '/~' . $_SESSION['user']['db_user'] . '/'; 
				//echo "Try3: " . $_SESSION['user']['temp_url'];  
			} else {
				$isLive = FALSE;
				// $_SESSION['user']['temp_url'] = $_SESSION['user']['url'] . '/';
				// $_SESSION['user']['server_url'] = $_SESSION['user']['url'] . '/';
				
				if ($_SESSION['user']['web_user'] <> '') {
						$_SESSION['user']['temp_url'] = 'http://' . IP . '/~' . $_SESSION['user']['web_user'] . '/';
						$_SESSION['user']['server_url'] = 'http://' . IP . '/~' . $_SESSION['user']['web_user'] . '/';
					} else {
				$_SESSION['user']['temp_url'] = $_SESSION['user']['url'] . '/';
				$_SESSION['user']['server_url'] = $_SESSION['user']['url'] . '/';
				
				//echo "Try4 - islive ius false: " . $_SESSION['user']['temp_url'];			
					}
			}
			$context = stream_context_create(array('http' => array('header'=>'Connection: close\r\n')));
			$tempCheck = '';//file_get_contents($_SESSION['user']['temp_url'],false,$context);
			//$tempCheck = $data;//file_get_contents($_SESSION['user']['temp_url']);
			//
			// get sub dirs
			$pathsArr = explode('httpdocs/', $_SESSION['user']['path']);
			$db_user = $_SESSION['user']['db_user'];
			//
			/* remove this code for now
			if (count($pathsArr) > 1 && $isLive === false) {
				// look for another account for the correct db_user for the path
				$rootPath = $pathsArr[0] . 'httpdocs/';
				$q = "select * from `clients` where `path` = '$rootPath' and `db_user` != '$db_user'";
				$sel = mysql_query($q) 
					or die(mysql_error());
				$parentUser = mysql_fetch_array($sel, MYSQL_ASSOC);
				if (isset($parentUser['db_user']) && $tempCheck == "") {
					$_SESSION['user']['temp_url'] = 'http://' . IP . '/~' . $parentUser['db_user'] . '/';
					$_SESSION['user']['server_url'] = $_SESSION['user']['temp_url'];
				} else if (!isset($parentUser['db_user']) && $tempCheck == ""){
					$q = "SELECT * FROM  `clients` WHERE  `path` LIKE '%$rootPath%' AND `db_user` != '$db_user'";
					$sel = mysql_query($q) 
						or die(mysql_error());
					$parentUser = mysql_fetch_array($sel, MYSQL_ASSOC);
					if (isset($parentUser['db_user'])) {
						$_SESSION['user']['temp_url'] = 'http://' . IP . '/~' . $parentUser['db_user'] . '/';
						$_SESSION['user']['server_url'] = $_SESSION['user']['temp_url'];
					}
				}
			}
			
			
			if ($pathsArr[1] != "" && stristr($_SESSION['user']['url'], 'bigfoliodemo') === FALSE && stristr($_SESSION['user']['path'], 'subdomains') === FALSE && $isLive === FALSE) {
				 $_SESSION['user']['temp_url'] .= $pathsArr[1] . '/'; 
				 $_SESSION['user']['server_url'] .= $pathsArr[1] . '/';
			}
		*/
			// now convert double slashes into single slashes
			$_SESSION['user']['temp_url'] = "http://" . str_replace("//", "/", substr($_SESSION['user']['temp_url'], 7));
			$_SESSION['user']['server_url'] = "http://" . str_replace("//", "/", substr($_SESSION['user']['server_url'], 7));
		
			// 
			// assign template and template name to session data
			include('inc/db.inc.php');
			$q = "select * from properties where property_id = 'TEMPLATE'";
			$sel = mysql_query($q)
				or die(mysql_error());
			$row = mysql_fetch_array($sel,MYSQL_ASSOC);
			$_SESSION['template'] = $row['property_value'];
		
			$q = "select * from properties where property_id = 'TEMPLATE_NAME'";
			$sel = mysql_query($q)
				or die(mysql_error());
			$row = mysql_fetch_array($sel,MYSQL_ASSOC);
			$_SESSION['template_name'] = $row['property_value'];
		
			$sql="SELECT * FROM folders";
			$result=@mysql_query($sql);

			if (!$result) {
				$_SESSION['hasFolders'] = 'false';
			} else {
				$_SESSION['hasFolders'] = 'true';
			}
			//
			$q = "select * from settings where settings_id = 1";
			$sel = mysql_query($q)
				or die(mysql_error());
			$s = mysql_fetch_array($sel, MYSQL_ASSOC);
			if ($_SESSION['template'] == 'rubix' && $s['flash_vars'] == "") {
				header("Location:./#/site-settings/layout-settings");
			} else {
				// comment out for testing
				 header("Location:./#/site-settings/profile");
			
			}
		}
	}
} else { 
	//include('inc/define.php');
	//echo($_SESSION['ERROR']);
	// Normal GET request
	// Check for session error
	if (isset($_SESSION['ERROR']) && 
	($_SESSION['ERROR'] != "Access denied for user 'www-data'@'localhost' (using password: NO)" 
	&& stristr($_SESSION['ERROR'], "mysql") === FALSE)) {
		$err = $_SESSION['ERROR'];
		//$_SESSION['ERROR'] = "<!-- ".$_SESSION['ERROR']." -->";
	}
	if (isset($_GET['logout'])) {
		unset($_SESSION['admin']);
		unset($_SESSION['is_signed_in']);
		unset($_SESSION['ERROR']);
		unset($_SESSION['hasFolders']);
		header("Location:login.php");
	} 
}
?>
<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7 ]> <html class="no-js ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
       Remove this if you use the .htaccess -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>BIG Folio Admin 4</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile viewport optimized: j.mp/bplateviewport -->
  <meta name="viewport" content="width=device-width">

  <!-- Place favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
  <link rel="shortcut icon" href="/favicon.ico">


  <!-- CSS: implied media="all" -->
  <link rel="stylesheet" href="css/style.css?v=2">
  <link rel="stylesheet" href="css/bf4_login.css?v=2">
  <link rel="stylesheet" href="css/custom-theme/jquery-ui-1.8.16.custom.css?v=2">

  <!-- Uncomment if you are specifically targeting less enabled mobile browsers
  <link rel="stylesheet" media="handheld" href="css/handheld.css?v=2">  -->

  <!-- All JavaScript at the bottom, except for Modernizr which enables HTML5 elements & feature detects -->
  <script src="js/libs/modernizr-1.7.min.js"></script>

</head>

<body>

  <div id="container">
    <header>
	<div id="logo">
		<img src="img/bf_logo_2014_horizontal.jpg" alt="BIG Folio">
	</div>
    </header>
    <div id="main" role="main">
		<div id="content">
			<? if (isset($err)) { ?>
			<div class="error">
				<?=$err?>
			</div>
			<? } ?>
			<form action="login.php" method="post" accept-charset="utf-8">
				<ul>
					<li>
						<label for="email">Email:</label>
						<input name="email" id="email" type="text" class="bfInputText" />
					</li>
					<li>
						<label for="password">Password:</label>
						<input name="pass" id="pass" type="password" class="bfInputText" />
					</li>
					<? if ($_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == 'macbook') { ?>
					<li>                                               
						<label for="server">Server:</label>
						<select name="server" value="Please select one:">
							<option value="localhost">localhost</option>
							<option value="bigshow">bigshow</option>
							<option value="rickey">rickey</option>
							<option value="sandberg4">sandberg</option>
							<option value="ripken4">ripken</option>
							<option value="dawson4">dawson</option>
							<option value="saberhagen4">saberhagen</option>
							<option value="puckett4">puckett</option>
							<option value="mattingly4">mattingly</option>
						</select>
					</li>
					<? } else { ?>
						<?
						/*
						if (stristr($_SERVER['HTTP_HOST'], "mattingly") !== FALSE) {
							$server = "mattingly";
						} else 
						if (stristr($_SERVER['HTTP_HOST'], "puckett") !== FALSE) {
							$server = "puckett";
						} else
						if (stristr($_SERVER['HTTP_HOST'], "saberhagen") !== FALSE) {
							$server = "saberhagen";
						} else
						if (stristr($_SERVER['HTTP_HOST'], "dawson") !== FALSE) {
							$server = "dawson";
						} else 
						if (stristr($_SERVER['HTTP_HOST'], "ripken") !== FALSE) {
							$server = "ripken";
						} else                 
						if (stristr($_SERVER['HTTP_HOST'], "sandberg") !== FALSE) {
							$server = "sandberg";
						}
						*/ 
						$hostArr = explode(".", $_SERVER['HTTP_HOST']);
						$server = $hostArr[0]; //substr($hostArr[0], 7); // get rid of http://   
						
						if ($server == "bfbs" || $_SERVER['HTTP_HOST'] == "www.bigsho.ws") { $server = "bigshow"; }
						
						?>
						<input type="hidden" name="server" value="<?=$server?>" />
					<? } ?>
					<li>
						<input type="submit" name="submit" id="submit" value="Sign In" class="uiButton"/><br /> 
					</li>
					<? if ($_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == 'macbook' || $_SERVER['HTTP_HOST'] == 'www.bigsho.ws' || $_SERVER['HTTP_HOST'] == 'bigshow.phpfogapp.com' || $_SERVER['HTTP_HOST'] == 'bfbs.phpfogapp.com') { ?>
						<hr>
						<div class="loginSmallText">
							<a href="bigshow-signup.php">Create a free account.</a>
							<br>
							<a href="bigshow-forgot.php">Forgot your password?</a>
						</div>
					<? } ?>
					<!--
					<li>
						<a href="forgot.php" class="forgot">Forgot your password?</a>
					</li>
					-->
				</ul>
			</form>
		</div>
    </div>
  </div> <!--! end of #container -->
  <footer>
		<p>BIG Folio version <?=ADMIN_VERSION?> &bull; <a href="http://bigfolio.com/help/">Help</a></p>
  </footer>


  <!-- JavaScript at the bottom for fast page loading -->

  <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if necessary -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.js"></script>
  <script>window.jQuery || document.write("<script src='js/libs/jquery-1.6.2.min.js'>\x3C/script>")</script>


  <!-- scripts concatenated and minified via ant build script-->
  <script src="js/plugins.js"></script>
  <script src="js/plugins/jquery-ui-1.8.16.custom.min.js"></script>
  <script src="js/login_script.js"></script>
  <!-- end scripts-->


  <!--[if lt IE 7 ]>
    <script src="js/libs/dd_belatedpng.js"></script>
    <script>DD_belatedPNG.fix("img, .png_bg"); // Fix any <img> or .png_bg bg-images. Also, please read goo.gl/mZiyb </script>
  <![endif]-->


  <!-- mathiasbynens.be/notes/async-analytics-snippet Change UA-XXXXX-X to be your site's ID -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33048620-1']);
  _gaq.push(['_setDomainName', 'bigfolio4.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>


  <!--[if lt IE 7 ]>
    <script src="js/libs/dd_belatedpng.js"></script>
    <script>DD_belatedPNG.fix("img, .png_bg"); // Fix any <img> or .png_bg bg-images. Also, please read goo.gl/mZiyb </script>
  <![endif]-->


</body>
</html>
