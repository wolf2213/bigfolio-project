<?php
session_start();            
define("ADMIN_VERSION","4.02");
$_SESSION['server'] = "bigshow";
//
// db stuff
include('inc/define.php');
$_SESSION['user']['db_user'] = DB_USER;
$_SESSION['user']['db_pass'] = DB_PASS;
$_SESSION['user']['db_name'] = DB_NAME;
include("inc/db.inc.php");
include("inc/functions.inc.php");
$err = "";
$resetSent = false;
//
function sendResetEmail($e, $str) {
	
	//
	// make message
	$message  = "A request to reset your BIG show password was sent. If you did not make this request, you can ignore this email. To reset your password, click the link below.\n\n";
	$message .= "<a href='".ADMIN_URL."bigshow-forgot.php?x=$str' target='_blank'>".ADMIN_URL."bigshow-forgot.php?x=$str</a>";
	
	//
	// include sendgrid
	require_once('inc/mailer/class.phpmailer.php');
	require_once('inc/mailer/class.smtp.php');
	require_once('inc/mailer/smtp.php');
	
	//
	// send activation email via sendgrid
	$mail             = new PHPMailer();
	// SMTP Settings
	$mail->IsSMTP(); // telling the class to use SMTP
	$mail->Host       = "smtp.sendgrid.net";      // SMTP server
	$mail->SMTPDebug  = 2;                        // enables SMTP debug information (for testing)
	$mail->SMTPAuth   = true;                     // enable SMTP authentication
	$mail->Host       = "smtp.sendgrid.net";      // sets the SMTP server
	$mail->Port       = 25;                       // set the SMTP port 
	$mail->Username   = $_SMTP_USER; // SMTP account username
	$mail->Password   = $_SMTP_PASS; // SMTP account password

	$mail->SetFrom('bigshow@bigfolio.com', 'bigshow@bigfolio.com');
	$mail->Subject    = "Welcome to BIG Show";
	$mail->AltBody    = $message;
	$mail->MsgHTML(nl2br($message));
	$mail->AddAddress($e);

	// Send it ... 
	$result = $mail->Send();
	
}
//
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	//
	// vars
	if (isset($_POST['email'])) {
		$e = $_POST['email'];
		//
		// check if email exists in db
		$q = "select * from clients where email = '$e'";
		$sel = mysql_query($q)
			or die(mysql_error());
		if (mysql_num_rows($sel)) {
			$user = mysql_fetch_array($sel, MYSQL_ASSOC);
			if ($user["active"] == 1) {
				$uid = $user['id'];
				$vcode = sha1(uniqid());
				$q = "update `clients` set verify_code = '$vcode' where id = $uid";
				$sel = mysql_query($q) 
					or die(mysql_error());
			
				$str = $user['username'] . "&" . $vcode;
				$str = base64_encode($str);
			
				sendResetEmail($e, $str);
				$resetSent = true;
			} else {
				$err .= 'Sorry, that account has not been verified.';
			}
		} else {
			$err .= 'Sorry, that email was not found.';
			$e = "";
		}
	} else if (isset($_POST['pass'])) {
		$id = $_POST['id'];
		$pass = sha1($_POST['pass']);
		$q = "update `clients` set password = '$pass' where id = $id";
		$sel = mysql_query($q) 
			or die(mysql_error());
		$_SESSION['error'] = "Password has been successfully reset";
		header("Location:./login.php");
	}
} else if (isset($_GET['x'])) {
	$str = base64_decode($_GET['x']);
	$str = explode('&', $str);
	$u = $str[0];
	$vc = $str[1];
	//
	$q = "select * from `clients` where `username` = '$u' and `verify_code` = '$vc'";
	$sel = mysql_query($q) 
		or die(mysql_error());
	if (mysql_num_rows($sel) == 0) {
		$err = "There was a problem with the reset link used.";
	} else {
		$user = mysql_fetch_array($sel, MYSQL_ASSOC);
		$uid = $user['id'];
		$q = "update `clients` set verify_code = '' where id = $uid";
		$sel = mysql_query($q) 
			or die(mysql_error());
		$resetOK = true;
	}
	//

}
?>
<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7 ]> <html class="no-js ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
       Remove this if you use the .htaccess -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>BIG Folio Admin 4</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile viewport optimized: j.mp/bplateviewport -->
  <meta name="viewport" content="width=device-width">

  <!-- Place favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
  <link rel="shortcut icon" href="/favicon.ico">


  <!-- CSS: implied media="all" -->
  <link rel="stylesheet" href="css/style.css?v=2">
  <link rel="stylesheet" href="css/bf4_login.css?v=2">
  <link rel="stylesheet" href="css/custom-theme/jquery-ui-1.8.16.custom.css?v=2">

  <!-- Uncomment if you are specifically targeting less enabled mobile browsers
  <link rel="stylesheet" media="handheld" href="css/handheld.css?v=2">  -->

  <!-- All JavaScript at the bottom, except for Modernizr which enables HTML5 elements & feature detects -->
  <script src="js/libs/modernizr-1.7.min.js"></script>

</head>

<body>

  <div id="signupContainer">
    <header>
	<div id="logo">
		<img src="img/logo-top.png" alt="BIG Folio">
	</div>
    </header>
    <div id="main" role="main">
		<div id="content">
			<? if ($resetSent) { ?>
				
			<div id="resetSent">
				<p>Check your inbox for an email with a reset link. Click the link to reset your password.</p>
				<p class="loginSmallText">*If you don't see it in your inbox, make sure to check your spam folder.</p>
			</div>
			
			<? } else if ($resetOK) { ?>
				
				<form action="bigshow-forgot.php" method="post" accept-charset="utf-8">
					<input type="hidden" name="id" value="<?=$uid?>"/>
					<ul>
						<li>Enter a new password.</li>
						<li>
							<label for="password">Password:</label>
							<input name="pass" id="pass" type="password" class="bfInputText" value="<?=$p?>"/>
						</li>
						<li>
							<input type="submit" name="submit" id="submit" value="Reset" class="uiButton"/><br /> 
						</li>
					</ul>
				</form>
			
			<? } else { ?>
				
			<? if (isset($err)) { ?>
			<div class="error">
				<?=$err?>
			</div>
			<? } ?>
			<form action="bigshow-forgot.php" method="post" accept-charset="utf-8">
				<ul>
					<li>Enter your email to reset your password.</li>
					<li>
						<label for="email">Email: (<a class='bfToolTip' title='An activation email will be sent to this address. Make sure to enter an email address you can check.'>?</a>)</label>
						<input name="email" id="email" type="text" class="bfInputText" value="<?=$e?>"/>
					</li>
					<li>
						<input type="submit" name="submit" id="submit" value="Reset" class="uiButton"/><br /> 
					</li>
				</ul>
			</form>
			<? } ?>
		</div>
    </div>
  </div> <!--! end of #signupContainer -->


  <!-- JavaScript at the bottom for fast page loading -->

  <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if necessary -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.js"></script>
  <script>window.jQuery || document.write("<script src='js/libs/jquery-1.6.2.min.js'>\x3C/script>")</script>


  <!-- scripts concatenated and minified via ant build script-->
  <script src="js/plugins.js"></script>
  <script src="js/plugins/jquery-ui-1.8.16.custom.min.js"></script>
  <script src="js/plugins/jquery.tooltip.min.js"></script> 
  <script src="js/signup_script.js"></script>
  <!-- end scripts-->


  <!--[if lt IE 7 ]>
    <script src="js/libs/dd_belatedpng.js"></script>
    <script>DD_belatedPNG.fix("img, .png_bg"); // Fix any <img> or .png_bg bg-images. Also, please read goo.gl/mZiyb </script>
  <![endif]-->


  <!-- mathiasbynens.be/notes/async-analytics-snippet Change UA-XXXXX-X to be your site's ID -->
  <script>
    var _gaq=[["_setAccount","UA-XXXXX-X"],["_trackPageview"]];
    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];g.async=1;
    g.src=("https:"==location.protocol?"//ssl":"//www")+".google-analytics.com/ga.js";
    s.parentNode.insertBefore(g,s)}(document,"script"));
  </script>


  <!--[if lt IE 7 ]>
    <script src="js/libs/dd_belatedpng.js"></script>
    <script>DD_belatedPNG.fix("img, .png_bg"); // Fix any <img> or .png_bg bg-images. Also, please read goo.gl/mZiyb </script>
  <![endif]-->


</body>
</html>